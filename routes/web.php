<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::any('/logout', 'UserController@logout');
Route::get('/reset-password/{token}', 'UserController@resetPassword');
Route::post('/login', 'UserController@loginPost');
Route::post('/forgot-password-submit', 'UserController@forgotPasswordPost');
Route::post('/change-password-submit', 'UserController@changePasswordPost');
Route::post('/reset-password-submit', 'UserController@resetPasswordPost');

//change language
Route::post('/switch-language','UserController@switchLanguage');

Route::group(['middleware' => 'CheckLogin'], function () {
	Route::get('/', 'UserController@index');
	Route::get('/login', 'UserController@index');
	Route::get('/forgot-pass', 'UserController@forgotPass');
	Route::get('/verify-email', 'UserController@verifyEmail');
});

Route::prefix('customapi')->namespace('CustomAPI')->group(function(){
	Route::get('/render-certificate/{id}','CustomAPIController@renderCertificate');
	Route::get('/delete-schoolclasses/{school_id}','TestController@deleteSchoolClasses');
	Route::get('/create-pdf/{id}','TestController@createPdf');
	Route::get('empty-tables', 'TestController@emptytables');
	Route::get('/villageschool-teachers','TestController@getTeachers');
	Route::get('get-tables','TestController@getTablesWithColumns');
	Route::post('/test','TestController@tests');
});