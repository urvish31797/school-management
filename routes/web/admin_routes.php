<?php

Route::prefix('admin')->namespace('Admin')->group(function () {

	//Routes For Hertronic Admin
	Route::group(['middleware'=>'HertronicAdmin'],function(){

		//Route for Ministries
		Route::post('/ministries-list', 'MinistryController@ministries_list')->name('ministries.list');
		Route::resource('/ministries', 'MinistryController');

		//Route for Login as
		Route::post('/loginas-list', 'LoginAsController@loginas_list')->name('fetch-loginas-lists');
		Route::post('/login-as', 'LoginAsController@auth_loginas')->name('login-as');
		Route::resource('/loginas', 'LoginAsController');

		//Route for Academic Degrees
		Route::post('/academicdegree-list', 'AcademicDegreeController@academicdegree_list')->name('academicdegree.list');
		Route::resource('academicdegree', 'AcademicDegreeController');

		//Route for Job & Works
		Route::post('/jobandwork-list', 'JobAndWorkController@jobandwork_list')->name('jobandwork.list');
		Route::resource('jobandwork','JobAndWorkController');

		//Route for Countries
		Route::post('/country-list', 'CountryController@country_list')->name('country.list');
		Route::resource('/country', 'CountryController');

		//Route for States
		Route::post('/state-list', 'StateController@state_list')->name('state.list');
		Route::resource('/state', 'StateController');

		//Route for Cantons
		Route::post('/canton-list', 'CantonController@canton_list')->name('canton.list');
		Route::post('/statebycountry-list', 'CantonController@statebycountry_list')->name('fetch-statebycountry-lists');
		Route::resource('/canton', 'CantonController');

		//Route for Municipalities
		Route::post('/municipalities-list', 'MunicipalityController@municipalities_list')->name('municipalities.list');
		Route::resource('/municipalities', 'MunicipalityController');

		//Route for Postal Codes
		Route::post('/postalcodes-list', 'PostalCodeController@postalcodes_list')->name('postalcodes.list');
		Route::resource('/postalcodes', 'PostalCodeController');

		//Route for Ownership Types
		Route::post('/ownershiptypes-list', 'OwnershipTypeController@ownershiptypes_list')->name('ownershiptypes.list');
		Route::resource('/ownershiptypes', 'OwnershipTypeController');

		//Route for Universities
		Route::post('/universities-list', 'UniversityController@universities_list')->name('universities.list');
		Route::resource('/universities', 'UniversityController');

		//Route for Colleges
		Route::post('/colleges-list', 'CollegeController@colleges_list')->name('colleges.list');
		Route::resource('/colleges', 'CollegeController');

		//Route for Grades
		Route::post('/grades-list', 'GradeController@grades_list')->name('grades.list');
		Route::resource('/grades', 'GradeController');

		//Route for Classes
		Route::post('/classes-list', 'ClassController@classes_list')->name('classes.list');
		Route::resource('/classes', 'ClassController');

		//Route for Nationalities
		Route::post('/nationalities-list', 'NationalityController@nationalities_list')->name('nationalities.list');
		Route::resource('/nationalities', 'NationalityController');

		//Route for Religions
		Route::post('/religions-list', 'ReligionController@religions_list')->name('religions.list');
		Route::resource('/religions', 'ReligionController');

		//Route for Citizenships
		Route::post('/citizenships-list', 'CitizenshipController@citizenships_list')->name('citizenships.list');
    	Route::resource('/citizenships', 'CitizenshipController');

		//Route for Vocations
		Route::post('/vocations-list', 'VocationController@vocations_list')->name('vocations.list');
		Route::resource('/vocations', 'VocationController');

		//Route for Education Period
		Route::post('/educationperiod-list', 'EducationPeriodController@educationperiod_list')->name('educationperiod.list');
		Route::resource('/educationperiod', 'EducationPeriodController');

		//Route for Courses
		Route::post('/courses-list', 'CourseController@courses_list')->name('courses.list');
		Route::resource('/courses', 'CourseController');

		//Route for Descriptive Final Marks
		Route::post('/descriptivefinalmarks-list', 'DFMController@descriptivefinalmarks_list')->name('descriptivefinalmarks.list');
		Route::resource('/descriptivefinalmarks', 'DFMController');

		//Route for Engagement Type
		Route::post('/engagementtypes-list', 'EngagementTypeController@engagementtypes_list')->name('engagementtypes.list');
		Route::resource('/engagementtypes', 'EngagementTypeController');

		//Route for Shifts
		Route::post('/shifts-list', 'ShiftsController@shifts_list')->name('shifts.list');
		Route::resource('/shifts', 'ShiftsController');

		//Route for National Education Plans
		Route::post('/nationaleducationplans-list', 'NationalEducationPlanController@nationaleducationplans_list')->name('nationaleducationplans.list');
		Route::resource('/nationaleducationplans', 'NationalEducationPlanController');

		//Route for Education Profiles
		Route::post('/educationprofiles-list', 'EducationProfileController@educationprofiles_list')->name('educationprofiles.list');
		Route::resource('/educationprofiles', 'EducationProfileController');

		//Route for Qualification Degrees
		Route::post('/qualificationdegrees-list', 'QualificationDegreeController@qualificationdegrees_list')->name('qualificationdegrees.list');
		Route::resource('/qualificationdegrees', 'QualificationDegreeController');

		//Route for Grade Attempts Number
		Route::post('/gradeattemptsnumber-list', 'GradeAttemptsNumberController@gradeattemptsnumber_list')->name('gradeattemptsnumber.list');
		Route::resource('/gradeattemptsnumber', 'GradeAttemptsNumberController');

		//Route for Certificate Type
		Route::post('/certificatetype-list', 'CertificateTypeController@certificatetype_list')->name('certificatetype.list');
		Route::resource('/certificatetype', 'CertificateTypeController');

		//Route for Mark Explanation
		Route::post('/markexplanation-list', 'MarkExplanationController@markexplanation_list')->name('markexplanation.list');
		Route::resource('/markexplanation', 'MarkExplanationController');

		//Route for Certificate Blueprints
		Route::post('/certificateblueprints-list', 'CertificateBlueprintsController@certificateblueprints_list')->name('certificateblueprints.list');
		Route::resource('/certificateblueprints', 'CertificateBlueprintsController');

		//Route for School Year
		Route::post('/change-schoolyear', 'SchoolYearController@change_schoolyear')->name('change-schoolyear');
		Route::post('/schoolyear-list', 'SchoolYearController@schoolyear_list')->name('schoolyear.list');
		Route::resource('schoolyear','SchoolYearController');

		//Route for Attendance Way
		Route::post('/change-attendanceway','AttendanceWayController@change_attendanceway')->name('change-attendanceway');
		Route::post('/attendanceway-list','AttendanceWayController@attendanceway_list')->name('attendanceway.list');
		Route::resource('attendanceway','AttendanceWayController');

		//Route for Education Way
		Route::post('/change-educationway','EducationwayController@change_educationway')->name('change-educationway');
		Route::post('/educationway-list','EducationwayController@educationway_list')->name('educationway.list');
		Route::resource('educationway','EducationwayController');

		//Route for Periodic Exam Way
		Route::post('/change-periodicexamway','PeriodicExamWayController@change_periodicexamway')->name('change-periodicexamway');
		Route::post('/periodicexamway-list','PeriodicExamWayController@periodicexamway_list')->name('periodicexamway.list');
		Route::resource('periodicexamway','PeriodicExamWayController');

		//Route for Descriptive periodic exam mark
		Route::post('/descriptiveperiodicexammark-list','DescriptivePeriodicExamMarkController@descriptiveperiodicexammark_list')->name('descriptiveperiodicexammark.list');
		Route::resource('descriptiveperiodicexammark','DescriptivePeriodicExamMarkController');

		//Route for working weeks
		Route::post('/working-weeks-list','WorkingWeeksController@working_weeks_list')->name('working-weeks.list');
		Route::resource('working-weeks','WorkingWeeksController');

		//Route for Periodic Exam Type
		Route::post('/periodic-exam-type-list','PeriodicExamTypeController@periodic_exam_type_list')->name('periodic-exam-type.list');
		Route::resource('periodic-exam-type','PeriodicExamTypeController');

		//Routes for Village School
		Route::post('/villageschools-list', 'VillageSchoolController@villageschools_list')->name('villageschools.list');
		Route::resource('/villageschools', 'VillageSchoolController');

		//Route for Student Behaviour
		Route::post('/studentbehaviour-list', 'StudentBehaviourController@studentbehaviour_list')->name('studentbehaviour.list');
		Route::resource('/studentbehaviour', 'StudentBehaviourController');

		//Route for Extra Curricural Activity Type
		Route::post('/extracurricuralactivitytype-list', 'ExtracurricuralActivityTypeController@extracurricuralactivitytype_list')->name('extracurricuralactivitytype.list');
		Route::resource('/extracurricuralactivitytype', 'ExtracurricuralActivityTypeController');

		//Route for Discipline Measure Type
		Route::post('/disciplinemeasuretype-list', 'StudentDisciplineMeasureTypeController@disciplinemeasuretype_list')->name('disciplinemeasuretype.list');
		Route::resource('/disciplinemeasuretype', 'StudentDisciplineMeasureTypeController');

		//Route for Foreign Language Group
		Route::post('/foreignlanguagegroup-list', 'ForeignLanguageGroupController@foreignlanguagegroup_list')->name('foreignlanguagegroup.list');
		Route::resource('/foreignlanguagegroup', 'ForeignLanguageGroupController');

		//Route for Optional Courses Group Type
		Route::post('/optionalcoursesgroup-list', 'OptionalCoursesGroupController@optionalcoursesgroup_list')->name('optionalcoursesgroup.list');
		Route::resource('/optionalcoursesgroup', 'OptionalCoursesGroupController');

		//Route for Facultative Courses Groups
		Route::post('/facultativecoursesgroup-list', 'FacultativeCoursesGroupController@facultativecoursesgroup_list')->name('facultativecoursesgroup.list');
		Route::resource('/facultativecoursesgroup', 'FacultativeCoursesGroupController');

		//Route for General Courses Groups
		Route::post('/generalpurposegroup-list', 'GeneralPurposeGroupController@generalpurposegroup_list')->name('generalpurposegroup.list');
		Route::resource('/generalpurposegroup', 'GeneralPurposeGroupController');

		//Route for Sub Class Groups
		Route::post('/subclassgroup-list', 'SubClassGroupController@subclassgroup_list')->name('subclassgroup.list');
		Route::resource('/subclassgroup', 'SubClassGroupController');

		//Route for Education Program
		Route::post('/educationprogram-list', 'EducationProgramController@educationprogram_list')->name('fetch-educationprogram-lists');
		Route::resource('/educationprogram', 'EducationProgramController');

		//Route for Language
		Route::post('/languages-list', 'LanguageController@languages_list')->name('languages.list');
		Route::resource('/languages', 'LanguageController');

		//Route for Translations
		Route::post('/translations-list', 'TranslationController@translations_list')->name('translations.list');
		Route::resource('/translations', 'TranslationController');
	});

	// Routes for Ministry Admin
	Route::group(['middleware'=>'MinistryAdmin'],function(){

		//Route for Sub Admins
		Route::post('/subadmin-list', 'SubAdminController@subadmin_list')->name('fetch-subadmin-list');
		Route::resource('/subadmin', 'SubAdminController');

	});

	Route::group(['middleware'=>'CheckHertronicAndMinistryAdmin'],function(){

		//Route for dashboard
		Route::get('/profile', 'AdminController@profile')->name('my-profile');
		Route::post('/edit-profile', 'AdminController@editProfile');
		Route::resource('/dashboard', 'AdminController');

		//Route for Schools
		Route::post('/schools-list', 'SchoolController@schools_list')->name('fetch-schools-lists');
		Route::post('/fetcheducationplan-school', 'SchoolController@fetchEducationPlan')->name('fetch-educationplan-school');
		Route::post('/delete-educationplan', 'SchoolController@educationPlanDeleteCheck')->name('delete-educationplan');
		Route::resource('/schools', 'SchoolController');

		//Route for Employees
		Route::post('/employees-list', 'TeacherController@employees_list')->name('fetch-employees-lists');
		Route::resource('/employees', 'TeacherController');

		//Routes for Student
		Route::post('/editStudentEnroll', 'StudentController@editStudentEnroll');
		Route::post('/students-list', 'StudentController@students_list')->name('fetch-students-lists');
		Route::post('/check-student-id', 'StudentController@checkStuId');
		Route::resource('/students', 'StudentController');

		//Route for Education Plans
		Route::post('/educationplan-list', 'EducationPlanController@educationplan_list')->name('fetch-educationplan-lists');
		Route::resource('/educationplan', 'EducationPlanController');
	});
});