<?php

Route::prefix('employee')->namespace('Employee')->group(function () {

    Route::group(['middleware' => 'CheckSCAndStaff'], function () {
        Route::get('/profile', 'EmployeeController@profile');
        Route::post('/edit-profile', 'EmployeeController@editProfile');
        Route::post('/fetchCollege', 'EmployeeController@fetchCollege');
        Route::post('/editEducationDetails', 'EmployeeController@editEducationDetails');
        Route::post('/editEngagementDetails', 'EmployeeController@editEngagementDetails');
        Route::post('/switch-role', 'EmployeeController@switchRole');
        Route::resource('/dashboard', 'EmployeeController');
    });

    //Routes For Employee (SC)
    Route::group(['middleware' => 'CheckSC'], function () {

        //School coordinator my School schoolcoordinator
        Route::resource('/myschool', 'SchoolCoordinatorController');

        //Routes For Village School schoolcoordinator
        Route::post('/villageschool-list', 'VillageSchoolController@villageschool_list')->name('fetch-villageschool-lists');
        Route::resource('/villageschools', 'VillageSchoolController');

        //Route for Main Book schoolcoordinator
        Route::post('/mainbooks-list', 'MainBookController@mainbooks_list')->name('fetch-mainbooks-lists');
        Route::post('/mainbooks-students-list', 'MainBookController@mainbooks_student_list')->name('fetch-mainbooks-students-lists');
        Route::resource('/mainbooks', 'MainBookController');

        //Route for School Sub Admin schoolcoordinator
        Route::post('/subadmins-list', 'SubAdminController@subadmins_list')->name('fetch-subadmins-lists');
        Route::resource('/subadmins', 'SubAdminController');

        //Route for Employees schoolcoordinator
        Route::post('/staff-list', 'TeacherController@employees_list')->name('fetch-staff-lists');
        Route::post('/engage-employees-list', 'TeacherController@engage_employees_list')->name('fetch-engage-employees-lists');
        Route::post('/engage-employee-store', 'TeacherController@engageEmployeePost');
        Route::get('/engage-employee', 'TeacherController@engageEmployee')->name('engageemployee');
        Route::resource('/employees', 'TeacherController');

        //Routes for Student Add And Edit schoolcoordinator
        Route::post('/fetch-student-educationplan', 'StudentController@enrollStudents')->name('fetch-student-educationplan');
        Route::post('/student-list', 'StudentController@student_list')->name('fetch-student-lists');
        Route::post('/check-student-id', 'StudentController@checkStuId');
        Route::post('/studentenroll-update', 'StudentController@editStudentEnroll');
        Route::resource('/students', 'StudentController');

        //Routes for  enroll student schoolcoordinator
        Route::get('/view/educationplan/{id}', 'EnrollStudentController@viewEducationPlan')->name('view-educationplan');
        Route::post('/enrollstudents-list', 'EnrollStudentController@enrollstudents_list')->name('fetch-enroll-students-lists');
        Route::post('/fetcheducationplan-students', 'EnrollStudentController@fetchEducationPlan')->name('fetch-educationplan-student');
        Route::post('/fetch-grades', 'EnrollStudentController@fetchGrades');
        Route::resource('/enrollstudents', 'EnrollStudentController');

        //Clone Semester
        Route::resource('/clonesemester', 'CloneSemesterController');

        //School coordinator class creation schoolcoordinator
        Route::get('/view-student-detail/{id}', 'ClassCreationController@viewStudentDetail')->name('view-studentdetail');
        Route::get('/class-creation-semester/{id}', 'ClassCreationController@classCreationSemesters')->name('class-creation-semester-detail');
        Route::post('/classcreationsem-lists', 'ClassCreationController@classcreationsem_lists')->name('fetch-classcreationsem-lists');
        Route::post('/classcreation-list', 'ClassCreationController@classcreation_list')->name('fetch-classcreation-lists');
        Route::post('/fetch-class-students-list', 'ClassCreationController@class_student_list');
        Route::post('/fetch-previousgrade-students', 'ClassCreationController@fetchPreviousGradeStudent');
        Route::post('/fetchClassEducationPlan', 'ClassCreationController@fetchClassEducationPlan');
        // Route::post('/course-class-selection', 'ClassCreationController@classCourseSelection');
        Route::post('/delete-student-course', 'ClassCreationController@deleteStudentCourse');
        Route::resource('/classcreation', 'ClassCreationController');

        //School coordinator class creation sub classes
        Route::get('/view-homeroomteacher/{id}', 'ClassCreationSubClassController@viewHomeRoomTeacher');
        Route::get('/classcreation/subclass/{id}', 'ClassCreationSubClassController@classCreationSubClass');
        Route::get('/classcreation/view-subclass/{id}', 'ClassCreationSubClassController@viewSubClass');
        Route::get('/classcreation/view-subclass-students/{id}', 'ClassCreationSubClassController@viewSubclassStudents');
        Route::post('/update-homeroomteacher', 'ClassCreationSubClassController@updateHomeroomTeacher');
        Route::post('/subclass-list', 'ClassCreationSubClassController@subclass_list');
        Route::post('/delete-subclass-course', 'ClassCreationSubClassController@deleteSubClassCourse');
        Route::post('/fetch-subclass-students-list', 'ClassCreationSubClassController@subclass_students_list');
        Route::post('/subclass/create', 'ClassCreationSubClassController@createSubClass');
        Route::post('/schoolyear-students-list', 'ClassCreationSubClassController@school_year_students_list');

        //School coordinator Final Marks
        Route::post('/generate-certificate', 'FinalMarkController@generateCertificate');
        Route::resource('/finalmarks', 'FinalMarkController');

        //Route for Course Group schoolcoordinator
        Route::post('/coursegroup-list', 'CourseGroupController@coursegroup_list')->name('fetch-coursegroup-lists');
        Route::post('/fetch-coursegroup', 'CourseGroupController@fetchCoursesGroup');
        Route::post('/coursegroup-students-list', 'CourseGroupController@coursegroup_students_list');
        Route::post('/fetch-educationplan-hours', 'CourseGroupController@fetchEducationPlanHours');
        Route::resource('/coursegroup', 'CourseGroupController');

        //Route for Course Order schoolcoordinator
        Route::post('/courseorders-list', 'CourseOrderController@courseorders_list')->name('fetch-courseorders-lists');
        Route::post('/educationplancourses-list', 'CourseOrderController@fetchCourses')->name('fetch-educationplancourses-lists');
        Route::resource('/courseorders', 'CourseOrderController');
    });

    //Routes For Staff (Teacher, Homeroomteacher, Principal)
    Route::group(['middleware' => 'CheckStaff'], function () {
        Route::post('/previous-unit-history-list', 'AttendanceAccomplishmentController@previous_unit_history_list')->name('fetch-previous-unit-history');
        Route::resource('/attendance-accomplishment', 'AttendanceAccomplishmentController');

        Route::post('/attendance-lists', 'AttendanceAccomplishmentSubController@teacher_attendance_listing')->name('fetch-attendance-lists');
        Route::post('/classstudents-list', 'AttendanceAccomplishmentSubController@classstudents_list')->name('fetch-classstudents-lists');
        Route::post('/classattendance-list', 'AttendanceAccomplishmentSubController@lecture_detail_listing')->name('fetch-class-attendance-lists');
        // Route::post('/student-behaviour-lists', 'AttendanceAccomplishmentSubController@student_behaviour_lists')->name('fetch-student-behaviour-lists');
        Route::post('/fetch-course-hour','AttendanceAccomplishmentSubController@fetch_course_hours')->name('fetch-course-hours');
        Route::resource('/student-behaviour-notes', 'AttendanceStudentBehaviourController');

        Route::post('/attendance-verifying-list', 'AttendanceVerifyingController@attendance_listing')->name('attendance-verifying.list');
        Route::post('/attendance-class-detail', 'AttendanceVerifyingController@attendance_class_detail');
        Route::post('/delete-attendance-verifying-lecture', 'AttendanceVerifyingController@delete_attendance_lecture');
        Route::resource('/attendance-verifying','AttendanceVerifyingController');

        Route::post('/periodic-exam-list', 'PeriodicExamsController@exam_listing')->name('periodic-exam.list');
        Route::post('/exam-student-list', 'PeriodicExamsController@exam_student_listing')->name('periodic-exam-student.lists');
        Route::resource('/periodic-exam','PeriodicExamsController');

        Route::post('/class-working-weeks-list','ClassWorkingWeeksController@class_working_weeks_list')->name('class-working-weeks.list');
        Route::resource('/class-working-weeks','ClassWorkingWeeksController');
    });
});
