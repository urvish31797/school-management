<?php

Route::prefix('student')->namespace('Student')->group(function () {
	Route::group(['middleware'=>'CheckStudent'],function(){
		Route::get('/profile', 'StudentController@profile');
		Route::get('/dashboard', 'StudentController@dashboard');
		Route::post('/edit-profile', 'StudentController@editProfile');
	});
});