@extends('layout.app_with_login')
@section('title', trans('sidebar.sidebar_nav_courses'))
@section('content')
<!-- Page Content  -->
<div class="section">
    <div class="container-fluid">
        <div class="row ">
            <div class="col-12 mb-3">
                <h2 class="title"><span>{{trans('sidebar.sidebar_nav_masters')}} > </span>{{trans('sidebar.sidebar_nav_courses')}}</h2>
            </div>
            <div class="col-md-4 mb-3">
                <input type="text" id="search_course" maxlength="30" class="form-control icon_control search_control" placeholder="{{trans('general.gn_search')}}">
            </div>
            <div class="col-md-4 text-md-right mb-3">

            </div>
            <div class="col-md-2 col-6 mb-3">

            </div>
            <div class="col-md-2 col-6 mb-3">
                <a><button class="theme_btn show_modal full_width small_btn" data-toggle="modal" data-target="#add_new">{{trans('general.gn_add_new')}}</button></a>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="theme_table">
                    <div class="table-responsive">
                        {{$dt_html->table(['class'=>"courses_listing display responsive","width"=>"100%"],true)}}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Add New Popup -->
<div class="theme_modal modal fade" id="add_new" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <img src="{{asset('images/ic_close_bg.png')}}" class="modal_top_bg">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <img src="{{asset('images/ic_close_circle_white.png')}}">
                </button>
                <form name="add-course-form" id="add-course-form">
                    <div class="row">
                        <div class="col-lg-1"></div>
                        <div class="col-lg-10">
                            <h5 class="modal-title" id="exampleModalCenterTitle">{{trans('sidebar.sidebar_nav_courses')}}</h5>
                            <input type="hidden" id="pkCrs">
                            @foreach($languages as $k => $v)
                            <div class="form-group">
                                <label>{{trans('general.gn_name')}} {{$v->language_name}} *</label>
                                <input type="text" name="crs_CourseName_{{$v->language_key}}" id="crs_CourseName_{{$v->language_key}}" class="form-control force_require icon_control" required="">
                            </div>
                            @endforeach
                            <div class="form-group">
                                <label>{{trans('general.gn_alternative_name')}}</label>
                                <input type="text" name="crs_CourseAlternativeName" id="crs_CourseAlternativeName" class="form-control icon_control">
                            </div>
                            <div class="form-group">
                                <label>{{trans('general.gn_uid')}} *</label>
                                <input type="text" name="crs_Uid" id="crs_Uid" class="form-control icon_control">
                            </div>
                            <div class="form-group">
                                <label>{{trans('general.gn_type')}} *</label>
                                <select name="crs_CourseType" id="crs_CourseType" class="form-control icon_control dropdown_control">
                                    <option value="">{{trans('general.gn_select')}}</option>
                                    <option value="General">{{trans('general.gn_general')}}</option>
                                    <option value="Specialization">{{trans('general.gn_specialization')}}</option>
                                    <option value="DummyOptionalCourse">{{trans('general.gn_dummy_optional_course')}}</option>
                                    <option value="DummyForeignCourse">{{trans('general.gn_dummy_foreign_course')}}</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label>{{trans('general.gn_is_foreign_language')}}: *</label>
                                <div class="form-check custom_check_div">
                                    <input class="form-check-input" type="radio" name="crs_IsForeignLanguage" value="Yes">
                                    <label class="custom_radio"></label>
                                    <label class="form-check-label" for="Customer">{{trans('general.gn_yes')}}</label>
                                </div>
                                <div class="form-check custom_check_div">
                                    <input class="form-check-input" type="radio" name="crs_IsForeignLanguage" checked="checked" value="No">
                                    <label class="custom_radio"></label>
                                    <label class="form-check-label" for="Customer">{{trans('general.gn_no')}}</label>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>{{trans('general.gn_note')}}</label>
                                <input type="text" name="crs_Notes" id="crs_Notes" class="form-control icon_control">
                            </div>
                            <div class="text-center modal_btn ">
                                <button type="button" onclick="save()" class="theme_btn">{{trans('general.gn_submit')}}</button>
                            </div>
                        </div>
                        <div class="col-lg-1"></div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="theme_modal modal fade" id="delete_prompt" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <img src="{{asset('images/ic_close_bg.png')}}" class="modal_top_bg">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <img src="{{asset('images/ic_close_circle_white.png')}}">
                </button>
                <div class="row">
                    <div class="col-lg-1"></div>
                    <div class="col-lg-10">
                        <h5 class="modal-title" id="exampleModalCenterTitle">{{trans('general.gn_delete')}}</h5>
                        <div class="form-group text-center">
                            <label>{{trans('general.gn_delete_prompt')}}</label>
                            <input type="hidden" id="did">
                        </div>
                        <div class="text-center modal_btn ">
                            <button style="display: none;" class="theme_btn show_delete_modal full_width small_btn" data-toggle="modal" data-target="#delete_prompt">{{trans('general.gn_delete')}}</button>
                            <button type="button" onclick="confirmDelete()" class="theme_btn">{{trans('general.gn_yes')}}</button>
                            <button type="button" data-dismiss="modal" class="theme_btn red_btn">{{trans('general.gn_no')}}</button>
                        </div>
                    </div>
                    <div class="col-lg-1"></div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('datatable-scripts')
<!-- Include datatable Page JS -->
<script type="text/javascript" id="datatable_script" src="{{ asset('js/dashboard/course.js') }}"></script>
<script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
{!! $dt_html->scripts() !!}
{!! JsValidator::formRequest(\App\Http\Requests\CourseRequest::class, '#add-course-form'); !!}
@endpush
