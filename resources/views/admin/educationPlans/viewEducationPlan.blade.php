@extends('layout.app_with_login')
@section('title', trans('sidebar.sidebar_nav_education_plan'))
@section('content')
<?php
$i = 1;
$grades = [];
$gradesTmp = [];
foreach($mdata->mandatoryCourse as $k => $v){
    if(!empty($v->grade->gra_GradeNumeric)){
        $gradesTmp[$v->fkEmcGra] = $v->grade->gra_GradeNumeric;
    }
}
$grades = array_unique($gradesTmp);

?>
 <!-- Page Content  -->
<div class="section">
	<div class="container-fluid">
		<div class="row ">
            <div class="col-12 mb-3">
                <h2 class="title">
                <span>
                @if(Auth::guard('admin')->user()->type=='HertronicAdmin')
                {{trans('sidebar.sidebar_nav_ministry_masters')}} >
                @endif
                </span>
                <a class="no_sidebar_active" href="{{url('/admin/educationplan')}}">
                <span>{{trans('sidebar.sidebar_nav_education_plan')}} > </span>
                </a>
                {{trans('general.gn_view_courses')}}
                </h2>
            </div>
            <div class="col-12">
                <div class="white_box pt-3 pb-3">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-lg-12 ">
                                <div class="pl-5 pr-5">
                                    <div class="row">
                                        @foreach($languages as $k => $v)
                                        <div class="col-md-6">
                                            <p>{{trans('sidebar.sidebar_nav_education_plan')}} ({{$v->language_name}}) : <label> {{$mdata['epl_EducationPlanName_'.$v->language_key]}}</label></p>
                                        </div>
                                        @endforeach


                                        <div class="col-md-6 text-left">
                                            <p>{{trans('sidebar.sidebar_nav_education_plan')}} {{trans('general.gn_for')}} : <label> {{$mdata->educationProgram->edp_Name}}</label></p>
                                        </div>

                                        <div class="col-md-6">
                                            <p>{{trans('sidebar.sidebar_nav_national_education_plan')}} : <label> {{$mdata->nationalEducationPlan->nep_NationalEducationPlanName}}</label></p>
                                        </div>

                                        <div class="col-md-6 text-left">
                                            <p>{{trans('sidebar.sidebar_nav_qualification_degree')}} : <label> {{$mdata->QualificationDegree->qde_QualificationDegreeName ?? ''}}</label></p>
                                        </div>

                                        <div class="col-md-6">
                                            <p>{{trans('sidebar.sidebar_nav_education_profile')}} : <label> {{$mdata->educationProfile->epr_EducationProfileName ?? ''}}</label></p>
                                        </div>

                                        <div class="col-md-6 text-left">
                                            <p>{{trans('sidebar.sidebar_certificate_blueprints')}} : <label> {{$mdata->CertificateBluePrints->cbp_BluePrintName ?? ''}}</label></p>
                                        </div>

                                        <div class="col-md-6">
                                            <p>{{trans('general.gn_canton')}} : <label> {{$mdata->canton->can_CantonName ?? ''}}, {{$mdata->canton->state->sta_StateName ?? ''}}</label></p>
                                        </div>

                                    @foreach($grades as $kg => $vg)
                                    <div class="profile_info_container ep_gra_{{$kg}}">
                                        <div class="row">
                                            <div class="col-4"></div>
                                            <div class="col-4" style="text-align: center;"><strong class="gra_name">{{trans('general.gn_grade')}} - {{$vg}}</strong></div>
                                            <div class="col-4 text-right">

                                            </div>
                                            <div class="col-md-12">
                                                <p class="mt-2"><strong class="title_label">{{trans('general.gn_mandatory_courses')}}</strong></p>
                                                <div class="table-responsive mt-2">
                                                    <table class="color_table mcg_table mcg_table_{{$kg}}">
                                                        <tbody>
                                                            <tr>
                                                                <th width="15%">{{trans('general.sr_no')}}</th>
                                                                <th width="45%">{{trans('general.gn_courses')}}</th>
                                                                <th width="5%">{{trans('general.gn_week_hours')}}</th>
                                                                <th width="15%">{{trans('general.gn_teacher_week_hour_rate')}}</th>
                                                                <th width="15%">{{trans('general.gn_summer_holiday_hour')}}</th>
                                                                <th width="10%">{{trans('general.gn_default')}}</th>
                                                            </tr>
                                                            <?php $m = 1;?>
                                                            @foreach($mdata->mandatoryCourse as $k => $v)
                                                            @if($kg == $v->grade->pkGra)
                                                            <tr class="mcg mcg_{{$v->mandatoryCourseGroup->pkCrs}} mcg_gra_{{$kg}}">
                                                                <td>{{$m}}</td>
                                                                <td>{{$v->mandatoryCourseGroup->crs_CourseName}}</td>
                                                                <td>{{$v->emc_hours}}</td>
                                                                <td>{{$v->emc_weekHourRateTeacher}}</td>
                                                                <td>{{$v->emc_summerHolidayHourRate}}</td>
                                                                <td><div class="form-group form-check"><input disabled type="checkbox" class="form-check-input" name="default_mcg[]" @if($v->emc_default == 'Yes') checked @endif id="default_mcg_{{$v->mandatoryCourseGroup->pkCrs}}" value="{{$v->mandatoryCourseGroup->pkCrs}}_{{$kg}}"><label class="custom_checkbox"></label></div></td>
                                                            </tr>
                                                            <?php $m++; ?>
                                                            @endif
                                                            @endforeach
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <p class="mt-2"><strong class="title_label">{{trans('general.gn_optional_courses')}}</strong></p>
                                                <div class="table-responsive mt-2">
                                                    <table class="color_table ocg_table ocg_table_{{$kg}}">
                                                        <tbody>
                                                            <tr>
                                                                <th width="15%">{{trans('general.sr_no')}}</th>
                                                                <th width="45%">{{trans('general.gn_courses')}}</th>
                                                                <th width="20%">{{trans('general.gn_course_order')}}</th>
                                                                <th width="15%">{{trans('general.gn_week_hours')}}</th>
                                                                <th width="25%">{{trans('general.gn_default')}}</th>
                                                            </tr>
                                                        <?php $o = 1;?>
                                                        @foreach($mdata->optionalCourse as $k => $v)
                                                        @if($kg == $v->grade->pkGra)
                                                        <tr class="ocg ocg_{{$v->optionalCoursesGroup->pkCrs}} ocg_gra_{{$kg}} ocg_ord_{{$v->eoc_order}}">
                                                            <td>{{$o}}</td>
                                                            <td>{{$v->optionalCoursesGroup->crs_CourseName}}</td>
                                                            <td>{{$v->eoc_order}}</td>
                                                            <td>{{$v->eoc_hours}}</td>
                                                            <td><div class="form-group form-check"><input disabled type="checkbox" @if($v->eoc_default == 'Yes') checked @endif class="form-check-input" name="default_ocg[]" id="default_ocg_{{$v->optionalCoursesGroup->pkCrs}}" value="{{$v->optionalCoursesGroup->pkCrs}}_{{$kg}}"><label class="custom_checkbox"></label></div></td>
                                                        </tr>
                                                        <?php $o++; ?>
                                                        @endif
                                                        @endforeach
                                                    </tbody></table>
                                                </div>
                                            </div>

                                            <div class="col-md-12">
                                                <p class="mt-2"><strong class="title_label">{{trans('general.gn_mandatory_foreign_language_courses')}}</strong></p>
                                                <div class="table-responsive mt-2">
                                                    <table class="color_table fcg_table fcg_table_{{$kg}}">
                                                        <tbody>
                                                            <tr>
                                                                <th width="15%">{{trans('general.sr_no')}}</th>
                                                                <th width="45%">{{trans('general.gn_courses')}}</th>
                                                                <th width="20%">{{trans('general.gn_course_order')}}</th>
                                                                <th width="15%">{{trans('general.gn_week_hours')}}</th>
                                                                <th width="25%">{{trans('general.gn_default')}}</th>
                                                            </tr>
                                                        <?php $f = 1;?>
                                                        @foreach($mdata->foreignLanguageCourse as $k => $v)
                                                        @if($kg == $v->grade->pkGra)
                                                        <tr class="fcg fcg_{{$v->foreignLanguageGroup->pkCrs}} fcg_gra_{{$kg}} fcg_ord_{{$v->efc_order}}">
                                                            <td>{{$f}}</td>
                                                            <td>{{$v->foreignLanguageGroup->crs_CourseName}}</td>
                                                            <td>{{$v->efc_order}}</td>
                                                            <td>{{$v->efc_hours}}</td>
                                                            <td><div class="form-group form-check"><input disabled @if($v->efc_default == 'Yes') checked @endif type="checkbox" class="form-check-input" name="default_fcg[]" id="default_fcg_{{$v->foreignLanguageGroup->pkCrs}}" value="{{$v->foreignLanguageGroup->pkCrs}}_{{$kg}}"><label class="custom_checkbox"></label></div></td>
                                                        </tr>
                                                        <?php $f++; ?>
                                                        @endif
                                                        @endforeach
                                                    </tbody></table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <?php $i++;?>
                                    @endforeach

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End Content Body -->
@endsection