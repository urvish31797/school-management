@extends('layout.app_with_login')
@section('title', trans('sidebar.sidebar_nav_education_plan'))
@section('script', asset('js/dashboard/education_plan.js'))
@section('content')
<?php $mcdata = []; $ocdata = []; $fcdata = [];
   $i = 1;
   $grades = [];
   $gradesTmp = [];
   foreach($mdata->mandatoryCourse as $k => $v){
       $gradesTmp[$v->fkEmcGra] = $v->grade->gra_GradeNumeric ?? "";
   }

   $grades = array_unique($gradesTmp);

   ?>
<!-- Page Content  -->
<div class="section">
   <div class="container-fluid">
      <div class="row ">
         <div class="col-12 mb-3">
            <h2 class="title"><span>@if(Auth::guard('admin')->user()->type=='HertronicAdmin') {{trans('sidebar.sidebar_nav_ministry_masters')}} > @endif</span> <a class="no_sidebar_active" href="{{url('/admin/educationplan')}}"><span>{{trans('sidebar.sidebar_nav_education_plan')}} > </span></a> {{trans('general.gn_edit')}}</h2>
         </div>
         <div class="col-12">
            <div class="white_box pt-3 pb-3">
               <div class="container-fluid">
                  <form name="add-education-plan-form" id="edit-education-plan">
                     <div class="row">
                        <div class="col-lg-12">
                           <div class="pl-5 pr-5">
                              <div>
                                 <p class="mt-2"><strong>{{trans('sidebar.sidebar_nav_education_plan')}}</strong></p>
                              </div>
                              <div class="bg-color-form">
                                 <div class="row">
                                    <input type="hidden" id="eid" value="{{$mdata->pkEpl}}">
                                    @foreach($languages as $k => $v)
                                    <div class="col-lg-6">
                                       <div class="form-group">
                                          <label>{{trans('general.gn_name')}} {{$v->language_name}} *</label>
                                          <input type="text" name="epl_EducationPlanName_{{$v->language_key}}" id="epl_EducationPlanName_{{$v->language_key}}" class="form-control force_require icon_control" value="{{$mdata['epl_EducationPlanName_'.$v->language_key]}}" required="">
                                       </div>
                                    </div>
                                    @endforeach
                                    <div class="col-lg-6">
                                       <div class="form-group">
                                          <label>{{trans('sidebar.sidebar_nav_education_program')}} *</label>
                                          <select name="fkEplEdp" id="fkEplEdp" class="form-control icon_control dropdown_control">
                                             <option value="">{{trans('general.gn_select')}}</option>
                                             @foreach($educationProgram as $tkey => $tvalue)
                                             <option  @if($mdata->fkEplEdp == $tvalue['pkEdp']) selected @endif value="{{$tvalue['pkEdp']}}">{{$tvalue['edp_Name']}}</option>
                                             {{-- Sub Parent --}}
                                             @foreach($tvalue['children'] as $ckey=> $cValue)
                                             @if($cValue['edp_ParentId']==$tvalue['pkEdp'])
                                             <option @if($mdata->fkEplEdp == $cValue['pkEdp']) selected @endif value="{{$cValue['pkEdp']}}">&emsp;&emsp;{{$cValue['edp_Name']}}</option>
                                             {{-- Sub Sub Parent --}}
                                             @foreach($cValue['children'] as $cckey=> $ccValue)
                                             @if($ccValue['edp_ParentId']==$cValue['pkEdp'])
                                             <option @if($mdata->fkEplEdp == $ccValue['pkEdp']) selected @endif value="{{$ccValue['pkEdp']}}">&emsp;&emsp;&emsp;&emsp;{{$ccValue['edp_Name']}}</option>
                                             {{-- Sub Sub Sub Parent --}}
                                             @foreach($ccValue['children'] as $skey=> $sValue)
                                             @if($sValue['edp_ParentId']==$ccValue['pkEdp'])
                                             <option @if($mdata->fkEplEdp == $sValue['pkEdp']) selected @endif value="{{$sValue['pkEdp']}}">&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;{{$sValue['edp_Name']}}</option>
                                             {{-- Sub Sub Sub Sub Parent --}}
                                             @foreach($sValue['children'] as $sskey=> $ssValue)
                                             @if($ssValue['edp_ParentId']==$ssValue['pkEdp'])
                                             <option @if($mdata->fkEplEdp == $sValue['pkEdp']) selected @endif value="{{$ssValue['pkEdp']}}">&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;{{$ssValue['edp_Name']}}</option>
                                             @endif
                                             @endforeach
                                             @endif
                                             @endforeach
                                             @endif
                                             @endforeach
                                             @endif
                                             @endforeach
                                             @endforeach
                                          </select>
                                       </div>
                                    </div>
                                    <div class="col-lg-6">
                                       <div class="form-group">
                                          <label>{{trans('sidebar.sidebar_nav_national_education_plan')}} *</label>
                                          <select name="fkEplNep" id="fkEplNep" class="form-control icon_control dropdown_control">
                                             <option value="">{{trans('general.gn_select')}}</option>
                                             @foreach($nationalEducationPlan as $k => $v)
                                             <option @if($mdata->fkEplNep == $v->pkNep) selected @endif value="{{$v->pkNep}}">{{$v->nep_NationalEducationPlanName}}</option>
                                             @endforeach
                                          </select>
                                       </div>
                                    </div>
                                    <div class="col-lg-6">
                                       <div class="form-group">
                                          <label>{{trans('general.gn_canton')}} *</label>
                                          <select name="fkEplCan" id="fkEplCan" class="form-control icon_control dropdown_control select2_drop">
                                             <option value="">{{trans('general.gn_select')}}</option>
                                             @if(!empty($cantons))
                                                @foreach($cantons as $k => $v)
                                                   <option @if($mdata->fkEplCan == $v->pkCan) selected @endif value="{{$v->pkCan}}">{{$v->can_CantonName}}, {{$v->state->sta_StateName}}</option>
                                                @endforeach
                                             @endif
                                          </select>
                                       </div>
                                    </div>
                                    <div class="col-lg-6">
                                       <div class="form-group">
                                          <label>{{trans('sidebar.sidebar_certificate_blueprints')}} *</label>
                                          <select name="fkCbp" id="fkCbp" class="form-control icon_control dropdown_control">
                                             <option value="">{{trans('general.gn_select')}}</option>
                                             @if(!empty($certificateBluePrints))
                                             @foreach($certificateBluePrints as $k => $v)
                                             <option @if($v->pkCbp == $mdata->fkCbp) selected="selected" @endif value="{{$v->pkCbp}}">{{$v->cbp_BluePrintName}}</option>
                                             @endforeach
                                             @endif
                                          </select>
                                       </div>
                                    </div>
                                    <div class="col-lg-6">
                                       <div class="form-group">
                                          <label>{{trans('sidebar.sidebar_nav_education_profile')}} </label>
                                          <select name="fkEplEpr" id="fkEplEpr" class="form-control icon_control dropdown_control">
                                             <option value="">{{trans('general.gn_select')}}</option>
                                             @foreach($educationProfile as $k => $v)
                                             <option @if($mdata->fkEplEpr == $v->pkEpr) selected @endif value="{{$v->pkEpr}}">{{$v->epr_EducationProfileName}}</option>
                                             @endforeach
                                          </select>
                                       </div>
                                    </div>
                                    <div class="col-lg-6">
                                       <div class="form-group">
                                          <label>{{trans('sidebar.sidebar_nav_qualification_degree')}} </label>
                                          <select name="fkEplQde" id="fkEplQde" class="form-control icon_control dropdown_control">
                                             <option value="">{{trans('general.gn_select')}}</option>
                                             @foreach($qualificationDegree as $k => $v)
                                             <option @if($mdata->fkEplQde == $v->pkQde) selected @endif value="{{$v->pkQde}}">{{$v->qde_QualificationDegreeName}}</option>
                                             @endforeach
                                          </select>
                                       </div>
                                    </div>
                                    <div class="col-lg-6">
                                       <div class="form-group">
                                          <label>{{trans('general.gn_grades')}} </label>
                                          <select name="gra_sel" id="gra_sel" class="form-control icon_control dropdown_control">
                                             <option value="">{{trans('general.gn_select')}}</option>
                                             @foreach($grade as $k => $v)
                                             <option value="{{$v->pkGra}}">{{$v->gra_GradeNumeric}} ({{$v->gra_GradeNameRoman}})</option>
                                             @endforeach
                                          </select>
                                       </div>
                                    </div>
                                    <div class="col-lg-6">
                                       <label style="visibility: hidden;display: block;">{{trans('general.gn_grades')}} </label>
                                       <div class="form-group">
                                          <button type="button" class="theme_btn" id="add_ep_gra">{{trans('general.gn_add')}}</button>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              @foreach($grades as $kg => $vg)
                              <div class="profile_info_container ep_gra_{{$kg}}">
                                 <div class="row">
                                    <div class="col-4"></div>
                                    <div class="col-4" style="text-align: center;"><strong class="gra_name">{{trans('general.gn_grade')}} - {{$vg}}</strong></div>
                                    <div class="col-4 text-right">
                                       <img src="{{asset('images/ic_close_circle.png')}}" class="rm_eng close_img" data-eed="{{$i}}">
                                    </div>
                                    <div class="col-md-12">
                                       <p class="mt-2"><strong class="title_label">{{trans('general.gn_mandatory_courses')}}</strong></p>
                                       <div class="table-responsive mt-2">
                                          <table class="color_table mcg_table mcg_table_{{$kg}}">
                                             <tbody>
                                                <tr>
                                                   <th width="15%">{{trans('general.sr_no')}}</th>
                                                   <th width="45%">{{trans('general.gn_courses')}}</th>
                                                   <th width="5%">{{trans('general.gn_week_hours')}}</th>
                                                   <th width="5%">{{trans('general.gn_teacher_week_hour_rate')}}</th>
                                                   <th width="5%">{{trans('general.gn_summer_holiday_hour')}}</th>
                                                   <th width="15%">{{trans('general.gn_action')}}</th>
                                                   <th width="25%">{{trans('general.gn_default')}}</th>
                                                </tr>
                                                <?php $m = 1;?>
                                                @foreach($mdata->mandatoryCourse as $k => $v)
                                                @if($kg == $v->grade->pkGra)
                                                <tr class="mcg_item mcg mcg_{{$v->mandatoryCourseGroup->pkCrs}} mcg_gra_{{$kg}}">
                                                   <td>{{$m}}</td>
                                                   <td>{{$v->mandatoryCourseGroup->crs_CourseName}}</td>
                                                   @if($v->mandatoryCourseGroup->crs_CourseAlternativeName == "Stručna praksa")
                                                   <td><input onkeyup="cleanHrs(this)" class="ids_only form-control form-control-border input_disable" readonly name="mcg_hrs[]" type="text" id="mcg_hrs_{{$v->mandatoryCourseGroup->pkCrs}}_{{$kg}}" value="{{$v->emc_hours}}" aria-required="true" class="valid" maxlength="3"></td>
                                                   <td><input onkeyup="cleanHrs(this)" class="ids_only form-control form-control-border input_disable" readonly type="text" name="mcg_weekHourRateTeacher[]" id="mcg_weekHourRateTeacher_{{$v->mandatoryCourseGroup->pkCrs}}_{{$kg}}" maxlength="3" value="{{$v->emc_weekHourRateTeacher}}"></td>
                                                   <td><input onkeyup="cleanHrs(this)" required="" class="ids_only form-control form-control-border" type="text" name="mcg_summerHolidayHourRate[]" id="mcg_summerHolidayHourRate_{{$v->mandatoryCourseGroup->pkCrs}}_{{$kg}}" maxlength="3" value="{{$v->emc_summerHolidayHourRate}}"></td>
                                                   @else
                                                   <td><input onkeyup="cleanHrs(this)" required="" class="ids_only form-control form-control-border" name="mcg_hrs[]" type="text" id="mcg_hrs_{{$v->mandatoryCourseGroup->pkCrs}}_{{$kg}}" value="{{$v->emc_hours}}" aria-required="true" class="valid" maxlength="3"></td>
                                                   <td><input onkeyup="cleanHrs(this)" class="ids_only form-control form-control-border" type="text" name="mcg_weekHourRateTeacher[]" id="mcg_weekHourRateTeacher_{{$v->mandatoryCourseGroup->pkCrs}}_{{$kg}}" maxlength="3" value="{{$v->emc_weekHourRateTeacher}}"></td>
                                                   <td><input onkeyup="cleanHrs(this)" class="ids_only form-control form-control-border input_disable" readonly type="text" name="mcg_summerHolidayHourRate[]" id="mcg_summerHolidayHourRate_{{$v->mandatoryCourseGroup->pkCrs}}_{{$kg}}" maxlength="3" value="{{$v->emc_summerHolidayHourRate}}"></td>
                                                   @endif
                                                   <td><a sel-id="{{$i}}" data-id="{{$v->mandatoryCourseGroup->pkCrs}}" data-gra="{{$kg}}" onclick="removeMCG(this)" href="javascript:void(0)" class="theme_btn red_btn min_btn">{{trans('general.gn_delete')}}</a></td>
                                                   <td>
                                                      <input type="hidden" class="hidden_inputs" name="mcg_gra[]" value="{{$kg}}">
                                                      <input class="hidden_inputs" type="hidden" name="MCG[]" value="{{$v->mandatoryCourseGroup->pkCrs}}">
                                                      <div class="form-group form-check">
                                                         <input type="checkbox" class="form-check-input" name="default_mcg[]" @if($v->emc_default == 'Yes') checked @endif id="default_mcg_{{$v->mandatoryCourseGroup->pkCrs}}" value="{{$v->mandatoryCourseGroup->pkCrs}}_{{$kg}}">
                                                         <label class="custom_checkbox"></label>
                                                      </div>
                                                   </td>
                                                </tr>
                                                <?php $m++; ?>
                                                @endif
                                                @endforeach
                                                <tr class="mcg mcg_main">
                                                   <td>{{$m}}</td>
                                                   <td>
                                                      <select onchange="selectCourse(this)" class="form-control icon_control dropdown_control select2drp ids_only" id="mcg_select_{{$i}}" name="_{{$i}}" tabindex="-1" aria-hidden="true">
                                                         <option value="">{{trans('general.gn_select')}}</option>
                                                         @foreach($course as $k => $v)
                                                         <option value="{{$v->pkCrs}}" data-alternativename="{{$v->crs_CourseAlternativeName}}">{{$v->crs_CourseName}}</option>
                                                         @endforeach
                                                      </select>
                                                   </td>
                                                   <td><input onkeyup="cleanHrs(this)" type="text" class="hours_input form-control form-control-border" id="mcg_hrs" maxlength="3"></td>
                                                   <td><input onkeyup="cleanHrs(this)" type="text" class="hours_input form-control form-control-border" id="mcg_weekHourRateTeacher" maxlength="3"></td>
                                                   <td><input onkeyup="cleanHrs(this)" type="text" class="hours_input form-control form-control-border input_disable" readonly id="mcg_summerHolidayHourRate" maxlength="3"></td>
                                                   <td>
                                                      <a onclick="addMCG(this)" href="javascript:void(0)" class="addElemBtn theme_btn min_btn" data-elem-id="{{$i}}" data-elem-gra="{{$kg}}">Add</a>
                                                   </td>
                                                   <td></td>
                                                </tr>
                                             </tbody>
                                          </table>
                                       </div>
                                    </div>
                                    <div class="col-md-12">
                                       <p class="mt-2"><strong class="title_label">{{trans('general.gn_optional_courses')}}</strong></p>
                                       <div class="table-responsive mt-2">
                                          <table class="color_table ocg_table ocg_table_{{$kg}}">
                                             <tbody>
                                                <tr>
                                                   <th width="15%">{{trans('general.sr_no')}}</th>
                                                   <th width="45%">{{trans('general.gn_courses')}}</th>
                                                   <th width="20%">{{trans('general.gn_course_order')}}</th>
                                                   <th width="15%">{{trans('general.gn_week_hours')}}</th>
                                                   <th width="15%">{{trans('general.gn_action')}}</th>
                                                   <th width="25%">{{trans('general.gn_default')}}</th>
                                                </tr>
                                                <?php $o = 1;?>
                                                @foreach($mdata->optionalCourse as $k => $v)
                                                @if($kg == $v->grade->pkGra)
                                                <tr class="ocg_item ocg ocg_{{$v->optionalCoursesGroup->pkCrs}} ocg_gra_{{$kg}} ocg_ord_{{$v->eoc_order}}">
                                                   <td>{{$o}}</td>
                                                   <td>{{$v->optionalCoursesGroup->crs_CourseName}}</td>
                                                   <td><input type="hidden" class="hidden_inputs" name="OCG[]" value="{{$v->optionalCoursesGroup->pkCrs}}"><input type="hidden" class="hidden_inputs" name="OCG_order[]" value="{{$v->eoc_order}}">{{$v->eoc_order}}</td>
                                                   <td><input onkeyup="cleanHrs(this)" required="" class="ids_only form-control form-control-border" name="ocg_hrs[]" type="text" id="ocg_hrs_{{$v->optionalCoursesGroup->pkCrs}}_{{$kg}}" value="{{$v->eoc_hours}}" aria-required="true" class="valid" maxlength="3"></td>
                                                   <td><input type="hidden" class="hidden_inputs" name="ocg_gra[]" value="{{$kg}}"><a sel-id="{{$i}}" data-id="{{$v->optionalCoursesGroup->pkCrs}}" data-gra="{{$kg}}" onclick="removeOCG(this)" href="javascript:void(0)" class="theme_btn red_btn min_btn">{{trans('general.gn_delete')}}</a></td>
                                                   <td>
                                                      <div class="form-group form-check"><input type="checkbox" @if($v->eoc_default == 'Yes') checked @endif class="form-check-input" name="default_ocg[]" id="default_ocg_{{$v->optionalCoursesGroup->pkCrs}}" value="{{$v->optionalCoursesGroup->pkCrs}}_{{$kg}}"><label class="custom_checkbox"></label></div>
                                                   </td>
                                                </tr>
                                                <?php $o++; ?>
                                                @endif
                                                @endforeach
                                                <tr class="ocg ocg_main">
                                                   <td>{{$o}}</td>
                                                   <td>
                                                      <select class="form-control icon_control dropdown_control select2drp ids_only" id="ocg_select_{{$i}}" name="_{{$i}}" tabindex="-1" aria-hidden="true">
                                                         <option value="">{{trans('general.gn_select')}}</option>
                                                         @foreach($optionalCoursesGroup as $k => $v)
                                                         <option value="{{$v->pkCrs}}">{{$v->crs_CourseName}}</option>
                                                         @endforeach
                                                      </select>
                                                   </td>
                                                   <td>
                                                      <select class="form-control icon_control dropdown_control cu_order select2drp" id="eoc_order" name="eoc_order" tabindex="-1" aria-hidden="true">
                                                         <option value="">{{trans('general.gn_select')}}</option>
                                                         <option value="1">1</option>
                                                         <option value="2">2</option>
                                                         <option value="3">3</option>
                                                         <option value="4">4</option>
                                                         <option value="5">5</option>
                                                      </select>
                                                   </td>
                                                   <td><input onkeyup="cleanHrs(this)" class="hours_input form-control form-control-border" type="text" id="ocg_hrs" maxlength="3"></td>
                                                   <td>
                                                      <a onclick="addOCG(this)" href="javascript:void(0)" class="addElemBtn theme_btn min_btn" data-elem-id="{{$i}}" data-elem-gra="{{$kg}}">{{trans('general.gn_add')}}</a>
                                                   </td>
                                                   <td></td>
                                                </tr>
                                             </tbody>
                                          </table>
                                       </div>
                                    </div>
                                    <div class="col-md-12">
                                       <p class="mt-2"><strong class="title_label">{{trans('general.gn_mandatory_foreign_language_courses')}}</strong></p>
                                       <div class="table-responsive mt-2">
                                          <table class="color_table fcg_table fcg_table_{{$kg}}">
                                             <tbody>
                                                <tr>
                                                   <th width="15%">{{trans('general.sr_no')}}</th>
                                                   <th width="45%">{{trans('general.gn_courses')}}</th>
                                                   <th width="20%">{{trans('general.gn_course_order')}}</th>
                                                   <th width="15%">{{trans('general.gn_week_hours')}}</th>
                                                   <th width="15%">{{trans('general.gn_action')}}</th>
                                                   <th width="25%">{{trans('general.gn_default')}}</th>
                                                </tr>
                                                <?php $f = 1;?>
                                                @foreach($mdata->foreignLanguageCourse as $k => $v)
                                                @if($kg == $v->grade->pkGra)
                                                <tr class="fcg_item fcg fcg_{{$v->foreignLanguageGroup->pkCrs}} fcg_gra_{{$kg}} fcg_ord_{{$v->efc_order}}">
                                                   <td>{{$f}}</td>
                                                   <td>{{$v->foreignLanguageGroup->crs_CourseName}}</td>
                                                   <td><input type="hidden" class="hidden_inputs" name="FCG[]" value="{{$v->foreignLanguageGroup->pkCrs}}"><input type="hidden" class="hidden_inputs" name="FCG_order[]" value="{{$v->efc_order}}">{{$v->efc_order}}</td>
                                                   <td><input onkeyup="cleanHrs(this)" class="ids_only form-control form-control-border" required="" name="fcg_hrs[]" type="text" id="fcg_hrs_{{$v->foreignLanguageGroup->pkCrs}}_{{$kg}}" value="{{$v->efc_hours}}" maxlength="3"></td>
                                                   <td><input type="hidden" class="hidden_inputs" name="fcg_gra[]" value="{{$kg}}"><a sel-id="{{$i}}" data-id="{{$v->foreignLanguageGroup->pkCrs}}" data-gra="{{$kg}}" onclick="removeFCG(this)" href="javascript:void(0)" class="theme_btn red_btn min_btn">{{trans('general.gn_delete')}}</a></td>
                                                   <td>
                                                      <div class="form-group form-check"><input @if($v->efc_default == 'Yes') checked @endif type="checkbox" class="form-check-input" name="default_fcg[]" id="default_fcg_{{$v->foreignLanguageGroup->pkCrs}}" value="{{$v->foreignLanguageGroup->pkCrs}}_{{$kg}}"><label class="custom_checkbox"></label></div>
                                                   </td>
                                                </tr>
                                                <?php $f++; ?>
                                                @endif
                                                @endforeach
                                                <tr class="fcg fcg_main">
                                                   <td>{{$f}}</td>
                                                   <td>
                                                      <select class="form-control icon_control dropdown_control select2drp ids_only" id="fcg_select_{{$i}}" name="_{{$i}}" tabindex="-1" aria-hidden="true">
                                                         <option value="">{{trans('general.gn_select')}}</option>
                                                         @foreach($foreignLanguageGroup as $k => $v)
                                                         <option value="{{$v->pkCrs}}">{{$v->crs_CourseName}}</option>
                                                         @endforeach
                                                      </select>
                                                   </td>
                                                   <td>
                                                      <select class="form-control icon_control dropdown_control cu_order select2drp" id="efc_order" name="efc_order">
                                                         <option value="">{{trans('general.gn_select')}}</option>
                                                         <option value="1">1</option>
                                                         <option value="2">2</option>
                                                         <option value="3">3</option>
                                                         <option value="4">4</option>
                                                         <option value="5">5</option>
                                                      </select>
                                                   </td>
                                                   <td><input onkeyup="cleanHrs(this)" class="hours_input form-control form-control-border" type="text" id="fcg_hrs" maxlength="3"></td>
                                                   <td>
                                                      <a onclick="addFCG(this)" href="javascript:void(0)" class="addElemBtn theme_btn min_btn" data-elem-id="{{$i}}" data-elem-gra="{{$kg}}">{{trans('general.gn_add')}}</a>
                                                   </td>
                                                   <td></td>
                                                </tr>
                                             </tbody>
                                          </table>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <?php $i++;?>
                              @endforeach
                              <div class="text-center">
                                 <button type="submit" class="theme_btn">{{trans('general.gn_update')}}</button>
                                 <a class="theme_btn red_btn" href="{{url('/admin/educationplan')}}">{{trans('general.gn_cancel')}}</a>
                              </div>
                           </div>
                        </div>
                     </div>
                  </form>
               </div>
            </div>
         </div>
      </div>
   </div>
   <div class="ep_add_element" style="display: none;">
      <div class="profile_info_container">
         <div class="row">
            <div class="col-4"></div>
            <div class="col-4" style="text-align: center;"><strong class="gra_name"></strong></div>
            <div class="col-4 text-right">
               <img src="{{asset('images/ic_close_circle.png')}}" class="rm_eng close_img">
            </div>
            <div class="col-md-12">
               <p class="mt-2"><strong class="title_label">{{trans('general.gn_mandatory_courses')}}</strong></p>
               <div class="table-responsive mt-2">
                  <table class="color_table mcg_table">
                     <tr>
                        <th width="10%">{{trans('general.sr_no')}}</th>
                        <th width="45%">{{trans('general.gn_courses')}}</th>
                        <th width="5%">{{trans('general.gn_week_hours')}}</th>
                        <th width="5%">{{trans('general.gn_teacher_week_hour_rate')}}</th>
                        <th width="5%">{{trans('general.gn_summer_holiday_hour')}}</th>
                        <th width="15%">{{trans('general.gn_action')}}</th>
                        <th width="25%">{{trans('general.gn_default')}}</th>
                     </tr>
                     <tr class="mcg mcg_main">
                        <td>1</td>
                        <td>
                           <select onchange="selectCourse(this)" class="form-control icon_control dropdown_control ids_only" id="mcg_select">
                              <option value="">{{trans('general.gn_select')}}</option>
                              @foreach($course as $k => $v)
                              <option data-alternativename="{{$v->crs_CourseAlternativeName}}" value="{{$v->pkCrs}}">{{$v->crs_CourseName}}</option>
                              @endforeach
                           </select>
                        </td>
                        <td><input onkeyup="cleanHrs(this)" class="hours_input form-control form-control-border" type="text" id="mcg_hrs" maxlength="3"></td>
                        <td><input onkeyup="cleanHrs(this)" class="hours_input form-control form-control-border" type="text" id="mcg_weekHourRateTeacher" maxlength="3"></td>
                        <td><input onkeyup="cleanHrs(this)" class="hours_input form-control form-control-border input_disable" readonly="readonly" type="text" id="mcg_summerHolidayHourRate" maxlength="3"></td>
                        <td>
                           <a onclick="addMCG(this)" href="javascript:void(0)" class="addElemBtn theme_btn min_btn">{{trans('general.gn_add')}}</a>
                        </td>
                        <td></td>
                     </tr>
                  </table>
               </div>
            </div>
            <div class="col-md-12">
               <p class="mt-2"><strong class="title_label">{{trans('general.gn_optional_courses')}}</strong></p>
               <div class="table-responsive mt-2">
                  <table class="color_table ocg_table">
                     <tr>
                        <th width="10%">{{trans('general.sr_no')}}</th>
                        <th width="45%">{{trans('general.gn_courses')}}</th>
                        <th width="20%">{{trans('general.gn_course_order')}}</th>
                        <th width="15%">{{trans('general.gn_week_hours')}}</th>
                        <th width="15%">{{trans('general.gn_action')}}</th>
                        <th width="25%">{{trans('general.gn_default')}}</th>
                     </tr>
                     <tr class="ocg ocg_main">
                        <td>1</td>
                        <td>
                           <select class="form-control icon_control dropdown_control ids_only" id="ocg_select">
                              <option value="">{{trans('general.gn_select')}}</option>
                              @foreach($optionalCoursesGroup as $k => $v)
                              <option value="{{$v->pkCrs}}">{{$v->crs_CourseName}}</option>
                              @endforeach
                           </select>
                        </td>
                        <td>
                           <select class="form-control icon_control dropdown_control cu_order" id="eoc_order" name="eoc_order">
                              <option value="">{{trans('general.gn_select')}}</option>
                              <option value="1">1</option>
                              <option value="2">2</option>
                              <option value="3">3</option>
                              <option value="4">4</option>
                              <option value="5">5</option>
                           </select>
                        </td>
                        <td><input onkeyup="cleanHrs(this)" class="hours_input" type="text" id="ocg_hrs" maxlength="3"></td>
                        <td>
                           <a onclick="addOCG(this)" href="javascript:void(0)" class="addElemBtn theme_btn min_btn">{{trans('general.gn_add')}}</a>
                        </td>
                        <td></td>
                     </tr>
                  </table>
               </div>
            </div>
            <div class="col-md-12">
               <p class="mt-2"><strong class="title_label">{{trans('general.gn_mandatory_foreign_language_courses')}}</strong></p>
               <div class="table-responsive mt-2">
                  <table class="color_table fcg_table">
                     <tr>
                        <th width="10%">{{trans('general.sr_no')}}</th>
                        <th width="45%">{{trans('general.gn_courses')}}</th>
                        <th width="20%">{{trans('general.gn_course_order')}}</th>
                        <th width="15%">{{trans('general.gn_week_hours')}}</th>
                        <th width="15%">{{trans('general.gn_action')}}</th>
                        <th width="25%">{{trans('general.gn_default')}}</th>
                     </tr>
                     <tr class="fcg fcg_main">
                        <td>1</td>
                        <td>
                           <select class="form-control icon_control dropdown_control ids_only" id="fcg_select">
                              <option value="">{{trans('general.gn_select')}}</option>
                              @foreach($foreignLanguageGroup as $k => $v)
                              <option value="{{$v->pkCrs}}">{{$v->crs_CourseName}}</option>
                              @endforeach
                           </select>
                        </td>
                        <td>
                           <select class="form-control icon_control dropdown_control cu_order" id="efc_order" name="efc_order">
                              <option value="">{{trans('general.gn_select')}}</option>
                              <option value="1">1</option>
                              <option value="2">2</option>
                              <option value="3">3</option>
                              <option value="4">4</option>
                              <option value="5">5</option>
                           </select>
                        </td>
                        <td><input onkeyup="cleanHrs(this)" class="hours_input" type="text" id="fcg_hrs" maxlength="3"></td>
                        <td>
                           <a onclick="addFCG(this)" href="javascript:void(0)" class="addElemBtn theme_btn min_btn">{{trans('general.gn_add')}}</a>
                        </td>
                        <td></td>
                     </tr>
                  </table>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<input type="hidden" id="all_mandatory_validate_txt" value="{{trans('validation.validate_sel_all_mandatory')}}">
<input type="hidden" id="all_optional_validate_txt" value="{{trans('validation.validate_sel_all_optional')}}">
<input type="hidden" id="all_foreign_validate_txt" value="{{trans('validation.validate_sel_all_foreign')}}">
<input type="hidden" id="hours_validate_txt" value="{{trans('validation.validate_week_hours')}}">
<input type="hidden" id="hours_rate_validate_txt" value="{{trans('validation.validate_teacher_week_hours_rate')}}">
<input type="hidden" id="summer_holiday_validate_txt" value="{{trans('validation.validate_summer_holiday_hours')}}">
<input type="hidden" id="OCG_validate_txt" value="{{trans('validation.validate_OCG')}}">
<input type="hidden" id="FCG_validate_txt" value="{{trans('validation.validate_FCG')}}">
<input type="hidden" id="MCG_validate_txt" value="{{trans('validation.validate_MCG')}}">
<input type="hidden" id="grade_validate_txt" value="{{trans('validation.validate_grade')}}">
<input type="hidden" id="course_order_validate_txt" value="{{trans('validation.validate_course_order')}}">
<input type="hidden" id="course_grade_validate_txt" value="{{trans('validation.validate_course_grade')}}">
<input type="hidden" id="order_grade_validate_txt" value="{{trans('validation.validate_order_grade')}}">
<input type="hidden" id="ep_course_validate_txt" value="{{trans('validation.validate_education_plan_course')}}">
<input type="hidden" id="ep_grade_validate_txt" value="{{trans('message.msg_grade_already_selected')}}">
<input type="hidden" id="ep_ocg_ordername_validate_txt" value="{{trans('validation.validate_ocg_ordername')}}">
<input type="hidden" id="ep_fog_ordername_validate_txt" value="{{trans('validation.validate_fog_ordername')}}">

<script type="text/javascript">
   var listing_url = "{{route('fetch-educationplan-lists')}}";
   var FCG = [];
   var OCG = [];
   var MCG = [];
   <?php foreach($mdata->mandatoryCourse as $k => $v){?>
       MCG.push('{{$v->mandatoryCourseGroup->pkCrs}}');
   <?php } ?>
   <?php foreach($mdata->optionalCourse as $k => $v){?>
       OCG.push('{{$v->optionalCoursesGroup->pkCrs}}');
   <?php } ?>
   <?php foreach($mdata->foreignLanguageCourse as $k => $v){?>
       FCG.push('{{$v->foreignLanguageGroup->pkCrs}}');
   <?php } ?>
</script>
<!-- End Content Body -->
@endsection
@push('datatable-scripts')
<!-- Include this Page JS -->
<script type="text/javascript" src="{{ asset('js/dashboard/education_plan.js') }}"></script>
@endpush