@extends('layout.app_with_login')
@section('title', trans('sidebar.sidebar_nav_education_plan'))
@section('script', asset('js/dashboard/education_plan.js'))
@section('content')
<!-- Page Content  -->
<div class="section">
   <div class="container-fluid">
      <div class="row ">
         <div class="col-12 mb-3">
            <h2 class="title"><span>{{trans('sidebar.sidebar_nav_education_plan')}} > </span>{{trans('general.gn_add_new')}}</h2>
         </div>
         <div class="col-12">
            <div class="white_box pt-3 pb-3">
               <div class="container-fluid">
                  <form name="add-education-plan-form">
                     <div class="row">
                        <div class="col-lg-12">
                           <div class="pl-5 pr-5">
                              <div>
                                 <p class="mt-2"><strong>{{trans('sidebar.sidebar_nav_education_plan')}}</strong></p>
                              </div>
                              <div class="bg-color-form">
                                 <div class="row">
                                    @foreach($languages as $k => $v)
                                    <div class="col-lg-6">
                                       <div class="form-group">
                                          <label>{{trans('general.gn_name')}} {{$v->language_name}} *</label>
                                          <input type="text" name="epl_EducationPlanName_{{$v->language_key}}" id="epl_EducationPlanName_{{$v->language_key}}" class="form-control force_require icon_control" required="">
                                       </div>
                                    </div>
                                    @endforeach
                                    <div class="col-lg-6">
                                       <div class="form-group">
                                          <label>{{trans('sidebar.sidebar_nav_education_program')}} *</label>
                                          <select name="fkEplEdp" id="fkEplEdp" class="form-control icon_control dropdown_control">
                                             <option value="">{{trans('general.gn_select')}}</option>
                                             @foreach($educationProgram as $tkey => $tvalue)
                                             <option value="{{$tvalue['pkEdp']}}">{{$tvalue['edp_Name']}}</option>
                                             {{-- Sub Parent --}}
                                             @foreach($tvalue['children'] as $ckey=> $cValue)
                                             @if($cValue['edp_ParentId']==$tvalue['pkEdp'])
                                             <option value="{{$cValue['pkEdp']}}">&emsp;&emsp;{{$cValue['edp_Name']}}</option>
                                             {{-- Sub Sub Parent --}}
                                             @foreach($cValue['children'] as $cckey=> $ccValue)
                                             @if($ccValue['edp_ParentId']==$cValue['pkEdp'])
                                             <option value="{{$ccValue['pkEdp']}}">&emsp;&emsp;&emsp;&emsp;{{$ccValue['edp_Name']}}</option>
                                             {{-- Sub Sub Sub Parent --}}
                                             @foreach($ccValue['children'] as $skey=> $sValue)
                                             @if($sValue['edp_ParentId']==$ccValue['pkEdp'])
                                             <option value="{{$sValue['pkEdp']}}">&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;{{$sValue['edp_Name']}}</option>
                                             {{-- Sub Sub Sub Sub Parent --}}
                                             @foreach($sValue['children'] as $sskey=> $ssValue)
                                             @if($ssValue['edp_ParentId']==$sValue['pkEdp'])
                                             <option value="{{$ssValue['pkEdp']}}">&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;{{$ssValue['edp_Name']}}</option>
                                             @endif
                                             @endforeach
                                             @endif
                                             @endforeach
                                             @endif
                                             @endforeach
                                             @endif
                                             @endforeach
                                             @endforeach
                                          </select>
                                       </div>
                                    </div>
                                    <div class="col-lg-6">
                                       <div class="form-group">
                                          <label>{{trans('sidebar.sidebar_nav_national_education_plan')}} *</label>
                                          <select name="fkEplNep" id="fkEplNep" class="form-control icon_control dropdown_control">
                                             <option value="">{{trans('general.gn_select')}}</option>
                                             @foreach($nationalEducationPlan as $k => $v)
                                             <option value="{{$v->pkNep}}">{{$v->nep_NationalEducationPlanName}}</option>
                                             @endforeach
                                          </select>
                                       </div>
                                    </div>
                                    <div class="col-lg-6">
                                       <div class="form-group">
                                          <label>{{trans('general.gn_canton')}} *</label>
                                          <select name="fkEplCan" id="fkEplCan" class="form-control icon_control dropdown_control select2_drop">
                                             <option value="">{{trans('general.gn_select')}}</option>
                                             @if(!empty($cantons))
                                                @foreach($cantons as $k => $v)
                                                   <option value="{{$v->pkCan}}">{{$v->can_CantonName}}, {{$v->state->sta_StateName}}</option>
                                                @endforeach
                                             @endif
                                          </select>
                                       </div>
                                    </div>
                                    <div class="col-lg-6">
                                       <div class="form-group">
                                          <label>{{trans('sidebar.sidebar_certificate_blueprints')}} *</label>
                                          <select name="fkCbp" id="fkCbp" class="form-control icon_control dropdown_control">
                                             <option value="">{{trans('general.gn_select')}}</option>
                                             @if(!empty($certificateBluePrints))
                                             @foreach($certificateBluePrints as $k => $v)
                                             <option value="{{$v->pkCbp}}">{{$v->cbp_BluePrintName}}</option>
                                             @endforeach
                                             @endif
                                          </select>
                                       </div>
                                    </div>
                                    <div class="col-lg-6">
                                       <div class="form-group">
                                          <label>{{trans('sidebar.sidebar_nav_education_profile')}}</label>
                                          <select name="fkEplEpr" id="fkEplEpr" class="form-control icon_control dropdown_control">
                                             <option value="">{{trans('general.gn_select')}}</option>
                                             @foreach($educationProfile as $k => $v)
                                             <option value="{{$v->pkEpr}}">{{$v->epr_EducationProfileName}}</option>
                                             @endforeach
                                          </select>
                                       </div>
                                    </div>
                                    <div class="col-lg-6">
                                       <div class="form-group">
                                          <label>{{trans('sidebar.sidebar_nav_qualification_degree')}}</label>
                                          <select name="fkEplQde" id="fkEplQde" class="form-control icon_control dropdown_control">
                                             <option value="">{{trans('general.gn_select')}}</option>
                                             @foreach($qualificationDegree as $k => $v)
                                             <option value="{{$v->pkQde}}">{{$v->qde_QualificationDegreeName}}</option>
                                             @endforeach
                                          </select>
                                       </div>
                                    </div>
                                    <div class="col-lg-6">
                                       <div class="form-group">
                                          <label>{{trans('general.gn_grades')}} </label>
                                          <select name="gra_sel" id="gra_sel" class="form-control icon_control dropdown_control">
                                             <option value="">{{trans('general.gn_select')}}</option>
                                             @foreach($grade as $k => $v)
                                             <option value="{{$v->pkGra}}">{{$v->gra_GradeNumeric}} ({{$v->gra_GradeNameRoman}})</option>
                                             @endforeach
                                          </select>
                                       </div>
                                    </div>
                                    <div class="col-lg-6">
                                       <label style="visibility: hidden;display: block;">{{trans('general.gn_grades')}} </label>
                                       <div class="form-group">
                                          <button type="button" class="theme_btn" id="add_ep_gra">{{trans('general.gn_add')}}</button>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="text-center">
                                 <button type="submit" class="theme_btn">{{trans('general.gn_submit')}}</button>
                                 <a class="theme_btn red_btn" href="{{url('/admin/educationplan')}}">{{trans('general.gn_cancel')}}</a>
                              </div>
                           </div>
                        </div>
                     </div>
                  </form>
               </div>
            </div>
         </div>
      </div>
   </div>
   <div class="ep_add_element" style="display: none;">
      <div class="profile_info_container">
         <div class="row">
            <div class="col-4"></div>
            <div class="col-4" style="text-align: center;"><strong class="gra_name"></strong></div>
            <div class="col-4 text-right">
               <img src="{{asset('images/ic_close_circle.png')}}" class="rm_eng close_img">
            </div>
            <div class="col-md-12">
               <p class="mt-2"><strong class="title_label">{{trans('general.gn_mandatory_courses')}}</strong></p>
               <div class="table-responsive mt-2">
                  <table class="color_table mcg_table">
                     <tr>
                        <th width="10%">{{trans('general.sr_no')}}</th>
                        <th width="45%">{{trans('general.gn_courses')}}</th>
                        <th width="5%">{{trans('general.gn_week_hours')}}</th>
                        <th width="5%">{{trans('general.gn_teacher_week_hour_rate')}}</th>
                        <th width="5%">{{trans('general.gn_summer_holiday_hour')}}</th>
                        <th width="15%">{{trans('general.gn_action')}}</th>
                        <th width="25%">{{trans('general.gn_default')}}</th>
                     </tr>
                     <tr class="mcg mcg_main">
                        <td>1</td>
                        <td>
                           <select onchange="selectCourse(this)" class="form-control icon_control dropdown_control ids_only" id="mcg_select">
                              <option value="">{{trans('general.gn_select')}}</option>
                              @foreach($course as $k => $v)
                              <option value="{{$v->pkCrs}}" data-alternativename="{{$v->crs_CourseAlternativeName}}">{{$v->crs_CourseName}}</option>
                              @endforeach
                           </select>
                        </td>
                        <td><input onkeyup="cleanHrs(this)" class="hours_input form-control form-control-border" type="text" id="mcg_hrs" maxlength="3"></td>
                        <td><input onkeyup="cleanHrs(this)" class="hours_input form-control form-control-border" type="text" id="mcg_weekHourRateTeacher" maxlength="3"></td>
                        <td><input onkeyup="cleanHrs(this)" class="hours_input form-control form-control-border input_disable" readonly="readonly" type="text" id="mcg_summerHolidayHourRate" maxlength="3"></td>
                        <td>
                           <a onclick="addMCG(this)" href="javascript:void(0)" class="addElemBtn theme_btn min_btn">{{trans('general.gn_add')}}</a>
                        </td>
                        <td></td>
                     </tr>
                  </table>
               </div>
            </div>
            <div class="col-md-12">
               <p class="mt-2"><strong class="title_label">{{trans('general.gn_optional_courses')}}</strong></p>
               <div class="table-responsive mt-2">
                  <table class="color_table ocg_table">
                     <tr>
                        <th width="10%">{{trans('general.sr_no')}}</th>
                        <th width="45%">{{trans('general.gn_courses')}}</th>
                        <th width="20%">{{trans('general.gn_course_order')}}</th>
                        <th width="15%">{{trans('general.gn_week_hours')}}</th>
                        <th width="15%">{{trans('general.gn_action')}}</th>
                        <th width="25%">{{trans('general.gn_default')}}</th>
                     </tr>
                     <tr class="ocg ocg_main">
                        <td>1</td>
                        <td>
                           <select class="form-control icon_control dropdown_control ids_only" id="ocg_select">
                              <option value="">{{trans('general.gn_select')}}</option>
                              @foreach($optionalCoursesGroup as $k => $v)
                              <option value="{{$v->pkCrs}}">{{$v->crs_CourseName}}</option>
                              @endforeach
                           </select>
                        </td>
                        <td>
                           <select class="form-control icon_control dropdown_control cu_order" id="eoc_order" name="eoc_order">
                              <option value="">{{trans('general.gn_select')}}</option>
                              <option value="1">1</option>
                              <option value="2">2</option>
                              <option value="3">3</option>
                              <option value="4">4</option>
                              <option value="5">5</option>
                           </select>
                        </td>
                        <td><input onkeyup="cleanHrs(this)" class="hours_input form-control form-control-border" maxlength="3" type="text" id="ocg_hrs"></td>
                        <td>
                           <a onclick="addOCG(this)" href="javascript:void(0)" class="addElemBtn theme_btn min_btn">{{trans('general.gn_add')}}</a>
                        </td>
                        <td></td>
                     </tr>
                  </table>
               </div>
            </div>
            <div class="col-md-12">
               <p class="mt-2"><strong class="title_label">{{trans('general.gn_mandatory_foreign_language_courses')}}</strong></p>
               <div class="table-responsive mt-2">
                  <table class="color_table fcg_table">
                     <tr>
                        <th width="10%">{{trans('general.sr_no')}}</th>
                        <th width="45%">{{trans('general.gn_courses')}}</th>
                        <th width="20%">{{trans('general.gn_course_order')}}</th>
                        <th width="15%">{{trans('general.gn_week_hours')}}</th>
                        <th width="15%">{{trans('general.gn_action')}}</th>
                        <th width="25%">{{trans('general.gn_default')}}</th>
                     </tr>
                     <tr class="fcg fcg_main">
                        <td>1</td>
                        <td>
                           <select class="form-control icon_control dropdown_control ids_only" id="fcg_select">
                              <option value="">{{trans('general.gn_select')}}</option>
                              @foreach($foreignLanguageGroup as $k => $v)
                              <option value="{{$v->pkCrs}}">{{$v->crs_CourseName}}</option>
                              @endforeach
                           </select>
                        </td>
                        <td>
                           <select class="form-control icon_control dropdown_control cu_order" id="efc_order" name="efc_order">
                              <option value="">{{trans('general.gn_select')}}</option>
                              <option value="1">1</option>
                              <option value="2">2</option>
                              <option value="3">3</option>
                              <option value="4">4</option>
                              <option value="5">5</option>
                           </select>
                        </td>
                        <td><input onkeyup="cleanHrs(this)" class="hours_input form-control form-control-border" maxlength="3" type="text" id="fcg_hrs"></td>
                        <td>
                           <a onclick="addFCG(this)" href="javascript:void(0)" class="addElemBtn theme_btn min_btn">{{trans('general.gn_add')}}</a>
                        </td>
                        <td></td>
                     </tr>
                  </table>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<input type="hidden" id="all_mandatory_validate_txt" value="{{trans('validation.validate_sel_all_mandatory')}}">
<input type="hidden" id="all_optional_validate_txt" value="{{trans('validation.validate_sel_all_optional')}}">
<input type="hidden" id="all_foreign_validate_txt" value="{{trans('validation.validate_sel_all_foreign')}}">
<input type="hidden" id="hours_validate_txt" value="{{trans('validation.validate_week_hours')}}">
<input type="hidden" id="hours_rate_validate_txt" value="{{trans('validation.validate_teacher_week_hours_rate')}}">
<input type="hidden" id="summer_holiday_validate_txt" value="{{trans('validation.validate_summer_holiday_hours')}}">
<input type="hidden" id="OCG_validate_txt" value="{{trans('validation.validate_OCG')}}">
<input type="hidden" id="FCG_validate_txt" value="{{trans('validation.validate_FCG')}}">
<input type="hidden" id="MCG_validate_txt" value="{{trans('validation.validate_MCG')}}">
<input type="hidden" id="grade_validate_txt" value="{{trans('validation.validate_grade')}}">
<input type="hidden" id="course_order_validate_txt" value="{{trans('validation.validate_course_order')}}">
<input type="hidden" id="course_grade_validate_txt" value="{{trans('validation.validate_course_grade')}}">
<input type="hidden" id="order_grade_validate_txt" value="{{trans('validation.validate_order_grade')}}">
<input type="hidden" id="ep_course_validate_txt" value="{{trans('validation.validate_education_plan_course')}}">
<input type="hidden" id="ep_grade_validate_txt" value="{{trans('message.msg_grade_already_selected')}}">
<input type="hidden" id="ep_ocg_ordername_validate_txt" value="{{trans('validation.validate_ocg_ordername')}}">
<input type="hidden" id="ep_fog_ordername_validate_txt" value="{{trans('validation.validate_fog_ordername')}}">
<script>
   var listing_url = "{{route('fetch-educationplan-lists')}}";

   var FCG = [];
   var OCG = [];
   var MCG = [];
</script>
<!-- End Content Body -->
@endsection
@push('datatable-scripts')
<!-- Include this Page JS -->
<script type="text/javascript" src="{{ asset('js/dashboard/education_plan.js') }}"></script>
@endpush