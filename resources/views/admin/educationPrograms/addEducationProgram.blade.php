@extends('layout.app_with_login')
@section('title','Education Programs')
@section('script', asset('js/dashboard/education_programs.js'))
@section('content')
 <!-- Page Content  -->
<div class="section">
    <div class="container-fluid">
        <div class="row ">
            <div class="col-12 mb-3">
                <h2 class="title"><a class="no_sidebar_active" href="{{url('/admin/educationprogram')}}"><span>{{trans('sidebar.sidebar_nav_education_program')}} > </span></a> {{trans('general.gn_add_new')}}</h2>
            </div>
            <div class="col-md-4 mb-3">

            </div>
            <div class="col-md-4 text-md-right mb-3">

            </div>
            <div class="col-md-2 col-6 mb-3">

            </div>
            <div class="col-md-2 col-6 mb-3">

            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="white_box pt-5 pb-5">
                    <div class="container-fluid">
                        <form name="add-educationProgram-form">
                            <div class="row">
                                <div class="col-lg-3"></div>
                                <div class="col-lg-6">
                                    <div class="">
                                        <input type="hidden" name="edp_Uid" value="EPR" id="edp_Uid">
                                        <input type="hidden" name="pkEdp" id="pkEdp">
                                        @foreach($languages as $k => $v)
                                            <div class="form-group">
                                                <label>{{trans('general.gn_name')}} {{$v->language_name}} *</label>
                                                <input type="text" name="edp_Name_{{$v->language_key}}" id="edp_Name_{{$v->language_key}}" class="form-control force_require icon_control" required="">
                                            </div>
                                        @endforeach
                                        <div class="form-group">
                                            <label>{{trans('general.gn_stream')}} *</label>
                                            <select id="edp_ParentId" name="edp_ParentId" class="form-control icon_control dropdown_control">
                                                <option value="">{{trans('general.gn_select')}}</option>
                                                <option value="0">{{trans('general.gn_self')}}</option>
                                                {{-- Parent --}}
                                                @foreach($educationProgram as $tkey => $tvalue)
                                                    <option value="{{$tvalue['pkEdp']}}">{{$tvalue['edp_Name']}}</option>
                                                    {{-- Sub Parent --}}
                                                    @foreach($tvalue['children'] as $ckey=> $cValue)
                                                        @if($cValue['edp_ParentId']==$tvalue['pkEdp'])
                                                            <option value="{{$cValue['pkEdp']}}">&emsp;&emsp;{{$cValue['edp_Name']}}</option>
                                                            {{-- Sub Sub Parent --}}
                                                            @foreach($cValue['children'] as $cckey=> $ccValue)
                                                                @if($ccValue['edp_ParentId']==$cValue['pkEdp'])
                                                                    <option value="{{$ccValue['pkEdp']}}">&emsp;&emsp;&emsp;&emsp;{{$ccValue['edp_Name']}}</option>
                                                                    {{-- Sub Sub Sub Parent --}}
                                                                    @foreach($ccValue['children'] as $skey=> $sValue)
                                                                        @if($sValue['edp_ParentId']==$ccValue['pkEdp'])
                                                                            <option value="{{$sValue['pkEdp']}}">&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;{{$sValue['edp_Name']}}</option>
                                                                            {{-- Sub Sub Sub Sub Parent --}}
                                                                            @foreach($sValue['children'] as $sskey=> $ssValue)
                                                                                @if($ssValue['edp_ParentId']==$sValue['pkEdp'])
                                                                                    <option value="{{$ssValue['pkEdp']}}">&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;{{$ssValue['edp_Name']}}</option>
                                                                                    {{-- Sub Sub Sub Sub Parent --}}
                                                                                    @foreach($ssValue['children'] as $sskey=> $sssValue)
                                                                                        @if($sssValue['edp_ParentId']==$ssValue['pkEdp'])
                                                                                            <option value="{{$sssValue['pkEdp']}}">&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;{{$sssValue['edp_Name']}}</option>
                                                                                            {{--sub sub Sub Sub Sub Sub Parent --}}
                                                                                            @foreach($sssValue['children'] as $spValue)
                                                                                                @if($spValue['edp_ParentId']==$sssValue['pkEdp'])
                                                                                                    <option value="{{$spValue['pkEdp']}}">&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;{{$spValue['edp_Name']}}</option>
                                                                                                @endif
                                                                                            @endforeach
                                                                                        @endif
                                                                                    @endforeach

                                                                                @endif
                                                                            @endforeach

                                                                        @endif
                                                                    @endforeach

                                                                @endif
                                                            @endforeach

                                                        @endif
                                                    @endforeach

                                                @endforeach

                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label>Note</label>
                                            <input type="text" name="edp_Notes" id="edp_Notes" class="form-control icon_control">
                                        </div>

                                    </div>
                                    <div class="text-center">
                                        <button type="submit" class="theme_btn">{{trans('general.gn_submit')}}</button>
                                        <a class="theme_btn red_btn no_sidebar_active" href="{{url('/admin/educationprogram')}}">{{trans('general.gn_cancel')}}</a>
                                    </div>
                                </div>
                                <div class="col-lg-3"></div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    var listing_url = "{{route('fetch-educationprogram-lists')}}";
</script>
@endsection

@push('datatable-scripts')
<!-- Include datatable Page JS -->
<script type="text/javascript" src="{{ asset('js/dashboard/education_programs.js') }}"></script>
@endpush