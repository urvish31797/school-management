@extends('layout.app_with_login')
@section('title', trans('sidebar.sidebar_nav_universities'))
@section('content')
<!-- Page Content  -->
<div class="section">
    <div class="container-fluid">
        <div class="row ">
            <div class="col-12 col-md-5 mb-3">
                <h2 class="title"><span>{{trans('sidebar.sidebar_nav_masters')}} > </span>{{trans('sidebar.sidebar_nav_universities')}}</h2>
            </div>
            <div class="col-md-4 mb-3 offset-md-3">
                <input type="text" id="search_university" maxlength="30" class="form-control icon_control search_control" placeholder="{{trans('general.gn_search')}}">
            </div>
            <div class="col-md-4 text-md-right mb-3">
                <div class="row">
                    <div class="col-4 text-right pt-1 pr-0">
                        <label class="blue_label">{{trans('general.gn_ownership_type')}}</label>
                    </div>
                    <div class="col-7">
                        @include('partials.dropdown.dropdowns',[
                        'type'=>'ownership_type',
                        'name'=>'',
                        'selected'=>'',
                        'extra_params'=>[
                        "id"=>"ownership_filter"
                        ]
                        ])
                    </div>
                </div>
            </div>
            <div class="col-md-3 text-md-right mb-3">
                <div class="row">
                    <div class="col-4 text-right pt-1 pr-0">
                        <label class="blue_label">{{trans('general.gn_country')}}</label>
                    </div>
                    <div class="col-8">
                        @include('partials.dropdown.dropdowns',[
                        'type'=>'country',
                        'name'=>'',
                        'selected'=>'',
                        'extra_params'=>[
                        "id"=>"country_filter",
                        ]
                        ])
                    </div>
                </div>
            </div>
            <div class="col-md-3 text-md-right mb-3">
                <div class="row">
                    <div class="col-4 text-right pt-1 p-0">
                        <label class="blue_label">{{trans('general.gn_started_year')}}</label>
                    </div>
                    <div class="col-8">
                        <input type="text" id="year_filter" class="form-control datepicker-year date_control icon_control" placeholder="{{trans('general.gn_year')}}">
                    </div>
                </div>
            </div>
            <div class="col-md-2 col-6 mb-3">
                <a><button class="theme_btn show_modal full_width small_btn" data-toggle="modal" data-target="#add_new">{{trans('general.gn_add_new')}}</button></a>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="theme_table">
                    <div class="table-responsive">
                        {{$dt_html->table(['class'=>"universities_listing display responsive","width"=>"100%"],true)}}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Add New Popup -->
<div class="theme_modal modal fade" id="add_new" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <img src="{{asset('images/ic_close_bg.png')}}" class="modal_top_bg">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <img src="{{asset('images/ic_close_circle_white.png')}}">
                </button>
                <form name="add-university-form" id="add-university-form">
                    <div class="row">
                        <div class="col-lg-1"></div>
                        <div class="col-lg-10">
                            <h5 class="modal-title" id="exampleModalCenterTitle">{{trans('sidebar.sidebar_nav_universities')}}</h5>
                            <div class="text-center">
                                <div class="profile_box">
                                    <div class="profile_pic">
                                        <img id="university_img" src="{{ asset('images/user.png') }}">
                                        <input type="hidden" id="img_tmp" value="{{ asset('images/user.png') }}">
                                    </div>
                                </div>
                                <div class="upload_pic_link">
                                    <a href="javascript:void(0)">
                                        {{trans('general.gn_upload_photo')}}<input type="file" id="upload_profile" name="upload_profile"></a>

                                </div>
                            </div>
                            <input type="hidden" id="pkUni">
                            @foreach($languages as $k => $v)
                            <div class="form-group">
                                <label>{{trans('general.gn_name')}} {{$v->language_name}} *</label>
                                <input type="text" name="uni_UniversityName_{{$v->language_key}}" id="uni_UniversityName_{{$v->language_key}}" class="form-control force_require icon_control" required="">
                            </div>
                            @endforeach
                            <div class="form-group">
                                <label>{{trans('general.gn_started_year')}} *</label>
                                <input type="text" name="uni_YearStartedFounded" id="uni_YearStartedFounded" class="form-control datepicker-year  date_control icon_control">
                            </div>
                            <div class="form-group">
                                <label>{{trans('general.gn_country')}} *</label>
                                @include('partials.dropdown.dropdowns',[
                                'type'=>'country',
                                'name'=>'fkUniCny',
                                'selected'=>'',
                                'extra_params'=>[
                                "id"=>"fkUniCny",
                                ]
                                ])
                            </div>
                            <div class="form-group">
                                <label>{{trans('general.gn_ownership_type')}} *</label>
                                @include('partials.dropdown.dropdowns',[
                                'type'=>'ownership_type',
                                'name'=>'fkUniOty',
                                'selected'=>'',
                                'extra_params'=>[
                                "id"=>"fkUniOty"
                                ]
                                ])
                            </div>
                            <div class="form-group">
                                <label>{{trans('general.gn_note')}}</label>
                                <input type="text" name="uni_Notes" id="uni_Notes" class="form-control icon_control">
                            </div>
                            <div class="text-center modal_btn ">
                                <button type="button" onclick="save()" class="theme_btn">{{trans('general.gn_submit')}}</button>
                            </div>
                        </div>
                        <div class="col-lg-1"></div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="theme_modal modal fade" id="delete_prompt" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <img src="{{asset('images/ic_close_bg.png')}}" class="modal_top_bg">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <img src="{{asset('images/ic_close_circle_white.png')}}">
                </button>
                <div class="row">
                    <div class="col-lg-1"></div>
                    <div class="col-lg-10">
                        <h5 class="modal-title" id="exampleModalCenterTitle">{{trans('general.gn_delete')}}</h5>
                        <div class="form-group text-center">
                            <label>{{trans('general.gn_delete_prompt')}}</label>
                            <input type="hidden" id="did">
                        </div>
                        <div class="text-center modal_btn ">
                            <button style="display: none;" class="theme_btn show_delete_modal full_width small_btn" data-toggle="modal" data-target="#delete_prompt">{{trans('general.gn_delete')}}</button>
                            <button type="button" onclick="confirmDelete()" class="theme_btn">{{trans('general.gn_yes')}}</button>
                            <button type="button" data-dismiss="modal" class="theme_btn red_btn">{{trans('general.gn_no')}}</button>
                        </div>
                    </div>
                    <div class="col-lg-1"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End Content Body -->
@endsection

@push('datatable-scripts')
<!-- Include datatable Page JS -->
<script type="text/javascript" src="{{ asset('js/dashboard/university.js') }}"></script>
<script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
{!! $dt_html->scripts() !!}
{!! JsValidator::formRequest(\App\Http\Requests\UniversityRequest::class, '#add-university-form'); !!}
@endpush
