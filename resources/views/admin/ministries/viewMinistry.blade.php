@extends('layout.app_with_login')
@section('title', trans('sidebar.sidebar_nav_ministry_super_admin'))
@section('content')
<!-- Page Content  -->
<div class="section">
    <div class="container-fluid">
        <div class="row ">
            <div class="col-12 mb-3">
                <h2 class="title">
                <span>{{trans('sidebar.sidebar_nav_user_management')}} > </span>
                <a href="{{url('/admin/ministries')}}"><span>{{trans('sidebar.sidebar_nav_ministry_super_admin')}} > </span></a> {{trans('general.gn_details')}}</h2>
            </div>
            <div class="col-12">
                <div class="white_box pt-5 pb-5">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-lg-1"></div>
                            <div class="col-lg-10">
                                <div class="text-center">
                                    <div class="profile_box">
                                        <div class="profile_pic">
                                            <img src="@if(!empty($mdata->adm_Photo)) {{asset('images/users/')}}/{{$mdata->adm_Photo}} @else {{ asset('images/user.png') }}@endif">
                                        </div>
                                    </div>
                                    <h5 class="profile_name">{{$mdata->adm_Name}}</h5>
                                </div>
                                <div class="profile_info_container">
                                    <div class="row">
                                        <div class="col-md-6 col-lg-3">
                                            <div class="form-group">
                                                <p class="label">{{trans('general.n_uid')}} :</p>
                                                <p class="value">{{$mdata->adm_Uid}}</p>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-lg-3">
                                            <div class="form-group">
                                                <p class="label">{{trans('general.gn_government')}} ID :</p>
                                                <p class="value">{{ empty($mdata->adm_GovId) ? $mdata->adm_TempGovId : $mdata->adm_GovId }}</p>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-lg-3">
                                            <div class="form-group">
                                                <p class="label">{{trans('login.ln_email')}} :</p>
                                                <p class="value">{{$mdata->email}}</p>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-lg-3">
                                            <div class="form-group">
                                                <p class="label">{{trans('general.gn_phone')}} :</p>
                                                <p class="value">{{$mdata->adm_Phone}}</p>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-lg-3">
                                            <div class="form-group">
                                                <p class="label">{{trans('general.gn_gender')}} :</p>
                                                <p class="value">{{$mdata->adm_Gender}}</p>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-lg-3">
                                            <div class="form-group">
                                                <p class="label">{{trans('general.gn_dob')}} :</p>
                                                <p class="value">@if(!empty($mdata->adm_DOB)){{$mdata->adm_DOB}}@endif</p>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-lg-3">
                                            <div class="form-group">
                                                <p class="label">{{trans('general.gn_country')}} :</p>
                                                <p class="value">{{$mdata->country}}</p>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-lg-3">
                                            <div class="form-group">
                                                <p class="label">{{trans('general.gn_state')}} :</p>
                                                <p class="value">{{$mdata->state}}</p>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-lg-3">
                                            <div class="form-group">
                                                <p class="label">{{trans('general.gn_canton')}} :</p>
                                                <p class="value">{{$mdata->canton}}</p>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-lg-3">
                                            <div class="form-group">
                                                <p class="label">{{trans('general.gn_status')}} :</p>
                                                <p class="value">{{$mdata->adm_Status}}</p>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-lg-3">
                                            <div class="form-group">
                                                <p class="label">{{trans('general.gn_address')}} :</p>
                                                <p class="value">{{$mdata->adm_Address}}</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-1"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End Content Body -->
@endsection
