@extends('layout.app_with_login')
@section('title', trans('sidebar.sidebar_nav_school_year'))
@section('content')
<div class="section">
    <div class="container-fluid">
        <div class="row ">
            <div class="col-12 mb-3">
                <h2 class="title"><span>{{trans('sidebar.sidebar_nav_school_year')}} ></span> {{trans('general.gn_add_new')}}</h2>
            </div>
            <div class="col-12">
                <div class="white_box pt-3 pb-3">
                    <div class="container-fluid">
                        <form name="add-schoolYear-form" id="add-schoolYear-form">
                            <input type="hidden" id="pkSye" value="0">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="pl-5 pr-5">
                                        <div>
                                            <p class="mt-2"><strong>{{trans('sidebar.sidebar_nav_school_year')}}</strong></p>
                                        </div>
                                        <div class="bg-color-form">
                                            <div class="row">
                                                @foreach($languages as $k => $v)
                                                <div class="col-lg-6">
                                                <div class="form-group">
                                                    <label>{{trans('general.gn_name')}} {{$v->language_name}}*</label>
                                                    <input type="text" name="sye_NameCharacter_{{$v->language_key}}" id="sye_NameCharacter_{{$v->language_key}}" class="form-control force_require icon_control" required="">
                                                </div>
                                                </div>
                                                @endforeach
                                                <div class="col-lg-6">
                                                    <label>{{trans('sidebar.sidebar_nav_school_year')}} {{trans('general.gn_short_name')}} *</label>
                                                    <input  type="text" name="sye_ShortName" id="sye_ShortName" class="form-control icon_control">
                                                </div>
                                                <div class="col-lg-6">
                                                    <label>{{trans('sidebar.sidebar_nav_school_year')}} *</label>
                                                    <input  type="text" name="sye_NameNumeric" id="sye_NameNumeric" class="form-control icon_control datepicker-year">
                                                </div>
                                            </div>
                                            <div class="canton_add_element">
                                                <div class="profile_info_container">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                        <p class="mt-2">
                                                            <strong class="title_label">{{trans('general.gn_school_year_week_number')}}</strong>
                                                        </p>
                                                        <div class="table-responsive mt-2">
                                                            <table class="color_table weekhour_table">
                                                                <tr>
                                                                    <th width="10%">{{trans('general.sr_no')}}</th>
                                                                    <th width="60%">{{trans('general.gn_canton')}}</th>
                                                                    <th width="20%">{{trans('general.gn_week_number')}}</th>
                                                                    <th width="10%">{{trans('general.gn_action')}}</th>
                                                                </tr>
                                                                <tr class="main_weekhour">
                                                                    <td class="sr">1</td>
                                                                    <td>
                                                                    <select class="form-control icon_control dropdown_control ids_only select2drp" id="fkSywCan">
                                                                        <option value="">{{trans('general.gn_select')}}</option>
                                                                        @foreach($cantons as $k => $v)
                                                                            <option value="{{$v->pkCan}}">{{$v->can_CantonName}}, {{$v->state->sta_StateName}}</option>
                                                                        @endforeach
                                                                    </select>
                                                                    </td>
                                                                    <td>
                                                                        <input onkeyup="cleanWeeks(this)" class="form-control form-control-border" maxlength="2" type="text" id="sye_NumberofWeek">
                                                                    </td>
                                                                    <td>
                                                                        <a onclick="addWeek(this)" data-id="1" href="javascript:void(0)" class="addElemBtn theme_btn min_btn">
                                                                            {{trans('general.gn_add')}}
                                                                        </a>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="text-center">
                                        <button type="button" onclick="save()" class="theme_btn">{{trans('general.gn_submit')}}</button>
                                        <a class="theme_btn red_btn" href="{{url('/admin/schoolyear')}}">{{trans('general.gn_cancel')}}</a>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<input type="hidden" id="canton_validate_msg" value="{{trans('general.gn_canton_validate_msg')}}">
<input type="hidden" id="week_hour_validate_msg" value="{{trans('validation.validate_week_hours')}}">
<input type="hidden" id="canton_added_validate_msg" value="{{trans('general.gn_canton_added_msg')}}">
<input type="hidden" id="add_week_hrs_msg" value="{{trans('general.gn_add_week_hrs_msg')}}">
<script>
    var change_url = "{{route('change-schoolyear')}}";
</script>
@endsection
@push('datatable-scripts')
<!-- Include datatable Page JS -->
<script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
<script type="text/javascript" id="datatable_script" src="{{ asset('js/dashboard/schoolYear.js') }}"></script>
{!! JsValidator::formRequest(\App\Http\Requests\SchoolYearRequest::class, '#add-schoolYear-form'); !!}
@endpush