@extends('layout.app_with_login')
@section('title', trans('sidebar.sidebar_nav_school_year'))
@section('content')
<div class="section">
	<div class="container-fluid">
		<div class="row ">
            <div class="col-12 mb-3">
                <h2 class="title">
                    <span>
                    <a href="{{url('/admin/schoolyear')}}">
                        {{trans('sidebar.sidebar_nav_school_year')}}
                    </a> >
                    </span>
                    {{trans('general.gn_view_details')}}
                </h2>
            </div>
            <div class="col-12">
                <div class="white_box pt-3 pb-3">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-lg-12 ">
                                <div class="pl-5 pr-5">
                                    <div class="row">
                                        @foreach($languages as $k => $v)
                                        <div class="col-md-6">
                                            <p>{{trans('sidebar.sidebar_nav_school_year')}} ({{$v->language_name}}) : <label> {{$cdata['sye_NameCharacter_'.$v->language_key]}}</label></p>
                                        </div>
                                        @endforeach

                                        <div class="col-md-6 text-left">
                                        <p>{{trans('sidebar.sidebar_nav_school_year')}} : <label> {{$cdata->sye_NameNumeric}}</label></p>
                                        </div>

                                        <div class="col-md-6">
                                        <p>{{trans('sidebar.sidebar_nav_school_year')}} {{trans('general.gn_short_name')}} : <label> {{$cdata->sye_ShortName}}</label></p>
                                        </div>
                                    </div>
                                    <div class="profile_info_container">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <p class="mt-2"><strong class="title_label">{{trans('general.gn_school_year_week_number')}}</strong></p>
                                                <div class="table-responsive mt-2">
                                                    <table class="color_table">
                                                        <tbody>
                                                            <tr>
                                                                <th width="10%">{{trans('general.sr_no')}}</th>
                                                                <th width="70%">{{trans('general.gn_canton')}}</th>
                                                                <th width="20%">{{trans('general.gn_week_number')}}</th>
                                                            </tr>
                                                            <?php $m = 1;?>
                                                            @foreach($cdata->cantonweekNumbers as $k => $v)
                                                            <tr>
                                                                <td>{{$m}}</td>
                                                                <td>{{$v->cantons->can_CantonName}}, {{$v->cantons->state->sta_StateName}}</td>
                                                                <td>{{$v->sye_NumberofWeek}}</td>
                                                            </tr>
                                                            <?php $m++; ?>
                                                            @endforeach
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection