@extends('layout.app_with_login')
@section('title',trans('sidebar.sidebar_nav_dmt'))
@section('content')
 <!-- Page Content  -->
<div class="section">
    <div class="container-fluid">
        <div class="row ">
            <div class="col-12 mb-3">
                <h2 class="title"><span>{{trans('sidebar.sidebar_nav_student_masters')}} > </span>{{trans('sidebar.sidebar_nav_dmt')}}</h2>
            </div>
            <div class="col-md-4 mb-3">
                <input type="text" id="search_disciplineMeasureType" maxlength="30" class="form-control icon_control search_control" placeholder="{{trans('general.gn_search')}}">
            </div>
            <div class="col-md-4 text-md-right mb-3">

            </div>
            <div class="col-md-2 col-6 mb-3">

            </div>
            <div class="col-md-2 col-6 mb-3">
                <a><button class="theme_btn show_modal full_width small_btn" data-toggle="modal" data-target="#add_new">{{trans('general.gn_add_new')}}</button></a>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="theme_table">
                    <div class="table-responsive">
                        {{$dt_html->table(['class'=>"disciplineMeasureType_listing display responsive","width"=>"100%"],true)}}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Add New Popup -->
<div class="theme_modal modal fade" id="add_new" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <img src="{{asset('images/ic_close_bg.png')}}" class="modal_top_bg">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <img src="{{asset('images/ic_close_circle_white.png')}}">
                </button>
                <form name="add-disciplineMeasureType-form" id="add-disciplineMeasureType-form">
                    <div class="row">
                        <div class="col-lg-1"></div>
                        <div class="col-lg-10">
                            <h5 class="modal-title" id="exampleModalCenterTitle">{{trans('sidebar.sidebar_nav_dmt')}}</h5>
                            <div class="form-group">
                                <input type="hidden" name="smt_Uid" id="smt_Uid" value="SBE" class="form-control icon_control">
                                <input type="hidden" id="pkSmt">
                            </div>
                            @foreach($languages as $k => $v)
                                <div class="form-group">
                                    <label>{{trans('general.gn_name')}} {{$v->language_name}} *</label>
                                    <input type="text" name="smt_DisciplineMeasureName_{{$v->language_key}}" id="smt_DisciplineMeasureName_{{$v->language_key}}" class="form-control force_require icon_control" required="">
                                </div>
                            @endforeach
                            <div class="form-group">
                                <label>{{trans('general.gn_note')}}</label>
                                <input type="text" name="smt_Notes" id="smt_Notes" class="form-control icon_control">
                            </div>
                            <div class="text-center modal_btn ">
                                <button type="button" onclick="save()" class="theme_btn">{{trans('general.gn_submit')}}</button>
                            </div>
                        </div>
                        <div class="col-lg-1"></div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="theme_modal modal fade" id="delete_prompt" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <img src="{{asset('images/ic_close_bg.png')}}" class="modal_top_bg">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <img src="{{asset('images/ic_close_circle_white.png')}}">
                </button>
                    <div class="row">
                        <div class="col-lg-1"></div>
                        <div class="col-lg-10">
                            <h5 class="modal-title" id="exampleModalCenterTitle">{{trans('general.gn_delete')}}</h5>
                            <div class="form-group text-center">
                                <label>{{trans('general.gn_delete_prompt')}}</label>
                                <input type="hidden" id="did">
                            </div>
                            <div class="text-center modal_btn ">
                                <button style="display: none;" class="theme_btn show_delete_modal full_width small_btn" data-toggle="modal" data-target="#delete_prompt">{{trans('general.gn_delete')}}</button>
                                <button type="button" onclick="confirmDelete()" class="theme_btn">{{trans('general.gn_yes')}}</button>
                                <button type="button" data-dismiss="modal" class="theme_btn red_btn">{{trans('general.gn_no')}}</button>
                            </div>
                        </div>
                        <div class="col-lg-1"></div>
                    </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('datatable-scripts')
{!! $dt_html->scripts() !!}
<!-- Include datatable Page JS -->
<script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
<script type="text/javascript" id="datatable_script" src="{{ asset('js/dashboard/discipline_measure.js') }}"></script>
{!! JsValidator::formRequest(\App\Http\Requests\StudentDisciplineMeasureTypeRequest::class, '#add-disciplineMeasureType-form'); !!}
@endpush
