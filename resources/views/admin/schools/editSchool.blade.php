@extends('layout.app_with_login')
@section('title', trans('sidebar.sidebar_nav_school_management'))
@section('script', asset('js/dashboard/school.js'))
@section('content')
<!-- Page Content  -->
<div class="section">
    <div class="container-fluid">
        <div class="row ">
            <div class="col-12 mb-3">
                <h2 class="title"><span>@if(Auth::guard('admin')->user()->type=='HertronicAdmin') {{trans('sidebar.sidebar_nav_ministry_masters')}} > @endif </span><a class="no_sidebar_active" href="{{url('/admin/schools')}}"><span>{{trans('sidebar.sidebar_nav_school_management')}} > </span></a> {{trans('general.gn_edit')}}</h2>
            </div>
            <div class="col-12">
                <div class="white_box pt-3 pb-3">
                    <div class="container-fluid">
                        <form name="add-school-form" id="edit-school">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="pl-5 pr-5">
                                        <div class="bg-color-form">
                                            <div class="row">
                                                <input type="hidden" id="eid" value="{{$mdata->pkSch}}">
                                                @foreach($languages as $k => $v)
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <label>{{trans('general.gn_name')}} {{$v->language_name}} *</label>
                                                        <input type="text" name="sch_SchoolName_{{$v->language_key}}" id="sch_SchoolName_{{$v->language_key}}" class="form-control force_require icon_control" value="{{$mdata['sch_SchoolName_'.$v->language_key]}}" required="">
                                                    </div>
                                                </div>
                                                @endforeach
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>{{trans('general.gn_school')}} ID *</label>
                                                        <input type="text" name="sch_SchoolId" id="sch_SchoolId" class="form-control" value="{{$mdata->sch_SchoolId}}">
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>{{trans('general.gn_email')}} *</label>
                                                        <input type="text" name="sch_SchoolEmail" id="sch_SchoolEmail" value="{{$mdata->sch_SchoolEmail}}" class="form-control">
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>{{trans('general.gn_phone')}} *</label>
                                                        <input type="text" name="sch_PhoneNumber" id="sch_PhoneNumber" class="form-control" value="{{$mdata->sch_PhoneNumber}}">
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
    			                                    <div class="form-group">
    			                                        <label>{{trans('general.gn_ownership_type')}} *</label>
                                                        @include('partials.dropdown.dropdowns',[
                                                        'type'=>'ownership_type',
                                                        'name'=>'fkSchOty',
                                                        'selected'=>$mdata->fkSchOty,
                                                        'extra_params'=>[
                                                            "id"=>"fkSchOty"
                                                        ]
                                                        ])
    			                                    </div>
    			                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>{{trans('general.gn_founder')}} *</label>
                                                        <input type="text" name="sch_Founder" id="sch_Founder" value="{{$mdata->sch_Founder}}" class="form-control">
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>{{trans('general.gn_founding_date')}} *</label>
                                                        <input type="text" name="sch_FoundingDate" value="{{$mdata->sch_FoundingDate}}" id="sch_FoundingDate" class="form-control date_control datepicker icon_control">
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>{{trans('general.gn_running_semester')}} *</label>
                                                        @include('partials.dropdown.dropdowns',[
                                                        'type'=>'education_period',
                                                        'name'=>'fkSchEdp',
                                                        'selected'=>$mdata->fkSchEdp,
                                                        'extra_params'=>[
                                                            "id"=>"fkSchEdp"
                                                        ]
                                                        ])
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <label>{{trans('general.gn_address')}} *</label>
                                                        <input type="text" id="sch_Address" name="sch_Address" class="form-control" value="{{$mdata->sch_Address}}">
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <label>{{trans('general.gn_ministry_license_number')}} *</label>
                                                        <input type="text" name="sch_MinistryApprovalCertificate" id="sch_MinistryApprovalCertificate" class="form-control" value="{{$mdata->sch_MinistryApprovalCertificate}}">
                                                    </div>
                                                </div>

                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <label>{{trans('sidebar.sidebar_nav_postal_code')}} *</label>
                                                        <select class="form-control icon_control dropdown_control valid" name="fkSchPof" id="fkSchPof" required="">
                                                            <option value="">{{trans('general.gn_select')}}</option>
                                                            @if(!empty($postOffice))
                                                            @foreach($postOffice as $key => $val)
                                                            <option @if($val['pkPof']==$mdata->fkSchPof) selected="selected" @endif value="{{$val['pkPof']}}">{{$val['pof_PostOfficeNumber']}}</option>
                                                            @endforeach
                                                            @endif
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="bg-color-form">
                                            <div class="row">
                                                <div class="col-lg-4">
                                                    <div class="form-group">
                                                        <label>{{trans('sidebar.sidebar_nav_education_program')}}</label>
                                                        <select onchange="fetchEPlan(this.value)" name="fkEplEdp" id="fkEplEdp" class="form-control icon_control dropdown_control">
                                                            <option value="">{{trans('general.gn_select')}}</option>
                                                            @foreach($educationProgram as $tkey => $tvalue)
                                                            <option value="{{$tvalue['pkEdp']}}">{{$tvalue['edp_Name']}}</option>
                                                            {{-- Sub Parent --}}
                                                            @foreach($tvalue['children'] as $ckey=> $cValue)
                                                            @if($cValue['edp_ParentId']==$tvalue['pkEdp'])
                                                            <option value="{{$cValue['pkEdp']}}">&emsp;&emsp;{{$cValue['edp_Name']}}</option>
                                                            {{-- Sub Sub Parent --}}
                                                            @foreach($cValue['children'] as $cckey=> $ccValue)
                                                            @if($ccValue['edp_ParentId']==$cValue['pkEdp'])
                                                            <option value="{{$ccValue['pkEdp']}}">&emsp;&emsp;&emsp;&emsp;{{$ccValue['edp_Name']}}</option>
                                                            {{-- Sub Sub Sub Parent --}}
                                                            @foreach($ccValue['children'] as $skey=> $sValue)
                                                            @if($sValue['edp_ParentId']==$ccValue['pkEdp'])
                                                            <option value="{{$sValue['pkEdp']}}">&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;{{$sValue['edp_Name']}}</option>
                                                            {{-- Sub Sub Sub Sub Parent --}}
                                                            @foreach($sValue['children'] as $sskey=> $ssValue)
                                                            @if($ssValue['edp_ParentId']==$sValue['pkEdp'])
                                                            <option value="{{$ssValue['pkEdp']}}">&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;{{$ssValue['edp_Name']}}</option>
                                                            @endif
                                                            @endforeach
                                                            @endif
                                                            @endforeach
                                                            @endif
                                                            @endforeach
                                                            @endif
                                                            @endforeach
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-lg-4">
                                                    <div class="form-group">
                                                        <label>{{trans('general.gn_education_plan')}} </label>
                                                        <select name="eplan" id="eplan" class="form-control icon_control dropdown_control">
                                                            <option value="">{{trans('general.gn_select')}}</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-lg-4">
                                                    <div class="form-group visibility_hidden">sdf</div>
                                                    <div class="form-group">
                                                        <button onclick="addPlan()" type="button" class="theme_btn min_btn">{{trans('general.gn_add')}}</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="table-responsive mt-2">
                                                <table class="color_table school_plan_table">
                                                    <tr>
                                                        <th width="6%">{{trans('general.sr_no')}}</th>
                                                        <th width="15%">{{trans('general.gn_parent_category')}}</th>
                                                        <th width="12%">{{trans('general.gn_child_category')}}</th>
                                                        <th width="15%">{{trans('general.gn_education_plan')}}</th>
                                                        <th width="15%">{{trans('general.gn_national_education_plan')}}</th>
                                                        <th width="12%">{{trans('general.gn_qualification_degree')}}</th>
                                                        <th width="12%">{{trans('general.gn_education_profile')}}</th>
                                                        <th width="10%">{{trans('general.gn_action')}}</th>
                                                    </tr>
                                                    @foreach($mdata->schoolEducationPlanAssignment as $k => $v)
                                                    <tr class="sch sch_{{$v->educationPlan->pkEpl}} ">
                                                        <td>{{$k+1}}</td>
                                                        <td>@if($v->educationProgram->edp_ParentId == 0) - @else {{$v->educationProgram->parent['edp_Name_'.$current_language]}} @endif</td>
                                                        <td>{{$v->educationProgram['edp_Name_'.$current_language]}}</td>
                                                        <td>{{$v->educationPlan['epl_EducationPlanName_'.$current_language]}}</td>
                                                        <td>{{$v->educationPlan->nationalEducationPlan['nep_NationalEducationPlanName_'.$current_language]}}</td>
                                                        <td>{{$v->educationPlan->QualificationDegree['qde_QualificationDegreeName_'.$current_language]}}</td>
                                                        <td>{{$v->educationPlan->educationProfile['epr_EducationProfileName_'.$current_language]}}</td>
                                                        <td>
                                                            <div class="form-group form-check"><input type="checkbox" class="form-check-input" name="sep_Status[]" value="{{$v->educationPlan->pkEpl}}" id="exampleCheck{{$v->educationPlan->pkEpl}}" @if($v->sep_Status == 'Active') checked="" @endif><label class="custom_checkbox"></label><label class="form-check-label label-text" for="exampleCheck{{$v->educationPlan->pkEpl}}"><strong></strong></label><a target="_blank" href="{{url('admin/educationplan/')}}/{{$v->educationPlan->pkEpl}}"><i class="fa fa-info-circle" aria-hidden="true"></i></a></div>
                                                            <a data-id="{{$v->educationPlan->pkEpl}}" onclick="removePlan(this)" href="javascript:void(0)"><i style="color:red" class="fa fa-trash" aria-hidden="true"></i></a>
                                                        </td>
                                                    </tr>
                                                    @endforeach
                                                </table>
                                            </div>
                                        </div>
                                        <div class="bg-color-form" id="view_sc">
                                            <div class="row">
                                                <div class="col-12">
                                                    <h6 class="profile_inner_title">{{trans('general.gn_school_coordinator')}}</h6>
                                                </div>
                                                <div class="col-md-6 col-lg-4">
                                                    <div class="form-group">
                                                        <p class="label">{{trans('general.gn_name')}} :</p>
                                                        <p class="value">{{$mdata->employeesEngagement[0]->employee->emp_EmployeeName ?? ''}} {{$mdata->employeesEngagement[0]->employee->emp_EmployeeSurname ?? ''}}</p>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-lg-4">
                                                    <div class="form-group">
                                                        <p class="label">{{trans('general.gn_email')}} :</p>
                                                        <p class="value">{{$mdata->employeesEngagement[0]->employee->email ?? ''}}</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="text-center">
                                                <button type="button" class="theme_btn" id="edit_profile">{{ trans('general.gn_edit')}} {{ trans('general.gn_school_coordinator')}}</button>
                                            </div>
                                        </div>
                                        <div class="profile_info_container" id="edit_sc" style="display: none;">
                                            <div class="row">
                                                <div class="col-12">
                                                    <h6 class="profile_inner_title">{{trans('general.gn_school_coordinator')}}</h6>
                                                </div>
                                                @php
                                                $fkEenEmp = 0;
                                                $fkEenEpty = 0;
                                                $fkEenSch = 0;
                                                $start_date = '';
                                                @endphp
                                                @foreach($mdata->employeesEngagement as $k => $v)
                                                @if($v['een_DateOfFinishEngagement'] == null && $v['fkEenEpty'] == 2)
                                                @php
                                                $fkEenEmp = $v['fkEenEmp'];
                                                $fkEenEpty = $v['fkEenEpty'];
                                                $fkEenSch = $v['fkEenSch'];
                                                @endphp
                                                @endif
                                                @if($v['fkEenSch'] == $school_id && $v['een_DateOfFinishEngagement'] == null && $v['fkEenEpty'] == 2)
                                                @php
                                                $start_date = $v['een_DateOfEngagement'];
                                                @endphp
                                                @endif
                                                @endforeach
                                                <div class="col-md-6 ">
                                                    <div class="form-group">
                                                        <label>{{trans('general.gn_start_date')}} *</label>
                                                        <input type="text" id="start_date" name="start_date" class="form-control icon_control date_control start_date" value="{{$start_date}}">
                                                    </div>
                                                </div>
                                                <div class="col-md-6 ">
                                                    <div class="form-group">
                                                        <label>{{trans('general.gn_end_date')}}</label>
                                                        <input type="text" id="end_date" name="end_date" class="form-control icon_control date_control end_date">
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>{{trans('general.gn_employees')}} *</label>
                                                        @include('partials.dropdown.dropdowns',[
                                                        'type'=>'school_with_canton_employees',
                                                        'name'=>'sc_id',
                                                        'selected'=>$mdata->employeesEngagement[0]->fkEenEmp,
                                                        'extra_params'=>[
                                                        "id"=>"sc_id",
                                                        "cantonId"=>$cantonId,
                                                        "school_id"=>$school_id
                                                        ]
                                                        ])
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="text-center">
                                            <button type="submit" class="theme_btn">{{trans('general.gn_submit')}}</button>
                                            <a class="theme_btn red_btn no_sidebar_active" href="{{url('/admin/schools')}}">{{trans('general.gn_cancel')}}</a>
                                        </div>
                                    </div>
                                </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<input type="hidden" id="education_plan_validate_txt" value="{{trans('validation.validate_education_plan')}}">
<input type="hidden" id="education_plan_add_validate_txt" value="{{trans('message.education_plan_already_added')}}">
<script type="text/javascript">
    var listing_url = "{{route('fetch-schools-lists')}}";
    var fetch_edu_url = "{{route('fetch-educationplan-school')}}";
    var SP = [];
    <?php
    foreach($mdata -> schoolEducationPlanAssignment as $k => $v) {
        ?>
        SP.push('{{$v->educationPlan->pkEpl}}');
        <?php
    } ?>
</script>
<!-- End Content Body -->
@endsection
@push('datatable-scripts')
<!-- Include this Page JS -->
<script type="text/javascript" src="{{ asset('js/dashboard/school.js') }}"></script>
@endpush
