@extends('layout.app_with_login')
@section('title', trans('sidebar.sidebar_nav_school_management'))
@section('content')
 <!-- Page Content  -->
<div class="section">
	<div class="container-fluid">
		<div class="row ">
            <div class="col-12 mb-3">
                <h2 class="title"><span>@if(Auth::guard('admin')->user()->type=='HertronicAdmin') {{trans('sidebar.sidebar_nav_ministry_masters')}} > @endif </span> <a class="no_sidebar_active" href="{{url('/admin/schools')}}"><span>{{trans('sidebar.sidebar_nav_school_management')}} > </span></a> {{trans('general.gn_view_details')}}</h2>
            </div>
            <div class="col-12">
                <div class="white_box pt-3 pb-3">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-lg-12 ">
                                <div class="pl-5 pr-5">
                                    <div class="row">
                                        @foreach($languages as $k => $v)
                                        <div class="col-md-6">
                                            <p>{{trans('general.gn_school')}} {{trans('general.gn_name')}} ({{$v->language_name}}) : <label> {{$mdata['sch_SchoolName_'.$v->language_key]}}</label></p>
                                        </div>
                                        @endforeach

                                        <div class="col-md-6">
                                            <p>{{trans('general.gn_ministry_license_number')}} : <label> {{$mdata->sch_MinistryApprovalCertificate}}</label></p>
                                        </div>

                                        <div class="col-md-6">
                                            <p>{{trans('sidebar.sidebar_nav_postal_code')}} : <label> {{$mdata->postalCode->pof_PostOfficeNumber ?? ''}} - {{$mdata->postalCode->pof_PostOfficeName ?? ''}}</label></p>
                                        </div>

                                        <div class="col-md-6">
                                            <p>{{trans('general.gn_email')}} : <label> {{$mdata->sch_SchoolEmail ?? ''}}</label></p>
                                        </div>

                                        <div class="col-md-6">
                                            <p>{{trans('general.gn_phone')}} : <label> {{$mdata->sch_PhoneNumber ?? ''}}</label></p>
                                        </div>

                                        <div class="col-md-6">
                                            <p>{{trans('general.gn_founder')}} : <label> {{$mdata->sch_Founder ?? ''}}</label></p>
                                        </div>

                                        <div class="col-md-6">
                                            <p>{{trans('general.gn_founding_date')}} : <label> {{$mdata->sch_FoundingDate ?? ''}}</label></p>
                                        </div>

                                        <div class="col-md-6">
                                            <p>{{trans('general.gn_address')}} : <label> {{$mdata->sch_Address ?? ''}}</label></p>
                                        </div>

                                        <div class="col-md-6">
                                            <p>{{trans('general.gn_running_semester')}} : <label> {{$mdata->educationperiod->edp_EducationPeriodName ?? ''}}</label></p>
                                        </div>

                                        <div class="bg-color-form col-md-12">
                                            <p class="mt-2"><strong>{{trans('general.gn_education_plans_n_programs')}}</strong></p>
                                            <div class="table-responsive mt-2">
                                                <table class="color_table">
                                                    <tr>
                                                        <th width="6%">{{trans('general.sr_no')}}</th>
                                                        <th width="15%">{{trans('general.gn_parent_category')}}</th>
                                                        <th width="12%">{{trans('general.gn_child_category')}}</th>
                                                        <th width="15%">{{trans('general.gn_education_plan')}}</th>
                                                        <th width="15%">{{trans('general.gn_national_education_plan')}}</th>
                                                        <th width="12%">{{trans('general.gn_qualification_degree')}}</th>
                                                        <th width="12%">{{trans('general.gn_education_profile')}}</th>
                                                        <th width="10%">{{trans('general.gn_action')}}</th>
                                                    </tr>
                                                    @foreach($mdata->schoolEducationPlanAssignment as $k => $v)
                                                        <tr class="sch sch_{{$v->educationPlan->pkEpl}} ">
                                                            <td>{{$k+1}}</td>
                                                            <td>@if($v->educationProgram->edp_ParentId == 0) - @else {{$v->educationProgram->parent['edp_Name_'.$current_language]}} @endif</td>
                                                            <td>{{$v->educationProgram['edp_Name_'.$current_language]}}</td>
                                                            <td>{{$v->educationPlan['epl_EducationPlanName_'.$current_language]}}</td>
                                                            <td>{{$v->educationPlan->nationalEducationPlan['nep_NationalEducationPlanName_'.$current_language]}}</td>
                                                            <td>{{$v->educationPlan->QualificationDegree['qde_QualificationDegreeName_'.$current_language]}}</td>
                                                            <td>{{$v->educationPlan->educationProfile['epr_EducationProfileName_'.$current_language]}}</td>
                                                            <td><a target="_blank" href="{{url('admin/educationplan/')}}/{{$v->educationPlan->pkEpl}}"><i class="fa fa-info-circle" aria-hidden="true"></i></a></td>
                                                        </tr>
                                                    @endforeach
                                                </table>
                                            </div>
                                        </div>
                                        <div class="bg-color-form col-md-12">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <p>{{trans('general.gn_school_coordinator')}} {{trans('general.gn_name')}} : <label> {{$mdata->employeesEngagement[0]->employee->emp_EmployeeName ?? ''}} {{$mdata->employeesEngagement[0]->employee->emp_EmployeeSurname ?? ''}}</label></p>
                                                </div>
                                                <div class="col-md-6">
                                                    <p>{{trans('general.gn_school_coordinator')}} {{trans('general.gn_email')}} : <label> {{$mdata->employeesEngagement[0]->employee->email ?? ''}}</label></p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End Content Body -->
@endsection