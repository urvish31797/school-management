@extends('layout.app_with_login')
@section('title', trans('sidebar.sidebar_nav_school_management'))
@section('script', asset('js/dashboard/school.js'))
@section('content')
<!-- Page Content  -->
<div class="section">
    <div class="container-fluid">
        <div class="row ">
            <div class="col-12 mb-3">
                <h2 class="title"><span>{{trans('sidebar.sidebar_nav_school_management')}} > </span>{{trans('general.gn_add_new')}}</h2>
            </div>
            <div class="col-12">
                <div class="white_box pt-3 pb-3">
                    <div class="container-fluid">
                        <form name="add-school-form">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="pl-5 pr-5">
                                        <div class="bg-color-form">
                                            <div class="row">
                                                @foreach($languages as $k => $v)
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <label>{{trans('general.gn_name')}} {{$v->language_name}} *</label>
                                                        <input type="text" name="sch_SchoolName_{{$v->language_key}}" id="sch_SchoolName_{{$v->language_key}}" class="form-control force_require icon_control" required="">
                                                    </div>
                                                </div>
                                                @endforeach
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>{{trans('general.gn_school')}} ID *</label>
                                                        <input type="text" name="sch_SchoolId" id="sch_SchoolId" class="form-control">
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>{{trans('general.gn_email')}} *</label>
                                                        <input type="text" name="sch_SchoolEmail" id="sch_SchoolEmail" class="form-control">
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>{{trans('general.gn_phone')}} *</label>
                                                        <input type="text" name="sch_PhoneNumber" id="sch_PhoneNumber" class="form-control">
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
    			                                    <div class="form-group">
    			                                        <label>{{trans('general.gn_ownership_type')}} *</label>
                                                        @include('partials.dropdown.dropdowns',[
                                                        'type'=>'ownership_type',
                                                        'name'=>'fkSchOty',
                                                        'selected'=>'',
                                                        'extra_params'=>[
                                                            "id"=>"fkSchOty"
                                                        ]
                                                        ])
    			                                    </div>
    			                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>{{trans('general.gn_founder')}} *</label>
                                                        <input type="text" name="sch_Founder" id="sch_Founder" class="form-control">
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>{{trans('general.gn_founding_date')}} *</label>
                                                        <input type="text" name="sch_FoundingDate" id="sch_FoundingDate" class="form-control date_control datepicker icon_control">
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>{{trans('general.gn_running_semester')}} *</label>
                                                        @include('partials.dropdown.dropdowns',[
                                                        'type'=>'education_period',
                                                        'name'=>'fkSchEdp',
                                                        'selected'=>'',
                                                        'extra_params'=>[
                                                            "id"=>"fkSchEdp"
                                                        ]
                                                        ])
                                                    </div>
                                                    </div>
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <label>{{trans('general.gn_ministry_license_number')}} *</label>
                                                        <input type="text" name="sch_MinistryApprovalCertificate" id="sch_MinistryApprovalCertificate" class="form-control">
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <label>{{trans('general.gn_address')}} *</label>
                                                        <input type="text" id="sch_Address" name="sch_Address" class="form-control">
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <label>{{trans('sidebar.sidebar_nav_postal_code')}} *</label>
                                                        <select class="form-control icon_control dropdown_control valid" name="fkSchPof" id="fkSchPof" required="">
                                                            <option value="">{{trans('general.gn_select')}}</option>
                                                            @if(!empty($postOffice))
                                                            @foreach($postOffice as $key => $val)
                                                            <option value="{{$val['pkPof']}}">{{$val['pof_PostOfficeNumber']}}</option>
                                                            @endforeach
                                                            @endif
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="bg-color-form">
                                            <div class="row">
                                                <div class="col-lg-4">
                                                    <div class="form-group">
                                                        <label>{{trans('sidebar.sidebar_nav_education_program')}}</label>
                                                        <select onchange="fetchEPlan(this.value)" name="fkEplEdp" id="fkEplEdp" class="form-control icon_control dropdown_control">
                                                            <option value="">{{trans('general.gn_select')}}</option>
                                                            @foreach($educationProgram as $tkey => $tvalue)
                                                            <option value="{{$tvalue['pkEdp']}}">{{$tvalue['edp_Name']}}</option>
                                                            {{-- Sub Parent --}}
                                                            @foreach($tvalue['children'] as $ckey=> $cValue)
                                                            @if($cValue['edp_ParentId']==$tvalue['pkEdp'])
                                                            <option value="{{$cValue['pkEdp']}}">&emsp;&emsp;{{$cValue['edp_Name']}}</option>
                                                            {{-- Sub Sub Parent --}}
                                                            @foreach($cValue['children'] as $cckey=> $ccValue)
                                                            @if($ccValue['edp_ParentId']==$cValue['pkEdp'])
                                                            <option value="{{$ccValue['pkEdp']}}">&emsp;&emsp;&emsp;&emsp;{{$ccValue['edp_Name']}}</option>
                                                            {{-- Sub Sub Sub Parent --}}
                                                            @foreach($ccValue['children'] as $skey=> $sValue)
                                                            @if($sValue['edp_ParentId']==$ccValue['pkEdp'])
                                                            <option value="{{$sValue['pkEdp']}}">&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;{{$sValue['edp_Name']}}</option>
                                                            {{-- Sub Sub Sub Sub Parent --}}
                                                            @foreach($sValue['children'] as $sskey=> $ssValue)
                                                            @if($ssValue['edp_ParentId']==$sValue['pkEdp'])
                                                            <option value="{{$ssValue['pkEdp']}}">&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;{{$ssValue['edp_Name']}}</option>
                                                            @endif
                                                            @endforeach
                                                            @endif
                                                            @endforeach
                                                            @endif
                                                            @endforeach
                                                            @endif
                                                            @endforeach
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-lg-4">
                                                    <div class="form-group">
                                                        <label>{{trans('general.gn_education_plan')}} </label>
                                                        <select name="eplan" id="eplan" class="form-control icon_control dropdown_control">
                                                            <option value="">{{trans('general.gn_select')}}</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-lg-4">
                                                    <div class="form-group visibility_hidden">sdf</div>
                                                    <div class="form-group">
                                                        <button onclick="addPlan()" type="button" class="theme_btn min_btn">{{trans('general.gn_add')}}</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="table-responsive mt-2">
                                                <table class="color_table school_plan_table" style="display: none;">
                                                    <tr>
                                                        <th width="6%">{{trans('general.sr_no')}}</th>
                                                        <th width="15%">{{trans('general.gn_parent_category')}}</th>
                                                        <th width="12%">{{trans('general.gn_child_category')}}</th>
                                                        <th width="15%">{{trans('general.gn_education_plan')}}</th>
                                                        <th width="15%">{{trans('general.gn_national_education_plan')}}</th>
                                                        <th width="12%">{{trans('general.gn_qualification_degree')}}</th>
                                                        <th width="12%">{{trans('general.gn_education_profile')}}</th>
                                                        <th width="10%">{{trans('general.gn_action')}}</th>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                        <div class="bg-color-form">
                                            <div class="container-fluid">
                                                <div class="row">
                                                    <div class="col-12">
                                                        <h6 class="profile_inner_title">{{trans('general.gn_school_coordinator')}}</h6>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label>{{trans('general.gn_select_existing_employee')}} ? : *</label>
                                                            <div class="form-check custom_check_div">
                                                                <input class="form-check-input" type="radio" name="sel_exists_employee" id="Customer1" value="Yes">
                                                                <label class="custom_radio"></label>
                                                                <label class="form-check-label" for="Customer1">{{trans('general.gn_yes')}}</label>
                                                            </div>
                                                            <div class="form-check custom_check_div">
                                                                <input class="form-check-input" type="radio" name="sel_exists_employee" checked="checked" id="Customer1" value="No">
                                                                <label class="custom_radio"></label>
                                                                <label class="form-check-label" for="Customer1">{{trans('general.gn_no')}}</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 add_new_sc">
                                                        <div class="form-group">
                                                            <label>{{trans('general.gn_first_name')}} *</label>
                                                            <input type="text" id="emp_EmployeeName" name="emp_EmployeeName" class="form-control" placeholder="{{trans('general.gn_enter')}} {{trans('general.gn_first_name')}}">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 add_new_sc">
                                                        <div class="form-group">
                                                            <label>{{trans('general.gn_last_name')}} *</label>
                                                            <input type="text" id="emp_EmployeeSurname" name="emp_EmployeeSurname" class="form-control" placeholder="{{trans('general.gn_enter')}} {{trans('general.gn_last_name')}}">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 add_new_sc">
                                                        <div class="form-group">
                                                            <label>{{trans('general.gn_school_coordinator')}} {{trans('general.gn_email')}} *</label>
                                                            <input type="text" id="sch_CoordEmail" name="sch_CoordEmail" class="form-control" value="">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 add_new_sc">
                                                        <div class="form-group">
                                                            <label>{{trans('general.gn_phone')}} *</label>
                                                            <input type="number" name="emp_PhoneNumber" id="emp_PhoneNumber" class="form-control" placeholder="{{trans('general.gn_enter')}} {{trans('general.gn_phone_number')}}">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 add_new_sc">
                                                        <div class="form-group">
                                                            <label>{{trans('general.gn_gender')}}</label>
                                                            <select onchange="checkEmployeeID()" name="emp_EmployeeGender" id="emp_EmployeeGender" class="form-control icon_control dropdown_control">
                                                                <option value="Male">{{trans('general.gn_male')}}</option>
                                                                <option value="Female">{{trans('general.gn_female')}}</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 add_new_sc">
                                                        <div class="form-group">
                                                            <label>{{trans('general.gn_dob')}} *</label>
                                                            <input onchange="checkEmployeeID()" type="text" name="emp_DateOfBirth" id="emp_DateOfBirth" class="form-control icon_control date_control datepicker">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 add_new_sc opt_id">
                                                        <div class="form-group">
                                                            <label>{{trans('general.gn_employee')}} ID *</label>
                                                            <input onfocusout="checkEmployeeID()" type="text" name="emp_EmployeeID" id="emp_EmployeeID" class="form-control numbersonly isValidEmpID" placeholder="{{trans('general.gn_enter')}} ID" maxlength="13">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 opt_tmp_id hide_content add_new_sc">
                                                        <div class="form-group">
                                                            <label>{{trans('general.gn_temp_citizen_id')}} </label>
                                                            <input required="required" type="text" name="emp_TempCitizenId" id="emp_TempCitizenId" class="form-control numbersonly isValidTempID" placeholder="{{trans('general.gn_enter')}} {{trans('general.gn_temp_citizen_id')}}" maxlength="13">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 add_new_sc">
                                                        <div class="form-group">
                                                            <label>{{trans('general.gn_MunicipalityofBirth')}} *</label>
                                                            <select name="fkEmpMun" id="fkEmpMun" class="form-control icon_control dropdown_control select2drp">
                                                                <option value="">{{trans('general.gn_select')}}</option>
                                                                @foreach($Municipalities as $k => $v)
                                                                <option data-countryId="{{$v->canton->state->country->pkCny}}" value="{{$v->pkMun}}">{{$v->mun_MunicipalityName}}, {{$v->canton->state->country->cny_CountryName}}</option>
                                                                @endforeach
                                                            </select>
                                                            <input type="hidden" id="fkEmpCny" name="fkEmpCny">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 add_new_sc">
                                                        <div class="form-group form-check ">
                                                            <input type="checkbox" class="form-check-input" id="havent_identification_number">
                                                            <label class="custom_checkbox"></label>
                                                            <label class="form-check-label label-text" for="exampleCheck1">{{trans('general.gn_havent_identification_number')}}</label>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 add_new_sc">
                                                        <div class="form-group">
                                                            <label>{{trans('general.gn_nationality')}} *</label>
                                                            <select name="fkEmpNat" id="fkEmpNat" class="form-control icon_control dropdown_control select2drp">
                                                                <option value="">{{trans('general.gn_select')}}</option>
                                                                @foreach($Nationalities as $k => $v)
                                                                <option value="{{$v->pkNat}}">{{$v->nat_NationalityName}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 add_new_sc">
                                                        <div class="form-group">
                                                            <label>{{trans('general.gn_religion')}} *</label>
                                                            <select name="fkEmpRel" id="fkEmpRel" class="form-control icon_control dropdown_control select2drp">
                                                                <option value="">{{trans('general.gn_select')}}</option>
                                                                @foreach($Religions as $k => $v)
                                                                <option value="{{$v->pkRel}}">{{$v->rel_ReligionName}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 add_new_sc">
                                                        <div class="form-group">
                                                            <label>{{trans('general.gn_citizenship')}} *</label>
                                                            <select name="fkEmpCtz" id="fkEmpCtz" class="form-control icon_control dropdown_control select2drp">
                                                                <option value="">{{trans('general.gn_select')}}</option>
                                                                @foreach($Citizenships as $k => $v)
                                                                <option value="{{$v->pkCtz}}">{{$v->ctz_CitizenshipName}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 add_new_sc">
                                                        <div class="form-group">
                                                            <label>{{trans('general.gn_ResidencePostoffice')}} *</label>
                                                            <select name="fkEmpPof" id="fkEmpPof" class="form-control icon_control dropdown_control select2drp">
                                                                <option value="">{{trans('general.gn_select')}}</option>
                                                                @foreach($PostalCodes as $k => $value)
                                                                <option value="{{$value->pkPof}}">{{$value->pof_PostOfficeName}} {{$value->pof_PostOfficeNumber}} ({{$value->municipality->mun_MunicipalityName}}, {{$value->municipality->canton->state->country->cny_CountryName}})</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 ">
                                                        <div class="form-group">
                                                            <label>{{trans('general.gn_start_date')}} *</label>
                                                            <input type="text" id="start_date" name="start_date" class="form-control icon_control date_control start_date" value="">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 exists_employee" style="display: none;">
                                                        <div class="form-group">
                                                            <label>{{trans('general.gn_employees')}} *</label>
                                                            @include('partials.dropdown.dropdowns',[
                                                            'type'=>'canton_employees',
                                                            'name'=>'sc_id',
                                                            'selected'=>'',
                                                            'extra_params'=>[
                                                            "id"=>"sc_id",
                                                            "cantonId"=>$cantonId
                                                            ]
                                                            ])
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label>{{trans('general.gn_week_hourly_rate')}} *</label>
                                                            <input required type="text" id="ewh_WeeklyHoursRate" name="ewh_WeeklyHoursRate" class="form-control">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label>{{trans('general.gn_hourly_note')}} </label>
                                                            <input type="text" name="ewh_Notes" id="ewh_Notes" class="form-control">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 add_new_sc">
                                                        <div class="form-group">
                                                            <label>{{trans('general.gn_enagement_note')}} </label>
                                                            <input type="text" id="een_notes" name="een_notes" class="form-control">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="text-center">
                                            <button type="submit" class="theme_btn">{{trans('general.gn_submit')}}</button>
                                            <a class="theme_btn red_btn no_sidebar_active" href="{{url('/admin/schools')}}">{{trans('general.gn_cancel')}}</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<input type="hidden" id="education_plan_validate_txt" value="{{trans('validation.validate_education_plan')}}">
<input type="hidden" id="education_plan_add_validate_txt" value="{{trans('message.education_plan_already_added')}}">
<input type="hidden" id="employeemsgerror" value="{{trans('message.msg_employee_id_incorrect')}}">
<!-- End Content Body -->
<script>
    var listing_url = "{{route('fetch-schools-lists')}}";
    var fetch_edu_url = "{{route('fetch-educationplan-school')}}";
    var SP = [];

</script>
@endsection
@push('datatable-scripts')
<!-- Include this Page JS -->
<script type="text/javascript" src="{{ asset('js/dashboard/school.js') }}"></script>
@endpush
