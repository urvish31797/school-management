<nav id="sidebar" class="">
    <div class="sidebar-header">
        <h3>Hertronic</h3>
    </div>

    <ul class="list-unstyled components slim_scroll">
        <li class="{{ (request()->is('admin/dashboard')) ? 'active' : '' }}">
            <a href="{{url('/admin/dashboard')}}">
                <img alt="" src="{{ asset('images/ic_dashoard_color.png') }}" class="color">
                <img alt="" src="{{ asset('images/ic_dashoard.png') }}" class="selected">
                {{trans('sidebar.sidebar_nav_dasbhoard')}}
            </a>
        </li>
        <li class="{{ (request()->is('admin/ministries*')) || (request()->is('admin/loginas*')) ? 'active' : '' }}">
            <a href="#pageSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                <img alt="" src="{{ asset('images/ic_menu_color.png') }}" class="color">
                <img alt="" src="{{ asset('images/ic_menu.png') }}" class="selected">
                {{trans('sidebar.sidebar_nav_user_management')}}
                <i class="cfa fas fa-chevron-down right-arrow"></i>
            </a>
            <ul class="collapse list-unstyled {{ (request()->is('admin/ministries*')) || (request()->is('admin/loginas*')) ? 'show' : '' }}" id="pageSubmenu">
                <li class="{{ (request()->is('admin/ministries*')) ? 'active' : '' }}">
                    <a href="{{route('ministries.index')}}">{{trans('sidebar.sidebar_nav_ministry_super_admin')}}</a>
                </li>
                <li class="{{ (request()->is('admin/loginas*')) ? 'active' : '' }}">
                    <a href="{{route('loginas.index')}}">{{trans('sidebar.sidebar_nav_login_as')}}</a>
                </li>
            </ul>
        </li>
        <li class="{{ (request()->is('admin/academicdegree*')) || (request()->is('admin/jobandwork*')) || (request()->is('admin/country*')) || (request()->is('admin/state*')) || (request()->is('admin/canton*')) || (request()->is('admin/municipalities*')) || (request()->is('admin/postalcodes*')) || (request()->is('admin/ownershiptypes*')) || (request()->is('admin/universities*')) || (request()->is('admin/colleges*')) || (request()->is('admin/grades*')) || (request()->is('admin/classes*')) || (request()->is('admin/nationalities*')) || (request()->is('admin/religions*')) || (request()->is('admin/citizenships*')) || (request()->is('admin/vocations*')) || (request()->is('admin/educationperiod*')) || (request()->is('admin/courses*')) || (request()->is('admin/descriptivefinalmarks*')) || (request()->is('admin/engagementtypes*')) || (request()->is('admin/shifts*')) || (request()->is('admin/nationaleducationplans*')) || (request()->is('admin/educationprofiles*')) || (request()->is('admin/qualificationdegrees*')) || (request()->is('admin/gradeattemptsnumber*')) || (request()->is('admin/certificatetype*')) || (request()->is('admin/markexplanation*')) || (request()->is('admin/certificateblueprints*')) || (request()->is('admin/schoolyear*')) || (request()->is('admin/schoolyear*')) || (request()->is('admin/attendanceway*')) || (request()->is('admin/educationway*')) || (request()->is('admin/periodicexamway*')) || (request()->is('admin/descriptiveperiodicexammark*')) || (request()->is('admin/working-weeks*')) || (request()->is('admin/periodic-exam-type*')) ? 'active' : '' }}">
            <a href="#pageSubmenu1" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                <img alt="" src="{{ asset('images/ic_cart_color.png') }}" class="color">
                <img alt="" src="{{ asset('images/ic_cart.png') }}" class="selected">
                {{trans('sidebar.sidebar_nav_masters')}}
                <i class="cfa fas fa-chevron-down right-arrow"></i>
            </a>
            <ul class="collapse list-unstyled {{ (request()->is('admin/academicdegree*')) || (request()->is('admin/jobandwork*')) || (request()->is('admin/country*')) || (request()->is('admin/state*')) || (request()->is('admin/canton*')) || (request()->is('admin/municipalities*')) || (request()->is('admin/postalcodes*')) || (request()->is('admin/ownershiptypes*')) || (request()->is('admin/universities*')) || (request()->is('admin/colleges*')) || (request()->is('admin/grades*')) || (request()->is('admin/classes*')) || (request()->is('admin/nationalities*')) || (request()->is('admin/religions*')) || (request()->is('admin/citizenships*')) || (request()->is('admin/vocations*')) || (request()->is('admin/educationperiod*')) || (request()->is('admin/courses*')) || (request()->is('admin/descriptivefinalmarks*')) || (request()->is('admin/engagementtypes*')) || (request()->is('admin/shifts*')) || (request()->is('admin/nationaleducationplans*')) || (request()->is('admin/educationprofiles*')) || (request()->is('admin/qualificationdegrees*')) || (request()->is('admin/gradeattemptsnumber*')) || (request()->is('admin/certificatetype*')) || (request()->is('admin/markexplanation*')) || (request()->is('admin/certificateblueprints*')) || (request()->is('admin/schoolyear*')) || (request()->is('admin/attendanceway*')) || (request()->is('admin/educationway*')) || (request()->is('admin/periodicexamway*')) || (request()->is('admin/descriptiveperiodicexammark*')) || (request()->is('admin/working-weeks*')) || (request()->is('admin/periodic-exam-type*')) ? 'show' : '' }}" id="pageSubmenu1">
                <li class="{{ (request()->is('admin/academicdegree*')) ? 'active' : '' }}">
                    <a href="{{ route('academicdegree.index') }}">{{trans('sidebar.sidebar_nav_academic_degree')}}</a>
                </li>
                <li class="{{ (request()->is('admin/jobandwork*')) ? 'active' : '' }}">
                    <a href="{{ route('jobandwork.index') }}">{{trans('sidebar.sidebar_nav_job_work')}}</a>
                </li>
                <li class="{{ (request()->is('admin/country*')) ? 'active' : '' }}">
                    <a href="{{ route('country.index') }}">{{trans('sidebar.sidebar_nav_countries')}}</a>
                </li>
                <li class="{{ (request()->is('admin/state*')) ? 'active' : '' }}">
                    <a href="{{ route('state.index') }}">{{trans('sidebar.sidebar_nav_states')}}</a>
                </li>
                <li class="{{ (request()->is('admin/canton*')) ? 'active' : '' }}">
                    <a href="{{ route('canton.index') }}">{{trans('sidebar.sidebar_nav_cantons')}}</a>
                </li>
                <li class="{{ (request()->is('admin/municipalities*')) ? 'active' : '' }}">
                    <a href="{{ route('municipalities.index') }}">{{trans('sidebar.sidebar_nav_municipalities')}}</a>
                </li>
                <li class="{{ (request()->is('admin/postalcodes*')) ? 'active' : '' }}">
                    <a href="{{ route('postalcodes.index') }}">{{trans('sidebar.sidebar_nav_postal_code')}}</a>
                </li>
                <li class="{{ (request()->is('admin/ownershiptypes*')) ? 'active' : '' }}">
                    <a href="{{route('ownershiptypes.index') }}">{{trans('sidebar.sidebar_nav_ownership_types')}}</a>
                </li>
                <li class="{{ (request()->is('admin/universities*')) ? 'active' : '' }}">
                    <a href="{{route('universities.index') }}">{{trans('sidebar.sidebar_nav_universities')}}</a>
                </li>
                <li class="{{ (request()->is('admin/colleges*')) ? 'active' : '' }}">
                    <a href="{{route('colleges.index') }}">{{trans('sidebar.sidebar_nav_colleges')}}</a>
                </li>
                <li class="{{ (request()->is('admin/grades*')) ? 'active' : '' }}">
                    <a href="{{route('grades.index')}}">{{trans('sidebar.sidebar_nav_grades')}}</a>
                </li>
                <li class="{{ (request()->is('admin/classes*')) ? 'active' : '' }}">
                    <a href="{{route('classes.index')}}">{{trans('sidebar.sidebar_nav_classes')}}</a>
                </li>
                <li class="{{ (request()->is('admin/nationalities*')) ? 'active' : '' }}">
                    <a href="{{route('nationalities.index')}}">{{trans('sidebar.sidebar_nav_nationalities')}}</a>
                </li>
                <li class="{{ (request()->is('admin/religions*')) ? 'active' : '' }}">
                    <a href="{{route('religions.index')}}">{{trans('sidebar.sidebar_nav_religions')}}</a>
                </li>
                <li class="{{ (request()->is('admin/citizenships*')) ? 'active' : '' }}">
                    <a href="{{route('citizenships.index')}}">{{trans('sidebar.sidebar_nav_country_citizenship')}}</a>
                </li>
                <li class="{{ (request()->is('admin/vocations*')) ? 'active' : '' }}">
                    <a href="{{route('vocations.index')}}">{{trans('sidebar.sidebar_nav_vocations')}}</a>
                </li>
                <li class="{{ (request()->is('admin/educationperiod*')) ? 'active' : '' }}">
                    <a href="{{ route('educationperiod.index') }}">{{trans('sidebar.sidebar_nav_education_periods')}}</a>
                </li>
                <li class="{{ (request()->is('admin/courses*')) ? 'active' : '' }}">
                    <a href="{{route('courses.index')}}">{{trans('sidebar.sidebar_nav_courses')}}</a>
                </li>
                <li class="{{ (request()->is('admin/descriptivefinalmarks*')) ? 'active' : '' }}">
                    <a href="{{route('descriptivefinalmarks.index')}}">{{trans('sidebar.sidebar_nav_dfm')}}</a>
                </li>
                <li class="{{ (request()->is('admin/engagementtypes*')) ? 'active' : '' }}">
                    <a href="{{route('engagementtypes.index')}}">{{trans('sidebar.sidebar_nav_et')}}</a>
                </li>
                <li class="{{ (request()->is('admin/shifts*')) ? 'active' : '' }}">
                    <a href="{{route('shifts.index')}}">{{trans('sidebar.sidebar_nav_shifts')}}</a>
                </li>
                <li class="{{ (request()->is('admin/nationaleducationplans*')) ? 'active' : '' }}">
                    <a href="{{ route('nationaleducationplans.index') }}">{{trans('sidebar.sidebar_nav_national_education_plan')}}</a>
                </li>
                <li class="{{ (request()->is('admin/educationprofiles*')) ? 'active' : '' }}">
                    <a href="{{ route('educationprofiles.index') }}">{{trans('sidebar.sidebar_nav_education_profile')}}</a>
                </li>
                <li class="{{ (request()->is('admin/qualificationdegrees*')) ? 'active' : '' }}">
                    <a href="{{ route('qualificationdegrees.index') }}">{{trans('sidebar.sidebar_nav_qualification_degree')}}</a>
                </li>
                <li class="{{ (request()->is('admin/gradeattemptsnumber*')) ? 'active' : '' }}">
                    <a href="{{ route('gradeattemptsnumber.index') }}">{{trans('sidebar.sidebar_grade_attempts_number')}}</a>
                </li>
                <li class="{{ (request()->is('admin/certificatetype*')) ? 'active' : '' }}">
                    <a href="{{ route('certificatetype.index') }}">{{trans('sidebar.sidebar_certificate_type')}}</a>
                </li>
                <li class="{{ (request()->is('admin/markexplanation*')) ? 'active' : '' }}">
                    <a href="{{ route('markexplanation.index') }}">{{trans('sidebar.sidebar_mark_explanation')}}</a>
                </li>
                <li class="{{ (request()->is('admin/certificateblueprints*')) ? 'active' : '' }}">
                    <a href="{{ route('certificateblueprints.index') }}">{{trans('sidebar.sidebar_certificate_blueprints')}}</a>
                </li>
                <li class="{{ (request()->is('admin/schoolyear*')) ? 'active' : '' }}">
                    <a href="{{ url('admin/schoolyear') }}">{{trans('sidebar.sidebar_nav_school_year')}}</a>
                </li>
                <li class="{{ (request()->is('admin/attendanceway*')) ? 'active' : '' }}">
                    <a href="{{ url('admin/attendanceway') }}">{{trans('general.gn_attendance_way')}}</a>
                </li>
                <li class="{{ (request()->is('admin/educationway*')) ? 'active' : '' }}">
                    <a href="{{ url('admin/educationway') }}">{{trans('general.gn_education_way')}}</a>
                </li>
                <li class="{{ (request()->is('admin/periodicexamway*')) ? 'active' : '' }}">
                    <a href="{{ url('admin/periodicexamway') }}">{{trans('general.gn_periodic_exam_way')}}</a>
                </li>
                <li class="{{ (request()->is('admin/descriptiveperiodicexammark*')) ? 'active' : '' }}">
                    <a href="{{ url('admin/descriptiveperiodicexammark') }}">{{trans('sidebar.sidebar_descriptive_periodic_exam_mark')}}</a>
                </li>
                <li class="{{ (request()->is('admin/working-weeks*')) ? 'active' : '' }}">
                    <a href="{{ route('working-weeks.index') }}">{{trans('sidebar.sidebar_nav_working_weeks')}}</a>
                </li>
                <li class="{{ (request()->is('admin/periodic-exam-type*')) ? 'active' : '' }}">
                    <a href="{{ route('periodic-exam-type.index') }}">{{trans('general.gn_periodic_exam_type')}}</a>
                </li>
            </ul>
        </li>
        <li class="{{ (request()->is('admin/schools*')) || (request()->is('admin/employees*')) || (request()->is('admin/students*')) || (request()->is('admin/educationplan*')) || (request()->is('admin/villageschools*')) ? 'active' : '' }}">
            <a href="#pageSubmenu2" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                <img alt="" src="{{ asset('images/ic_lock_color.png') }}" class="color">
                <img alt="" src="{{ asset('images/ic_lock.png') }}" class="selected">
                {{trans('sidebar.sidebar_nav_ministry_masters')}}
                <i class="cfa fas fa-chevron-down right-arrow"></i>
            </a>
            <ul class="collapse list-unstyled {{ (request()->is('admin/schools*')) || (request()->is('admin/employees*')) || (request()->is('admin/students*')) || (request()->is('admin/educationplan*')) || (request()->is('admin/villageschools*')) ? 'show' : '' }}" id="pageSubmenu2">
                <li class="{{ (request()->is('admin/schools*')) ? 'active' : '' }}">
                    <a href="{{url('/admin/schools')}}">{{trans('sidebar.sidebar_nav_schools')}}</a>
                </li>
                <li class="{{ (request()->is('admin/employees*')) ? 'active' : '' }}">
                    <a href="{{url('/admin/employees')}}">{{trans('sidebar.sidebar_nav_employees')}}</a>
                </li>
                <li class="{{ (request()->is('admin/student*')) ? 'active' : '' }}">
                    <a href="{{url('/admin/students')}}">{{trans('sidebar.sidebar_nav_students')}}</a>
                </li>
                <li class="{{ (request()->is('admin/educationplan*')) ? 'active' : '' }}">
                    <a href="{{url('/admin/educationplan')}}">{{trans('sidebar.sidebar_nav_education_plan')}}</a>
                </li>
                <li class="{{ (request()->is('admin/villageschools*')) ? 'active' : '' }}">
                    <a href="{{url('/admin/villageschools')}}">{{trans('sidebar.sidebar_nav_village_schools')}}</a>
                </li>
            </ul>
        </li>
        <li class="{{ (request()->is('admin/studentbehaviour*')) || (request()->is('admin/extracurricuralactivitytype*')) || (request()->is('admin/disciplinemeasuretype*')) ? 'active' : '' }}">
            <a href="#pageSubmenu3" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                <img alt="" src="{{asset('images/ic_cart_color.png')}}" class="color">
                <img alt="" src="{{asset('images/ic_cart.png')}}" class="selected">
                {{trans('sidebar.sidebar_nav_student_masters')}}
                <i class="cfa fas fa-chevron-down right-arrow"></i>
            </a>
            <ul class="collapse list-unstyled {{ (request()->is('admin/studentbehaviour*')) || (request()->is('admin/extracurricuralactivitytype*')) || (request()->is('admin/disciplinemeasuretype*')) ? 'show' : '' }}" id="pageSubmenu3">
                <li class="{{ (request()->is('admin/studentbehaviour*')) ? 'active' : '' }}">
                    <a href="{{url('/admin/studentbehaviour')}}">{{trans('sidebar.sidebar_nav_behaviour')}}</a>
                </li>
                <li class="{{ (request()->is('admin/extracurricuralactivitytype*')) ? 'active' : '' }}">
                    <a href="{{url('/admin/extracurricuralactivitytype')}}">{{trans('sidebar.sidebar_nav_ecat')}}</a>
                </li>
                <li class="{{ (request()->is('admin/disciplinemeasuretype*')) ? 'active' : '' }}">
                    <a href="{{url('/admin/disciplinemeasuretype')}}">{{trans('sidebar.sidebar_nav_dmt')}}</a>
                </li>
            </ul>
        </li>
        <li class="{{ (request()->is('admin/foreignlanguagegroup*')) || (request()->is('admin/optionalcoursesgroup*')) || (request()->is('admin/facultativecoursesgroup*')) || (request()->is('admin/generalpurposegroup*')) || (request()->is('admin/subclassgroup*')) ? 'active' : '' }}">
            <a href="#pageSubmenu4" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                <img alt="" src="{{asset('images/ic_ebook_color.png')}}" class="color">
                <img alt="" src="{{asset('images/ic_ebook.png')}}" class="selected">
                {{trans('sidebar.sidebar_nav_course_groups')}}
                <i class="cfa fas fa-chevron-down right-arrow"></i>
            </a>
            <ul class="collapse list-unstyled {{ (request()->is('admin/foreignlanguagegroup*')) || (request()->is('admin/optionalcoursesgroup*')) || (request()->is('admin/facultativecoursesgroup*')) || (request()->is('admin/generalpurposegroup*')) || (request()->is('admin/subclassgroup*')) ? 'show' : '' }}" id="pageSubmenu4">
                <li class="{{ (request()->is('admin/foreignlanguagegroup*')) ? 'active' : '' }}">
                    <a href="{{url('/admin/foreignlanguagegroup')}}">{{trans('sidebar.sidebar_nav_flg')}}</a>
                </li>
                <li class="{{ (request()->is('admin/optionalcoursesgroup*')) ? 'active' : '' }}">
                    <a href="{{url('/admin/optionalcoursesgroup')}}">{{trans('sidebar.sidebar_nav_ocg')}}</a>
                </li>
                <li class="{{ (request()->is('admin/facultativecoursesgroup*')) ? 'active' : '' }}">
                    <a href="{{url('/admin/facultativecoursesgroup')}}">{{trans('sidebar.sidebar_nav_fcg')}}</a>
                </li>
                <li class="{{ (request()->is('admin/generalpurposegroup*')) ? 'active' : '' }}">
                    <a href="{{url('/admin/generalpurposegroup')}}">{{trans('sidebar.sidebar_nav_gpg')}}</a>
                </li>
                <li class="{{ (request()->is('admin/subclassgroup*')) ? 'active' : '' }}">
                    <a href="{{url('/admin/subclassgroup')}}">{{trans('sidebar.sidebar_nav_scg')}}</a>
                </li>
            </ul>
        </li>
        <li class="{{ (request()->is('admin/languages*')) || (request()->is('admin/translations*')) ? 'active' : '' }}">
            <a href="#pageSubmenu5" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                <img alt="" src="{{ asset('images/ic_reports_color.png') }}" class="color">
                <img alt="" src="{{ asset('images/ic_reports.png') }}" class="selected">
                {{trans('sidebar.sidebar_nav_translations')}}
                <i class="cfa fas fa-chevron-down right-arrow"></i>
            </a>
            <ul class="collapse list-unstyled {{ (request()->is('admin/languages*')) || (request()->is('admin/translations*')) ? 'show' : '' }}" id="pageSubmenu5">
                <li class="{{ (request()->is('admin/languages*')) ? 'active' : '' }}">
                    <a href="{{ url('admin/languages') }}">
                        {{trans('sidebar.sidebar_nav_languages')}}
                    </a>
                </li>
                <li class="{{ (request()->is('admin/translations*')) ? 'active' : '' }}">
                    <a href="{{ url('admin/translations') }}">
                        {{trans('sidebar.sidebar_nav_keywords')}}
                    </a>
                </li>
            </ul>
        </li>
        <li class="{{ (request()->is('admin/educationprogram*')) ? 'active' : '' }}">
            <a href="{{url('/admin/educationprogram')}}">
                <img alt="" src="{{asset('images/ic_educational-programs_color.png')}}" class="color">
                <img alt="" src="{{asset('images/ic_educational-programs.png')}}" class="selected">
                {{trans('sidebar.sidebar_nav_education_program')}}
            </a>
        </li>
        <li>
            <a href="#">
                <img alt="" src="{{ asset('images/ic_reports_color.png') }}" class="color">
                <img alt="" src="{{ asset('images/ic_reports.png') }}" class="selected">
                {{trans('sidebar.sidebar_nav_ministry_report')}}
            </a>
        </li>
        <li>
            <a href="#">
                <img alt="" src="{{ asset('images/ic_logs_color.png') }}" class="color">
                <img alt="" src="{{ asset('images/ic_logs.png') }}" class="selected">
                {{trans('sidebar.sidebar_nav_logs')}}
            </a>
        </li>
    </ul>
    <div class="bottom-link">
        <a href="#x" class="help-link"><img alt="" src="{{ asset('images/ic_comment.png') }}">{{trans('general.gn_help')}}?</a>
        <a href="#x" class="logout"><img alt="" src="{{ asset('images/ic_logout.png') }}"></a>
    </div>


</nav>
