<nav id="sidebar" class="">
    <div class="sidebar-header">
        <h3>Hertronic</h3>
    </div>

    <ul class="list-unstyled components slim_scroll">
        <li class="{{ (request()->is('admin/dashboard')) ? 'active' : '' }}">
            <a href="{{url('/admin/dashboard')}}">
               <img alt="" src="{{ asset('images/ic_dashoard_color.png') }}" class="color">
               <img alt="" src="{{ asset('images/ic_dashoard.png') }}" class="selected">
                {{trans('sidebar.sidebar_nav_dasbhoard')}}
            </a>
        </li>
        <li class="{{ (request()->is('admin/subadmin*')) ? 'active' : '' }}">
            <a href="{{url('/admin/subadmin')}}">
                <img alt="" src="{{asset('images/ic_menu_color.png')}}" class="color">
                <img alt="" src="{{asset('images/ic_menu.png')}}" class="selected">
                {{trans('sidebar.sidebar_nav_user_management')}}
            </a>
        </li>
        <li class="{{ (request()->is('admin/educationplan*')) ? 'active' : '' }}">
            <a href="{{url('/admin/educationplan')}}">
               <img alt="" src="{{asset('images/ic_monitor_color.png')}}" class="color">
               <img alt="" src="{{asset('images/ic_monitor.png')}}" class="selected">
                {{trans('sidebar.sidebar_nav_education_plan')}}
            </a>
        </li>
        <li class="{{ (request()->is('admin/schools*')) ? 'active' : '' }}">
            <a href="{{url('/admin/schools')}}">
               <img alt="" src="{{asset('images/ic_modules_color.png')}}" class="color">
               <img alt="" src="{{asset('images/ic_modules.png')}}" class="selected">
                {{trans('sidebar.sidebar_nav_school_management')}}
            </a>
        </li>
        <li class="{{ (request()->is('admin/students*')) ? 'active' : '' }}">
            <a  href="{{url('/admin/students')}}">
               <img alt="" src="{{asset('images/ic_users_color.png')}}" class="color">
               <img alt="" src="{{asset('images/ic_users.png')}}" class="selected">
                {{trans('sidebar.sidebar_nav_students')}}
            </a>
        </li>
        <li class="{{ (request()->is('admin/employees*')) ? 'active' : '' }}">
            <a href="{{url('/admin/employees')}}">
               <img alt="" src="{{asset('images/ic_team_color.png')}}" class="color">
               <img alt="" src="{{asset('images/ic_team.png')}}" class="selected">
                {{trans('sidebar.sidebar_nav_employees')}}
            </a>
        </li>
         <li>
            <a href="#">
               <img alt="" src="{{asset('images/ic_attendant-list_color.png')}}" class="color">
               <img alt="" src="{{asset('images/ic_attendant-list.png')}}" class="selected">
                {{trans('sidebar.sidebar_nav_attendance')}}
            </a>
        </li>
        <li>
            <a href="#">
               <img alt="" src="{{asset('images/ic_reports_color.png')}}" class="color">
               <img alt="" src="{{asset('images/ic_reports.png')}}" class="selected">
                {{trans('sidebar.sidebar_nav_report')}}
            </a>
        </li>
        <li>
            <a href="#">
                <img alt="" src="{{asset('images/ic_logs_color.png')}}" class="color">
                <img alt="" src="{{asset('images/ic_logs.png')}}" class="selected">
                {{trans('sidebar.sidebar_nav_logs')}}
            </a>
        </li>
    </ul>
    <div class="bottom-link">
      <a href="#x" class="help-link">
        <img alt="" src="{{ asset('images/ic_comment.png')}}" >{{trans('general.gn_help')}}
      ?</a>
      <a href="#x" class="logout">
        <img alt="" src="{{ asset('images/ic_logout.png')}}" >
      </a>
    </div>
</nav>