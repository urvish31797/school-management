@extends('layout.app_with_login')
@section('title','Ministry Sub Admin')
@section('script', asset('js/dashboard/sub_admin.js'))
@section('content')
 <!-- Page Content  -->
<div class="section">
	<div class="container-fluid">
		<div class="row ">
            <div class="col-12 mb-3">
                <h2 class="title"><span>{{trans('sidebar.sidebar_nav_user_management')}} > </span><a class="no_sidebar_active" href="{{url('/admin/subadmin')}}"><span>{{trans('general.gn_sub_admin')}} > </span></a> {{trans('general.gn_add')}}</h2>
            </div>
            <div class="col-12">
                <div class="white_box pt-5 pb-5">
                    <div class="container-fluid">
                    	<form name="add-ministry-form">
                            <div class="row">
                                <div class="col-lg-3"></div>
                                <div class="col-lg-6">
                                    <div class="text-center">
                                        <div class="profile_box">
                                            <div class="profile_pic">
                                                <img alt="" id="user_img" src="{{ asset('images/user.png') }}">
                                                <input type="hidden" id="img_tmp" value="{{ asset('images/user.png') }}">
                                            </div>
                                        </div>
                                        <div  class="upload_pic_link">
                                            <a href="javascript:void(0)">
                                            {{trans('general.gn_upload_photo')}}<input type="file" accept="image/jpeg,image/png" id="upload_profile" name="upload_profile"></a>

                                        </div>
                                    </div>
                                    <div class="">
                                        <!-- <div class="form-group">
                                            <label>{{trans('general.gn_first_name')}} *</label>
                                            <input type="text" name="fname" class="form-control" placeholder="{{trans('general.gn_enter')}} {{trans('general.gn_first_name')}}">
                                        </div>
                                        <div class="form-group">
                                            <label>{{trans('general.gn_last_name')}} *</label>
                                            <input type="text" name="lname" class="form-control" placeholder="{{trans('general.gn_enter')}} {{trans('general.gn_last_name')}}">
                                        </div> -->
                                        <div class="form-group">
                                            <label>{{trans('general.gn_name')}} *</label>
                                            <input type="text" name="adm_Name" id="adm_Name" class="form-control" placeholder="{{trans('general.gn_enter')}} {{trans('general.gn_name')}}">
                                        </div>
                                        <div class="form-group">
                                            <label>{{trans('login.ln_email')}} *</label>
                                            <input type="text" name="email" class="form-control" placeholder="{{trans('general.gn_enter')}} {{trans('login.ln_email')}}">
                                        </div>
                                        <div class="form-group">
                                            <label>{{trans('general.gn_phone')}}</label>
                                            <input type="text" name="adm_Phone" class="form-control" placeholder="{{trans('general.gn_enter')}} {{trans('general.gn_phone')}}">
                                        </div>
                                        <div class="form-group">
                                            <label>{{trans('general.gn_title')}}</label>
                                            <input type="text" name="adm_Title" class="form-control" placeholder="{{trans('general.gn_enter')}} {{trans('general.gn_title')}}">
                                        </div>
                                        <div class="form-group">
                                            <label>{{trans('general.gn_gender')}} *</label>
                                            <select onchange="checkEmployeeID()" name="adm_Gender" id="adm_Gender" class="form-control icon_control dropdown_control">
                                                <option value="">{{trans('general.gn_select')}}</option>
                                                <option value="Male">{{trans('general.gn_male')}}</option>
                                                <option value="Female">{{trans('general.gn_female')}}</option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label>{{trans('general.gn_dob')}} *</label>
                                            <input onchange="checkEmployeeID()" type="text" name="adm_DOB" id="adm_DOB" class="form-control icon_control date_control datepicker" placeholder="{{trans('general.gn_enter')}} {{trans('general.gn_dob')}}">
                                        </div>
                                        <div class="form-group opt_id">
                                            <label>{{trans('general.gn_government')}} ID *</label>
                                            <input onfocusout="checkEmployeeID()" type="text" name="adm_GovId" class="form-control numbersonly isValidEmpID" maxlength="13" placeholder="{{trans('general.gn_enter')}} {{trans('general.gn_government')}} ID">
                                        </div>
                                        <div class="form-group form-check">
                                            <input type="checkbox" name="havent_identification_number" class="form-check-input" id="havent_identification_number">
                                            <label class="custom_checkbox"></label>
                                            <label class="form-check-label label-text" for="exampleCheck1">{{trans('general.gn_havent_identification_number')}}</label>
                                        </div>
                                        <div class="form-group opt_tmp_id hide_content">
                                            <label>{{trans('general.gn_temp_citizen_id')}} *</label>
                                            <input type="text" id="adm_TempGovId" name="adm_TempGovId" class="form-control numbersonly isValidEmpID" placeholder="{{trans('general.gn_enter')}} {{trans('general.gn_temp_citizen_id')}}" maxlength="13">
                                        </div>
                                        <div class="form-group">
                                            <label>{{trans('general.gn_address')}}</label>
                                            <input type="text" name="adm_Address" class="form-control" placeholder="{{trans('general.gn_enter')}} {{trans('general.gn_address')}}">
                                        </div>
                                        <div class="form-group">
                                            <label>{{trans('general.gn_canton')}} *</label>
                                            <select name="fkAdmCan" class="form-control icon_control dropdown_control">
                                                <option value="">{{trans('general.gn_select')}}</option>
                                                @foreach($data as $k => $v)
                                                    <option value="{{$v->pkCan}}">
                                                      {{$v['can_CantonName_'.$current_language]}}
                                                    </option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label>{{trans('general.gn_status')}} *</label>
                                            <select class="form-control" name="adm_Status" id="can_Status">
                                                <option value="">{{trans('general.gn_select')}}</option>
                                                <option value="Active">{{trans('general.gn_active')}}</option>
                                                <option value="Inactive">{{trans('general.gn_inactive')}}</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="text-center">
                                        <button type="submit" class="theme_btn">{{trans('general.gn_submit')}}</button>
                                        <a class="theme_btn red_btn no_sidebar_active" href="{{url('/admin/subadmin')}}">{{trans('general.gn_cancel')}}</a>
                                    </div>
                                </div>
                                <div class="col-lg-3"></div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<input type="hidden" id="employeemsgerror" value="{{trans('message.msg_employee_id_incorrect')}}">
<script>
    var listing_url = "{{route('fetch-subadmin-list')}}";
</script>
@endsection

@push('datatable-scripts')
<!-- Include this Page JS -->
<script type="text/javascript" src="{{ asset('js/dashboard/sub_admin.js') }}"></script>
@endpush