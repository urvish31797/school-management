@extends('layout.app_with_login')
@section('title', trans('sidebar.sidebar_nav_municipalities'))
@section('content')
<!-- Page Content  -->
<div class="section">
    <div class="container-fluid">
        <div class="row ">
            <div class="col-12 mb-3">
                <h2 class="title"><span>{{trans('sidebar.sidebar_nav_masters')}} > </span>{{trans('sidebar.sidebar_nav_municipalities')}}</h2>
            </div>
            <div class="col-md-4 mb-3">
                <input type="text" id="search_municipality" maxlength="30" class="form-control icon_control search_control" placeholder="{{trans('general.gn_search')}}">
            </div>
            <div class="col-md-1 text-md-right mb-3">

            </div>
            <div class="col-md-5 text-md-right mb-3">
                <div class="row">
                    <div class="col-4 text-right pr-0 pt-1">
                        <label class="blue_label">{{trans('general.gn_canton')}}</label>
                    </div>
                    <div class="col-8">
                        @include('partials.dropdown.dropdowns',[
                        'type'=>'canton_with_state',
                        'name'=>'',
                        'selected'=>'',
                        'extra_params'=>[
                        "id"=>"canton_filter",
                        ]
                        ])
                    </div>
                </div>
            </div>
            <div class="col-md-2 col-6 mb-3">
                <a><button class="theme_btn show_modal full_width small_btn" data-toggle="modal" data-target="#add_new">{{trans('general.gn_add_new')}}</button></a>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="theme_table">
                    <div class="table-responsive">
                        {{$dt_html->table(['class'=>"municipalities_listing display responsive","width"=>"100%"],true)}}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Add New Popup -->
<div class="theme_modal modal fade" id="add_new" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <img src="{{asset('images/ic_close_bg.png')}}" class="modal_top_bg">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <img src="{{asset('images/ic_close_circle_white.png')}}">
                </button>
                <form name="add-municipality-form" id="add-municipality-form">
                    <div class="row">
                        <div class="col-lg-1"></div>
                        <div class="col-lg-10">
                            <h5 class="modal-title" id="exampleModalCenterTitle">{{trans('sidebar.sidebar_nav_municipalities')}}</h5>
                            <input type="hidden" id="pkMun">
                            @foreach($languages as $k => $v)
                            <div class="form-group">
                                <label>{{trans('general.gn_name')}} {{$v->language_name}} *</label>
                                <input type="text" name="mun_MunicipalityName_{{$v->language_key}}" id="mun_MunicipalityName_{{$v->language_key}}" class="form-control force_require icon_control" required="">
                            </div>
                            @endforeach
                            <div class="form-group">
                                <label>{{trans('general.gn_genitive_name')}}</label>
                                <input type="text" name="mun_MunicipalityNameGenitive" id="mun_MunicipalityNameGenitive" class="form-control icon_control">
                            </div>
                            <div class="form-group">
                                <label>{{trans('general.gn_canton')}} *</label>
                                @include('partials.dropdown.dropdowns',[
                                'type'=>'canton_with_state',
                                'name'=>'fkMunCan',
                                'selected'=>'',
                                'extra_params'=>[
                                "id"=>"fkMunCan",
                                ]
                                ])
                            </div>
                            <div class="form-group">
                                <label>{{trans('general.gn_note')}}</label>
                                <input type="text" name="mun_Notes" id="mun_Notes" class="form-control icon_control">
                            </div>
                            <div class="text-center modal_btn ">
                                <button type="button" onclick="save()" class="theme_btn">{{trans('general.gn_submit')}}</button>
                            </div>
                        </div>
                        <div class="col-lg-1"></div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="theme_modal modal fade" id="delete_prompt" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <img src="{{asset('images/ic_close_bg.png')}}" class="modal_top_bg">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <img src="{{asset('images/ic_close_circle_white.png')}}">
                </button>
                <div class="row">
                    <div class="col-lg-1"></div>
                    <div class="col-lg-10">
                        <h5 class="modal-title" id="exampleModalCenterTitle">{{trans('general.gn_delete')}}</h5>
                        <div class="form-group text-center">
                            <label>{{trans('general.gn_delete_prompt')}}</label>
                            <input type="hidden" id="did">
                        </div>
                        <div class="text-center modal_btn ">
                            <button style="display: none;" class="theme_btn show_delete_modal full_width small_btn" data-toggle="modal" data-target="#delete_prompt">{{trans('general.gn_delete')}}</button>
                            <button type="button" onclick="confirmDelete()" class="theme_btn">{{trans('general.gn_yes')}}</button>
                            <button type="button" data-dismiss="modal" class="theme_btn red_btn">{{trans('general.gn_no')}}</button>
                        </div>
                    </div>
                    <div class="col-lg-1"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End Content Body -->
@endsection

@push('datatable-scripts')
<!-- Include datatable Page JS -->
<script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
<script type="text/javascript" src="{{ asset('js/dashboard/municipality.js') }}"></script>
{!! $dt_html->scripts() !!}
{!! JsValidator::formRequest(\App\Http\Requests\MunicipalityRequest::class, '#add-municipality-form'); !!}
@endpush
