@extends('layout.app_with_login')
@section('title', trans('sidebar.sidebar_certificate_blueprints'))
@section('content')
<!-- Page Content  -->
<div class="section">
    <div class="container-fluid">
        <div class="row ">
            <div class="col-12 mb-3">
                <h2 class="title"><span>{{trans('sidebar.sidebar_certificate_blueprints')}} > </span>{{trans('general.gn_edit')}}</h2>
            </div>
            <div class="col-12">
                <div class="white_box pt-3 pb-3">
                    <div class="container-fluid">
                        <form name="add-certificate-blueprint-form" id="add-certificate-blueprint-form">
                            <input type="hidden" id="pkCbp" value="{{$data->pkCbp}}">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="pl-5 pr-5">
                                        <div>
                                            <p class="mt-2"><strong>{{trans('sidebar.sidebar_certificate_blueprints')}}</strong></p>
                                        </div>
                                        <div class="bg-color-form">
                                            <div class="row">
                                                @foreach($languages as $k => $v)
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        @php
                                                        $val = "cbp_BluePrintName_".$v->language_key;
                                                        @endphp
                                                        <label>{{trans('general.gn_name')}} {{$v->language_name}} *</label>
                                                        <input maxlength="60" type="text" name="cbp_BluePrintName_{{$v->language_key}}" id="cbp_BluePrintName_{{$v->language_key}}" class="form-control force_require icon_control" required="" value="{{$data->$val}}">
                                                    </div>
                                                </div>
                                                @endforeach

                                                @foreach($languages as $k => $v)
                                                <div class="col-lg-12">
                                                    <div class="form-group">
                                                        @php
                                                        $value = "cbp_htmlContent_".$v->language_key;
                                                        @endphp
                                                        <label>{{trans('sidebar.sidebar_certificate_blueprints')}} {{$v->language_name}} *</label><button class="btn btn-primary btn-sm" type="button" onclick="preview('{{$v->language_key}}')">Preview</button>
                                                        <a href="void:javascript()" style="display: none;" class="show_preview_modal" data-toggle="modal" data-target="#PreviewModal">preview</a>
                                                        <input type="hidden" class="languages" id="{{$v->language_key}}">
                                                        <textarea style="height: 250px" class="form-control force_require icon_control" name="cbp_htmlContent_{{$v->language_key}}" id="cbp_htmlContent_{{$v->language_key}}" required="">{{$data->$value}}</textarea>
                                                    </div>
                                                </div>
                                                @endforeach

                                                <div class="col-lg-12">
                                                    <div class="form-group">
                                                        <label>{{trans('general.gn_status')}} *</label>
                                                        <select class="form-control" name="cbp_Cbpstatus" id="cbp_Cbpstatus">
                                                            <option value="Active" @if($data->cbp_Cbpstatus == "Active") selected="selected" @endif>{{trans('general.gn_active')}}</option>
                                                            <option value="Inactive" @if($data->cbp_Cbpstatus == "Inactive") selected="selected" @endif>{{trans('general.gn_inactive')}}</option>
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="col-lg-12">
                                                    <div class="form-group">
                                                        <label>{{trans('sidebar.sidebar_nav_keywords')}} *</label>
                                                        <input type="text" name="cbp_keywords" id="cbp_keywords" class="form-control icon_control" required="" value="{{$data->cbp_keywords}}">
                                                    </div>
                                                </div>

                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <label>{{trans('general.gn_start_date')}} *</label>
                                                        <input type="text" name="cbp_startdate" id="cbp_startdate" class="form-control icon_control date_control datepicker" required="" value="{{$data->cbp_startdate}}">
                                                    </div>
                                                </div>

                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <label>{{trans('general.gn_end_date')}} *</label>
                                                        <input type="text" name="cbp_enddate" id="cbp_enddate" value="{{$data->cbp_enddate}}" class="form-control icon_control date_control datepicker">
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                        <div class="text-center">
                                            <button type="button" onclick="save()" class="theme_btn">{{trans('general.gn_submit')}}</button>
                                            <a class="theme_btn red_btn" href="{{url('/admin/certificateblueprints')}}">{{trans('general.gn_cancel')}}</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="theme_modal modal fade" id="PreviewModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <div class="row" id="previewsection">

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('datatable-scripts')
<!-- Include this Page JS -->
<script type="text/javascript" src="{{ asset('js/dashboard/certificate_blueprints.js') }}"></script>
<script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
{!! JsValidator::formRequest(\App\Http\Requests\CertificateBluePrintsRequest::class, '#add-certificate-blueprint-form'); !!}
@endpush
