@extends('layout.app_with_login')
@section('title', trans('sidebar.sidebar_nav_cantons'))
@section('content')
<!-- Page Content  -->
<div class="section">
    <div class="container-fluid">
        <div class="row ">
            <div class="col-12 mb-3">
                <h2 class="title"><span>{{trans('sidebar.sidebar_nav_masters')}} > </span>{{trans('sidebar.sidebar_nav_cantons')}}</h2>
            </div>
            <div class="col-md-4 mb-3">
                <input type="text" id="search_canton" class="form-control icon_control search_control" maxlength="30" placeholder="{{trans('general.gn_search')}}">
            </div>
            <div class="col-md-3 text-md-right mb-3">
                <div class="row">
                    <div class="col-4 text-right pr-0 pt-1">
                        <label class="blue_label">{{trans('general.gn_country')}}</label>
                    </div>
                    <div class="col-8">
                        @include('partials.dropdown.dropdowns',[
                        'type'=>'country',
                        'name'=>'',
                        'selected'=>'',
                        'extra_params'=>[
                        "id"=>"country_filter",
                        "onchange"=>"fetchState(this.value,1)"
                        ]
                        ])
                    </div>
                </div>
            </div>
            <div class="col-md-3 text-md-right mb-3">
                <div class="row">
                    <div class="col-4 text-right pr-0 pt-1">
                        <label class="blue_label">{{trans('general.gn_state')}}</label>
                    </div>
                    <div class="col-8">
                        <select id="state_filter" class="form-control icon_control dropdown_control">
                            <option value="">{{trans('general.gn_select')}}</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="col-md-2 col-6 mb-3">
                <a><button class="theme_btn show_modal full_width small_btn" data-toggle="modal" data-target="#add_new">{{trans('general.gn_add_new')}}</button></a>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="theme_table">
                    <div class="table-responsive">
                        {{$dt_html->table(['class'=>"cantons_listing display responsive","width"=>"100%"],true)}}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Add New Popup -->
<div class="theme_modal modal fade" id="add_new" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <img src="{{asset('images/ic_close_bg.png')}}" class="modal_top_bg">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <img src="{{asset('images/ic_close_circle_white.png')}}">
                </button>
                <form name="add-canton-form" id="add-canton-form">
                    <div class="row">
                        <div class="col-lg-1"></div>
                        <div class="col-lg-10">
                            <h5 class="modal-title" id="exampleModalCenterTitle">{{trans('sidebar.sidebar_nav_cantons')}}</h5>
                            <input type="hidden" id="pkCan">
                            @foreach($languages as $k => $v)
                            <div class="form-group">
                                <label>{{trans('general.gn_name')}} {{$v->language_name}} *</label>
                                <input type="text" name="can_CantonName_{{$v->language_key}}" id="can_CantonName_{{$v->language_key}}" class="form-control force_require icon_control" required="">
                            </div>
                            @endforeach
                            <div class="form-group">
                                <label>{{trans('general.gn_genitive_name')}}</label>
                                <input type="text" name="can_CantonNameGenitive" id="can_CantonNameGenitive" class="form-control icon_control">
                            </div>
                            <div class="form-group">
                                <label>{{trans('general.gn_country')}}</label>
                                @include('partials.dropdown.dropdowns',[
                                'type'=>'country',
                                'name'=>'selCountry',
                                'selected'=>'',
                                'extra_params'=>[
                                "id"=>"selCountry",
                                "onchange"=>"fetchState(this.value,2)"
                                ]
                                ])
                            </div>
                            <div class="form-group">
                                <label>{{trans('general.gn_state')}} *</label>
                                <select class="form-control icon_control dropdown_control" name="fkCanSta" id="fkCanSta">
                                    <option value="">{{trans('general.gn_select')}}</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label>{{trans('general.gn_note')}}</label>
                                <input type="text" name="can_Note" id="can_Note" class="form-control icon_control">
                            </div>
                            <div class="form-group">
                                <label>{{trans('general.gn_status')}} *</label>
                                <select class="form-control icon_control dropdown_control" name="can_Status" id="can_Status">
                                    <option value="">{{trans('general.gn_select')}}</option>
                                    <option value="Active">{{trans('general.gn_active')}}</option>
                                    <option value="Inactive">{{trans('general.gn_inactive')}}</option>
                                </select>
                            </div>
                            <div class="text-center modal_btn ">
                                <button type="button" onclick="save()" class="theme_btn">{{trans('general.gn_submit')}}</button>
                            </div>
                        </div>
                        <div class="col-lg-1"></div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="theme_modal modal fade" id="delete_prompt" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <img src="{{asset('images/ic_close_bg.png')}}" class="modal_top_bg">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <img src="{{asset('images/ic_close_circle_white.png')}}">
                </button>
                <div class="row">
                    <div class="col-lg-1"></div>
                    <div class="col-lg-10">
                        <h5 class="modal-title" id="exampleModalCenterTitle">{{trans('general.gn_delete')}}</h5>
                        <div class="form-group text-center">
                            <label>{{trans('general.gn_delete_prompt')}}</label>
                            <input type="hidden" id="did">
                        </div>
                        <div class="text-center modal_btn ">
                            <button style="display: none;" class="theme_btn show_delete_modal full_width small_btn" data-toggle="modal" data-target="#delete_prompt">{{trans('general.gn_delete')}}</button>
                            <button type="button" onclick="confirmDelete()" class="theme_btn">{{trans('general.gn_yes')}}</button>
                            <button type="button" data-dismiss="modal" class="theme_btn red_btn">{{trans('general.gn_no')}}</button>
                        </div>
                    </div>
                    <div class="col-lg-1"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End Content Body -->
<script>
    var statebycountry_url = "{{route('fetch-statebycountry-lists')}}";
</script>
@endsection

@push('datatable-scripts')
<!-- Include datatable Page JS -->
<script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
<script type="text/javascript" src="{{ asset('js/dashboard/canton.js') }}"></script>
{!! $dt_html->scripts() !!}
{!! JsValidator::formRequest(\App\Http\Requests\CantonRequest::class, '#add-canton-form'); !!}
@endpush
