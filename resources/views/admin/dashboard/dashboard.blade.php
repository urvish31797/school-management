@extends('layout.app_with_login')
@section('title', trans('sidebar.sidebar_nav_dasbhoard'))
@section('content')
<!-- Page Content  -->
<div class="section">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				<h2 class="title">{{trans('sidebar.sidebar_nav_dasbhoard') ?? 'dashboard'}}</h2>
			</div>
			<div class="col-lg-4 col-md-6">
				<a href="#x">
    				<div class="card-dashboard">
        				<p>{{trans('sidebar.sidebar_nav_students')}}</p>
        				<h3>2,560</h3>
        				<img src="{{ asset('images/ic_shape2.png') }}">
        			</div>
        		</a>
			</div>
			<div class="col-lg-4 col-md-6">
				<a href="#x">
    				<div class="card-dashboard">
        				<p>{{trans('general.gn_super_admin')}}</p>
        				<h3>{{$SuperAdminCount}}</h3>
        				<img src="{{ asset('images/ic_shape3.png') }}">
        			</div>
        		</a>
			</div>
			<div class="col-lg-4 col-md-6">
				<a href="#x">
    				<div class="card-dashboard">
        				<p>{{trans('general.gn_parents')}}</p>
        				<h3>3560</h3>
        				<img src="{{ asset('images/ic_shape4.png') }}">
        			</div>
        		</a>
			</div>
		</div>
	</div>
</div>

<div class="section">
	<div class="container-fluid">
		<div class="row">
    		<div class="col-lg-4 col-md-12">
    			<p class="caps-head">{{trans('sidebar.sidebar_nav_teachers')}}</p>
    			<div class="teachers">
        			<div class="profile-cover ">
			        	<img src="{{ asset('images/img7.png') }}">
			        </div>
			        <div class="profile-cover ">
			        	<img src="{{ asset('images/img7.png') }}">
			        </div>
			        <div class="profile-cover ">
			        	<img src="{{ asset('images/img7.png') }}">
			        </div>
			        <div class="profile-cover ">
			        	<img src="{{ asset('images/img7.png') }}">
			        </div>
			        <div class="profile-cover ">
			        	<img src="{{ asset('images/img7.png') }}">
			        </div>
			        <div class="profile-cover ">
			        	<img src="{{ asset('images/img7.png') }}">
			        </div>
			        <div class="profile-cover ">
			        	<img src="{{ asset('images/img7.png') }}">
			        </div>
			        <div class="profile-link">
			        	<a href="#x">+12 more</a>
			        </div>
			    </div>
			    <div class="graph mt-4">
    				<img src="{{ asset('images/graph1.png') }}">
    			</div>
    		</div>
    		<div class="col-lg-8 col-md-12">
    			<div class="graph">
    				<img src="{{ asset('images/graph.png') }}">
    			</div>
    		</div>
    	</div>
	</div>
</div>
@endsection