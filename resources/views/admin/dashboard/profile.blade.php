@extends('layout.app_with_login')
@section('title', trans('general.gn_profile'))
@section('script', asset('js/dashboard/profile.js'))
@section('content')
 <!-- Page Content  -->
<div class="section">
  <div class="container-fluid">
    <h5 class="title">{{trans('general.gn_my_profile')}}</h5>
        <div class="white_box">
            <div class="theme_tab">
                <ul class="nav nav-tabs" id="myTab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">{{trans('general.gn_general_information')}}</a>
                    </li>
                </ul>
                <div class="tab-content" id="myTabContent">
                    <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                        <div class="inner_tab" id="profile_detail">
                            <div class="row">
                                <div class="col-lg-3"></div>
                                <div class="col-lg-6">
                                    <div class="text-center">
                                        <div class="profile_box">
                                            <div class="profile_pic">
                                                <img src="@if(!empty($admin_data->adm_Photo)) {{asset('images/users/')}}/{{$admin_data->adm_Photo}} @else {{ asset('images/user.png') }}@endif">
                                            </div>
                                        </div>
                                        <h5 class="profile_name">{{$admin_data->name}}</h5>
                                        <p class="profile_mail">{{$admin_data->email}}</p>
                                    </div>
                                    <div class="profile_info_container">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <p class="label">{{trans('general.gn_title')}} :</p>
                                                    <p class="value">{{$admin_data->adm_Title}}</p>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <p class="label">{{trans('general.gn_phone')}} :</p>
                                                    <p class="value">{{$admin_data->adm_Phone}}</p>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <p class="label">{{trans('general.gn_gender')}} :</p>
                                                    <p class="value">{{$admin_data->adm_Gender}}</p>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <p class="label">{{trans('general.gn_dob')}} :</p>
                                                    <p class="value">{{$admin_data->adm_DOB}}</p>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <p class="label">{{trans('general.gn_government')}} ID :</p>
                                                    <p class="value">
                                                    {{!empty($admin_data->adm_TempGovId) ? $admin_data->adm_TempGovId : $admin_data->adm_GovId}}
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="text-center">
                                        <button class="theme_btn" id="edit_profile">{{trans('general.gn_edit_profile')}}</button>
                                        <button class="theme_btn" data-toggle="modal" data-target="#change_pass">{{trans('general.gn_change_password')}}</button>
                                    </div>
                                </div>
                                <div class="col-lg-3"></div>
                            </div>
                        </div>
                        <div class="inner_tab" id="edit_profile_detail" style="display: none;">
                          <form id="edit-profile-form" name="edit-profile">
                            <div class="row">
                                <div class="col-lg-3"></div>
                                <div class="col-lg-6">
                                    <div class="text-center">
                                        <div class="profile_box">
                                            <div class="profile_pic">
                                                <img id="user_img" src="@if(!empty($admin_data->adm_Photo)) {{asset('images/users/')}}/{{$admin_data->adm_Photo}} @else {{ asset('images/user.png') }}@endif">
                                                <input type="hidden" id="img_tmp" value="{{ asset('images/user.png') }}">
                                            </div>
                                            <div class="edit_pencile">
                                              <img src="{{ asset('images/ic_pen.png') }}">
                                              <input type="file" id="upload_profile" name="upload_profile" accept="image/jpeg,image/png">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="">
                                        <div class="form-group">
                                            <label>{{trans('general.gn_name')}}</label>
                                            <input type="text" name="name" class="form-control" value="{{$admin_data->adm_Name}}">
                                          </div>
                                          <div class="form-group">
                                            <label>{{trans('login.ln_email')}}</label>
                                            <input type="text" name="email" class="form-control" value="{{$admin_data->email}}">
                                          </div>
                                          <div class="form-group">
                                            <label>{{trans('general.gn_title')}}</label>
                                            <input type="text" name="title" class="form-control" value="{{$admin_data->adm_Title}}">
                                          </div>
                                          <div class="form-group">
                                            <label>{{trans('general.gn_phone')}}</label>
                                            <input type="number" name="phone" class="form-control" value="{{$admin_data->adm_Phone}}">
                                          </div>
                                          <div class="form-group">
                                            <label>{{trans('general.gn_gender')}}</label>
                                            <select onchange="checkEmployeeID()" id="adm_Gender" name="gender" class="form-control icon_control dropdown_control">
                                              <option @if($admin_data->adm_Gender == 'Male') {{'selected'}} @endif value="Male">Male</option>
                                              <option @if($admin_data->adm_Gender == 'Female') {{'selected'}} @endif value="Female">Female</option>
                                            </select>
                                          </div>
                                          <div class="form-group">
                                            <label>{{trans('general.gn_dob')}}</label>
                                            <input onchange="checkEmployeeID()" type="text" name="dob" id="adm_DOB" class="form-control icon_control date_control datepicker" value="{{$admin_data->adm_DOB}}">
                                          </div>
                                          <div class="form-group opt_id @if(!empty($admin_data->adm_TempGovId)) hide_content @endif">
                                            <label>{{trans('general.gn_government')}} ID</label>
                                            <input onfocusout="checkEmployeeID()" type="text" name="govt_id" id="adm_GovId" class="form-control numbersonly isValidEmpID" value="{{$admin_data->adm_GovId}}" maxlength="13">
                                          </div>
                                          <div class="form-group form-check">
                                              <input type="checkbox" name="havent_identification_number" class="form-check-input" id="havent_identification_number" @if(!empty($admin_data->adm_TempGovId)) checked @endif>
                                              <label class="custom_checkbox"></label>
                                              <label class="form-check-label label-text" for="exampleCheck1">{{trans('general.gn_havent_identification_number')}}</label>
                                          </div>
                                          <div class="form-group opt_tmp_id @if(empty($admin_data->adm_TempGovId)) hide_content @endif">
                                              <label>{{trans('general.gn_temp_citizen_id')}} *</label>
                                              <input type="text" id="adm_TempGovId" name="adm_TempGovId" class="form-control numbersonly isValidEmpID" placeholder="{{trans('general.gn_enter')}} {{trans('general.gn_temp_citizen_id')}}" value="{{$admin_data->adm_TempGovId}}" maxlength="13">
                                          </div>
                                    </div>
                                    <div class="text-center">
                                        <button type="submit" class="theme_btn">{{trans('general.gn_submit')}}</button>
                                        <button class="theme_btn red_btn" id="cancel_edit_profile" type="button">{{trans('general.gn_cancel')}}</button>
                                    </div>
                                </div>
                                <div class="col-lg-3"></div>
                            </div>
                          </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
      </div>
  </div>

           {{-- @if ($errors->any())
                <div class="alert alert-danger">
                  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            @if (\Session::has('success'))
                <div class="alert alert-success">
                  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <ul>
                        <li>{!! \Session::get('success') !!}</li>
                    </ul>
                </div>
            @endif

            @php
              $_REQUEST['data'] = (isset($_REQUEST['data']) && !empty($_REQUEST['data']))?$_REQUEST['data']:'profile';
            @endphp --}}

<!-- change password Modal -->
    <div class="theme_modal modal fade" id="change_pass" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-body">
              <img src="{{ asset('images/ic_close_bg.png') }}" class="modal_top_bg">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <img src="{{ asset('images/ic_close_circle_white.png') }}">
                </button>
                <form name="change-password-form">
                  <div class="row">
                    <div class="col-lg-1"></div>
                    <div class="col-lg-10">
                      <h5 class="modal-title" id="exampleModalCenterTitle">{{trans('general.gn_change_password')}}</h5>
                      <div class="form-group">
                        <label>{{trans('general.gn_old_password')}} *</label>
                        <input type="password" name="old_password" name="id" class="form-control pass_control icon_control" placeholder="">
                      </div>
                      <div class="form-group">
                        <label>{{trans('general.gn_new_password')}} *</label>
                        <input type="password" name="new_password" id="new_password" class="form-control pass_control icon_control" placeholder="">
                      </div>
                      <div class="form-group">
                        <label>{{trans('general.gn_confirm_password')}} *</label>
                        <input type="password" name="confirm_password" id="confirm_password" class="form-control pass_control icon_control" placeholder="">
                      </div>
                      <div class="text-center modal_btn ">
                        <button type="submit" class="theme_btn">{{trans('general.gn_submit')}}</button>
                      </div>
                      <div class="text-center process_msg">
                        <p class="green_msg custom_success_msg"></p>
                        <p class="error_msg custom_error_msg"></p>
                    </div>
                    <div class="col-lg-1"></div>
                  </div>
                </form>
            </div>
        </div>
      </div>
    </div>

    <input type="hidden" id="employeemsgerror" value="{{trans('message.msg_employee_id_incorrect')}}">
<!-- End Content Body -->
@endsection
@push('datatable-scripts')
<!-- Include this Page JS -->
<script type="text/javascript" src="{{ asset('js/dashboard/profile.js') }}"></script>
@endpush