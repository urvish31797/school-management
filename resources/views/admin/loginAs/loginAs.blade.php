@extends('layout.app_with_login')
@section('title', trans('sidebar.sidebar_nav_login_as'))
@section('script', asset('js/dashboard/login_as.js'))
@section('content')
 <!-- Page Content  -->
<div class="section">
    <div class="container-fluid">
        <div class="row ">
            <div class="col-12 mb-3">
                <h2 class="title"><span>{{trans('sidebar.sidebar_nav_user_management')}} > </span>{{trans('sidebar.sidebar_nav_login_as')}}</h2>
            </div>
            <div class="col-md-4 mb-3">
                <input type="text" id="search_login_as" maxlength="30" class="form-control icon_control search_control" placeholder="{{trans('general.gn_search')}}">
            </div>
            <div class="col-md-8 text-md-right mb-3">
                <div class="row">
                    <div class="col-8">
                        <label class="blue_label">{{trans('general.gn_roles')}}</label>
                    </div>
                    <div class="col-4">
                        <select id="role_type" class="form-control icon_control dropdown_control">
                            <option value="">{{trans('general.gn_select')}}</option>
                            <option value="MinistryAdmin">{{trans('general.gn_ministry_super_admin')}}</option>
                            <option value="SchoolCoordinator">{{trans('general.gn_school_coordinator')}}</option>
                            <option value="Teacher">{{trans('general.gn_teacher')}}</option>
                        </select>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="theme_table">
                    <div class="table-responsive">
                        <table id="login_as_listing" class="display" style="width:100%">
                            <thead>
                                <tr>
                                    <th>{{trans('general.sr_no')}}.</th>
                                    <th>{{trans('general.gn_uid')}}</th>
                                    <th>{{trans('general.gn_name')}}</th>
                                    <th>{{trans('general.gn_roles')}}</th>
                                    <th>{{trans('login.ln_email')}}</th>
                                    <th>{{trans('general.gn_status')}}</th>
                                    <th><div class="action">{{trans('sidebar.sidebar_nav_login_as')}}</div></th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<input type="hidden" id="school_coordinator_txt" value="{{trans('general.gn_school_coordinator')}}">
<input type="hidden" id="teacher_txt" value="{{trans('general.gn_teacher')}}">
<input type="hidden" id="msa_txt" value="{{trans('general.gn_ministry_super_admin')}}">

<div class="theme_modal modal fade" id="login_prompt" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <img src="{{asset('images/ic_close_bg.png')}}" class="modal_top_bg">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <img src="{{asset('images/ic_close_circle_white.png')}}">
                </button>
                    <div class="row">
                        <div class="col-lg-1"></div>
                        <div class="col-lg-10">
                            <h5 class="modal-title" id="exampleModalCenterTitle">{{trans('general.gn_delete')}}</h5>
                            <div class="form-group text-center">
                                <label>{{trans('general.gn_delete_prompt')}}</label>
                                <input type="hidden" id="did">
                            </div>
                            <div class="text-center modal_btn ">
                                <button style="display: none;" class="theme_btn show_delete_modal full_width small_btn" data-toggle="modal" data-target="#delete_prompt">{{trans('sidebar.sidebar_nav_languages')}}</button>
                                <button type="button" onclick="confirmDelete()" class="theme_btn">{{trans('general.gn_yes')}}</button>
                                <button type="button" data-dismiss="modal" class="theme_btn red_btn">{{trans('general.gn_no')}}</button>
                            </div>
                        </div>
                        <div class="col-lg-1"></div>
                    </div>
            </div>
        </div>
    </div>
</div>
<script>
    var listing_url = "{{route('fetch-loginas-lists')}}";
    var auth_login_url = "{{route('login-as')}}";
</script>
@endsection

@push('datatable-scripts')
<!-- Include datatable Page JS -->
<script type="text/javascript" id="datatable_script" src="{{ asset('js/dashboard/login_as.js') }}"></script>
@endpush
