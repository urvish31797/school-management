@extends('layout.app_with_login')
@section('title', trans('general.gn_profile'))
@section('script', asset('js/dashboard/employee_profile.js'))
@section('content')
 <!-- Page Content  -->
<div class="section">
  <div class="container-fluid">
    <h5 class="title">{{trans('general.gn_my_profile')}}</h5>
        <div class="white_box">
            <div class="theme_tab">
                <ul class="nav nav-tabs" id="myTab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">{{trans('general.gn_general_information')}}</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="Current-tab" data-toggle="tab" href="#Current" role="tab" aria-controls="Current" aria-selected="true">{{trans('general.gn_qualifications')}}</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="tab3-tab" data-toggle="tab" href="#tab3" role="tab" aria-controls="tab3" aria-selected="true">{{trans('general.gn_work_experience')}}</a>
                    </li>
                </ul>
                <div class="tab-content" id="myTabContent">
                    <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                        <div class="inner_tab" id="profile_detail">
                            <div class="row">
                                <div class="col-lg-1"></div>
                                <div class="col-lg-10">
                                    <div class="text-center">
                                        <div class="profile_box">
                                            <div class="profile_pic">
                                                <img src="@if(!empty($logged_user->emp_PicturePath)) {{asset('images/users/')}}/{{$logged_user->emp_PicturePath}} @else {{ asset('images/user.png') }}@endif">
                                            </div>
                                        </div>
                                        <h5 class="profile_name">{{$logged_user->emp_EmployeeName}} {{$logged_user->emp_EmployeeSurname}}</h5>
                                        <p class="profile_mail">{{$logged_user->email}}</p>
                                    </div>
                                    <div class="profile_info_container">
                                        <div class="row">
                                          <div class="col-12">
                                            <h6 class="profile_inner_title">{{trans('general.gn_general_information')}}</h6>
                                          </div>
                                            <div class="col-md-6 col-lg-3">
                                                <div class="form-group">
                                                    <p class="label">ID :</p>
                                                    <p class="value">{{$logged_user->emp_EmployeeID}}</p>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-lg-3">
                                                <div class="form-group">
                                                    <p class="label">{{trans('general.gn_gender')}} :</p>
                                                    <p class="value">{{$logged_user->emp_EmployeeGender}}</p>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-lg-3">
                                                <div class="form-group">
                                                    <p class="label">{{trans('general.gn_dob')}}:</p>
                                                    <p class="value">@if(!empty($logged_user->emp_DateOfBirth)){{date('d/m/Y',strtotime($logged_user->emp_DateOfBirth))}}@endif</p>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-lg-3">
                                                <div class="form-group">
                                                    <p class="label">{{trans('general.gn_place_of_birth')}} :</p>
                                                    <p class="value">{{$logged_user->emp_PlaceOfBirth}}</p>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-lg-3">
                                                <div class="form-group">
                                                    <p class="label">{{trans('general.gn_phone')}} :</p>
                                                    <p class="value">{{$logged_user->emp_PhoneNumber}}</p>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-lg-3">
                                                <div class="form-group">
                                                    <p class="label">{{trans('general.gn_municipality')}} :</p>
                                                    <p class="value">@if(isset($EmployeesDetail->municipality)){{$EmployeesDetail->municipality->mun_MunicipalityName}}@endif</p>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-lg-3">
                                                <div class="form-group">
                                                    <p class="label">{{trans('general.gn_nationality')}} :</p>
                                                    <p class="value">@if(isset($EmployeesDetail->nationality)){{$EmployeesDetail->nationality->nat_NationalityName}}@endif</p>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-lg-3">
                                                <div class="form-group">
                                                    <p class="label">{{trans('general.gn_religion')}} :</p>
                                                    <p class="value">@if(isset($EmployeesDetail->religion)){{$EmployeesDetail->religion->rel_ReligionName}} @endif</p>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-lg-3">
                                                <div class="form-group">
                                                    <p class="label">{{trans('general.gn_citizenship')}} :</p>
                                                    <p class="value">@if(isset($EmployeesDetail->citizenship)){{$EmployeesDetail->citizenship->ctz_CitizenshipName}}@endif</p>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-lg-3">
                                                <div class="form-group">
                                                    <p class="label">{{trans('general.gn_postal_code')}} :</p>
                                                    <p class="value">@if(isset($EmployeesDetail->postalCode)){{$EmployeesDetail->postalCode->pof_PostOfficeNumber}}@endif</p>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-lg-3">
                                                <div class="form-group">
                                                    <p class="label">{{trans('general.gn_address')}} :</p>
                                                    <p class="value">{{$logged_user->emp_Address}}</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="text-center">
                                        <button class="theme_btn" id="edit_profile">{{trans('general.gn_edit_profile')}}</button>
                                        <button class="theme_btn" data-toggle="modal" data-target="#change_pass">{{trans('general.gn_change_password')}}</button>
                                    </div>
                                </div>
                                <div class="col-lg-1"></div>
                            </div>
                        </div>
                        <div class="inner_tab" id="edit_profile_detail" style="display: none;">
                          <form id="edit-profile-form" name="edit-profile">
                          <div class="row">
                                <div class="col-lg-3"></div>
                                <div class="col-lg-6">
                                    <div class="text-center">
                                        <div class="profile_box">
                                            <div class="profile_pic">
                                                <img id="user_img" src="@if(!empty($logged_user->emp_PicturePath)) {{asset('images/users/')}}/{{$logged_user->emp_PicturePath}} @else {{ asset('images/user.png') }}@endif">
                                                <input type="hidden" id="img_tmp" value="{{ asset('images/user.png') }}">
                                            </div>
                                            <div class="edit_pencile">
                                              <img src="{{ asset('images/ic_pen.png') }}">
                                              <input type="file" id="upload_profile" name="upload_profile" accept="image/jpeg,image/png" oninvalid="setCustomValidity('Please select a valid image')">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="">
                                          <div class="form-group">
                                              <label>{{trans('general.gn_name')}} *</label>
                                              <input type="text" name="emp_EmployeeName" class="form-control" placeholder="{{trans('general.gn_enter')}} {{trans('general.gn_name')}}" value="{{$logged_user->emp_EmployeeName}}">
                                              <input id="aid" type="hidden" value="{{$logged_user->id}}">
                                          </div>
                                          <div class="form-group">
                                            <label>{{trans('general.gn_employee')}} ID *</label>
                                            <input type="text" name="emp_EmployeeID" id="emp_EmployeeID" class="form-control" placeholder="{{trans('general.gn_enter')}} ID" value="{{$logged_user->emp_EmployeeID}}">
                                          </div>
                                          <div class="form-group">
                                            <label>{{trans('general.gn_temp_citizen_id')}} </label>
                                            <input type="text" name="emp_TempCitizenId" id="emp_TempCitizenId" class="form-control" placeholder="{{trans('general.gn_enter')}} {{trans('general.gn_temp_citizen_id')}}" value="{{$logged_user->emp_TempCitizenId}}">
                                          </div>
                                          <div class="form-group">
                                            <label>{{trans('general.gn_gender')}}</label>
                                            <select name="emp_EmployeeGender" id="emp_EmployeeGender" class="form-control icon_control dropdown_control">
                                              <option @if($logged_user->emp_EmployeeGender == 'Male') selected @endif value="Male">{{trans('general.gn_male')}}</option>
                                              <option @if($logged_user->emp_EmployeeGender == 'Female') selected @endif value="Female">{{trans('general.gn_female')}}</option>
                                            </select>
                                          </div>
                                          <div class="form-group">
                                            <label>{{trans('general.gn_dob')}}</label>
                                            <input  type="text" name="emp_DateOfBirth" id="emp_DateOfBirth" class="form-control icon_control date_control datepicker" value="@if(!empty($logged_user->emp_DateOfBirth)){{date('d/m/Y',strtotime($logged_user->emp_DateOfBirth))}}@endif">
                                          </div>
                                          <div class="form-group">
                                            <label>{{trans('general.gn_place_of_birth')}} *</label>
                                            <input type="text" name="emp_PlaceOfBirth" id="emp_PlaceOfBirth" class="form-control" placeholder="{{trans('general.gn_enter')}} {{trans('general.gn_place_of_birth')}}" value="{{$logged_user->emp_PlaceOfBirth}}">
                                          </div>
                                          <div class="form-group">
                                            <label>{{trans('general.gn_country')}} *</label>
                                            <select name="fkEmpCny" class="form-control icon_control dropdown_control">
                                              <option value="">{{trans('general.gn_select')}}</option>
                                              @foreach($Countries as $k => $v)
                                                <option @if($EmployeesDetail->fkEmpCny == $v->pkCny) selected @endif value="{{$v->pkCny}}">{{$v->cny_CountryName}}</option>
                                              @endforeach
                                            </select>
                                          </div>
                                          <div class="form-group">
                                            <label>{{trans('general.gn_municipality')}} *</label>
                                            <select name="fkEmpMun" class="form-control icon_control dropdown_control">
                                              <option value="">{{trans('general.gn_select')}}</option>
                                              @foreach($Municipalities as $k => $v)
                                                <option @if($EmployeesDetail->fkEmpMun == $v->pkMun) selected @endif value="{{$v->pkMun}}">{{$v->mun_MunicipalityName}}</option>
                                              @endforeach
                                            </select>
                                          </div>
                                          <div class="form-group">
                                            <label>{{trans('general.gn_nationality')}} *</label>
                                            <select name="fkEmpNat" class="form-control icon_control dropdown_control">
                                              <option value="">{{trans('general.gn_select')}}</option>
                                              @foreach($Nationalities as $k => $v)
                                                <option @if($EmployeesDetail->fkEmpNat == $v->pkNat) selected @endif value="{{$v->pkNat}}">{{$v->nat_NationalityName}}</option>
                                              @endforeach
                                            </select>
                                          </div>
                                          <div class="form-group">
                                            <label>{{trans('general.gn_religion')}} *</label>
                                            <select name="fkEmpRel" class="form-control icon_control dropdown_control">
                                              <option value="">{{trans('general.gn_select')}}</option>
                                              @foreach($Religions as $k => $v)
                                                <option @if($EmployeesDetail->fkEmpRel == $v->pkRel) selected @endif value="{{$v->pkRel}}">{{$v->rel_ReligionName}}</option>
                                              @endforeach
                                            </select>
                                          </div>
                                          <div class="form-group">
                                            <label>{{trans('general.gn_citizenship')}} *</label>
                                            <select name="fkEmpCtz" class="form-control icon_control dropdown_control">
                                              <option value="">{{trans('general.gn_select')}}</option>
                                              @foreach($Citizenships as $k => $v)
                                                <option @if($EmployeesDetail->fkEmpCtz == $v->pkCtz) selected @endif value="{{$v->pkCtz}}">{{$v->ctz_CitizenshipName}}</option>
                                              @endforeach
                                            </select>
                                          </div>
                                          <div class="form-group">
                                            <label>{{trans('general.gn_address')}}</label>
                                            <input type="text" name="emp_Address" class="form-control" placeholder="{{trans('general.gn_enter')}} {{trans('general.gn_address')}}" value="{{$logged_user->emp_Address}}">
                                          </div>
                                          <div class="form-group">
                                            <label>{{trans('general.gn_phone')}} *</label>
                                            <input type="number" name="emp_PhoneNumber" id="emp_PhoneNumber" class="form-control" placeholder="{{trans('general.gn_enter')}} {{trans('general.gn_phone_number')}}" value="{{$logged_user->emp_PhoneNumber}}">
                                          </div>
                                          <div class="form-group">
                                            <label>{{trans('general.gn_email')}} *</label>
                                            <input type="text" name="email" id="email" class="form-control" placeholder="{{trans('general.gn_enter')}} {{trans('general.gn_email')}}" value="{{$logged_user->email}}">
                                          </div>
                                          <div class="form-group">
                                            <label>{{trans('general.gn_postal_code')}} *</label>
                                            <select name="fkEmpPof" class="form-control icon_control dropdown_control">
                                              <option value="">{{trans('general.gn_select')}}</option>
                                              @foreach($PostalCodes as $k => $v)
                                                <option @if($EmployeesDetail->fkEmpPof == $v->pkPof) selected @endif value="{{$v->pkPof}}">{{$v->pof_PostOfficeNumber}}</option>
                                              @endforeach
                                            </select>
                                          </div>
                                          <div class="form-group">
                                            <label>{{trans('general.gn_note')}}</label>
                                            <input type="text" name="emp_Notes" id="emp_Notes" class="form-control" placeholder="{{trans('general.gn_enter')}} {{trans('general.gn_note')}}" value="{{$logged_user->emp_Notes}}">
                                          </div>
                                      </div>
                                    <div class="text-center">
                                        <button type="submit" class="theme_btn">{{trans('general.gn_update')}}</button>
                                        <button class="theme_btn red_btn" id="cancel_edit_profile" type="button">{{trans('general.gn_cancel')}}</button>
                                    </div>
                                </div>
                                <div class="col-lg-3"></div>
                            </div>
                          </form>
                        </div>
                    </div>
                    <div class="tab-pane fade show" id="Current" role="tabpanel" aria-labelledby="Current-tab">
                        <div class="inner_tab" id="profile_detail2">
                            <div class="row">
                                <div class="col-lg-1"></div>
                                <div class="col-lg-10">
                                    <div class="profile_info_container">
                                        <div class="row">
                                          <div class="col-12">
                                            <h6 class="profile_inner_title">{{trans('general.gn_education_qualifications')}}</h6>
                                          </div>
                                          <div class="col-12">
                                            <div class="table-responsive">
                                              <table class="profile_table">
                                                <tbody>
                                                  @foreach($EmployeesDetail->employeeEducation as $ke => $ve)
                                                    <tr>
                                                      <td>{{$ke+1}}</td>
                                                      <td><p class="label">{{trans('general.gn_faculty')}} ({{trans('general.gn_college')}}) :*</p>
                                                            <p class="value">{{$ve->college->col_CollegeName}}</p>
                                                      </td>
                                                      <td>
                                                        <p class="label">{{trans('general.gn_university')}} :</p>
                                                      <p class="value">{{$ve->university->uni_UniversityName}}</p>
                                                      </td>
                                                      <td>
                                                        <p class="label">{{trans('general.gn_year_of_passing')}} :</p>
                                                      <p class="value">{{$ve->eed_YearsOfPassing}}</p>
                                                      </td>
                                                      <td>
                                                        <p class="label">{{trans('general.gn_document')}} :</p>
                                                      <p class="value"><a target="_blank" href="{{asset('files/users')}}/{{$ve->eed_PicturePath}}">{{$ve->eed_PicturePath}}</a></p>
                                                      </td>
                                                    </tr>
                                                  @endforeach
                                                </tbody>
                                              </table>
                                            </div>
                                          </div>

                                        </div>
                                    </div>
                                    <div class="text-center">
                                        <button class="theme_btn" id="edit_profile2">{{trans('general.gn_edit_profile')}}</button>
                                        <button class="theme_btn" data-toggle="modal" data-target="#change_pass">{{trans('general.gn_change_password')}}</button>
                                    </div>
                                </div>
                                <div class="col-lg-1"></div>
                            </div>
                        </div>
                        <div class="inner_tab" id="edit_profile_detail2" style="display: none;">
                          <form name="employee-education-detail-form">
                            <div class="row">
                                <div class="col-lg-3"></div>
                                <div class="col-lg-6 profile_de_details_add">
                                  @foreach($EmployeesDetail->employeeEducation as $ke => $ve)
                                    <div class="profile_info_container">
                                      <div class="row">
                                        <div class="col-12 text-right">
                                          <img src="{{asset('images/ic_close_circle.png')}}" class="close_img rm_ed" data-eed="{{$ke+1}}">
                                        </div>
                                        <div class="form-group col-md-6">
                                          <label>{{trans('general.gn_university')}} :</label>
                                          <select id="fkEedUni_{{$ke+1}}" required onchange="fetchCollege(this)" name="fkEedUni_{{$ke+1}}" class="form-control icon_control dropdown_control">
                                            <option value="">{{trans('general.gn_select')}}</option>
                                            @foreach($Universities as $k =>$v)
                                              <option @if($ve->university->pkUni == $v->pkUni) selected @endif value="{{$v->pkUni}}">{{$v->uni_UniversityName}}</option>
                                            @endforeach
                                          </select>
                                        </div>
                                        <div class="form-group col-md-6">
                                          <label>{{trans('general.gn_faculty')}} ({{trans('general.gn_college')}}) :*</label>
                                          <select id="fkEedCol_{{$ke+1}}" required name="fkEedCol_{{$ke+1}}" class="form-control icon_control dropdown_control college_sel">
                                            <option value="">{{trans('general.gn_select')}}</option>
                                            @foreach($ve->university->college as $k =>$v)
                                              <option @if($ve->college->pkCol == $v->pkCol) selected @endif value="{{$v->pkCol}}">{{$v['col_CollegeName_'.$current_language]}}</option>
                                            @endforeach
                                          </select>
                                        </div>
                                        <div class="form-group col-md-6">
                                          <label>{{trans('general.gn_academic_degree')}} :</label>
                                          <select id="fkEedAcd_{{$ke+1}}" required name="fkEedAcd_{{$ke+1}}" class="form-control icon_control dropdown_control">
                                            <option value="">{{trans('general.gn_select')}}</option>
                                            @foreach($AcademicDegrees as $k =>$v)
                                              <option @if($ve->academicDegree->pkAcd == $v->pkAcd) selected @endif value="{{$v->pkAcd}}">{{$v->acd_AcademicDegreeName}}</option>
                                            @endforeach
                                          </select>
                                        </div>
                                        <div class="form-group col-md-6">
                                          <label>{{trans('general.gn_qualification_degree')}} :</label>
                                          <select id="fkEedQde_{{$ke+1}}" required name="fkEedQde_{{$ke+1}}" class="form-control icon_control dropdown_control">
                                            <option value="">{{trans('general.gn_select')}}</option>
                                            @foreach($QualificationDegrees as $k =>$v)
                                              <option @if($ve->qualificationDegree->pkQde == $v->pkQde) selected @endif value="{{$v->pkQde}}">{{$v->qde_QualificationDegreeName}}</option>
                                            @endforeach
                                          </select>
                                        </div>
                                        <div class="form-group col-md-6">
                                          <label>{{trans('general.gn_designation')}} :</label>
                                          <select required id="fkEedEde_{{$ke+1}}" name="fkEedEde_{{$ke+1}}" class="form-control icon_control dropdown_control">
                                            <option value="">{{trans('general.gn_select')}}</option>
                                            @foreach($EmployeeDesignations as $k =>$v)
                                              <option @if($ve->employeeDesignation->pkEde == $v->pkEde) selected @endif value="{{$v->pkEde}}">{{$v->ede_EmployeeDesignationName}}</option>
                                            @endforeach
                                          </select>
                                        </div>
                                        <div class="form-group col-md-6">
                                          <label>{{trans('general.gn_year_of_passing')}} :</label>
                                          <input  required class="form-control datepicker-year date_control icon_control" type="text" id="eed_YearsOfPassing_{{$ke+1}}" name="eed_YearsOfPassing_{{$ke+1}}" value="{{$ve->eed_YearsOfPassing}}">
                                        </div>
                                        <div class="form-group col-md-6">
                                          <label>{{trans('general.gn_short_title')}} :</label>
                                          <input required class="form-control" type="text" id="eed_ShortTitle_{{$ke+1}}" name="eed_ShortTitle_{{$ke+1}}" value="{{$ve->eed_ShortTitle}}">
                                        </div>
                                        <div class="form-group col-md-6">
                                          <label>{{trans('general.gn_number_of_semesters')}} :</label>
                                          <input required class="form-control" type="number" id="eed_SemesterNumbers_{{$ke+1}}" name="eed_SemesterNumbers_{{$ke+1}}" value="{{$ve->eed_SemesterNumbers}}">
                                        </div>
                                        <div class="form-group col-md-6">
                                          <label>{{trans('general.gn_ect_points')}} :</label>
                                          <input required class="form-control" type="number" id="eed_EctsPoints_{{$ke+1}}" name="eed_EctsPoints_{{$ke+1}}" value="{{$ve->eed_EctsPoints}}">
                                        </div>

                                        <div class="form-group col-md-6">
                                          <label>{{trans('general.gn_document')}} :</label>
                                          <div class="upload_file">
                                            <input type="text" id="file_name_{{$ke+1}}" name="file_name_{{$ke+1}}" class="form-control" value="{{$ve->eed_PicturePath}}">
                                            <input class="diploma_file" type="file" id="eed_DiplomaPicturePath_{{$ke+1}}" name="eed_DiplomaPicturePath_{{$ke+1}}" accept="application/pdf,image/jpeg,image/png" />
                                          </div>
                                        </div>

                                        <div class="form-group col-md-6">
                                          <label>{{trans('general.gn_note')}} :</label>
                                          <input class="form-control" type="text" id="eed_Notes_{{$ke+1}}" name="eed_Notes_{{$ke+1}}" value="{{$ve->eed_Notes}}">
                                        </div>
                                      </div>
                                    </div>
                                    @endforeach
                                    <div class="text-center">
                                      <button class="theme_btn min_btn" id="add_qa" type="button">{{trans('general.gn_add')}}</button>
                                    </div>
                                    <div class="text-center">
                                        <button type="submit" class="theme_btn">{{trans('general.gn_update')}}</button>
                                        <button class="theme_btn red_btn" id="cancel_edit_profile2" type="button">{{trans('general.gn_cancel')}}</button>
                                    </div>
                                </div>
                                <div class="col-lg-3"></div>
                            </div>
                          </form>
                        </div>
                    </div>
                    <div class="tab-pane fade show" id="tab3" role="tabpanel" aria-labelledby="tab3-tab">
                        <div class="inner_tab" id="profile_detail3">
                            <div class="row">
                                <div class="col-lg-1"></div>
                                <div class="col-lg-10">
                                    <div class="profile_info_container">
                                        <div class="row">
                                          <div class="col-12">
                                            <h6 class="profile_inner_title">{{trans('general.gn_work_experience')}}</h6>
                                          </div>
                                          <div class="col-12">
                                            <div class="table-responsive">
                                              <table class="profile_table">
                                                <tbody>
                                                  @foreach($EmployeesDetail->EmployeesEngagement as $k => $v)
                                                  <tr>
                                                    <td>{{$k+1}}</td>
                                                    <td>
                                                      <p class="label">{{$v->school->sch_SchoolName}} :</p>
                                                      <p class="value">{{$v->employeeType->epty_Name}}</p>
                                                      <br>
                                                      <p class="label">{{trans('general.gn_type_of_engagement')}} :</p>
                                                      <p class="value">{{$v->engagementType->ety_EngagementTypeName}}</p>
                                                    </td>
                                                    <td>
                                                      <p class="label">{{trans('general.gn_date_of_engagement')}} :</p>
                                                      <p class="value">{{$v->een_DateOfEngagement}}</p>
                                                      <br>
                                                      <p class="label">{{trans('general.gn_hourly_rate')}} :</p>
                                                      <p class="value">{{$v->een_WeeklyHoursRate}}</p>
                                                    </td>
                                                    <td>
                                                      <p class="label">{{trans('general.gn_date_of_engagement_end')}} :</p>
                                                      <p class="value">@if(!empty($v->een_DateOfFinishEngagement)){{$v->een_DateOfFinishEngagement}}@endif</p>
                                                    </td>
                                                  </tr>
                                                  @endforeach
                                                </tbody>
                                              </table>
                                            </div>
                                          </div>

                                        </div>
                                    </div>
                                    <div class="text-center">
                                      @if($logged_user->type == 'SchoolCoordinator')
                                        <button class="theme_btn" id="edit_profile3">{{trans('general.gn_edit_profile')}}</button>
                                      @endif
                                        <button class="theme_btn" data-toggle="modal" data-target="#change_pass">{{trans('general.gn_change_password')}}</button>
                                    </div>
                                </div>
                                <div class="col-lg-1"></div>
                            </div>
                        </div>
                        <div class="inner_tab" id="edit_profile_detail3" style="display: none;">
                            <form name="engage-emp-form">
                              <input type="hidden" id="sid" name="sid" value="{{$MainSchool}}">
                                  <div class="row">
                                    <div class="col-md-2"></div>
                                    <div class="col-md-8 profile_eng_details_add">
                                      @foreach($EmployeesEngagements as $ke => $v)
                                        <div class="profile_info_container">
                                          <div class="row">
                                          <div class="col-12 text-right">
                                              <img src="{{asset('images/ic_close_circle.png')}}" class="rm_eng close_img" data-eed="{{$k+1}}">
                                          </div>
                                          <div class="col-md-6">
                                              <div class="form-group">
                                                  <label>{{trans('general.gn_date_of_engagement')}} *</label>
                                                  <input  required type="text" id="start_date_{{$ke+1}}" name="start_date_{{$ke+1}}" class="form-control icon_control date_control datepicker_norm" value="@if(!empty($v->een_DateOfEngagement)){{$v->een_DateOfEngagement}}@endif">
                                              </div>
                                          </div>
                                          <div class="col-md-6">
                                              <div class="form-group">
                                                  <label>{{trans('general.gn_date_of_engagement_end')}} </label>
                                                  <input  type="text" id="end_date_{{$ke+1}}" name="end_date_{{$ke+1}}" class="form-control icon_control date_control datepicker_norm" value="@if(!empty($v->een_DateOfFinishEngagement)){{$v->een_DateOfFinishEngagement}}@endif">
                                              </div>
                                          </div>
                                          <div class="col-md-6">
                                              <div class="form-group">
                                                  <label>{{trans('general.gn_week_hourly_rate')}} *</label>
                                                  <input required type="text" id="een_WeeklyHoursRate_{{$ke+1}}" name="een_WeeklyHoursRate_{{$ke+1}}" class="form-control" value="{{$v->een_WeeklyHoursRate}}">
                                              </div>
                                          </div>
                                          <div class="col-md-6">
                                              <div class="form-group">
                                                  <label>{{trans('general.gn_type_of_engagement')}} *</label>
                                                  <select required id="fkEenEty_{{$ke+1}}" name="fkEenEty_{{$ke+1}}" class="form-control icon_control dropdown_control">
                                                      <option value="">{{trans('general.gn_select')}}</option>
                                                      @foreach($EngagementTypes as $k => $ve)
                                                        <option @if($v->fkEenEty == $ve->pkEty) selected @endif value="{{$ve->pkEty}}">{{$ve->ety_EngagementTypeName}}</option>
                                                      @endforeach
                                                  </select>
                                              </div>
                                          </div>
                                          <div class="col-md-6">
                                              <div class="form-group">
                                                  <label>{{trans('general.gn_employee_type')}} *</label>
                                                  <select required id="fkEenEpty_{{$ke+1}}" name="fkEenEpty_{{$ke+1}}" class="form-control icon_control dropdown_control">
                                                      <option value="">{{trans('general.gn_select')}}</option>
                                                      @foreach($EmployeeTypes as $k => $ve)
                                                        <option @if($v->fkEenEpty == $ve->pkEpty) selected @endif value="{{$ve->pkEpty}}">@if(!empty($ve->epty_subCatName)){{$ve->epty_subCatName}} @else {{$ve->epty_Name}} @endif</option>
                                                      @endforeach
                                                  </select>
                                              </div>
                                          </div>
                                          <div class="col-md-6">
                                              <div class="form-group">
                                                  <label>{{trans('general.gn_note')}}</label>
                                                  <input type="text" id="note_{{$ke+1}}" name="note_{{$ke+1}}" class="form-control" value="{{$v->een_Notes}}">
                                              </div>
                                          </div>
                                        </div>
                                      </div>
                                    @endforeach
                                    <div class="text-center add_eng_btn">
                                      <button class="theme_btn min_btn" id="add_eng" type="button">{{trans('general.gn_add')}}</button>
                                    </div>
                                  </div>
                                  <div class="col-md-2"></div>
                                </div>
                              <div class="col-md-12">
                                  <div class="form-group">
                                      <div class="card">
                                        <div class="card-body">
                                          <b>{{trans('general.gn_note')}}:</b> {{trans('general.gn_end_date_note')}}
                                        </div>
                                      </div>
                                  </div>
                              </div>
                              <div class="col-md-12">
                                  <div class="text-center">
                                      <button type="Submit" class="theme_btn">{{trans('general.gn_submit')}}</button>
                                      <button class="theme_btn red_btn" id="cancel_edit_profile3" type="button">{{trans('general.gn_cancel')}}</button>
                                  </div>
                              </div>
                          </form>
                        </div>
                    </div>
                    <!-- Add Qualification element -->
                    <div id="profile_de_details" style="display: none;">
                      <div class="profile_info_container">
                        <div class="row">
                          <div class="col-12 text-right">
                            <img src="{{asset('images/ic_close_circle.png')}}" class="close_img rm_ed">
                          </div>
                          <div class="form-group col-md-6">
                            <label>{{trans('general.gn_university')}} :</label>
                            <select id="fkEedUni" required onchange="fetchCollege(this)" name="fkEedUni" class="form-control icon_control dropdown_control">
                              <option value="">{{trans('general.gn_select')}}</option>
                              @foreach($Universities as $k =>$v)
                                <option value="{{$v->pkUni}}">{{$v->uni_UniversityName}}</option>
                              @endforeach
                            </select>
                          </div>
                          <div class="form-group col-md-6">
                            <label>{{trans('general.gn_faculty')}} ({{trans('general.gn_college')}}) :*</label>
                            <select id="fkEedCol" required name="fkEedCol" class="form-control icon_control dropdown_control college_sel">
                              <option value="">{{trans('general.gn_select')}}</option>

                            </select>
                          </div>
                          <div class="form-group col-md-6">
                            <label>{{trans('general.gn_academic_degree')}} :</label>
                            <select id="fkEedAcd" required name="fkEedAcd" class="form-control icon_control dropdown_control">
                              <option value="">{{trans('general.gn_select')}}</option>
                              @foreach($AcademicDegrees as $k =>$v)
                                <option value="{{$v->pkAcd}}">{{$v->acd_AcademicDegreeName}}</option>
                              @endforeach
                            </select>
                          </div>
                          <div class="form-group col-md-6">
                            <label>{{trans('general.gn_qualification_degree')}} :</label>
                            <select id="fkEedQde" required name="fkEedQde" class="form-control icon_control dropdown_control">
                              <option value="">{{trans('general.gn_select')}}</option>
                              @foreach($QualificationDegrees as $k =>$v)
                                <option value="{{$v->pkQde}}">{{$v->qde_QualificationDegreeName}}</option>
                              @endforeach
                            </select>
                          </div>
                          <div class="form-group col-md-6">
                            <label>{{trans('general.gn_designation')}} :</label>
                            <select required id="fkEedEde" name="fkEedEde" class="form-control icon_control dropdown_control">
                              <option value="">{{trans('general.gn_select')}}</option>
                              @foreach($EmployeeDesignations as $k =>$v)
                                <option value="{{$v->pkEde}}">{{$v->ede_EmployeeDesignationName}}</option>
                              @endforeach
                            </select>
                          </div>
                          <div class="form-group col-md-6">
                            <label>{{trans('general.gn_year_of_passing)}} :</label>
                            <input  required class="form-control datepicker-year date_control icon_control" type="text" id="eed_YearsOfPassing" name="eed_YearsOfPassing">
                          </div>
                          <div class="form-group col-md-6">
                            <label>{{trans('general.gn_short_title')}} :</label>
                            <input required class="form-control" type="text" id="eed_ShortTitle" name="eed_ShortTitle">
                          </div>
                          <div class="form-group col-md-6">
                            <label>{{trans('general.gn_number_of_semesters')}} :</label>
                            <input required class="form-control" type="number" id="eed_SemesterNumbers" name="eed_SemesterNumbers">
                          </div>
                          <div class="form-group col-md-6">
                            <label>{{trans('general.gn_ect_points')}} :</label>
                            <input required class="form-control" type="number" id="eed_EctsPoints" name="eed_EctsPoints">
                          </div>

                          <div class="form-group col-md-6">
                            <label>{{trans('general.gn_document')}} :</label>
                            <div class="upload_file">
                              <input type="text" id="file_name" name="file_name" value="{{trans('general.gn_upload')}}" class="form-control">
                              <input class="diploma_file" required type="file" id="eed_DiplomaPicturePath" name="eed_DiplomaPicturePath" accept="application/pdf,image/jpeg,image/png" />
                            </div>
                          </div>

                          <div class="form-group col-md-6">
                            <label>{{trans('general.gn_note')}} :</label>
                            <input class="form-control" type="text" id="eed_Notes" name="eed_Notes">
                          </div>
                        </div>
                      </div>
                    </div>

                    <!-- Add Engagement element -->
                    <div id="profile_eng_details" style="display: none;">
                      <div class="profile_info_container new_emp_eng">
                        <div class="row">
                        <div class="col-12 text-right">
                            <img src="{{asset('images/ic_close_circle.png')}}" class="close_img rm_eng" data-eed="">
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>{{trans('general.gn_date_of_engagement')}} *</label>
                                <input  required type="text" id="start_date" name="start_date" class="form-control icon_control date_control datepicker_future">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>{{trans('general.gn_date_of_engagement_end')}} </label>
                                <input  type="text" id="end_date" name="end_date" class="form-control icon_control date_control datepicker_future">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                              <label>{{trans('general.gn_week_hourly_rate')}} *</label>
                              <input required type="text" id="een_WeeklyHoursRate" name="een_WeeklyHoursRate" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>{{trans('general.gn_type_of_engagement')}} *</label>
                                <select required id="fkEenEty" name="fkEenEty" class="form-control icon_control dropdown_control">
                                    <option value="">{{trans('general.gn_select')}}</option>
                                    @foreach($EngagementTypes as $k => $v)
                                      <option value="{{$v->pkEty}}">{{$v->ety_EngagementTypeName}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>{{trans('general.gn_employee_type')}} *</label>
                                <select required id="fkEenEpty" name="fkEenEpty" class="form-control icon_control dropdown_control">
                                    <option value="">{{trans('general.gn_select')}}</option>
                                    @foreach($EmployeeTypes as $k => $v)
                                      @if($v->epty_Name != 'SchoolCoordinator')
                                        <option value="{{$v->pkEpty}}">@if(!empty($v->epty_subCatName)){{$v->epty_subCatName}} @else {{$v->epty_Name}} @endif</option>
                                      @endif
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>{{trans('general.gn_note')}}</label>
                                <input type="text" id="note" name="note" class="form-control">
                            </div>
                        </div>
                      </div>
                    </div>
                    </div>

                    <!-- change password Modal -->
                    <div class="theme_modal modal fade" id="change_pass" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                      <div class="modal-dialog modal-dialog-centered" role="document">
                        <div class="modal-content">
                            <div class="modal-body">
                              <img src="{{ asset('images/ic_close_bg.png') }}" class="modal_top_bg">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                  <img src="{{ asset('images/ic_close_circle_white.png') }}">
                                </button>
                                <form name="change-password-form">
                                  <div class="row">
                                    <div class="col-lg-1"></div>
                                    <div class="col-lg-10">
                                      <h5 class="modal-title" id="exampleModalCenterTitle">{{trans('general.gn_change_password')}}</h5>
                                      <div class="form-group">
                                        <label>{{trans('general.gn_old_password')}} *</label>
                                        <input type="password" name="old_password" name="id" class="form-control pass_control icon_control" placeholder="">
                                      </div>
                                      <div class="form-group">
                                        <label>{{trans('general.gn_new_password')}} *</label>
                                        <input type="password" name="new_password" id="new_password" class="form-control pass_control icon_control" placeholder="">
                                      </div>
                                      <div class="form-group">
                                        <label>{{trans('general.gn_confirm_password')}} *</label>
                                        <input type="password" name="confirm_password" id="confirm_password" class="form-control pass_control icon_control" placeholder="">
                                      </div>
                                      <div class="text-center modal_btn ">
                                        <button type="submit" class="theme_btn">{{trans('general.gn_submit')}}</button>
                                      </div>
                                      <div class="text-center process_msg">
                                        <p class="green_msg custom_success_msg"></p>
                                        <p class="error_msg custom_error_msg"></p>
                                    </div>
                                    <div class="col-lg-1"></div>
                                  </div>
                                </div>
                                </form>
                            </div>
                        </div>
                      </div>
                    </div>

                  </div>

                </div>
        </div>
  </div>
  <input type="hidden" id="add_qualification_txt" value="{{trans('message.msg_please_add_qualification')}}">
  <input type="hidden" id="add_engagment_txt" value="{{trans('message.msg_please_add_engagment')}}">
</div>






<!-- End Content Body -->
@endsection
@push('datatable-scripts')
<!-- Include this Page JS -->
<script type="text/javascript" src="{{ asset('js/dashboard/employee_profile.js') }}"></script>
@endpush