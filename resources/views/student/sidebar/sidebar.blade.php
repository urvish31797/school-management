<nav id="sidebar" class="">
    <div class="sidebar-header">
        <h3>Hertronic</h3>
    </div>

    <ul class="list-unstyled components slim_scroll">
        <li class="{{ (request()->is('student/dashboard')) ? 'active' : '' }}">
            <a href="{{url('/student/dashboard')}}">
               <img alt="" src="{{ asset('images/ic_dashoard_color.png') }}" class="color">
               <img alt="" src="{{ asset('images/ic_dashoard.png') }}" class="selected">
                {{trans('sidebar.sidebar_nav_dasbhoard')}}
            </a>
        </li>
        <li>
            <a href="#">
               <img alt="" src="{{asset('images/ic_test-results_color.png')}}" class="color">
               <img alt="" src="{{asset('images/ic_test-results.png')}}" class="selected">
                {{trans('sidebar.sidebar_nav_exam_result')}}
            </a>
        </li>
        <li>
            <a href="#">
               <img alt="" src="{{asset('images/ic_attendant-list_color.png')}}" class="color">
               <img alt="" src="{{asset('images/ic_attendant-list.png')}}" class="selected">
                {{trans('sidebar.sidebar_nav_attendance')}}
            </a>
        </li>
    </ul>
    <div class="bottom-link">
      <a href="#x" class="help-link"><img alt="" src="{{ asset('images/ic_comment.png')}}" >{{trans('general.gn_help')}}?</a>
      <a href="#x" class="logout"><img alt="" src="{{ asset('images/ic_logout.png')}}" ></a>
    </div>


</nav>