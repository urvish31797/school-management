@extends('layout.app_with_login')
@section('title', trans('general.gn_homeroomteacher_history'))
@section('script', asset('js/dashboard/class_creation.js'))
@section('content')
<div class="section">
  <div class="container-fluid">
        <div class="col-12 mb-3">
               <h2 class="title"><span>{{trans('sidebar.sidebar_nav_organization')}} > </span><a class="no_sidebar_active" href="{{url('/employee/classcreation')}}"><span>{{trans('sidebar.sidebar_nav_class_creation')}} > </span></a>@if(isset($classDetails)){{trans('general.gn_edit')}}@else{{trans('general.gn_homeroomteacher_history')}}@endif</h2>
        </div>
        <div class="col-12">
            <div class="white_box pt-5 pb-5">
                <div class="container-fluid">
                  <div class="col-lg-10 offset-lg-1">
                    <div class="row">
                        <div class="col-md-12">
                          <div class="card-grey">
                              <div class="row">
                                  <div class="col-md-6 col-lg-3">
                                      <div class="form-group">
                                          <p class="label">{{trans('general.gn_school_year')}} :</p>
                                          <p class="value">{{$mdata->classCreationSchoolYear->sye_NameCharacter ?? ''}}</p>
                                      </div>
                                  </div>
                                  <div class="col-md-6 col-lg-3">
                                      <div class="form-group">
                                          <p class="label">{{trans('general.gn_homeroom_teacher')}} :</p>
                                          <p class="value">@if(isset($mdata->classCreationSemester[0]->homeRoomTeacher[0]->employeesEngagement->employee->emp_EmployeeName)){{$mdata->classCreationSemester[0]->homeRoomTeacher[0]->employeesEngagement->employee->emp_EmployeeName}} {{$mdata->classCreationSemester[0]->homeRoomTeacher[0]->employeesEngagement->employee->emp_EmployeeSurname}}@endif</p>
                                      </div>
                                  </div>
                                  <div class="col-md-6 col-lg-3">
                                      <div class="form-group">
                                          <p class="label">{{trans('general.gn_semester')}} :</p>
                                          <p class="value">@if(isset($mdata->classCreationSemester[0]->semester)){{$mdata->classCreationSemester[0]->semester->edp_EducationPeriodName ?? ''}}@endif</p>
                                      </div>
                                  </div>
                                  <div class="col-md-6 col-lg-3">
                                      <div class="form-group">
                                          <p class="label">{{trans('general.gn_school_grade')}} :</p>
                                          <p class="value">{{$Grades[0] ?? ''}}</p>
                                      </div>
                                  </div>
                                  <div class="col-md-6 col-lg-3">
                                      <div class="form-group">
                                          <p class="label">{{trans('general.gn_chief_student')}} :</p>
                                          <p class="value">@if(isset($mdata->classCreationSemester[0]->chiefStudent->student)){{$mdata->classCreationSemester[0]->chiefStudent->student->full_name ?? ''}}@endif</p>
                                      </div>
                                  </div>
                                  <div class="col-md-6 col-lg-3">
                                      <div class="form-group">
                                          <p class="label">{{trans('general.gn_treasure_student')}} :</p>
                                          <p class="value">@if(isset($mdata->classCreationSemester[0]->treasureStudent->student)){{$mdata->classCreationSemester[0]->treasureStudent->student->full_name ?? ''}}@endif</p>
                                      </div>
                                  </div>
                                  <div class="col-md-6 col-lg-3">
                                      <div class="form-group">
                                          <p class="label">{{trans('general.gn_class')}} :</p>
                                          <p class="value">{{$mdata->classCreationClasses->cla_ClassName ?? ''}}</p>
                                      </div>
                                  </div>
                                  <div class="col-md-6 col-lg-3">
                                      <div class="form-group">
                                          <p class="label">{{trans('general.gn_status')}} :</p>
                                          <p class="value">@if($mdata->clr_Status == 'Pending')<span class="custom_badge badge badge-warning">{{trans('general.gn_pending')}}</span>@else<span class="custom_badge badge badge-success">{{trans('general.gn_publish')}}</span>@endif</p>
                                      </div>
                                  </div>
                              </div>
                          </div>
                        </div>
                      </div>

                      <div class="row">
                        <div class="col-12">
                          <div class="theme_table">
                            <div class="table-responsive">
                              <table class="profile_table expand_table">
                              <thead>
                                  <tr>
                                      <th>No.</th>
                                      <th>{{trans('general.gn_employee')}}</th>
                                      <th>{{trans('general.gn_type_of_engagement')}}</th>
                                      <th>{{trans('general.gn_date_of_engagement')}}</th>
                                      <th>{{trans('general.gn_date_of_engagement_end')}}</th>
                                      <th>{{trans('general.gn_hourly_rate')}}</th>
                                      <th></th>
                                  </tr>
                              </thead>
                              <tbody>
                                @foreach($HomeRoomTeacher as $k => $v)
                                  <tr id="{{$v->employeesEngagement->pkEen}}" data-toggle="collapse" href="#collapseExample{{$v->employeesEngagement->pkEen}}" role="button" aria-expanded="false" aria-controls="collapseExample">
                                  <td>{{$k+1}}</td>
                                  <td>
                                    <p class="value">{{$v->employeesEngagement->employee->emp_EmployeeName}} {{$v->employeesEngagement->employee->emp_EmployeeSurname}}</p>
                                  </td>
                                  <td>
                                    <p class="value">{{$v->employeesEngagement->engagementType->ety_EngagementTypeName}}</p>
                                  </td>
                                  <td>
                                    <p class="value">{{$v->employeesEngagement->een_DateOfEngagement}}</p>
                                  </td>
                                  <td>
                                    <p class="value">@if(!empty($v->employeesEngagement->een_DateOfFinishEngagement)){{$v->employeesEngagement->een_DateOfFinishEngagement}}@endif</p>
                                  </td>
                                  <td>
                                    <p class="value">{{$v->employeesEngagement->getLatestHourRates[0]->ewh_WeeklyHoursRate ?? ''}}</p>
                                  </td>
                                  <td class="collapse_sign"> + </td>
                                </tr>
                                @if(!empty($v->employeesEngagement->getAllHourRates))
                                  @foreach($v->employeesEngagement->getAllHourRates as $gk => $vk)
                                  <tr class="collapse" id="collapseExample{{$v->employeesEngagement->pkEen}}">
                                      <td></td>
                                      <td colspan="7">
                                          <div class="expand_div">
                                              <div class="row">
                                                  <div class="col-md-4">
                                                      <p class="label">{{trans('general.gn_hourly_rate')}}</p>
                                                      <p class="value">{{$vk->ewh_WeeklyHoursRate ?? ''}}</p>
                                                  </div>
                                                  <div class="col-md-4">
                                                      <p class="label">{{trans('general.gn_start_date')}}</p>
                                                      <p class="value">{{$vk->ewh_StartDate ?? ''}}</p>
                                                  </div>
                                                  <div class="col-md-4">
                                                      <p class="label">{{trans('general.gn_end_date')}}</p>
                                                      <p class="value">{{$vk->ewh_EndDate ?? '-'}}</p>
                                                  </div>
                                                  <div class="col-12 mt-2">
                                                      <p class="label">{{trans('general.gn_hourly_note')}}</p>
                                                      <p class="value">{{$vk->ewh_Notes ?? ''}}</p>
                                                  </div>
                                              </div>
                                          </div>
                                      </td>
                                  </tr>
                                  @endforeach
                                @endif
                                @endforeach
                              </tbody>
                              </table>

                            </div>
                          </div>
                          <br>
                          <div class="text-center">
                            <button type="button" class="theme_btn" id="update_hometeacher" onclick="updateTeacher()">{{trans('general.gn_update_homeroomteacher')}}</button>
                          </div>

                        </div>
                      </div>

                      <div class="row" id="update_hometeacher_div" style="display: none;">
                        <div class="col-md-12">
                          <div class="card-grey">
                            <div class="row">
                              <form name="update_hometeacher_form">
                                <input type="hidden" id="hrtId" name="hrtId" value="{{$hrtId}}">
                                <input type="hidden" id="engId" name="engId" value="{{$engId}}">
                                <input type="hidden" id="pkCcs" name="pkCcs" value="{{$id}}">
                                    <div class="profile_info_container">
                                        <div class="row">
                                            <div class="col-12">
                                                <h6 class="profile_inner_title">{{trans('general.gn_homeroom_teacher')}}</h6>
                                            </div>
                                            <div class="col-md-12">
                                              <div class="form-group">
                                                      <label>{{trans('general.gn_employees')}} *</label>
                                                      <select name="homeroomteacher_sel" id="homeroomteacher_sel" class="form-control icon_control dropdown_control">
                                                      @foreach($employees as $k => $v)
                                                        <option @if($v->id == $engId) selected @endif value="{{$v->id}}">{{$v->empName}} - {{$v->empId}}</option>
                                                      @endforeach
                                                      </select>
                                              </div>
                                            </div>
                                            {{-- <div class="col-md-6 ">
                                                <div class="form-group">
                                                    <label>{{trans('general.gn_end_date')}} </label>
                                                    <input type="text" id="end_date" name="end_date" class="form-control icon_control date_control" value="">
                                                </div>
                                            </div> --}}
                                        </div>
                                    </div>
                                     <div class="text-center">
                                        <button type="submit" class="theme_btn">{{trans('general.gn_update')}}</button>
                                        <button type="button" id="cancel_btn" class="theme_btn red_btn">{{trans('general.gn_cancel')}}</button>
                                    </div>

                              </form>
                            </div>
                          </div>
                        </div>
                      </div>

                  </div>
                </div>
            </div>
        </div>
  </div>
</div>
<script>
var listing_url = "{{route('fetch-classcreation-lists')}}";
var class_creation_sem_url = "{{route('fetch-classcreationsem-lists')}}";
</script>
@endsection

@push('datatable-scripts')
<!-- Include this Page JS -->
<script type="text/javascript" src="{{ asset('js/dashboard/class_creation.js') }}"></script>
@endpush