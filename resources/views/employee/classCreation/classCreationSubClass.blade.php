@extends('layout.app_with_login')
@section('title', trans('sidebar.sidebar_nav_class_creation'))
@section('script', asset('js/dashboard/class_creation_sub_class.js'))
@section('content')
<?php

$Grades = '';
foreach ($mdata->classCreationGrades as $k => $v) {
    $selGrades[] = $v->grade->gra_GradeNumeric;
}
$Grades = implode(', ', $selGrades);

?>
<!-- Page Content  -->
<div class="section">
	<div class="container-fluid">
		<div class="row">
			<div class="col-12 mb-3">
               <h2 class="title"><span>{{trans('sidebar.sidebar_nav_organization')}} > </span><a class="no_sidebar_active" href="{{url('/employee/classcreation')}}"><span>{{trans('sidebar.sidebar_nav_class_creation')}} > </span></a>{{trans('general.gn_sub_class')}}</h2>
            </div>

        <div class="col-12">
            <div class="white_box pt-5 pb-5">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-10 offset-lg-1">
                           <div class="row">
                                <div class="col-md-12">
                                    <div class="card-grey">
                                        <div class="row">
                                            <input type="hidden" id="pkCcs" value="{{$id}}">
                                            <div class="col-md-6 col-lg-3">
                                                <div class="form-group">
                                                    <p class="label">{{trans('general.gn_school_year')}} :</p>
                                                    <p class="value">{{$mdata->classCreationSchoolYear->sye_NameCharacter}}</p>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-lg-3">
                                                <div class="form-group">
                                                    <p class="label">{{trans('general.gn_homeroom_teacher')}} :</p>
                                                    <p class="value">@if(isset($mdata->classCreationSemester[0]->homeRoomTeacher[0])){{$mdata->classCreationSemester[0]->homeRoomTeacher[0]->employeesEngagement->employee->emp_EmployeeName}} {{$mdata->classCreationSemester[0]->homeRoomTeacher[0]->employeesEngagement->employee->emp_EmployeeSurname}}@endif</p>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-lg-3">
                                                <div class="form-group">
                                                    <p class="label">{{trans('general.gn_semester')}} :</p>
                                                    <p class="value">{{$mdata->classCreationSemester[0]->semester->edp_EducationPeriodName}}</p>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-lg-3">
                                                <div class="form-group">
                                                    <p class="label">{{trans('general.gn_school_grade')}} :</p>
                                                    <p class="value">{{$Grades}}</p>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-lg-3">
                                                <div class="form-group">
                                                    <p class="label">{{trans('general.gn_chief_student')}} :</p>
                                                    <p class="value">@if(isset($mdata->classCreationSemester[0]->chiefStudent->student)){{$mdata->classCreationSemester[0]->chiefStudent->student->full_name}}@endif</p>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-lg-3">
                                                <div class="form-group">
                                                    <p class="label">{{trans('general.gn_treasure_student')}} :</p>
                                                    <p class="value">@if(isset($mdata->classCreationSemester[0]->treasureStudent->student)){{$mdata->classCreationSemester[0]->treasureStudent->student->full_name}}@endif</p>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-lg-3">
                                                <div class="form-group">
                                                    <p class="label">{{trans('general.gn_class')}} :</p>
                                                    <p class="value">{{$mdata->classCreationClasses->cla_ClassName}}</p>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-lg-3">
                                                <div class="form-group">
                                                    <p class="label">{{trans('general.gn_status')}} :</p>
                                                    <p class="value">@if($mdata->clr_Status == 'Pending')<span class="custom_badge badge badge-warning">{{trans('general.gn_pending')}}</span>@else<span class="custom_badge badge badge-success">{{trans('general.gn_publish')}}</span>@endif</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label>{{trans('general.gn_search_student')}}</label>
                                                <input type="text" id="search_student" maxlength="30" class="form-control icon_control search_control" placeholder="{{trans('general.gn_student')}} ID, {{trans('general.gn_name')}}">
                                            </div>
                                        </div>
                                    </div>
                                    <p class="mt-3"><strong>{{trans('general.gn_students_of_class')}} {{$mdata->classCreationClasses->cla_ClassName}}</strong></p>
                                    <div class="table-responsive mt-2">
                                        <table id="student_listing" class="display" style="width:100%">
                                            <thead>
                                                <tr>
                                                    <th>{{trans('general.sr_no')}}.</th>
                                                    <th>Id</th>
                                                    <th>{{trans('general.gn_student_name')}}</th>
                                                    <th>{{trans('general.gn_grade')}}</th>
                                                    <th>{{trans('general.gn_education_plan')}}</th>
                                                    <th><div class="action">{{trans('general.gn_action')}}</div></th>
                                                </tr>
                                            </thead>
                                        </table>
                                    </div>

                                    <p class="mt-3"><strong>{{trans('general.gn_class_creation_step_3_subtext')}}</strong></p>

                                    <form name="sub_class_form">
                                        <div class="row">

                                            <div class="col-md-4 col-lg-3 text-md-right mb-3">
                                                <div class="row">
                                                    <div class="col-4 text-right pr-0 pt-1">
                                                        <label class="blue_label">{{trans('general.gn_sub_class')}}</label>
                                                    </div>
                                                    <div class="col-8">
                                                        <select id="sub_select" name="sub_select" class="form-control icon_control dropdown_control">
                                                            <option value="">{{trans('general.gn_select')}}</option>
                                                            @foreach($subClassGroups as $k => $v)
                                                            <option value="{{$v->pkScg}}">{{$v->scg_Name}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-4 col-lg-4 text-md-right mb-3">
                                                <div class="row">
                                                    <div class="col-3 text-right pr-0 pt-1">
                                                        <label class="blue_label">{{trans('general.gn_courses')}}</label>
                                                    </div>
                                                    <div class="col-8">
                                                        <select id="course_select" name="course_select" class="form-control icon_control dropdown_control select2_single">
                                                            <option value="">{{trans('general.gn_select')}}</option>
                                                                @foreach ($courses as $k => $v)
                                                                @if(count($v->educationPlansMandatoryCourse))

                                                                    @foreach ($v->educationPlansMandatoryCourse as $kmc => $vmc)
                                                                        <option data-gra="{{$v->pkGra}}" value="{{$vmc->mandatoryCourseGroup->pkCrs}}">{{$vmc->mandatoryCourseGroup->crs_CourseName}} ({{trans('general.gn_grade')}} - {{$v->gra_GradeNumeric}})</option>
                                                                    @endforeach

                                                                @endif
                                                                @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-4 col-lg-3 text-md-right mb-3">
                                                <div class="row">
                                                    <div class="col-3 text-right pr-0 pt-1">
                                                        <label class="blue_label">{{trans('general.gn_teacher')}}</label>
                                                    </div>
                                                    <div class="col-8">
                                                        <select id="teacher_select" name="teacher_select" class="form-control icon_control dropdown_control select2_single" placeholder="{{trans('general.gn_teacher')}}">
                                                        <option value="">{{trans('general.gn_select')}}</option>
                                                        @foreach($employees as $ek => $ev)
                                                        <option value="{{$ek}}">{{$ev}}</option>
                                                        @endforeach
                                                    </select>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="table-responsive mt-2 selected_sub_class">
                                                <table class="color_table class_seleted_students sel_stu_elem hide_content">
                                                        <tr>
                                                            <th>{{trans('general.sr_no')}}</th>
                                                            <th>{{trans('general.gn_sub_class')}}</th>
                                                            <th>Id</th>
                                                            <th>{{trans('general.gn_students')}}</th>
                                                            <th>{{trans('general.gn_courses')}}</th>
                                                            <th>{{trans('general.gn_teachers')}}</th>
                                                            <th>{{trans('general.gn_action')}}</th>
                                                        </tr>
                                                    <tbody>
                                                    </tbody>
                                                </table>
                                            </div>

                                        </div>
                                        <div class="text-center">
                                            <button type="submit" class="small_btn theme_btn">{{trans('general.gn_submit')}}</button>
                                        </div>
                                    </form>
                                </div>


                        </div>

                        </div>
                    </div>
                </div>
            </div>

		</div>
	</div>
</div>

<input type="hidden" id="teacher_txt" value="{{trans('general.gn_teacher')}}">
<input type="hidden" id="subclass_grade_txt" value="{{trans('validation.validate_subclass_grade_student')}}">
<input type="hidden" id="stu_sel_valid_txt" value="{{trans('message.msg_student_select_valid')}}">
<input type="hidden" id="stu_sel_txt" value="{{trans('message.msg_sel_stu')}}">
<input type="hidden" id="subclass_course_txt" value="{{trans('validation.validate_subclass_course')}}">
<input type="hidden" id="subclass_teacher_txt" value="{{trans('validation.validate_subclass_teacher')}}">
<input type="hidden" id="subclass_txt" value="{{trans('validation.validate_subclass_subclass')}}">
<input type="hidden" id="subclass_create_txt" value="{{trans('validation.validate_subclass_create')}}">

<button id="trigger_edu_pop" class="hide_content" type="button" data-toggle="modal" data-target="#eplan_prompt">trigg</button>

<div class="theme_modal modal fade" id="eplan_prompt" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <img alt="Close" src="{{asset('images/ic_close_bg.png')}}" class="modal_top_bg">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <img alt="Profile Photo" src="{{asset('images/ic_close_circle_white.png')}}">
                </button>
                    <form name="course_details">
                    <div class="row">
                        <div class="col-lg-1"></div>
                        <div class="col-lg-10">
                            <h5 class="modal-title" id="exampleModalCenterTitle">{{trans('general.gn_view_courses')}}</h5>
                            <div class="course_details">

                            </div>
                            <div class="text-center modal_btn">
                                <button type="submit" class="theme_btn min_btn">{{trans('general.gn_submit')}}</button>
                                <button type="button" data-dismiss="modal" class="theme_btn red_btn min_btn">{{trans('general.gn_cancel')}}</button>
                            </div>
                        </div>
                        <div class="col-lg-1"></div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
</div>
@endsection

@push('datatable-scripts')
<!-- Include this Page JS -->
<script type="text/javascript" src="{{ asset('js/dashboard/class_creation_sub_class.js') }}"></script>
@endpush