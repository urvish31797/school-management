@extends('layout.app_with_login')
@section('title', trans('sidebar.sidebar_nav_class_creation'))
@section('script', asset('js/dashboard/class_creation.js'))
@section('content')

<!-- Page Content  -->
<div class="section">
	<div class="container-fluid">
		<div class="row">
			<div class="col-12 mb-3">
               <h2 class="title">
                <span>
                    {{trans('sidebar.sidebar_nav_organization')}} >
                </span>
                    <a class="no_sidebar_active" href="{{url('/employee/classcreation')}}">
                    <span>
                        {{trans('sidebar.sidebar_nav_class_creation')}} >
                    </span>
                    <span>
                        {{trans('general.gn_student')}} >
                    </span>
                    </a>

                    {{trans('general.gn_view_details')}}
               </h2>
            </div>
            <input type="hidden" id="stu_sel_valid_txt" value="{{trans('message.msg_student_select_valid')}}">
            <input type="hidden" id="stu_sel_txt" value="{{trans('message.msg_sel_stu')}}">

            <div class="col-12">
                <div class="white_box pt-5 pb-5">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-lg-10 offset-lg-1">

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="card-grey">
                                            <div class="row">
                                                <div class="col-md-6 col-lg-3">
                                                    <div class="form-group">
                                                        <p class="label">{{trans('general.gn_school_year')}} :</p>
                                                        <p class="value">{{$mdata->classCreationSchoolYear->sye_NameCharacter ?? ''}}</p>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-lg-3">
                                                    <div class="form-group">
                                                        <p class="label">{{trans('general.gn_school_grade')}} :</p>
                                                        <p class="value">{{$mdata->classCreationSemester[0]->classStudentsSemester[0]->studentEnroll->grade->gra_GradeNumeric ?? ''}}</p>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-lg-3">
                                                    <div class="form-group">
                                                        <p class="label">{{trans('general.gn_class')}} :</p>
                                                        <p class="value">{{$mdata->classCreationClasses->cla_ClassName ?? ''}}</p>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-lg-3">
                                                    <div class="form-group">
                                                        <p class="label">{{trans('general.gn_semester')}} :</p>
                                                        <p class="value">@if(isset($mdata->classCreationSemester[0]->semester)){{$mdata->classCreationSemester[0]->semester->edp_EducationPeriodName}}@endif</p>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-lg-3">
                                                    <div class="form-group">
                                                        <p class="label">{{trans('general.gn_student_name')}} :</p>
                                                        <p class="value">{{$mdata->classCreationSemester[0]->classStudentsSemester[0]->studentEnroll->student->full_name ?? ''}}</p>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-lg-3">
                                                    <div class="form-group">
                                                        <p class="label">{{trans('sidebar.sidebar_nav_education_plan')}} :</p>
                                                        <p class="value">{{$mdata->classCreationSemester[0]->classStudentsSemester[0]->studentEnroll->educationProgram->edp_Name ?? ''}} - {{$mdata->classCreationSemester[0]->classStudentsSemester[0]->studentEnroll->educationPlan->epl_EducationPlanName ?? ''}}</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        @if(count($mandatoryCourses))
                                        <p class="mt-4"><strong>{{trans('general.gn_mandatory_courses')}}</strong></p>
                                        <div class="table-responsive mt-2">
                                            <table class="color_table">
                                                <tbody>
                                                    <tr>
                                                        <th width="10%">{{trans('general.sr_no')}}</th>
                                                        <th width="40%">{{trans('general.gn_courses')}}</th>
                                                        <th width="40%">{{trans('general.gn_teachers')}}</th>
                                                        <th width="10%">{{trans('general.gn_action')}}</th>
                                                    </tr>
                                                    <?php $i = 1;?>
                                                    @foreach ($mandatoryCourses as $k => $v)
                                                    <tr>
                                                        <td>{{$i}}</td>
                                                        <td>{{$v['crs_CourseName']}}</td>
                                                        <td>{{$v['crs_Teachers']}}</td>
                                                        <td>
                                                            <a title="{{trans('general.gn_delete')}}" onclick="triggerCourseDelete({{$v['pkCsa']}})" href="javascript:void(0)"><img src="{{asset('images/ic_delete.png')}}"></a>
                                                        </td>
                                                    </tr>
                                                    <?php $i++; ?>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>

                                        @if(count($subclassCourses))
                                        @foreach ($subclassCourses as $k => $v)
                                        <p class="mt-4"><strong>{{trans('general.gn_mandatory_courses')}}</strong></p>
                                        <p class="label"><strong>{{trans('sidebar.sidebar_nav_scg')}} - {{$subclassCourses[$k][0]['scg_Name']}}</strong></p>
                                        <div class="table-responsive mt-2">
                                            <table class="color_table">
                                                <tbody>
                                                    <tr>
                                                        <th width="10%">{{trans('general.sr_no')}}</th>
                                                        <th width="40%">{{trans('general.gn_courses')}}</th>
                                                        <th width="40%">{{trans('general.gn_teachers')}}</th>
                                                        <th width="10%">{{trans('general.gn_action')}}</th>
                                                    </tr>
                                                    <?php $i = 1;?>
                                                    @foreach($v as $vk => $vv)
                                                    <tr>
                                                        <td>{{$i}}</td>
                                                        <td>{{$vv['crs_CourseName']}}</td>
                                                        <td>{{$vv['crs_Teachers']}}</td>
                                                        <td>
                                                            <a title="{{trans('general.gn_delete')}}" onclick="triggerCourseDelete({{$vv['pkCsa']}})" href="javascript:void(0)"><img src="{{asset('images/ic_delete.png')}}"></a>
                                                        </td>
                                                    </tr>
                                                    <?php $i++; ?>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                        @endforeach
                                        @endif

                                        @endif

                                        @if(count($foriegnCourses))
                                        <hr>
                                        @foreach ($foriegnCourses as $k => $v)
                                        <p class="mt-4"><strong>{{trans('general.gn_foreign_language_courses')}}</strong></p>
                                        <p class="label"><strong>{{trans('sidebar.sidebar_nav_flg')}} - {{$foriegnCourses[$k][0]['fon_Name']}}</strong></p>
                                        <div class="table-responsive mt-2">
                                            <table class="color_table">
                                                <tbody>
                                                    <tr>
                                                        <th width="10%">{{trans('general.sr_no')}}</th>
                                                        <th width="40%">{{trans('general.gn_courses')}}</th>
                                                        <th width="40%">{{trans('general.gn_teachers')}}</th>
                                                        <th width="10%">{{trans('general.gn_action')}}</th>
                                                    </tr>
                                                    <?php $i = 1;?>
                                                    @foreach ($v as $kk => $vv)
                                                    <tr>
                                                        <td>{{$i}}</td>
                                                        <td>{{$vv['crs_CourseName']}}</td>
                                                        <td>{{$vv['crs_Teachers']}}</td>
                                                        <td>
                                                            <a title="{{trans('general.gn_delete')}}" onclick="triggerCourseDelete({{$vv['pkCsa']}})" href="javascript:void(0)"><img src="{{asset('images/ic_delete.png')}}"></a>
                                                        </td>
                                                    </tr>
                                                    <?php $i++; ?>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                        @endforeach
                                        @endif

                                        @if(count($optionalCourses))
                                        <hr>
                                        @foreach ($optionalCourses as $k => $v)
                                        <p class="mt-4"><strong>{{trans('general.gn_optional_courses')}}</strong></p>
                                        <p class="label"><strong>{{trans('sidebar.sidebar_nav_ocg')}} - {{$optionalCourses[$k][0]['ocg_Name']}}</strong></p>
                                        <div class="table-responsive mt-2">
                                            <table class="color_table">
                                                <tbody>
                                                    <tr>
                                                        <th width="10%">{{trans('general.sr_no')}}</th>
                                                        <th width="40%">{{trans('general.gn_courses')}}</th>
                                                        <th width="40%">{{trans('general.gn_teachers')}}</th>
                                                        <th width="10%">{{trans('general.gn_action')}}</th>
                                                    </tr>
                                                    <?php $i = 1;?>
                                                    @foreach ($v as $kk => $vv)
                                                    <tr>
                                                        <td>{{$i}}</td>
                                                        <td>{{$vv['crs_CourseName']}}</td>
                                                        <td>{{$vv['crs_Teachers']}}</td>
                                                        <td>
                                                            <a title="{{trans('general.gn_delete')}}" onclick="triggerCourseDelete({{$vv['pkCsa']}})" href="javascript:void(0)"><img src="{{asset('images/ic_delete.png')}}"></a>
                                                        </td>
                                                    </tr>
                                                    <?php $i++; ?>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                        @endforeach
                                        @endif

                                        @if(count($facultativeCourses))
                                        <hr>
                                        @foreach ($facultativeCourses as $k => $v)
                                        <p class="mt-4"><strong>{{trans('general.gn_facultative_courses')}}</strong></p>
                                        <p class="label"><strong>{{trans('sidebar.sidebar_nav_fcg')}} - {{$facultativeCourses[$k][0]['fcg_Name']}}</strong></p>
                                        <div class="table-responsive mt-2">
                                            <table class="color_table">
                                                <tbody>
                                                    <tr>
                                                        <th width="10%">{{trans('general.sr_no')}}</th>
                                                        <th width="40%">{{trans('general.gn_courses')}}</th>
                                                        <th width="40%">{{trans('general.gn_teachers')}}</th>
                                                        <th width="10%">{{trans('general.gn_action')}}</th>
                                                    </tr>
                                                    <?php $i = 1;?>
                                                    @foreach ($v as $kk => $vv)
                                                    <tr>
                                                        <td>{{$i}}</td>
                                                        <td>{{$vv['crs_CourseName']}}</td>
                                                        <td>{{$vv['crs_Teachers']}}</td>
                                                        <td>
                                                            <a title="{{trans('general.gn_delete')}}" onclick="triggerCourseDelete({{$vv['pkCsa']}})" href="javascript:void(0)"><img src="{{asset('images/ic_delete.png')}}"></a>
                                                        </td>
                                                    </tr>
                                                    <?php $i++; ?>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                        @endforeach
                                        @endif

                                        @if(count($generalCourses))
                                        <hr>
                                        @foreach ($generalCourses as $k => $v)
                                        <p class="mt-4"><strong>{{trans('general.gn_general_courses')}}</strong></p>
                                        <p class="label"><strong>{{trans('sidebar.sidebar_nav_gpg')}} - {{$generalCourses[$k][0]['gpg_Name']}}</strong></p>
                                        <div class="table-responsive mt-2">
                                            <table class="color_table">
                                                <tbody>
                                                    <tr>
                                                        <th width="10%">{{trans('general.sr_no')}}</th>
                                                        <th width="40%">{{trans('general.gn_courses')}}</th>
                                                        <th width="40%">{{trans('general.gn_teachers')}}</th>
                                                        <th width="10%">{{trans('general.gn_action')}}</th>
                                                    </tr>
                                                    <?php $i = 1;?>
                                                    @foreach ($v as $kk => $vv)
                                                    <tr>
                                                        <td>{{$i}}</td>
                                                        <td>{{$vv['crs_CourseName']}}</td>
                                                        <td>{{$vv['crs_Teachers']}}</td>
                                                        <td>
                                                            <a title="{{trans('general.gn_delete')}}" onclick="triggerCourseDelete({{$vv['pkCsa']}})" href="javascript:void(0)"><img src="{{asset('images/ic_delete.png')}}"></a>
                                                        </td>
                                                    </tr>
                                                    <?php $i++; ?>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                        @endforeach
                                        @endif
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
	    </div>
    </div>
</div>

<div class="theme_modal modal fade" id="delete_prompt" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <img src="{{asset('images/ic_close_bg.png')}}" class="modal_top_bg">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <img src="{{asset('images/ic_close_circle_white.png')}}">
                </button>
                    <div class="row">
                        <div class="col-lg-1"></div>
                        <div class="col-lg-10">
                            <h5 class="modal-title" id="exampleModalCenterTitle">{{trans('general.gn_delete')}}</h5>
                            <div class="form-group text-center">
                                <label>{{trans('general.gn_delete_prompt')}}</label>
                                <input type="hidden" id="did">
                            </div>
                            <div class="text-center modal_btn ">
                                <button style="display: none;" class="theme_btn show_delete_modal full_width small_btn" data-toggle="modal" data-target="#delete_prompt">{{trans('general.gn_delete')}}</button>
                                <button type="button" onclick="confirmCourseDelete()" class="theme_btn">{{trans('general.gn_yes')}}</button>
                                <button type="button" data-dismiss="modal" class="theme_btn red_btn">{{trans('general.gn_no')}}</button>
                            </div>
                        </div>
                        <div class="col-lg-1"></div>
                    </div>
            </div>
        </div>
    </div>
</div>
<script>
var listing_url = "{{route('fetch-classcreation-lists')}}";
var class_creation_sem_url = "{{route('fetch-classcreationsem-lists')}}";
</script>
@endsection

@push('datatable-scripts')
<!-- Include this Page JS -->
<script type="text/javascript" src="{{ asset('js/dashboard/class_creation.js') }}"></script>
@endpush