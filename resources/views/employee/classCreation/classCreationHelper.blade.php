@section('course_detail')
@if(isset($course_detail))
<?php $i = 1;
$tmpStuCourse = [];
$tmpStuCourse = Session::get('stu_Courses_'.$pkClr);
?>
<div class="table-responsive mt-2">
    <input type="hidden" name="graId" id="graId" value="{{$pkGra}}">
    <input type="hidden" name="edpId" id="edpId" value="{{$pkEpl}}">
    <table class="color_table">
        <tbody>
            <tr>
                <th width="15%">{{trans('general.sr_no')}}</th>
                <th width="50%">{{trans('general.gn_courses')}}</th>
                <th>{{trans('general.gn_week_hours')}}</th>
            </tr>
            @foreach($course_detail->mandatoryCourse as $k => $v)
             <tr>
                <td>{{$i}}</td>
                <td>{{$v->mandatoryCourseGroup->crs_CourseName}} - {{$course_detail->epl_EducationPlanName_en}}</td>
                <td>{{$v->emc_hours}}</td>
            </tr>
            <?php $i++;?>
            @endforeach
        </tbody>
    </table>
</div>
@endif
@endsection

@section('step_2')
@if(isset($data2))
<?php $i = 1;?>
    <table class="color_table class_seleted_students sel_stu_elem">
        <tbody><tr>
            <th>{{trans('general.sr_no')}}</th>
            <th>ID</th>
            <th>{{trans('general.gn_name')}}</th>
            <th>{{trans('general.gn_grade')}}</th>
            <th>{{trans('general.gn_education_plan')}}</th>
            <th><div class="action">{{trans('general.gn_action')}}</div></th>
        </tr>
        @foreach($data2 as $k => $v)
        <tr class="stu stu_{{$v[0]->studentEnroll->pkSte}}">
            <td>{{$i}}</td>
            <td>@if($v[0]->studentEnroll->student->stu_StudentID != null) {{$v[0]->studentEnroll->student->stu_StudentID}} @else {{$v[0]->studentEnroll->student->stu_TempCitizenId}} @endif</td>
            <td>{{$v[0]->studentEnroll->student->full_name}}</td>
            <td>{{$v[0]->studentEnroll->grade->gra_GradeNumeric}}</td>
            <td>{{$v[0]->studentEnroll->educationProgram->edp_Name}} - {{$v[0]->studentEnroll->educationPlan->epl_EducationPlanName}}</td>
            <td>
                <input type="hidden" name="stu_ids[]" value="{{$v[0]->studentEnroll->grade->pkGra}}_{{$v[0]->studentEnroll->pkSte}}">
                <input type="hidden" name="gra_id[]" value="{{$v[0]->studentEnroll->grade->pkGra}}">
                <input type="hidden" name="epl_ids[]" value="{{$v[0]->studentEnroll->educationPlan->pkEpl}}">
                <!-- <button onclick="triggerEduPlan(this)" data-gra="{{$v[0]->studentEnroll->grade->pkGra}}" data-stu="{{$v[0]->studentEnroll->pkSte}}" data-edu="{{$v[0]->studentEnroll->educationPlan->pkEpl}}" type="button" class="theme_btn min_btn btn_margin">
                {{trans('general.gn_view_courses')}}
                </button> -->
                <a data-id="{{$v[0]->studentEnroll->pkSte}}" onclick="removeSelStu(this)" href="javascript:void(0)" class="theme_btn red_btn min_btn btn_margin">{{trans('general.gn_delete')}}</a>
            </td>
        </tr>
        <?php $i++;?>
        @endforeach
    </tbody>
    </table>
@endif
@endsection

@section('step_3')
@if(isset($data3))
    <?php $j = 1;
    ?>
    @if($showBulkTeacherSelectionButton)
    <div class="text-right">
        <button style="font-size:15px;" type="button" onclick="autoTeacherSelection(this)" class="theme_btn small_btn" data-hometeacherid={{$homeroomteacher}}>{{trans('general.gn_assign_homeroomteacher_button')}}</button>
    </div>
    @endif

    @foreach ($data3 as $k => $v)
    @if(count($v->educationPlansMandatoryCourse))
    <br>
    <p id="{{$v->pkGra}}"><strong>
    @if($mixedCourseClass)
    {{trans('general.gn_common_courses')}} {{trans('general.gn_of')}}
    @endif
    {{trans('general.gn_grade')}} - {{$v->gra_GradeNumeric}}
    </strong></p>
    <div class="table-responsive mt-2">
    <table class="color_table">
        <tbody>
            <tr>
                <th>{{trans('general.sr_no')}}</th>
                <th>{{trans('general.gn_courses')}}</th>
                <th>{{trans('general.gn_week_hours')}}</th>
                <th width="30%">{{trans('general.gn_teachers')}}</th>
                <th>{{trans('general.gn_default')}}</th>
            </tr>
                <?php $i = 1;?>
            @foreach ($v->educationPlansMandatoryCourse as $kmc => $vmc)
                <tr data-gra="{{$v->pkGra}}" data-edp="{{$vmc->educationPlan->pkEpl}}" data-crs="{{$vmc->mandatoryCourseGroup->pkCrs}}">
                    <td>{{$i}}</td>
                    <td>
                        @if($mixedCourseClass)
                            {{$vmc->mandatoryCourseGroup->crs_CourseName}}
                        @else
                            {{$vmc->mandatoryCourseGroup->crs_CourseName}} - {{$vmc->educationPlan->epl_EducationPlanName}}
                        @endif
                    </td>
                    <td>{{$vmc->emc_hours}}</td>
                    <td>
                        <div class="form-group">
                            <input type="hidden" name="courses[{{$v->pkGra}}_{{$vmc->fkEplCrs}}]" value="{{$v->pkGra}}_{{$vmc->fkEplCrs}}">
                            <select multiple required id="fkCtcEeg_{{$v->pkGra}}_{{$vmc->fkEplCrs}}" @if($vmc->emc_default == 'No' && !$mixedCourseClass) disabled @endif name="fkCtcEeg_{{$v->pkGra}}_{{$vmc->fkEplCrs}}[]" class="form-control icon_control dropdown_control select2_multi" placeholder="{{trans('general.gn_teachers')}}">
                                @foreach($employees as $ek => $ev)
                                <option @if(!empty($existTeacher) && isset($existTeacher[$v->pkGra]))@if(array_key_exists($vmc->fkEplCrs,$existTeacher[$v->pkGra]))@if(in_array($ev->pkEen,$existTeacher[$v->pkGra][$vmc->fkEplCrs])) selected @endif @endif @endif value="{{$ev->pkEen}}">{{$ev->employee->emp_EmployeeName}} {{$ev->employee->emp_EmployeeSurname}}</option>
                                @endforeach
                            </select>
                        </div>
                    </td>
                    <td>
                        <div class="form-group form-check">
                            <input type="checkbox" class="defaultCrsTeacherChckbox form-check-input" value="{{$v->pkGra}}_{{$vmc->fkEplCrs}}" @if($vmc->emc_default == 'Yes') checked @endif>
                            <label class="custom_checkbox"></label>
                        </div>
                        {{-- @if(!empty($indicationCourses))
                            @foreach($indicationCourses as $gra => $crs)
                                @foreach($crs as $crk => $crv)
                                    @for($m=0;$m<count($crv);$m++)
                                        @if($vmc->mandatoryCourseGroup->pkCrs == $crv[$m] && $crk == $vmc->educationPlan->pkEpl && $gra == $v->pkGra)
                                        <img alt="" src="{{asset('images/ic_warning.png')}}">
                                        @endif
                                    @endfor
                                @endforeach
                            @endforeach
                        @endif --}}
                    </td>
                </tr>
                <?php $i++;$j++; ?>
            @endforeach
        </tbody>
    </table>

    @if($mixedCourseClass && count($educationPlanDetailsBackup))
    <p id="{{$v->pkGra}}"><strong>
    {{trans('general.gn_remain_course')}} {{trans('general.gn_of')}} {{trans('general.gn_grade')}} -  {{$v->gra_GradeNumeric}}
    </strong></p>
    <div class="table-responsive mt-2">
    <table class="color_table">
            <tr>
                <th>{{trans('general.sr_no')}}</th>
                <th>{{trans('general.gn_courses')}}</th>
                <th>{{trans('general.gn_week_hours')}}</th>
            </tr>
            <tbody>
                <?php $i = 1;?>
                @foreach ($educationPlanDetailsBackup as $ks => $vs)
                @if($vs->pkGra == $v->pkGra)
                @foreach ($vs->educationPlansMandatoryCourse as $kmc => $vmc)
                <tr data-gra="{{$v->pkGra}}" data-edp="{{$vmc->educationPlan->pkEpl}}" data-crs="{{$vmc->mandatoryCourseGroup->pkCrs}}">
                    <td>{{$i}}</td>
                    <td>
                    {{$vmc->mandatoryCourseGroup->crs_CourseName}} - {{$vmc->educationPlan->epl_EducationPlanName}}
                    </td>
                    <td>{{$vmc->emc_hours}}</td>
                </tr>
                <?php $i++;$j++; ?>
                @endforeach
                @endif
                @endforeach
            </tbody>
    </table>
    @endif
    @endif
    @endforeach
@endif
@endsection

@section('step_4')
@if(isset($data4))
@foreach ($data4 as $k => $v)
    <p><strong>
    {{trans('general.gn_grade')}} - {{$k}}
    </strong></p>
    <div class="table-responsive mt-2">
        <table class="color_table">
            <tr>
                <th>{{trans('general.sr_no')}}</th>
                <th>{{trans('general.gn_student_name')}}</th>
                <th>{{trans('general.gn_education_plan')}}</th>
                <th><div class="action">{{trans('general.gn_action')}}</div></th>
            </tr>
            <tbody>
                <?php $i = 1; ?>
                @foreach($v as $kk => $vv)
                <tr>
                    <td>{{$i}}</td>
                    <td>{{$vv['stuName']}}</td>
                    <td>{{$vv['edp_Name']}}</td>
                    <td><button onclick="triggerEduPlan(this)" data-gra="{{$vv['pkGra']}}" data-stu="{{$vv['pkSte']}}" data-edu="{{$vv['pkEpl']}}" type="button" class="theme_btn min_btn btn_margin">{{trans('general.gn_view_courses')}}</button></td>
                </tr>
                <?php $i++; ?>
                @endforeach
            </tbody>
        </table>
    </div>
@endforeach
@endif
@endsection

@section('step_5')
@if(isset($data5))
<?php $i = 1; $j = 1;
    $tmpGra = [];
    $step3data = Session::get('cc_step_3_'.$pkClr);
    foreach ($data5->classCreationGrades as $k => $v) {
        $tmpGra[] = $v->grade->gra_GradeNumeric;
    }

    if(!empty($tmpGra)){
        asort($tmpGra);
        $na = array_unique($tmpGra);
        $grades = implode(', ', $na);
    }else{
        $grades = '';
    }
?>
    <div class="col-md-12">
        <div class="card-grey">
            <div class="row">
                <div class="col-md-6 col-lg-3">
                    <div class="form-group">
                        <p class="label">{{trans('general.gn_school_year')}} :</p>
                        <p class="value">{{$data5->classCreationSchoolYear->sye_NameCharacter}}</p>
                    </div>
                </div>
                <div class="col-md-6 col-lg-3">
                    <div class="form-group">
                        <p class="label">{{trans('general.gn_homeroom_teacher')}} :</p>
                        <p class="value">{{$data5->classCreationSemester[0]->homeRoomTeacher[0]->employeesEngagement->employee->emp_EmployeeName}} {{$data5->classCreationSemester[0]->homeRoomTeacher[0]->employeesEngagement->employee->emp_EmployeeSurname}}</p>
                    </div>
                </div>
                <div class="col-md-6 col-lg-3">
                    <div class="form-group">
                        <p class="label">{{trans('general.gn_semester')}} :</p>
                        <p class="value">{{$data5->classCreationSemester[0]->semester->edp_EducationPeriodName}}</p>
                    </div>
                </div>
                <div class="col-md-6 col-lg-3">
                    <div class="form-group">
                        <p class="label">{{trans('general.gn_school_grade')}} :</p>
                        <p class="value">{{$grades}}</p>
                    </div>
                </div>
                <div class="col-md-6 col-lg-3">
                    <div class="form-group">
                        <p class="label">{{trans('general.gn_chief_student')}} :</p>
                        <p class="value">{{$data5->classCreationSemester[0]->chiefStudent->student->full_name ?? '-'}}</p>
                    </div>
                </div>
                <div class="col-md-6 col-lg-3">
                    <div class="form-group">
                        <p class="label">{{trans('general.gn_treasure_student')}} :</p>
                        <p class="value">{{$data5->classCreationSemester[0]->treasureStudent->student->full_name ?? '-'}}</p>
                    </div>
                </div>
                <div class="col-md-6 col-lg-3">
                    <div class="form-group">
                        <p class="label">{{trans('general.gn_class')}} :</p>
                        <p class="value">{{$data5->classCreationClasses->cla_ClassName}}</p>
                    </div>
                </div>
            </div>
        </div>
        <p class="mt-3"><strong>{{trans('general.gn_students_of_class')}} {{$data5->classCreationClasses->cla_ClassName}}</strong></p>
        <div class="table-responsive mt-2">
            <table class="color_table">
                <tbody><tr>
                    <th>{{trans('general.sr_no')}}</th>
                    <th>ID</th>
                    <th>{{trans('general.gn_name')}}</th>
                    <th>{{trans('general.gn_grade')}}</th>
                    <th>{{trans('general.gn_education_plan')}}</th>
                </tr>
                @foreach($students as $k => $v)
                <tr>
                    <td>{{$k+1}}</td>
                    <td>@if($v->student->stu_StudentID != null) {{$v->student->stu_StudentID}} @else {{$v->student->stu_TempCitizenId}} @endif</td>
                    <td>{{$v->student->full_name}}</td>
                    <td>{{$v->grade->gra_GradeNumeric}}</td>
                    <td>{{$v->educationProgram->edp_Name}} - {{$v->educationPlan->epl_EducationPlanName}}</td>
                </tr>
                @endforeach
            </tbody>
            </table>
        </div>
        <p class="mt-3"><strong>{{trans('general.gn_class_creation_step_3_subtext')}}</strong></p>

            @foreach ($courses as $k => $v)
            @if(count($v->educationPlansMandatoryCourse))
            <br>
            <p><strong></strong>{{trans('general.gn_grade')}} - {{$v->gra_GradeNumeric}}</p>
            <div class="table-responsive mt-2">
            <table class="color_table">
                <tbody>
                    <tr>
                        <th>{{trans('general.sr_no')}}</th>
                        <th>{{trans('general.gn_courses')}}</th>
                        <th>{{trans('general.gn_week_hours')}}</th>
                        <th width="30%">{{trans('general.gn_teachers')}}</th>
                        <th>{{trans('general.gn_default')}}</th>
                    </tr>
                        <?php $i = 1;?>
                    @foreach ($v->educationPlansMandatoryCourse as $kmc => $vmc)

                        <tr>
                            <td>{{$i}}</td>
                            <td>{{$vmc->mandatoryCourseGroup->crs_CourseName}} - {{$vmc->educationPlan->epl_EducationPlanName}}</td>
                            <td>{{$vmc->emc_hours}}</td>
                            <td>
                                <div class="form-group">
                                    @if(isset($step3data['fkCtcEeg_'.$v->pkGra.'_'.$vmc->fkEplCrs]))
                                    <input type="hidden" name="courses[{{$v->pkGra}}_{{$vmc->fkEplCrs}}]" value="{{$v->pkGra}}_{{$vmc->fkEplCrs}}_{{$vmc->educationPlan->pkEpl}}">
                                    @endif
                                    <select multiple id="fkCtcEeg_{{$v->pkGra}}_{{$vmc->fkEplCrs}}" name="fkCtcEeg_{{$v->pkGra}}_{{$vmc->fkEplCrs}}[]" class="form-control icon_control dropdown_control select2_multi5 input_disable" placeholder="{{trans('general.gn_teachers')}}">
                                        @foreach($employees as $ek => $ev)
                                        <option @if(isset($step3data['fkCtcEeg_'.$v->pkGra.'_'.$vmc->fkEplCrs]) && in_array($ek,$step3data['fkCtcEeg_'.$v->pkGra.'_'.$vmc->fkEplCrs])) selected @endif value="{{$ek}}">{{$ev}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </td>
                            <td>
                                <div class="form-group form-check">
                                    <input type="checkbox" disabled class="form-check-input" @if(isset($step3data['fkCtcEeg_'.$v->pkGra.'_'.$vmc->fkEplCrs])) checked @endif>
                                    <label class="custom_checkbox"></label>
                                </div>
                            </td>
                        </tr>
                        <?php $i++;$j++; ?>
                    @endforeach

                </tbody>
            </table>
            @endif
            @endforeach

        <ul class="list-unstyled list-inline btn-flex mt-3">
            <li><button type="button" class="small_btn theme_btn btn-border prev-step" onclick="prevTab(4)">{{trans('general.gn_previous')}}</button></li>
            <li><button type="submit" class="small_btn theme_btn next-step">{{trans('general.gn_submit')}}</button></li>
        </ul>
    </div>

@endif
@endsection