@extends('layout.app_with_login')
@section('title', trans('sidebar.sidebar_nav_class_creation'))
@section('script', asset('js/dashboard/class_creation.js'))
@section('content')
<?php
$selGrades = [];
$selCla = '';
$selVsc = '';
$pkClr = '';
$pkCcs = '';
$selNote = '';
$pkCcs = '';
$fkCcsSen = '';
$fkCcsSent = '';
$defaultYear = '';
$hometeacher = '';
$chiefStuName = '';
$chiefStuGra = '';
$chiefTreName = '';
$chiefTreGra = '';
$teacherstartdate = '';
if(isset($classDetails)){
    $hometeacher = $TeacherEmp;
    $teacherstartdate = $start_date;
    $pkClr = $classDetails->pkClr;
    foreach ($classDetails->classCreationGrades as $k => $v) {
        $selGrades[] = $v->fkCcgGra;
    }
    $pkCcs = $classDetails->classCreationSemester[0]->pkCcs;
    $fkCcsSen = $classDetails->classCreationSemester[0]->fkCcsSen;
    $chiefStuName = $classDetails->classCreationSemester[0]->chiefStudent->student->stuName ?? '';
    $chiefStuGra = $classDetails->classCreationSemester[0]->chiefStudent->grade->gra_GradeNumeric ?? '';
    $chiefTreName = $classDetails->classCreationSemester[0]->treasureStudent->student->stuName ?? '';
    $chiefTreGra = $classDetails->classCreationSemester[0]->treasureStudent->grade->gra_GradeNumeric ?? '';
    $fkCcsSent = $classDetails->classCreationSemester[0]->fkCcsSent;
    $selCla = $classDetails->fkClrCla;
    $selNote = $classDetails->clr_Notes;
    $selVsc = $classDetails->fkClrVsc;
    $defaultYear = $classDetails->classCreationSchoolYear->pkSye;
    $currentSem = $classDetails->classCreationSemester[0]->fkCcsEdp;
}

?>
<!-- Page Content  -->
<div class="section">
	<div class="container-fluid">
		<div class="row">
			<div class="col-12 mb-3">
               <h2 class="title"><span>{{trans('sidebar.sidebar_nav_organization')}} > </span><a class="no_sidebar_active" href="{{url('/employee/classcreation')}}"><span>{{trans('sidebar.sidebar_nav_class_creation')}} > </span></a>@if(isset($classDetails)){{trans('general.gn_edit')}}@else{{trans('general.gn_add')}}@endif</h2>
            </div>
        <input type="hidden" id="teacher_txt" value="{{trans('general.gn_teacher')}}">
        <input type="hidden" id="principal_txt" value="{{trans('general.gn_principal')}}">
        <input type="hidden" id="stu_sel_valid_txt" value="{{trans('message.msg_student_select_valid')}}">
        <input type="hidden" id="stu_sel_txt" value="{{trans('message.msg_sel_stu')}}">
        <input type="hidden" id="course_select_txt" value="{{trans('validation.validate_course_select')}}">
        <div class="col-12">
            <div class="white_box pt-5 pb-5">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-10 offset-lg-1">
                            <div class="process">
                                <div class="process-row nav nav-tabs">
                                    <div class="process-step">
                                        <button type="button" class="btn-round-1 btn-process btn-round active" href="#menu1">1</button>
                                    </div>
                                    <div class="process-step">
                                        <button type="button" class="btn-round-2 btn-process btn-round" href="#menu2">2</button>
                                    </div>
                                    <div class="process-step">
                                        <button type="button" class="btn-round-3 btn-process btn-round" href="#menu3">3</button>
                                    </div>
                                    <div class="process-step">
                                        <button type="button" class="btn-round-4 btn-process btn-round" href="#menu4">4</button>
                                    </div>
                                    <div class="process-step">
                                        <button type="button" class="btn-round-5 btn-process btn-round" href="#menu5">5</button>
                                    </div>
                                    <!-- <div class="process-step">
                                        <button type="button" class="btn-round-6 btn-process btn-round" href="#menu6">6</button>
                                    </div> -->
                                </div>
                            </div>

                            <div class="tab-content mt-5">
                                <div id="menu1" class="tab-pane active in">
                                    <form name="class-creation-step-1">
                                        <input type="hidden" id="fkClrSch" value="{{$mainSchool}}">
                                        <input type="hidden" id="pkClr" value="{{$pkClr}}">
                                        <input type="hidden" id="pkCcs" value="{{$pkCcs}}">
                                        <div class="row">
                                            <div class="col-lg-8 offset-lg-2">
                                                <div class="form-group">
                                                    <label>{{trans('general.gn_school_year')}} *</label>
                                                    <select disabled id="fkClrSye" name="fkClrSye" class="form-control icon_control dropdown_control">
                                                        <option value="">{{trans('general.gn_select')}}</option>
                                                        @foreach($schoolYears as $k => $v)
                                                        <option
                                                        @if(!empty($defaultYear))
                                                            @if($defaultYear == $v->pkSye)selected="selected"
                                                            @endif
                                                        @else
                                                            @if($v->sye_DefaultYear == 1) selected="selected"
                                                            @endif
                                                        @endif
                                                        value="{{$v->pkSye}}">{{$v->sye_NameCharacter}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <label>{{trans('general.gn_semester')}} *</label>
                                                    <select style="width:100%" readonly id="fkClrEdp" name="fkClrEdp" class="input_disable form-control icon_control dropdown_control">
                                                        <option value="">{{trans('general.gn_select')}}</option>
                                                        @foreach($semesters as $k => $v)
                                                        <option @if($currentSem == $v->pkEdp) selected="selected" @endif value="{{$v->pkEdp}}">{{$v->edp_EducationPeriodName}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <label>{{trans('general.gn_school_grade')}} *</label>
                                                    <select style="width:100%" multiple="multiple" required id="fkClrGra" name="fkClrGra[]" class="form-control icon_control dropdown_control select2_drop">
                                                        @foreach($grades as $k => $v)
                                                        <option @if(in_array($v->pkGra,$selGrades)) selected @endif value="{{$v->pkGra}}">{{$v->gra_GradeNumeric}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <label>{{trans('general.gn_class')}} *</label>
                                                    <select style="width:100%" id="fkClrCla" name="fkClrCla" class="form-control icon_control dropdown_control">
                                                        <option value="">{{trans('general.gn_select')}}</option>
                                                        @foreach($classes as $k => $v)
                                                        <option @if($selCla == $v->pkCla) selected @endif value="{{$v->pkCla}}">{{$v->cla_ClassName}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>

                                                <div class="form-group form-check">
                                                    <input type="checkbox" class="form-check-input" id="is_village_school" @if(!empty($selVsc)) checked @endif>
                                                    <label class="custom_checkbox"></label>
                                                    <label class="form-check-label label-text" for="exampleCheck1">{{trans('general.gn_is_village_school')}}</label>
                                                </div>

                                                <div class="form-group village_schools_drp @if(empty($selVsc)) hide_content @endif">
                                                    <label>{{trans('general.gn_village_school')}} *</label>
                                                    <select style="width:100%" id="fkClrVsc" name="fkClrVsc" class="form-control icon_control dropdown_control">
                                                        <option value="">{{trans('general.gn_select')}}</option>
                                                        @foreach($villageSchools as $k => $v)
                                                        <option @if($selVsc == $v->pkVsc) selected @endif value="{{$v->pkVsc}}">{{$v->vsc_VillageSchoolName}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>

                                                <div class="form-group">
                                                    <label>{{trans('general.gn_homeroom_teacher')}} *</label>
                                                    <select style="width:100%" required id="empId" name="empId" class="form-control icon_control dropdown_control select2_drop">
                                                        <option value="">{{trans('general.gn_select')}}</option>
                                                        @foreach($employees as $k => $v)
                                                            <option @if($v->id == $hometeacher) selected @endif value="{{$v->id}}">{{$v->empName}} - {{$v->empId}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>

                                                <div class="form-group">
                                                    <label>{{trans('general.gn_start_date')}} *</label>
                                                    <input required type="text" id="start_date" name="start_date" class="form-control datepicker icon_control date_control" value="{{$teacherstartdate ?? ''}}">
                                                </div>

                                                <div class="form-group">
                                                    <label>{{trans('general.gn_enagement_note')}}</label>
                                                    <input type="text" id="een_Notes" name="een_Notes" class="form-control" placeholder="{{trans('general.gn_enter')}} {{trans('general.gn_note')}}" value="{{$een_Notes ?? ''}}">
                                                </div>

                                                <div class="form-group">
                                                    <input type="hidden" id="edit_fkCcsSen" data-name="{{$chiefStuName}}" data-gra="{{$chiefStuGra}}" value="{{$fkCcsSen}}">
                                                    <label>{{trans('general.gn_chief_student')}}</label>
                                                    <select style="width:100%" id="fkCcsSen" name="fkCcsSen" class="form-control icon_control dropdown_control">
                                                        <option value="">{{trans('general.gn_select')}}</option>
                                                    </select>
                                                </div>

                                                <div class="form-group">
                                                    <input type="hidden" id="edit_fkCcsSent" data-name="{{$chiefTreName}}" data-gra="{{$chiefTreGra}}" value="{{$fkCcsSent}}">
                                                    <label>{{trans('general.gn_treasure_student')}}</label>
                                                    <select style="width:100%" id="fkCcsSent" name="fkCcsSent" class="form-control icon_control dropdown_control">
                                                        <option value="">{{trans('general.gn_select')}}</option>
                                                    </select>
                                                </div>

                                                <div class="form-group">
                                                    <label>{{trans('general.gn_note')}}</label>
                                                    <input type="text" id="clr_Notes" name="clr_Notes" class="form-control" placeholder="{{trans('general.gn_enter')}} {{trans('general.gn_note')}}" value="{{$selNote}}">
                                                </div>
                                                <ul class="list-unstyled list-inline btn-flex mt-3" >
                                                 <li style="visibility: hidden;"><button type="button" class="small_btn theme_btn prev-step">{{trans('general.gn_previous')}}</button></li>
                                                 <li><button type="submit" class="small_btn theme_btn next-step">{{trans('general.gn_next')}}</button></li>
                                                </ul>
                                           </div>
                                        </div>
                                    </form>
                                </div>

                                <div id="menu2" class="tab-pane fade">
                                    <form name="class-creation-step-2">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <div class="row align-items-end">
                                                    <div class="col-md-3 mb-3">
                                                        <div class="form-group">
                                                            <label>{{trans('general.gn_search_student')}}</label>
                                                            <input type="text" id="search_student" maxlength="30" class="form-control icon_control search_control" placeholder="{{trans('general.gn_student')}} ID, {{trans('general.gn_name')}}">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3 mb-3" style="display:none" id="oldClassDropdownDiv">
                                                        <div class="form-group">
                                                                <label>{{trans('general.gn_select_previous_year_class')}}</label>
                                                                <select class="form-control icon_control dropdown_control" id="oldClassDropdown" name="oldClassDropdown">
                                                                </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3 mb-3" style="display:none" id="filterbtnDiv">
                                                        <div class="form-group">
                                                        <label></label>
                                                            <button type="button" class="small_btn theme_btn btn-border" id="filter_students_btn">{{trans('general.gn_add_students')}}</button>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="table-responsive mt-2">
                                                    <table id="student_listing" class="display" style="width:100%">
                                                        <thead>
                                                            <tr>
                                                                <th>{{trans('general.sr_no')}}.</th>
                                                                <th>Id</th>
                                                                <th>{{trans('general.gn_student_name')}}</th>
                                                                <th>{{trans('general.gn_grade')}}</th>
                                                                <th>{{trans('general.gn_education_plan')}}</th>
                                                                <th><div class="action">{{trans('general.gn_action')}}</div></th>
                                                            </tr>
                                                        </thead>
                                                    </table>
                                                </div>
                                                <p class="mt-2 sel_stu_elem hide_content"><strong>{{trans('general.gn_selected_students')}}</strong></p>
                                                <div class="table-responsive mt-2 step_2_resp">
                                                    <table class="color_table class_seleted_students sel_stu_elem hide_content">
                                                        <tbody>
                                                            <tr>
                                                                <th>{{trans('general.sr_no')}}.</th>
                                                                <th>Id</th>
                                                                <th>{{trans('general.gn_student_name')}}</th>
                                                                <th>{{trans('general.gn_grade')}}</th>
                                                                <th>{{trans('general.gn_education_plan')}}</th>
                                                                <th><div class="action">{{trans('general.gn_action')}}</div></th>
                                                            </tr>

                                                        </tbody>
                                                    </table>
                                                </div>
                                                <ul class="list-unstyled list-inline btn-flex mt-3">
                                                 <li><button type="button" class="small_btn theme_btn btn-border prev-step" onclick="prevTab(1)" data-href="menu1">{{trans('general.gn_previous')}}</button></li>
                                                 <li><button type="submit" class="small_btn theme_btn next-step">{{trans('general.gn_next')}}</button></li>
                                                </ul>
                                           </div>
                                       </div>
                                   </form>
                                </div>

                                <div id="menu3" class="tab-pane fade">
                                    <form name="class-creation-step-3">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <div class="table-responsive mt-2 step_3_resp">
                                                    <table class="color_table">
                                                        <tbody>
                                                            <tr>
                                                                <th>{{trans('general.sr_no')}}</th>
                                                                <th>{{trans('general.gn_courses')}}</th>
                                                                <th>{{trans('general.gn_week_hours')}}</th>
                                                                <th>{{trans('general.gn_teachers')}}</th>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                                <ul class="list-unstyled list-inline btn-flex mt-3">
                                                 <li><button type="button" class="small_btn theme_btn btn-border prev-step" onclick="prevTab(2)">{{trans('general.gn_previous')}}</button></li>
                                                 <li><button type="submit" class="small_btn theme_btn next-step">{{trans('general.gn_next')}}</button></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </form>
                                </div>

                                <div id="menu4" class="tab-pane fade">
                                    <form name="class-creation-step-4">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <p><strong>{{trans('general.gn_selected_students')}}</strong></p>
                                                <div class="table-responsive mt-2 step_4_resp">
                                                    <table class="color_table">
                                                        <tr>
                                                            <th>{{trans('general.sr_no')}}</th>
                                                            <th>{{trans('general.gn_student_name')}}</th>
                                                            <th>{{trans('general.gn_education_plan')}}</th>
                                                            <th><div class="action">{{trans('general.gn_action')}}</div></th>
                                                        </tr>
                                                    </table>
                                                </div>
                                                <ul class="list-unstyled list-inline btn-flex mt-3">
                                                 <li><button type="button" class="small_btn theme_btn btn-border prev-step" onclick="prevTab(3)">{{trans('general.gn_previous')}}</button></li>
                                                 <li><button type="submit" class="small_btn theme_btn next-step">{{trans('general.gn_next')}}</button></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </form>
                                </div>

                                <div id="menu5" class="tab-pane fade">
                                    <form name="class-creation-step-5">
                                        <div class="row step_5_resp">
                                            <div class="col-lg-8 offset-lg-2">
                                                <div class="form-group">
                                                    <label>{{trans('general.gn_homeroom_teacher')}} *</label>
                                                    <select class="form-control icon_control dropdown_control">
                                                        <option value="">{{trans('general.gn_select')}}</option>
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <label>{{trans('general.gn_chief_student')}} *</label>
                                                    <select class="form-control icon_control dropdown_control">
                                                        <option value="">{{trans('general.gn_select')}}</option>
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <label>{{trans('general.gn_treasure_student')}} *</label>
                                                    <select class="form-control icon_control dropdown_control">
                                                        <option value="">{{trans('general.gn_select')}}</option>
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <label>{{trans('general.gn_note')}}</label>
                                                    <input type="text" id="clr_Notes" name="clr_Notes" class="form-control" placeholder="{{trans('general.gn_enter')}} {{trans('general.gn_note')}}">
                                                </div>
                                                <ul class="list-unstyled list-inline btn-flex mt-3" >
                                                 <li style="visibility: hidden;"><button type="button" class="small_btn theme_btn prev-step" onclick="prevTab(4)">{{trans('general.gn_previous')}}</button></li>
                                                 <li><button type="submit" class="small_btn theme_btn next-step">{{trans('general.gn_next')}}</button></li>
                                                </ul>
                                           </div>
                                        </div>
                                    </form>
                                </div>

                                <div id="menu6" class="tab-pane fade">
                                    <form name="class-creation-step-6">
                                        <div class="row step_6_resp">
                                            <div class="col-md-12">
                                                <div class="card-grey">
                                                    <div class="row">
                                                        <div class="col-md-6 col-lg-3">
                                                            <div class="form-group">
                                                                <p class="label">{{trans('general.gn_school_year')}} :</p>
                                                                <p class="value">2019/20</p>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6 col-lg-3">
                                                            <div class="form-group">
                                                                <p class="label">{{trans('general.gn_homeroom_teacher')}} :</p>
                                                                <p class="value">Chinaza Akachi</p>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6 col-lg-3">
                                                            <div class="form-group">
                                                                <p class="label">{{trans('general.gn_semester')}} :</p>
                                                                <p class="value">1</p>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6 col-lg-3">
                                                            <div class="form-group">
                                                                <p class="label">{{trans('general.gn_school_grade')}} :</p>
                                                                <p class="value">5</p>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6 col-lg-3">
                                                            <div class="form-group">
                                                                <p class="label">{{trans('general.gn_chief_student')}} :</p>
                                                                <p class="value">Andrei Masharin</p>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6 col-lg-3">
                                                            <div class="form-group">
                                                                <p class="label">{{trans('general.gn_treasure_student')}} :</p>
                                                                <p class="value">Maria Trofimova</p>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6 col-lg-3">
                                                            <div class="form-group">
                                                                <p class="label">{{trans('general.gn_class')}} :</p>
                                                                <p class="value">1A</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <p class="mt-3"><strong>Students of Class 1A</strong></p>
                                                <div class="table-responsive mt-2">
                                                    <table class="color_table">
                                                        <tbody><tr>
                                                            <th>{{trans('general.sr_no')}}</th>
                                                            <th>ID</th>
                                                            <th>{{trans('general.gn_name')}}</th>
                                                            <th>{{trans('general.gn_grade')}}</th>
                                                            <th>{{trans('general.gn_education_plan')}}</th>
                                                        </tr>
                                                    </tbody>
                                                    </table>
                                                </div>
                                                <p class="mt-3"><strong>{{trans('general.gn_class_creation_step_3_subtext')}}</strong></p>
                                                <div class="table-responsive mt-2">
                                                    <table class="color_table">
                                                        <tbody><tr>
                                                            <th>{{trans('general.sr_no')}}</th>
                                                            <th>{{trans('general.gn_courses')}}</th>
                                                            <th>{{trans('general.gn_week_hours')}}</th>
                                                            <th>{{trans('general.gn_teachers')}}</th>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                                <ul class="list-unstyled list-inline btn-flex mt-3">
                                                    <li><button type="button" class="small_btn theme_btn btn-border prev-step" onclick="prevTab(4)">{{trans('general.gn_previous')}}</button></li>
                                                    <li><button type="button" class="small_btn theme_btn next-step">{{trans('general.gn_submit')}}</button></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
		</div>
	</div>
</div>
<button id="trigger_edu_pop" class="hide_content" type="button" data-toggle="modal" data-target="#eplan_prompt">trigg</button>

<div class="theme_modal modal fade" id="eplan_prompt" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <img src="{{asset('images/ic_close_bg.png')}}" class="modal_top_bg">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <img src="{{asset('images/ic_close_circle_white.png')}}">
                </button>
                    <form name="course_details" id="course_details">
                    <div class="row">
                        <div class="col-lg-1"></div>
                        <div class="col-lg-10">
                            <h5 class="modal-title" id="exampleModalCenterTitle">{{trans('general.gn_view_courses')}}</h5>
                            <div class="course_details">
                            </div>
                            <!-- <div class="text-center modal_btn ">
                                <button type="submit" class="theme_btn min_btn">{{trans('general.gn_submit')}}</button>
                                <button type="button" data-dismiss="modal" class="theme_btn red_btn min_btn">{{trans('general.gn_cancel')}}</button>
                            </div> -->
                        </div>
                        <div class="col-lg-1"></div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<input type="hidden" id="gradetxt" value="{{trans('general.gn_grade')}}">
<input type="hidden" id="bothOptionMessage" value="{{trans('general.gn_select_option_first')}}">
<input type="hidden" id="combineerror" value="{{trans('message.msg_combine_class_error')}}">
<input type="hidden" id="combinevillageerror" value="{{trans('message.msg_combine_villageclass_error')}}">
<script>
var listing_url = "{{route('fetch-classcreation-lists')}}";
var class_creation_sem_url = "{{route('fetch-classcreationsem-lists')}}";
</script>
@endsection

@push('datatable-scripts')
<!-- Include this Page JS -->
<script type="text/javascript" src="{{ asset('js/dashboard/class_creation.js') }}"></script>
@endpush