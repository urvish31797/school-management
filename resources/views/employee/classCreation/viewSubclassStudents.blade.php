@extends('layout.app_with_login')
@section('title', trans('general.gn_sub_class'))
@section('script', asset('js/dashboard/class_creation_sub_class.js'))
@section('content')
<?php
$Grades = '';
foreach ($mdata->classCreationGrades as $k => $v) {
    $selGrades[] = $v->grade->gra_GradeNumeric;
}
asort($selGrades);
$an = array_unique($selGrades);
$Grades = implode(', ', $an);
?>
<!-- Page Content  -->
<div class="section">
	<div class="container-fluid">
		<div class="row">
			<div class="col-12 mb-3">
               <h2 class="title">
                   <span>{{trans('sidebar.sidebar_nav_organization')}} > </span>
                   <a class="no_sidebar_active" href="{{url('/employee/classcreation')}}">
                    <span>{{trans('sidebar.sidebar_nav_class_creation')}} > </span>
                    <span>{{trans('general.gn_sub_class')}} > </span>
                   </a>

                {{trans('general.gn_view_details')}}
                </h2>
            </div>

            <div class="col-12">
                <div class="white_box pt-5 pb-5">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-lg-10 offset-lg-1">

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="card-grey">
                                            <div class="row">
                                                <div class="col-md-6 col-lg-3">
                                                    <div class="form-group">
                                                        <p class="label">{{trans('general.gn_school_year')}} :</p>
                                                        <p class="value">{{$mdata->classCreationSchoolYear->sye_NameCharacter}}</p>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-lg-3">
                                                    <div class="form-group">
                                                        <p class="label">{{trans('general.gn_school_grade')}} :</p>
                                                        <p class="value">{{$Grades}}</p>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-lg-3">
                                                    <div class="form-group">
                                                        <p class="label">{{trans('general.gn_class')}} :</p>
                                                        <p class="value">{{$mdata->classCreationClasses->cla_ClassName}}</p>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-lg-3">
                                                    <div class="form-group">
                                                        <p class="label">{{trans('general.gn_semester')}} :</p>
                                                        <p class="value">@if(isset($mdata->classCreationSemester[0]->semester)){{$mdata->classCreationSemester[0]->semester->edp_EducationPeriodName}}@endif</p>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-lg-3">
                                                    <div class="form-group">
                                                        <p class="label">{{trans('general.gn_sub_class')}} :</p>
                                                        <p class="value">{{$subclass}}</p>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-lg-3">
                                                    <div class="form-group">
                                                        <p class="label">{{trans('general.gn_course')}} :</p>
                                                        <p class="value">{{$course}}</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <p class="mt-3"><strong>{{trans('sidebar.sidebar_nav_students')}}</strong></p>
                                    <div class="table-responsive mt-2">
                                        <table class="color_table">
                                            <tbody><tr>
                                                <th>{{trans('general.sr_no')}}</th>
                                                <th>{{trans('general.gn_grade')}}</th>
                                                <th>{{trans('general.gn_name')}}</th>
                                                <th>{{trans('general.gn_teacher')}}</th>
                                                <th>{{trans('general.gn_action')}}</th>
                                            </tr>
                                            @if(isset($classData))
                                            @foreach($classData as $k => $v)
                                            <tr>
                                                <td>{{$k+1}}</td>
                                                <td>{{$v['grade']}}</td>
                                                <td>{{$v['stuName']}}</td>
                                                <td>{{$v['empName']}}</td>
                                                <td>
                                                    <a title="{{trans('general.gn_delete')}}" onclick="triggerStudentCourseDelete({{$v['pkCsa']}})" href="javascript:void(0)"><img src="{{asset('images/ic_delete.png')}}"></a>
                                                </td>
                                            </tr>
                                            @endforeach
                                            @endif
                                        </tbody>
                                        </table>
                                    </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>


        </div>
    </div>
</div>

<div class="theme_modal modal fade" id="delete_prompt" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <img alt="Close" src="{{asset('images/ic_close_bg.png')}}" class="modal_top_bg">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <img alt="Profile Photo" src="{{asset('images/ic_close_circle_white.png')}}">
                    </button>
                        <div class="row">
                            <div class="col-lg-1"></div>
                            <div class="col-lg-10">
                                <h5 class="modal-title" id="exampleModalCenterTitle">{{trans('general.gn_delete')}}</h5>
                                <div class="form-group text-center">
                                    <label>{{trans('general.gn_delete_prompt')}} ?</label>
                                    <input type="hidden" id="did">
                                </div>
                                <div class="text-center modal_btn ">
                                    <button style="display: none;" class="theme_btn show_delete_modal full_width small_btn" data-toggle="modal" data-target="#delete_prompt">{{trans('general.gn_delete')}}</button>
                                    <button type="button" onclick="courseDelete(1)" class="theme_btn red_btn">{{trans('general.gn_delete_course')}}</button>
                                    <button type="button" onclick="courseDelete(2)" class="theme_btn red_btn">{{trans('general.gn_delete_subclass_course')}}</button>
                                </div>
                            </div>
                            <div class="col-lg-1"></div>
                        </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('datatable-scripts')
<!-- Include this Page JS -->
<script type="text/javascript" src="{{ asset('js/dashboard/class_creation_sub_class.js') }}"></script>
@endpush