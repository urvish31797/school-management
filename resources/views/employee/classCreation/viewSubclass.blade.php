@extends('layout.app_with_login')
@section('title', trans('general.gn_sub_class'))
@section('script', asset('js/dashboard/class_creation_sub_class.js'))
@section('content')
<!-- Page Content  -->
<div class="section">
	<div class="container-fluid">
        <input type="hidden" id="current_language" value="{{ $current_language }}">
        <div class="row">
			<div class="col-12 mb-3">
               <h2 class="title">
                   <span>{{trans('sidebar.sidebar_nav_organization')}} > </span>
                <a class="no_sidebar_active" href="{{url('/employee/classcreation')}}">
                <span>
                {{trans('sidebar.sidebar_nav_class_creation')}} >
                </span>
                </a>
            {{trans('general.gn_sub_class')}}
            </h2>
            </div>
            <div class="col-md-3 mb-3">
                <input type="text" id="search_sub_class" maxlength="30" class="form-control icon_control search_control" placeholder="{{trans('general.gn_search')}}">
            </div>
            <div class="col-md-3 text-md-right mb-3">
                <div class="row">
                </div>
            </div>
            <div class="col-md-4 text-md-right mb-3">
                <div class="row">

                </div>
            </div>
            <div class="col-md-2 col-6 mb-3">
                <a href="{{url('employee/classcreation/subclass/'.$id)}}"><button class="theme_btn small_btn no_sidebar_active">{{trans('general.gn_add_new')}}</button></a>
            </div>
        </div>
        <input type="hidden" id="pkClr" value="{{$id}}">
        <div class="row">
            <div class="col-12">
                <div class="theme_table">
                    <div class="table-responsive">
                        <table id="sub_class_view_listing" class="display" style="width:100%">
                            <thead>
                                <tr>
                                    <th>{{trans('general.sr_no')}}.</th>
                                    <th>{{trans('general.gn_sub_class')}}</th>
                                    <th>{{trans('general.gn_course')}}</th>
                                    <th>{{trans('general.gn_action')}}</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <input type="hidden" id="sub_class_txt" value="{{trans('general.gn_sub_class')}}">
    <input type="hidden" id="edit_class_text" value="{{trans('general.gn_edit')}}">
    <input type="hidden" id="delete_class_text" value="{{trans('general.gn_delete')}}">
</div>
@endsection

@push('datatable-scripts')
<!-- Include this Page JS -->
<script type="text/javascript" src="{{ asset('js/dashboard/class_creation_sub_class.js') }}"></script>
@endpush