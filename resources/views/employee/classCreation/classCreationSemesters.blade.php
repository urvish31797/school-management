@extends('layout.app_with_login')
@section('title', trans('sidebar.sidebar_nav_class_creation'))
@section('script', asset('js/dashboard/class_creation.js'))
@section('content')
<!-- Page Content  -->
<div class="section">
	<div class="container-fluid">
        <input type="hidden" id="current_language" value="{{ $current_language }}">
        <div class="row">
			<div class="col-12 mb-3">
               <h2 class="title"><span>{{trans('sidebar.sidebar_nav_organization')}} > </span> <a class="no_sidebar_active" href="{{url('/employee/classcreation')}}"><span>{{trans('sidebar.sidebar_nav_class_creation')}} > </span></a>{{trans('general.gn_semester')}}</h2>
            </div>
            <div class="col-md-4 mb-3">
            </div>
            <div class="col-md-3 text-md-right mb-3">
                <div class="row">

                </div>
            </div>
            <div class="col-md-3 text-md-right mb-3">
                <div class="row">

                </div>
            </div>
        </div>
        <input type="hidden" id="pkClr" value="{{$pkClr}}">
        <div class="row">
            <div class="col-12">
                <div class="theme_table">
                    <div class="table-responsive">
                        <table id="class_creation_semester_listing" class="display" style="width:100%">
                            <thead>
                                <tr>
                                    <th>{{trans('general.sr_no')}}.</th>
                                    <th>{{trans('general.gn_grade')}}</th>
                                    <th>{{trans('general.gn_class')}}</th>
                                    <th>{{trans('general.gn_semester')}}</th>
                                    <th>{{trans('general.gn_no_of_students')}}</th>
                                    <th>{{trans('general.gn_school_year')}}</th>
                                    <th>{{trans('general.gn_homeroom_teacher')}}</th>
                                    <th>{{trans('general.gn_action')}}</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>

    </div>

    <div class="theme_modal modal fade" id="delete_prompt" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <img src="{{asset('images/ic_close_bg.png')}}" class="modal_top_bg">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <img src="{{asset('images/ic_close_circle_white.png')}}">
                    </button>
                        <div class="row">
                            <div class="col-lg-1"></div>
                            <div class="col-lg-10">
                                <h5 class="modal-title" id="exampleModalCenterTitle">{{trans('general.gn_delete')}}</h5>
                                <div class="form-group text-center">
                                    <label>{{trans('general.gn_delete_prompt')}} ?</label>
                                    <input type="hidden" id="did">
                                </div>
                                <div class="text-center modal_btn ">
                                    <button style="display: none;" class="theme_btn show_delete_modal full_width small_btn" data-toggle="modal" data-target="#delete_prompt">{{trans('general.gn_delete')}}</button>
                                    <button type="button" onclick="confirmDelete()" class="theme_btn">{{trans('general.gn_yes')}}</button>
                                    <button type="button" data-dismiss="modal" class="theme_btn red_btn">{{trans('general.gn_no')}}</button>
                                </div>
                            </div>
                            <div class="col-lg-1"></div>
                        </div>
                </div>
            </div>
        </div>
    </div>
            <input type="hidden" id="sub_class_txt" value="{{trans('general.gn_sub_class')}}">
            <input type="hidden" id="final_marks_txt" value="{{trans('general.gn_final_marks')}}">
            <input type="hidden" id="homeroomteacher_history_txt" value="{{trans('general.gn_homeroomteacher_history')}}">
            <input type="hidden" id="edit_class_text" value="{{trans('general.gn_edit')}}">
            <input type="hidden" id="delete_class_text" value="{{trans('general.gn_delete')}}">
</div>
<script>
var listing_url = "{{route('fetch-classcreation-lists')}}";
var class_creation_sem_url = "{{route('fetch-classcreationsem-lists')}}";
</script>
@endsection

@push('datatable-scripts')
<!-- Include this Page JS -->
<script type="text/javascript" src="{{ asset('js/dashboard/class_creation.js') }}"></script>
@endpush