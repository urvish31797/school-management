@extends('layout.app_with_login')
@section('title', trans('sidebar.sidebar_nav_class_creation'))
@section('script', asset('js/dashboard/class_creation.js'))
@section('content')
<!-- Page Content  -->
<div class="section">
	<div class="container-fluid">
        <input type="hidden" id="current_language" value="{{ $current_language }}">
        <div class="row">
			<div class="col-12 mb-3">
               <h2 class="title"><span>{{trans('sidebar.sidebar_nav_organization')}} > </span> {{trans('sidebar.sidebar_nav_class_creation')}}</h2>
            </div>
            <div class="col-md-3 mb-3">
                <input type="text" id="search_class_creations" maxlength="30" class="form-control icon_control search_control" placeholder="{{trans('general.gn_search')}}">
            </div>
            <div class="col-md-2 text-md-right mb-3">
                <div class="row">
                    <div class="col-3 text-right pr-0 pt-1">
                        <label class="blue_label">{{trans('general.gn_grade')}}</label>
                    </div>
                    <div class="col-9">
                        <select class="form-control icon_control dropdown_control" id="searchGrade">
                            <option value="">{{trans('general.gn_select')}}</option>
                            @foreach($searchGrade as $gs)
                                <option value="{{$gs['gra_GradeNumeric']}}">{{$gs['gra_GradeNumeric']}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
            <div class="col-md-3 text-md-right mb-3">
                <div class="row">
                    <div class="col-5 text-right pr-0 pt-1">
                        <label class="blue_label">{{trans('general.gn_school_year')}}</label>
                    </div>
                    <div class="col-7">
                        <select class="form-control icon_control dropdown_control" id="search_sch_year" style="inline-size:auto">
                            <option value="">{{trans('general.gn_select')}}</option>
                            @foreach($searchSchYear as $sy)
                                <option @if($sy['sye_DefaultYear'] == 1) selected="selected"@endif value="{{$sy['pkSye']}}">{{$sy['sye_NameCharacter']}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
            @if($showCloneButton)
            <div class="col-md-2 col-6 mb-3">
                <a><button class="theme_btn small_btn" data-toggle="modal" data-target="#clone_semester">{{trans('general.gn_clone_semester')}}</button></a>
            </div>
            @endif
            <div class="col-md-2 col-6 mb-3">
                <a class="theme_btn small_btn" href="{{url('/employee/classcreation/create')}}">{{trans('general.gn_add_new')}}</a>
            </div>
        </div>

        <div class="row">
            <div class="col-12">
                <div class="theme_table">
                    <div class="table-responsive">
                        <table id="class_creation_listing" class="display" style="width:100%">
                            <thead>
                                <tr>
                                    <th>{{trans('general.sr_no')}}.</th>
                                    <th>{{trans('general.gn_grade')}}</th>
                                    <th>{{trans('general.gn_class')}}</th>
                                    <th>{{trans('sidebar.sidebar_nav_village_schools')}}</th>
                                    <th>{{trans('general.gn_school_year')}}</th>
                                    <th>{{trans('general.gn_status')}}</th>
                                    <th>{{trans('general.gn_actions')}}</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>

    </div>

    <input type="hidden" id="sub_class_txt" value="{{trans('general.gn_sub_class')}}">
    <div class="theme_modal modal fade" id="delete_prompt" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <img src="{{asset('images/ic_close_bg.png')}}" class="modal_top_bg">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <img src="{{asset('images/ic_close_circle_white.png')}}">
                    </button>
                        <div class="row">
                            <div class="col-lg-1"></div>
                            <div class="col-lg-10">
                                <h5 class="modal-title" id="exampleModalCenterTitle">{{trans('general.gn_delete')}}</h5>
                                <div class="form-group text-center">
                                    <label>{{trans('general.gn_delete_prompt')}} ?</label>
                                    <input type="hidden" id="did">
                                </div>
                                <div class="text-center modal_btn ">
                                    <button style="display: none;" class="theme_btn show_delete_modal full_width small_btn" data-toggle="modal" data-target="#delete_prompt">{{trans('general.gn_delete')}}</button>
                                    <button type="button" onclick="confirmDelete()" class="theme_btn">{{trans('general.gn_yes')}}</button>
                                    <button type="button" data-dismiss="modal" class="theme_btn red_btn">{{trans('general.gn_no')}}</button>
                                </div>
                            </div>
                            <div class="col-lg-1"></div>
                        </div>
                </div>
            </div>
        </div>
    </div>

    <div class="theme_modal modal fade" id="clone_semester" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <img src="{{asset('images/ic_close_bg.png')}}" class="modal_top_bg">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <img src="{{asset('images/ic_close_circle_white.png')}}">
                    </button>
                        <div class="row">
                            <div class="col-lg-1"></div>
                            <div class="col-lg-10">
                                <h5 class="modal-title" id="exampleModalCenterTitle">{{trans('general.gn_clone_semester')}}</h5>
                                <div class="form-group text-center">
                                    <label>{{trans('general.gn_clone_prompt')}} ?</label>
                                    <label>{{trans('general.gn_from')}} : {{$currentSemester}} -> {{trans('general.gn_too')}} : {{$nextSemester}}</label>
                                    <input type="hidden" id="currentSemesterId" value="{{$currentSemesterId}}">
                                    <input type="hidden" id="nextSemesterId" value="{{$nextSemesterId}}">
                                </div>
                                <div class="text-center modal_btn ">
                                    <button type="button" onclick="cloneSemester()" class="theme_btn">{{trans('general.gn_yes')}}</button>
                                    <button type="button" data-dismiss="modal" class="theme_btn red_btn">{{trans('general.gn_no')}}</button>
                                </div>
                            </div>
                            <div class="col-lg-1"></div>
                        </div>
                </div>
            </div>
        </div>
    </div>

</div>
<script>
var listing_url = "{{route('fetch-classcreation-lists')}}";
var class_creation_sem_url = "{{route('fetch-classcreationsem-lists')}}";
</script>
@endsection

@push('datatable-scripts')
<!-- Include this Page JS -->
<script type="text/javascript" src="{{ asset('js/dashboard/class_creation.js') }}"></script>
@endpush