@extends('layout.app_with_login')
@section('title', trans('sidebar.sidebar_nav_education_plan'))
@section('content')
 <!-- Page Content  -->
<div class="section">
	<div class="container-fluid">
		<div class="row ">
            <div class="col-12 mb-3">
                <h2 class="title"><span>{{trans('sidebar.sidebar_nav_user_management')}} > </span><a class="no_sidebar_active" href="{{url('/employee/students')}}"><span>{{trans('sidebar.sidebar_nav_students')}} >  </span><a class="no_sidebar_active" href="{{url('/employee/enrollStudents')}}"><span>{{trans('general.gn_enroll')}} > </span></a> {{trans('general.gn_view_courses')}}</h2>
            </div>
            <div class="col-12">
                <div class="white_box pt-3 pb-3">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-lg-12 ">
                                <div class="pl-5 pr-5">
                                    <div class="row">
                                        @foreach($languages as $k => $v)
                                        <div class="col-md-6">
                                            <p>{{trans('sidebar.sidebar_nav_education_plan')}} ({{$v->language_name}}) : <label> {{$mdata['epl_EducationPlanName_'.$v->language_key]}}</label></p>
                                        </div>
                                        @endforeach


                                        <div class="col-md-6 text-left">
                                            <p>{{trans('sidebar.sidebar_nav_education_plan')}} {{trans('general.gn_for')}} : <label> {{$mdata->educationProgram->edp_Name}}</label></p>
                                        </div>

                                        <div class="col-md-6">
                                            <p>{{trans('sidebar.sidebar_nav_national_education_plan')}} : <label> {{$mdata->nationalEducationPlan->nep_NationalEducationPlanName}}</label></p>
                                        </div>

                                        <div class="col-md-6 text-left">
                                            <p>{{trans('sidebar.sidebar_nav_qualification_degree')}} : <label> {{$mdata->QualificationDegree->qde_QualificationDegreeName}}</label></p>
                                        </div>

                                        <div class="col-md-6">
                                            <p>{{trans('sidebar.sidebar_nav_education_profile')}} : <label> {{$mdata->educationProfile->epr_EducationProfileName}}</label></p>
                                        </div>

                                        <div class="col-md-12">
                                            <p class="mt-2"><strong>{{trans('general.gn_mandatory_courses')}}</strong></p>
                                            <div class="table-responsive mt-2">
                                                <table class="color_table">
                                                    <tr>
                                                        <th>{{trans('general.sr_no')}}</th>
                                                        <th width="45%">{{trans('general.gn_courses')}}</th>
                                                        <th width="15%">{{trans('general.gn_grade')}}</th>
                                                        <th>{{trans('general.gn_week_hours')}}</th>
                                                        <th>{{trans('general.gn_default')}}</th>
                                                    </tr>

                                                    @foreach($mdata->mandatoryCourse as $k => $v)
                                                        <tr>
                                                            <td>{{$k+1}}</td>
                                                            <td>{{$v->mandatoryCourseGroup->crs_CourseName}}</td>
                                                            <td>{{$v->grade->gra_GradeNumeric}} ({{$v->grade->gra_GradeNameRoman}})</td>
                                                            <td>{{$v->emc_hours}}</td>
                                                            <td>
                                                                <div class="form-group form-check">
                                                                    <input type="checkbox" disabled class="form-check-input" @if($v->emc_default == 'Yes') checked @endif name="default_mcg[]" id="default_mcg_{{$v->mandatoryCourseGroup->pkCrs}}" value="{{$v->mandatoryCourseGroup->pkCrs}}">
                                                                    <label class="custom_checkbox"></label>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                </table>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <p class="mt-2"><strong>{{trans('general.gn_optional_courses')}}</strong></p>
                                            <div class="table-responsive mt-2">
                                                <table class="color_table">
                                                    <tr>
                                                        <th>{{trans('general.sr_no')}}</th>
                                                        <th width="45%">{{trans('general.gn_courses')}}</th>
                                                        <th width="15%">{{trans('general.gn_grade')}}</th>
                                                        <th>{{trans('general.gn_week_hours')}}</th>
                                                        <th>{{trans('general.gn_default')}}</th>
                                                    </tr>
                                                    @foreach($mdata->optionalCourse as $k => $v)
                                                        <tr>
                                                            <td>{{$k+1}}</td>
                                                            <td>{{$v->optionalCoursesGroup->crs_CourseName}}</td>
                                                            <td>{{$v->grade->gra_GradeNumeric}} ({{$v->grade->gra_GradeNameRoman}})</td>
                                                            <td>{{$v->eoc_hours}}</td>
                                                            <td>
                                                                <div class="form-group form-check">
                                                                    <input type="checkbox" disabled class="form-check-input" @if($v->eoc_default == 'Yes') checked @endif name="default_ocg[]" id="default_mcg_{{$v->optionalCoursesGroup->pkCrs}}" value="{{$v->optionalCoursesGroup->pkCrs}}">
                                                                    <label class="custom_checkbox"></label>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                </table>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <p class="mt-2"><strong>{{trans('general.gn_mandatory_foreign_language_courses')}}</strong></p>
                                            <div class="table-responsive mt-2">
                                                <table class="color_table">
                                                    <tr>
                                                        <th>{{trans('general.sr_no')}}</th>
                                                        <th width="45%">{{trans('general.gn_courses')}}</th>
                                                        <th width="15%">{{trans('general.gn_grade')}}</th>
                                                        <th>{{trans('general.gn_week_hours')}}</th>
                                                        <th>{{trans('general.gn_default')}}</th>
                                                    </tr>
                                                    @foreach($mdata->foreignLanguageCourse as $k => $v)
                                                        <tr>
                                                            <td>{{$k+1}}</td>
                                                            <td>{{$v->foreignLanguageGroup->crs_CourseName}}</td>
                                                            <td>{{$v->grade->gra_GradeNumeric}} ({{$v->grade->gra_GradeNameRoman}})</td>
                                                            <td>{{$v->efc_hours}}</td>
                                                            <td>
                                                                <div class="form-group form-check">
                                                                    <input type="checkbox" disabled class="form-check-input" @if($v->efc_default == 'Yes') checked @endif name="default_fcg[]" id="default_fcg_{{$v->foreignLanguageGroup->pkCrs}}" value="{{$v->foreignLanguageGroup->pkCrs}}">
                                                                    <label class="custom_checkbox"></label>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End Content Body -->
@endsection

