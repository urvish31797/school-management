@extends('layout.app_with_login')
@section('title', trans('sidebar.sidebar_nav_enroll_students'))
@section('script', asset('js/dashboard/enroll_students.js'))
@section('content')

<div class="section">
	<div class="container-fluid">
		<div class="row">
			<div class="col-12 mb-3">
				<h2 class="title"><span>{{trans('sidebar.sidebar_nav_user_management')}} > </span><a class="no_sidebar_active" href="{{url('/employee/students')}}"><span>{{trans('sidebar.sidebar_nav_students')}} > </span></a> {{trans('general.gn_enroll')}}</h2>
            </div>
		</div>
        <input type="hidden" id="student_sel_valid_txt" value="{{trans('message.msg_student_sel_exist')}}">
        <input type="hidden" id="student_sel_txt" value="{{trans('message.msg_sel_student')}}">
		<div class="col-md-12">
            <div class="white_box p-5 pl-3 pr-3">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="">
                            <div class="form-group">
                            	<label>{{trans('general.gn_search_students')}}</label>
                				<input type="text" id="search_students" maxlength="30" class="form-control icon_control search_control" placeholder="{{trans('general.gn_search')}}">

                            </div>
                        </div>
                    </div>

                    <div class="col-lg-12">
                    	<div class="table-responsive mt-2 main_table">
                            <table id="enroll_stu_listing" class="display" style="width:100%">
                                <thead>
                                    <tr>
                                        <th>{{trans('general.sr_no')}}</th>
                                        <th>{{trans('general.gn_student_id')}} </th>
                                        <th>{{trans('general.gn_name')}}</th>
                                        <th>{{trans('general.gn_surname')}}</th>
                                        <th>{{trans('general.gn_action')}}</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                        <div class="col-md-12 sel_emp_div" style="display: none;">
                            <p class="mt-2"><strong>{{trans('general.gn_student')}}</strong></p>
                            <div class="table-responsive mt-2">
                                <table class="color_table sel_emp_table">
                                    <tr>
                                        <th width="10%">{{trans('general.sr_no')}}</th>
                                        <th width="25%">{{trans('general.gn_student_id')}}</th>
                                        <th width="20%">{{trans('general.gn_name')}}</th>
                                        <th width="15%">{{trans('general.gn_surname')}}</th>
                                        <th width="30%">{{trans('general.gn_action')}}</th>
                                    </tr>
                                </table>
                            </div>
                        </div>

                        <form name="enroll-stu-form">
                        	<input type="hidden" name="select_student" id="students_id">
                            <div class="profile_info_container">
                                <div class="row">
                                    <div class="col-md-12">
                                    	<h6 class="profile_inner_title">{{trans('general.gn_details_enrollment')}}</h6>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>{{trans('general.gn_date_of_enrollment')}} *</label>
                                            <input type="text" id="ste_EnrollmentDate" name="ste_EnrollmentDate" class="form-control icon_control datepicker">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>{{trans('general.gn_main_book_number')}} *</label>
                                            <select class="form-control icon_control dropdown_control" name="fkSteMbo" id="fkSteMbo">
                                                <option selected value="">Select</option>
                                                @forelse($mainBooks as $mbvalue)
                                                    <option value="{{$mbvalue->pkMbo }}">{{ $mbvalue->mbo_MainBookNameRoman }}</option>
                                                @empty
                                                    <option selected>No data found</option>
                                                @endforelse
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>{{trans('general.gn_order_no')}} *</label>
                                            <input type="number" name="ste_MainBookOrderNumber" id="ste_MainBookOrderNumber" class="form-control">
                                        </div>
                                    </div>


                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>{{trans('general.gn_school_year')}} *</label>
                                            <select class="form-control icon_control dropdown_control" name="fkSteSye" id="fkSteSye">
                                                <option value="" selected>Select</option>
                                                @forelse($schoolYear as $yeValue)
                                                    <option value="{{$yeValue->pkSye }}">{{ $yeValue->sye_NameCharacter }}</option>
                                                @empty
                                                    <option selected>No data found</option>
                                                @endforelse
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>{{trans('general.gn_education_program')}} *</label>
                                            <select class="form-control icon_control dropdown_control" name="fkSteEdp" id="fkSteEdp">
                                                <option value="" selected>Select</option>
                                                @forelse($educationProg as $mbvalue)
                                                    <option value="{{$mbvalue->pkEdp }}">{{ $mbvalue->edp_Name }}</option>
                                                @empty
                                                    <option selected>No data found</option>
                                                @endforelse
                                            </select>
                                        </div>
                                    </div>
                                    <input type="hidden" id="msg_select_education_plan" value="{{ trans('message.msg_select_education_plan')}}">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>{{trans('general.gn_education_plan')}} *
                                                <button type="button" class="btn-darkinfo" onclick="viewEduPlan()">  i </button>
                                            </label>
                                            <select class="form-control icon_control dropdown_control" name="fkSteEpl" id="fkSteEpl">
                                                <option value="" selected>Select</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>{{trans('general.gn_grade')}} *</label>
                                            <select class="form-control icon_control dropdown_control" name="fkSteGra" id="fkSteGra">
                                            	<option selected value="">Select</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>{{trans('general.gn_enroll_based_on')}} </label>
                                            <textarea class="form-control icon_control" name="ste_EnrollBasedOn" id="ste_EnrollBasedOn"> </textarea>

                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>{{trans('general.gn_ste_Reason')}} </label>
                                            <textarea class="form-control icon_control" name="ste_Reason" id="ste_Reason"> </textarea>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>{{trans('general.gn_finishing_date')}} </label>
                                            <input type="text" id="ste_FinishingDate" name="ste_FinishingDate" class="form-control icon_control datepicker">
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>{{trans('general.gn_breaking_date')}} </label>
                                            <input type="text" id="ste_BreakingDate" name="ste_BreakingDate" class="form-control icon_control datepicker">
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>{{trans('general.gn_expelling_date')}} </label>
                                            <input type="text" id="ste_ExpellingDate" name="ste_ExpellingDate" class="form-control icon_control datepicker">
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="text-center">

                    					<button type="Submit" class="theme_btn">{{trans('general.gn_submit')}}</button>
                    					<a class="theme_btn red_btn no_sidebar_active" href="{{url('/employee/students')}}">{{trans('general.gn_cancel')}}</a>

                    					</div>
                                    </div>


                                </div>
                            </div>
                        </form>
                    </div>

                </div>
            </div>
        </div>
	</div>
</div>



@endsection
@push('datatable-scripts')
<!-- Include this Page JS -->
<script type="text/javascript" src="{{ asset('js/dashboard/enroll_students.js') }}"></script>
@endpush