@extends('layout.app_with_login')
@section('title', trans('sidebar.sidebar_class_calendar_working_weeks'))
@section('content')
<!-- Page Content  -->
<div class="section">
   <div class="container-fluid">
      <div class="row">
         <div class="col-12 mb-3">
            <h2 class="title">
            <a href="{{url('/employee/class-working-weeks')}}">
               <span>{{trans('sidebar.sidebar_class_calendar_working_weeks')}} > </span>
            </a>
            {{trans('general.gn_view_details')}}
            </h2>
         </div>
         <div class="col-12">
            <div class="white_box pt-3 pb-3">
               <div class="container-fluid">
                  <form name="view-class-working-weeks" id="view-class-working-weeks">
                     <div class="row">
                        <div class="col-md-12">
                           <div>
                              <p class="mt-2"><strong>{{trans('general.gn_view_details')}}</strong></p>
                           </div>
                           <div class="card-grey">
                              <div class="row">
                                 <div class="col-md-6">
                                    <div class="form-group">
                                       <p class="label">{{trans('general.gn_shift')}} :</p>
                                       <p class="value">{{$ccw['shift_name']}}</p>
                                    </div>
                                 </div>
                                 <div class="col-md-6">
                                    <div class="form-group">
                                       <p class="label">{{trans('general.gn_school_year')}} :</p>
                                       <p class="value">{{$ccw['school_year']}}</p>
                                    </div>
                                 </div>
                                 <div class="col-md-6">
                                    <div class="form-group">
                                       <p class="label">{{trans('sidebar.sidebar_nav_working_weeks')}} :</p>
                                       <p class="value">{{$ccw['wek_weekName']}}</p>
                                    </div>
                                 </div>
                                 <div class="col-md-6">
                                    <div class="form-group">
                                       <p class="label">{{trans('general.gn_note')}} :</p>
                                       <p class="value">{{$ccw['notes']}}</p>
                                    </div>
                                 </div>
                                 <div class="col-md-6">
                                    <div class="form-group">
                                       <p class="label">{{trans('general.gn_first_student')}} :</p>
                                       <p class="value">{{$ccw['first_student']}}</p>
                                    </div>
                                 </div>
                                 <div class="col-md-6">
                                    <div class="form-group">
                                       <p class="label">{{trans('general.gn_second_student')}} :</p>
                                       <p class="value">{{$ccw['second_student']}}</p>
                                    </div>
                                 </div>

                              </div>
                           </div>
                        </div>
                     </div>
                  </form>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<!-- End Content Body -->
@endsection
@push('datatable-scripts')
<!-- Include this Page JS -->
@endpush