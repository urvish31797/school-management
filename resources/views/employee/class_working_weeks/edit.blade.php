@extends('layout.app_with_login')
@section('title', trans('sidebar.sidebar_class_calendar_working_weeks'))
@push('custom-styles')
<link rel="stylesheet" href="{{ asset('css/class_working_weeks.css') }}">
@endpush
@section('content')
<div class="section">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12 mb-3">
                <h2 class="title"><span>{{trans('sidebar.sidebar_class_calendar_working_weeks')}} ></span> {{trans('general.gn_add_new')}}</h2>
            </div>
            <div class="col-12">
                <div class="white_box pt-3 pb-3">
                    <div class="container-fluid">
                        <form name="add-classcalendar-workingweeks-form" id="add-classcalendar-workingweeks-form">
                            <input type="hidden" name="pkCcw" id="pkCcw" value="{{$row->pkCcw}}">
                            <input type="hidden" name="fkCcwCcs" id="fkCcwCcs" value="{{$class_sem_id}}">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="pl-5 pr-5">
                                        <p class="mt-2"><strong>{{trans('sidebar.sidebar_class_calendar_working_weeks')}}</strong></p>
                                        <div class="card-grey">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <p class="label">
                                                        {{trans('general.gn_class')}} :
                                                        </p>
                                                        <p> {{$classname}} </p>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>{{trans('general.gn_shift')}} *</label>
                                                    @include('partials.dropdown.dropdowns',[
                                                    'type'=>'shifts',
                                                    'name'=>'fkCcwShi',
                                                    'selected'=>$row->fkCcwShi,
                                                    'extra_params'=>[
                                                    "id"=>"fkCcwShi",
                                                    ]
                                                    ])
                                                </div>
                                            </div>
                                            </div>
                                        </div>
                                        <div class="cww_add_element">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="table-responsive mt-2">
                                                        <table class="color_table classworkingweek_table">
                                                            <tr>
                                                                <th>{{trans('general.sr_no')}}</th>
                                                                <th>{{trans('sidebar.sidebar_nav_working_weeks')}}</th>
                                                                <th>{{trans('general.gn_first_student')}}</th>
                                                                <th>{{trans('general.gn_second_student')}}</th>
                                                                <th>{{trans('general.gn_start_date')}}</th>
                                                                <th>{{trans('general.gn_end_date')}}</th>
                                                                <th>{{trans('general.gn_note')}}</th>
                                                            </tr>
                                                            <tr class="main_weekhour selectedweekhrs">
                                                                <td class="sr">1</td>
                                                                <td>
                                                                    <select class="form-control select2drp" id="fkCcwWek" name="fkCcwWek">
                                                                        <option value="">{{trans('general.gn_select')}}</option>
                                                                        @foreach($working_weeks as $k => $v)
                                                                        <option @if($row->fkCcwWek == $k) selected @endif value="{{$k}}">{{$v}}</option>
                                                                        @endforeach
                                                                    </select>
                                                                </td>
                                                                <td>
                                                                    <select class="select2 form-control select2drp" id="fkCcwSteOne" name="fkCcwSteOne">
                                                                        <option value="">{{trans('general.gn_select')}}</option>
                                                                        @foreach($student_list as $k => $v)
                                                                        <option @if($row->fkCcwSteOne == $k) selected @endif value="{{$k}}">{{$v}}</option>
                                                                        @endforeach
                                                                    </select>
                                                                </td>
                                                                <td>
                                                                    <select class="select2 form-control select2drp" id="fkCcwSteTwo" name="fkCcwSteTwo">
                                                                        <option value="">{{trans('general.gn_select')}}</option>
                                                                        @foreach($student_list as $k => $v)
                                                                        <option @if($row->fkCcwSteTwo == $k) selected @endif value="{{$k}}">{{$v}}</option>
                                                                        @endforeach
                                                                    </select>
                                                                </td>
                                                                <td>
                                                                    <input type="text" class="form-control start_datepicker" id="start_date" value="{{$row->csd}}" name="ccw_startDate">
                                                                </td>
                                                                <td>
                                                                    <input type="text" class="form-control end_datepicker" id="end_date" value="{{$row->ced}}" name="ccw_endDate">
                                                                </td>
                                                                <td>
                                                                    <input type="text" class="form-control" id="ccw_notes" value="{{$row->ccw_notes}}" name="ccw_notes">
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="text-center">
                                        <button type="submit" class="theme_btn">{{trans('general.gn_submit')}}</button>
                                        <a class="theme_btn red_btn" href="{{url('/employee/class-working-weeks')}}">{{trans('general.gn_cancel')}}</a>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<input type="hidden" id="week_select_msg" value="{{trans('message.msg_select_week')}}">
<input type="hidden" id="stu_one_select_msg" value="{{trans('message.msg_select_first_student')}}">
<input type="hidden" id="stu_two_select_msg" value="{{trans('message.msg_select_second_student')}}">
<input type="hidden" id="start_date_select_msg" value="{{trans('message.msg_select_start_date')}}">
<input type="hidden" id="end_date_select_msg" value="{{trans('message.msg_select_end_date')}}">
<input type="hidden" id="week_already_selected_msg" value="{{trans('message.msg_week_groups_selected')}}">
<input type="hidden" id="add_class_week_msg" value="{{trans('message.msg_add_class_calendar_weeks')}}">

@endsection
@push('datatable-scripts')
<!-- Include datatable Page JS -->
<script type="text/javascript" id="datatable_script" src="{{ asset('js/dashboard/class_working_weeks.js') }}"></script>
@endpush