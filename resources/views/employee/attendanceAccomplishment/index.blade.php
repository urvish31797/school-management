@extends('layout.app_with_login')
@section('title', trans('sidebar.sidebar_nav_sa_sa'))
@section('script', asset('js/dashboard/attendanceAccomplishment.js'))
@section('content')
<!-- Page Content  -->
<div class="section">
	<div class="container-fluid">
        <div class="row">
			<div class="col-12 mb-3">
               <h2 class="title">{{trans('sidebar.sidebar_nav_sa_sa')}}</h2>
            </div>
            <div class="col-md-3 mb-3">
                <input type="text" id="search_attendance" maxlength="30" class="form-control icon_control search_control" placeholder="{{trans('general.gn_search')}}">
            </div>
            <div class="col-md-3 text-md-right mb-3">
                <div class="row">
                    <div class="col-3 text-right pr-0 pt-1">
                        <label class="blue_label">{{trans('general.gn_grade')}}</label>
                    </div>
                    <div class="col-9">
                        <select class="form-control icon_control dropdown_control" id="searchGrade">
                            <option value="">{{trans('general.gn_select')}}</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="col-md-3 text-md-right mb-3">
                <div class="row">
                    <div class="col-5 text-right pr-0 pt-1">
                        <label class="blue_label">{{trans('general.gn_school_year')}}</label>
                    </div>
                    <div class="col-7">
                        <select class="form-control icon_control dropdown_control" id="search_sch_year">
                            <option value="">{{trans('general.gn_select')}}</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="col-md-3 text-md-right mb-3">
                <a href="{{url('/employee/attendance-accomplishment/create')}}"><button class="theme_btn small_btn">{{trans('general.gn_add_new')}}</button></a>
            </div>
        </div>

        <div class="row">
            <div class="col-12">
                <div class="theme_table">
                    <div class="table-responsive">
                        <table id="attendance_listing" class="display" style="width:100%">
                            <thead>
                                <tr>
                                    <th>{{trans('general.sr_no')}}.</th>
                                    <th>{{trans('general.gn_shift')}}</th>
                                    <th>{{trans('general.gn_date')}}</th>
                                    <th>{{trans('general.gn_hour')}}</th>
                                    <th>{{trans('sidebar.sidebar_nav_village_schools')}}</th>
                                    <th>{{trans('general.gn_actions')}}</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>

    </div>

    <input type="hidden" id="sub_class_txt" value="{{trans('general.gn_sub_class')}}">
    <div class="theme_modal modal fade" id="delete_prompt" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <img src="{{asset('images/ic_close_bg.png')}}" class="modal_top_bg">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <img src="{{asset('images/ic_close_circle_white.png')}}">
                    </button>
                        <div class="row">
                            <div class="col-lg-1"></div>
                            <div class="col-lg-10">
                                <h5 class="modal-title" id="exampleModalCenterTitle">{{trans('general.gn_delete')}}</h5>
                                <div class="form-group text-center">
                                    <label>{{trans('general.gn_delete_prompt')}} ?</label>
                                    <input type="hidden" id="did">
                                </div>
                                <div class="text-center modal_btn ">
                                    <button style="display: none;" class="theme_btn show_delete_modal full_width small_btn" data-toggle="modal" data-target="#delete_prompt">{{trans('general.gn_delete')}}</button>
                                    <button type="button" onclick="confirmDelete()" class="theme_btn">{{trans('general.gn_yes')}}</button>
                                    <button type="button" data-dismiss="modal" class="theme_btn red_btn">{{trans('general.gn_no')}}</button>
                                </div>
                            </div>
                            <div class="col-lg-1"></div>
                        </div>
                </div>
            </div>
        </div>
    </div>
</div>
<input type="hidden" id="student_behaviour_label" value="{{trans('general.gn_student_behaviour_attendance_note')}}">
<script>
   var class_stu_url = "{{route('fetch-classstudents-lists')}}";
   var attendance_url = "{{route('fetch-attendance-lists')}}";
   var class_attendance_detail_url = "{{route('fetch-class-attendance-lists')}}";
   var previous_unit_history_url = "{{route('fetch-previous-unit-history')}}";
</script>
@endsection

@push('datatable-scripts')
<!-- Include this Page JS -->
<script type="text/javascript" src="{{ asset('js/dashboard/attendanceAccomplishment.js') }}"></script>
@endpush