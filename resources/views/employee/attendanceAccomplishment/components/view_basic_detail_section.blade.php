<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <p class="label">{{trans('general.gn_village_school')}} : {{$basic_details['villageSchool']}}</p>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <p class="label">{{trans('general.gn_date')}} : {{$basic_details['date']}}</p>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <p class="label">{{trans('general.gn_hour_order_number')}} : {{$basic_details['lecturenumber']}}</p>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <p class="label">{{trans('general.gn_shift')}} : {{$basic_details['shift']}}</p>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <p class="label">{{trans('general.gn_education_way')}} : {{$basic_details['educationway']}}</p>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <p class="label">{{trans('general.gn_attendance_way')}} : {{$basic_details['attendanceway']}}</p>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <p class="label">{{trans('general.gn_verified')}} : {{$basic_details['verified']}}</p>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <p class="label">{{trans('general.gn_class_details')}} : {{$basic_details['classgradename']}}</p>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <p class="label">{{trans('general.gn_on_duty_first_student')}} : {{$working_weeks_details['first_student'] ?? ''}}</p>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <p class="label">{{trans('general.gn_on_duty_second_student')}} : {{$working_weeks_details['second_student'] ?? ''}}</p>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <p class="label">{{trans('sidebar.sidebar_nav_working_weeks')}} : {{$working_weeks_details['working_week'] ?? ''}}</p>
        </div>
    </div>
</div>