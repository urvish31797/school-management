<div class="col-lg-12">
    <div class="form-group">
       <label>{{trans('sidebar.sidebar_nav_classes')}}</label>
       <div class="theme_table">
          <div class="table-responsive slim_scroll">
                <table id="grade_listing" class="display" style="width:100%">
                   <thead>
                      <tr>
                         <th>{{trans('general.gn_grade')}}</th>
                         <th width="20%">{{trans('general.gn_course')}}</th>
                         <th width="10%">{{trans('general.gn_prescribed_hours')}}</th>
                         <th width="10%">{{trans('general.gn_hour_order_number')}}</th>
                         <th>{{trans('general.gn_unit_title_no')}}</th>
                         <th>{{trans('general.gn_course_content')}}</th>
                         <th>{{trans('general.gn_action')}}</th>
                      </tr>
                   </thead>
                   <tbody>
                      @for($i=0;$i<count($components['classRows']);$i++)
                      <tr class="grade_rows" id="grade_row_{{$components['classRows'][$i]['graId']}}">
                         <td>
                            <input type="hidden" class="all_grades" name="grades[{{$components['classRows'][$i]['graId']}}]" id="grades_{{$components['classRows'][$i]['graId']}}" value="{{$components['classRows'][$i]['graId']}}">
                            <input type="hidden" class="all_class" name="class[{{$components['classRows'][$i]['graId']}}]" id="class_{{$components['classRows'][$i]['graId']}}" value="{{$components['classRows'][$i]['classId']}}">
                            <input type="hidden" class="all_hour" name="sya_LectureOrderNumber_{{$components['classRows'][$i]['graId']}}" id="sya_LectureOrderNumber_{{$components['classRows'][$i]['graId']}}" value="{{$components['classRows'][$i]['hournumber']}}">
                            {{$components['classRows'][$i]['gradeName']}}{{$components['classRows'][$i]['className']}}
                         </td>
                         <td>
                            <select onchange="fetchCourseHours(this.value,{{$components['classRows'][$i]['graId']}})" class="form-control icon_control dropdown_control" name="courses_{{$components['classRows'][$i]['graId']}}" id="courses_{{$components['classRows'][$i]['graId']}}">
                               @foreach($components['classRows'][$i]['courses'] as $k => $v)
                                <option @if($components['classInformation']['courseId'] == $v->pkCrs) selected @endif value="{{$v->pkCrs}}">{{$v->crs_CourseName}}</option>
                               @endforeach
                            </select>
                         </td>
                         <td id="prescribed_hours_{{$components['classRows'][$i]['graId']}}">{{$components['classRows'][$i]['prescribed']}}</td>
                         <td id="hour_number_{{$components['classRows'][$i]['graId']}}">{{$components['classRows'][$i]['hournumber']}}</td>
                         <td>
                             <textarea style="height: 100px" class="form-control force_require icon_control courseunitnumber_input" name="sya_CourseUnitNumber_{{$components['classRows'][$i]['graId']}}" id="sya_CourseUnitNumber_{{$components['classRows'][$i]['graId']}}" required=""></textarea>
                         </td>
                         <td>
                             <textarea style="height: 100px" class="form-control force_require icon_control" name="sya_CourseContent_{{$components['classRows'][$i]['graId']}}" id="sya_CourseContent_{{$components['classRows'][$i]['graId']}}" required=""></textarea>
                        </td>
                         <td>
                            <a onclick="triggerCoursePrompt(true,{{$components['classRows'][$i]['graId']}})" href="javascript:void(0)"><i class="fa fa-eye" aria-hidden="true"></i></a>
                            <a onclick="removeGrade({{$components['classRows'][$i]['graId']}})" href="javascript:void(0)"><i class="fa fa-trash" aria-hidden="true"></i></a>
                         </td>
                      </tr>
                      @endfor
                   </tbody>
                </table>
          </div>
       </div>
    </div>

    <div class="text-center" id="get_students_btn_div">
        <button type="button" onclick="getStudentsList()" class="theme_btn">{{trans('general.gn_get_students')}}</button>
     </div>
 </div>