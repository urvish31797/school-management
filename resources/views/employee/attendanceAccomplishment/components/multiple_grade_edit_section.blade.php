<div class="col-lg-12">
    <div class="form-group">
       <label>{{trans('sidebar.sidebar_nav_classes')}}</label>
       <div class="theme_table">
          <div class="table-responsive slim_scroll">
                <table id="grade_listing" class="display" style="width:100%">
                   <thead>
                      <tr>
                         <th>{{trans('general.gn_grade')}}</th>
                         <th width="20%">{{trans('general.gn_course')}}</th>
                         <th width="10%">{{trans('general.gn_hour_order_number')}}</th>
                         <th>{{trans('general.gn_unit_title_no')}}</th>
                         <th>{{trans('general.gn_course_content')}}</th>
                      </tr>
                   </thead>
                   <tbody>
                      @foreach ($lecture_details as $k => $details)
                        <tr class="grade_rows">
                            <td>{{$details['grade']}}{{$details['class']}}</td>
                            <td>{{$details['course']}}</td>
                            <td>{{$details['sya_LectureOrderNumber']}}</td>
                            <input type="hidden" name="lectures[]" value="{{$details['pkSya']}}">
                            <td><textarea style="height: 100px" class="form-control force_require icon_control courseunitnumber_input" name="sya_CourseUnitNumber_{{$details['pkSya']}}">{{$details['sya_CourseUnitNumber']}}</textarea></td>
                            <td><textarea style="height: 100px" class="form-control force_require icon_control" name="sya_CourseContent_{{$details['pkSya']}}">{{$details['sya_CourseContent']}}</textarea></td>
                        </tr>
                      @endforeach
                   </tbody>
                </table>
          </div>
       </div>
    </div>
 </div>