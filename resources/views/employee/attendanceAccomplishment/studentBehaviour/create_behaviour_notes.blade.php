@extends('layout.app_with_login')
@section('title', trans('sidebar.sidebar_nav_sa_sa'))
@section('content')
<div class="section">
   <div class="container-fluid">
      <div class="row">
         <div class="col-12 mb-3">
            <h2 class="title">
            <a href="{{url('/employee/attendance-accomplishment')}}">
               <span>{{trans('sidebar.sidebar_nav_sa_sa')}} > </span>
            </a>
            {{trans('general.gn_student_behaviour_attendance_note')}}
            </h2>
         </div>
         <div class="col-12">
            <div class="white_box pt-3 pb-3">
               <div class="container-fluid">
                  <form name="add-student-behaviour-form" id="add-student-behaviour-form">
                     <div class="row">
                        <div class="col-md-12">
                           <p class="mt-2"><strong>{{trans('general.gn_student_behaviour_attendance_note')}}</strong></p>
                           <div class="card-grey">
                              @include('employee.attendanceAccomplishment.components.view_basic_detail_section')
                           </div>
                           <div class="row">
                              <div class="col-lg-12">
                                 <div class="form-group">
                                    <label>{{trans('sidebar.sidebar_nav_students')}}</label>
                                    <div class="theme_table">
                                       <div class="table-responsive slim_scroll">
                                          <table id="lecture_detail_listing" class="display" style="width:100%">
                                             <thead>
                                                <tr>
                                                   <th width="5%">{{trans('general.sr_no')}}.</th>
                                                   <th width="40%">{{trans('general.gn_student_name')}}</th>
                                                   <th width="5%">{{trans('general.gn_class')}}</th>
                                                   <th width="10%">{{trans('general.gn_lecture_status')}}</th>
                                                   <th>{{trans('general.student_explanation')}}</th>
                                                </tr>
                                             </thead>
                                             <tbody>
                                             @php $count = 1 @endphp
                                             @foreach($attendance_details as $key => $val)
                                                @foreach ($val as $k => $v)
                                                <tr>
                                                   <input type="hidden" name="pkSae[]" value="{{$v['pkSae']}}">
                                                   <td>{{$count++}}</td>
                                                   <td>{{$v['studentname']}}</td>
                                                   <td>{{$v['grade']}}{{$v['class']}}</td>
                                                   <td>{{$v['attendancestatus']}}</td>
                                                   <td><input type="text" class="form-control" value="{{$v['tdesc']}}" name="sae_StudentTDesc[{{$v['pkSae']}}]"></td>
                                                </tr>
                                                @endforeach
                                             @endforeach
                                             </tbody>
                                          </table>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="text-center">
                              <button type="button" onclick="save()" class="theme_btn">{{trans('general.gn_submit')}}</button>
                              <a class="theme_btn red_btn" href="{{url('/employee/attendance-accomplishment')}}">{{trans('general.gn_cancel')}}</a>
                           </div>
                        </div>

                     </div>
                  </form>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<script>
   var save_behaviour_notes_url = "{{route('student-behaviour-notes.store')}}";
</script>
@endsection
@push('datatable-scripts')
<!-- Include this Page JS -->
<script type="text/javascript" src="{{ asset('js/dashboard/student_behaviour_notes.js') }}"></script>
@endpush