@extends('layout.app_with_login')
@section('title', trans('sidebar.sidebar_nav_sa_sa'))
@section('content')
<div class="section">
   <div class="container-fluid">
      <div class="row">
         <div class="col-12 mb-3">
            <h2 class="title">
            <a href="{{url('/employee/attendance-accomplishment')}}">
               <span>{{trans('sidebar.sidebar_nav_sa_sa')}} > </span>
            </a>
            {{trans('general.gn_view_details')}}
            </h2>
         </div>
         <div class="col-12">
            <div class="white_box pt-3 pb-3">
               <div class="container-fluid">
                  <form name="add-attend-accom-form" id="view-attend-accom-form">
                     <div class="row">
                        <div class="col-md-12">
                           <div>
                              <p class="mt-2"><strong>{{trans('general.gn_view_details')}}</strong></p>
                           </div>
                           <div class="card-grey">
                           @include('employee.attendanceAccomplishment.components.view_basic_detail_section')
                           </div>
                           <div class="row">
                              <div class="col-lg-12">
                                 <div class="form-group">
                                 <label>{{trans('sidebar.sidebar_nav_classes')}}</label>
                                    <div class="theme_table">
                                       <div class="table-responsive slim_scroll">
                                       <table id="grade_listing" class="display" style="width:100%">
                                          <thead>
                                             <tr>
                                             <th>{{trans('general.gn_grade')}}</th>
                                             <th>{{trans('general.gn_course')}}</th>
                                             <th>{{trans('general.gn_hour_order_number')}}</th>
                                             <th>{{trans('general.gn_unit_title_no')}}</th>
                                             <th>{{trans('general.gn_course_content')}}</th>
                                             </tr>
                                          </thead>
                                          <tbody>
                                             @foreach($lecture_details as $key => $val)
                                             <tr>
                                                <td>{{$val['grade']}}{{$val['class']}}</td>
                                                <td>{{$val['course']}}</td>
                                                <td>{{$val['sya_LectureOrderNumber']}}</td>
                                                <td>{{$val['sya_CourseUnitNumber']}}</td>
                                                <td>{{$val['sya_CourseContent']}}</td>
                                             </tr>
                                             @endforeach
                                          </tbody>
                                       </table>
                                       </div>
                                    </div>
                                 </div>
                              </div>

                              <div class="col-lg-12">
                                 <div class="form-group">
                                    <label>{{trans('sidebar.sidebar_nav_students')}}</label>
                                    <div class="theme_table">
                                       <div class="table-responsive slim_scroll">
                                          <table id="lecture_detail_listing" class="display" style="width:100%">
                                             <thead>
                                                <tr>
                                                   <th>{{trans('general.sr_no')}}.</th>
                                                   <th>{{trans('general.gn_student_name')}}</th>
                                                   <th>{{trans('general.gn_class')}}</th>
                                                   <th>{{trans('general.gn_status')}}</th>
                                                   <th>{{trans('general.gn_lecture_status')}}</th>
                                                   <th></th>
                                                </tr>
                                             </thead>
                                             <tbody>
                                             @php $count = 1 @endphp
                                             @foreach($attendance_details as $key => $val)
                                                @foreach ($val as $k => $v)
                                                <tr data-toggle="collapse" href="#collapseExample{{$v['pkSae']}}" role="button" aria-expanded="false" aria-controls="collapseExample">
                                                   <td>{{$count++}}</td>
                                                   <td>{{$v['studentname']}}</td>
                                                   <td>{{$v['grade']}}{{$v['class']}}</td>
                                                   <td>{{$v['attendancestatus']}}</td>
                                                   <td>{{$v['attendanceLecturestatus']}}</td>
                                                   <td class="collapse_sign"> + </td>
                                                </tr>
                                                <tr class="collapse" id="collapseExample{{$v['pkSae']}}">
                                                   <td colspan="6">
                                                      <div class="expand_div">
                                                         <div class="row">
                                                            <div class="col-12 mt-2">
                                                               <p class="label">{{trans('general.gn_teacher_notes')}} :</p>
                                                               <p class="value">{{$v['tdesc']}}</p>
                                                            </div>
                                                            <div class="col-12 mt-2">
                                                               <p class="label">{{trans('general.gn_homeroomteacher_notes')}} :</p>
                                                               <p class="value">{{$v['hdesc']}}</p>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </td>
                                                </tr>
                                                @endforeach
                                             @endforeach
                                             </tbody>
                                          </table>
                                       </div>
                                    </div>
                                 </div>
                              </div>

                              <div class="col-lg-12">
                                 <div class="form-group">
                                    <label>{{trans('general.gn_note')}}</label>
                                    <input type="text" name="dbl_Notes" id="dbl_Notes" class="form-control icon_control" disabled value="{{$basic_details['note']}}">
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </form>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<script>
   var class_stu_url = "{{route('fetch-classstudents-lists')}}";
   var attendance_url = "{{route('fetch-attendance-lists')}}";
   var class_attendance_detail_url = "{{route('fetch-class-attendance-lists')}}";
   var previous_unit_history_url = "{{route('fetch-previous-unit-history')}}";
</script>
@endsection
@push('datatable-scripts')
@endpush