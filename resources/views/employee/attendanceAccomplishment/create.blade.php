@extends('layout.app_with_login')
@section('title', trans('sidebar.sidebar_nav_sa_sa'))
@section('script',asset('js/dashboard/attendanceAccomplishment.js'))
@section('content')
<!-- Page Content  -->
<div class="section">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12 mb-3">
                <h2 class="title">
                    <a href="{{url('/employee/attendance-accomplishment')}}">
                        <span>{{trans('sidebar.sidebar_nav_sa_sa')}} > </span>
                    </a>
                    {{trans('general.gn_add_new')}}
                </h2>
            </div>
            <div class="col-12">
                <div class="white_box pt-3 pb-3">
                    <div class="container-fluid">
                        <form name="add-attend-accom-form" id="add-attend-accom-form">
                            <input type="hidden" name="classDetail" id="classDetail" value="{{$classDetailJSON}}">
                            <input type="hidden" name="pkDbl" id="pkDbl">

                            <div class="row">
                                <div class="col-md-12">
                                    <div>
                                        <p class="mt-2"><strong>{{trans('general.gn_add_attendance')}}</strong></p>
                                    </div>
                                    <div class="card-grey">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <p class="label">{{trans('general.gn_course')}} :</p>
                                                    <p class="value">{{$components['classInformation']['courseName'] ?? ''}}</p>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <p class="label">{{trans('general.gn_village_school')}} :</p>
                                                    <p class="value">{{$components['classInformation']['villageschoolName'] ?? ''}}</p>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <p class="label">{{trans('general.gn_class')}} :</p>
                                                    <p class="value">{{$components['classInformation']['classInfo'] ?? ''}}</p>
                                                </div>
                                            </div>
                                            @if(count($components['classRows']) == 1)
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <p class="label">{{trans('general.gn_hour_order_number_school_year')}} : {{$components['classRows'][0]['hournumber']}}</p>
                                                    <p class="label">{{trans('general.gn_prescribed_total_hours')}} : {{$components['classRows'][0]['prescribed']}}</p>
                                                </div>
                                            </div>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="bg-color-form">
                                        <div class="row">

                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <label>{{trans('general.gn_date')}} *</label>
                                                    <input type="text" name="dbk_Date" id="dbk_Date" value="{{date('d/m/Y')}}" class="form-control icon_control date_control" required="">
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <label>{{trans('general.gn_hour')}} *</label>
                                                    <select class="form-control icon_control dropdown_control" name="dbl_LectureNumber" id="dbl_LectureNumber">
                                                        <option value="">{{trans('general.gn_select')}}</option>
                                                        @for($i=1;$i<=7;$i++)
                                                        <option value="{{$i}}">{{$i}}</option>
                                                        @endfor
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <label>{{trans('general.gn_education_way')}} *</label>
                                                    @include('partials.dropdown.dropdowns',[
                                                    'type'=>'education_way',
                                                    'name'=>'fkDblEw',
                                                    'selected'=>$default_education_way,
                                                    'extra_params'=>[
                                                    "id"=>"fkDblEw",
                                                    ]
                                                    ])
                                                </div>
                                            </div>

                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <label>{{trans('general.gn_attendance_way')}} *</label>
                                                    @include('partials.dropdown.dropdowns',[
                                                    'type'=>'attendance_way',
                                                    'name'=>'fkDblAw',
                                                    'selected'=>$default_attendance_way,
                                                    'extra_params'=>[
                                                    "id"=>"fkDblAw",
                                                    ]
                                                    ])
                                                </div>
                                            </div>

                                            @if(count($components['classRows']) > 1)
                                            @include('employee.attendanceAccomplishment.components.multiple_grade_section')
                                            @else
                                            <input type="hidden" name="grades[{{$components['classRows'][0]['graId']}}]" id="grades_{{$components['classRows'][0]['graId']}}" value="{{$components['classRows'][0]['graId']}}">
                                            <input type="hidden" name="class[{{$components['classRows'][0]['graId']}}]" id="class_{{$components['classRows'][0]['graId']}}" value="{{$components['classRows'][0]['classId']}}">
                                            <input type="hidden" name="courses_{{$components['classRows'][0]['graId']}}" id="courses_{{$components['classRows'][0]['graId']}}" value="{{$components['classInformation']['courseId']}}">
                                            <input type="hidden" name="sya_LectureOrderNumber_{{$components['classRows'][0]['graId']}}" id="sya_LectureOrderNumber_{{$components['classRows'][0]['graId']}}" value="{{$components['classRows'][0]['hournumber']}}">
                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <label>{{trans('general.gn_unit_title_no')}} *</label> &nbsp;<a onclick="triggerCoursePrompt(true,{{$components['classRows'][0]['graId']}})" href="javascript:void(0)" id="course_view_history"><img title="{{trans('general.gn_view_history')}}" alt="{{trans('general.gn_view_history')}}" src="{{ asset('images/ic_eye.png')}}" /></a>
                                                    <input type="text" id="sya_CourseUnitNumber_{{$components['classRows'][0]['graId']}}" name="sya_CourseUnitNumber_{{$components['classRows'][0]['graId']}}" class="form-control courseunitnumber_input" required="">
                                                </div>
                                            </div>
                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <label>{{trans('general.gn_course_content')}} *</label>
                                                    <textarea style="height: 100px" class="form-control force_require icon_control" name="sya_CourseContent_{{$components['classRows'][0]['graId']}}" id="sya_CourseContent_{{$components['classRows'][0]['graId']}}" required=""></textarea>
                                                </div>
                                            </div>
                                            @endif

                                            <div class="col-lg-12 @if(count($components['classRows']) > 1) hide_content @endif" id="student_listing_div">
                                                <div class="form-group">
                                                    <label>{{trans('sidebar.sidebar_nav_students')}}</label>
                                                    <div class="theme_table">
                                                        <div class="table-responsive slim_scroll">
                                                            <table id="students_listing" class="display" style="width:100%">
                                                                <thead>
                                                                    <tr>
                                                                        <th>{{trans('general.sr_no')}}.</th>
                                                                        <th>{{trans('general.gn_student_name')}}</th>
                                                                        <th>{{trans('general.gn_grade')}}</th>
                                                                        @for($i=1;$i<=7;$i++)
                                                                        <th>{{trans('general.gn_hour')}}{{$i}}</th>
                                                                        @endfor
                                                                    </tr>
                                                                </thead>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <label>{{trans('general.gn_note')}}</label>
                                                    <input type="text" name="dbl_Notes" id="dbl_Notes" class="form-control icon_control">
                                                </div>
                                            </div>


                                        </div>
                                    </div>
                                    <div class="text-center">
                                        <button type="submit" class="theme_btn">{{trans('general.gn_submit')}}</button>
                                        <a class="theme_btn red_btn" href="{{url('/employee/attendance-accomplishment')}}">{{trans('general.gn_cancel')}}</a>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<a class="hide_content"><button class="theme_btn delete_grade_modal full_width small_btn" data-toggle="modal" data-target="#delete_grade_prompt"></button></a>
<div class="theme_modal modal fade" id="delete_grade_prompt" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <img src="{{asset('images/ic_close_bg.png')}}" class="modal_top_bg">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <img src="{{asset('images/ic_close_circle_white.png')}}">
                </button>
                <div class="row">
                    <div class="col-lg-1"></div>
                    <div class="col-lg-10">
                        <h5 class="modal-title" id="exampleModalCenterTitle">{{trans('general.gn_delete')}}</h5>
                        <div class="form-group text-center">
                            <label>{{trans('general.gn_delete_prompt')}} ?</label>
                            <input type="hidden" id="delete_gradeid">
                        </div>
                        <div class="text-center modal_btn ">
                            <button style="display: none;" class="theme_btn show_delete_modal full_width small_btn" data-toggle="modal" data-target="#delete_prompt">{{trans('general.gn_delete')}}</button>
                            <button type="button" onclick="confirmDeleteGrade()" class="theme_btn">{{trans('general.gn_yes')}}</button>
                            <button type="button" data-dismiss="modal" class="theme_btn red_btn">{{trans('general.gn_no')}}</button>
                        </div>
                    </div>
                    <div class="col-lg-1"></div>
                </div>
            </div>
        </div>
    </div>
</div>

<a class="hide_content"><button class="theme_btn show_modal full_width small_btn" data-toggle="modal" data-target="#course_history_prompt"></button></a>
<div class="theme_modal modal fade" id="course_history_prompt" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-body slim_scroll">
                <img alt="" src="{{asset('images/ic_close_bg.png')}}" class="modal_top_bg">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <img alt="" src="{{asset('images/ic_close_circle_white.png')}}">
                </button>
                <div class="row">
                    <div class="col-lg-1"></div>
                    <div class="col-lg-10">
                        <h5 class="modal-title" id="exampleModalCenterTitle">{{trans('general.gn_view_history')}}</h5>
                        <input type="hidden" id="temp_courseId">
                        <input type="hidden" id="temp_classlableId">
                        <input type="hidden" id="temp_gradeId">
                        <input type="text" id="course_history_search" class="form-control" placeholder="{{trans('general.gn_search')}}">
                        <br>
                        <div class="course_details">
                        </div>
                    </div>
                    <div class="col-lg-1"></div>
                </div>
            </div>
        </div>
    </div>
</div>

<input type="hidden" id="msg_cannot_delete_entry" value="{{trans('message.msg_cannot_delete_entry')}}">
<input type="hidden" id="no_students_error_msg" value="{{trans('message.msg_sel_student')}}">
<script>
    var class_stu_url = "{{route('fetch-classstudents-lists')}}";
    var attendance_url = "{{route('fetch-attendance-lists')}}";
    var class_attendance_detail_url = "{{route('fetch-class-attendance-lists')}}";
    var previous_unit_history_url = "{{route('fetch-previous-unit-history')}}";
    var fetch_course_hours_url = "{{route('fetch-course-hours')}}"
</script>
<!-- End Content Body -->
@endsection
@push('datatable-scripts')
<!-- Include this Page JS -->
<script type="text/javascript" src="{{ asset('js/dashboard/attendanceAccomplishment.js') }}"></script>
@endpush
