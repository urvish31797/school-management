@extends('layout.app_with_login')
@section('title', trans('sidebar.sidebar_nav_sa_sa'))
@section('script',asset('js/dashboard/attendanceAccomplishment.js'))
@section('content')
<!-- Page Content  -->
<div class="section">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12 mb-3">
                <h2 class="title">
                    <a href="{{url('/employee/attendance-accomplishment')}}">
                        <span>{{trans('sidebar.sidebar_nav_sa_sa')}} > </span>
                    </a>
                    {{trans('general.gn_add_new')}}
                </h2>
            </div>
            <div class="col-12">
                <div class="white_box pt-3 pb-3">
                    <div class="container-fluid">
                        <form name="edit-attend-accom-form" id="edit-attend-accom-form">
                            <input type="hidden" name="pkDbl" id="pkDbl" value="{{$id}}">

                            <div class="row">
                                <div class="col-md-12">
                                    <div>
                                        <p class="mt-2"><strong>{{trans('general.gn_add_attendance')}}</strong></p>
                                    </div>
                                    <div class="card-grey">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <p class="label">{{trans('general.gn_class')}} :</p>
                                                    <p class="value">{{$basic_details['classgradename']}}</p>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <p class="label">{{trans('general.gn_village_school')}} :</p>
                                                    <p class="value">{{$basic_details['villageSchool']}}</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="bg-color-form">
                                        <div class="row">
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <label>{{trans('general.gn_date')}} *</label>
                                                    <input type="text" disabled name="dbk_Date" id="dbk_Date" value="{{$basic_details['date']}}" class="form-control icon_control date_control">
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <label>{{trans('general.gn_hour')}} *</label>
                                                    <select class="form-control icon_control dropdown_control" name="dbl_LectureNumber" id="dbl_LectureNumber">
                                                        <option value="">{{trans('general.gn_select')}}</option>
                                                        @for($i=1;$i<=7;$i++)
                                                        <option @if($i == $basic_details['lecturenumber']) selected @endif value="{{$i}}">{{$i}}</option>
                                                        @endfor
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <label>{{trans('general.gn_education_way')}} *</label>
                                                    @include('partials.dropdown.dropdowns',[
                                                    'type'=>'education_way',
                                                    'name'=>'fkDblEw',
                                                    'selected'=>$basic_details['educationwayid'],
                                                    'extra_params'=>[
                                                    "id"=>"fkDblEw",
                                                    ]
                                                    ])
                                                </div>
                                            </div>

                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <label>{{trans('general.gn_attendance_way')}} *</label>
                                                    @include('partials.dropdown.dropdowns',[
                                                    'type'=>'attendance_way',
                                                    'name'=>'fkDblAw',
                                                    'selected'=>$basic_details['attendancewayid'],
                                                    'extra_params'=>[
                                                    "id"=>"fkDblAw",
                                                    ]
                                                    ])
                                                </div>
                                            </div>

                                            @include('employee.attendanceAccomplishment.components.multiple_grade_edit_section')

                                            <div class="col-lg-12" id="student_listing_div">
                                                <div class="form-group">
                                                    <label>{{trans('sidebar.sidebar_nav_students')}}</label>
                                                    <div class="theme_table">
                                                        <div class="table-responsive slim_scroll">
                                                            <table id="students_listing_edit_tbl" class="display" style="width:100%">
                                                                <thead>
                                                                    <tr>
                                                                        <th>{{trans('general.sr_no')}}.</th>
                                                                        <th>{{trans('general.gn_student_name')}}</th>
                                                                        <th>{{trans('general.gn_grade')}}</th>
                                                                        @for($i=1;$i<=7;$i++)
                                                                            <th>{{trans('general.gn_hour')}}{{$i}}</th>
                                                                        @endfor
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                @php $count = 1; @endphp
                                                                @foreach ($attendance_details as $k => $v)
                                                                    @foreach ($v as $details )
                                                                        <tr>
                                                                            <td>{{$count++}}</td>
                                                                            <td>{{$details['studentname']}}</td>
                                                                            <td>{{$details['grade']}}{{$details['class']}}</td>
                                                                            @for($i=1;$i<=7;$i++)
                                                                            <td>
                                                                                <div class="form-group form-check">
                                                                                <input @if($basic_details['lecturenumber'] == $i && $details['original_status'] == "P") checked @else disabled='disabled' @endif type="checkbox" class="form-check-input check_all_{{$i}} stu_checkboxes" name="hour{{$i}}[]" value="{{$details['pkSae']}}">
                                                                                <label class="custom_checkbox"></label>
                                                                                </div>
                                                                            </td>
                                                                            @endfor
                                                                        </tr>
                                                                    @endforeach
                                                                @endforeach
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <label>{{trans('general.gn_note')}}</label>
                                                    <input type="text" name="dbl_Notes" id="dbl_Notes" value="{{$basic_details['note']}}" class="form-control icon_control">
                                                </div>
                                            </div>


                                        </div>
                                    </div>
                                    <div class="text-center">
                                        <button type="submit" class="theme_btn">{{trans('general.gn_submit')}}</button>
                                        <a class="theme_btn red_btn" href="{{url('/employee/attendance-accomplishment')}}">{{trans('general.gn_cancel')}}</a>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<input type="hidden" id="msg_cannot_delete_entry" value="{{trans('message.msg_cannot_delete_entry')}}">
<input type="hidden" id="no_students_error_msg" value="{{trans('message.msg_sel_student')}}">
<script>
    var class_stu_url = "{{route('fetch-classstudents-lists')}}";
    var attendance_url = "{{route('fetch-attendance-lists')}}";
    var class_attendance_detail_url = "{{route('fetch-class-attendance-lists')}}";
    var previous_unit_history_url = "{{route('fetch-previous-unit-history')}}";
    var fetch_course_hours_url = "{{route('fetch-course-hours')}}"
</script>
<!-- End Content Body -->
@endsection
@push('datatable-scripts')
<!-- Include this Page JS -->
<script type="text/javascript" src="{{ asset('js/dashboard/attendanceAccomplishment.js') }}"></script>
@endpush
