@extends('layout.app_with_login')
@section('title', trans('sidebar.sidebar_periodic_exam'))
@section('content')
<div class="section">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12 mb-3">
                <h2 class="title"><span>{{trans('sidebar.sidebar_periodic_exam')}} ></span> {{trans('general.gn_edit')}}</h2>
            </div>
        </div>    
    </div>    
</div>
@endsection
@push('datatable-scripts')
<!-- Include datatable Page JS -->
<script type="text/javascript" id="datatable_script" src="{{ asset('js/dashboard/periodic_exam.js') }}"></script>
@endpush