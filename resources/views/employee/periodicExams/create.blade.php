@extends('layout.app_with_login')
@section('title', trans('sidebar.sidebar_periodic_exam'))
@section('content')
<div class="section">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12 mb-3">
                <h2 class="title"><span>{{trans('sidebar.sidebar_periodic_exam')}} ></span> {{trans('general.gn_add_new')}}</h2>
            </div>
            <div class="col-12">
                <div class="white_box pt-3 pb-3">
                    <div class="container-fluid">
                        <form name="add-periodic-exam-form" id="add-periodic-exam-form">
                            <div class="row">
                                <div class="col-lg-12">
                                    <p class="mt-2"><strong>{{trans('sidebar.sidebar_periodic_exam')}}</strong></p>
                                    <div class="card-grey">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <p class="label">{{trans('general.gn_course')}} :</p>
                                                    <p class="value">{{$components['classInformation']['courseName'] ?? ''}}</p>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <p class="label">{{trans('general.gn_village_school')}} :</p>
                                                    <p class="value">{{$components['classInformation']['villageschoolName'] ?? ''}}</p>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <p class="label">{{trans('general.gn_class')}} :</p>
                                                    <p class="value">{{$components['classInformation']['classInfo'] ?? ''}}</p>
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <label>{{trans('general.gn_date')}} *</label>
                                                    <input type="text" name="pex_Date" id="pex_Date" value="{{date('d/m/Y')}}" class="form-control icon_control date_control" required="">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>{{trans('general.gn_periodic_exam_way')}} *</label>
                                                    @include('partials.dropdown.dropdowns',[
                                                    'type'=>'periodic_exam_way',
                                                    'name'=>'fkPexPew',
                                                    'selected'=>$default_exam_way,
                                                    'extra_params'=>[
                                                    "id"=>"fkPexPew",
                                                    ]
                                                    ])
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>{{trans('general.gn_periodic_exam_type')}} *</label>
                                                    @include('partials.dropdown.dropdowns',[
                                                    'type'=>'periodic_exam_type',
                                                    'name'=>'fkPexPet',
                                                    'selected'=>'',
                                                    'extra_params'=>[
                                                    "id"=>"fkPexPet",
                                                    ]
                                                    ])
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                <label>{{trans('general.gn_exam_content')}} *</label>
                                                <textarea class="form-control" name="pex_ExamContent" id="pex_ExamContent"></textarea>
                                                </div>
                                            </div>

                                            @if(count($components['classRows']) > 1)
                                            @for($i=0;$i<count($components['classRows']);$i++)
                                            <input type="hidden" class="all_grades" name="grades[{{$components['classRows'][$i]['graId']}}]" id="grades_{{$components['classRows'][$i]['graId']}}" value="{{$components['classRows'][$i]['graId']}}">
                                            @endfor
                                            @else
                                            <input type="hidden" name="grades[{{$components['classRows'][0]['graId']}}]" id="grades_{{$components['classRows'][0]['graId']}}" value="{{$components['classRows'][0]['graId']}}">
                                            @endif
                                            
                                            <div class="col-md-2 text-md-left mb-2" id="delete_btn_div" style="display:none">
                                                <button type="button" class="theme_btn red_btn small_btn" onclick="selected_delete()">{{trans('general.gn_delete')}}</button>
                                            </div>           
                                            
                                            <div class="col-md-12">
                                                <div class="table-responsive mt-2">
                                                    <div class="theme_table">
                                                        <div class="table-responsive slim_scroll">
                                                            <table id="students_listing" class="color_table display" style="width:100%">
                                                                <thead>
                                                                    <tr>
                                                                        <th>
                                                                            <div class="form-group form-check">
                                                                                <input type="checkbox" class="form-check-input" id="select-all">
                                                                                <label class="custom_checkbox"></label>
                                                                                <label class="form-check-label label-text" for="select-all"></label>
                                                                            </div>
                                                                        </th>
                                                                        <th>{{trans('general.gn_student_name')}}</th>
                                                                        <th>{{trans('general.gn_grade')}}</th>
                                                                        <th>{{trans('general.gn_numeric_mark')}}</th>
                                                                        <th>{{trans('sidebar.sidebar_nav_dfm')}}</th>
                                                                        <th>{{trans('general.gn_exam_points_number')}}</th>
                                                                    </tr>
                                                                </thead>
                                                            </table>
                                                            <br>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <br>
                                        </div>
                                    </div>
                                    <br>
                                                                            
                                    <div class="text-center">
                                        <button type="submit" class="theme_btn">{{trans('general.gn_submit')}}</button>
                                        <a class="theme_btn red_btn" href="{{url('/employee/periodic-exam')}}">{{trans('general.gn_cancel')}}</a>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<input type="hidden" id="msg_cannot_delete_all_students" value="{{trans('messge.msg_cannot_delete_all_students')}}">
<script>
    var class_stu_url = "{{route('periodic-exam-student.lists')}}";
</script>

@endsection
@push('datatable-scripts')
<!-- Include datatable Page JS -->
<script type="text/javascript" id="datatable_script" src="{{ asset('js/dashboard/periodic_exam.js') }}"></script>
@endpush