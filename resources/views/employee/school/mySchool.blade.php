@extends('layout.app_with_login')
@section('title', trans('sidebar.sidebar_nav_my_school'))
@section('script', asset('js/dashboard/my_school.js'))
@section('content')
<!-- Page Content  -->
<div class="section">
	<div class="container-fluid">
		<div class="row">
			<div class="col-12 mb-3">
               <h2 class="title"><span>{{trans('general.gn_school')}} > </span> {{trans('sidebar.sidebar_nav_my_school')}}</h2>
            </div>
		</div>

        <div class="white_box">
            <div class="theme_tab">
                <ul class="nav nav-tabs" id="myTab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">{{trans('general.gn_basic_information')}}</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="Current-tab" data-toggle="tab" href="#Current" role="tab" aria-controls="Current" aria-selected="true">{{trans('general.gn_about')}}</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="Three-tab" data-toggle="tab" href="#Three" role="tab" aria-controls="Three" aria-selected="true">{{trans('general.gn_education_program')}}</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="Fourth-tab" data-toggle="tab" href="#Fourth" role="tab" aria-controls="Fourth" aria-selected="true">{{trans('general.gn_principal')}}</a>
                    </li>
                </ul>
                <div class="tab-content" id="myTabContent">
                    <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                        <div class="inner_tab" id="profile_detail">
                            <div class="row">
                                <div class="col-lg-1"></div>
                                <div class="col-lg-10">
                                    <div class="text-center">
                                        <div class="profile_box">
                                            <div class="profile_pic">
                                                <img src="@if(!empty($SchoolDetail->sch_SchoolLogo)) {{asset('images/schools/')}}/{{$SchoolDetail->sch_SchoolLogo}} @else {{ asset('images/img4.png') }}@endif">
                                            </div>
                                        </div>
                                        <h5 class="profile_name">{{$SchoolDetail['sch_SchoolName_'.$current_language]}}</h5>
                                    </div>
                                    <div class="container-fluid">
                                        <div class="row">
                                        	<div class="col-12">
                                        		<h6 class="profile_inner_title">{{trans('general.gn_basic_information')}}</h6>
                                        	</div>
                                            <div class="col-md-6 col-lg-4">
                                                <div class="form-group">
                                                    <p class="label">{{trans('general.gn_school')}} ID :</p>
                                                    <p class="value">@if(!empty($SchoolDetail->sch_SchoolId)){{$SchoolDetail->sch_SchoolId}}@endif</p>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-lg-4">
                                                <div class="form-group">
                                                    <p class="label">{{trans('general.gn_ministry_license_number')}}:</p>
                                                    <p class="value">@if(!empty($SchoolDetail->sch_MinistryApprovalCertificate)){{$SchoolDetail->sch_MinistryApprovalCertificate}}@endif</p>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-lg-4">
                                                <div class="form-group">
                                                    <p class="label">{{trans('general.gn_email')}} :</p>
                                                    <p class="value">@if(!empty($SchoolDetail->sch_SchoolEmail)){{$SchoolDetail->sch_SchoolEmail}}@endif</p>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-lg-4">
                                                <div class="form-group">
                                                    <p class="label">{{trans('general.gn_phone')}} :</p>
                                                    <p class="value">@if(!empty($SchoolDetail->sch_PhoneNumber)){{$SchoolDetail->sch_PhoneNumber}}@endif</p>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-lg-4">
                                                <div class="form-group">
                                                    <p class="label">{{trans('general.gn_zipcode')}} :</p>
                                                    <p class="value">@if(isset($SchoolDetail->postalCode)){{$SchoolDetail->postalCode->pof_PostOfficeNumber}}@endif</p>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-lg-4">
                                                <div class="form-group">
                                                    <p class="label">{{trans('general.gn_address')}} :</p>
                                                    <p class="value">@if(!empty($SchoolDetail->sch_Address)){{$SchoolDetail->sch_Address}}@endif</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="container-fluid">
                                        <div class="row">
                                            <div class="col-12">
                                                <h6 class="profile_inner_title">{{trans('general.gn_school_coordinator')}}</h6>
                                            </div>
                                            <div class="col-md-6 col-lg-4">
                                                <div class="form-group">
                                                    <p class="label">{{trans('general.gn_name')}} :</p>
                                                    <p class="value">{{$logged_user->emp_EmployeeName}} {{$logged_user->emp_EmployeeSurname}}</p>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-lg-4">
                                                <div class="form-group">
                                                    <p class="label">{{trans('general.gn_email')}} :</p>
                                                    <p class="value">{{$logged_user->email}}</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="text-center">
                                        <button class="theme_btn" id="edit_profile">{{trans('general.gn_edit_profile')}}</button>
                                    </div>
                                </div>
                                <div class="col-lg-1"></div>
                            </div>
                        </div>

                        <div class="inner_tab" id="edit_profile_detail" style="display: none;">
                        	<form id="edit-profile-form" name="school-basic">
                        	<div class="row">
                                <div class="col-lg-3"></div>
                                <div class="col-lg-6">
                                    <div class="text-center">
                                        <div class="profile_box">
                                            <div class="profile_pic">
                                                <img id="user_img" src="@if(!empty($SchoolDetail->sch_SchoolLogo)) {{asset('images/schools/')}}/{{$SchoolDetail->sch_SchoolLogo}} @else {{ asset('images/img4.png') }}@endif">
                                            </div>
                                            <div class="edit_pencile">
                                            	<img src="{{asset('images/ic_pen.png')}}">
                                            	<input type="file" id="upload_profile" name="upload_profile">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-3"></div>
                                <div class="col-lg-10 offset-lg-1">
                                	<div class="container-fluid">
                                        <div class="row">
                                        	<div class="col-12">
                                        		<h6 class="profile_inner_title">{{trans('general.gn_basic_information')}}</h6>
                                        	</div>
                                        	<input type="hidden" id="sid" name="sid" value="{{$SchoolDetail->pkSch}}">
                                        	@foreach($languages as $k => $v)
                                        	<div class="col-md-6">
				                                <div class="form-group">
				                                    <label>{{trans('general.gn_school')}} {{trans('general.gn_name')}} {{$v->language_name}} *</label>
				                                    <input type="text" name="sch_SchoolName_{{$v->language_key}}" id="sch_SchoolName_{{$v->language_key}}" class="form-control force_require icon_control" required="" value="{{$SchoolDetail['sch_SchoolName_'.$v->language_key]}}">
				                                </div>
				                            </div>
				                            @endforeach
                                        	<div class="col-md-6">
                                        		<div class="form-group">
                                                	<label>{{trans('general.gn_school')}} ID *</label>
                                                	<input type="text" name="sch_SchoolId" id="sch_SchoolId" class="form-control" value="@if(!empty($SchoolDetail->sch_SchoolId)){{$SchoolDetail->sch_SchoolId}}@endif">
                                                </div>
                                        	</div>
                                        	<div class="col-md-6">
                                        		<div class="form-group">
                                                	<label>{{trans('general.gn_email')}} *</label>
                                                	<input type="text" name="sch_SchoolEmail" id="sch_SchoolEmail" class="form-control" value="@if(!empty($SchoolDetail->sch_SchoolEmail)){{$SchoolDetail->sch_SchoolEmail}}@endif">
                                                </div>
                                        	</div>
                                        	<div class="col-md-6">
                                        		<div class="form-group">
                                                	<label>{{trans('general.gn_phone')}} *</label>
                                                	<input type="number" name="sch_PhoneNumber" id="sch_PhoneNumber" class="form-control" value="@if(!empty($SchoolDetail->sch_PhoneNumber)){{$SchoolDetail->sch_PhoneNumber}}@endif">
                                                </div>
                                        	</div>
                                        	<div class="col-md-6">
	                                        	<div class="form-group">
			                                        <label>{{trans('general.gn_postal_code')}} *</label>
  			                                        <select name="fkSchPof" id="fkSchPof" class="form-control icon_control dropdown_control">
  			                                        	<option value="">{{trans('general.gn_select')}}</option>
  			                                            @foreach($PostalCodes as $k => $v)
  			                                            	<option @if($SchoolDetail->fkSchPof == $v->pkPof) selected @endif value="{{$v->pkPof}}">{{$v->pof_PostOfficeNumber}}</option>
  			                                            @endforeach
  			                                        </select>
  			                                    </div>
			                                    </div>
    			                                <div class="col-md-6">
    			                                    <div class="form-group">
    			                                        <label>{{trans('general.gn_ownership_type')}} *</label>
    			                                        <select name="fkSchOty" id="fkSchOty" class="form-control icon_control dropdown_control">
    			                                        	<option value="">{{trans('general.gn_select')}}</option>
    			                                            @foreach($OwnershipTypes as $k => $v)
    			                                            	<option @if($SchoolDetail->fkSchOty == $v->pkOty) selected @endif value="{{$v->pkOty}}">{{$v->oty_OwnershipTypeName}}</option>
    			                                            @endforeach
    			                                        </select>
    			                                    </div>
    			                                </div>
                                        	<div class="col-md-6">
                                        		<div class="form-group">
                                                	<label>{{trans('general.gn_address')}}</label>
                                                	<input type="text" id="sch_Address" name="sch_Address" class="form-control" value="@if(!empty($SchoolDetail->sch_Address)){{$SchoolDetail->sch_Address}}@endif">
                                                </div>
                                        	</div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>{{trans('general.gn_founder')}} *</label>
                                                    <input type="text" name="sch_Founder" id="sch_Founder" class="form-control" value="@if(!empty($SchoolDetail->sch_Founder)){{$SchoolDetail->sch_Founder}}@endif">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>{{trans('general.gn_founding_date')}} *</label>
                                                    <input type="text" name="sch_FoundingDate" id="sch_FoundingDate" class="form-control date_control datepicker icon_control" value="@if(!empty($SchoolDetail->sch_FoundingDate)){{$SchoolDetail->sch_FoundingDate}}@endif">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                              <div class="form-group">
                                                  <label>{{trans('general.gn_running_semester')}} *</label>
                                                  <select name="fkSchEdp" id="fkSchEdp" class="form-control icon_control dropdown_control">
                                                      @foreach($educationPeriods as $k => $v)
                                                        <option @if($SchoolDetail->fkSchEdp == $v->pkEdp) selected @endif value="{{$v->pkEdp}}">{{$v->edp_EducationPeriodName}}</option>
                                                      @endforeach
                                                  </select>
                                              </div>
                                            </div>
                                        </div>
                                    </div>
                                	 <div class="text-center">
                                        <button type="submit" class="theme_btn">{{trans('general.gn_update')}}</button>
                                        <button type="button" id="cancel_edit_profile" class="theme_btn red_btn">{{trans('general.gn_cancel')}}</button>
                                    </div>
                                </div>
                            </div>
                            </form>
                        </div>
                    </div>
                    <div class="tab-pane fade show" id="Current" role="tabpanel" aria-labelledby="Current-tab">
                        <div class="inner_tab" id="profile_detail2">
                            <div class="row">
                                <div class="col-lg-1"></div>
                                <div class="col-lg-10">
                                    <div class="container-fluid">
                                        <div class="row">
                                        	<div class="col-12">
                                        		<h6 class="profile_inner_title">{{trans('general.gn_about')}} {{trans('general.gn_school')}}</h6>
                                        	</div>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <p class="value">{{$SchoolDetail->sch_AboutSchool}}</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="container-fluid">
                                        <div class="row">
                                        	<div class="col-12">
                                        		<h6 class="profile_inner_title">{{trans('general.gn_photos_of_school')}}</h6>
                                        	</div>
                                        	@foreach($SchoolDetail->schoolPhoto as $k => $v)
                                    		<div class="col-lg-2 col-md-4 text-center">
                                    			<img src="{{asset('images/schools')}}/{{$v->sph_SchoolPhoto}}" class="rounded mb-2">
                                    		</div>
                                    		@endforeach
                                        </div>
                                    </div>
                                    <div class="text-center">
                                        <button class="theme_btn " id="edit_profile2" >{{trans('general.gn_edit_profile')}}</button>
                                    </div>
                                </div>
                                <div class="col-lg-1"></div>
                            </div>
                        </div>

                        <div class="inner_tab" id="edit_profile_detail2" style="display: none;">
                        	<form id="edit-profile-form" name="school-about">
                        	<div class="row">
                                <input type="hidden" id="sid" name="sid" value="{{$SchoolDetail->pkSch}}">
                                <div class="col-lg-10 offset-lg-1">
                                	<div class="container-fluid">
                                        <div class="row">
                                        	<div class="col-12">
                                        		<h6 class="profile_inner_title">{{trans('general.gn_about')}} {{trans('general.gn_school')}}</h6>
                                        	</div>
                                        	<div class="col-md-12">
                                        		<div class="form-group">
                                                	<textarea rows="6" name="sch_AboutSchool" id="sch_AboutSchool" class="form-control">{{$SchoolDetail->sch_AboutSchool}}</textarea>
                                                </div>
                                        	</div>
                                        </div>
                                    </div>
                                    <div class="container-fluid">
                                        <div class="row">
                                        	<div class="col-12">
                                        		<h6 class="profile_inner_title">{{trans('general.gn_photos_of_school')}}</h6>
                                        	</div>
                                        	<div class="col-12 img_show_div">
                                        		@foreach($SchoolDetail->schoolPhoto as $k => $v)
                                        		<div class="col-md-2 old_sch_imgs" id="sch_img_{{$v->pkSph}}">
                                        			<img src="{{asset('images/schools')}}/{{$v->sph_SchoolPhoto}}">
                                        			<span class="close_spn" oid="{{$v->pkSph}}"><img src="{{asset('images/ic_delete.png')}}"></span>
                                        		</div>
                                        		@endforeach
                                        	</div>
                                        	<div class="col-md-12 text-center">
                                        		<input multiple id="school_imgs" name="school_imgs[]" type="file" style="display: none;">
                                                <button class="theme_btn min_btn" id="upload_link" type="button">{{trans('general.gn_browse')}}</button>
                                        	</div>
                                        </div>
                                    </div>
                                	 <div class="text-center">
                                        <button type="submit" class="theme_btn">{{trans('general.gn_update')}}</button>
                                        <button type="button" id="cancel_edit_profile2" class="theme_btn red_btn">{{trans('general.gn_cancel')}}</button>
                                    </div>
                                </div>
                            </div>
                            </form>
                        </div>

                    </div>
                    <div class="tab-pane fade show" id="Three" role="tabpanel" aria-labelledby="Three-tab">
                        <div class="inner_tab" id="profile_detail3">
                            <div class="row">
                                <div class="col-lg-1"></div>
                                <div class="col-lg-10">

                                    <div class="container-fluid">
                                        <div class="row">
                                        	<div class="col-12">
                                        		<h6 class="profile_inner_title">{{trans('general.gn_education_program')}}</h6>
                                                <table class="color_table school_plan_table">
                                                    <tr>
                                                        <th width="7%">{{trans('general.sr_no')}}</th>
                                                        <th width="15%">{{trans('general.gn_parent_category')}}</th>
                                                        <th width="12%">{{trans('general.gn_child_category')}}</th>
                                                        <th width="15%">{{trans('general.gn_education_plan')}}</th>
                                                        <th width="15%">{{trans('general.gn_national_education_plan')}}</th>
                                                        <th width="12%">{{trans('general.gn_qualification_degree')}}</th>
                                                        <th width="12%">{{trans('general.gn_education_profile')}}</th>
                                                        <th width="10%">{{trans('general.gn_status')}}</th>
                                                    </tr>
                                                    @foreach($SchoolDetail->schoolEducationPlanAssignment as $k => $v)
                                                        <tr class="sch sch_{{$v->educationPlan->pkEpl}} ">
                                                            <td>{{$k+1}}</td>
                                                            <td>@if($v->educationProgram->edp_ParentId == 0) - @else {{$v->educationProgram->parent['edp_Name_'.$current_language]}} @endif</td>
                                                            <td>{{$v->educationProgram['edp_Name_'.$current_language]}}</td>
                                                            <td>{{$v->educationPlan['epl_EducationPlanName_'.$current_language]}}</td>
                                                            <td>{{$v->educationPlan->nationalEducationPlan['nep_NationalEducationPlanName_'.$current_language]}}</td>
                                                            <td>{{$v->educationPlan->QualificationDegree['qde_QualificationDegreeName_'.$current_language]}}</td>
                                                            <td>{{$v->educationPlan->educationProfile['epr_EducationProfileName_'.$current_language]}}</td>
                                                            <td>@if($v->sep_Status == 'Active') {{trans('general.gn_active')}} @else {{trans('general.gn_inactive')}} @endif
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                </table>
                                        	</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-1"></div>
                            </div>
                        </div>

                    </div>
                    <div class="tab-pane fade show" id="Fourth" role="tabpanel" aria-labelledby="Fourth-tab">
                        <div class="inner_tab" id="profile_detail4">
                            <div class="row">
                                <div class="col-lg-1"></div>
                                <div class="col-lg-10">

                                    <div class="container-fluid">
                                        <div class="row">
                                            <div class="col-12">
                                                <h6 class="profile_inner_title">{{trans('general.gn_principal')}}</h6>
                                            </div>
                                            <div class="col-md-4 col-lg-4">
                                                <?php
                                                    $pname = '';
                                                    $date = '';
                                                    $principalId = 0;
                                                    $acting = '';
                                                    foreach($SchoolDetail->employeesEngagement as $k => $v){
                                                        if(isset($v->employeeType->epty_Name) && $v->employeeType->epty_Name=='Principal') {
                                                            $pname = $v->employee->emp_EmployeeName." ".$v->employee->emp_EmployeeSurname;
                                                            $principalId = $v->pkEen;
                                                            $date = $v->een_DateOfEngagement;
                                                            $acting = $v->een_ActingAsPrincipal;
                                                        }
                                                    }
                                                ?>
                                                <div class="form-group">
                                                    <p class="label">{{trans('general.gn_name')}} :</p>
                                                    <p class="value">{{$pname}}</p>
                                                </div>
                                            </div>
                                            <div class="col-md-4 col-lg-4">
                                                <div class="form-group">
                                                    <p class="label">{{trans('general.gn_duration')}} :</p>
                                                    <p class="value">{{$date}} - {{trans('general.gn_present')}}</p>
                                                </div>
                                            </div>
                                            <div class="col-md-4 col-lg-4">
                                                <div class="form-group">
                                                    <p class="label">{{trans('general.gn_acting_principal')}} :</p>
                                                    <p class="value">
                                                    @if($acting == 'Yes')
                                                    {{trans('general.gn_yes')}}
                                                    @else
                                                    {{trans('general.gn_no')}}
                                                    @endif
                                                    </p>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                    <div class="text-center">
                                        <button class="theme_btn" id="edit_profile4" type="button">{{trans('general.gn_edit_profile')}}</button>
                                    </div>
                                </div>
                                <div class="col-lg-1"></div>
                            </div>
                        </div>

                        <div class="inner_tab" id="edit_profile_detail4" style="display: none;">
                            <form name="school_principal">
                            <div class="row">
                                <input type="hidden" id="sid" name="sid" value="{{$SchoolDetail->pkSch}}">
                                <div class="col-lg-10 offset-lg-1">
                                    <div class="container-fluid">
                                        <div class="row">
                                            <div class="col-12">
                                                <h6 class="profile_inner_title">{{trans('general.gn_principal')}}</h6>
                                            </div>
                                            <div class="col-md-6 ">
                                                <div class="form-group">
                                                    <label>{{trans('general.gn_start_date')}} *</label>
                                                    <input type="text" id="start_date" name="start_date" class="form-control icon_control date_control start_date" value="{{$date}}">
                                                </div>
                                            </div>
                                            <div class="col-md-6 exists_employee">
                                                <div class="form-group">
                                                    <label>{{trans('general.gn_employees')}} *</label>
                                                    <select name="principal_sel" id="principal_sel" class="form-control icon_control dropdown_control">
                                                        <option value="">{{trans('general.gn_select')}}</option>
                                                        @foreach($SchoolDetail->employeesEngagement as $k => $v)
                                                            <?php
                                                                $type = '';
                                                                if(isset($v->employeeType->epty_Name))
                                                                {
                                                                    if($v->employeeType->epty_Name=='SchoolCoordinator'){
                                                                        $type = trans('general.gn_school_coordinator');
                                                                    }elseif ($v->employeeType->epty_Name=='Teacher') {
                                                                        $type = trans('general.gn_teacher');
                                                                    }elseif ($v->employeeType->epty_Name=='Principal') {
                                                                        $type = trans('general.gn_principal');
                                                                    }
                                                                }

                                                            ?>
                                                            <option @if($v->pkEen == $principalId) selected="selected" @endif value="{{$v->pkEen}}">{{$v->employee->emp_EmployeeName}} {{$v->employee->emp_EmployeeSurname}} ({{$type}})</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div class="form-group form-check" id="principal_checkbox">
                                                    <input @if($acting == 'Yes') checked="checked" @endif name="een_ActingAsPrincipal" type="checkbox" class="form-check-input" id="een_ActingAsPrincipal">
                                                    <label class="custom_checkbox"></label>
                                                    <label class="form-check-label" for="exampleCheck1">{{trans('general.gn_acting_principal')}}</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                     <div class="text-center">
                                        <button type="submit" class="theme_btn">{{trans('general.gn_update')}}</button>
                                        <button type="button" id="cancel_edit_profile4" class="theme_btn red_btn">{{trans('general.gn_cancel')}}</button>
                                    </div>
                                </div>
                            </div>
                            </form>
                        </div>

                    </div>
                </div>
            </div>
        </div>
		</div>
</div>
@endsection
@push('datatable-scripts')
<!-- Include this Page JS -->
<script type="text/javascript" src="{{ asset('js/dashboard/my_school.js') }}"></script>
@endpush