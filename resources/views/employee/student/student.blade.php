@extends('layout.app_with_login')
@section('title', trans('sidebar.sidebar_nav_students'))
@section('script', asset('js/dashboard/student.js'))
@section('content')
 <!-- Page Content  -->
<div class="section">
	<div class="container-fluid">
		<div class="row ">
            <div class="col-12 mb-3">
                @if($logged_user->type=='MinistryAdmin')
                    <h2 class="title">{{trans('sidebar.sidebar_nav_students')}} </h2>
                @else
                    <h2 class="title"><span>@if(Request::is('employee/students')){{trans('sidebar.sidebar_nav_user_management')}} @else {{trans('sidebar.sidebar_nav_ministry_masters')}} @endif > </span>{{trans('sidebar.sidebar_nav_students')}}</h2>
                @endif
            </div>
            <div class="col-md-4 mb-3">
                <input type="text" id="search_student" maxlength="30" class="form-control icon_control search_control" placeholder="{{trans('general.gn_search')}}">
            </div>
            <div class="col-md-4 text-md-right mb-3">
                <div class="row">

                </div>
            </div>
            <div class="col-md-2 col-6 mb-3">

            </div>
            <div class="col-md-2 col-6 mb-3">
                @if(Request::is('employee/students'))
                <a><button class="theme_btn small_btn" data-toggle="modal" data-target="#add_new">{{trans('general.gn_add_new')}}</button></a>
                @endif
            </div>
        </div>
        @if($logged_user->type == 'HertronicAdmin' || $logged_user->type=='MinistryAdmin')
            <input type="hidden" id="is_HSA">
        @endif

        <div class="row">
            <div class="col-12">
                <div class="theme_table">
                    <div class="table-responsive">
                        <table id="student_listing" class="display" style="width:100%">
                            <thead>
                                <tr>
                                    <th>{{trans('general.sr_no')}}.</th>
                                    <th>{{trans('general.gn_student_id')}}</th>
                                    <th>{{trans('general.gn_student_name')}}</th>
                                    <th>{{trans('general.gn_place_of_birth')}}</th>
                                    <th>{{trans('general.gn_gender')}}</th>
                                    <th>{{trans('general.gn_address')}}</th>
                                    <th><div class="action">{{trans('general.gn_actions')}}</div></th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
	</div>
    <div class="theme_modal modal fade" id="add_new" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <img alt="" src="{{asset('images/ic_close_bg.png')}}" class="modal_top_bg">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <img alt="" src="{{asset('images/ic_close_circle_white.png')}}">
                    </button>
                    <div class="row">
                        <div class="col-lg-1"></div>
                        <div class="col-lg-10">
                            <h5 class="modal-title" id="exampleModalCenterTitle">{{trans('general.gn_student')}}</h5>

                            <div class="text-center modal_btn">
                                <a href="{{url('employee/students/create')}}">
                                    <button type="button" class="theme_btn">{{trans('general.gn_add_new_student')}}</button>
                                </a>

                                <p class="mt-3 mb-3">{{trans('general.gn_or')}}</p>
                                <a  href="{{url('/employee/enrollstudents/create')}}">
                                    <button type="button" class="theme_btn">
                                    {{trans('general.gn_enroll_student')}}
                                    </button>
                                </a>
                            </div>
                            <div class="text-center mt-3">
                                <span><small> ( {{trans('general.gn_enroll_student_note')}} )</small></span>
                            </div>
                        </div>
                        <div class="col-lg-1"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="theme_modal modal fade" id="delete_prompt" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <img alt="" src="{{asset('images/ic_close_bg.png')}}" class="modal_top_bg">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <img alt="" src="{{asset('images/ic_close_circle_white.png')}}">
                </button>
                    <div class="row">
                        <div class="col-lg-1"></div>
                        <div class="col-lg-10">
                            <h5 class="modal-title" id="exampleModalCenterTitle">{{trans('general.gn_delete')}}</h5>
                            <div class="form-group text-center">
                                <label>{{trans('general.gn_delete_prompt')}} ?</label>
                                <input type="hidden" id="did">
                            </div>
                            <div class="text-center modal_btn ">
                                <button style="display: none;" class="theme_btn show_delete_modal full_width small_btn" data-toggle="modal" data-target="#delete_prompt">{{trans('general.gn_delete')}}</button>
                                <button type="button" onclick="confirmDelete()" class="theme_btn">{{trans('general.gn_yes')}}</button>
                                <button type="button" data-dismiss="modal" class="theme_btn red_btn">{{trans('general.gn_no')}}</button>
                            </div>
                        </div>
                        <div class="col-lg-1"></div>
                    </div>
            </div>
        </div>
    </div>
</div>
<script>
    @if($logged_user->type=='HertronicAdmin' || $logged_user->type=='MinistryAdmin')
        var listing_url = "{{route('fetch-students-lists')}}";
        var fetch_edu_url = "";
    @else
        var listing_url = "{{route('fetch-student-lists')}}";
        var fetch_edu_url = "{{route('fetch-student-educationplan')}}";
    @endif
</script>
@endsection

@push('datatable-scripts')
<!-- Include datatable Page JS -->
<script type="text/javascript" src="{{ asset('js/dashboard/student.js') }}"></script>
@endpush