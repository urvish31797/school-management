@extends('layout.app_with_login')
@section('title', trans('sidebar.sidebar_nav_students'))
@section('content')
<!-- Page Content  -->
<div class="section">
    <div class="container-fluid">
        @if ($logged_user->type == 'MinistryAdmin')
        <h2 class="title"><a class="no_sidebar_active" href="{{ url('admin/students') }}"> <span>{{ trans('sidebar.sidebar_nav_students') }} >
                </span></a>
            {{ trans('general.gn_details') }}</h2>
        @else
        <h2 class="title"><span>
                @if ($logged_user->type != 'HertronicAdmin')
                {{ trans('sidebar.sidebar_nav_user_management') }} >
            </span><a class="no_sidebar_active" href="{{ url('employee/students') }}">@else {{ trans('sidebar.sidebar_nav_ministry_masters') }}>
                </span><a class="no_sidebar_active" href="{{ url('admin/students') }}">
                    @endif <span>{{ trans('sidebar.sidebar_nav_students') }} > </span></a>
                {{ trans('general.gn_details') }}</h2>
        @endif
        <div class="white_box">
            @if ($logged_user->type == 'HertronicAdmin' || $logged_user->type == 'MinistryAdmin')
            <input type="hidden" id="is_HSA">
            @endif
            <div class="theme_tab">
                <ul class="nav nav-tabs" id="myTab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">{{ trans('general.gn_general_information') }}</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="Current-tab" data-toggle="tab" href="#Current" role="tab" aria-controls="Current" aria-selected="true">{{ trans('general.gn_details_enrollment') }}</a>
                    </li>
                </ul>
                <div class="tab-content" id="myTabContent">
                    <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                        <div class="row">
                            <div class="col-lg-1"></div>
                            <div class="col-lg-10">
                                <div class="text-center">
                                    <div class="profile_box">
                                        <div class="profile_pic">
                                            <img src="@if (!empty($mdata->stu_PicturePath)) {{ asset('images/students/') }}/{{ $mdata->stu_PicturePath }}
                                            @else {{ asset('images/user.png') }} @endif">
                                        </div>
                                    </div>
                                    <h5 class="profile_name">{{ $mdata->stu_StudentName }}</h5>
                                </div>
                                <div class="profile_info_container">
                                    <div class="row">
                                        <div class="col-md-6 col-lg-3">
                                            <div class="form-group">
                                                <p class="label">{{ trans('general.gn_student_name') }}:</p>
                                                <p class="value">{{ $mdata->stu_StudentName }}
                                                    {{ $mdata->stu_StudentSurname }}</p>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-lg-3">
                                            <div class="form-group">
                                                <p class="label">{{ trans('general.gn_temp_citizen_id') }} :</p>
                                                <p class="value">{{ $mdata->stu_TempCitizenId }}</p>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-lg-3">
                                            <div class="form-group">
                                                <p class="label">{{ trans('general.gn_student_id') }} :</p>
                                                <p class="value">{{ $mdata->stu_StudentID }}</p>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-lg-3">
                                            <div class="form-group">
                                                <p class="label">{{ trans('general.gn_gender') }} :</p>
                                                <p class="value">{{ $mdata->stu_StudentGender }}</p>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-lg-3">
                                            <div class="form-group">
                                                <p class="label">{{ trans('general.gn_date_Of_birth') }} :</p>
                                                <p class="value">
                                                    @if (!empty($mdata->stu_DateOfBirth))
                                                    {{ date('d/m/Y', strtotime($mdata->stu_DateOfBirth)) }}
                                                    @endif
                                                </p>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-lg-3">
                                            <div class="form-group">
                                                <p class="label">{{ trans('general.gn_place_of_birth') }} :</p>
                                                <p class="value">{{ $mdata->stu_PlaceOfBirth }}</p>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-lg-3">
                                            <div class="form-group">
                                                <p class="label">{{ trans('general.gn_address') }} :</p>
                                                <p class="value">{{ $mdata->stu_Address }}</p>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-lg-3">
                                            <div class="form-group">
                                                <p class="label">{{ trans('general.gn_MunicipalityofBirth') }} :</p>
                                                <p class="value">
                                                    {{ $mdata->municipality->mun_MunicipalityName ?? '' }}
                                                </p>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-lg-3">
                                            <div class="form-group">
                                                <p class="label">{{ trans('general.gn_nationality') }} :</p>
                                                <p class="value">{{ $mdata->nationality->nat_NationalityName ?? '' }}
                                                </p>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-lg-3">
                                            <div class="form-group">
                                                <p class="label">{{ trans('general.gn_country') }} :</p>
                                                <p class="value">{{ $mdata->country->cny_CountryName ?? '' }}</p>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-lg-3">
                                            <div class="form-group">
                                                <p class="label">{{ trans('general.gn_religions') }} :</p>
                                                <p class="value">{{ $mdata->riligeion->rel_ReligionName ?? '' }}</p>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-lg-3">
                                            <div class="form-group">
                                                <p class="label">{{ trans('general.gn_student_email') }} :</p>
                                                <p class="value">{{ $mdata->email }}</p>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-lg-3">
                                            <div class="form-group">
                                                <p class="label">{{ trans('general.gn_distance_in_kilometers') }} :
                                                </p>
                                                <p class="value">{{ $mdata->stu_DistanceInKilometers }}</p>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-lg-3">
                                            <div class="form-group">
                                                <p class="label">{{ trans('general.gn_mobile_phone_number') }} :</p>
                                                <p class="value">{{ $mdata->stu_MobilePhoneNumber }}</p>
                                            </div>
                                        </div>

                                        <div class="col-md-6 col-lg-3">
                                            <div class="form-group">
                                                <p class="label">{{ trans('general.gn_phone') }} :</p>
                                                <p class="value">{{ $mdata->stu_PhoneNumber }}</p>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-lg-3">
                                            <div class="form-group">
                                                <p class="label">{{ trans('general.gn_ResidencePostoffice') }} :</p>
                                                <p class="value">{{ $mdata->postalCode->pof_PostOfficeName }} -
                                                    {{ $mdata->postalCode->pof_PostOfficeNumber ?? '' }}</p>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-lg-3">
                                            <div class="form-group">
                                                <p class="label">{{ trans('general.gn_father_name') }} :</p>
                                                <p class="value">{{ $mdata->stu_FatherName }}</p>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-lg-3">
                                            <div class="form-group">
                                                <p class="label">{{ trans('general.gn_mother_name') }} :</p>
                                                <p class="value">{{ $mdata->stu_MotherName }}</p>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-lg-3">
                                            <div class="form-group">
                                                <p class="label">{{ trans('general.gn_father_job') }} :</p>
                                                <p class="value">{{ $mdata->jawFather->jaw_Name_en ?? '' }}</p>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-lg-3">
                                            <div class="form-group">
                                                <p class="label">{{ trans('general.gn_parent_email') }} :</p>
                                                <p class="value">{{ $mdata->stu_ParentsEmail }}</p>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-lg-3">
                                            <div class="form-group">
                                                <p class="label">{{ trans('general.gn_Parent_phone_no') }} :</p>
                                                <p class="value">{{ $mdata->stu_ParentsEmail }}</p>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-lg-3">
                                            <div class="form-group">
                                                <p class="label">{{ trans('general.gn_special_needs') }} :</p>
                                                <p class="value">{{ $mdata->stu_SpecialNeed }}</p>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-lg-3">
                                            <div class="form-group">
                                                <p class="label">{{ trans('general.gn_notes') }} :</p>
                                                <p class="value">{{ $mdata->stu_Notes }}</p>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-1"></div>
                        </div>

                    </div>
                    <div class="tab-pane fade show" id="Current" role="tabpanel" aria-labelledby="Current-tab">
                        <div class="row">
                            <div class="col-lg-1"></div>
                            <div class="col-lg-10">
                                <div class="profile_info_container">
                                    <div class="row">
                                        <div class="col-12">
                                        </div>
                                        <div class="col-12">
                                            <div class="table-responsive">
                                                <table class="profile_table">
                                                    <tbody>
                                                        @foreach ($mdata->enrollStudent as $k => $v)
                                                        <tr>
                                                            <td>{{ $k + 1 }}</td>
                                                            <td>
                                                                <p class="label">{{ trans('general.gn_grade') }}
                                                                    :
                                                                </p>
                                                                <p class="value">
                                                                    {{ $v->grade->gra_GradeNumeric }}
                                                                </p>
                                                                <br>
                                                                <p class="label">
                                                                    {{ trans('general.gn_school_year') }} :</p>
                                                                <p class="value">
                                                                    {{ $v->schoolYear->sye_NameCharacter }}</p>
                                                            </td>
                                                            <td>
                                                                <p class="label">
                                                                    {{ trans('general.gn_education_program') }} :
                                                                </p>
                                                                <p class="value">
                                                                    {{ $v->educationProgram->edp_Name }}</p>
                                                                <br>
                                                                <p class="label">
                                                                    {{ trans('general.gn_education_plan') }} :
                                                                </p>
                                                                <p class="value">
                                                                    {{ $v->educationPlan->epl_EducationPlanName }}
                                                                </p>
                                                            </td>
                                                            <td>
                                                                <p class="label">
                                                                    {{ trans('general.gn_date_of_enrollment') }}
                                                                    :
                                                                </p>
                                                                <p class="value">
                                                                    {{ UtilHelper::hertronicDate($v->ste_EnrollmentDate) }}
                                                                </p>
                                                                <br>
                                                                <p class="label">
                                                                    {{ trans('general.gn_date_of_end_enrollment') }}
                                                                    :</p>
                                                                <p class="value">
                                                                    {{ UtilHelper::hertronicDate($v->ste_EnrollmentFinishDate) }}
                                                                </p>
                                                            </td>
                                                            <td>
                                                                {{-- @if ($logged_user->type == 'HertronicAdmin' || $logged_user->type == 'MinistryAdmin') --}}
                                                                <p class="label">{{ trans('general.gn_school') }}
                                                                    :
                                                                </p>
                                                                <p class="value">{{ $v->school->sch_SchoolName }}
                                                                </p>
                                                                <br>
                                                                {{-- @endif --}}
                                                                <p class="label">
                                                                    {{ trans('general.gn_village_school') }} :
                                                                </p>
                                                                <p class="value">
                                                                    {{ $v->villageschool->vsc_VillageSchoolName ?? '-' }}
                                                                </p>
                                                            </td>
                                                        </tr>
                                                        @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-1"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End Content Body -->
@endsection
