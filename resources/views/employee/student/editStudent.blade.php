@extends('layout.app_with_login')
@section('title', trans('sidebar.sidebar_nav_students'))
@section('script', asset('js/dashboard/student.js'))
@section('content')
<!-- Page Content  -->
<div class="section">
    <div class="container-fluid">
        @if($logged_user->type=='MinistryAdmin')
        <h2 class="title"><a class="no_sidebar_active" href="{{url('admin/students')}}"> <span>{{trans('sidebar.sidebar_nav_students')}} > </span></a> {{trans('general.gn_edit')}}</h2>

        @else
        <h2 class="title"><span>@if($logged_user->type != 'HertronicAdmin'){{trans('sidebar.sidebar_nav_user_management')}} > </span><a class="no_sidebar_active" href="{{url('employee/students')}}"> @else {{trans('sidebar.sidebar_nav_ministry_masters')}} > </span><a class="no_sidebar_active" href="{{url('admin/students')}}"> @endif<span>{{trans('sidebar.sidebar_nav_students')}} > </span></a> {{trans('general.gn_edit')}}</h2>
        @endif
        <div class="white_box">
            @if($logged_user->type == 'HertronicAdmin' || $logged_user->type=='MinistryAdmin')
            <input type="hidden" id="is_HSA">
            @endif
            <div class="theme_tab">
                <ul class="nav nav-tabs" id="myTab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">{{trans('general.gn_general_information')}}</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="tab3-tab" data-toggle="tab" href="#tab3" role="tab" aria-controls="tab3" aria-selected="true">{{trans('general.gn_details_enrollment')}}</a>
                    </li>
                </ul>
                <div class="tab-content" id="myTabContent">
                    <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                        <form name="add-student-form">
                            <div class="row">
                                <div class="col-lg-3"></div>
                                <div class="col-lg-6">
                                    <input type="hidden" id="image_validation_msg" value="{{trans('message.msg_image_validation')}}">
                                    <div class="text-center">
                                        <div class="profile_box">
                                            <div class="profile_pic">
                                                <img id="user_img" src="@if(!empty($aStudentData->stu_PicturePath)) {{asset('images/students')}}/{{$aStudentData->stu_PicturePath}} @else {{ asset('images/user.png') }}@endif">
                                                <input type="hidden" id="img_tmp" value="{{ asset('images/user.png') }}">
                                            </div>
                                            <div class="edit_pencile">
                                                <img src="{{asset('images/ic_pen.png')}}">
                                                <input type="file" id="upload_profile" name="stu_PicturePath" accept="image/jpeg,image/png">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="">
                                        <div class="form-group">
                                            <label>{{trans('general.gn_first_name')}} *</label>
                                            <input type="text" name="stu_StudentName" value="{{$aStudentData->stu_StudentName}}" class="form-control" placeholder="{{trans('general.gn_enter')}} {{trans('general.gn_first_name')}}">
                                            <input id="aid" type="hidden" value="{{$aStudentData->id}}">
                                        </div>
                                        <div class="form-group">
                                            <label>{{trans('general.gn_last_name')}} *</label>
                                            <input type="text" name="stu_StudentSurname" value="{{$aStudentData->stu_StudentSurname}}" class="form-control" placeholder="{{trans('general.gn_enter')}} {{trans('general.gn_last_name')}}">
                                        </div>
                                        <div class="form-group">
                                            <label>{{trans('general.gn_gender')}} *</label>
                                            <select onchange="checkStudentID()" class="form-control icon_control dropdown_control" name="stu_StudentGender" id="stu_StudentGender">
                                                <option value="">{{trans('general.gn_select')}}</option>
                                                <option @if($aStudentData->stu_StudentGender == 'Male') selected @endif value="Male">{{trans('general.gn_male')}}</option>
                                                <option @if($aStudentData->stu_StudentGender == 'Female') selected @endif value="Female">{{trans('general.gn_female')}}</option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label>{{trans('general.gn_date_Of_birth')}} *</label>
                                            <input onchange="checkStudentID()" type="text" name="stu_DateOfBirth" id="stu_DateOfBirth" value="{{date('d/m/Y',strtotime($aStudentData->stu_DateOfBirth))}}" class="form-control icon_control date_control datepicker" placeholder="{{trans('general.gn_enter')}} {{trans('general.gn_dob')}}">
                                        </div>
                                        <div class="form-group">
                                            <label>{{trans('general.gn_student_id')}} *</label>
                                            <input maxlength="13" onfocusout="checkStudentID()" @if(!empty($aStudentData->stu_TempCitizenId) && empty($aStudentData->stu_StudentID)) disabled @endif type="text" id="stu_StudentID" name="stu_StudentID" value="{{$aStudentData->stu_StudentID}}" class="form-control numbersonly isValidStuID" placeholder="{{trans('general.gn_enter')}} {{trans('general.gn_student_id')}}" >
                                        </div>
                                        <div class="form-group form-check">
                                            <input @if(empty($aStudentData->stu_StudentID)) checked @endif type="checkbox" class="form-check-input" id="havent_identification_number">
                                            <label class="custom_checkbox"></label>
                                            <label class="form-check-label label-text" for="exampleCheck1">{{trans('general.gn_havent_identification_number')}}</label>
                                        </div>
                                        <div class="form-group opt_tmp_id @if(empty($aStudentData->stu_TempCitizenId) || !empty($aStudentData->stu_StudentID) ) hide_content @endif">
                                            <label>{{trans('general.gn_temp_citizen_id')}} *</label>
                                            <input type="text" maxlength="13" onchange="checkStuId(this.value)" name="stu_TempCitizenId" value="{{$aStudentData->stu_TempCitizenId}}" class="form-control numbersonly isValidTempID" placeholder="{{trans('general.gn_enter')}} {{trans('general.gn_temp_citizen_id')}}">
                                        </div>
                                        <div class="form-group">
                                            <label>{{trans('general.gn_place_of_birth')}} *</label>
                                            <input type="text" name="stu_PlaceOfBirth" value="{{$aStudentData->stu_PlaceOfBirth}}" class="form-control" placeholder="{{trans('general.gn_enter')}} Place of birth">
                                        </div>
                                        <div class="form-group">
                                            <label>{{trans('general.gn_MunicipalityofBirth')}} *</label>
                                            <select class="form-control icon_control dropdown_control" id="fkStuMun" name="fkStuMun">
                                                <option value="">{{trans('general.gn_select')}}</option>
                                                @foreach($municipality as $value)
                                                <option data-countryId="{{$value->canton->state->country->pkCny}}" @if($aStudentData->fkStuMun == $value->pkMun) selected @endif value="{{$value->pkMun}}">{{$value->mun_MunicipalityName}}, {{$value->canton->state->country->cny_CountryName}}</option>
                                                @endforeach
                                            </select>
                                            <input type="hidden" id="fkStuCny" name="fkStuCny" value="{{$aStudentData->fkStuCny}}">
                                        </div>
                                        <div class="form-group">
                                            <label>{{trans('general.gn_nationality')}} *</label>
                                            <select class="form-control icon_control dropdown_control" name="fkStuNat">
                                                <option value="">{{trans('general.gn_select')}}</option>
                                                @foreach($nationality as $value)
                                                <option @if($aStudentData->fkStuNat == $value->pkNat) selected @endif value="{{$value->pkNat}}">{{$value->nat_NationalityName}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label>{{trans('general.gn_religions')}} *</label>
                                            <select class="form-control icon_control dropdown_control" name="fkStuRel">
                                                <option value="">{{trans('general.gn_select')}}</option>
                                                @foreach($religion as $value)
                                                <option @if($aStudentData->fkStuRel == $value->pkRel) selected @endif value="{{$value->pkRel}}">{{$value->rel_ReligionName}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label>{{trans('general.gn_address')}}</label>
                                            <input type="text" name="stu_Address" value="{{$aStudentData->stu_Address}}" class="form-control" placeholder="{{trans('general.gn_enter')}} {{trans('general.gn_address')}}">
                                        </div>

                                        <div class="form-group">
                                            <label>{{trans('general.gn_student_email')}}</label>
                                            <input type="text" name="email" class="form-control" value="{{$aStudentData->email}}" placeholder="{{trans('general.gn_enter')}} {{trans('general.gn_email')}}">
                                        </div>
                                        <div class="form-group">
                                            <label>{{trans('general.gn_distance_in_kilometers')}}</label>
                                            <input type="text" name="stu_DistanceInKilometers" value="{{$aStudentData->stu_DistanceInKilometers}}" class="form-control" placeholder="{{trans('general.gn_enter')}} Kilometers">
                                        </div>
                                        <div class="form-group">
                                            <label>{{trans('general.gn_phone')}}</label>
                                            <input type="text" name="stu_PhoneNumber" value="{{$aStudentData->stu_PhoneNumber}}" class="form-control" placeholder="{{trans('general.gn_enter')}} {{trans('general.gn_phone_number')}}">
                                        </div>
                                        <div class="form-group">
                                            <label>{{trans('general.gn_mobile_phone_number')}}</label>
                                            <input type="text" name="stu_MobilePhoneNumber" value="{{$aStudentData->stu_MobilePhoneNumber}}" class="form-control" placeholder="{{trans('general.gn_enter')}} {{trans('general.gn_phone_number')}}">
                                        </div>
                                        <div class="form-group">
                                            <label>{{trans('general.gn_ResidencePostoffice')}} *</label>
                                            <select class="form-control icon_control dropdown_control" name="fkStuPof">
                                                <option value="">{{trans('general.gn_select')}}</option>
                                                @foreach($postalCode as $value)
                                                <option @if($aStudentData->fkStuPof == $value->pkPof) selected @endif value="{{$value->pkPof}}">{{$value->pof_PostOfficeName}} {{$value->pof_PostOfficeNumber}} ({{$value->municipality->mun_MunicipalityName}}, {{$value->municipality->canton->state->country->cny_CountryName}})</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label>{{trans('general.gn_father_name')}}</label>
                                            <input type="text" name="stu_FatherName" value="{{$aStudentData->stu_FatherName}}" class="form-control" placeholder="{{trans('general.gn_enter')}} {{trans('general.gn_father_name')}}">
                                        </div>
                                        <div class="form-group">
                                            <label>{{trans('general.gn_father')}} {{trans('general.gn_adjective_name')}}</label>
                                            <input type="text" name="stu_FatherName_adjective" value="{{$aStudentData->stu_FatherName_adjective}}" class="form-control" placeholder="{{trans('general.gn_enter')}} {{trans('general.gn_father')}} {{trans('general.gn_adjective_name')}}">
                                        </div>
                                        <div class="form-group">
                                            <label>{{trans('general.gn_father')}} {{trans('general.gn_genitive_name')}}</label>
                                            <input type="text" name="stu_FatherName_genitive" value="{{$aStudentData->stu_FatherName_genitive}}" class="form-control" placeholder="{{trans('general.gn_enter')}} {{trans('general.gn_father')}} {{trans('general.gn_genitive_name')}}">
                                        </div>
                                        <div class="form-group">
                                            <label>{{trans('general.gn_mother_name')}} *</label>
                                            <input type="text" name="stu_MotherName" value="{{$aStudentData->stu_MotherName}}" class="form-control" placeholder="{{trans('general.gn_enter')}} {{trans('general.gn_mother_name')}}">
                                        </div>
                                        <div class="form-group">
                                            <label>{{trans('general.gn_mother')}} {{trans('general.gn_adjective_name')}}</label>
                                            <input type="text" name="stu_MotherName_adjective" value="{{$aStudentData->stu_MotherName_adjective}}" class="form-control" placeholder="{{trans('general.gn_enter')}} {{trans('general.gn_mother')}} {{trans('general.gn_adjective_name')}}">
                                        </div>
                                        <div class="form-group">
                                            <label>{{trans('general.gn_mother')}} {{trans('general.gn_genitive_name')}}</label>
                                            <input type="text" name="stu_MotherName_genitive" value="{{$aStudentData->stu_MotherName_genitive}}" class="form-control" placeholder="{{trans('general.gn_enter')}} {{trans('general.gn_mother')}} {{trans('general.gn_genitive_name')}}">
                                        </div>
                                        <div class="form-group">
                                            <label>{{trans('general.gn_father_job')}}</label>
                                            <select class="form-control icon_control dropdown_control" name="fkStuFatherJaw">
                                                <option value="">{{trans('general.gn_select')}}</option>
                                                @foreach($jawWork as $value)
                                                <option @if($aStudentData->fkStuFatherJaw == $value->pkJaw) selected @endif value="{{$value->pkJaw}}">{{$value->jaw_Name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label>{{trans('general.gn_mother_job')}}</label>
                                            <select class="form-control icon_control dropdown_control" name="fkStuMotherJaw">
                                                <option value="">{{trans('general.gn_select')}}</option>
                                                @foreach($jawWork as $value)
                                                <option @if($aStudentData->fkStuMotherJaw == $value->pkJaw) selected @endif value="{{$value->pkJaw}}">{{$value->jaw_Name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label>{{trans('general.gn_parent_email')}}</label>
                                            <input type="text" name="stu_ParentsEmail" value="{{$aStudentData->stu_ParentsEmail}}" class="form-control" placeholder="{{trans('general.gn_enter')}} {{trans('general.gn_email')}}">
                                        </div>
                                        <div class="form-group">
                                            <label>{{trans('general.gn_Parent_phone_no')}}</label>
                                            <input type="number" name="stu_ParantsPhone" value="{{$aStudentData->stu_ParantsPhone}}" class="form-control" placeholder="{{trans('general.gn_enter')}} {{trans('general.gn_phone_number')}}">
                                        </div>
                                        <div class="form-group">
                                            <label>{{trans('general.gn_special_needs')}}</label>
                                            <select class="form-control icon_control dropdown_control" name="stu_SpecialNeed">
                                                <option value="">{{trans('general.gn_select')}}</option>
                                                <option @if($aStudentData->stu_SpecialNeed == 'Yes') selected @endif value="Yes">{{trans('general.gn_yes')}}</option>
                                                <option @if($aStudentData->stu_SpecialNeed == 'No') selected @endif value="No">{{trans('general.gn_no')}}</option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label>{{trans('general.gn_notes')}}</label>
                                            <input type="text" name="stu_Notes" value="{{$aStudentData->stu_Notes}}" class="form-control" placeholder="{{trans('general.gn_enter')}} {{trans('general.gn_note')}}">
                                        </div>

                                    </div>
                                    <div class="text-center">
                                        <button type="submit" class="theme_btn">{{trans('general.gn_update')}}</button>
                                        <a class="theme_btn red_btn no_sidebar_active" @if($logged_user->type == 'HertronicAdmin' || $logged_user->type == 'MinistryAdmin') href="{{url('/admin/students')}}" @else href="{{url('/employee/students')}}" @endif>{{trans('general.gn_cancel')}}</a>
                                    </div>
                                </div>
                                <div class="col-lg-3"></div>
                            </div>
                        </form>
                    </div>
                    <div class="tab-pane fade show" id="tab3" role="tabpanel" aria-labelledby="tab3-tab">
                        <div class="inner_tab" id="profile_detail3">
                            <div class="row">
                                <div class="col-lg-1"></div>
                                <div class="col-lg-10">
                                    <div class="profile_info_container">
                                        <div class="row">
                                            <div class="col-12">
                                                <div class="table-responsive">
                                                    <table class="profile_table">
                                                        <tbody>
                                                            @foreach($aStudentData->enrollStudent as $k => $v)
                                                            <tr>
                                                                <td>{{$k+1}}</td>
                                                                <td>
                                                                    <p class="label">{{trans('general.gn_grade')}} :</p>
                                                                    <p class="value">{{$v->grade->gra_GradeNumeric}}</p>
                                                                    <br>
                                                                    <p class="label">{{trans('general.gn_school_year')}} :</p>
                                                                    <p class="value">{{$v->schoolYear->sye_NameCharacter}}</p>
                                                                </td>
                                                                <td>
                                                                    <p class="label">{{trans('general.gn_education_program')}} :</p>
                                                                    <p class="value">{{$v->educationProgram->edp_Name}}</p>
                                                                    <br>
                                                                    <p class="label">{{trans('general.gn_education_plan')}} :</p>
                                                                    <p class="value">{{$v->educationPlan->epl_EducationPlanName}}</p>
                                                                </td>
                                                                <td>
                                                                    @if($logged_user->type == 'HertronicAdmin' || $logged_user->type=='MinistryAdmin')
                                                                    <p class="label">{{trans('general.gn_school')}} :</p>
                                                                    <p class="value">{{$v->school->sch_SchoolName}}</p>
                                                                    <br>
                                                                    @endif
                                                                    <p class="label">{{trans('general.gn_date_of_enrollment')}} :</p>
                                                                    <p class="value">{{date('d/m/Y',strtotime($v->ste_EnrollmentDate))}}</p>
                                                                </td>
                                                                <td>
                                                                    <p class="label">{{trans('general.gn_village_school')}} :</p>
                                                                    <p class="value">{{$v->villageschool->vsc_VillageSchoolName ?? '-'}}</p>
                                                                </td>
                                                            </tr>
                                                            @endforeach
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                    @if($logged_user->type != 'HertronicAdmin' && $logged_user->type !='MinistryAdmin')
                                    <div class="text-center">
                                        <button class="theme_btn" id="edit_profile3">{{trans('general.gn_update')}}</button>
                                    </div>
                                    @endif
                                </div>
                                <div class="col-lg-1"></div>
                            </div>
                        </div>
                        <div class="inner_tab" id="edit_profile_detail3" style="display: none;">
                            <form name="enroll-student-form">
                                <div class="row">
                                    <input type="hidden" id="fkSteStu" value="{{$aStudentData->id}}">
                                    <div class="col-md-2"></div>
                                    <div class="col-md-8 profile_eng_details_add">
                                        @foreach($aStudentData->enrollStudent as $k => $v)
                                        <div class="profile_info_container">
                                            <input type="hidden" name="pkSte_{{$k+1}}" id="pkSte_{{$k+1}}" class="pkSte" value="{{$v->pkSte}}">
                                            <div class="row">
                                                <div class="col-12 text-right hide_content">
                                                    <img src="{{asset('images/ic_close_circle.png')}}" data-eng="{{$v->pkSte}}" class="rm_eng close_img" data-eed="{{$k+1}}">
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>{{trans('general.gn_date_of_enrollment')}} *</label>
                                                        <input @if(!is_null($v->ste_EnrollmentFinishDate)) readonly @endif required type="text" id="start_date_{{$k+1}}" name="start_date_{{$k+1}}" class="form-control icon_control @if(is_null($v->ste_EnrollmentFinishDate))date_control datepicker @endif" value="@if(!empty($v->ste_EnrollmentDate)){{UtilHelper::hertronicDate($v->ste_EnrollmentDate)}}@endif">
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>{{trans('general.gn_main_book_number')}} *</label>
                                                        <select @if(!is_null($v->ste_EnrollmentFinishDate)) readonly @endif required class="form-control icon_control dropdown_control @if(!is_null($v->ste_EnrollmentFinishDate)) input_disable @endif" name="fkSteMbo_{{$k+1}}" id="fkSteMbo_{{$k+1}}">
                                                            <option value="">{{trans('general.gn_select')}}</option>
                                                            @foreach($mainBooks as $mbvalue)
                                                            <option @if($v->fkSteMbo == $mbvalue->pkMbo) selected @endif value="{{$mbvalue->pkMbo }}">{{ $mbvalue->mbo_MainBookNameRoman }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>{{trans('general.gn_order_no')}} *</label>
                                                        <input required type="number" name="ste_MainBook_{{$k+1}}" id="ste_MainBook_{{$k+1}}" class="form-control" value="{{$v->ste_MainBookOrderNumber}}">
                                                    </div>
                                                </div>


                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>{{trans('general.gn_school_year')}} *</label>
                                                        <select @if(!is_null($v->ste_EnrollmentFinishDate)) readonly @endif required class="form-control icon_control dropdown_control @if(!is_null($v->ste_EnrollmentFinishDate)) input_disable @endif" name="fkSteSye_{{$k+1}}" id="fkSteSye_{{$k+1}}">
                                                            <option value="">{{trans('general.gn_select')}}</option>
                                                            @foreach($schoolYear as $yeValue)
                                                            <option @if($v->fkSteSye == $yeValue->pkSye) selected @endif value="{{$yeValue->pkSye }}">{{ $yeValue->sye_NameCharacter }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>{{trans('general.gn_education_program')}} *</label>
                                                        <select @if(!is_null($v->ste_EnrollmentFinishDate)) readonly @endif required class="form-control icon_control dropdown_control eprog @if(!is_null($v->ste_EnrollmentFinishDate)) input_disable @endif" onchange="fetchEducationPlan(this)" name="fkSteEdp_{{$k+1}}" id="fkSteEdp_{{$k+1}}">
                                                            <option value="">{{trans('general.gn_select')}}</option>
                                                            @foreach($educationProg as $mbvalue)
                                                            <option @if($v->fkSteEdp == $mbvalue->pkEdp) selected @endif value="{{$mbvalue->pkEdp }}">{{ $mbvalue->edp_Name }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                                <input type="hidden" id="msg_select_education_plan" value="{{ trans('message.msg_select_education_plan')}}">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>{{trans('general.gn_education_plan')}} *
                                                            <button type="button" class="btn-darkinfo" data-id="{{$k+1}}" onclick="viewEduPlan(this)"> i </button>
                                                        </label>
                                                        <select @if(!is_null($v->ste_EnrollmentFinishDate)) readonly @endif required class="@if(!is_null($v->ste_EnrollmentFinishDate)) input_disable @endif form-control icon_control dropdown_control eplan" onchange="fetchGrades(this)" name="fkSteEpl_{{$k+1}}" id="fkSteEpl_{{$k+1}}">
                                                            <option value="">{{trans('general.gn_select')}}</option>
                                                            @foreach($v->educationProgram->schoolEducationPlanAssignment as $mbvalue)
                                                            <option @if($v->fkSteEpl == $mbvalue->educationPlan->pkEpl) selected @endif value="{{$mbvalue->educationPlan->pkEpl }}">{{ $mbvalue->educationPlan->epl_EducationPlanName }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>{{trans('general.gn_grade')}} *</label>
                                                        <select @if(!is_null($v->ste_EnrollmentFinishDate)) readonly @endif required class="form-control icon_control dropdown_control @if(!is_null($v->ste_EnrollmentFinishDate)) input_disable @endif" name="fkSteGra_{{$k+1}}" id="fkSteGra_{{$k+1}}">
                                                            <option value="">{{trans('general.gn_select')}}</option>
                                                            @foreach($allGrades as $ka => $va)
                                                            <option @if($v->fkSteGra == $va->pkGra) selected @endif value="{{$va->pkGra}}">{{$va->gra_GradeNumeric}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>{{trans('general.gn_village_school')}}</label>
                                                        <select class="form-control icon_control dropdown_control" name="fkSteViSch_{{$k+1}}" id="fkSteViSch_{{$k+1}}">
                                                            @foreach($village_schools as $key => $value)
                                                            <option @if(!empty($v->fkSteViSch) && $v->fkSteViSch == $key) selected @endif value="{{$key}}">{{$value}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>{{trans('general.gn_enroll_based_on')}} </label>
                                                        <textarea class="form-control icon_control" name="ste_EnrollBasedOn_{{$k+1}}" id="ste_EnrollBasedOn_{{$k+1}}">{{$v->ste_EnrollBasedOn}}</textarea>

                                                    </div>
                                                </div>

                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>{{trans('general.gn_ste_Reason')}} </label>
                                                        <textarea class="form-control icon_control" name="ste_Reason_{{$k+1}}" id="ste_Reason_{{$k+1}}">{{$v->ste_Reason}}</textarea>
                                                    </div>
                                                </div>

                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>{{trans('general.gn_finishing_date')}} </label>
                                                        <input type="text" id="ste_FinishingDate_{{$k+1}}" name="ste_FinishingDate_{{$k+1}}" class="form-control icon_control datepicker" value="@if(!empty($v->ste_FinishingDate)){{date('d/m/Y',strtotime($v->ste_FinishingDate))}}@endif">
                                                    </div>
                                                </div>

                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>{{trans('general.gn_breaking_date')}} </label>
                                                        <input type="text" id="ste_BreakingDate_{{$k+1}}" name="ste_BreakingDate_{{$k+1}}" class="form-control icon_control datepicker" value="@if(!empty($v->ste_BreakingDate)){{date('d/m/Y',strtotime($v->ste_BreakingDate))}}@endif">
                                                    </div>
                                                </div>

                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>{{trans('general.gn_expelling_date')}} </label>
                                                        <input type="text" id="ste_ExpellingDate_{{$k+1}}" name="ste_ExpellingDate_{{$k+1}}" class="form-control icon_control datepicker" value="@if(!empty($v->ste_ExpellingDate)){{date('d/m/Y',strtotime($v->ste_ExpellingDate))}}@endif">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        @endforeach
                                        <div class="text-center add_eng_btn">
                                            <button class="theme_btn min_btn" id="add_eng" type="button">{{trans('general.gn_add')}}</button>
                                        </div>
                                    </div>
                                    <div class="col-md-2"></div>
                                </div>

                                <div class="col-md-12">
                                    <div class="text-center">
                                        <button type="Submit" class="theme_btn">{{trans('general.gn_submit')}}</button>
                                        <button class="theme_btn red_btn" id="cancel_edit_profile3" type="button">{{trans('general.gn_cancel')}}</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <!-- Add Enrollment element -->
        <div id="profile_eng_details" style="display: none;">
            <div class="profile_info_container new_emp_eng">
                <input type="hidden" name="pkSte" id="pkste" class="pkSte" value="new">
                <div class="row">
                    <div class="col-12 text-right">
                        <img src="{{asset('images/ic_close_circle.png')}}" class="close_img rm_eng" data-eed="">
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>{{trans('general.gn_date_of_enrollment')}} *</label>
                            <input required type="text" id="start_date" name="start_date" class="form-control icon_control date_control datepicker_norm">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>{{trans('general.gn_main_book_number')}} *</label>
                            <select required class="form-control icon_control dropdown_control" name="fkSteMbo" id="fkSteMbo">
                                <option value="">{{trans('general.gn_select')}}</option>
                                @foreach($mainBooks as $mbvalue)
                                <option value="{{$mbvalue->pkMbo }}">{{ $mbvalue->mbo_MainBookNameRoman }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>{{trans('general.gn_order_no')}} *</label>
                            <input required type="number" name="ste_MainBook" id="ste_MainBook" class="form-control">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>{{trans('general.gn_school_year')}} *</label>
                            <select required class="form-control icon_control dropdown_control" name="fkSteSye" id="fkSteSye">
                                <option value="">{{trans('general.gn_select')}}</option>
                                @foreach($schoolYear as $yeValue)
                                <option value="{{$yeValue->pkSye }}">{{ $yeValue->sye_NameCharacter }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label>{{trans('general.gn_education_program')}} *</label>
                            <select required class="form-control icon_control dropdown_control eprog" onchange="fetchEducationPlan(this)" name="fkSteEdp" id="fkSteEdp">
                                <option value="">{{trans('general.gn_select')}}</option>
                                @foreach($educationProg as $mbvalue)
                                <option value="{{$mbvalue->pkEdp }}">{{ $mbvalue->edp_Name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <input type="hidden" id="msg_select_education_plan" value="{{ trans('message.msg_select_education_plan')}}">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>{{trans('general.gn_education_plan')}} *
                                <button type="button" class="btn-darkinfo" data-id="" onclick="viewEduPlan(this)"> i </button>
                            </label>
                            <select required class="form-control icon_control dropdown_control eplan" onchange="fetchGrades(this)" name="fkSteEpl" id="fkSteEpl">
                                <option value="">{{trans('general.gn_select')}}</option>
                            </select>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label>{{trans('general.gn_grade')}} *</label>
                            <select required class="form-control icon_control dropdown_control" name="fkSteGra" id="fkSteGra">
                                <option value="">{{trans('general.gn_select')}}</option>
                            </select>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label>{{trans('general.gn_village_school')}}</label>
                            <select class="form-control icon_control dropdown_control" name="fkSteViSch" id="fkSteViSch">
                                @foreach($village_schools as $key => $value)
                                <option value="{{$key}}">{{$value}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label>{{trans('general.gn_enroll_based_on')}} </label>
                            <textarea class="form-control icon_control" name="ste_EnrollBasedOn" id="ste_EnrollBasedOn"></textarea>

                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label>{{trans('general.gn_ste_Reason')}} </label>
                            <textarea class="form-control icon_control" name="ste_Reason" id="ste_Reason"></textarea>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label>{{trans('general.gn_finishing_date')}} </label>
                            <input type="text" id="ste_FinishingDate" name="ste_FinishingDate" class="form-control icon_control datepicker">
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label>{{trans('general.gn_breaking_date')}} </label>
                            <input type="text" id="ste_BreakingDate" name="ste_BreakingDate" class="form-control icon_control datepicker">
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label>{{trans('general.gn_expelling_date')}} </label>
                            <input type="text" id="ste_ExpellingDate" name="ste_ExpellingDate" class="form-control icon_control datepicker">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<input type="hidden" id="studentmsgerror" value="{{trans('message.msg_student_id_incorrect')}}">
<script>
    @if($logged_user->type == 'HertronicAdmin' || $logged_user->type == 'MinistryAdmin')
    var listing_url = "{{route('fetch-students-lists')}}";
    var fetch_edu_url = "";
    @else
    var listing_url = "{{route('fetch-student-lists')}}";
    var fetch_edu_url = "{{route('fetch-educationplan-student')}}";
    @endif

</script>
@endsection
@push('datatable-scripts')
<!-- Include this Page JS -->
<script type="text/javascript" src="{{ asset('js/dashboard/student.js') }}"></script>
@endpush
