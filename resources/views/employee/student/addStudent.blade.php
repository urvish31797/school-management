@extends('layout.app_with_login')
@section('title', trans('sidebar.sidebar_nav_students'))
@section('script', asset('js/dashboard/student.js'))
@section('content')
<!-- Page Content  -->
<div class="section">
	<div class="container-fluid">
		<div class="row">
			<div class="col-12 mb-3">
				<h2 class="title"><span>{{trans('sidebar.sidebar_nav_user_management')}} > </span><a class="no_sidebar_active" href="{{url('/employee/students')}}"><span>{{trans('sidebar.sidebar_nav_students')}} > </span></a> {{trans('general.gn_add_new')}}</h2>
            </div>
            @if($logged_user->type == 'HertronicAdmin')
            	<input type="hidden" id="is_HSA">
        	@endif
        <div class="col-12">
            <div class="white_box pt-5 pb-5">
                <div class="container-fluid">
                    <form name="add-student-form">
	                    <div class="row">
	                        <div class="col-lg-3"></div>
	                        <div class="col-lg-6">
	                        	<input type="hidden" id="image_validation_msg" value="{{trans('message.msg_image_validation')}}">
	                            <div class="text-center">
	                                <div class="profile_box">
	                                    <div class="profile_pic">
	                       	                <img id="user_img" src="{{ asset('images/user.png') }}">
	                                        <input type="hidden" id="img_tmp" value="{{ asset('images/user.png') }}">
	                                    </div>
	                                    <div class="edit_pencile">
	                                        <img src="{{asset('images/ic_pen.png')}}">
	                                        <input type="file" id="upload_profile" name="stu_PicturePath" accept="image/jpeg,image/png">
	                                    </div>
	                                </div>
	                            </div>
		                            <div class="">
		                                <div class="form-group">
		                                    <label>{{trans('general.gn_first_name')}} *</label>
		                                    <input type="text" name="stu_StudentName" class="form-control" placeholder="{{trans('general.gn_enter')}} {{trans('general.gn_first_name')}}">
		                                </div>
		                                <div class="form-group">
		                                    <label>{{trans('general.gn_last_name')}} *</label>
		                                    <input type="text" name="stu_StudentSurname" class="form-control" placeholder="{{trans('general.gn_enter')}} {{trans('general.gn_last_name')}}">
		                                </div>
		                                <div class="form-group">
		                                    <label>{{trans('general.gn_gender')}} *</label>
		                                    <select onchange="checkStudentID()" class="form-control icon_control dropdown_control" name="stu_StudentGender" id="stu_StudentGender">
		                                        <option value="">{{trans('general.gn_select')}}</option>
		                                        <option value="Male">{{trans('general.gn_male')}}</option>
		                                        <option value="Female">{{trans('general.gn_female')}}</option>
		                                    </select>
		                                </div>
		                                <div class="form-group">
		                                    <label>{{trans('general.gn_date_Of_birth')}} *</label>
		                                    <input onchange="checkStudentID()" type="text" id="stu_DateOfBirth" name="stu_DateOfBirth" class="form-control icon_control date_control datepicker" placeholder="{{trans('general.gn_enter')}} {{trans('general.gn_dob')}}">
		                                </div>
										<div class="form-group">
		                                    <label>{{trans('general.gn_student_id')}}*</label>
		                                    <input maxlength="13" type="text" onfocusout="checkStudentID()" id="stu_StudentID" name="stu_StudentID" class="form-control numbersonly isValidStuID" placeholder="{{trans('general.gn_enter')}} {{trans('general.gn_student_id')}}">
		                                </div>
		                                <div class="form-group form-check">
		                                    <input type="checkbox" class="form-check-input" id="havent_identification_number">
		                                    <label class="custom_checkbox"></label>
		                                    <label class="form-check-label label-text" for="exampleCheck1">{{trans('general.gn_havent_identification_number')}}</label>
		                                </div>
		                                <div class="form-group opt_tmp_id">
			                                <div class="form-group opt_tmp_id hide_content">
			                                    <label>{{trans('general.gn_temp_citizen_id')}}</label>
			                                    <input maxlength="13" type="text" onchange="checkStuId(this.value)" name="stu_TempCitizenId" class="form-control numbersonly isValidTempID" placeholder="{{trans('general.gn_enter')}} {{trans('general.gn_temp_citizen_id')}}">
			                                </div>
		                                </div>
		                                <div class="form-group">
		                                    <label>{{trans('general.gn_place_of_birth')}} *</label>
		                                    <input type="text" name="stu_PlaceOfBirth" class="form-control" placeholder="{{trans('general.gn_enter')}} Place of birth">
		                                </div>
		                                <div class="form-group">
		                                    <label>{{trans('general.gn_MunicipalityofBirth')}} *</label>
		                                    <select class="form-control icon_control dropdown_control" id="fkStuMun" name="fkStuMun">
		                                        <option value="">{{trans('general.gn_select')}}</option>
		                                        @forelse($municipality as $mk => $value)
		                                        	<option data-countryId="{{$value->canton->state->country->pkCny}}" value="{{$value->pkMun}}">{{$value->mun_MunicipalityName}}, {{$value->canton->state->country->cny_CountryName}}</option>
		                                        @empty
		                                        	<option>No Data Available!</option>
		                                        @endforelse
		                                    </select>
											<input type="hidden" id="fkStuCny" name="fkStuCny">
		                                </div>
		                                <div class="form-group">
		                                    <label>{{trans('general.gn_nationality')}} *</label>
		                                    <select class="form-control icon_control dropdown_control" name="fkStuNat">
		                                        <option value="">{{trans('general.gn_select')}}</option>
		                                        @forelse($nationality as $value)
		                                        	<option value="{{$value->pkNat}}">{{$value->nat_NationalityName}}</option>
		                                        @empty
		                                        	<option>No Data Available!</option>
		                                        @endforelse
		                                    </select>
		                                </div>
		                                <div class="form-group">
		                                    <label>{{trans('general.gn_religions')}} *</label>
		                                    <select class="form-control icon_control dropdown_control" name="fkStuRel">
		                                        <option value="">{{trans('general.gn_select')}}</option>
		                                        @forelse($religion as $value)
		                                        	<option value="{{$value->pkRel}}">{{$value->rel_ReligionName}}</option>
		                                        @empty
		                                        	<option>No Data Available!</option>
		                                        @endforelse
		                                    </select>
		                                </div>
		                                <div class="form-group">
		                                    <label>{{trans('general.gn_address')}}</label>
		                                    <input type="text" name="stu_Address" class="form-control" placeholder="{{trans('general.gn_enter')}} Address">
		                                </div>
		                                <div class="form-group">
		                                    <label>{{trans('general.gn_student_email')}}</label>
		                                    <input type="text" id="email" name="email" class="form-control" placeholder="{{trans('general.gn_enter')}} {{trans('general.gn_email')}}">
		                                </div>
		                                <div class="form-group">
		                                    <label>{{trans('general.gn_distance_in_kilometers')}} </label>
		                                    <input type="text" name="stu_DistanceInKilometers" class="form-control" placeholder="{{trans('general.gn_enter')}} Kilometers">
		                                </div>
		                                <div class="form-group">
		                                    <label>{{trans('general.gn_phone')}}</label>
		                                    <input type="number" name="stu_PhoneNumber" class="form-control" placeholder="{{trans('general.gn_enter')}} {{trans('general.gn_phone_number')}}">
		                                </div>
		                                <div class="form-group">
		                                    <label>{{trans('general.gn_mobile_phone_number')}}</label>
		                                    <input type="number" name="stu_MobilePhoneNumber" class="form-control" placeholder="{{trans('general.gn_enter')}} {{trans('general.gn_phone_number')}}">
		                                </div>
		                                <div class="form-group">
		                                    <label>{{trans('general.gn_ResidencePostoffice')}} *</label>
		                                    <select class="form-control icon_control dropdown_control" name="fkStuPof">
		                                        <option value="">{{trans('general.gn_select')}}</option>
		                                        @forelse($postalCode as $value)
		                                        	<option value="{{$value->pkPof}}">{{$value->pof_PostOfficeName}} {{$value->pof_PostOfficeNumber}} ({{$value->municipality->mun_MunicipalityName}}, {{$value->municipality->canton->state->country->cny_CountryName}})</option>
		                                        @empty
		                                        	<option>No Data Available!</option>
		                                        @endforelse
		                                    </select>
		                                </div>
		                                <div class="form-group">
		                                    <label>{{trans('general.gn_father_name')}}</label>
		                                    <input type="text" name="stu_FatherName" class="form-control" placeholder="{{trans('general.gn_enter')}} {{trans('general.gn_father_name')}}">
		                                </div>
		                                <div class="form-group">
		                                    <label>{{trans('general.gn_father')}} {{trans('general.gn_adjective_name')}}</label>
		                                    <input type="text" name="stu_FatherName_adjective" class="form-control" placeholder="{{trans('general.gn_enter')}} {{trans('general.gn_father')}} {{trans('general.gn_adjective_name')}}">
		                                </div>
		                                <div class="form-group">
		                                    <label>{{trans('general.gn_father')}} {{trans('general.gn_genitive_name')}}</label>
		                                    <input type="text" name="stu_FatherName_genitive" class="form-control" placeholder="{{trans('general.gn_enter')}} {{trans('general.gn_father')}} {{trans('general.gn_genitive_name')}}">
		                                </div>
		                                <div class="form-group">
		                                    <label>{{trans('general.gn_mother_name')}} *</label>
		                                    <input type="text" name="stu_MotherName" class="form-control" placeholder="{{trans('general.gn_enter')}} {{trans('general.gn_mother_name')}}">
		                                </div>
		                                <div class="form-group">
		                                    <label>{{trans('general.gn_mother')}} {{trans('general.gn_adjective_name')}}</label>
		                                    <input type="text" name="stu_MotherName_adjective" class="form-control" placeholder="{{trans('general.gn_enter')}} {{trans('general.gn_mother')}} {{trans('general.gn_adjective_name')}}">
		                                </div>
		                                <div class="form-group">
		                                    <label>{{trans('general.gn_mother')}} {{trans('general.gn_genitive_name')}}</label>
		                                    <input type="text" name="stu_MotherName_genitive" class="form-control" placeholder="{{trans('general.gn_enter')}} {{trans('general.gn_mother')}} {{trans('general.gn_genitive_name')}}">
		                                </div>
		                                <div class="form-group">
		                                    <label>{{trans('general.gn_father_job')}} </label>
		                                    <select class="form-control icon_control dropdown_control" name="fkStuFatherJaw">
		                                        <option value="">{{trans('general.gn_select')}}</option>
		                                        @forelse($jawWork as $value)
		                                        	<option value="{{$value->pkJaw}}">{{$value->jaw_Name}}</option>
		                                        @empty
		                                        	<option>No Data Available!</option>
		                                        @endforelse
		                                    </select>
		                                </div>
		                                <div class="form-group">
		                                    <label>{{trans('general.gn_mother_job')}} </label>
		                                    <select class="form-control icon_control dropdown_control" name="fkStuMotherJaw">
		                                        <option value="">{{trans('general.gn_select')}}</option>
		                                        @forelse($jawWork as $value)
		                                        	<option value="{{$value->pkJaw}}">{{$value->jaw_Name}}</option>
		                                        @empty
		                                        	<option>No Data Available!</option>
		                                        @endforelse
		                                    </select>
		                                </div>
		                                <div class="form-group">
		                                    <label>{{trans('general.gn_parent_email')}}</label>
		                                    <input type="text" name="stu_ParentsEmail" class="form-control" placeholder="{{trans('general.gn_enter')}} {{trans('general.gn_email')}}">
		                                </div>
		                                <div class="form-group">
		                                    <label>{{trans('general.gn_Parent_phone_no')}}</label>
		                                    <input type="number" name="stu_ParantsPhone" class="form-control" placeholder="{{trans('general.gn_enter')}} {{trans('general.gn_phone')}}">
		                                </div>
		                                <div class="form-group">
		                                    <label>{{trans('general.gn_special_needs')}}</label>
		                                    <select class="form-control icon_control dropdown_control" name="stu_SpecialNeed">
		                                        <option value="Yes">{{trans('general.gn_yes')}}</option>
		                                        <option value="No">{{trans('general.gn_no')}}</option>
		                                    </select>
		                                </div>
		                                <div class="form-group">
		                                    <label>{{trans('general.gn_notes')}}</label>
		                                    <input type="text" name="stu_Notes" class="form-control" placeholder="{{trans('general.gn_enter')}} {{trans('general.gn_note')}}">
		                                </div>

		                            </div>
		                        <div class="text-center">
	                            	<button type="submit" class="theme_btn">{{trans('general.gn_submit')}}</button>
	                                <a class="theme_btn red_btn no_sidebar_active" href="{{url('/employee/students')}}">{{trans('general.gn_cancel')}}</a>
	                            </div>
	                        </div>
	                        <div class="col-lg-3"></div>
	                    </div>
                	</form>
                </div>
            </div>
        </div>
		</div>
	</div>
</div>

<input type="hidden" id="studentmsgerror" value="{{trans('message.msg_student_id_incorrect')}}">
<script>
    @if($logged_user->type=='HertronicAdmin' || $logged_user->type=='MinistryAdmin')
		var listing_url = "{{route('fetch-students-lists')}}";
		var fetch_edu_url = "";
    @else
		var listing_url = "{{route('fetch-student-lists')}}";
		var fetch_edu_url = "{{route('fetch-educationplan-lists')}}";
    @endif
</script>
@endsection
@push('datatable-scripts')
<!-- Include this Page JS -->
<script type="text/javascript" src="{{ asset('js/dashboard/student.js') }}"></script>
@endpush