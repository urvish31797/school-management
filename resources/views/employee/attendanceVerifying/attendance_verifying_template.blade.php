@section('attendance_verify')
<span onclick="this.parentElement.style.display='none'" class="closebtn">&times;</span>
<h2>Lecture {{$basic_details['lecturenumber']}}</h2>

<div class="col-12">
    <div class="container-fluid">
        <form name="edit-attend-accom-form" id="edit-attend-accom-form">
            <div class="row">
            <div class="col-md-12">
                <div>
                    <p class="mt-2"><strong>{{trans('general.gn_view_details')}}</strong></p>
                </div>
                <div class="card-grey">
                @include('employee.attendanceAccomplishment.components.view_basic_detail_section')
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="form-group">
                        <label>{{trans('sidebar.sidebar_nav_classes')}}</label>
                        <div class="theme_table">
                            <div class="table-responsive slim_scroll">
                            <table id="grade_listing" class="display" style="width:100%">
                                <thead>
                                    <tr>
                                    <th>{{trans('general.gn_grade')}}</th>
                                    <th>{{trans('general.gn_course')}}</th>
                                    <th>{{trans('general.gn_hour_order_number')}}</th>
                                    <th>{{trans('general.gn_unit_title_no')}}</th>
                                    <th>{{trans('general.gn_course_content')}}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($lecture_details as $key => $val)
                                    <tr>
                                    <td>{{$val['grade']}}{{$val['class']}}</td>
                                    <td>{{$val['course']}}</td>
                                    <td>{{$val['sya_LectureOrderNumber']}}</td>
                                    <td>{{$val['sya_CourseUnitNumber']}}</td>
                                    <td>{{$val['sya_CourseContent']}}</td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            </div>
                        </div>
                        </div>
                    </div>

                    <div class="col-lg-12">
                        <div class="form-group">
                        <label>{{trans('sidebar.sidebar_nav_students')}}</label>
                        <div class="theme_table">
                            <div class="table-responsive slim_scroll">
                                <table id="lecture_detail_listing" class="display" style="width:100%">
                                    <thead>
                                    <tr>
                                        <th>{{trans('general.sr_no')}}.</th>
                                        <th>{{trans('general.gn_student_name')}}</th>
                                        <th>{{trans('general.gn_class')}}</th>
                                        <th>{{trans('general.gn_status')}}</th>
                                        <th>{{trans('general.gn_lecture_status')}}</th>
                                        <th>{{trans('general.gn_teacher_notes')}}</th>
                                        <th>{{trans('general.gn_homeroomteacher_notes')}}</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @php $count = 1 @endphp
                                    @foreach($attendance_details as $key => $val)
                                    @foreach ($val as $k => $v)
                                    <tr>
                                        <td>{{$count++}}</td>
                                        <td>{{$v['studentname']}}</td>
                                        <td>{{$v['grade']}}{{$v['class']}}</td>
                                        <td>{{$v['attendancestatus']}}</td>
                                        <td class="radio_status">
                                            <input type="radio" name="attendanceLecturestatus[{{$v['pkSae']}}]" @if($v['attendanceLecturestatus'] == "Justified") checked @endif value="Justified"> {{trans('general.gn_justified')}} &nbsp;&nbsp;
                                            <input type="radio" name="attendanceLecturestatus[{{$v['pkSae']}}]" @if($v['attendanceLecturestatus'] == "Unjustified") checked @endif value="Unjustified"> {{trans('general.gn_unjustified')}}
                                        </td>
                                        <td><p title="{{$v['tdesc']}}">{{substr($v['tdesc'],0,20)}}</p></td>
                                        <td>
                                            <input type="text" class="form-control icon_control" name="sae_StudentHTDesc[{{$v['pkSae']}}]" value="{{$v['hdesc']}}" >
                                        </td>
                                        
                                    </tr>
                                    @endforeach
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        </div>
                    </div>

                    <div class="col-lg-12">
                        <div class="form-group">
                        <label>{{trans('general.gn_note')}}</label>
                        <input type="text" name="dbl_Notes" id="dbl_Notes" class="form-control icon_control" disabled value="{{$basic_details['note']}}">
                        </div>
                    </div>
                </div>
                <div class="text-center">
                    <button type="button" onclick="save()" class="theme_btn">{{trans('general.gn_submit')}}</button>
                    <a class="theme_btn red_btn" href="#" onclick="confirmDeleteLecture({{$id}})">{{trans('general.gn_delete')}}</a>
                </div>
            </div>
            </div>
        </form>
    </div>
</div>
@endsection