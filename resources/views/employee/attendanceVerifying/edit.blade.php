@extends('layout.app_with_login')
@section('title', trans('sidebar.sidebar_attendance_verifying'))
@push('custom-styles')
<link rel="stylesheet" href="{{ asset('css/attendance_verifying.css') }}">
@endpush
@section('content')
<div class="section">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12 mb-3">
                <h2 class="title"><span>{{trans('sidebar.sidebar_attendance_verifying')}} ></span> {{trans('general.gn_edit')}}</h2>
            </div>
            <div class="col-12">
                <div class="white_box pt-3 pb-3">
                    <div class="container-fluid">
                        <form name="edit-attendance-verifying-form" id="edit-attendance-verifying-form">
                            <input type="hidden" value="{{$id}}" name="dailybook_id">
                            <div class="row">
                                <div class="col-lg-12">
                                    <p class="mt-2"><strong>{{trans('sidebar.sidebar_attendance_verifying')}}</strong></p>
                                    <div class="card-grey">                                            
                                        <!-- lecture box -->
                                        <div class="lecture_colum_box">
                                            <div class="row">
                                                @if($count > 0)
                                                    @foreach($dailyBookLecture as $k => $v)
                                                    
                                                    <div class="lecture_column">
                                                    <a href="#" onclick="getclass_detail({{$v->pkDbl}});">
                                                        <h3>Lecture {{$v->dbl_LectureNumber}}</h3>
                                                        <p>{{trans('general.gn_note')}} : {{$v->dbl_Notes}}</p>
                                                        
                                                        <!-- <a href="#" onclick="confirmDeleteLecture({{$v->pkDbl}})"><i class="fas fa-trash right-arrow"></i></a> -->
                                                        </a>
                                                    </div>
                                                    
                                                    @endforeach
                                                @else
                                                <center><p>{{trans('message.msg_no_lecturers_found')}}</p></center>
                                                @endif
                                            </div>
                                            <div class="lecture_tab" style="display:none">
                                                
                                            </div>
                                        </div>
                                        <!-- end lecture box -->
                                    </div>                                    
                                </div>    
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="theme_modal modal fade" id="delete_prompt" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <img alt="" src="{{asset('images/ic_close_bg.png')}}" class="modal_top_bg">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <img alt="" src="{{asset('images/ic_close_circle_white.png')}}">
                    </button>
                    <div class="row">
                        <div class="col-lg-1"></div>
                        <div class="col-lg-10">
                            <h5 class="modal-title" id="exampleModalCenterTitle">{{trans('general.gn_delete')}}</h5>
                            <div class="form-group text-center">
                                <label>{{trans('general.gn_delete_prompt')}}</label>
                            </div>
                            <div class="text-center modal_btn">
                                <button style="display: none;" class="theme_btn show_delete_modal full_width small_btn" data-toggle="modal" data-target="#delete_prompt">{{trans('general.gn_delete')}}</button>
                                <button type="button" data-toggle="modal" data-target="#confirm_delete_prompt" class="theme_btn">{{trans('general.gn_yes')}}</button>
                                <button type="button" data-dismiss="modal" class="theme_btn red_btn">{{trans('general.gn_no')}}</button>
                            </div>
                        </div>
                        <div class="col-lg-1"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="theme_modal modal fade" id="confirm_delete_prompt" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <img alt="" src="{{asset('images/ic_close_bg.png')}}" class="modal_top_bg">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <img alt="" src="{{asset('images/ic_close_circle_white.png')}}">
                    </button>
                    <div class="row">
                        <div class="col-lg-1"></div>
                        <div class="col-lg-10">
                            <h5 class="modal-title" id="exampleModalCenterTitle">{{trans('general.gn_delete')}}</h5>
                            <div class="form-group text-center">
                                <label>{{trans('general.gn_confirm_delete_prompt')}}</label>
                                <input type="hidden" id="lecture_did">
                            </div>
                            <div class="text-center modal_btn">
                                <button type="button" onclick="confirmDeleteLectureDetail()" class="theme_btn">{{trans('general.gn_yes')}}</button>
                                <button type="button" data-dismiss="modal" class="theme_btn red_btn">{{trans('general.gn_no')}}</button>
                            </div>
                        </div>
                        <div class="col-lg-1"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

<input type="hidden" id="student_attendance_status_msg" value="{{trans('message.msg_please_select_student_attendance_status')}}">
@endsection
@push('datatable-scripts')
<!-- Include datatable Page JS -->
<script type="text/javascript" id="datatable_script" src="{{ asset('js/dashboard/attendance_verifying.js') }}"></script>
@endpush