@extends('layout.app_with_login')
@section('title', trans('general.gn_final_marks'))
@section('script', asset('js/dashboard/final_mark.js'))
@section('content')
<?php
   $unjustifiedAbs = '';
   $fkSemGan = '';
   if(!empty($mdata->classCreationSemester[0]->classStudentsSemester[0]->sem_UnjustifiedAbsenceHours)){
       $unjustifiedAbs = $mdata->classCreationSemester[0]->classStudentsSemester[0]->sem_UnjustifiedAbsenceHours;
   }
   if(!empty($mdata->classCreationSemester[0]->classStudentsSemester[0]->sem_JustifiedAbsenceHours)){
       $justifiedAbs = $mdata->classCreationSemester[0]->classStudentsSemester[0]->sem_JustifiedAbsenceHours;
   }
   ?>
<!-- Page Content  -->
<div class="section">
<div class="container-fluid">
   <div class="row">
      <div class="col-12 mb-3">
         <h2 class="title"><span>{{trans('sidebar.sidebar_nav_organization')}} > </span><a class="no_sidebar_active" href="{{url('/employee/classcreation')}}"><span>{{trans('sidebar.sidebar_nav_class_creation')}} > </span></a><a class="no_sidebar_active" href="{{url('/employee/class-creation-semester')}}/{{$mdata->classCreationSemester[0]->fkCcsClr}}"><span>{{trans('general.gn_semester')}} > </span></a>{{trans('general.gn_final_marks')}}</h2>
      </div>
      <a class="hide_content final_mark_redirect no_sidebar_active" href="{{url('/employee/finalmarks')}}/{{$mdata->classCreationSemester[0]->classStudentsSemester[0]->fkSemCcs}}">Final Mark Redirect</a>
      <input type="hidden" id="teacher_txt" value="{{trans('general.gn_teacher')}}">
      <input type="hidden" id="principal_txt" value="{{trans('general.gn_principal')}}">
      <input type="hidden" id="stu_sel_valid_txt" value="{{trans('message.msg_student_select_valid')}}">
      <input type="hidden" id="stu_sel_txt" value="{{trans('message.msg_sel_stu')}}">
      <div class="col-12">
         <div class="white_box pt-5 pb-5">
            <div class="container-fluid">
               <div class="row">
                  <div class="col-lg-10 offset-lg-1">
                     <form name="final_mark_form">
                        <input type="hidden" id="pkSem" value="{{$mdata->classCreationSemester[0]->classStudentsSemester[0]->pkSem}}">
                        <div class="row">
                           <div class="col-md-12">
                              <div class="card-grey">
                                 <div class="row">
                                    <div class="col-md-6 col-lg-3">
                                       <div class="form-group">
                                          <p class="label">{{trans('general.gn_school_year')}} :</p>
                                          <p class="value">{{$mdata->classCreationSchoolYear->sye_NameCharacter}}</p>
                                       </div>
                                    </div>
                                    <div class="col-md-6 col-lg-3">
                                       <div class="form-group">
                                          <p class="label">{{trans('general.gn_student_name')}} :</p>
                                          <p class="value">{{$mdata->classCreationSemester[0]->classStudentsSemester[0]->studentEnroll->student->full_name}}</p>
                                       </div>
                                    </div>
                                    <div class="col-md-6 col-lg-3">
                                       <div class="form-group">
                                          <p class="label">{{trans('general.gn_homeroom_teacher')}} :</p>
                                          <p class="value">@if(isset($mdata->classCreationSemester[0]->homeRoomTeacher[0])){{$mdata->classCreationSemester[0]->homeRoomTeacher[0]->employeesEngagement->employee->emp_EmployeeName}} {{$mdata->classCreationSemester[0]->homeRoomTeacher[0]->employeesEngagement->employee->emp_EmployeeSurname}} @endif</p>
                                       </div>
                                    </div>
                                    <div class="col-md-6 col-lg-3">
                                       <div class="form-group">
                                          <p class="label">{{trans('general.gn_semester')}} :</p>
                                          <p class="value">{{$mdata->classCreationSemester[0]->semester->edp_EducationPeriodName}}</p>
                                       </div>
                                    </div>
                                    <div class="col-md-6 col-lg-3">
                                       <div class="form-group">
                                          <p class="label">{{trans('general.gn_school_grade')}} :</p>
                                          <p class="value">{{$mdata->classCreationSemester[0]->classStudentsSemester[0]->studentEnroll->grade->gra_GradeNumeric}}</p>
                                       </div>
                                    </div>
                                    <div class="col-md-6 col-lg-3">
                                       <div class="form-group">
                                          <p class="label">{{trans('general.gn_class')}} :</p>
                                          <p class="value">{{$mdata->classCreationClasses->cla_ClassName}}</p>
                                       </div>
                                    </div>
                                    <div class="col-md-6 col-lg-3">
                                       <div class="form-group">
                                          <p class="label">{{trans('general.gn_education_plans_n_programs')}} :</p>
                                          <p class="value">{{$mdata->classCreationSemester[0]->classStudentsSemester[0]->studentEnroll->educationProgram->edp_Name}} - {{$mdata->classCreationSemester[0]->classStudentsSemester[0]->studentEnroll->educationPlan->epl_EducationPlanName}}</p>
                                       </div>
                                    </div>
                                    <div class="col-md-6 col-lg-3">
                                       <div class="form-group">
                                          <p class="label">{{trans('general.gn_status')}} :</p>
                                          <p class="value">@if($mdata->clr_Status == 'Pending')<span class="custom_badge badge badge-warning">{{trans('general.gn_pending')}}</span>@else<span class="custom_badge badge badge-success">{{trans('general.gn_publish')}}</span>@endif</p>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <p class="mt-3"><strong>{{trans('general.gn_details')}}</strong></p>
                              <div class="row">
                                 <div class="col-lg-6">
                                    <div class="row">
                                       <div class="col-md-12">
                                          <div class="form-group">
                                             <label>{{trans('general.gn_certifcate_number')}} *</label>
                                             @if(!empty($studentCertificates))
                                             <input type="hidden" name="pkScr" id="pkScr" value="{{$studentCertificates->pkScr}}">
                                             @endif
                                             <input type="text" id="scr_CertificateNo" name="scr_CertificateNo" class="form-control" @if(!empty($studentCertificates)) readonly="readonly" value="{{$studentCertificates->scr_CertificateNo}}" @endif>
                                          </div>
                                       </div>
                                       <div class="col-md-12">
                                          <div class="form-group">
                                             <label>{{trans('general.gn_certificate_issue_date')}} *</label>
                                             <input type="text" id="scr_Date" name="scr_Date"  @if(!empty($studentCertificates)) readonly="readonly" class="form-control"
                                             value="{{date('d/m/Y',strtotime($studentCertificates->scr_Date))}}"
                                             @else class="form-control datepicker" @endif>
                                          </div>
                                       </div>
                                       <div class="col-md-12">
                                          <div class="form-group">
                                             <label>{{trans('general.gn_certificate_type')}} *</label>
                                             <select id="fkScrCty" name="fkScrCty" class="form-control icon_control dropdown_control" @if(!empty($studentCertificates)) disabled="disabled" @endif>
                                             <option value="">{{trans('general.gn_select')}}</option>
                                             @foreach($certificateType as $k => $v)
                                             <option @if($v->pkCty == 1) selected @endif value="{{$v->pkCty}}">{{$v->cty_Name}}</option>
                                             @endforeach
                                             </select>
                                          </div>
                                       </div>
                                       <div class="col-md-12">
                                          <div class="form-group">
                                             <label>{{trans('general.gn_unjustified_absence_hours')}} *</label>
                                             <input type="number" min="1" id="sem_UnjustifiedAbsenceHours" name="sem_UnjustifiedAbsenceHours" class="form-control" value="{{$mdata->classCreationSemester[0]->classStudentsSemester[0]->sem_UnjustifiedAbsenceHours}}">
                                          </div>
                                       </div>
                                       <div class="col-md-12">
                                          <div class="form-group">
                                             <label>{{trans('general.gn_justified_absence_hours')}} *</label>
                                             <input type="number" min="1" id="sem_JustifiedAbsenceHours" name="sem_JustifiedAbsenceHours" class="form-control" value="{{$mdata->classCreationSemester[0]->classStudentsSemester[0]->sem_JustifiedAbsenceHours}}">
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="col-lg-6">
                                    <div class="row">
                                       <div class="col-md-12">
                                          <div class="form-group">
                                             <label>{{trans('general.gn_gan')}} *</label>
                                             <select id="gan" name="gan" class="form-control icon_control dropdown_control" >
                                                <option value="">{{trans('general.gn_select')}}</option>
                                                @foreach($GAN as $k => $v)
                                                <option @if($mdata->classCreationSemester[0]->classStudentsSemester[0]->fkSemGan == $v->pkGan) selected @endif value="{{$v->pkGan}}">{{$v->gan_Name}}</option>
                                                @endforeach
                                             </select>
                                          </div>
                                       </div>
                                       @if($showDFM)
                                       <div class="col-md-12 @if(empty($mdata->classCreationSemester[0]->classStudentsSemester[0]->fkCcaDfm)) hide_content @endif" id="drp_dfm">
                                          <div class="form-group">
                                             <label>{{trans('general.gn_general_success_dfm')}} </label>
                                             <select id="fkCcaDfm" name="fkCcaDfm" class="form-control icon_control dropdown_control" >
                                                <option value="">{{trans('general.gn_select')}}</option>
                                                @foreach($DFM as $k => $v)
                                                @if($v->dfm_Text_en != 'Done' && $v->dfm_Text_en != 'Undone')
                                                <option @if($mdata->classCreationSemester[0]->classStudentsSemester[0]->fkCcaDfm == $v->pkDfm) selected @endif value="{{$v->pkDfm}}">{{$v->dfm_Text}}</option>
                                                @endif
                                                @endforeach
                                             </select>
                                          </div>
                                       </div>
                                       <div class="col-md-12">
                                          <div class="form-group form-check">
                                             <input @if(!empty($mdata->classCreationSemester[0]->classStudentsSemester[0]->sem_generalSuccessDescriptiveFinalmarks)) checked="checked" @endif name="csa_ManualDescriptMarks_checkbox" type="checkbox" class="form-check-input" id="manual_dfm_check">
                                             <label class="custom_checkbox"></label>
                                             <label class="form-check-label label-text" for="exampleCheck1">{{trans('general.gn_manual_add_general_success_dfm')}}</label>
                                          </div>
                                       </div>
                                       @endif
                                       <div class="col-md-12 @if(empty($mdata->classCreationSemester[0]->classStudentsSemester[0]->sem_generalSuccessDescriptiveFinalmarks)) hide_content @endif" id="manual_dfm">
                                          <div class="form-group">
                                             <div class="form-group">
                                                <label>{{trans('general.gn_general_success_dfm')}} </label>
                                                <input type="text" id="sem_generalSuccessDescriptiveFinalmarks" name="sem_generalSuccessDescriptiveFinalmarks" class="form-control" @if(!empty($mdata->classCreationSemester[0]->classStudentsSemester[0]->sem_generalSuccessDescriptiveFinalmarks)) value="{{$mdata->classCreationSemester[0]->classStudentsSemester[0]->sem_generalSuccessDescriptiveFinalmarks}}" @endif>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="col-md-12">
                                          <div class="form-group">
                                             <label>{{trans('general.gn_student_behaviour')}} *</label>
                                             <input type="hidden" name="pkStb" @if(!empty($studentBehaviour->pkStb)) value="{{$studentBehaviour->pkStb}}" @endif>
                                             <select id="fkStbSbe" name="fkStbSbe" class="form-control icon_control dropdown_control" >
                                                <option value="">{{trans('general.gn_select')}}</option>
                                                @foreach($SDT as $k => $v)
                                                <option @if(!empty($studentBehaviour) && $studentBehaviour->fkStbSbe == $v->pkSbe) selected @endif value="{{$v->pkSbe}}">{{$v->sbe_BehaviourName}}</option>
                                                @endforeach
                                             </select>
                                          </div>
                                       </div>
                                       <div class="col-md-12">
                                          <div class="form-group">
                                             <label>{{trans('general.student_explanation')}}</label>
                                             <input type="text" id="stb_Explanation" name="stb_Explanation" class="form-control" @if(!empty($studentBehaviour)) value="{{$studentBehaviour->stb_Explanation}}" @endif>
                                          </div>
                                       </div>
                                       <div class="col-md-12">
                                          <div class="form-group">
                                             <label>{{trans('general.gn_average_success_marks')}}</label>
                                             <input type="number" id="sem_AvgGeneralSuccess" name="sem_AvgGeneralSuccess" class="form-control" readonly @if($showDFM) value="0" @else value="{{$mdata->classCreationSemester[0]->classStudentsSemester[0]->sem_AvgGeneralSuccess}}" @endif>
                                          </div>
                                       </div>
                                       <div class="col-md-12">
                                          <div class="form-group">
                                             <label>{{trans('general.gn_general_success_marks')}}</label>
                                             <input type="number" id="sem_GeneralSuccess" name="sem_GeneralSuccess" class="form-control" readonly @if($showDFM) value="0" @else value="{{$mdata->classCreationSemester[0]->classStudentsSemester[0]->sem_GeneralSuccess}}" @endif>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <p class="mt-3"><strong>{{trans('general.gn_mandatory_courses')}}</strong></p>
                              <div class="table-responsive mt-2 step_2_resp">
                                 <table class="color_table class_seleted_students sel_stu_elem ">
                                    <tbody>
                                       <tr>
                                          <th>#</th>
                                          <th>{{trans('general.gn_courses')}}</th>
                                          <th>{{trans('general.gn_final_marks')}}</th>
                                          <th>{{trans('general.gn_dfm')}}</th>
                                       </tr>
                                       <?php $m = 1;?>

                                       @foreach($courses as $k => $v)
                                       <tr class="mcg_item mcg">
                                          <td id="{{$v['pkCsa']}}">{{$m}}</td>
                                          <td>{{$v['crs_CourseName']}}</td>
                                          @if($v['crs_CourseAlternativeName'] == 'Stručna praksa')
                                          <td>
                                             <div class="col-md-12">
                                                <div class="form-group">
                                                   -
                                                   <input type="hidden" name="pkCsa[]" value="{{$v['pkCsa']}}">
                                                   <input type="hidden" name="pkCrs[]" value="{{$v['pkCrs']}}">
                                                </div>
                                             </div>
                                          </td>
                                          <td>
                                             <div class="col-md-12">
                                                <div class="form-group">
                                                   <select required id="fkCsaDfm_{{$v['pkCsa']}}" name="fkCsaDfm_{{$v['pkCsa']}}" class="form-control icon_control dropdown_control" >
                                                      <option value="">{{trans('general.gn_select')}}</option>
                                                      @foreach($DFM as $k => $value)
                                                      @if($value->dfm_Text_en == 'Done' || $value->dfm_Text_en == 'Undone')
                                                      <option @if($v['fkCsaDfm'] == $value->pkDfm) selected="selected" @endif value="{{$value->pkDfm}}">{{$value->dfm_Text}}</option>
                                                      @endif
                                                      @endforeach
                                                   </select>
                                                </div>
                                             </div>
                                          </td>
                                          @else
                                          @if(!$showDFM)
                                          <td>
                                             <div class="col-md-12">
                                                <div class="form-group">
                                                   <input type="hidden" name="pkCsa[]" value="{{$v['pkCsa']}}">
                                                   <input type="hidden" name="pkCrs[]" value="{{$v['pkCrs']}}">
                                                   <select required id="csa_FinalMarks_{{$v['pkCsa']}}" name="csa_FinalMarks_{{$v['pkCsa']}}" class="form-control icon_control dropdown_control cal_avg">
                                                      <option value="">{{trans('general.gn_select')}}</option>
                                                      @foreach($finalMarksScore as $kf => $vf)
                                                      <option @if($v['final_mark'] == $vf) selected @endif value="{{$vf}}">{{$vf}}</option>
                                                      @endforeach
                                                   </select>
                                                </div>
                                             </div>
                                          </td>
                                          <td>
                                             <div class="col-md-12">
                                                <div class="form-group">
                                                   -
                                                </div>
                                             </div>
                                          </td>
                                          @else
                                          <td>
                                             <div class="col-md-12">
                                                <div class="form-group">
                                                   -
                                                   <input type="hidden" name="pkCsa[]" value="{{$v['pkCsa']}}">
                                                   <input type="hidden" name="pkCrs[]" value="{{$v['pkCrs']}}">
                                                </div>
                                             </div>
                                          </td>
                                          <td>
                                             <div class="col-md-12 @if(!empty($v['dfm_manual'])) hide_content @endif" id="dropdown_dfm_{{$v['pkCsa']}}">
                                                <div class="form-group">
                                                   <select required id="fkCsaDfm_{{$v['pkCsa']}}" name="fkCsaDfm_{{$v['pkCsa']}}" class="form-control icon_control dropdown_control valid" >
                                                      <option value="">{{trans('general.gn_select')}}</option>
                                                      @foreach($DFM as $k => $val)
                                                      @if($val->dfm_Text_en != 'Done' && $val->dfm_Text_en != 'Undone')
                                                      <option @if($v['fkCsaDfm'] == $val->pkDfm) selected="selected" @endif value="{{$val->pkDfm}}">{{$val->dfm_Text}}</option>
                                                      @endif
                                                      @endforeach
                                                   </select>
                                                </div>
                                             </div>
                                             <div class="col-md-12">
                                                <div class="form-group form-check">
                                                   <input @if(!empty($v['dfm_manual'])) checked="checked" @endif name="course_manual_dfm_check_checkbox_{{$v['pkCsa']}}" type="checkbox" data-dfm="{{$v['pkCsa']}}" class="form-check-input dfm_check" id="manual_dfm_check_{{$v['pkCsa']}}">
                                                   <label class="custom_checkbox"></label>
                                                   <label class="form-check-label label-text" for="exampleCheck1">{{trans('general.gn_manual_dfm')}}</label>
                                                </div>
                                             </div>
                                             <div class="col-md-12 @if(empty($v['dfm_manual'])) hide_content @endif " id="manual_dfm_{{$v['pkCsa']}}">
                                                <div class="form-group">
                                                   <div class="form-group">
                                                      <label>{{trans('general.gn_dfm')}} *</label>
                                                      <input required type="text" id="csa_ManualDescriptMarks_{{$v['pkCsa']}}" name="csa_ManualDescriptMarks_{{$v['pkCsa']}}" class="form-control"  value="{{$v['dfm_manual']}}">
                                                   </div>
                                                </div>
                                             </div>
                                          </td>
                                          @endif
                                          @endif
                                       </tr>
                                       <?php $m++; ?>
                                       @endforeach
                                    </tbody>
                                 </table>
                              </div>
                              @if(!empty($OCGCrs))
                              <p class="mt-3"><strong>{{trans('general.gn_optional_courses')}}</strong></p>
                              <div class="table-responsive mt-2 step_2_resp">
                                 <table class="color_table class_seleted_students sel_stu_elem ">
                                    <tbody>
                                       <tr>
                                          <th>#</th>
                                          <th>{{trans('general.gn_courses')}}</th>
                                          <th>{{trans('general.gn_final_marks')}}</th>
                                          <th>{{trans('general.gn_dfm')}}</th>
                                       </tr>
                                       <?php $o = 1;?>
                                       @foreach($OCGCrs as $k => $v)
                                       <tr class="ocg_item ocg">
                                          <td id="{{$v['pkCsa']}}">{{$o}}</td>
                                          <td>{{$v['crs_CourseName']}}</td>
                                          <td>
                                             @if($showDFM)
                                             <div class="col-md-12">
                                                <div class="form-group">
                                                   -
                                                </div>
                                             </div>
                                             <input type="hidden" name="pkCsa[]" value="{{$v['pkCsa']}}">
                                             <input type="hidden" name="pkCrs[]" value="{{$v['pkCrs']}}">
                                             @else
                                             <div class="col-md-12">
                                                <div class="form-group">
                                                   <input type="hidden" name="pkCsa[]" value="{{$v['pkCsa']}}">
                                                   <input type="hidden" name="pkCrs[]" value="{{$v['pkCrs']}}">
                                                   <select required id="csa_FinalMarks_{{$v['pkCsa']}}" name="csa_FinalMarks_{{$v['pkCsa']}}" class="form-control icon_control dropdown_control cal_avg">
                                                      <option value="">{{trans('general.gn_select')}}</option>
                                                      @foreach($finalMarksScore as $kf => $vf)
                                                      <option @if($v['final_mark'] == $vf) selected @endif value="{{$vf}}">{{$vf}}</option>
                                                      @endforeach
                                                   </select>
                                                </div>
                                             </div>
                                             @endif
                                          </td>
                                          <td>
                                             @if($showDFM)
                                             <div class="col-md-12 @if(!empty($v['dfm_manual'])) hide_content @endif" id="dropdown_dfm_{{$v['pkCsa']}}">
                                                <div class="form-group">
                                                   <select required id="fkCsaDfm_{{$v['pkCsa']}}" name="fkCsaDfm_{{$v['pkCsa']}}" class="form-control icon_control dropdown_control valid" >
                                                      <option value="">{{trans('general.gn_select')}}</option>
                                                      @foreach($DFM as $k => $val)
                                                      @if($val->dfm_Text_en != 'Done' && $val->dfm_Text_en != 'Undone')
                                                      <option @if($v['fkCsaDfm'] == $val->pkDfm) selected="selected" @endif value="{{$val->pkDfm}}">{{$val->dfm_Text}}</option>
                                                      @endif
                                                      @endforeach
                                                   </select>
                                                </div>
                                             </div>
                                             <div class="col-md-12">
                                                <div class="form-group form-check">
                                                   <input @if(!empty($v['dfm_manual'])) checked="checked" @endif name="course_manual_dfm_check_checkbox_{{$v['pkCsa']}}" type="checkbox" data-dfm="{{$v['pkCsa']}}" class="form-check-input dfm_check" id="manual_dfm_check_{{$v['pkCsa']}}">
                                                   <label class="custom_checkbox"></label>
                                                   <label class="form-check-label label-text" for="exampleCheck1">{{trans('general.gn_manual_dfm')}}</label>
                                                </div>
                                             </div>
                                             <div class="col-md-12 @if(empty($v['dfm_manual'])) hide_content @endif" id="manual_dfm_{{$v['pkCsa']}}">
                                                <div class="form-group">
                                                   <div class="form-group">
                                                      <label>{{trans('general.gn_dfm')}} *</label>
                                                      <input required type="text" id="csa_ManualDescriptMarks_{{$v['pkCsa']}}" name="csa_ManualDescriptMarks_{{$v['pkCsa']}}" class="form-control" value="{{$v['dfm_manual']}}">
                                                   </div>
                                                </div>
                                             </div>
                                             @else
                                             <div class="col-md-12">
                                                <div class="form-group">
                                                   -
                                                </div>
                                             </div>
                                             @endif
                                          </td>
                                       </tr>
                                       <?php $o++; ?>
                                       @endforeach
                                    </tbody>
                                 </table>
                              </div>
                              @endif
                              @if(!empty($FCGCrs))
                              <p class="mt-3"><strong>{{trans('general.gn_facultative_courses')}}</strong></p>
                              <div class="table-responsive mt-2 step_2_resp">
                                 <table class="color_table class_seleted_students sel_stu_elem ">
                                    <tbody>
                                       <tr>
                                          <th>#</th>
                                          <th>{{trans('general.gn_courses')}}</th>
                                          <th>{{trans('general.gn_final_marks')}}</th>
                                          <th>{{trans('general.gn_dfm')}}</th>
                                       </tr>
                                       <?php $fc = 1;?>
                                       @foreach($FCGCrs as $k => $v)
                                       <tr class="fcg_item fcg">
                                          <td id="{{$v['pkCsa']}}">{{$fc}}</td>
                                          <td>{{$v['crs_CourseName']}}</td>
                                          <td>
                                             @if($showDFM)
                                             <div class="col-md-12">
                                                <div class="form-group">
                                                   -
                                                </div>
                                             </div>
                                             <input type="hidden" name="pkCsa[]" value="{{$v['pkCsa']}}">
                                             <input type="hidden" name="pkCrs[]" value="{{$v['pkCrs']}}">
                                             @else
                                             <div class="col-md-12">
                                                <div class="form-group">
                                                   <input type="hidden" name="pkCsa[]" value="{{$v['pkCsa']}}">
                                                   <input type="hidden" name="pkCrs[]" value="{{$v['pkCrs']}}">
                                                   <select required id="csa_FinalMarks_{{$v['pkCsa']}}" name="csa_FinalMarks_{{$v['pkCsa']}}" class="form-control icon_control dropdown_control cal_avg">
                                                      <option value="">{{trans('general.gn_select')}}</option>
                                                      @foreach($finalMarksScore as $kf => $vf)
                                                      <option @if($v['final_mark'] == $vf) selected @endif value="{{$vf}}">{{$vf}}</option>
                                                      @endforeach
                                                   </select>
                                                </div>
                                             </div>
                                             @endif
                                          </td>
                                          <td>
                                             @if($showDFM)
                                             <div class="col-md-12 @if(!empty($v['dfm_manual'])) hide_content @endif" id="dropdown_dfm_{{$v['pkCsa']}}">
                                                <div class="form-group">
                                                   <select required id="fkCsaDfm_{{$v['pkCsa']}}" name="fkCsaDfm_{{$v['pkCsa']}}" class="form-control icon_control dropdown_control valid" >
                                                      <option value="">{{trans('general.gn_select')}}</option>
                                                      @foreach($DFM as $k => $va)
                                                      @if($va->dfm_Text_en != 'Done' && $va->dfm_Text_en != 'Undone')
                                                      <option @if($v['fkCsaDfm'] == $va->pkDfm) selected="selected" @endif value="{{$va->pkDfm}}">{{$va->dfm_Text}}</option>
                                                      @endif
                                                      @endforeach
                                                   </select>
                                                </div>
                                             </div>
                                             <div class="col-md-12">
                                                <div class="form-group form-check">
                                                   <input @if(!empty($v['dfm_manual'])) checked="checked" @endif name="course_manual_dfm_check_checkbox_{{$v['pkCsa']}}" type="checkbox" data-dfm="{{$v['pkCsa']}}" class="form-check-input dfm_check" id="manual_dfm_check_{{$v['pkCsa']}}">
                                                   <label class="custom_checkbox"></label>
                                                   <label class="form-check-label label-text" for="exampleCheck1">{{trans('general.gn_manual_dfm')}}</label>
                                                </div>
                                             </div>
                                             <div class="col-md-12 @if(empty($v['dfm_manual'])) hide_content @endif" id="manual_dfm_{{$v['pkCsa']}}">
                                                <div class="form-group">
                                                   <div class="form-group">
                                                      <label>{{trans('general.gn_dfm')}} *</label>
                                                      <input required type="text" id="csa_ManualDescriptMarks_{{$v['pkCsa']}}" name="csa_ManualDescriptMarks_{{$v['pkCsa']}}" class="form-control" value="{{$v['dfm_manual']}}">
                                                   </div>
                                                </div>
                                             </div>
                                             @else
                                             <div class="col-md-12">
                                                <div class="form-group">
                                                   -
                                                </div>
                                             </div>
                                             @endif
                                          </td>
                                       </tr>
                                       <?php $fc++; ?>
                                       @endforeach
                                    </tbody>
                                 </table>
                              </div>
                              @endif
                           </div>
                        </div>
                        <div class="row">
                           <div class="col-lg-12 text-center">
                              <button type="submit" class="theme_btn">{{trans('general.gn_submit')}}</button>
                              <button type="button" onclick="printCertificate()" class="theme_btn">{{trans('general.gn_print')}}</button>
                           </div>
                        </div>
                     </form>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
@endsection
@push('datatable-scripts')
<!-- Include this Page JS -->
<script type="text/javascript" src="{{ asset('js/dashboard/final_mark.js') }}"></script>
@endpush