@extends('layout.app_with_login')
@section('title', trans('general.gn_final_marks'))
@section('script', asset('js/dashboard/final_mark.js'))
@section('content')
<?php

$Grades = '';
foreach ($mdata->classCreationGrades as $k => $v) {
    $selGrades[] = $v->grade->gra_GradeNumeric;
}
asort($selGrades);
$an = array_unique($selGrades);
$Grades = implode(', ', $an);
//dd($mdata);
?>
<!-- Page Content  -->
<div class="section">
	<div class="container-fluid">
		<div class="row">
			<div class="col-12 mb-3">
               <h2 class="title"><span>{{trans('sidebar.sidebar_nav_organization')}} > </span><a class="no_sidebar_active" href="{{url('/employee/classcreation')}}"><span>{{trans('sidebar.sidebar_nav_class_creation')}} > </span></a><a class="no_sidebar_active" href="{{url('/employee/class-creation-semester')}}/{{$mdata->classCreationSemester[0]->fkCcsClr}}"><span>{{trans('general.gn_semester')}} > </span></a>{{trans('general.gn_final_marks')}}</h2>
            </div>
        <input type="hidden" id="teacher_txt" value="{{trans('general.gn_teacher')}}">
        <input type="hidden" id="principal_txt" value="{{trans('general.gn_principal')}}">
        <input type="hidden" id="stu_sel_valid_txt" value="{{trans('message.msg_student_select_valid')}}">
        <input type="hidden" id="stu_sel_txt" value="{{trans('message.msg_sel_stu')}}">
        <div class="col-12">
            <div class="white_box pt-5 pb-5">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-10 offset-lg-1">

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="card-grey">
                                        <div class="row">
                                            <div class="col-md-6 col-lg-3">
                                                <div class="form-group">
                                                    <p class="label">{{trans('general.gn_school_year')}} :</p>
                                                    <p class="value">{{$mdata->classCreationSchoolYear->sye_NameCharacter}}</p>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-lg-3">
                                                <div class="form-group">
                                                    <p class="label">{{trans('general.gn_school_grade')}} :</p>
                                                    <p class="value">{{$Grades}}</p>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-lg-3">
                                                <div class="form-group">
                                                    <p class="label">{{trans('general.gn_class')}} :</p>
                                                    <p class="value">{{$mdata->classCreationClasses->cla_ClassName}}</p>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-lg-3">
                                                <div class="form-group">
                                                    <p class="label">{{trans('general.gn_homeroom_teacher')}} :</p>
                                                    <p class="value">@if(isset($mdata->classCreationSemester[0]->homeRoomTeacher[0])){{$mdata->classCreationSemester[0]->homeRoomTeacher[0]->employeesEngagement->employee->emp_EmployeeName}} {{$mdata->classCreationSemester[0]->homeRoomTeacher[0]->employeesEngagement->employee->emp_EmployeeSurname}}@endif</p>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-lg-3">
                                                <div class="form-group">
                                                    <p class="label">{{trans('general.gn_semester')}} :</p>
                                                    <p class="value">{{$mdata->classCreationSemester[0]->semester->edp_EducationPeriodName}}</p>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-lg-3">
                                                <div class="form-group">
                                                    <p class="label">{{trans('general.gn_chief_student')}} :</p>
                                                    <p class="value">@if(isset($mdata->classCreationSemester[0]->chiefStudent->student)){{$mdata->classCreationSemester[0]->chiefStudent->student->full_name}}@endif</p>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-lg-3">
                                                <div class="form-group">
                                                    <p class="label">{{trans('general.gn_treasure_student')}} :</p>
                                                    <p class="value">@if(isset($mdata->classCreationSemester[0]->treasureStudent->student)){{$mdata->classCreationSemester[0]->treasureStudent->student->full_name}}@endif</p>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-lg-3">
                                                <div class="form-group">
                                                    <p class="label">{{trans('general.gn_status')}} :</p>
                                                    <p class="value">@if($mdata->clr_Status == 'Pending')<span class="custom_badge badge badge-warning">{{trans('general.gn_pending')}}</span>@else<span class="custom_badge badge badge-success">{{trans('general.gn_publish')}}</span>@endif</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <p class="mt-3"><strong>{{trans('general.gn_students_of_class')}} {{$mdata->classCreationClasses->cla_ClassName}}</strong></p>
                                    <div class="table-responsive mt-2">
                                        <table class="color_table">
                                            <tbody><tr>
                                                <th>{{trans('general.sr_no')}}</th>
                                                <th>ID</th>
                                                <th>{{trans('general.gn_name')}}</th>
                                                <th>{{trans('general.gn_grade')}}</th>
                                                <th>{{trans('general.gn_education_plan')}}</th>
                                                <th>{{trans('general.gn_action')}}</th>
                                            </tr>
                                            @foreach($mdata->classCreationSemester[0]->classStudentsSemester as $k => $v)
                                            <tr>
                                                <td>{{$k+1}}</td>
                                                <td>@if($v->studentEnroll->student->stu_StudentID != null) {{$v->studentEnroll->student->stu_StudentID}} @else {{$v->studentEnroll->student->stu_TempCitizenId}} @endif</td>
                                                <td>{{$v->studentEnroll->student->full_name}}</td>
                                                <td>{{$v->studentEnroll->grade->gra_GradeNumeric}}</td>
                                                <td>{{$v->studentEnroll->educationProgram->edp_Name}} - {{$v->studentEnroll->educationPlan->epl_EducationPlanName}}</td>
                                                {{-- <td><a  class="ajax_request no_sidebar_active theme_btn min_btn" href="{{url('employee/finalMark')}}/{{$v->pkSem}}">{{trans('general.gn_select')}}</a></td> --}}
                                                <td>
                                                    <!-- onclick="openFinalMarks({{$v->pkSem}}) -->
                                                <a data-url="employee/finalmarks/{{$v->pkSem}}/edit" href="{{url('employee/finalmarks/')}}/{{$v->pkSem}}{{'/edit'}}" ">{{-- {{trans('general.gn_select')}} --}}<img src="{{asset('images/ic_eye.png')}}"></a>
                                                @if(count($v->classStudentCertificate))
                                                <a href="javascript:void(0)" onclick="printFinalMarks({{$v->pkSem}})">{{-- {{trans('general.gn_print')}} --}}<img src="{{asset('images/ic_printer.png')}}"></a>
                                                @endif
                                            </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                        </table>
                                    </div>

                                </div>
                            </div>


                        </div>
                    </div>
                </div>
            </div>

		</div>
	</div>
</div>
@endsection

@push('datatable-scripts')
<!-- Include this Page JS -->
<script type="text/javascript" src="{{ asset('js/dashboard/final_mark.js') }}"></script>
@endpush