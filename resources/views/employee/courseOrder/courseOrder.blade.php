@extends('layout.app_with_login')
@section('title', trans('sidebar.sidebar_nav_course_orders'))
@section('script', asset('js/dashboard/course_order.js'))
@section('content')

<?php
$selOrderData = [];
$selMCGrades = [];
$selOCGrades = [];
$selFLGrades = [];

$grades = [];
$gradesTmp = [];

if(isset($mdata) && !empty($mdata->educationPlan)){
    foreach ($mdata->educationPlan->mandatoryCourse as $k => $v) {
        $tmpMC[] = $v->fkEmcGra;
        $gradesTmp[$v->fkEmcGra] = $v->grade->gra_GradeNumeric;
    }
    $grades = array_unique($gradesTmp);
    foreach ($mdata->educationPlan->optionalCourse as $k => $v) {
        $tmpOC[] = $v->fkEocGra;
    }
    $selOCGrades = array_count_values($tmpOC);

    foreach ($mdata->educationPlan->foreignLanguageCourse as $k => $v) {
        $tmpMC[] = $v->fkEflGra;
    }
    $selMCGrades = array_count_values($tmpMC);
    foreach($mdata->courseOrderAllocation as $k => $v){
        $selOrderData[$v->fkCoaGra][$v->fkCoaCrs] = $v->coa_OrderNo;
    }
}
?>
<!-- Page Content  -->
<div class="section">
	<div class="container-fluid">
		<div class="row">
			<div class="col-12 mb-3">
               <h2 class="title"><span>{{trans('sidebar.sidebar_nav_organization')}} > </span><a class="no_sidebar_active" href="{{url('/employee/courseorders')}}"><span>{{trans('sidebar.sidebar_nav_course_orders')}} > </span></a>@if(isset($mdata)){{trans('general.gn_edit')}}@else{{trans('general.gn_add')}}@endif</h2>
            </div>
        </div>
        <div class="col-md-12">
            <div class="white_box p-5 pl-3 pr-3">
                <form name="course_order_form">
                    @if(isset($mdata->pkCor))
                        <input type="hidden" id="pkCor" value="{{$mdata->pkCor}}">
                    @endif
                <div class="row">
                    <div class="col-lg-7">
                        <div class="">
                            <div class="form-group">
                                <label>{{trans('general.gn_education_program_or_plan')}}</label>
                                <select @if(isset($mdata)) disabled @endif onchange="fetchCourses(this.value)" id="epl" name="epl" class="form-control icon_control dropdown_control">
                                    <option value="">{{trans('general.gn_select')}}</option>
                                    @foreach($planDetail as $k => $v)
                                        <option @if(isset($mdata) && $mdata->fkCorEpl == $v->educationPlan->pkEpl) selected @endif value="{{$v->educationPlan->pkEpl}}">{{$v->educationProgram->edp_Name}} - {{$v->educationPlan->epl_EducationPlanName}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>

                    @foreach($grades as $kg => $vg)
                    <div class="profile_info_container full_width ep_gra_{{$kg}}">
                        <div class="row">
                            <div class="col-4"></div>
                            <div class="col-4" style="text-align: center;"><strong class="gra_name">{{trans('general.gn_grade')}} - {{$vg}}</strong></div>
                            <div class="col-4 text-right"></div>
                            <div class="col-lg-6">
                                <p class="mt-2"><strong>{{trans('general.gn_mandatory_courses')}}</strong></p>
                                <div class="table-responsive mt-2 mb-3">
                                    <table class="color_table mc_course">
                                        <tbody>
                                            <tr>
                                                <th>#</th>
                                                <th>{{trans('general.gn_courses')}}</th>
                                                <th>{{trans('general.gn_grade')}}</th>
                                                <th>{{trans('general.gn_course_order')}}</th>
                                            </tr>
                                            @if(isset($mdata))
                                            <?php $m = 1;?>
                                            @foreach($mdata->educationPlan->mandatoryCourse as $k => $v)
                                            @if($kg == $v->grade->pkGra)
                                            <tr class="mc_elem course_{{$v->fkEplCrs}} gra_{{$v->fkEmcGra}}">
                                                <td>{{$m}}</td>
                                                <td>{{$v->mandatoryCourseGroup->crs_CourseName}}</td>
                                                <td>{{$v->grade->gra_GradeNumeric}}</td>
                                                <td>
                                                    <input type="hidden" name="crs_ids[]" value="{{$v->fkEplCrs}}">
                                                    <input type="hidden" name="epr_ids[]" value="{{$mdata->fkCorEdp}}">
                                                    <input type="hidden" name="gra_ids[]" value="{{$v->fkEmcGra}}">
                                                    <select currval="@if(isset($selOrderData[$v->fkEmcGra][$v->fkEplCrs])){{$selOrderData[$v->fkEmcGra][$v->fkEplCrs]}}@endif" id="sel_mc_{{$kg}}_{{$m}}" name="sel_mc_{{$kg}}_{{$m}}" data-sel-gra="mc_gra_{{$v->fkEmcGra}}" onchange="courseOrderSelect(this)" required="" class="form-control icon_control dropdown_control mc_gra_{{$v->fkEmcGra}}">
                                                        <option value="">{{trans('general.gn_select')}}</option>
                                                        @foreach($selMCGrades as $vk => $vv)
                                                        @if($v->fkEmcGra == $vk)
                                                            @for ($x = 1; $x <= $vv; $x++)
                                                                <option @if(isset($selOrderData[$v->fkEmcGra]) && isset($selOrderData[$v->fkEmcGra][$v->fkEplCrs]) && $selOrderData[$v->fkEmcGra][$v->fkEplCrs] == $x) selected @elseif(isset($selOrderData[$v->fkEmcGra]) && in_array($x, $selOrderData[$v->fkEmcGra])) disabled @endif value="{{$x}}">{{$x}}</option>
                                                            @endfor
                                                        @endif
                                                        @endforeach
                                                    </select>
                                                </td>
                                            </tr>
                                            <?php $m++;?>
                                            @endif
                                            @endforeach
                                            @foreach($mdata->educationPlan->foreignLanguageCourse as $k => $v)
                                            @if($kg == $v->grade->pkGra)
                                            <tr class="mc_elem course_{{$v->fkEflCrs}} gra_{{$v->fkEflGra}}">
                                                <td>{{$m}}</td>
                                                <td>{{$v->foreignLanguageGroup->crs_CourseName}}</td>
                                                <td>{{$v->grade->gra_GradeNumeric}}</td>
                                                <td>
                                                    <input type="hidden" name="crs_ids[]" value="{{$v->fkEflCrs}}"><input type="hidden" name="epr_ids[]" value="{{$mdata->fkCorEdp}}"><input type="hidden" name="gra_ids[]" value="{{$v->fkEflGra}}">
                                                    <select currval="@if(isset($selOrderData[$v->fkEflGra][$v->fkEflCrs])){{$selOrderData[$v->fkEflGra][$v->fkEflCrs]}}@endif" id="sel_mc_{{$kg}}_{{$m}}" name="sel_mc_{{$kg}}_{{$m}}" data-sel-gra="mc_gra_{{$v->fkEflGra}}" onchange="courseOrderSelect(this)" required="" class="form-control icon_control dropdown_control mc_gra_{{$v->fkEflGra}}"><option value="">{{trans('general.gn_select')}}</option>@foreach($selMCGrades as $vk => $vv)
                                                    @if($v->fkEflGra == $vk)
                                                        @for ($x = 1; $x <= $vv; $x++)
                                                        <option @if(isset($selOrderData[$v->fkEflGra]) && isset($selOrderData[$v->fkEflGra][$v->fkEflCrs]) && $selOrderData[$v->fkEflGra][$v->fkEflCrs] == $x) selected @elseif(isset($selOrderData[$v->fkEflGra]) && in_array($x, $selOrderData[$v->fkEflGra])) disabled @endif value="{{$x}}">{{$x}}</option>
                                                        @endfor
                                                    @endif
                                                    @endforeach</select>
                                                </td>
                                            </tr>
                                            <?php $m++;?>
                                            @endif
                                            @endforeach
                                            @endif
                                        </tbody>
                                    </table>
                                </div>
                            </div>

                            <div class="col-lg-6">
                                <p class="mt-2"><strong>{{trans('general.gn_optional_courses')}}</strong></p>
                                <div class="table-responsive mt-2 mb-3">
                                    <table class="color_table oc_course ">
                                        <tbody>
                                            <tr>
                                                <th>#</th>
                                                <th>{{trans('general.gn_courses')}}</th>
                                                <th>{{trans('general.gn_grade')}}</th>
                                                <th>{{trans('general.gn_course_order')}}</th>
                                            </tr>
                                            @if(isset($mdata))
                                            <?php $o = 1;?>
                                            @foreach($mdata->educationPlan->optionalCourse as $k => $v)
                                            @if($kg == $v->grade->pkGra)
                                            <tr class="oc_elem course_{{$v->fkEocCrs}} gra_{{$v->fkEocGra}}">
                                                <td>{{$o}}</td>
                                                <td>{{$v->optionalCoursesGroup->crs_CourseName}}</td>
                                                <td>{{$v->grade->gra_GradeNumeric}}</td>
                                                <td>
                                                    <input type="hidden" name="crs_ids[]" value="{{$v->fkEocCrs}}"><input type="hidden" name="epr_ids[]" value="{{$mdata->fkCorEdp}}"><input type="hidden" name="gra_ids[]" value="{{$v->fkEocGra}}"><input type="hidden" name="epl_ids[]" value="{{$mdata->fkCorEpl}}">
                                                    <select currval="@if(isset($selOrderData[$v->fkEocGra][$v->fkEocCrs])){{$selOrderData[$v->fkEocGra][$v->fkEocCrs]}}@endif" id="sel_oc_{{$k+1}}" name="sel_oc_{{$k+1}}" data-sel-gra="oc_gra_{{$v->fkEocGra}}" onchange="courseOrderSelect(this)" required="" class="form-control icon_control dropdown_control oc_gra_{{$v->fkEocGra}}"><option value="">{{trans('general.gn_select')}}</option>
                                                    @foreach($selOCGrades as $vk => $vv)
                                                    @if($v->fkEocGra == $vk)
                                                        @for ($x = 1; $x <= $vv; $x++)
                                                        <option @if(isset($selOrderData[$v->fkEocGra]) && isset($selOrderData[$v->fkEocGra][$v->fkEocCrs]) && $selOrderData[$v->fkEocGra][$v->fkEocCrs] == $x) selected @elseif(isset($selOrderData[$v->fkEocGra]) && in_array($x, $selOrderData[$v->fkEocGra]) && !empty($selOrderData[$v->fkEocGra][$v->fkEocCrs])) disabled @endif value="{{$x}}">{{$x}}</option>
                                                        @endfor
                                                    @endif
                                                    @endforeach
                                                    </select>
                                                </td>
                                            </tr>
                                            <?php $o++;?>
                                            @endif
                                            @endforeach
                                            @endif
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach

                    <div class="col-md-12">
                        <div class="text-center">
                            <button type="submit" class="theme_btn">{{trans('general.gn_submit')}}</button>
                            <a class="no_sidebar_active theme_btn red_btn" href="{{url('/employee/courseorders')}}">{{trans('general.gn_cancel')}}</a>
                        </div>
                    </div>
                </div>
            </form>
            </div>
        </div>
	</div>
    <input type="hidden" id="gn_optional_courses" value="{{trans('general.gn_optional_courses')}}">
    <input type="hidden" id="gn_courses" value="{{trans('general.gn_courses')}}">
    <input type="hidden" id="gn_grade" value="{{trans('general.gn_grade')}}">
    <input type="hidden" id="gn_mandatory_courses" value="{{trans('general.gn_mandatory_courses')}}">
    <input type="hidden" id="gn_course_order" value="{{trans('general.gn_course_order')}}">
</div>
<script>
    var listing_url = "{{route('fetch-courseorders-lists')}}";
    var courses_url = "{{route('fetch-educationplancourses-lists')}}";
</script>
@endsection

@push('datatable-scripts')
<!-- Include this Page JS -->s
<script type="text/javascript" src="{{ asset('js/dashboard/course_order.js') }}"></script>
@endpush