@extends('layout.app_with_login')
@section('title', trans('sidebar.sidebar_nav_course_orders'))
@section('script', asset('js/dashboard/course_order.js'))
@section('content')
<!-- Page Content  -->
<div class="section">
	<div class="container-fluid">
        <div class="row">
			<div class="col-12 mb-3">
               <h2 class="title"><span>{{trans('sidebar.sidebar_nav_organization')}} > </span> {{trans('sidebar.sidebar_nav_course_orders')}}</h2>
            </div>
            <div class="col-md-4 mb-3">
                <input type="text" id="search_course_order" class="form-control icon_control search_control" maxlength="30" placeholder="{{trans('general.gn_search')}}">
            </div>
            <div class="col-md-3 text-md-right mb-3">
            </div>
            <div class="col-md-3 text-md-right mb-3">
            </div>
            <div class="col-md-2 col-6 mb-3">
                <a href="{{url('/employee/courseorders/create')}}"><button class="theme_btn small_btn no_sidebar_active">{{trans('general.gn_add_new')}}</button></a>
            </div>
        </div>

        <div class="row">
            <div class="col-12">
                <div class="theme_table">
                    <div class="table-responsive">
                        <table id="course_order_listing" class="display" style="width:100%">
                            <thead>
                                <tr>
                                    <th>{{trans('general.sr_no')}}.</th>
                                    <th>ID</th>
                                    <th>{{trans('general.gn_education_program')}}</th>
                                    <th>{{trans('general.gn_actions')}}</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>

    </div>

    <div class="theme_modal modal fade" id="delete_prompt" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <img src="{{asset('images/ic_close_bg.png')}}" class="modal_top_bg">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <img src="{{asset('images/ic_close_circle_white.png')}}">
                    </button>
                        <div class="row">
                            <div class="col-lg-1"></div>
                            <div class="col-lg-10">
                                <h5 class="modal-title" id="exampleModalCenterTitle">{{trans('general.gn_delete')}}</h5>
                                <div class="form-group text-center">
                                    <label>{{trans('general.gn_delete_prompt')}} ?</label>
                                    <input type="hidden" id="did">
                                </div>
                                <div class="text-center modal_btn ">
                                    <button style="display: none;" class="theme_btn show_delete_modal full_width small_btn" data-toggle="modal" data-target="#delete_prompt">{{trans('general.gn_delete')}}</button>
                                    <button type="button" onclick="confirmDelete()" class="theme_btn">{{trans('general.gn_yes')}}</button>
                                    <button type="button" data-dismiss="modal" class="theme_btn red_btn">{{trans('general.gn_no')}}</button>
                                </div>
                            </div>
                            <div class="col-lg-1"></div>
                        </div>
                </div>
            </div>
        </div>
    </div>

</div>
<script>
    var listing_url = "{{route('fetch-courseorders-lists')}}";
    var courses_url = "{{route('fetch-educationplancourses-lists')}}";
</script>
@endsection

@push('datatable-scripts')
<!-- Include this Page JS -->
<script type="text/javascript" src="{{ asset('js/dashboard/course_order.js') }}"></script>
@endpush