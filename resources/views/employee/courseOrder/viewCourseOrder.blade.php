@extends('layout.app_with_login')
@section('title', trans('sidebar.sidebar_nav_course_orders'))
@section('content')
<?php
$selOrderData = [];
$selMCGrades = [];
$selOCGrades = [];
$selFLGrades = [];

$grades = [];
$gradesTmp = [];

if(isset($mdata) && !empty($mdata->educationPlan)){
    foreach ($mdata->educationPlan->mandatoryCourse as $k => $v) {
        $tmpMC[] = $v->fkEmcGra;
        $gradesTmp[$v->fkEmcGra] = $v->grade->gra_GradeNumeric;
    }
    $grades = array_unique($gradesTmp);

    // $selMCGrades = array_count_values($tmpMC);

    foreach ($mdata->educationPlan->optionalCourse as $k => $v) {
        $tmpOC[] = $v->fkEocGra;
    }
    $selOCGrades = array_count_values($tmpOC);

    foreach ($mdata->educationPlan->foreignLanguageCourse as $k => $v) {
        $tmpMC[] = $v->fkEflGra;
    }
    $selMCGrades = array_count_values($tmpMC);

    foreach($mdata->courseOrderAllocation as $k => $v){
        $selOrderData[$v->fkCoaCrs][$v->fkCoaGra] = $v->coa_OrderNo;
    }

}


?>
<!-- Page Content  -->
<div class="section">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12 mb-3">
               <h2 class="title"><span>{{trans('sidebar.sidebar_nav_organization')}} > </span><a class="no_sidebar_active" href="{{url('/employee/courseOrders')}}"><span>{{trans('sidebar.sidebar_nav_course_orders')}} > </span></a>{{trans('general.gn_view_details')}}</h2>
            </div>
        </div>
        <div class="col-md-12">
            <div class="white_box p-5 pl-3 pr-3">
                <form name="course_order_form">
                    @if(isset($mdata->pkCor))
                        <input type="hidden" id="pkCor" value="{{$mdata->pkCor}}">
                    @endif

                <div class="row">
                    <div class="col-lg-7">
                        <div class="">
                            <div class="form-group">
                                <label>{{trans('general.gn_education_program')}}</label>
                                <select @if(isset($mdata)) disabled @endif onchange="fetchCourses(this.value)" id="epl" name="epl" class="form-control icon_control dropdown_control">
                                    <option value="">{{trans('general.gn_select')}}</option>
                                    @foreach($planDetail as $k => $v)
                                        <option @if(isset($mdata) && $mdata->fkCorEpl == $v->educationPlan->pkEpl) selected @endif value="{{$v->educationPlan->pkEpl}}">{{$v->educationProgram->edp_Name}} - {{$v->educationPlan->epl_EducationPlanName}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>

                    @foreach($grades as $kg => $vg)
                    <div class="profile_info_container full_width ep_gra_{{$kg}}">
                        <div class="row">
                            <div class="col-4"></div>
                            <div class="col-4" style="text-align: center;"><strong class="gra_name">{{trans('general.gn_grade')}} - {{$vg}}</strong></div>
                            <div class="col-4 text-right"></div>

                        <div class="col-lg-6">
                            <p class="mt-2"><strong>{{trans('general.gn_mandatory_courses')}}</strong></p>
                            <div class="table-responsive mt-2 mb-3">
                                <table class="color_table mc_course ">
                                    <tbody>
                                        <tr class="text-center">
                                            <th>#</th>
                                            <th>{{trans('general.gn_courses')}}</th>
                                            <th>{{trans('general.gn_grade')}}</th>
                                            <th>{{trans('general.gn_course_order')}}</th>
                                        </tr>
                                        <?php $m = 1;?>
                                        @foreach($mdata->educationPlan->mandatoryCourse as $k => $v)
                                        @if($kg == $v->grade->pkGra)
                                        <tr class="mc_elem course_{{$v->fkEplCrs}} gra_{{$v->fkEmcGra}} text-center">
                                            <td>{{$m}}</td>
                                            <td>{{$v->mandatoryCourseGroup->crs_CourseName}}</td>
                                            <td>{{$v->grade->gra_GradeNumeric}}</td>
                                            <td>@if(isset($selOrderData[$v->fkEplCrs][$v->fkEmcGra])){{$selOrderData[$v->fkEplCrs][$v->fkEmcGra]}}@endif</td>
                                        </tr>
                                        <?php $m++;?>
                                        @endif
                                        @endforeach

                                        @foreach($mdata->educationPlan->foreignLanguageCourse as $k => $v)
                                        @if($kg == $v->grade->pkGra)
                                        <tr class="mc_elem course_{{$v->fkEflCrs}} gra_{{$v->fkEflGra}} text-center">
                                            <td>{{$m}}</td>
                                            <td>{{$v->foreignLanguageGroup->crs_CourseName}}</td>
                                            <td>{{$v->grade->gra_GradeNumeric}}</td>
                                            <td>@if(isset($selOrderData[$v->fkEflCrs][$v->fkEflGra])){{$selOrderData[$v->fkEflCrs][$v->fkEflGra]}}@endif</td>
                                        </tr>
                                        <?php $m++;?>
                                        @endif
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>

                        <div class="col-lg-6">
                            <p class="mt-2"><strong>{{trans('general.gn_optional_courses')}}</strong></p>
                            <div class="table-responsive mt-2 mb-3">
                                <table class="color_table oc_course ">
                                    <tbody>
                                        <tr class="text-center">
                                            <th>#</th>
                                            <th>{{trans('general.gn_courses')}}</th>
                                            <th>{{trans('general.gn_grade')}}</th>
                                            <th>{{trans('general.gn_course_order')}}</th>
                                        </tr>
                                        <?php $o = 1;?>
                                        @foreach($mdata->educationPlan->optionalCourse as $k => $v)
                                        @if($kg == $v->grade->pkGra)
                                        <tr class="oc_elem course_{{$v->fkEocCrs}} gra_{{$v->fkEocGra}} text-center">
                                            <td>{{$o}}</td>
                                            <td>{{$v->optionalCoursesGroup->crs_CourseName}}</td>
                                            <td>{{$v->grade->gra_GradeNumeric}}</td>
                                            <td>@if(isset($selOrderData[$v->fkEocCrs][$v->fkEocGra])){{$selOrderData[$v->fkEocCrs][$v->fkEocGra]}}@endif</td>
                                        </tr>
                                        <?php $o++;?>
                                        @endif
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>

                    </div>
                </div>
                @endforeach

                </div>
            </form>
            </div>
        </div>
    </div>
</div>

@endsection