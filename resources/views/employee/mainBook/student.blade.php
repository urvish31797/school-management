@extends('layout.app_with_login')
@section('title', trans('sidebar.sidebar_nav_main_books'))
@section('script', asset('js/dashboard/mainbook.js'))
@section('content')
 <!-- Page Content  -->
<div class="section">
	<div class="container-fluid">
		<div class="row ">
            <div class="col-12 mb-3">
                <h2 class="title"><span>{{trans('sidebar.sidebar_nav_school')}} ></span> <a class="no_sidebar_active" href="{{url('/employee/mainbooks')}}"><span>{{trans('sidebar.sidebar_nav_main_books')}} ></span></a>  {{trans('sidebar.sidebar_nav_students')}}</h2>
            </div>
            <div class="col-md-4 mb-3">
                <input type="text" id="search_student" maxlength="30" class="form-control icon_control search_control" placeholder="{{trans('general.gn_search')}}">
            </div>
            <div class="col-md-4 text-md-right mb-3">
                <div class="row">
                    <div class="col-4 text-right pt-1 p-0">
                        <label class="blue_label">{{trans('general.gn_joining_date')}}</label>
                    </div>
                    <div class="col-8">
                        <input type="text" id="mdate_filter" class="form-control datepicker date_control icon_control">
                    </div>
                </div>
                <input type="hidden" id="fkSteMbo" value="{{$mid}}">
            </div>
            <div class="col-md-2 col-6 mb-3">

            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="theme_table">
                    <div class="table-responsive">
                        <table id="student_listing" class="display" style="width:100%">
                            <thead>
                                <tr>
                                    <th>{{trans('general.sr_no')}}.</th>
                                    <th>{{trans('general.gn_student_name')}}</th>
                                    <th>{{trans('general.gn_father_name')}}</th>
                                    <th>{{trans('general.gn_mother_name')}}</th>
                                    <th>{{trans('general.gn_joining_date')}}</th>
                                    <th>{{trans('general.gn_grade')}}</th>
                                    <th>{{trans('general.gn_dob')}}</th>
                                    <th>{{trans('general.gn_phone_number')}}</th>
                                    <th>{{trans('general.gn_status')}}</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
	</div>


</div>
<script>
    var listing_url = "{{route('fetch-mainbooks-lists')}}";
</script>
@endsection

@push('datatable-scripts')
<!-- Include datatable Page JS -->
<script type="text/javascript" src="{{ asset('js/dashboard/mainbook.js') }}"></script>
@endpush