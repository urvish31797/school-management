@extends('layout.app_with_login')
@section('title', trans('sidebar.sidebar_nav_main_books'))
@section('script', asset('js/dashboard/mainbook.js'))
@section('content')
 <!-- Page Content  -->
<div class="section">
    <div class="container-fluid">
        <div class="row ">
            <div class="col-12 mb-3">
                <h2 class="title"><span>{{trans('sidebar.sidebar_nav_school')}} > </span>{{trans('sidebar.sidebar_nav_main_books')}}</h2>
            </div>
            <div class="col-md-4 mb-3">
                <input type="text" id="search_mainbook" maxlength="30" class="form-control icon_control search_control" placeholder="{{trans('general.gn_search')}}">
            </div>
            <div class="col-md-4 text-md-right mb-3">
                <div class="row">
                    <div class="col-4 text-right pt-1 p-0">
                        <label class="blue_label">{{trans('general.gn_opening_date')}}</label>
                    </div>
                    <div class="col-8">
                        <input type="text" id="date_filter" class="form-control datepicker date_control icon_control">
                    </div>
                </div>
            </div>
            <div class="col-md-2 col-6 mb-3">

            </div>
            <div class="col-md-2 col-6 mb-3">
                <a><button class="theme_btn show_modal full_width small_btn" data-toggle="modal" data-target="#add_new">{{trans('general.gn_add_new')}}</button></a>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="theme_table">
                    <div class="table-responsive">
                        <table id="mainbook_listing" class="display" style="width:100%">
                            <thead>
                                <tr>
                                    <th>{{trans('general.sr_no')}}.</th>
                                    <th>{{trans('general.gn_uid')}}</th>
                                    <th>{{trans('general.gn_name')}}</th>
                                    <th>{{trans('general.gn_opening_date')}}</th>
                                    <th>{{trans('general.gn_notes')}}</th>
                                    <th><div class="action">{{trans('general.gn_actions')}}</div></th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Add New Popup -->
<div class="theme_modal modal fade" id="add_new" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <img src="{{asset('images/ic_close_bg.png')}}" class="modal_top_bg">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <img src="{{asset('images/ic_close_circle_white.png')}}">
                </button>
                <form name="add-mainbook-form">
                    <div class="row">
                        <div class="col-lg-1"></div>
                        <div class="col-lg-10">
                            <h5 class="modal-title" id="exampleModalCenterTitle">{{trans('sidebar.sidebar_nav_main_books')}}</h5>
                            <div class="form-group">
                                <label>{{trans('general.gn_name')}} *</label>
                                <input type="text" name="mbo_MainBookNameRoman" id="mbo_MainBookNameRoman" class="form-control icon_control">
                            </div>
                            <input type="hidden" id="pkMbo">
                            <input type="hidden" id="fkMboSch" value="{{$mainSchool}}">
                            <div class="form-group">
                                <label>{{trans('general.gn_opening_date')}} *</label>
                                <input type="text" name="mbo_OpeningDate" id="mbo_OpeningDate" class="form-control icon_control datepicker date_control">
                            </div>
                            <div class="form-group">
                                <label>{{trans('general.gn_note')}}</label>
                                <input type="text" name="mbo_Notes" id="mbo_Notes" class="form-control icon_control">
                            </div>
                            <div class="text-center modal_btn ">
                                <button type="submit" class="theme_btn">{{trans('general.gn_submit')}}</button>
                            </div>
                        </div>
                        <div class="col-lg-1"></div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="theme_modal modal fade" id="delete_prompt" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <img src="{{asset('images/ic_close_bg.png')}}" class="modal_top_bg">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <img src="{{asset('images/ic_close_circle_white.png')}}">
                </button>
                    <div class="row">
                        <div class="col-lg-1"></div>
                        <div class="col-lg-10">
                            <h5 class="modal-title" id="exampleModalCenterTitle">{{trans('general.gn_delete')}}</h5>
                            <div class="form-group text-center">
                                <label>{{trans('general.gn_delete_prompt')}}</label>
                                <input type="hidden" id="did">
                            </div>
                            <div class="text-center modal_btn ">
                                <button style="display: none;" class="theme_btn show_delete_modal full_width small_btn" data-toggle="modal" data-target="#delete_prompt">{{trans('general.gn_delete')}}</button>
                                <button type="button" onclick="confirmDelete()" class="theme_btn">{{trans('general.gn_yes')}}</button>
                                <button type="button" data-dismiss="modal" class="theme_btn red_btn">{{trans('general.gn_no')}}</button>
                            </div>
                        </div>
                        <div class="col-lg-1"></div>
                    </div>
            </div>
        </div>
    </div>
</div>
<script>
    var listing_url = "{{route('fetch-mainbooks-lists')}}";
</script>
@endsection

@push('datatable-scripts')
<!-- Include datatable Page JS -->
<script type="text/javascript" id="datatable_script" src="{{ asset('js/dashboard/mainbook.js') }}"></script>
@endpush
