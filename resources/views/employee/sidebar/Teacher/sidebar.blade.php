<nav id="sidebar" class="">
    <div class="sidebar-header">
        <h3>Hertronic</h3>
    </div>

    <ul class="list-unstyled components slim_scroll">
        <li class="{{ (request()->is('employee/dashboard*')) ? 'active' : '' }}">
            <a href="{{url('/employee/dashboard')}}">
               <img alt="" src="{{ asset('images/ic_dashoard_color.png') }}" class="color">
               <img alt="" src="{{ asset('images/ic_dashoard.png') }}" class="selected">
                {{trans('sidebar.sidebar_nav_dasbhoard')}}
            </a>
        </li>
        <li class="{{ (request()->is('employee/attendance-accomplishment*')) || (request()->is('employee/student-behaviour-notes*')) ? 'active' : '' }}">
            <a href="{{url('/employee/attendance-accomplishment')}}">
               <img alt="" src="{{asset('images/ic_attendant-list_color.png')}}" class="color">
               <img alt="" src="{{asset('images/ic_attendant-list.png')}}" class="selected">
                {{trans('sidebar.sidebar_nav_sa_sa')}}
            </a>
        </li>
        <li class="{{ (request()->is('employee/periodic-exam*')) ? 'active' : '' }}">
            <a href="{{url('/employee/periodic-exam')}}">
               <img alt="" src="{{asset('images/ic_exam_color.png')}}" class="color">
               <img alt="" src="{{asset('images/ic_exam.png')}}" class="selected">
                {{trans('sidebar.sidebar_periodic_exam')}}
            </a>
        </li>
        <li class="{{ (request()->is('employee/subAdmins')) || (request()->is('employee/addSubAdmin')) ||  (request()->is('employee/viewSubAdmin*')) || (request()->is('employee/editSubAdmin*')) || (request()->is('employee/teachers')) ||  (request()->is('employee/viewTeacher*')) || (request()->is('employee/editTeacher*')) ? 'active' : '' }}">
            <a href="#pageSubmenu1" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
               <img alt="" src="{{asset('images/ic_users_color.png')}}" class="color">
                <img alt="" src="{{asset('images/ic_users.png')}}" class="selected">
                {{trans('sidebar.sidebar_nav_students')}}
                <i class="fas fa-chevron-down right-arrow"></i>
            </a>
            <ul class="collapse list-unstyled {{ (request()->is('employee/students')) || (request()->is('employee/addStudent')) || (request()->is('employee/editStudents*')) ? 'show' : '' }}" id="pageSubmenu1">
                <li>
                    <a href="#">{{trans('sidebar.sidebar_nav_behaviour')}}</a>
                </li>
                <li>
                    <a href="#">{{trans('sidebar.sidebar_nav_extracurricular_activity')}}</a>
                </li>
                <li>
                    <a href="#">{{trans('sidebar.sidebar_nav_discipline_measure')}}</a>
                </li>
            </ul>
        </li>
        <li>
            <a href="#">
               <img alt="" src="{{asset('images/ic_business_color.png')}}" class="color">
               <img alt="" src="{{asset('images/ic_business.png')}}" class="selected">
                PTM
            </a>
        </li>
        <li>
            <a href="#">
               <img alt="" src="{{asset('images/ic_attendant-list_color.png')}}" class="color">
               <img alt="" src="{{asset('images/ic_attendant-list.png')}}" class="selected">
                {{trans('sidebar.sidebar_nav_allocated_class')}}
            </a>
        </li>
    </ul>
    <div class="bottom-link">
      <a href="#x" class="help-link"><img src="{{ asset('images/ic_comment.png')}}" >{{trans('general.gn_help')}}?</a>
      <a href="#x" class="logout"><img src="{{ asset('images/ic_logout.png')}}" ></a>
    </div>


</nav>