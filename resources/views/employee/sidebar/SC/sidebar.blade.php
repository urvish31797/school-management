<nav id="sidebar" class="">
    <div class="sidebar-header">
        <h3>Hertronic</h3>
    </div>

    <ul class="list-unstyled components slim_scroll">
        <li class="{{ (request()->is('employee/dashboard')) ? 'active' : '' }}">
            <a href="{{url('/employee/dashboard')}}">
               <img alt="" src="{{ asset('images/ic_dashoard_color.png') }}" class="color">
               <img alt="" src="{{ asset('images/ic_dashoard.png') }}" class="selected">
                {{trans('sidebar.sidebar_nav_dasbhoard')}}
            </a>
        </li>
        <li class="{{ (request()->is('employee/myschool*')) || (request()->is('employee/villageschools*')) || (request()->is('employee/mainbooks*')) ? 'active' : '' }}">
            <a href="#pageSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
               <img alt="" src="{{asset('images/ic_high-school_color.png')}}" class="color">
               <img alt="" src="{{asset('images/ic_high-school.png')}}" class="selected">
                {{trans('sidebar.sidebar_nav_school')}}
                <i class="cfa fas fa-chevron-down right-arrow"></i>
            </a>
            <ul class="collapse list-unstyled {{ (request()->is('employee/myschool*')) || (request()->is('employee/villageschools*')) || (request()->is('employee/mainbooks*')) ? 'show' : '' }}" id="pageSubmenu">
                <li class="{{ (request()->is('employee/myschool*')) ? 'active' : '' }}">
                    <a href="{{url('/employee/myschool')}}">{{trans('sidebar.sidebar_nav_my_school')}}</a>
                </li>
                <li class="{{ (request()->is('employee/villageschools*')) ? 'active' : '' }}">
                    <a href="{{url('/employee/villageschools')}}">{{trans('sidebar.sidebar_nav_village_schools')}}</a>
                </li>
                <li class="{{ (request()->is('employee/mainbooks*')) ? 'active' : '' }}">
                    <a href="{{url('/employee/mainbooks')}}">{{trans('sidebar.sidebar_nav_main_books')}}</a>
                </li>
            </ul>
        </li>
        <li class="{{ (request()->is('employee/subadmins*')) || (request()->is('employee/employees*')) || (request()->is('employee/engage-employee*')) || (request()->is('employee/students*')) || (request()->is('employee/enrollstudents*')) ? 'active' : '' }}">
            <a href="#pageSubmenu1" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
               <img alt="" src="{{asset('images/ic_menu_color.png')}}" class="color">
                <img alt="" src="{{asset('images/ic_menu.png')}}" class="selected">
                {{trans('sidebar.sidebar_nav_user_management')}}
                <i class="cfa fas fa-chevron-down right-arrow"></i>
            </a>
            <ul class="collapse list-unstyled {{ (request()->is('employee/subadmins*')) || (request()->is('employee/employees*')) || (request()->is('employee/engage-employee*')) || (request()->is('employee/students*')) || (request()->is('employee/enrollstudents*')) ? 'show' : '' }}" id="pageSubmenu1">
                <li class="{{ (request()->is('employee/subadmins*')) ? 'active' : '' }}">
                    <a href="{{url('/employee/subadmins')}}">{{trans('sidebar.sidebar_nav_admin_staff')}}</a>
                </li>
                <li class="{{ (request()->is('employee/employees*')) || (request()->is('employee/engage-employee*')) ? 'active' : '' }}">
                    <a href="{{url('/employee/employees')}}">{{trans('sidebar.sidebar_nav_employees')}}</a>
                </li>
                <li class="{{ (request()->is('employee/students*')) || (request()->is('employee/enrollstudents*')) ? 'active' : '' }}">
                    <a href="{{url('/employee/students')}}">{{trans('sidebar.sidebar_nav_students')}}</a>
                </li>
            </ul>
        </li>
        <li class="{{ (request()->is('employee/classcreation*')) || (request()->is('employee/classcreation/view-subclass*')) || (request()->is('employee/class-creation-semester*')) || (request()->is('employee/view-homeroomteacher*')) || (request()->is('employee/finalmarks*')) || (request()->is('employee/view-student-detail*')) || (request()->is('employee/coursegroup*')) || (request()->is('employee/courseorders*')) ? 'active' : '' }}">
            <a href="#pageSubmenu2" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                <img alt="" src="{{asset('images/ic_collaboration_color.png')}}" class="color">
               <img alt="" src="{{asset('images/ic_collaboration.png')}}" class="selected">
                {{trans('sidebar.sidebar_nav_organization')}}
                <i class="cfa fas fa-chevron-down right-arrow"></i>
            </a>
            <ul class="collapse list-unstyled {{ (request()->is('employee/classcreation*')) || (request()->is('employee/classcreation/view-subclass*')) || (request()->is('employee/class-creation-semester*')) || (request()->is('employee/view-homeroomteacher*')) || (request()->is('employee/finalmarks*')) || (request()->is('employee/view-student-detail*')) || (request()->is('employee/coursegroup*')) || (request()->is('employee/courseorders*')) ? 'show' : '' }}" id="pageSubmenu2">
                <li class="{{ (request()->is('employee/classcreation*')) || (request()->is('employee/classcreation/view-subclass*')) || (request()->is('employee/class-creation-semester*')) || (request()->is('employee/view-homeroomteacher*')) || (request()->is('employee/finalmarks*')) || (request()->is('employee/view-student-detail*'))? 'active' : '' }}">
                    <a href="{{url('/employee/classcreation')}}">{{trans('sidebar.sidebar_nav_class_creation')}}</a>
                </li>
                <li class="{{ (request()->is('employee/coursegroup*')) ? 'active' : '' }}">
                    <a href="{{url('/employee/coursegroup')}}">{{trans('sidebar.sidebar_nav_course_groups')}}</a>
                </li>
                <li class="{{ (request()->is('employee/courseorders*')) ? 'active' : '' }}">
                    <a href="{{url('/employee/courseorders')}}">{{trans('sidebar.sidebar_nav_course_orders')}}</a>
                </li>
                <li>
                    <a href="#">{{trans('sidebar.sidebar_nav_certification')}}</a>
                </li>
            </ul>
        </li>
        <li>
            <a href="#">
               <img alt="" src="{{asset('images/ic_test-results_color.png')}}" class="color">
               <img alt="" src="{{asset('images/ic_test-results.png')}}" class="selected">
                {{trans('sidebar.sidebar_nav_exam_result')}}
            </a>
        </li>
        <li>
            <a href="#">
               <img alt="" src="{{asset('images/ic_attendant-list_color.png')}}" class="color">
               <img alt="" src="{{asset('images/ic_attendant-list.png')}}" class="selected">
                {{trans('sidebar.sidebar_nav_attendance')}}
            </a>
        </li>
        <li>
            <a href="#">
               <img alt="" src="{{asset('images/ic_reports_color.png')}}" class="color">
               <img alt="" src="{{asset('images/ic_reports.png')}}" class="selected">
                {{trans('sidebar.sidebar_nav_report')}}
            </a>
        </li>
        <li>
            <a href="#">
                <img alt="" src="{{asset('images/ic_logs_color.png')}}" class="color">
                <img alt="" src="{{asset('images/ic_logs.png')}}" class="selected">
                {{trans('sidebar.sidebar_nav_logs')}}
            </a>
        </li>
    </ul>
    <div class="bottom-link">
      <a href="#x" class="help-link"><img alt="" src="{{ asset('images/ic_comment.png')}}" >{{trans('general.gn_help')}}?</a>
      <a href="#x" class="logout"><img alt="" src="{{ asset('images/ic_logout.png')}}" ></a>
    </div>


</nav>