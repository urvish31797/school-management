@extends('layout.app_with_login')
@section('title', trans('sidebar.sidebar_nav_dasbhoard'))
@section('content')
<!-- Page Content  -->
<div class="section pt-0">
	<div class="section_search">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-3">
					<div class="form-group section_search_group section_search_group_small">
						<label>{{trans('general.gn_semester')}}</label>

						<select onchange='roleEmployeeSwitch()' class="form-control icon_control dropdown_control" id="sem_drop_down">
						@if(!empty($EducationPeriod))
							@foreach($EducationPeriod as $k => $v)
								<option @if($v->pkEdp == $defaultSemester) selected="selected" @endif value="{{$v->pkEdp}}">{{$v->edp_EducationPeriodName}}</option>
							@endforeach
						@endif
						</select>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group section_search_group">
						<label>{{trans('general.gn_allocated_class')}}</label>
						<select onchange='roleEmployeeSwitch()' class="form-control icon_control dropdown_control" id="class_drop_down">
						@if(!empty($classesList))
							@foreach($classesList as $k => $v)
								@if($v['isBreak'])
								<option class="text-center" value="" disabled="disabled">
									{{$v['break_label']}}
								</option>
								@else
									@if(isset($coursedata) && !empty($coursedata))
										<option value="{{$v['classId']}}" data-label="{{$v['courseName']}}" data-courseid="{{$v['courseId']}}" data-coursegroup="{{$v['coursegroupId']}}" data-subclass="{{$v['subclassId']}}" data-ccsgroups="{{$v['ccsGroups']}}" data-villageschool="{{$v['villageschoolId']}}"
										@if($coursedata['classId'] == $v['classId'] && $coursedata['courseId'] == $v['courseId'] && $coursedata['coursegroupId'] == $v['coursegroupId'] && $coursedata['subclassId'] == $v['subclassId'] && $coursedata['villageschoolId'] == $v['villageschoolId'])
											selected
										@endif>
										{{$v['courseName']}}
										</option>
									@else
										<option value="{{$v['classId']}}" data-courseid="{{$v['courseId']}}" data-coursegroup="{{$v['coursegroupId']}}" data-subclass="{{$v['subclassId']}}" data-ccsgroups="{{$v['ccsGroups']}}" data-villageschool="{{$v['villageschoolId']}}" @if($k == 0) selected @endif>{{$v['courseName']}}</option>
									@endif
								@endif
							@endforeach
						@else
							<option value="0">{{trans('message.msg_no_data_available')}}</option>
						@endif
						</select>
					</div>
				</div>
				<div class="col-md-3">
					<div class="form-group section_search_group section_search_group_small">
						<label>{{trans('general.gn_shift')}}</label>
						<select onchange='roleEmployeeSwitch()' class="form-control icon_control dropdown_control" id="shift_dropdown">
						@if(!empty($Shifts))
							@foreach($Shifts as $k => $v)
								<option @if(isset($shiftid) && $shiftid == $v->pkShi) selected @endif value="{{$v->pkShi}}">{{$v->shi_ShiftName}}</option>
							@endforeach
						@endif
						</select>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				<h2 class="title">{{trans('sidebar.sidebar_nav_dasbhoard')}}</h2>
			</div>
		</div>
	</div>
</div>
@endsection