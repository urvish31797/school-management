@extends('layout.app_with_login')
@section('title', trans('sidebar.sidebar_nav_dasbhoard'))
@section('content')
<!-- Page Content  -->
<div class="section pt-0">
	<div class="section_search">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-3">
					<div class="form-group section_search_group section_search_group_small">
						<label>{{trans('general.gn_semester')}}</label>
						<select onchange='roleEmployeeSwitch()' class="form-control icon_control dropdown_control" id="sem_drop_down">
						@if(!empty($EducationPeriod))
							@foreach($EducationPeriod as $k => $v)
								<option @if($v->pkEdp == $defaultSemester) selected="selected" @endif value="{{$v->pkEdp}}">{{$v->edp_EducationPeriodName}}</option>
							@endforeach
						@endif
						</select>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group section_search_group">
						<label>{{trans('general.gn_allocated_class')}}</label>
						<input class="form-control" value="{{$class}}" type="text" disabled="disabled">
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				<h2 class="title">{{trans('sidebar.sidebar_nav_dasbhoard')}}</h2>
			</div>
		</div>
	</div>
</div>
@endsection