@extends('layout.app_with_login')
@section('title', trans('sidebar.sidebar_nav_employees'))
@section('script', asset('js/dashboard/employee.js'))
@section('content')
 <!-- Page Content  -->
<div class="section">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12 mb-3">
               <h2 class="title"><span>{{trans('sidebar.sidebar_nav_user_management')}} > </span><a class="no_sidebar_active" href="{{url('/employee/employees')}}"><span>{{trans('sidebar.sidebar_nav_employees')}} > </span></a> {{trans('general.gn_add')}}</h2>
            </div>
        </div>

        <form name='add-teacher-form'>
        <div class="white_box">
            <div class="theme_tab">
                <ul class="nav nav-tabs" id="myTab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">{{trans('general.gn_general_information')}}</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="Current-tab" data-toggle="tab" href="#Current" role="tab" aria-controls="Current" aria-selected="true">{{trans('general.gn_add_engagement')}}</a>
                    </li>
                </ul>
                <div class="tab-content" id="myTabContent">
                    <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                        <div class="inner_tab" id="profile_detail">
                          <input id="eid" type="hidden" value="">
                            <input type="hidden" id="sid" value="{{$mainSchool}}">
                            <div class="row">
                                <div class="col-lg-1"></div>
                                <div class="col-lg-10">
                                    <div class="text-center">
                                        <div class="profile_box">
                                            <div class="profile_pic">
                                                <img id="user_img" src="{{ asset('images/user.png') }}">
                                                <input type="hidden" id="img_tmp" value="{{ asset('images/user.png') }}">
                                            </div>
                                        </div>
                                        <div  class="upload_pic_link">
                                            <a href="javascript:void(0)">
                                            {{trans('general.gn_upload_photo')}}<input type="file" id="upload_profile" name="upload_profile" accept="image/jpeg,image/png"></a>

                                        </div>
                                    </div>
                                    <input type="hidden" id="image_validation_msg" value="{{trans('message.msg_image_validation')}}">

                                    <div class="">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>{{trans('general.gn_first_name')}} *</label>
                                                    <input type="text" id="emp_EmployeeName" name="emp_EmployeeName" class="form-control" placeholder="{{trans('general.gn_enter')}} {{trans('general.gn_first_name')}}">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>{{trans('general.gn_last_name')}}  *</label>
                                                    <input type="text" id="emp_EmployeeSurname" name="emp_EmployeeSurname" class="form-control" placeholder="{{trans('general.gn_enter')}} {{trans('general.gn_last_name')}}">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>{{trans('general.gn_email')}} *</label>
                                                    <input type="text" name="email" id="email" class="form-control" placeholder="{{trans('general.gn_enter')}} {{trans('general.gn_email')}}">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>{{trans('general.gn_phone')}}</label>
                                                    <input type="number" name="emp_PhoneNumber" id="emp_PhoneNumber" class="form-control" placeholder="{{trans('general.gn_enter')}} {{trans('general.gn_phone_number')}}">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>{{trans('general.gn_gender')}}</label>
                                                    <select onchange="checkEmployeeID()" name="emp_EmployeeGender" id="emp_EmployeeGender" class="form-control icon_control dropdown_control">
                                                      <option value="Male">{{trans('general.gn_male')}}</option>
                                                      <option value="Female">{{trans('general.gn_female')}}</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>{{trans('general.gn_dob')}} *</label>
                                                    <input onchange="checkEmployeeID()" type="text" name="emp_DateOfBirth" id="emp_DateOfBirth" class="form-control icon_control date_control datepicker">
                                                </div>
                                            </div>
                                            <div class="col-md-6 opt_id">
                                                <div class="form-group">
                                                    <label>{{trans('general.gn_employee')}} ID *</label>
                                                    <input onfocusout="checkEmployeeID()" type="text" name="emp_EmployeeID" id="emp_EmployeeID" class="form-control numbersonly isValidEmpID" placeholder="{{trans('general.gn_enter')}} ID" maxlength="13">
                                                </div>
                                            </div>
                                            <div class="col-md-6 opt_tmp_id hide_content">
                                                <div class="form-group">
                                                    <label>{{trans('general.gn_temp_citizen_id')}} </label>
                                                    <input required="required" type="text" name="emp_TempCitizenId" id="emp_TempCitizenId" class="form-control numbersonly isValidTempID" placeholder="{{trans('general.gn_enter')}} {{trans('general.gn_temp_citizen_id')}}" maxlength="13">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>{{trans('general.gn_place_of_birth')}} *</label>
                                                    <input type="text" name="emp_PlaceOfBirth" id="emp_PlaceOfBirth" class="form-control" placeholder="{{trans('general.gn_enter')}} {{trans('general.gn_place_of_birth')}}">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                              <div class="form-group form-check">
                                                  <input type="checkbox" class="form-check-input" id="havent_identification_number">
                                                  <label class="custom_checkbox"></label>
                                                  <label class="form-check-label label-text" for="exampleCheck1">{{trans('general.gn_havent_identification_number')}}</label>
                                              </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                <label>{{trans('general.gn_MunicipalityofBirth')}} *</label>
                                                    <select name="fkEmpMun" id="fkEmpMun" class="form-control icon_control dropdown_control">
                                                      <option value="">{{trans('general.gn_select')}}</option>
                                                      @foreach($Municipalities as $k => $v)
                                                        <option data-countryId="{{$v->canton->state->country->pkCny}}" value="{{$v->pkMun}}">{{$v->mun_MunicipalityName}}, {{$v->canton->state->country->cny_CountryName}}</option>
                                                      @endforeach
                                                    </select>
                                                    <input type="hidden" id="fkEmpCny" name="fkEmpCny">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>{{trans('general.gn_nationality')}} *</label>
                                                    <select name="fkEmpNat" class="form-control icon_control dropdown_control">
                                                      <option value="">{{trans('general.gn_select')}}</option>
                                                      @foreach($Nationalities as $k => $v)
                                                        <option value="{{$v->pkNat}}">{{$v->nat_NationalityName}}</option>
                                                      @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>{{trans('general.gn_religion')}} *</label>
                                                    <select name="fkEmpRel" class="form-control icon_control dropdown_control">
                                                      <option value="">{{trans('general.gn_select')}}</option>
                                                      @foreach($Religions as $k => $v)
                                                        <option value="{{$v->pkRel}}">{{$v->rel_ReligionName}}</option>
                                                      @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>{{trans('general.gn_citizenship')}} *</label>
                                                    <select name="fkEmpCtz" class="form-control icon_control dropdown_control">
                                                      <option value="">{{trans('general.gn_select')}}</option>
                                                      @foreach($Citizenships as $k => $v)
                                                        <option value="{{$v->pkCtz}}">{{$v->ctz_CitizenshipName}}</option>
                                                      @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>{{trans('general.gn_ResidencePostoffice')}} *</label>
                                                    <select name="fkEmpPof" class="form-control icon_control dropdown_control">
                                                      <option value="">{{trans('general.gn_select')}}</option>
                                                      @foreach($PostalCodes as $k => $value)
                                                        <option value="{{$value->pkPof}}">{{$value->pof_PostOfficeName}} {{$value->pof_PostOfficeNumber}} ({{$value->municipality->mun_MunicipalityName}}, {{$value->municipality->canton->state->country->cny_CountryName}})</option>
                                                      @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>{{trans('general.gn_address')}}</label>
                                                    <input type="text" name="emp_Address" class="form-control" placeholder="{{trans('general.gn_enter')}} {{trans('general.gn_address')}}">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>{{trans('general.gn_note')}}</label>
                                                    <input type="text" name="emp_Notes" class="form-control" placeholder="{{trans('general.gn_enter')}} {{trans('general.gn_note')}}">
                                                </div>
                                            </div>
                                            <div class="row col-md-12">

                                                <div class="col-md-2"></div>
                                                <div class="col-md-8 profile_de_details_add">

                                                    <div class="text-center">
                                                      <button class="theme_btn" id="add_qa" type="button">{{trans('general.gn_add')}} {{trans('general.gn_qualification')}}</button>
                                                    </div>

                                                </div>
                                                <div class="col-md-2"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <input type="hidden" id="msg_add_engagemnet_field" value="{{ trans('message.msg_add_engagement')}}">
                                    <div class="text-center">
                                         <div class="text-center">
                                            <button type="submit" class="theme_btn">{{trans('general.gn_submit')}}</button>
                                            <a class="theme_btn red_btn no_sidebar_active" href="{{url('/employee/employees')}}">{{trans('general.gn_cancel')}}</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-1"></div>
                            </div>
                        </div>

                    </div>
                    <div class="tab-pane fade show" id="Current" role="tabpanel" aria-labelledby="Current-tab">
                        <div class="inner_tab">
                        <!-- Add Engagement element -->
                        <div class="row" id="profile_eng_details" style="display: none;">
                              <div class="col-md-2"></div>
                              <div class="col-md-8 container-fluid new_emp_eng">
                                <div class="row">
                                <div class="col-12 text-right">
                                    <img src="{{asset('images/ic_close_circle.png')}}" class="close_img rm_eng" data-eed="">
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>{{trans('general.gn_date_of_engagement')}} *</label>
                                        <input required type="text" id="start_date" name="start_date" class="form-control datepicker icon_control date_control">
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>{{trans('general.gn_employee_type')}} *</label>
                                        <select required id="fkEenEpty" name="fkEenEpty" class="form-control icon_control dropdown_control">
                                            <option value="">{{trans('general.gn_select')}}</option>
                                            @foreach($employeeType as $k => $v)
                                            @if($v->pkEpty!=6)
                                              <option value="{{$v->pkEpty}}">@if($v->epty_Name=='Principal') {{ trans('general.gn_principal')}} @elseif($v->epty_Name=='Teacher') {{ trans('general.gn_teacher')}} @endif</option>
                                            @endif
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group form-check" id="principal_checkbox" style="display:none">
                                        <input name="een_ActingAsPrincipal" type="checkbox" class="form-check-input" id="een_ActingAsPrincipal">
                                        <label class="custom_checkbox"></label>
                                        <label class="form-check-label" for="exampleCheck1">{{trans('general.gn_acting_principal')}}</label>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>{{trans('general.gn_type_of_engagement')}} *</label>
                                        <select required id="fkEenEty" name="fkEenEty" class="form-control icon_control dropdown_control">
                                            <option value="">{{trans('general.gn_select')}}</option>
                                            @foreach($EngagementTypes as $k => $v)
                                              <option value="{{$v->pkEty}}">{{$v->ety_EngagementTypeName}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>{{trans('general.gn_week_hourly_rate')}} *</label>
                                        <input required type="text" id="ewh_WeeklyHoursRate" name="ewh_WeeklyHoursRate" class="form-control">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                      <label>{{trans('general.gn_hourly_note')}} </label>
                                      <input type="text" name="ewh_Notes" id="ewh_Notes" class="form-control">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>{{trans('general.gn_enagement_note')}} </label>
                                        <input type="text" id="een_Notes" name="een_Notes" class="form-control">
                                    </div>
                                </div>
                              </div>
                            </div>
                            <div class="col-md-2"></div>

                        </div>
                            <div class="text-center add_eng_btn">
                              <button class="theme_btn min_btn" id="add_eng" type="button">{{trans('general.gn_add')}}</button>
                            </div>
                          <div class="col-md-12">
                              <div class="form-group">
                                  <div class="card">
                                    <div class="card-body">
                                      <b>{{trans('general.gn_note')}}:</b> {{trans('general.gn_end_date_note')}}
                                    </div>
                                  </div>
                              </div>
                          </div>

                          <div class="col-md-12">
                            <div class="text-center">
                                 <div class="text-center">
                                    <button type="submit" class="theme_btn">{{trans('general.gn_submit')}}</button>
                                    <a class="theme_btn red_btn no_sidebar_active" href="{{url('/employee/employees')}}">{{trans('general.gn_cancel')}}</a>
                                </div>
                            </div>
                          </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </form>
    </div>

    <!-- Add Qualification element -->
    <div id="profile_de_details" style="display: none;">
      <div class="profile_info_container">
        <div class="row">
          <div class="col-12 text-right">
            <img src="{{asset('images/ic_close_circle.png')}}" class="close_img rm_ed">
          </div>
          <div class="form-group col-md-6">
            <label>{{trans('general.gn_university')}} :*</label>
            <select id="fkEedUni" required onchange="fetchCollege(this)" name="fkEedUni" class="form-control icon_control dropdown_control">
              <option value="">{{trans('general.gn_select')}}</option>
              @foreach($Universities as $k =>$v)
                <option value="{{$v->pkUni}}">{{$v->uni_UniversityName}}</option>
              @endforeach
            </select>
          </div>
          <div class="form-group col-md-6">
            <label>{{trans('general.gn_faculty')}} ({{trans('general.gn_college')}}) :*</label>
            <select id="fkEedCol" required name="fkEedCol" class="form-control icon_control dropdown_control college_sel">
              <option value="">{{trans('general.gn_select')}}</option>

            </select>
          </div>
          <div class="form-group col-md-6">
            <label>{{trans('general.gn_academic_degree')}} :*</label>
            <select id="fkEedAcd" required name="fkEedAcd" class="form-control icon_control dropdown_control">
              <option value="">{{trans('general.gn_select')}}</option>
              @foreach($AcademicDegrees as $k =>$v)
                <option value="{{$v->pkAcd}}">{{$v->acd_AcademicDegreeName}}</option>
              @endforeach
            </select>
          </div>
          <div class="form-group col-md-6">
            <label>{{trans('general.gn_qualification_degree')}} :*</label>
            <select id="fkEedQde" required name="fkEedQde" class="form-control icon_control dropdown_control">
              <option value="">{{trans('general.gn_select')}}</option>
              @foreach($QualificationDegrees as $k =>$v)
                <option value="{{$v->pkQde}}">{{$v->qde_QualificationDegreeName}}</option>
              @endforeach
            </select>
          </div>
          <div class="form-group col-md-6">
            <label>{{trans('general.gn_designation')}} :*</label>
            <select required id="fkEedEde" name="fkEedEde" class="form-control icon_control dropdown_control">
              <option value="">{{trans('general.gn_select')}}</option>
              @foreach($EmployeeDesignations as $k =>$v)
                <option value="{{$v->pkEde}}">{{$v->ede_EmployeeDesignationName}}</option>
              @endforeach
            </select>
          </div>
          <div class="form-group col-md-6">
            <label>{{trans('general.gn_year_of_passing')}} :*</label>
            <input required class="form-control datepicker-year date_control icon_control" type="text" id="eed_YearsOfPassing" name="eed_YearsOfPassing">
          </div>
          <div class="form-group col-md-6">
            <label>{{trans('general.gn_short_title')}} :*</label>
            <input required class="form-control" type="text" id="eed_ShortTitle" name="eed_ShortTitle">
          </div>
          <div class="form-group col-md-6">
            <label>{{trans('general.gn_number_of_semesters')}} :*</label>
            <input required class="form-control" type="number" id="eed_SemesterNumbers" name="eed_SemesterNumbers">
          </div>
          <div class="form-group col-md-6">
            <label>{{trans('general.gn_ect_points')}} :*</label>
            <input required class="form-control" type="number" id="eed_EctsPoints" name="eed_EctsPoints">
          </div>

          <div class="form-group col-md-6">
            <label>{{trans('general.gn_document')}} :*</label>
            <div class="upload_file">
              <input type="text" id="file_name" name="file_name" value="{{trans('general.gn_upload')}}" class="form-control">
              <input class="diploma_file" required type="file" id="eed_DiplomaPicturePath" name="eed_DiplomaPicturePath" accept="application/pdf,image/jpeg,image/png" />
            </div>
          </div>

          <div class="form-group col-md-6">
            <label>{{trans('general.gn_note')}} :</label>
            <input class="form-control" type="text" id="eed_Notes" name="eed_Notes">
          </div>
        </div>
      </div>
    </div>
    <input type="hidden" id="add_qualification_txt" value="{{trans('message.msg_please_add_qualification')}}">
    <input type="hidden" id="please_fillup_required_fields" value="{{trans('message.please_fillup_required_fields')}}">
    <input type="hidden" id="employeemsgerror" value="{{trans('message.msg_employee_id_incorrect')}}">
</div>
<script>
    @if($logged_user->type=='HertronicAdmin' || $logged_user->type=='MinistryAdmin')
        var listing_url = "{{route('fetch-employees-lists')}}";
    @else
        var listing_url = "{{route('fetch-staff-lists')}}";
    @endif
</script>
@endsection

@push('datatable-scripts')
<!-- Include this Page JS -->
<script type="text/javascript" src="{{ asset('js/dashboard/employee.js') }}"></script>
@endpush