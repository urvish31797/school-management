@extends('layout.app_with_login')
@section('title', trans('sidebar.sidebar_nav_employees'))
@section('script', asset('js/dashboard/employee.js'))
@section('content')
 <!-- Page Content  -->
<div class="section">
    <div class="container-fluid">
        <div class="row">
            @if($logged_user->type=='HertronicAdmin' || $logged_user->type=='MinistryAdmin') <input type="hidden" id="is_admin" value="1"> @endif
            <div class="col-12 mb-3">
            @if($logged_user->type=='MinistryAdmin')
                <h2 class="title"><a class="no_sidebar_active" href="{{url('/admin/employees')}}"><span>{{trans('sidebar.sidebar_nav_employees')}} > </span></a> {{trans('general.gn_edit')}}</h2>
            @else
               <h2 class="title"><span>@if(Request::is('employee/*')){{trans('sidebar.sidebar_nav_user_management')}} > </span><a class="no_sidebar_active" href="{{url('/employee/employees')}}"> @else {{trans('sidebar.sidebar_nav_ministry_masters')}} > </span><a class="no_sidebar_active" href="{{url('/admin/employees')}}"> @endif <span>{{trans('sidebar.sidebar_nav_employees')}} > </span></a> {{trans('general.gn_edit')}}</h2>
            @endif
            </div>
        </div>
            <div class="white_box">
                <div class="theme_tab">
                    <ul class="nav nav-tabs" id="myTab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">{{trans('general.gn_general_information')}}</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="Current-tab" data-toggle="tab" href="#Current" role="tab" aria-controls="Current" aria-selected="true">{{trans('general.gn_edit_engagement')}}</a>
                        </li>
                    </ul>

                    <div class="tab-content" id="myTabContent">
                        <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                            <form name='add-teacher-form'>
                            <div class="inner_tab" id="profile_detail">
                                <input type="hidden" id="sid" value="{{$mainSchool}}">
                                <input id="eid" type="hidden" value="{{$EmployeesDetail->id}}">
                                <input id="engid" type="hidden" value="{{$EmployeesDetail->pkEen}}">
                                <div class="row">
                                    <div class="col-lg-1"></div>
                                    <div class="col-lg-10">
                                        <div class="text-center">
                                            <div class="profile_box">
                                                <div class="profile_pic">
                                                    <img id="user_img" src="@if(!empty($EmployeesDetail->emp_PicturePath)) {{asset('images/users/')}}/{{$EmployeesDetail->emp_PicturePath}} @else {{ asset('images/user.png') }}@endif">
                                                    <input type="hidden" id="img_tmp" value="{{ asset('images/user.png') }}">
                                                </div>
                                            </div>
                                            <div  class="upload_pic_link">
                                                <a href="javascript:void(0)">
                                                {{trans('general.gn_upload_photo')}}<input type="file" id="upload_profile" name="upload_profile" accept="image/jpeg,image/png"></a>

                                            </div>
                                        </div>
                                        <input type="hidden" id="image_validation_msg" value="{{trans('message.msg_image_validation')}}">
                                        <div class="">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>{{trans('general.gn_first_name')}} *</label>
                                                        <input type="text" name="emp_EmployeeName" class="form-control" placeholder="{{trans('general.gn_enter')}} {{trans('general.gn_first_name')}}" value="{{$EmployeesDetail->emp_EmployeeName}}">
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                  <div class="form-group">
                                                      <label>{{trans('general.gn_last_name')}}  *</label>
                                                      <input type="text" id="emp_EmployeeSurname" name="emp_EmployeeSurname" class="form-control" placeholder="{{trans('general.gn_enter')}} {{trans('general.gn_last_name')}}" value="{{$EmployeesDetail->emp_EmployeeSurname}}">
                                                  </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>{{trans('general.gn_email')}} *</label>
                                                        <input type="text" name="email" id="email" class="form-control" placeholder="{{trans('general.gn_enter')}} {{trans('general.gn_email')}}" value="{{$EmployeesDetail->email}}">
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>{{trans('general.gn_phone')}}</label>
                                                        <input type="number" name="emp_PhoneNumber" id="emp_PhoneNumber" class="form-control" placeholder="{{trans('general.gn_enter')}} {{trans('general.gn_phone_number')}}" value="{{$EmployeesDetail->emp_PhoneNumber}}">
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>{{trans('general.gn_gender')}}</label>
                                                        <select onchange="checkEmployeeID()" name="emp_EmployeeGender" id="emp_EmployeeGender" class="form-control icon_control dropdown_control">
                                                          <option @if($EmployeesDetail->emp_EmployeeGender == 'Male') selected @endif value="Male">{{trans('general.gn_male')}}</option>
                                                          <option @if($EmployeesDetail->emp_EmployeeGender == 'Female') selected @endif value="Female">{{trans('general.gn_female')}}</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>{{trans('general.gn_dob')}} *</label>
                                                        <input onchange="checkEmployeeID()" type="text" name="emp_DateOfBirth" id="emp_DateOfBirth" class="form-control icon_control date_control datepicker" value="@if(!empty($EmployeesDetail->emp_DateOfBirth)){{date('d/m/Y',strtotime($EmployeesDetail->emp_DateOfBirth))}}@endif">
                                                    </div>
                                                </div>
                                                <div class="col-md-6 opt_id @if(empty($EmployeesDetail->emp_EmployeeID)) hide_content @endif">
                                                    <div class="form-group">
                                                        <label>{{trans('general.gn_employee')}} ID *</label>
                                                        <input onfocusout="checkEmployeeID()" type="text" name="emp_EmployeeID" id="emp_EmployeeID" class="form-control numbersonly isValidEmpID" placeholder="{{trans('general.gn_enter')}} ID" value="{{$EmployeesDetail->emp_EmployeeID}}" maxlength="13">
                                                    </div>
                                                </div>
                                                <div class="col-md-6 opt_tmp_id @if(empty($EmployeesDetail->emp_TempCitizenId)) hide_content @endif"">
                                                    <div class="form-group">
                                                        <label>{{trans('general.gn_temp_citizen_id')}} </label>
                                                        <input type="text" name="emp_TempCitizenId" id="emp_TempCitizenId" class="form-control numbersonly isValidTempID" placeholder="{{trans('general.gn_enter')}} {{trans('general.gn_temp_citizen_id')}}" value="{{$EmployeesDetail->emp_TempCitizenId}}" maxlength="13">
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>{{trans('general.gn_place_of_birth')}} *</label>
                                                        <input type="text" name="emp_PlaceOfBirth" id="emp_PlaceOfBirth" class="form-control" placeholder="{{trans('general.gn_enter')}} {{trans('general.gn_place_of_birth')}}" value="{{$EmployeesDetail->emp_PlaceOfBirth}}">
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                  <div class="form-group form-check">
                                                      <input type="checkbox" @if(!empty($EmployeesDetail->emp_TempCitizenId)) checked @endif class="form-check-input" id="havent_identification_number">
                                                      <label class="custom_checkbox"></label>
                                                      <label class="form-check-label label-text" for="exampleCheck1">{{trans('general.gn_havent_identification_number')}}</label>
                                                  </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                    <label>{{trans('general.gn_MunicipalityofBirth')}} *</label>
                                                        <select name="fkEmpMun" id="fkEmpMun" class="form-control icon_control dropdown_control select2drp">
                                                          <option value="">{{trans('general.gn_select')}}</option>
                                                          @foreach($Municipalities as $k => $v)
                                                            <option data-countryId="{{$v->canton->state->country->pkCny}}" @if($EmployeesDetail->fkEmpMun == $v->pkMun) selected @endif value="{{$v->pkMun}}">{{$v->mun_MunicipalityName}}, {{$v->canton->state->country->cny_CountryName}}</option>
                                                          @endforeach
                                                        </select>
                                                        <input type="hidden" id="fkEmpCny" name="fkEmpCny" value="{{$EmployeesDetail->fkEmpCny}}">
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>{{trans('general.gn_nationality')}} *</label>
                                                        <select name="fkEmpNat" class="form-control icon_control dropdown_control">
                                                          <option value="">{{trans('general.gn_select')}}</option>
                                                          @foreach($Nationalities as $k => $v)
                                                            <option @if($EmployeesDetail->fkEmpNat == $v->pkNat) selected @endif value="{{$v->pkNat}}">{{$v->nat_NationalityName}}</option>
                                                          @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>{{trans('general.gn_religion')}} *</label>
                                                        <select name="fkEmpRel" class="form-control icon_control dropdown_control">
                                                          <option value="">{{trans('general.gn_select')}}</option>
                                                          @foreach($Religions as $k => $v)
                                                            <option @if($EmployeesDetail->fkEmpRel == $v->pkRel) selected @endif value="{{$v->pkRel}}">{{$v->rel_ReligionName}}</option>
                                                          @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>{{trans('general.gn_citizenship')}} *</label>
                                                        <select name="fkEmpCtz" class="form-control icon_control dropdown_control">
                                                          <option value="">{{trans('general.gn_select')}}</option>
                                                          @foreach($Citizenships as $k => $v)
                                                            <option @if($EmployeesDetail->fkEmpCtz == $v->pkCtz) selected @endif value="{{$v->pkCtz}}">{{$v->ctz_CitizenshipName}}</option>
                                                          @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                    <label>{{trans('general.gn_ResidencePostoffice')}} *</label>
                                                        <select name="fkEmpPof" class="form-control icon_control dropdown_control">
                                                          <option value="">{{trans('general.gn_select')}}</option>
                                                          @foreach($PostalCodes as $k => $value)
                                                            <option @if($EmployeesDetail->fkEmpPof == $value->pkPof) selected @endif value="{{$value->pkPof}}">{{$value->pof_PostOfficeName}} {{$value->pof_PostOfficeNumber}} ({{$value->municipality->mun_MunicipalityName}}, {{$value->municipality->canton->state->country->cny_CountryName}})</option>
                                                          @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>{{trans('general.gn_address')}}</label>
                                                        <input type="text" name="emp_Address" class="form-control" placeholder="{{trans('general.gn_enter')}} {{trans('general.gn_address')}}" value="{{$EmployeesDetail->emp_Address}}">
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>{{trans('general.gn_status')}} *</label>
                                                        <select class="form-control dropdown_control icon_control" name="emp_Status" id="emp_Status">
                                                            <option value="">{{trans('general.gn_select')}}</option>
                                                            <option @if($EmployeesDetail->emp_Status == 'Active') selected @endif value="Active">{{trans('general.gn_active')}}</option>
                                                            <option @if($EmployeesDetail->emp_Status == 'Inactive') selected @endif value="Inactive">{{trans('general.gn_inactive')}}</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>{{trans('general.gn_note')}}</label>
                                                        <input type="text" name="emp_Notes" class="form-control" placeholder="{{trans('general.gn_enter')}} {{trans('general.gn_note')}}" value="{{$EmployeesDetail->emp_Notes}}">
                                                    </div>
                                                </div>
                                                <div class="row col-md-12">

                                                    <div class="col-md-2"></div>
                                                    <div class="col-md-8 profile_de_details_add">

                                                        @foreach($EmployeesDetail->employeeEducation as $ke => $ve)
                                                            <div class="profile_info_container">
                                                              <div class="row">
                                                                <div class="col-12 text-right">
                                                                  <img src="{{asset('images/ic_close_circle.png')}}" class="close_img rm_ed" data-eed="{{$ke+1}}">
                                                                </div>
                                                                <div class="form-group col-md-6">
                                                                  <label>{{trans('general.gn_university')}} :*</label>
                                                                  <select id="fkEedUni_{{$ke+1}}" required onchange="fetchCollege(this)" name="fkEedUni_{{$ke+1}}" class="form-control icon_control dropdown_control">
                                                                    <option value="">{{trans('general.gn_select')}}</option>
                                                                    @foreach($Universities as $k =>$v)
                                                                      <option @if($ve->university->pkUni == $v->pkUni) selected @endif value="{{$v->pkUni}}">{{$v->uni_UniversityName}}</option>
                                                                    @endforeach
                                                                  </select>
                                                                </div>
                                                                <div class="form-group col-md-6">
                                                                  <label>{{trans('general.gn_faculty')}} ({{trans('general.gn_college')}}) :*</label>
                                                                  <select id="fkEedCol_{{$ke+1}}" required name="fkEedCol_{{$ke+1}}" class="form-control icon_control dropdown_control college_sel">
                                                                    <option value="">{{trans('general.gn_select')}}</option>
                                                                    @foreach($ve->university->college as $k =>$v)
                                                                      <option @if($ve->college->pkCol == $v->pkCol) selected @endif value="{{$v->pkCol}}">{{$v['col_CollegeName_'.$current_language]}}</option>
                                                                    @endforeach
                                                                  </select>
                                                                </div>
                                                                <div class="form-group col-md-6">
                                                                  <label>{{trans('general.gn_academic_degree')}} :*</label>
                                                                  <select id="fkEedAcd_{{$ke+1}}" required name="fkEedAcd_{{$ke+1}}" class="form-control icon_control dropdown_control">
                                                                    <option value="">{{trans('general.gn_select')}}</option>
                                                                    @foreach($AcademicDegrees as $k =>$v)
                                                                      <option @if($ve->academicDegree->pkAcd == $v->pkAcd) selected @endif value="{{$v->pkAcd}}">{{$v->acd_AcademicDegreeName}}</option>
                                                                    @endforeach
                                                                  </select>
                                                                </div>
                                                                <div class="form-group col-md-6">
                                                                  <label>{{trans('general.gn_qualification_degree')}} :*</label>
                                                                  <select id="fkEedQde_{{$ke+1}}" required name="fkEedQde_{{$ke+1}}" class="form-control icon_control dropdown_control">
                                                                    <option value="">{{trans('general.gn_select')}}</option>
                                                                    @foreach($QualificationDegrees as $k =>$v)
                                                                      <option @if($ve->qualificationDegree->pkQde == $v->pkQde) selected @endif value="{{$v->pkQde}}">{{$v->qde_QualificationDegreeName}}</option>
                                                                    @endforeach
                                                                  </select>
                                                                </div>
                                                                <div class="form-group col-md-6">
                                                                  <label>{{trans('general.gn_designation')}} :*</label>
                                                                  <select required id="fkEedEde_{{$ke+1}}" name="fkEedEde_{{$ke+1}}" class="form-control icon_control dropdown_control">
                                                                    <option value="">{{trans('general.gn_select')}}</option>
                                                                    @foreach($EmployeeDesignations as $k =>$v)
                                                                      <option @if($ve->employeeDesignation->pkEde == $v->pkEde) selected @endif value="{{$v->pkEde}}">{{$v->ede_EmployeeDesignationName}}</option>
                                                                    @endforeach
                                                                  </select>
                                                                </div>
                                                                <div class="form-group col-md-6">
                                                                  <label>{{trans('general.gn_year_of_passing')}} :*</label>
                                                                  <input required class="form-control datepicker-year date_control icon_control" type="text" id="eed_YearsOfPassing_{{$ke+1}}" name="eed_YearsOfPassing_{{$ke+1}}" value="{{$ve->eed_YearsOfPassing}}">
                                                                </div>
                                                                <div class="form-group col-md-6">
                                                                  <label>{{trans('general.gn_short_title')}} :*</label>
                                                                  <input required class="form-control" type="text" id="eed_ShortTitle_{{$ke+1}}" name="eed_ShortTitle_{{$ke+1}}" value="{{$ve->eed_ShortTitle}}">
                                                                </div>
                                                                <div class="form-group col-md-6">
                                                                  <label>{{trans('general.gn_number_of_semesters')}} :*</label>
                                                                  <input required class="form-control" type="number" id="eed_SemesterNumbers_{{$ke+1}}" name="eed_SemesterNumbers_{{$ke+1}}" value="{{$ve->eed_SemesterNumbers}}">
                                                                </div>
                                                                <div class="form-group col-md-6">
                                                                  <label>{{trans('general.gn_ect_points')}} :*</label>
                                                                  <input required class="form-control" type="number" id="eed_EctsPoints_{{$ke+1}}" name="eed_EctsPoints_{{$ke+1}}" value="{{$ve->eed_EctsPoints}}">
                                                                </div>

                                                                <div class="form-group col-md-6">
                                                                  <label>{{trans('general.gn_document')}} :*</label>
                                                                  <div class="upload_file">
                                                                    <input type="text" id="file_name_{{$ke+1}}" name="file_name_{{$ke+1}}" class="form-control" value="{{$ve->eed_PicturePath}}">
                                                                    <input class="diploma_file" type="file" id="eed_DiplomaPicturePath_{{$ke+1}}" name="eed_DiplomaPicturePath_{{$ke+1}}" accept="application/pdf,image/jpeg,image/png" />
                                                                  </div>
                                                                </div>

                                                                <div class="form-group col-md-6">
                                                                  <label>{{trans('general.gn_note')}} :</label>
                                                                  <input class="form-control" type="text" id="eed_Notes_{{$ke+1}}" name="eed_Notes_{{$ke+1}}" value="{{$ve->eed_Notes}}">
                                                                </div>
                                                                <div class="form-group col-md-6 preview_exist_file">
                                                                  <label style="visibility: hidden; display: block;">{{trans('general.gn_note')}} :</label>
                                                                  <a class="theme_btn"  href="{{asset('files/users')}}/{{$ve->eed_PicturePath}}" target="_blank">{{trans('general.gn_preview')}}</a>
                                                                </div>
                                                              </div>
                                                            </div>
                                                        @endforeach

                                                        <div class="text-center">
                                                          <button class="theme_btn" id="add_qa" type="button">{{trans('general.gn_add')}} {{trans('general.gn_qualification')}}</button>
                                                        </div>

                                                    </div>
                                                    <div class="col-md-2"></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="text-center">
                                             <div class="text-center">
                                                <button type="submit" class="theme_btn">{{trans('general.gn_submit')}}</button>
                                                <a class="theme_btn red_btn no_sidebar_active" @if(Request::is('employee/editEmployee/*')) href="{{url('/employee/employees')}}" @else href="{{url('/admin/employees')}}" @endif>{{trans('general.gn_cancel')}}</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-1"></div>
                                </div>
                            </div>
                            </form>
                        </div>
                        <div class="tab-pane fade show" id="Current" role="tabpanel" aria-labelledby="Current-tab">
                            <div class="inner_tab" id="profile_detail3">
                                <div class="row">
                                    <div class="col-lg-1"></div>
                                    <div class="col-lg-10">
                                        <div class="profile_info_container">
                                            <div class="row">
                                              <div class="col-12">
                                                <h6 class="profile_inner_title">{{trans('general.gn_work_experience')}}</h6>
                                              </div>
                                              <div class="col-12">
                                                <div class="table-responsive">
                                                  <table class="profile_table">
                                                    <tbody>
                                                      @foreach($EmployeesDetail->EmployeesEngagement as $k => $v)
                                                      <tr>
                                                        <td>{{$k+1}}</td>
                                                        <td>
                                                          <p class="label">{{$v->school->sch_SchoolName ?? ''}} :</p>
                                                          <p class="value">
                                                              @if(isset($v->employeeType->epty_Name) && !empty($v->employeeType->epty_Name))
                                                                  @if($v->employeeType->epty_Name=='Principal') {{ trans('general.gn_principal')}}
                                                                  @elseif($v->employeeType->epty_Name=='Teacher') {{ trans('general.gn_teacher')}}
                                                                  @elseif($v->employeeType->epty_Name=='SchoolCoordinator') {{ trans('general.gn_school_coordinator')}}
                                                                  @elseif($v->employeeType->epty_Name=='HomeroomTeacher') {{ trans('general.gn_homeroom_teacher')}}
                                                                  @else
                                                                  @endif
                                                              @endif
                                                          </p>
                                                          <br>
                                                          <p class="label">{{trans('general.gn_type_of_engagement')}} :</p>
                                                          <p class="value">{{$v->engagementType->ety_EngagementTypeName ?? ''}}</p>
                                                        </td>
                                                        <td>
                                                          <p class="label">{{trans('general.gn_date_of_engagement')}} :</p>
                                                          <p class="value">{{$v->een_DateOfEngagement}}</p>
                                                          <br>
                                                          <p class="label">{{trans('general.gn_hourly_rate')}} :</p>
                                                          <p class="value">{{$v->getLatestHourRates[0]->ewh_WeeklyHoursRate ?? ''}}</p>
                                                        </td>
                                                        <td>
                                                          <p class="label">{{trans('general.gn_date_of_engagement_end')}} :</p>
                                                          <p class="value">@if(!empty($v->een_DateOfFinishEngagement)){{$v->een_DateOfFinishEngagement}} @else - @endif</p>
                                                          <br>
                                                          <p class="label">{{trans('general.gn_hourly_note')}} :</p>
                                                          <p class="value">{{$v->getLatestHourRates[0]->ewh_Notes ?? ''}}</p>
                                                        </td>
                                                      </tr>
                                                      @endforeach
                                                    </tbody>
                                                  </table>
                                                </div>
                                              </div>

                                            </div>
                                        </div>
                                        <div class="text-center">
                                          {{-- @if($logged_user->type == 'SchoolCoordinator' || $logged_user->type == 'HertronicAdmin') --}}
                                            <button type="button" class="theme_btn" id="add_eng">{{trans('general.gn_edit')}}</button>
                                          {{-- @endif --}}
                                        </div>
                                    </div>
                                    <div class="col-lg-1"></div>
                                </div>
                            </div>
                            <!-- Add Engagement element -->
                            <div id="profile_eng_details" style="display: none;">
                                <form name="edit_eng_form">
                                @foreach($EmployeesDetail->EmployeesEngagement as $k => $v)
                                <div class="row" id="profile_eng_details_{{$k+1}}">
                                    <div class="col-md-2"></div>
                                    <div class="col-md-8 container-fluid new_emp_eng">
                                        <div class="row">
                                            <div class="col-12 text-right">
                                                <img src="{{asset('images/ic_close_circle.png')}}" class="close_img rm_eng" data-eed="{{$k+1}}">
                                            </div>
                                            <input type="hidden" name="emp_engagment_id[]" id="emp_engagment_id{{ $v->pkEen }}" value="{{ $v->pkEen }}">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>{{trans('general.gn_date_of_enrollment')}} *</label>
                                                    <input required type="text" id="start_date" name="start_date[]" class="form-control datepicker icon_control date_control" value="{{$v->een_DateOfEngagement}}">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>{{trans('general.gn_date_of_engagement_end')}} </label>
                                                    <input @if($v->fkEenEpty == 6) readonly="readonly" @endif type="text" id="end_date{{ $v->pkEen }}" name="end_date[]" class="form-control datepicker icon_control date_control @if($v->fkEenEpty == 6) input_disable @endif" value="@if(!empty($v->een_DateOfFinishEngagement)){{$v->een_DateOfFinishEngagement}}@endif">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                  <label>{{trans('general.gn_type_of_engagement')}} *</label>
                                                  <select required id="fkEenEty" name="fkEenEty[]" class="form-control icon_control dropdown_control">
                                                    <option value="">{{trans('general.gn_select')}}</option>
                                                    @foreach($EngagementTypes as $k => $et_v)
                                                      <option @if($v->fkEenEty == $et_v->pkEty) selected @endif value="{{$et_v->pkEty}}">{{$et_v->ety_EngagementTypeName}}</option>
                                                    @endforeach
                                                  </select>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                  <label>{{trans('general.gn_employee_type')}} *</label>
                                                  @if($v->fkEenEpty != 1)
                                                  <input name="een_ActingAsPrincipal[]" type="hidden">
                                                  @endif
                                                  <select readonly="readonly" id="fkEenEpty" name="fkEenEpty[]" class="form-control icon_control dropdown_control input_disable">
                                                    <option value="">{{trans('general.gn_select')}}</option>
                                                    @foreach($employeeType as $k => $et_v)
                                                      <option @if($v->fkEenEpty == $et_v->pkEpty) selected @endif value="{{$et_v->pkEpty}}">@if($et_v->epty_Name=='SchoolCoordinator') {{ trans('general.gn_school_coordinator')}} @elseif($et_v->epty_Name=='Principal') {{ trans('general.gn_principal')}} @elseif($et_v->epty_Name=='Teacher') {{ trans('general.gn_teacher')}} @elseif($et_v->epty_Name=='HomeroomTeacher') {{ trans('general.gn_homeroom_teacher')}} @endif</option>
                                                    @endforeach
                                                  </select>
                                                </div>
                                                <div class="form-group form-check" id="principal_checkbox" @if($v->fkEenEpty != 1) style="display:none" @endif>
                                                    <input name="een_ActingAsPrincipal[]" type="checkbox" class="form-check-input" id="een_ActingAsPrincipal" @if($v->een_ActingAsPrincipal == "Yes") checked="checked" @endif>
                                                    <label class="custom_checkbox"></label>
                                                    <label class="form-check-label" for="exampleCheck1">{{trans('general.gn_acting_principal')}}</label>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                  <label>{{trans('general.gn_hourly_rate')}} *</label>
                                                  <input required type="number" name="ewh_WeeklyHoursRate[]" id="ewh_WeeklyHoursRate" class="form-control" value="{{$v->getLatestHourRates[0]->ewh_WeeklyHoursRate ?? ''}}">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                  <label>{{trans('general.gn_hourly_note')}} </label>
                                                  <input type="text" name="ewh_Notes[]" id="ewh_Notes{{ $v->pkEen }}" class="form-control" value="{{$v->getLatestHourRates[0]->ewh_Notes ?? ''}}">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                  <label>{{trans('general.gn_enagement_note')}} </label>
                                                  <input type="text" name="een_Notes[]" id="een_Notes{{ $v->pkEen }}" class="form-control" value="{{$v->een_Notes}}">
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                    <div class="col-md-2"></div>
                                </div>
                                @endforeach
                                <div class="col-md-12">
                                  <div class="form-group">
                                      <div class="card">
                                        <div class="card-body">
                                          <b>{{trans('general.gn_note')}}:</b> {{trans('general.gn_end_date_note')}}
                                        </div>
                                      </div>
                                  </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="text-center">
                                         <div class="text-center">
                                            <button type="submit" class="theme_btn">{{trans('general.gn_submit')}}</button>
                                            <button type="button" class="theme_btn red_btn engage_cancel">{{trans('general.gn_cancel')}}</button>
                                        </div>
                                    </div>
                                </div>
                                </form>
                            </div>
                        </div>

                        </div>
                        </div>
                    </div>
                </div>
            </div>

    </div>

    <!-- Add Qualification element -->
    <div id="profile_de_details" style="display: none;">
      <div class="profile_info_container">
        <div class="row">
          <div class="col-12 text-right">
            <img src="{{asset('images/ic_close_circle.png')}}" class="close_img rm_ed">
          </div>
          <div class="form-group col-md-6">
            <label>{{trans('general.gn_university')}} :*</label>
            <select id="fkEedUni" required onchange="fetchCollege(this)" name="fkEedUni" class="form-control icon_control dropdown_control">
              <option value="">{{trans('general.gn_select')}}</option>
              @foreach($Universities as $k =>$v)
                <option value="{{$v->pkUni}}">{{$v->uni_UniversityName}}</option>
              @endforeach
            </select>
          </div>
          <div class="form-group col-md-6">
            <label>{{trans('general.gn_faculty')}} ({{trans('general.gn_college')}}) :*</label>
            <select id="fkEedCol" required name="fkEedCol" class="form-control icon_control dropdown_control college_sel">
              <option value="">{{trans('general.gn_select')}}</option>

            </select>
          </div>
          <div class="form-group col-md-6">
            <label>{{trans('general.gn_academic_degree')}} :*</label>
            <select id="fkEedAcd" required name="fkEedAcd" class="form-control icon_control dropdown_control">
              <option value="">{{trans('general.gn_select')}}</option>
              @foreach($AcademicDegrees as $k =>$v)
                <option value="{{$v->pkAcd}}">{{$v->acd_AcademicDegreeName}}</option>
              @endforeach
            </select>
          </div>
          <div class="form-group col-md-6">
            <label>{{trans('general.gn_qualification_degree')}} :*</label>
            <select id="fkEedQde" required name="fkEedQde" class="form-control icon_control dropdown_control">
              <option value="">{{trans('general.gn_select')}}</option>
              @foreach($QualificationDegrees as $k =>$v)
                <option value="{{$v->pkQde}}">{{$v->qde_QualificationDegreeName}}</option>
              @endforeach
            </select>
          </div>
          <div class="form-group col-md-6">
            <label>{{trans('general.gn_designation')}} :*</label>
            <select required id="fkEedEde" name="fkEedEde" class="form-control icon_control dropdown_control">
              <option value="">{{trans('general.gn_select')}}</option>
              @foreach($EmployeeDesignations as $k =>$v)
                <option value="{{$v->pkEde}}">{{$v->ede_EmployeeDesignationName}}</option>
              @endforeach
            </select>
          </div>
          <div class="form-group col-md-6">
            <label>{{trans('general.gn_year_of_passing')}} :*</label>
            <input required class="form-control datepicker-year date_control icon_control" type="text" id="eed_YearsOfPassing" name="eed_YearsOfPassing">
          </div>
          <div class="form-group col-md-6">
            <label>{{trans('general.gn_short_title')}} :*</label>
            <input required class="form-control" type="text" id="eed_ShortTitle" name="eed_ShortTitle">
          </div>
          <div class="form-group col-md-6">
            <label>{{trans('general.gn_number_of_semesters')}} :*</label>
            <input required class="form-control" type="number" id="eed_SemesterNumbers" name="eed_SemesterNumbers">
          </div>
          <div class="form-group col-md-6">
            <label>{{trans('general.gn_ect_points')}} :*</label>
            <input required class="form-control" type="number" id="eed_EctsPoints" name="eed_EctsPoints">
          </div>

          <div class="form-group col-md-6">
            <label>{{trans('general.gn_document')}} :*</label>
            <div class="upload_file">
              <input type="text" id="file_name" name="file_name" value="{{trans('general.gn_upload')}}" class="form-control">
              <input class="diploma_file" required type="file" id="eed_DiplomaPicturePath" name="eed_DiplomaPicturePath" accept="application/pdf,image/jpeg,image/png" />
            </div>
          </div>

          <div class="form-group col-md-6">
            <label>{{trans('general.gn_note')}} :</label>
            <input class="form-control" type="text" id="eed_Notes" name="eed_Notes">
          </div>
        </div>
      </div>
    </div>
    <input type="hidden" id="add_qualification_txt" value="{{trans('message.msg_please_add_qualification')}}">
    <input type="hidden" id="please_fillup_required_fields" value="{{trans('message.please_fillup_required_fields')}}">
    <input type="hidden" id="employeemsgerror" value="{{trans('message.msg_employee_id_incorrect')}}">
</div>
<script>
    @if($logged_user->type=='HertronicAdmin' || $logged_user->type=='MinistryAdmin')
        var listing_url = "{{route('fetch-employees-lists')}}";
    @else
        var listing_url = "{{route('fetch-staff-lists')}}";
    @endif
</script>
@endsection

@push('datatable-scripts')
<!-- Include this Page JS -->
<script type="text/javascript" src="{{ asset('js/dashboard/employee.js') }}"></script>
@endpush