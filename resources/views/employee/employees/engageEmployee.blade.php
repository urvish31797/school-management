@extends('layout.app_with_login')
@section('title', trans('sidebar.sidebar_nav_employees'))
@section('script', asset('js/dashboard/engage_employee.js'))
@section('content')
 <!-- Page Content  -->
<div class="section">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12 mb-3">
               <h2 class="title"><span>{{trans('sidebar.sidebar_nav_user_management')}} > </span><a class="no_sidebar_active" href="{{url('/employee/employees')}}"><span>{{trans('sidebar.sidebar_nav_employees')}} > </span></a> {{trans('general.gn_engagement')}}</h2>
            </div>
        </div>
        <div class="col-md-12">
            <div class="white_box p-5 pl-3 pr-3">
                <div class="row">
                    <div class="col-md-6 col-lg-6">
                        <div class="">
                            <div class="form-group">
                                <label>{{trans('general.gn_search_employees')}}</label>
                                <input type="text" maxlength="30" id="search_employee" class="form-control icon_control search_control" placeholder="{{trans('general.gn_enter')}} {{trans('general.gn_name')}}">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-lg-3">
                        <label class="blue_label">{{trans('general.gn_employee_type')}}</label>
                        <select class="form-control icon_control dropdown_control" id="employee_types">
                            <option value="" selected>Select</option>
                            @forelse($employeeType as $empTypVal)
                                @if($empTypVal->epty_Name != "HomeroomTeacher")
                                <option value="{{ $empTypVal->epty_Name }}">@if($empTypVal->epty_Name=='Principal') {{ trans('general.gn_principal')}} @elseif($empTypVal->epty_Name=='Teacher') {{ trans('general.gn_teacher')}} @endif</option>
                                @endif
                            @empty
                                <option >No Data Found</option>
                            @endforelse
                        </select>
                    </div>
                    <input type="hidden" id="emp_not_engaged" value="{{trans('general.gn_not_engaged')}}">
                    <div class="col-md-3 col-lg-3">
                        <div class="form-group">
                            <label class="blue_label">{{trans('general.gn_engagement_type')}}</label>
                            <select class="form-control icon_control dropdown_control" id="employee_eng_types">
                                <option value="" selected>Select</option>
                                <option value="1">{{trans('general.gn_engaged')}}</option>
                                <option value="2">{{trans('general.gn_not_engaged')}}</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <div class="table-responsive mt-2 main_table">
                            <table id="eng_emp_listing" class="display" style="width:100%">
                                <thead>
                                    <tr>
                                        <th>{{trans('general.sr_no')}}</th>
                                        <th>{{trans('general.gn_employee')}} ID</th>
                                        <th>{{trans('general.gn_name')}}</th>
                                        <th>{{trans('general.gn_email')}}</th>
                                        <th>{{trans('general.gn_employee_type')}}</th>
                                        <th>{{trans('general.gn_status')}}</th>
                                        <th>{{trans('general.gn_action')}}</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                        <div class="col-md-12 sel_emp_div" style="display: none;">
                            <p class="mt-2"><strong>{{trans('general.gn_employee')}}</strong></p>
                            <div class="table-responsive mt-2">
                                <table class="color_table sel_emp_table">
                                    <tr>
                                        <th width="10%">{{trans('general.sr_no')}}</th>
                                        <th width="25%">{{trans('general.gn_employee')}} ID</th>
                                        <th width="20%">{{trans('general.gn_name')}}</th>
                                        <th width="30%">{{trans('general.gn_action')}}</th>
                                    </tr>

                                </table>
                            </div>
                        </div>
                        <form name="engage-emp-form">
                            <div class="container-fluid">
                                <input type="hidden" id="eid" name="eid">
                                <div class="row">
                                    <div class="col-md-12">
                                        <h6 class="profile_inner_title">{{trans('general.gn_engagement_details')}}</h6>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>{{trans('general.gn_date_of_engagement')}} *</label>
                                            <input type="text" id="start_date" name="start_date" class="form-control icon_control date_control">
                                        </div>
                                    </div>
                                    <!-- <div class="col-md-6">
                                        <div class="form-group">
                                            <label>{{trans('general.gn_date_of_engagement_end')}} </label>
                                            <input type="text" id="end_date" name="end_date" class="form-control icon_control date_control">
                                        </div>
                                    </div> -->
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>{{trans('general.gn_employee_type')}} *</label>
                                            <select id="fkEenEpty" name="fkEenEpty" class="form-control icon_control dropdown_control">
                                                <option value="">{{trans('general.gn_select')}}</option>
                                                @foreach($EmployeeTypes as $k => $v)
                                                  <option value="{{$v->pkEpty}}"> @if($v->epty_Name=='Principal') {{ trans('general.gn_principal')}} @elseif($v->epty_Name=='Teacher') {{ trans('general.gn_teacher')}} @endif
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="form-group form-check" id="principal_checkbox" style="display:none">
                                            <input name="een_ActingAsPrincipal" type="checkbox" class="form-check-input" id="een_ActingAsPrincipal">
                                            <label class="custom_checkbox"></label>
                                            <label class="form-check-label" for="exampleCheck1">{{trans('general.gn_acting_principal')}}</label>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>{{trans('general.gn_type_of_engagement')}} *</label>
                                            <select id="fkEenEty" name="fkEenEty" class="form-control icon_control dropdown_control">
                                                <option value="">{{trans('general.gn_select')}}</option>
                                                @foreach($EngagementTypes as $k => $v)
                                                  <option value="{{$v->pkEty}}">{{$v->ety_EngagementTypeName}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>{{trans('general.gn_week_hourly_rate')}} *</label>
                                            <input required type="text" id="ewh_WeeklyHoursRate" name="ewh_WeeklyHoursRate" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                        <label>{{trans('general.gn_hourly_note')}} </label>
                                        <input type="text" name="ewh_Notes" id="ewh_Notes" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>{{trans('general.gn_enagement_note')}} </label>
                                            <input type="text" id="een_notes" name="een_notes" class="form-control">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="text-center">
                                    <button type="Submit" class="theme_btn">{{trans('general.gn_submit')}}</button>
                                    <a class="theme_btn red_btn no_sidebar_active" href="{{url('/employee/employees')}}">{{trans('general.gn_cancel')}}</a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

    </div>

<input type="hidden" id="add_qualification_txt" value="{{trans('message.msg_please_add_qualification')}}">
<input type="hidden" id="teacher_txt" value="{{trans('general.gn_teacher')}}">
<input type="hidden" id="principal_txt" value="{{trans('general.gn_principal')}}">
<input type="hidden" id="school_coordinator_txt" value="{{trans('general.gn_school_coordinator')}}">
<input type="hidden" id="emp_sel_valid_txt" value="{{trans('message.msg_emp_sel_exist')}}">
<input type="hidden" id="emp_sel_txt" value="{{trans('message.msg_sel_emp')}}">

</div>
<script>
var engage_list_url = "{{route('fetch-engage-employees-lists')}}";
</script>
@endsection

@push('datatable-scripts')
<!-- Include this Page JS -->
<script type="text/javascript" src="{{ asset('js/dashboard/engage_employee.js') }}"></script>
@endpush