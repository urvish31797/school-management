@extends('layout.app_with_login')
@section('title', trans('sidebar.sidebar_nav_employees'))
@section('content')
 <!-- Page Content  -->
<div class="section">
	<div class="container-fluid">
		<div class="row ">
            <div class="col-12 mb-3">
            @if($logged_user->type=='MinistryAdmin')
                <h2 class="title"><a class="no_sidebar_active" href="{{url('admin/employees')}}"> <span>{{trans('sidebar.sidebar_nav_employees')}} > </span></a> {{trans('general.gn_details')}}</h2>
            @else
                <h2 class="title"><span>@if(Request::is('employee/*')){{trans('sidebar.sidebar_nav_user_management')}} > </span><a class="no_sidebar_active" href="{{url('/employee/employees')}}"> @else {{trans('sidebar.sidebar_nav_ministry_masters')}} > </span><a class="no_sidebar_active" href="{{url('/admin/employees')}}"> @endif <span>{{trans('sidebar.sidebar_nav_employees')}} > </span></a> {{trans('general.gn_details')}}</h2>
            @endif
            </div>
            <div class="col-12">
                <div class="white_box pt-5 pb-5">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="text-center">
                                    <div class="profile_box">
                                        <div class="profile_pic">
                                            <img alt="" src="@if(!empty($mdata->emp_PicturePath)) {{asset('images/users/')}}/{{$mdata->emp_PicturePath}} @else {{ asset('images/user.png') }}@endif">
                                        </div>
                                    </div>
                                    <h5 class="profile_name">{{$mdata->emp_EmployeeName}} {{$mdata->emp_EmployeeSurname}}</h5>
                                </div>
                                <div class="profile_info_container">
                                    <div class="row">
                                        <div class="col-md-6 col-lg-3">
                                            <div class="form-group">
                                                <p class="label">{{trans('general.gn_employee')}} ID :</p>
                                                <p class="value">{{$mdata->emp_EmployeeID}}</p>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-lg-3">
                                            <div class="form-group">
                                                <p class="label">{{trans('general.gn_temp_citizen_id')}} :</p>
                                                <p class="value">{{$mdata->emp_TempCitizenId}}</p>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-lg-3">
                                            <div class="form-group">
                                                <p class="label">{{trans('login.ln_email')}} :</p>
                                                <p class="value">{{$mdata->email}}</p>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-lg-3">
                                            <div class="form-group">
                                                <p class="label">{{trans('general.gn_phone')}} :</p>
                                                <p class="value">{{$mdata->emp_PhoneNumber}}</p>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-lg-3">
                                            <div class="form-group">
                                                <p class="label">{{trans('general.gn_gender')}} :</p>
                                                <p class="value">{{$mdata->emp_EmployeeGender}}</p>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-lg-3">
                                            <div class="form-group">
                                                <p class="label">{{trans('general.gn_dob')}} :</p>
                                                <p class="value">@if(!empty($mdata->emp_DateOfBirth)){{date('d/m/Y',strtotime($mdata->emp_DateOfBirth))}}@endif</p>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-lg-3">
                                            <div class="form-group">
                                                <p class="label">{{trans('general.gn_country')}} :</p>
                                                <p class="value">{{isset($mdata->country->cny_CountryName) ? $mdata->country->cny_CountryName:''}}</p>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-lg-3">
                                            <div class="form-group">
                                                <p class="label">{{trans('general.gn_status')}} :</p>
                                                <p class="value">@if($mdata->emp_Status=='Active') {{trans('general.gn_active')}} @else {{trans('general.gn_inactive')}} @endif</p>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-lg-3">
                                            <div class="form-group">
                                                <p class="label">{{trans('general.gn_address')}} :</p>
                                                <p class="value">{{$mdata->emp_Address}}</p>
                                            </div>
                                        </div>
                                    </div>
                                    <br>
                                </div>
                                <div class="row">
                                    <div class="col-12">
                                        <h6 class="profile_inner_title">{{trans('general.gn_work_experience')}}</h6>
                                    </div>

                                    @if(count($mdata->employeesEngagement))
                                    <div class="col-12">
                                        <div class="theme_table">
                                            <div class="table-responsive ">
                                              <table class="profile_table expand_table">
                                                <thead>
                                                    <tr>
                                                        <th>No.</th>
                                                        <th>{{trans('general.gn_employee_type')}}</th>
                                                        <th>{{trans('general.gn_type_of_engagement')}}</th>
                                                        <th>{{trans('general.gn_date_of_engagement')}}</th>
                                                        <th>{{trans('general.gn_date_of_engagement_end')}}</th>
                                                        <th>{{trans('general.gn_hourly_rate')}}</th>
                                                        <th>{{trans('general.gn_school')}} {{trans('general.gn_name')}}</th>
                                                        <th></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                  @forelse($mdata->employeesEngagement as $k => $value)
                                                    <tr data-toggle="collapse" href="#collapseExample{{$value->pkEen}}" role="button" aria-expanded="false" aria-controls="collapseExample">
                                                        <td>{{$k+1}}</td>
                                                        <td>
                                                            @if(isset($value->employeeType->epty_Name))
                                                                @if($value->employeeType->epty_Name=='Principal') {{ trans('general.gn_principal')}}
                                                                @elseif($value->employeeType->epty_Name=='Teacher') {{ trans('general.gn_teacher')}}
                                                                @elseif($value->employeeType->epty_Name=='SchoolCoordinator') {{ trans('general.gn_school_coordinator')}}
                                                                @elseif($value->employeeType->epty_Name=='HomeroomTeacher') {{ trans('general.gn_homeroom_teacher')}}
                                                                @endif
                                                            @endif
                                                        </td>
                                                        <td>
                                                          {{$value->engagementType->ety_EngagementTypeName ?? ''}}
                                                        </td>
                                                        <td>
                                                          {{$value->een_DateOfEngagement ?? '-'}}
                                                        </td>
                                                        <td>
                                                          {{$value->een_DateOfFinishEngagement}}
                                                        </td>
                                                        <td>
                                                            {{$value->getLatestHourRates[0]->ewh_WeeklyHoursRate ?? ''}}
                                                        </td>
                                                        <td>
                                                            {{$value->school->sch_SchoolName ?? ''}}
                                                        </td>
                                                        <td class="collapse_sign"> + </td>
                                                    </tr>
                                                    @if(!empty($value->getAllHourRates))
                                                    @foreach($value->getAllHourRates as $gk => $vk)
                                                    <tr class="collapse" id="collapseExample{{$value->pkEen}}">
                                                        <td></td>
                                                        <td colspan="7">
                                                            <div class="expand_div">
                                                                <div class="row">
                                                                    <div class="col-md-4">
                                                                        <p class="label">{{trans('general.gn_hourly_rate')}}</p>
                                                                        <p class="value">{{$vk->ewh_WeeklyHoursRate ?? ''}}</p>
                                                                    </div>
                                                                    <div class="col-md-4">
                                                                        <p class="label">{{trans('general.gn_start_date')}}</p>
                                                                        <p class="value">{{$vk->ewh_StartDate ?? ''}}</p>
                                                                    </div>
                                                                    <div class="col-md-4">
                                                                        <p class="label">{{trans('general.gn_end_date')}}</p>
                                                                        <p class="value">{{$vk->ewh_EndDate ?? '-'}}</p>
                                                                    </div>
                                                                    <div class="col-12 mt-2">
                                                                        <p class="label">{{trans('general.gn_hourly_note')}}</p>
                                                                        <p class="value">{{$vk->ewh_Notes ?? ''}}</p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    @endforeach
                                                    @endif
                                                  @empty
                                                  <tr class="text-center">
                                                      {{trans('general.gn_no_data_found')}}
                                                  </tr>
                                                  @endforelse
                                                </tbody>
                                              </table>
                                            </div>
                                        </div>
                                    </div>
                                    @else
                                    <div class="col-12">
                                    <p>
                                        {{trans('general.gn_no_data_found')}}
                                    </p>
                                    </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End Content Body -->
@endsection

