@extends('layout.app_with_login')
@section('title', trans('sidebar.sidebar_nav_employees'))
@section('script', asset('js/dashboard/employee.js'))
@section('content')
 <!-- Page Content  -->
<div class="section">
	<div class="container-fluid">
		<div class="row ">
            @if($logged_user->type=='HertronicAdmin' || $logged_user->type=='MinistryAdmin') <input type="hidden" id="is_admin" value="1"> @endif
            <div class="col-12 mb-3">
            @if($logged_user->type=='MinistryAdmin')
                <h2 class="title">{{trans('sidebar.sidebar_nav_employees')}}</h2>
            @else
                <h2 class="title"><span>@if(Request::is('employee/employees')){{trans('sidebar.sidebar_nav_user_management')}} @else {{trans('sidebar.sidebar_nav_ministry_masters')}} @endif > </span>{{trans('sidebar.sidebar_nav_employees')}}
                </h2>
            @endif
            </div>
            <div class="col-md-4 mb-3">
                <input type="text" id="search_teacher" maxlength="30" class="form-control icon_control search_control" placeholder="{{trans('general.gn_search')}}">
            </div>
            @if(Request::is('admin/employees'))
            <div class="col-md-2 col-6 mb-3">

            </div>
            @endif
            <div class="col-md-2 col-6 mb-3">

            </div>
            <div class="col-md-4 text-md-right mb-3">
                <div class="row">
                    <div class="col-6 text-right pr-0 pt-1">
                        <label class="blue_label">{{trans('general.gn_employee_type')}}</label>
                    </div>
                    <div class="col-6">
                        <select class="form-control icon_control dropdown_control" id="employee_types">
                            <option value="" selected>{{trans('general.gn_select')}}</option>
                            @forelse($employeeType as $empTypVal)
                                <option value="{{ $empTypVal->epty_Name }}">@if($empTypVal->epty_Name=='Principal') {{ trans('general.gn_principal')}} @elseif($empTypVal->epty_Name=='Teacher') {{ trans('general.gn_teacher')}} @elseif($empTypVal->epty_Name=='SchoolCoordinator') {{ trans('general.gn_school_coordinator')}} @endif</option>
                            @empty
                                <option >No Data Found</option>
                            @endforelse
                        </select>
                    </div>
                </div>
            </div>
            @if(Request::is('employee/employees'))
            <div class="col-md-2 col-6 mb-3">
                <a><button class="theme_btn small_btn" data-toggle="modal" data-target="#add_new">{{trans('general.gn_add_new')}}</button></a>
            </div>
            @endif
        </div>
        <input type="hidden" id="emp_not_engaged" value="{{trans('general.gn_not_engaged')}}">
        <input type="hidden" id="teacher_txt" value="{{trans('general.gn_teacher')}}">
        <input type="hidden" id="principal_txt" value="{{trans('general.gn_principal')}}">
        <input type="hidden" id="school_coordinator_txt" value="{{trans('general.gn_school_coordinator')}}">
        <div class="row">
            <div class="col-12">
                <div class="theme_table">
                    <div class="table-responsive">
                        <table id="teacher_listing" class="display" style="width:100%">
                            <thead>
                                <tr>
                                    <th>{{trans('general.sr_no')}}.</th>
                                    <th>{{trans('general.gn_employee')}} ID</th>
                                    <th>{{trans('general.gn_name')}}</th>
                                    <th>{{trans('general.gn_email')}}</th>
                                    <th>{{trans('general.gn_employee_type')}}</th>
                                    {{-- <th>{{trans('general.gn_engagement_type')}}</th> --}}
                                    <th>{{trans('general.gn_status')}}</th>
                                    <th>{{trans('general.gn_actions')}}</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
	</div>


    <div class="theme_modal modal fade" id="delete_prompt" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <img src="{{asset('images/ic_close_bg.png')}}" class="modal_top_bg">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <img src="{{asset('images/ic_close_circle_white.png')}}">
                    </button>
                        <div class="row">
                            <div class="col-lg-1"></div>
                            <div class="col-lg-10">
                                <h5 class="modal-title" id="exampleModalCenterTitle">{{trans('general.gn_delete')}}</h5>
                                <div class="form-group text-center">
                                    <label>{{trans('general.gn_delete_prompt')}} ?</label>
                                    <input type="hidden" id="did">
                                </div>
                                <div class="text-center modal_btn ">
                                    <button style="display: none;" class="theme_btn show_delete_modal full_width small_btn" data-toggle="modal" data-target="#delete_prompt">{{trans('general.gn_delete')}}</button>
                                    <button type="button" onclick="confirmDelete()" class="theme_btn">{{trans('general.gn_yes')}}</button>
                                    <button type="button" data-dismiss="modal" class="theme_btn red_btn">{{trans('general.gn_no')}}</button>
                                </div>
                            </div>
                            <div class="col-lg-1"></div>
                        </div>
                </div>
            </div>
        </div>
    </div>
    <div class="theme_modal modal fade" id="add_new" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <img src="{{asset('images/ic_close_bg.png')}}" class="modal_top_bg">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <img src="{{asset('images/ic_close_circle_white.png')}}">
                    </button>
                    <div class="row">
                        <div class="col-lg-1"></div>
                        <div class="col-lg-10">
                            <h5 class="modal-title" id="exampleModalCenterTitle">{{trans('general.gn_employee')}}</h5>

                            <div class="text-center modal_btn">
                                <a href="{{url('/employee/employees/create')}}" class="theme_btn">{{trans('general.gn_add_new')}} {{trans('general.gn_employee')}}</a>
                                <p class="mt-3 mb-3">{{trans('general.gn_or')}}</p>
                                <a href="{{route('engageemployee')}}" class="theme_btn">{{trans('general.gn_engage_employee')}}</a>
                            </div>
                            <div class="text-center mt-3">
                                <span><small> ({{trans('general.gn_engage_employee_note')}})</small></span>
                            </div>
                        </div>
                        <div class="col-lg-1"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
<script>
    @if($logged_user->type=='HertronicAdmin' || $logged_user->type=='MinistryAdmin')
        var listing_url = "{{route('fetch-employees-lists')}}";
    @else
        var listing_url = "{{route('fetch-staff-lists')}}";
    @endif
</script>
@endsection

@push('datatable-scripts')
<!-- Include datatable Page JS -->
<script type="text/javascript" src="{{ asset('js/dashboard/employee.js') }}"></script>
@endpush