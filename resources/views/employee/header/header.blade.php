<nav class="navbar navbar-expand-lg navbar-light fixed-top">
    <div class="container-fluid">

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
          <button type="button" id="sidebarCollapse" class="btn btn-info">
              <i class="fas fa-align-left"></i>
              <span>Toggle Sidebar</span>
          </button>
            <ul class="nav navbar-nav ml-auto">
              @if($logged_user->type!='SchoolCoordinator')
              <li class="nav-item ">
                <select onchange='roleEmployeeSwitch()' id="year_drop_down" class="form-control icon_control dropdown_control">
                  @if(!empty($schoolYear))
                  @foreach($schoolYear as $k => $v)
                  <option @if($logged_user->syid == $v->pkSye) selected="selected" @endif value="{{$v->pkSye}}">{{$v->sye_NameCharacter}}</option>
                  @endforeach
                  @endif
                </select>
              </li>
              @endif
              <li class="nav-item ">
                <select onchange='roleEmployeeSwitch()' id="role_drop_down" class="form-control icon_control dropdown_control">
                    @foreach($employeeRoles->EmployeesEngagement as $k => $v)
                      @php
                        if($v->employeeType->epty_Name == 'SchoolCoordinator'){
                          $type = trans('general.gn_school_coordinator');
                        }elseif($v->employeeType->epty_Name == 'Teacher'){
                          $type = trans('general.gn_teacher');
                        }elseif($v->employeeType->epty_Name == 'Principal'){
                          $type = trans('general.gn_principal');
                        }elseif($v->employeeType->epty_Name == 'HomeroomTeacher'){
                          $type = trans('general.gn_homeroom_teacher');
                        }
                      @endphp
                      <option @if($v->employeeType->epty_Name==$logged_user->type && Session::get('curr_emp_sid') == $v->school->pkSch && Session::get('curr_emp_eid') == $v->pkEen) selected="selected" @endif value='{{$v->employeeType->epty_Name}}' data-eptid="{{$v->fkEenEpty}}" data-hid="{{$v->pkHrt}}" data-csemid="{{$v->fkHrtCcs}}" data-eid="{{$v->pkEen}}" data-sch="{{$v->school->pkSch}}">{{$type}} - {{$v->school->sch_SchoolName}}</option>
                    @endforeach
                </select>
              </li>

              <li class="nav-item ">
                <div class="custom-drop-down" id="custom-flag-drop-down">
                  <select onchange='langSwitch(this.value)' style="width:130px;">
                      @foreach($languages as $k => $v)
                        <option @if($current_language==$v->language_key) selected @endif value='{{$v->language_key}}' class="custom_flag {{$v->language_key}}" style="background-image:url({{asset('images/languages')}}/{{$v->flag}});" data-title="{{$v->language_name}}">{{$v->language_name}}</option>
                      @endforeach
                  </select>
                </div>
              </li>

              <li class="nav-item active">
                  <a class="nav-link" href="#"> <img src="{{ asset('images/ic_search.png') }}"></a>
              </li>

              <li class="nav-item dropdown notification_dropdown">
                  <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <img src="{{ asset('images/ic_bell.png') }}">
                  </a>
                  <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <ul>
                      <li>
                        <p class="noti_pere">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
                        <div class="close_noti">
                          <img src="{{ asset('images/ic_close_circle.png') }}">
                        </div>
                      </li>
                      <li>
                        <p class="noti_pere">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
                        <div class="close_noti">
                          <img src="{{ asset('images/ic_close_circle.png') }}">
                        </div>
                      </li>
                      <li>
                        <p class="noti_pere">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
                        <div class="close_noti">
                          <img src="{{ asset('images/ic_close_circle.png') }}">
                        </div>
                      </li>
                    </ul>
                  </div>
              </li>
              <li class="nav-item dropdown">
                  <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown1" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <div class="profile-cover">
                      <img src="@if(!empty($logged_user->emp_PicturePath)) {{asset('images/users/')}}/{{$logged_user->emp_PicturePath}} @else {{ asset('images/user.png') }}@endif">
                    </div>
                    <i class="fas fa-chevron-down right-arrow"></i>
                  </a>
                  <div class="dropdown-menu" aria-labelledby="navbarDropdown1">
                     <a class="dropdown-item" href="{{ url('employee/profile') }}">{{trans('general.gn_profile')}}</a>
                    <a class="dropdown-item" href="#">{{trans('general.gn_setting')}}</a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="{{ url('logout') }}">{{trans('general.gn_logout')}}</a>
                  </div>
              </li>
            </ul>

        </div>
    </div>
</nav>