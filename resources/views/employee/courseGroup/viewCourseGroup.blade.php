@extends('layout.app_with_login')
@section('title', trans('sidebar.sidebar_nav_course_groups'))
@section('script', asset('js/dashboard/course_group.js'))
@section('content')
<?php
$selTeacher = [];
$selGrades = [];

if(isset($mdata) && !empty($mdata->classStudents)){
    $tmpGrades = [];
    foreach ($mdata->classStudents as $k => $v) {
        foreach ($v->classCreationTeachers as $kt => $vt) {
            $selTeacher[] = $vt->fkCtcEeg;
        }

        $tmpGrades[] = $v->classSemester->studentEnroll->fkSteGra;
    }
    $selGrades = array_unique($tmpGrades);
}
?>
<!-- Page Content  -->
<div class="section">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12 mb-3">
                <h2 class="title"><span>{{trans('sidebar.sidebar_nav_organization')}} > </span><a class="no_sidebar_active" href="{{url('/employee/courseGroups')}}"><span>{{trans('sidebar.sidebar_nav_course_groups')}} > </span></a>{{trans('general.gn_view_details')}}</h2>
            </div>
        </div>
        <div class="col-md-12">
            <div class="white_box p-5 pl-3 pr-3">
                <form name="course_group_form">
                    @if(isset($mdata->pkCga))
                    <input type="hidden" id="pkCga" value="{{$mdata->pkCga}}">
                    @endif
                    <div class="row">
                        <div class="col-lg-4">
                            <div class="">
                                <div class="form-group">
                                    <label>{{trans('general.gn_course_group_type')}}</label>
                                    <select disabled id="course_group_main" name="course_group_main" onchange="fetchCourseGroup(this.value)" class="form-control icon_control dropdown_control">
                                        <option value="">{{trans('general.gn_select')}}</option>
                                        <option @if(!empty($mdata->fkCgaGpg)) selected @endif value="gpg">{{trans('general.gn_general_purpose_groups')}}</option>
                                        <option @if(!empty($mdata->fkCgaOcg)) selected @endif value="ocg">{{trans('general.gn_optional_courses_groups')}}</option>
                                        <option @if(!empty($mdata->fkCgaFlg)) selected @endif value="flg">{{trans('general.gn_foreign_language_groups')}}</option>
                                        <option @if(!empty($mdata->fkCgaFcg)) selected @endif value="fcg">{{trans('general.gn_facultative_courses_groups')}}</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="">
                                <div class="form-group">
                                    <label>{{trans('general.gn_course_groups')}}</label>
                                    <select disabled id="course_group_sub" name="course_group_sub" class="form-control icon_control dropdown_control">
                                        <option value="">{{trans('general.gn_select')}}</option>
                                        @if(!empty($mdata->GPG))
                                        @foreach($GPG as $k => $v)
                                        <option @if($v->id == $mdata->GPG->pkGpg) selected @endif value="{{$v->id}}">{{$v->group_Name}}</option>
                                        @endforeach
                                        @elseif(!empty($mdata->OCG))
                                        @foreach($OCG as $k => $v)
                                        <option @if($v->id == $mdata->OCG->pkOcg) selected @endif value="{{$v->id}}">{{$v->group_Name}}</option>
                                        @endforeach
                                        @elseif(!empty($mdata->FLG))
                                        @foreach($FLG as $k => $v)
                                        <option @if($v->id == $mdata->FLG->pkFon) selected @endif value="{{$v->id}}">{{$v->group_Name}}</option>
                                        @endforeach
                                        @elseif(!empty($mdata->FCG))
                                        @foreach($FCG as $k => $v)
                                        <option @if($v->id == $mdata->FCG->pkFcg) selected @endif value="{{$v->id}}">{{$v->group_Name}}</option>
                                        @endforeach
                                        @endif
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="">
                                <div class="form-group">
                                    <label>{{trans('general.gn_courses')}}</label>
                                    <select disabled id="subject" name="subject" class="form-control icon_control dropdown_control">
                                        <option value="">{{trans('general.gn_select')}}</option>
                                        @if(isset($mdata) && !empty($mdata->classStudents))
                                        @foreach($courses as $k => $v)
                                        @if(!empty($mdata->GPG) || !empty($mdata->OCG) || !empty($mdata->FCG))
                                        @if($v->crs_IsForeignLanguage == 'No')
                                        <option @if(isset($mdata->classStudents[0])) @if($v->pkCrs == $mdata->classStudents[0]->fkCsaEmc) selected @endif @endif value="{{$v->pkCrs}}">{{$v->crs_CourseName}}</option>
                                        @endif
                                        @else(!empty($courses->FLG))
                                        @if($v->crs_IsForeignLanguage == 'Yes')
                                        <option @if(isset($mdata->classStudents[0])) @if($v->pkCrs == $mdata->classStudents[0]->fkCsaEmc) selected @endif @endif value="{{$v->pkCrs}}">{{$v->crs_CourseName}}</option>
                                        @endif
                                        @endif
                                        @endforeach
                                        @endif
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="">
                                <div class="form-group">
                                    <label>{{trans('general.gn_school_year')}} </label>
                                    <select disabled id="fkClrSye" name="fkClrSye" class="form-control icon_control dropdown_control">
                                        <option value="">{{trans('general.gn_select')}}</option>
                                        @foreach($schoolYears as $k => $v)
                                        <option @if($mdata->fkCgaSye == $v->pkSye) selected @endif value="{{$v->pkSye}}">{{$v->sye_NameCharacter}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="">
                                <div class="form-group">
                                    <label>{{trans('general.gn_course_order')}} </label>
                                    <select disabled onchange="clearStuds()" id="course_order" name="course_order" class="form-control icon_control dropdown_control ">
                                        <option value="">{{trans('general.gn_select')}}</option>
                                        <option @if(isset($mdata) && $mdata->cga_OrderNumber == 1) selected @endif value="1">1</option>
                                        <option @if(isset($mdata) && $mdata->cga_OrderNumber == 2) selected @endif value="2">2</option>
                                        <option @if(isset($mdata) && $mdata->cga_OrderNumber == 3) selected @endif value="3">3</option>
                                        <option @if(isset($mdata) && $mdata->cga_OrderNumber == 4) selected @endif value="4">4</option>
                                        <option @if(isset($mdata) && $mdata->cga_OrderNumber == 5) selected @endif value="5">5</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-4 fcg_hour">
                            <div class="">
                                <div class="form-group">
                                    <label>{{trans('general.gn_week_hourly_rate')}}</label>
                                    <input type="number" disabled id="cg_hour" min="1" name="cg_hour" class="form-control" value="{{$mdata->cga_WeekHours}}">
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="">
                                <div class="form-group">
                                    <label>{{trans('general.gn_village_school')}}</label>
                                    @include('partials.dropdown.dropdowns',[
                                    'type'=>'village_school',
                                    'name'=>'fkCgaViSch',
                                    'selected'=>$mdata->fkCgaViSch,
                                    'extra_params'=>[
                                    "id"=>"fkCgaViSch",
                                    "school_id"=>$mainSchool,
                                    "disabled"=>"disabled"
                                    ]
                                    ])
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="">
                                <div class="form-group">
                                    <label>{{trans('general.gn_semester')}}</label>
                                    @include('partials.dropdown.dropdowns',[
                                    'type'=>'education_period',
                                    'name'=>'',
                                    'selected'=>'',
                                    'extra_params'=>[
                                    "id"=>"fkCgaEdu",
                                    "disabled"=>"disabled"
                                    ]
                                    ])
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <p class="mt-2"><strong>{{trans('general.gn_selected_students')}}</strong></p>
                            <div class="table-responsive mt-2 mb-2">
                                <table class="color_table class_seleted_students sel_stu_elem @if(isset($mdata) && empty($mdata->classStudents)) hide_content @endif">
                                    <tbody>
                                        <tr>
                                            <th>{{trans('general.sr_no')}}</th>
                                            <th>ID</th>
                                            <th>{{trans('general.gn_name')}}</th>
                                            <th>{{trans('general.gn_grade')}}</th>
                                            <th>{{trans('general.gn_education_plan')}}</th>
                                        </tr>
                                        @if(isset($mdata) && !empty($mdata->classStudents))
                                        @foreach($mdata->classStudents as $k => $v)
                                        <tr class="stu stu_{{$v->classSemester->fkSemSen}} epl_{{$v->classSemester->studentEnroll->fkSteEpl}} gra_{{$v->classSemester->studentEnroll->fkSteGra}}">
                                            <td>{{$k+1}}</td>
                                            <td>@if(!empty($v->classSemester->studentEnroll->student->stu_StudentID)){{$v->classSemester->studentEnroll->student->stu_StudentID}} @else {{$v->classSemester->studentEnroll->student->stu_TempCitizenId}}@endif</td>
                                            <td>{{$v->classSemester->studentEnroll->student->full_name}}</td>
                                            <td>{{$v->classSemester->studentEnroll->grade->gra_GradeNumeric}}</td>
                                            <td>{{$v->classSemester->studentEnroll->educationProgram->edp_Name}} - {{$v->classSemester->studentEnroll->educationPlan->epl_EducationPlanName}}</td>
                                        </tr>
                                        @endforeach
                                        @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="col-lg-7">
                            <div class="">
                                <div class="form-group">
                                    <label>{{trans('general.gn_teacher')}}</label>
                                    <select disabled required id="teacher" name="teacher[]" multiple="multiple" class="form-control icon_control dropdown_control select2_multi">
                                        @foreach($employees as $ek => $ev)
                                        <option @if(in_array($ev->pkEen,$selTeacher)) selected @endif value="{{$ev->pkEen}}">{{$ev->employee->emp_EmployeeName}} {{$ev->employee->emp_EmployeeSurname}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script>
    var listing_url = "{{route('fetch-coursegroup-lists')}}";

</script>
@endsection

@push('datatable-scripts')
<!-- Include this Page JS -->
<script type="text/javascript" src="{{ asset('js/dashboard/course_group.js') }}"></script>
@endpush
