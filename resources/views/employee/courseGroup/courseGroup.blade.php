@extends('layout.app_with_login')
@section('title', trans('sidebar.sidebar_nav_course_groups'))
@section('script', asset('js/dashboard/course_group.js'))
@section('content')
<?php
$selTeacher = [];
$selGrades = [];
$selectedYear = '';
$selSem = $currentSem;
if(isset($mdata) && !empty($mdata->classStudents)){
    $tmpGrades = [];
    foreach ($mdata->classStudents as $k => $v) {
        foreach ($v->classCreationTeachers as $kt => $vt) {
            $selTeacher[] = $vt->fkCtcEeg;
        }

        $tmpGrades[] = $v->classSemester->studentEnroll->fkSteGra;
    }
    $selGrades = array_unique($tmpGrades);
    $selectedYear = $mdata->fkCgaSye;
    $selSem = $mdata->fkCgaEdu;
}
?>
<!-- Page Content  -->
<div class="section">
    <input type="hidden" id="teacher_txt" value="{{trans('general.gn_teacher')}}">
    <input type="hidden" id="principal_txt" value="{{trans('general.gn_principal')}}">
    <input type="hidden" id="stu_sel_valid_txt" value="{{trans('message.msg_student_select_valid')}}">
    <input type="hidden" id="stu_sel_txt" value="{{trans('message.msg_sel_stu')}}">
    <input type="hidden" id="course_select_txt" value="{{trans('validation.validate_course_select')}}">
    <input type="hidden" id="cgtype_sel_valid_txt" value="{{trans('validation.validate_course_group_type')}}">
    <input type="hidden" id="grade_eplan_valid_txt" value="{{trans('validation.validate_grade_eplan')}}">
    <input type="hidden" id="student_sel_valid_txt" value="{{trans('validation.validate_student_select')}}">
    <input type="hidden" id="course_order_valid_txt" value="{{trans('validation.validate_course_order')}}">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12 mb-3">
                <h2 class="title"><span>{{trans('sidebar.sidebar_nav_organization')}} > </span><a class="no_sidebar_active" href="{{url('/employee/coursegroup')}}"><span>{{trans('sidebar.sidebar_nav_course_groups')}} > </span></a>@if(isset($mdata)){{trans('general.gn_edit')}}@else{{trans('general.gn_add')}}@endif</h2>
            </div>
        </div>
        <div class="col-md-12">
            <div class="white_box p-5 pl-3 pr-3">
                <form name="course_group_form">
                    @if(isset($mdata->pkCga))
                    <input type="hidden" id="pkCga" value="{{$mdata->pkCga}}">
                    @endif
                    <div class="row">
                        <div class="col-lg-4">
                            <div class="">
                                <div class="form-group">
                                    <label>{{trans('general.gn_course_group_type')}} *</label>
                                    <select id="course_group_main" name="course_group_main" onchange="fetchCourseGroup(this.value)" class="form-control icon_control dropdown_control">
                                        <option value="">{{trans('general.gn_select')}}</option>
                                        <option @if(!empty($mdata->fkCgaGpg)) selected @endif value="gpg">{{trans('general.gn_general_purpose_groups')}}</option>
                                        <option @if(!empty($mdata->fkCgaOcg)) selected @endif value="ocg">{{trans('general.gn_optional_courses_groups')}}</option>
                                        <option @if(!empty($mdata->fkCgaFlg)) selected @endif value="flg">{{trans('general.gn_foreign_language_groups')}}</option>
                                        <option @if(!empty($mdata->fkCgaFcg)) selected @endif value="fcg">{{trans('general.gn_facultative_courses_groups')}}</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="">
                                <div class="form-group">
                                    <label>{{trans('general.gn_course_groups')}} *</label>
                                    <select id="course_group_sub" name="course_group_sub" class="form-control icon_control dropdown_control">
                                        <option value="">{{trans('general.gn_select')}}</option>
                                        @if(!empty($mdata->GPG))
                                        @foreach($GPG as $k => $v)
                                        <option @if($v->id == $mdata->GPG->pkGpg) selected @endif value="{{$v->id}}">{{$v->group_Name}}</option>
                                        @endforeach
                                        @elseif(!empty($mdata->OCG))
                                        @foreach($OCG as $k => $v)
                                        <option @if($v->id == $mdata->OCG->pkOcg) selected @endif value="{{$v->id}}">{{$v->group_Name}}</option>
                                        @endforeach
                                        @elseif(!empty($mdata->FLG))
                                        @foreach($FLG as $k => $v)
                                        <option @if($v->id == $mdata->FLG->pkFon) selected @endif value="{{$v->id}}">{{$v->group_Name}}</option>
                                        @endforeach
                                        @elseif(!empty($mdata->FCG))
                                        @foreach($FCG as $k => $v)
                                        <option @if($v->id == $mdata->FCG->pkFcg) selected @endif value="{{$v->id}}">{{$v->group_Name}}</option>
                                        @endforeach
                                        @endif
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="">
                                <div class="form-group">
                                    <label>{{trans('general.gn_courses')}} *</label>
                                    <select id="subject" name="subject" class="form-control icon_control dropdown_control select2_drop">
                                        <option value="">{{trans('general.gn_select')}}</option>
                                        @if(isset($mdata) && !empty($mdata->classStudents))
                                        @foreach($courses as $k => $v)
                                        @if(!empty($mdata->GPG) || !empty($mdata->OCG) || !empty($mdata->FCG))
                                        @if($v->crs_IsForeignLanguage == 'No')
                                        <option @if(isset($mdata->classStudents[0])) @if($v->pkCrs == $mdata->fkCgaCrs) selected @endif @endif value="{{$v->pkCrs}}">{{$v->crs_CourseName}}</option>
                                        @endif
                                        @else(!empty($courses->FLG))
                                        @if($v->crs_IsForeignLanguage == 'Yes' && isset($mdata->classStudents[0]))
                                        <option @if(isset($mdata->classStudents[0])) @if($v->pkCrs == $mdata->fkCgaCrs) selected @endif @endif value="{{$v->pkCrs}}">{{$v->crs_CourseName}}</option>
                                        @endif
                                        @endif
                                        @endforeach
                                        @endif
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="">
                                <div class="form-group">
                                    <label>{{trans('general.gn_school_year')}} </label>
                                    <select disabled id="fkCgaSye" name="fkCgaSye" class="form-control icon_control dropdown_control">
                                        <option value="">{{trans('general.gn_select')}}</option>
                                        @foreach($schoolYears as $k => $v)
                                        <option @if(!empty($selectedYear)) @if($selectedYear==$v->pkSye)
                                            selected
                                            @endif
                                            @else
                                            @if($v->sye_DefaultYear == 1)
                                            selected
                                            @endif
                                            @endif
                                            value="{{$v->pkSye}}">{{$v->sye_NameCharacter}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="">
                                <div class="form-group">
                                    <label>{{trans('general.gn_semester')}}</label>
                                    <select disabled id="fkCgaEdu" name="fkCgaEdu" class="form-control icon_control dropdown_control">
                                        <option value="">{{trans('general.gn_select')}}</option>
                                        @foreach($semesters as $k => $v)
                                        <option @if($selSem==$v->pkEdp) selected @endif value="{{$v->pkEdp}}">{{$v->edp_EducationPeriodName}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="">
                                <div class="form-group">
                                    <label>{{trans('general.gn_grade')}} </label>
                                    <select multiple="multiple" id="fkClrGra" name="fkClrGra[]" class="form-control icon_control dropdown_control select2_multi">
                                        @foreach($grades as $k => $v)
                                        <option @if(in_array($v->pkGra,$selGrades)) selected @endif value="{{$v->pkGra}}">{{$v->gra_GradeNumeric}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="">
                                <div class="form-group">
                                    <label>{{trans('general.gn_course_order')}} *</label>
                                    <select onchange="clearStuds()" id="course_order" name="course_order" class="form-control icon_control dropdown_control ">
                                        <option value="">{{trans('general.gn_select')}}</option>
                                        <option @if(isset($mdata) && $mdata->cga_OrderNumber == 1) selected @endif value="1">1</option>
                                        <option @if(isset($mdata) && $mdata->cga_OrderNumber == 2) selected @endif value="2">2</option>
                                        <option @if(isset($mdata) && $mdata->cga_OrderNumber == 3) selected @endif value="3">3</option>
                                        <option @if(isset($mdata) && $mdata->cga_OrderNumber == 4) selected @endif value="4">4</option>
                                        <option @if(isset($mdata) && $mdata->cga_OrderNumber == 5) selected @endif value="5">5</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-4 fcg_hour @if(empty($mdata->FCG) && empty($mdata->GPG)) hide_content @endif">
                            <div class="">
                                <div class="form-group">
                                    <label>{{trans('general.gn_week_hourly_rate')}} *</label>
                                    <input type="number" id="cg_hour" min="1" name="cg_hour" class="form-control" value="@if(!empty($mdata->FCG) || !empty($mdata->GPG)){{$mdata->cga_WeekHours}}@endif">
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-4">
                            <div class="">
                                <div class="form-group">
                                    <label>{{trans('general.gn_village_school')}}</label>
                                    @if(!empty($mdata->fkCgaViSch))
                                    @include('partials.dropdown.dropdowns',[
                                    'type'=>'village_school',
                                    'name'=>'fkCgaViSch',
                                    'selected'=>$mdata->fkCgaViSch,
                                    'extra_params'=>[
                                    "id"=>"fkCgaViSch",
                                    "school_id"=>$mainSchool
                                    ]
                                    ])
                                    @else
                                    @include('partials.dropdown.dropdowns',[
                                    'type'=>'village_school',
                                    'name'=>'fkCgaViSch',
                                    'selected'=>'',
                                    'extra_params'=>[
                                    "id"=>"fkCgaViSch",
                                    "school_id"=>$mainSchool
                                    ]
                                    ])
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-12">
                            <div class="row">
                                <div class="col-lg-3">
                                    <div class="form-group">
                                        <label>{{trans('general.gn_search_student')}}</label>
                                        <input type="text" id="search_student" maxlength="30" class="form-control icon_control search_control" placeholder="{{trans('general.gn_student')}} {{trans('general.gn_name')}}">
                                    </div>
                                </div>
                                <div class="col-lg-3">
                                    <div class="">
                                        <div class="form-group">
                                            <label>{{trans('general.gn_class')}} </label>
                                            <select id="fkClrCla" name="fkClrCla" class="form-control icon_control dropdown_control">
                                                <option value="">{{trans('general.gn_select')}}</option>
                                                @foreach($classes as $k => $v)
                                                <option value="{{$v->pkCla}}">{{$v->cla_ClassName}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-3 flg_ocg_add ">
                                    <div class="form-group">
                                        <label style="visibility: hidden; display: block;">{{trans('general.gn_search_student')}}</label>
                                        <button type="button" class="theme_btn" onclick="addAllStu()">{{trans('general.gn_add')}}</button>
                                    </div>
                                </div>
                            </div>
                            <div class="table-responsive mt-2">
                                <table id="student_listing" class="display" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th>{{trans('general.sr_no')}}.</th>
                                            <th>Id</th>
                                            <th>{{trans('general.gn_student_name')}}</th>
                                            <th>{{trans('general.gn_grade')}}</th>
                                            <th>{{trans('general.gn_class')}}</th>
                                            <th>{{trans('general.gn_education_plan')}}</th>
                                            <th>
                                                <div class="form-group form-check"><input type="checkbox" onclick="checkAllStu(this)" class="form-check-input checkAll"><label class="custom_checkbox"></label></div>
                                            </th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                            <p class="mt-2"><strong>{{trans('general.gn_selected_students')}}</strong></p>
                            <div class="table-responsive mt-2 mb-2">
                                <table class="color_table class_seleted_students sel_stu_elem @if(isset($mdata) && empty($mdata->classStudents)) hide_content @endif">
                                    <tbody>
                                        <tr>
                                            <th>{{trans('general.sr_no')}}</th>
                                            <th>ID</th>
                                            <th>{{trans('general.gn_name')}}</th>
                                            <th>{{trans('general.gn_grade')}}</th>
                                            <th>{{trans('general.gn_education_plan')}}</th>
                                            <th>
                                                <div class="action">{{trans('general.gn_action')}}</div>
                                            </th>
                                        </tr>
                                        @if(isset($mdata) && !empty($mdata->classStudents))
                                        @foreach($mdata->classStudents as $k => $v)
                                        <tr class="stu stu_{{$v->classSemester->fkSemSen}} epl_{{$v->classSemester->studentEnroll->fkSteEpl}} gra_{{$v->classSemester->studentEnroll->fkSteGra}}">
                                            <td>{{$k+1}}</td>
                                            <td>@if(!empty($v->classSemester->studentEnroll->student->stu_StudentID)){{$v->classSemester->studentEnroll->student->stu_StudentID}} @else {{$v->classSemester->studentEnroll->student->stu_TempCitizenId}}@endif</td>
                                            <td>{{$v->classSemester->studentEnroll->student->full_name}}</td>
                                            <td>{{$v->classSemester->studentEnroll->grade->gra_GradeNumeric}}</td>
                                            <td>{{$v->classSemester->studentEnroll->educationProgram->edp_Name}} - {{$v->classSemester->studentEnroll->educationPlan->epl_EducationPlanName}}</td>
                                            <td>
                                                <input type="hidden" name="clr_hrs[]" value="{{$mdata->cga_WeekHours}}">
                                                <input type="hidden" name="fkCtcCcs[]" value="{{$v->classSemester->fkSemCcs}}">
                                                <input type="hidden" name="stu_ids[]" value="{{$v->classSemester->fkSemSen}}">
                                                <input type="hidden" name="sem_ids[]" value="{{$v->fkCsaSem}}">
                                                <input type="hidden" name="gra_ids[]" value="{{$v->classSemester->studentEnroll->fkSteGra}}">
                                                <input type="hidden" name="epl_ids[]" value="{{$v->classSemester->studentEnroll->fkSteEpl}}">
                                                <a data-id="{{$v->classSemester->fkSemSen}}" onclick="removeSelStu(this)" href="javascript:void(0)" class="theme_btn red_btn min_btn">Delete</a>
                                            </td>
                                        </tr>
                                        @endforeach
                                        @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="col-lg-7">
                            <div class="">
                                <div class="form-group">
                                    <label>{{trans('general.gn_teacher')}}</label>
                                    <select required id="teacher" name="teacher[]" multiple="multiple" class="form-control icon_control dropdown_control select2_multi">
                                        @foreach($employees as $ek => $ev)
                                        <option @if(in_array($ev->pkEen,$selTeacher)) selected @endif value="{{$ev->pkEen}}">{{$ev->employee->emp_EmployeeName}} {{$ev->employee->emp_EmployeeSurname}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="text-center">
                                <button type="submit" class="theme_btn">{{trans('general.gn_submit')}}</button>
                                <a class="theme_btn red_btn no_sidebar_active" href="{{url('/employee/coursegroup')}}">{{trans('general.gn_cancel')}}</a>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<button id="trigger_edu_pop" class="hide_content" type="button" data-toggle="modal" data-target="#eplan_prompt">trigg</button>

<div class="theme_modal modal fade" id="eplan_prompt" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <img src="{{asset('images/ic_close_bg.png')}}" class="modal_top_bg">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <img src="{{asset('images/ic_close_circle_white.png')}}">
                </button>
                <form name="course_details">
                    <div class="row">
                        <div class="col-lg-1"></div>
                        <div class="col-lg-10">
                            <h5 class="modal-title" id="exampleModalCenterTitle">{{trans('general.gn_view_courses')}}</h5>
                            <div class="course_details">

                            </div>
                            <div class="text-center modal_btn ">
                                <button type="submit" class="theme_btn min_btn">{{trans('general.gn_submit')}}</button>
                                <button type="button" data-dismiss="modal" class="theme_btn red_btn min_btn">{{trans('general.gn_cancel')}}</button>
                            </div>
                        </div>
                        <div class="col-lg-1"></div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script>
    var listing_url = "{{route('fetch-coursegroup-lists')}}";

</script>
@endsection

@push('datatable-scripts')
<!-- Include this Page JS -->
<script type="text/javascript" src="{{ asset('js/dashboard/course_group.js') }}"></script>
@endpush
