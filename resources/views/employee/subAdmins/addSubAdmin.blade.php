@extends('layout.app_with_login')
@section('title', trans('sidebar.sidebar_nav_sub_admins'))
@section('script', asset('js/dashboard/school_sub_admin.js'))
@section('content')
 <!-- Page Content  -->
<div class="section">
    <div class="container-fluid">
            <div class="row">
                <div class="col-12 mb-3">
                   <h2 class="title"><span>{{trans('sidebar.sidebar_nav_user_management')}} > </span><a class="no_sidebar_active" href="{{url('/employee/subadmins')}}"><span>{{trans('sidebar.sidebar_nav_admin_staff')}} > </span></a> {{trans('general.gn_add')}}</h2>
                </div>
            </div>
            <input type="hidden" id="image_validation_msg" value="{{trans('message.msg_image_validation')}}">
            <div class="white_box">
                <div class="theme_tab">
                    <ul class="nav nav-tabs" id="myTab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">{{trans('general.gn_general_information')}}</a>
                        </li>
                        <input type="hidden" id="image_validation_msg" value="{{trans('message.msg_image_validation')}}">
                    </ul>
                    <div class="tab-content" id="myTabContent">
                        <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                            <div class="inner_tab" id="profile_detail">
                                <form id="add-subAdmin-form" name='add-subAdmin-form'>
                                <input type="hidden" id="sid" value="{{$mainSchool}}">
                                <div class="row">
                                    <div class="col-lg-1"></div>
                                    <div class="col-lg-10">
                                        <div class="text-center">
                                            <div class="profile_box">
                                                <div class="profile_pic">
                                                    <img id="user_img" src="{{ asset('images/user.png') }}">
                                                    <input type="hidden" id="img_tmp" value="{{ asset('images/user.png') }}">
                                                </div>
                                            </div>
                                            <div  class="upload_pic_link">
                                                <a href="javascript:void(0)">
                                                {{trans('general.gn_upload_photo')}}<input accept="image/jpeg,image/png" type="file" id="upload_profile" name="upload_profile"><br></a>

                                            </div>
                                        </div>
                                        <div class="container-fluid">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>{{trans('general.gn_first_name')}} *</label>
                                                        <input type="text" name="emp_EmployeeName" class="form-control" placeholder="{{trans('general.gn_enter')}} {{trans('general.gn_first_name')}} ">
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>{{trans('general.gn_last_name')}}  *</label>
                                                        <input type="text" name="emp_EmployeeSurname" class="form-control" placeholder="{{trans('general.gn_enter')}} {{trans('general.gn_last_name')}}">
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>{{trans('general.gn_email')}} *</label>
                                                        <input type="text" name="email" id="email" class="form-control" placeholder="{{trans('general.gn_enter')}} {{trans('general.gn_email')}}">
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>{{trans('general.gn_phone')}} *</label>
                                                        <input type="number" name="emp_PhoneNumber" id="emp_PhoneNumber" class="form-control" placeholder="{{trans('general.gn_enter')}} {{trans('general.gn_phone_number')}}">
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>{{trans('general.gn_employee')}} ID *</label>
                                                        <input type="text" name="emp_EmployeeID" id="emp_EmployeeID" class="form-control" placeholder="{{trans('general.gn_enter')}} ID">
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>{{trans('general.gn_temp_citizen_id')}} </label>
                                                        <input type="text" name="emp_TempCitizenId" id="emp_TempCitizenId" class="form-control" placeholder="{{trans('general.gn_enter')}} {{trans('general.gn_temp_citizen_id')}}">
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>{{trans('general.gn_gender')}}</label>
                                                        <select name="emp_EmployeeGender" id="emp_EmployeeGender" class="form-control icon_control dropdown_control">
                                                            <option value="Male">{{trans('general.gn_male')}}</option>
                                                            <option value="Female">{{trans('general.gn_female')}}</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>{{trans('general.gn_dob')}}</label>
                                                        <input  type="text" name="emp_DateOfBirth" id="emp_DateOfBirth" class="form-control icon_control date_control datepicker">
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>{{trans('general.gn_place_of_birth')}} *</label>
                                                        <input type="text" name="emp_PlaceOfBirth" id="emp_PlaceOfBirth" class="form-control" placeholder="{{trans('general.gn_enter')}} {{trans('general.gn_place_of_birth')}}">
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                    <label>{{trans('general.gn_MunicipalityofBirth')}} *</label>
                                                        <select name="fkEmpMun" id="fkEmpMun" class="form-control icon_control dropdown_control">
                                                        <option value="">{{trans('general.gn_select')}}</option>
                                                        @foreach($Municipalities as $k => $v)
                                                            <option data-countryId="{{$v->canton->state->country->pkCny}}" value="{{$v->pkMun}}">{{$v->mun_MunicipalityName}}, {{$v->canton->state->country->cny_CountryName}}</option>
                                                        @endforeach
                                                        </select>
                                                        <input type="hidden" id="fkEmpCny" name="fkEmpCny">
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>{{trans('general.gn_nationality')}} *</label>
                                                        <select name="fkEmpNat" class="form-control icon_control dropdown_control">
                                                        <option value="">{{trans('general.gn_select')}}</option>
                                                        @foreach($Nationalities as $k => $v)
                                                            <option value="{{$v->pkNat}}">{{$v->nat_NationalityName}}</option>
                                                        @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>{{trans('general.gn_religion')}}</label>
                                                        <select name="fkEmpRel" class="form-control icon_control dropdown_control">
                                                        <option value="">{{trans('general.gn_select')}}</option>
                                                        @foreach($Religions as $k => $v)
                                                            <option value="{{$v->pkRel}}">{{$v->rel_ReligionName}}</option>
                                                        @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>{{trans('general.gn_citizenship')}} *</label>
                                                        <select name="fkEmpCtz" class="form-control icon_control dropdown_control">
                                                        <option value="">{{trans('general.gn_select')}}</option>
                                                        @foreach($Citizenships as $k => $v)
                                                            <option value="{{$v->pkCtz}}">{{$v->ctz_CitizenshipName}}</option>
                                                        @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                    <label>{{trans('general.gn_ResidencePostoffice')}} *</label>
                                                        <select name="fkEmpPof" class="form-control icon_control dropdown_control">
                                                        <option value="">{{trans('general.gn_select')}}</option>
                                                        @foreach($PostalCodes as $k => $value)
                                                            <option value="{{$value->pkPof}}">{{$value->pof_PostOfficeName}} {{$value->pof_PostOfficeNumber}} ({{$value->municipality->mun_MunicipalityName}}, {{$value->municipality->canton->state->country->cny_CountryName}})</option>
                                                        @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>{{trans('general.gn_address')}}</label>
                                                        <input type="text" name="emp_Address" class="form-control" placeholder="{{trans('general.gn_enter')}} {{trans('general.gn_address')}}">
                                                    </div>
                                                </div>
                                                <!-- <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>{{trans('general.gn_status')}} *</label>
                                                        <select class="form-control dropdown_control icon_control" name="emp_Status" id="emp_Status">
                                                            <option value="">{{trans('general.gn_select')}}</option>
                                                            <option value="Active">{{trans('general.gn_active')}}</option>
                                                            <option value="Inactive">{{trans('general.gn_inactive')}}</option>
                                                        </select>
                                                    </div>
                                                </div> -->
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>{{trans('general.gn_note')}}</label>
                                                        <input type="text" name="emp_Notes" class="form-control" placeholder="{{trans('general.gn_enter')}} {{trans('general.gn_note')}}">
                                                    </div>
                                                </div>
                                            </div>
                                            <br><br>
                                            <div class="row">
                                                {{-- <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>{{trans('general.gn_date_of_enrollment')}} *</label>
                                                        <input  type="text" id="eng_start_date" name="eng_start_date" class="form-control datepicker icon_control date_control">
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>{{trans('general.gn_date_of_engagement_end')}} </label>
                                                        <input  type="text" id="eng_end_date" name="eng_end_date" class="form-control datepicker icon_control date_control">
                                                    </div>
                                                </div> --}}
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>{{trans('general.gn_start_date')}} *</label>
                                                        <input  type="text" id="start_date" name="start_date" class="form-control datepicker icon_control date_control">
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                      <label>{{trans('general.gn_employee_type')}} *</label>
                                                      <select id="fkEenEpty" name="fkEenEpty" class="form-control icon_control dropdown_control">
                                                        <option value="">{{trans('general.gn_select')}}</option>
                                                        @foreach($employeeType as $k => $v)
                                                          <option value="{{$v->pkEpty}}">
                                                            @if(!empty($v->epty_subCatName) && $v->epty_subCatName=='Clerk')
                                                                {{trans('general.gn_clerk')}}
                                                            @elseif($v->epty_Name=='SchoolSubAdmin') {{trans('general.gn_school_sub_admin')}}
                                                            @endif
                                                          </option>
                                                        @endforeach
                                                      </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                      <label>{{trans('general.gn_type_of_engagement')}} *</label>
                                                      <select id="fkEenEty" name="fkEenEty" class="form-control icon_control dropdown_control">
                                                        <option value="">{{trans('general.gn_select')}}</option>
                                                        @foreach($EngagementTypes as $k => $v)
                                                          <option value="{{$v->pkEty}}">{{$v->ety_EngagementTypeName}}</option>
                                                        @endforeach
                                                      </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                      <label>{{trans('general.gn_hourly_rate')}} *</label>
                                                      <input type="number" name="ewh_WeeklyHoursRate" id="ewh_WeeklyHoursRate" class="form-control">
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                      <label>{{trans('general.gn_hourly_note')}} </label>
                                                      <input type="text" name="ewh_Notes" id="ewh_Notes" class="form-control">
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                      <label>{{trans('general.gn_enagement_note')}} </label>
                                                      <input type="text" name="een_Notes" id="een_Notes" class="form-control">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="text-center">
                                             <div class="text-center">
                                                <button type="submit" class="theme_btn">{{trans('general.gn_submit')}}</button>
                                                <a class="theme_btn red_btn no_sidebar_active" href="{{url('/employee/subadmins')}}">{{trans('general.gn_cancel')}}</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-1"></div>
                                </div>
                            </form>
                            </div>

                        </div>
                        <div class="tab-pane fade show" id="Current" role="tabpanel" aria-labelledby="Current-tab">
                            <div class="inner_tab" id="profile_detail2">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="card-grey">
                                            <div class="row">
                                                <div class="col-12 mb-2">
                                                    <strong>1. School</strong>
                                                </div>
                                                <div class="col-lg-3 col-md-6">
                                                    <div class="form-group form-check">
                                                        <input type="checkbox" class="form-check-input" id="exampleCheck1" checked="">
                                                        <label class="custom_checkbox"></label>
                                                        <label class="form-check-label label-text" for="exampleCheck1"><strong>View</strong></label>
                                                    </div>
                                                </div>
                                                <div class="col-lg-3 col-md-6">
                                                    <div class="form-group form-check">
                                                        <input type="checkbox" class="form-check-input" id="exampleCheck1">
                                                        <label class="custom_checkbox"></label>
                                                        <label class="form-check-label label-text" for="exampleCheck1"><strong>Add</strong></label>
                                                    </div>
                                                </div>
                                                <div class="col-lg-3 col-md-6">
                                                    <div class="form-group form-check">
                                                        <input type="checkbox" class="form-check-input" id="exampleCheck1">
                                                        <label class="custom_checkbox"></label>
                                                        <label class="form-check-label label-text" for="exampleCheck1"><strong>Edit</strong></label>
                                                    </div>
                                                </div>
                                                <div class="col-lg-3 col-md-6">
                                                    <div class="form-group form-check">
                                                        <input type="checkbox" class="form-check-input" id="exampleCheck1">
                                                        <label class="custom_checkbox"></label>
                                                        <label class="form-check-label label-text" for="exampleCheck1"><strong>Delete</strong></label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="card-grey">
                                            <div class="row">
                                                <div class="col-12 mb-2">
                                                    <strong>2. User Management</strong>
                                                </div>
                                                <div class="col-lg-3 col-md-6">
                                                    <div class="form-group form-check">
                                                        <input type="checkbox" class="form-check-input" id="exampleCheck1" checked="">
                                                        <label class="custom_checkbox"></label>
                                                        <label class="form-check-label label-text" for="exampleCheck1"><strong>View</strong></label>
                                                    </div>
                                                </div>
                                                <div class="col-lg-3 col-md-6">
                                                    <div class="form-group form-check">
                                                        <input type="checkbox" class="form-check-input" id="exampleCheck1">
                                                        <label class="custom_checkbox"></label>
                                                        <label class="form-check-label label-text" for="exampleCheck1"><strong>Add</strong></label>
                                                    </div>
                                                </div>
                                                <div class="col-lg-3 col-md-6">
                                                    <div class="form-group form-check">
                                                        <input type="checkbox" class="form-check-input" id="exampleCheck1">
                                                        <label class="custom_checkbox"></label>
                                                        <label class="form-check-label label-text" for="exampleCheck1"><strong>Edit</strong></label>
                                                    </div>
                                                </div>
                                                <div class="col-lg-3 col-md-6">
                                                    <div class="form-group form-check">
                                                        <input type="checkbox" class="form-check-input" id="exampleCheck1">
                                                        <label class="custom_checkbox"></label>
                                                        <label class="form-check-label label-text" for="exampleCheck1"><strong>Delete</strong></label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="card-grey">
                                            <div class="row">
                                                <div class="col-12 mb-2">
                                                    <strong>3. Organization</strong>
                                                </div>
                                                <div class="col-lg-3 col-md-6">
                                                    <div class="form-group form-check">
                                                        <input type="checkbox" class="form-check-input" id="exampleCheck1" checked="">
                                                        <label class="custom_checkbox"></label>
                                                        <label class="form-check-label label-text" for="exampleCheck1"><strong>View</strong></label>
                                                    </div>
                                                </div>
                                                <div class="col-lg-3 col-md-6">
                                                    <div class="form-group form-check">
                                                        <input type="checkbox" class="form-check-input" id="exampleCheck1">
                                                        <label class="custom_checkbox"></label>
                                                        <label class="form-check-label label-text" for="exampleCheck1"><strong>Add</strong></label>
                                                    </div>
                                                </div>
                                                <div class="col-lg-3 col-md-6">
                                                    <div class="form-group form-check">
                                                        <input type="checkbox" class="form-check-input" id="exampleCheck1">
                                                        <label class="custom_checkbox"></label>
                                                        <label class="form-check-label label-text" for="exampleCheck1"><strong>Edit</strong></label>
                                                    </div>
                                                </div>
                                                <div class="col-lg-3 col-md-6">
                                                    <div class="form-group form-check">
                                                        <input type="checkbox" class="form-check-input" id="exampleCheck1">
                                                        <label class="custom_checkbox"></label>
                                                        <label class="form-check-label label-text" for="exampleCheck1"><strong>Delete</strong></label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="card-grey">
                                            <div class="row">
                                                <div class="col-12 mb-2">
                                                    <strong>4. Exam Result</strong>
                                                </div>
                                                <div class="col-lg-3 col-md-6">
                                                    <div class="form-group form-check">
                                                        <input type="checkbox" class="form-check-input" id="exampleCheck1" checked="">
                                                        <label class="custom_checkbox"></label>
                                                        <label class="form-check-label label-text" for="exampleCheck1"><strong>View</strong></label>
                                                    </div>
                                                </div>
                                                <div class="col-lg-3 col-md-6">
                                                    <div class="form-group form-check">
                                                        <input type="checkbox" class="form-check-input" id="exampleCheck1">
                                                        <label class="custom_checkbox"></label>
                                                        <label class="form-check-label label-text" for="exampleCheck1"><strong>Add</strong></label>
                                                    </div>
                                                </div>
                                                <div class="col-lg-3 col-md-6">
                                                    <div class="form-group form-check">
                                                        <input type="checkbox" class="form-check-input" id="exampleCheck1">
                                                        <label class="custom_checkbox"></label>
                                                        <label class="form-check-label label-text" for="exampleCheck1"><strong>Edit</strong></label>
                                                    </div>
                                                </div>
                                                <div class="col-lg-3 col-md-6">
                                                    <div class="form-group form-check">
                                                        <input type="checkbox" class="form-check-input" id="exampleCheck1">
                                                        <label class="custom_checkbox"></label>
                                                        <label class="form-check-label label-text" for="exampleCheck1"><strong>Delete</strong></label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="card-grey">
                                            <div class="row">
                                                <div class="col-12 mb-2">
                                                    <strong>5. Attendance</strong>
                                                </div>
                                                <div class="col-lg-3 col-md-6">
                                                    <div class="form-group form-check">
                                                        <input type="checkbox" class="form-check-input" id="exampleCheck1" checked="">
                                                        <label class="custom_checkbox"></label>
                                                        <label class="form-check-label label-text" for="exampleCheck1"><strong>View</strong></label>
                                                    </div>
                                                </div>
                                                <div class="col-lg-3 col-md-6">
                                                    <div class="form-group form-check">
                                                        <input type="checkbox" class="form-check-input" id="exampleCheck1">
                                                        <label class="custom_checkbox"></label>
                                                        <label class="form-check-label label-text" for="exampleCheck1"><strong>Add</strong></label>
                                                    </div>
                                                </div>
                                                <div class="col-lg-3 col-md-6">
                                                    <div class="form-group form-check">
                                                        <input type="checkbox" class="form-check-input" id="exampleCheck1">
                                                        <label class="custom_checkbox"></label>
                                                        <label class="form-check-label label-text" for="exampleCheck1"><strong>Edit</strong></label>
                                                    </div>
                                                </div>
                                                <div class="col-lg-3 col-md-6">
                                                    <div class="form-group form-check">
                                                        <input type="checkbox" class="form-check-input" id="exampleCheck1">
                                                        <label class="custom_checkbox"></label>
                                                        <label class="form-check-label label-text" for="exampleCheck1"><strong>Delete</strong></label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="card-grey">
                                            <div class="row">
                                                <div class="col-12 mb-2">
                                                    <strong>6. Reports</strong>
                                                </div>
                                                <div class="col-lg-3 col-md-6">
                                                    <div class="form-group form-check">
                                                        <input type="checkbox" class="form-check-input" id="exampleCheck1" checked="">
                                                        <label class="custom_checkbox"></label>
                                                        <label class="form-check-label label-text" for="exampleCheck1"><strong>View</strong></label>
                                                    </div>
                                                </div>
                                                <div class="col-lg-3 col-md-6">
                                                    <div class="form-group form-check">
                                                        <input type="checkbox" class="form-check-input" id="exampleCheck1">
                                                        <label class="custom_checkbox"></label>
                                                        <label class="form-check-label label-text" for="exampleCheck1"><strong>Add</strong></label>
                                                    </div>
                                                </div>
                                                <div class="col-lg-3 col-md-6">
                                                    <div class="form-group form-check">
                                                        <input type="checkbox" class="form-check-input" id="exampleCheck1">
                                                        <label class="custom_checkbox"></label>
                                                        <label class="form-check-label label-text" for="exampleCheck1"><strong>Edit</strong></label>
                                                    </div>
                                                </div>
                                                <div class="col-lg-3 col-md-6">
                                                    <div class="form-group form-check">
                                                        <input type="checkbox" class="form-check-input" id="exampleCheck1">
                                                        <label class="custom_checkbox"></label>
                                                        <label class="form-check-label label-text" for="exampleCheck1"><strong>Delete</strong></label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="text-center">
                                             <div class="text-center">
                                                <button type="submit" class="theme_btn">{{trans('general.gn_submit')}}</button>
                                                <button type="button" class="theme_btn red_btn">{{trans('general.gn_cancel')}}</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
</div>
<script>
    var listing_url = "{{route('fetch-subadmins-lists')}}";
</script>
@endsection

@push('datatable-scripts')
<!-- Include this Page JS -->
<script type="text/javascript" src="{{ asset('js/dashboard/school_sub_admin.js') }}"></script>
@endpush
