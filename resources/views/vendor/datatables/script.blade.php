$(function(){window.{{ config('datatables-html.namespace', 'LaravelDataTables') }}=window.{{ config('datatables-html.namespace', 'LaravelDataTables') }}||{};window.{{ config('datatables-html.namespace', 'LaravelDataTables') }}["%1$s"]=$("#%1$s").on('processing.dt', function (e, settings, processing) {
    if (processing) {
      showLoader(true);
    } else {
      showLoader(false);
    }
}).DataTable(%2$s);});
