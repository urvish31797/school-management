<!DOCTYPE html>
<html lang="{{$current_language}}" xml:lang="{{$current_language}}">
<head>
    <meta charset="utf-8">
    <meta name="csrf-token" content="{{ @csrf_token() }}">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>@yield('title') - Hertronic</title>
    <link rel="icon" href="{{ asset('images/favicon.png') }}">
    @if(config('services.APP_MIN'))
    <link rel="stylesheet" type="text/css" href="{{ asset('css/theme_components.min.css') }}" />
    <link rel="stylesheet" href="{{ asset('css/custom.min.css') }}">
    @else
    <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/toastr.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/jquery.dataTables.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/select2/select2.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/msdropdown/dd.min.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/custom.css') }}">
    <link rel="stylesheet" href="{{ asset('css/custom-rt.css') }}">
    @endif

    @stack('custom-styles')
</head>
<body class="hold-transition sidebar-mini @if(Session::has('previous_login')) no_scroll @endif @if($logged_user->utype == 'employee') is_employee @endif">
    <div id="preloader">
        <div id="status">
            <div class="spinner"></div>
        </div>
    </div>
    <div class="inner-container" id="contents" style="opacity: 1;">
        @if (Session::has('middleware_error'))
        <input type="hidden" id="error_msg" value="{!! session('middleware_error') !!}">
        @endif
        @if(Session::has('previous_user'))
        <?php
      $type = '';
      $schoolId = '';
      $uimg = asset('images/user.png');
      if($logged_user->utype == 'admin'){
        $type = trans('general.gn_ministry_super_admin');
        if(!empty($logged_user->adm_Photo)){
          $uimg = asset('images/users/').'/'.$logged_user->adm_Photo;
        }
        $name = $logged_user->adm_Name;
      }else{
        if($logged_user->type == 'SchoolCoordinator'){
          $type = trans('general.gn_school_coordinator');
        }elseif($logged_user->type == 'Teacher'){
          $type = trans('general.gn_teacher');
        }elseif($logged_user->type == 'HomeroomTeacher'){
          $type = trans('general.gn_homeroom_teacher');
        }elseif($logged_user->type == 'Principal'){
          $type = trans('general.gn_principal');
        }
        if(!empty($logged_user->emp_PicturePath)){
          $uimg = asset('images/users/').'/'.$logged_user->emp_PicturePath;
        }
        $name = $logged_user->emp_EmployeeName." ".$logged_user->emp_EmployeeSurname;
        $schoolId = $logged_user->sid;
      }
    ?>
        <div class="sticky_login" data-schid="{{$schoolId}}">
            <div class="profile-cover">
                <img alt="User Photo" id="logged_user_img" src="{{$uimg}}">
            </div>
            <p>{{trans('general.gn_logged_in_as')}} {{$name}} - {{$type}}</p>
        </div>
        @endif
        <div class="wrapper @if(Session::has('previous_user')) loginAs @endif">

            <input type="hidden" id="web_base_url" value="{{url('/')}}">
            <!-- Left side column. contains the logo and sidebar -->
            {!!Session::get('previous_login')!!}
            @include('common.sidebar')
            <div id="content">
                <div id="preloader_new" style="opacity: 0; display: none;">
                    <div id="status_new">
                        <div class="spinner"></div>
                    </div>
                </div>
                <div class="overlay" onclick="closeOverlay()"></div>
                @include('common.header')
                @yield('content')
            </div>

        </div>
    </div>

    <!-- ./wrapper -->
    @include('layout.multi_language_message')
    <!-- jQuery 3 -->
    <script type="text/javascript" src="{{ asset('js/jquery.min.js')}}"></script>
    @if(config('services.APP_MIN'))
    <script type="text/javascript" src="{{ asset('js/theme_components.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/custom.min.js') }}"></script>
    @else
    <script type="text/javascript" src="{{ asset('js/all.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/popper.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/bootstrap.min.js') }}"></script>
    <script defer type="text/javascript" src="{{ asset('js/toastr.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/jquery.dataTables.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/jquery.validate.js') }}"></script>
    <script type="text/javascript" defer src="{{ asset('js/custom_validation_msg.js') }}"></script>
    <script type="text/javascript" src="{{ asset('plugins/select2/select2.full.min.js')}}"></script>
    <script defer type="text/javascript" src="{{ asset('plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/msdropdown/jquery.dd.min.js') }}"></script>
    <script src="{{ asset('plugins/jquery-slimscroll/jquery.slimscroll.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/custom.js') }}"></script>
    @endif

    <script type="text/javascript">
        var base_url = '{{url('/') }}';

        $(function() {
            showLoader(false);
        });

        if ($('#error_msg').val()) {
            toastr.error($('#error_msg').val());
        }

        $('.select2').select2({
            "language": {
                "noResults": function() {
                    return $('#no_result_text').val();
                }
            }
        , });

        $(".slim_scroll").slimScroll({
            size: '8px'
            , width: '100%'
            , height: '100%'
            , color: '#A9A9A9'
            , allowPageScroll: true
            , alwaysVisible: true
        });

    </script>
    @stack('custom-scripts')

    <div id="dataTable_script">
        @stack('datatable-scripts')
    </div>

</body>
</html>
