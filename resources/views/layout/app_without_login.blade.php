<!DOCTYPE html>
<html lang="en" xml:lang="en">
<head>
  <meta charset="utf-8">
  <meta name="csrf-token" content="{{ @csrf_token() }}">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>@yield('title') - Hertronic</title>
  <!-- Tell the browser to be responsive to screen width -->
  <link rel="icon" href="{{ asset('images/favicon.png') }}">
  @if(config('services.APP_MIN'))
  <link rel="stylesheet" type="text/css" href="{{ asset('css/login.min.css') }}" />
  <link rel="stylesheet" href="{{ asset('css/custom.min.css') }}">
  @else
  <link rel="stylesheet" type="text/css" href="{{ asset('css/bootstrap.min.css') }}">
  <link rel="stylesheet" type="text/css" href="{{ asset('css/custom.css') }}">
  <link rel="stylesheet" type="text/css" href="{{ asset('css/msdropdown/dd.min.css') }}" />
  @endif

  <style type="text/css">
    .alert a{
      text-decoration: none;
    }
    .alert{
        margin: 25px;
    }
    .alert.alert-danger {
      top: 2pc !important;
    }
    .alert.alert-success {
      top: 2pc !important;
    }
  </style>
  @stack('custom-styles')
</head>
<body class="hold-transition login-page">
  @if (Session::has('middleware_error'))
      <input type="hidden" id="error_msg" value="{!! session('middleware_error') !!}">
  @endif
  <div id="preloader"><div id="status"><div class="spinner"></div></div></div>
  <div class="auth_container" id="contents" style="opacity: 0;">
    <div class="auth_logo text-center">
        <a href="{{url('/')}}"><img src="{{ asset('images/ic_login_logo.png') }}" alt="Hetronic Logo" class="login_logo"></a>
    </div>
@yield('content')
<!-- /.login-box -->
</div>

<input type="hidden" id="web_base_url" value="{{url('/')}}">
<input type="hidden" id="field_required_txt" value="{{trans('validation.validate_field_required')}}">
<input type="hidden" id="email_validate_txt" value="{{trans('validation.validate_email_field')}}">
<input type="hidden" id="minlength_validate_txt" value="{{trans('validation.validate_minlength')}}">
<input type="hidden" id="maxlength_validate_txt" value="{{trans('validation.validate_maxlength')}}">
<input type="hidden" id="validate_password_txt" value="{{trans('validation.validate_password')}}">
<input type="hidden" id="validate_password_equalto_txt" value="{{trans('validation.validate_password_equalto')}}">
<input type="hidden" id="validate_equalto_txt" value="{{trans('validation.validate_equalto')}}">

<script type="text/javascript" src="{{ asset('js/jquery.min.js')}}"></script>
@if(config('services.APP_MIN'))
<script type="text/javascript" src="{{ asset('js/login.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/custom.min.js') }}"></script>
@else
<script type="text/javascript" src="{{ asset('js/all.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/custom.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/msdropdown/jquery.dd.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/jquery.validate.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/custom_validation_msg.js') }}"></script>
@endif
<script type="text/javascript">
    var base_url = '{{ url('/') }}';
    //show middleware authentication error
    if ($('#error_msg').val()) {
        toastr.error($('#error_msg').val());
    }
    //end
    $(document).ready(function() {
      $("#language_drop_down").msDropdown();
    })
</script>
@stack('custom-scripts')
</body>
</html>