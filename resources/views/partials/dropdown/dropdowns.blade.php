@php
$extra_params['lang'] = $current_language;
$extra_params['class'] = "form-control icon_control dropdown_control";
@endphp

@switch($type)

@case("country")
{{ Form::selectCountry($name, $selected, $extra_params) }}
@break

@case("canton")
{{ Form::selectCanton($name, $selected, $extra_params) }}
@break

@case("canton_with_state")
{{ Form::selectCantonwithState($name, $selected, $extra_params) }}
@break

@case("municipality")
{{ Form::selectMunicipality($name, $selected, $extra_params) }}
@break

@case("university")
{{ Form::selectUniversities($name, $selected, $extra_params) }}
@break

@case("education_period")
{{ Form::selectEducationPeriod($name, $selected, $extra_params) }}
@break

@case("ownership_type")
{{ Form::selectOwnershipType($name, $selected, $extra_params) }}
@break

@case("village_school")
{{ Form::selectVillageSchool($name, $selected, $extra_params) }}
@break

@case("attendance_way")
{{ Form::selectAttendanceWay($name, $selected, $extra_params) }}
@break

@case("education_way")
{{ Form::selectEducationWay($name, $selected, $extra_params) }}
@break

@case("canton_employees")
{{ Form::selectCantonEmployee($name, $selected, $extra_params) }}
@break

@case("school_with_canton_employees")
{{ Form::selectSchoolwithCantonEmployees($name, $selected, $extra_params) }}
@break

@case("shifts")
{{ Form::selectShifts($name, $selected, $extra_params) }}
@break

@case("periodic_exam_type")
{{ Form::selectPeriodicExamType($name, $selected, $extra_params) }}
@break

@case("periodic_exam_way")
{{ Form::selectPeriodicExamWay($name, $selected, $extra_params) }}
@break

@endswitch
