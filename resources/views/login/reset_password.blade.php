@extends('layout.app_without_login')
@section('title','Reset Password')
@section('content')

  <div class="auth_box">

    <form action="{{ url('admin/change-password-submit') }}" method="post" id="loginForm" name="loginForm">
      {{ csrf_field() }}
      <div class="row">
          <div class="col-lg-1"></div>
          <div class="col-lg-10">
            <h1>{{trans('reset.rp_reset_password')}}</h1>
            <input type="hidden" name="token" id="token" value="{{$token}}">
            <div class="form-group">
              <label>{{trans('reset.rp_password')}}</label>
              <input type="password" placeholder="{{trans('general.gn_new_password')}}" class="form-control" name="new_password" id="new_password">
            </div>

            <div class="form-group">
              <label>{{trans('general.gn_confirm_password')}}</label>
              <input type="password" placeholder="{{trans('general.gn_confirm_password')}}" class="form-control" name="confirm_password" id="confirm_password">
            </div>
            <div class="text-center">
              <button type="submit" class="theme_btn auth_btn">{{trans('general.gn_confirm')}}</button>
              <p><a href="{{ url('login') }}" class="auth_link">{{trans('general.gn_login')}}</a></p>
            </div>
            <div class="text-center process_msg"></div>
            <div class="row">
              <div class="col-lg-9">
                <div class="text-center">© Copyright {{ now()->year }} Hertronic | All Rights Reserved</div>
              </div>
              <div class="col-lg-3">
                <div class="custom-drop-down" id="custom-flag-drop-down">
                  <select onchange='langSwitch(this.value)' style="width:130px;">
                      @foreach($languages as $k => $v)
                        <option @if($current_language==$v->language_key) selected @endif value='{{$v->language_key}}' class="custom_flag {{$v->language_key}}" style="background-image:url({{asset('images/languages')}}/{{$v->flag}});" data-title="{{$v->language_name}}">{{$v->language_name}}</option>
                      @endforeach
                  </select>
                </div>
              </div>
            </div>
          </div>
          <div class="col-lg-1"></div>
      </div>
    </form>
  </div>
@endsection
@push('custom-styles')
<!-- Include this Page CSS -->
<link link rel="stylesheet" type="text/css" href="{{ asset('css/toastr.min.css') }}">
@endpush
@push('custom-scripts')
  <script type="text/javascript" src="{{ asset('js/login/reset_password.js') }}"></script>
  <script type="text/javascript" src="{{ asset('js/toastr.min.js') }}"></script>
@endpush