@extends('layout.app_without_login')
@section('title','Login')
@section('content')
    <div class="auth_box">
      <form action="{{ url('login') }}" method="post" id="loginForm" name="loginForm">
        {{ csrf_field() }}
        <div class="row">
          <div class="col-lg-1"></div>
          <div class="col-lg-10">
            <h1>{{trans('login.ln_login')}}</h1>
            <div class="form-group">
              <label>{{trans('login.ln_email')}}</label>
              <input type="text" name="email" class="form-control" placeholder="{{trans('login.ln_enter_email')}}">
            </div>
            <div class="form-group">
              <label>{{trans('login.ln_password')}}</label>
              <input type="password" name="password" class="form-control" placeholder="{{trans('login.ln_enter_password')}}">
            </div>
            <div class="text-center">
              <button type="submit" class="theme_btn auth_btn">{{trans('login.ln_login')}}</button>
              <p><a href="{{ url('forgot-pass') }}" class="auth_link">{{trans('login.ln_forgot_password')}}?</a></p>
            </div>
            <div class="text-center process_msg">
              @if(session()->has('success'))
                  <p class="green_msg">
                      {{ session()->get('success') }}
                  </p>
              @endif
              @if ($errors->any())
                @foreach ($errors->all() as $error)
                  <p class="error_msg">{{ $error }}</p>
                @endforeach
              @endif
            </div>
            <div class="row">
              <div class="col-lg-9">
                <div class="text-center">© Copyright {{ now()->year }} Hertronic | All Rights Reserved</div>
              </div>
              <div class="col-lg-3">
                  <div class="custom-drop-down" id="custom-flag-drop-down">
                    <select onchange='langSwitch(this.value)' style="width:130px;">
                        @foreach($languages as $k => $v)
                          <option @if($current_language==$v->language_key) selected @endif value='{{$v->language_key}}' class="custom_flag {{$v->language_key}}" style="background-image:url({{asset('images/languages')}}/{{$v->flag}});" data-title="{{$v->language_name}}">{{$v->language_name}}</option>
                        @endforeach
                    </select>
                  </div>
              </div>
            </div>

          </div>
          <div class="col-lg-1"></div>
        </div>
      <img alt="" src="{{ asset('images/ic_login_shape1.png') }}" class="login_botton_shape">
    </form>
    </div>

@endsection
@push('custom-scripts')
  <script type="text/javascript" src="{{ asset('js/login/login.js') }}"></script>
@endpush