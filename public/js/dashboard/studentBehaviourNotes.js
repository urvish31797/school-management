/**
* Attendance & Accomplishment
*
* This file is used for admin JS
*
* @package    Laravel
* @subpackage JS
* @since      1.0
*/

$(function() {
$('#behaviour_listing').on( 'processing.dt', function ( e, settings, processing ) {
    if(processing){
      showLoader(true);
    }else{
      showLoader(false);
    }
  }).DataTable({
    "language": {
      "sLengthMenu": $('#show_txt').val()+" _MENU_ "+$('#entries_txt').val(),
      "info": $('#showing_txt').val()+" _START_ "+$('#to_txt').val()+" _END_ "+$('#of_txt').val()+" _TOTAL_ "+$('#entries_txt').val(),
      "emptyTable": $('#msg_no_data_available_table').val(),
      "paginate": {
        "previous": $('#previous_txt').val(),
        "next": $('#next_txt').val()
      }
    },
    "lengthMenu": [100,200,300,500],
    "searching": false,
    "serverSide": true,
    "deferRender": true,
    "ajax": {
      "url": student_behaviour_listing_url,
      "type": "POST",
      "dataType": 'json',
      "data": function ( d ) {
        d.search = $('#search_student').val();
        d.pkDbl = $("#pkDbl").val();
      }
    },
    columns:[
        { "data": "index",className: "text-center"},
        { "data": "studentId"},
        { "data": "stu_StudentName"},
        { "data": "sae_StudentTDesc",sortable:!1},
  ],

});
});