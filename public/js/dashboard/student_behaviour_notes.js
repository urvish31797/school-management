var form = $("form[name='add-student-behaviour-form']");
function save() {
    showLoader(true);
    var formData = new FormData($(form)[0]);
    $.ajax({
        url: save_behaviour_notes_url,
        type: 'POST',
        processData: false,
        contentType: false,
        cache: false,
        data: formData,
        success: function (result) {
            toastr.success(result.message);
            redirectPage('employee/attendance-accomplishment');
        },
        error: function (data) {
            toastr.error($('#something_wrong_txt').val());
        }
    });
    showLoader(false);
}