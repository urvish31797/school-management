var page_url = base_url + '/admin/certificateblueprints';
var form = $("form[name='add-certificate-blueprint-form']");

$(function () {

  $("#search_certificate_blueprints").on('keyup', function () {
    $('.certificate_blueprints_listing').DataTable().ajax.reload();
  });

  $('#delete_prompt').on('hidden.bs.modal', function () {
    $("#did").val('');
  })

  $('#cbp_keywords').keyup(function () {
    $(this).val($(this).val().toUpperCase());
  });

  $("#cbp_startdate").datepicker({
    format: "dd/mm/yyyy",
    autoclose: true,
  });

  $('#cbp_enddate').datepicker({
    format: "dd/mm/yyyy",
    autoclose: true,
  });

});

function save() {
  if (form.valid()) {
    showLoader(true);
    var formData = new FormData(form[0]);
    formData.append("pkCbp", $("#pkCbp").val());
    $.ajax({
      url: page_url,
      type: 'POST',
      processData: false,
      contentType: false,
      cache: false,
      data: formData,
      success: function (result) {

        toastr.success(result.message);
        $('#add_new').modal('hide');
        setTimeout(() => {
          redirectPage("admin/certificateblueprints");
        }, 1000);

      },
      error: function (jqXHR) {
        if (jqXHR.status == 400) {
          toastr.error(jqXHR.responseJSON.message);
        }
        else if (jqXHR.status == 422) {
          setvalidationmessages(jqXHR.responseJSON.errors);
        }
        else {
          toastr.error($('#something_wrong_txt').val());
        }
      }
    });
    showLoader(false);
  }
}

function confirmDelete() {
  showLoader(true);
  var pkCbp = $('#did').val();
  $.ajax({
    url: page_url + '/' + pkCbp,
    type: 'DELETE',
    dataType: 'json',
    cache: false,
    success: function (result) {
      $('#delete_prompt').modal('hide');
      toastr.success(result.message);
      $('.certificate_blueprints_listing').DataTable().ajax.reload();
    },
    error: function (jqXHR) {
      if (jqXHR.status == 400) {
        toastr.error(jqXHR.responseJSON.message);
      }
      else {
        toastr.error($('#something_wrong_txt').val());
      }
    }
  });
  showLoader(false);
}

function triggerDelete(pkCbp) {
  $('#did').val(pkCbp);
  $(".show_delete_modal").click();
}

$('#PreviewModal').on('hidden.bs.modal', function () {
  $('#previewsection').html('');
});

function preview(key) {
  var html = $("#cbp_htmlContent_" + key).val();
  if (html != "") {
    $('#previewsection').html(html);
    $('.show_preview_modal').click();
  }
  else {
    toastr.error($('#something_wrong_txt').val());
  }
}
