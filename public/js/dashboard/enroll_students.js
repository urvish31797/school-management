/**
* Enroll Students
*
* This file is used for admin JS
*
* @package    Laravel
* @subpackage JS
* @since      1.0
*/

$(function () {
  showLoader(false);
  $('.datepicker').datepicker({ format: "dd/mm/yyyy", autoclose: true });

  $("#enroll_stu_listing").on('DOMNodeInserted DOMNodeRemoved', function () {
    if ($(this).find('tbody tr td').first().attr('colspan')) {
      $(this).parent().hide();
    } else {
      $(this).parent().show();
    }
  });

  $('#enroll_stu_listing').on('processing.dt', function (e, settings, processing) {
    if (processing) {
      showLoader(true);
    } else {
      showLoader(false);
    }
  }).DataTable({
    "language": {
      "sLengthMenu": $('#show_txt').val() + " _MENU_ " + $('#entries_txt').val(),
      "info": $('#showing_txt').val() + " _START_ " + $('#to_txt').val() + " _END_ " + $('#of_txt').val() + " _TOTAL_ " + $('#entries_txt').val(),
      "emptyTable": $('#msg_no_data_available_table').val(),
      "paginate": {
        "previous": $('#previous_txt').val(),
        "next": $('#next_txt').val()
      }
    },
    "lengthMenu": [10, 20, 30, 50],
    "searching": false,
    "serverSide": true,
    "deferRender": true,
    "ajax": {
      "url": enroll_list_url,
      "type": "POST",
      "dataType": 'json',
      "data": function (d) {
        d.search = $('#search_students').val();

      }
    },
    columns: [
      { "data": "index", className: "text-center" },
      {
        "data": "stu_StudentID", sortable: !1,
        render: function (data, type, row) {
          if (row.stu_StudentID != "") {
            return row.stu_StudentID;
          } else {
            return row.stu_TempCitizenId;
          }
        }
      },
      { "data": "stu_StudentName" },
      { "data": "stu_StudentSurname" },
      {
        "data": "stu_StudentID", sortable: !1,
        render: function (data, type, student) {
          var stuId = '';
          if (student.stu_StudentID != "") {
            stuId = student.stu_StudentID;
          } else {
            stuId = student.stu_TempCitizenId;
          }
          return '<a target="_blank" href="' + base_url + '/employee/students/' + student.id + '"><img src="' + imagepath + 'ic_eye_color.png"></a>\t\t\t\t\t\t<a href="javascript:void(0)" onclick="selEmp(this)" stu-uid="' + stuId + '" stu-name="' + student.stu_StudentName + '" stu-id="' + student.id + '" stu-surname="' + student.stu_StudentSurname + ' "><img src="' + imagepath + 'ic_plus.png"></a>'
        }
      },
    ],
  });

  $('.main_table').hide();
  $("#search_students").on('keyup', function () {
    var checkVal = $(this).val();
    if (checkVal) {
      $('.main_table').show();
    } else {
      $('.main_table').hide();
    }
    $('#enroll_stu_listing').DataTable().ajax.reload();
  });


  $("form[name='enroll-stu-form']").validate({
    errorClass: "error_msg",
    rules: {
      ste_EnrollmentDate: {
        required: true
      },
      fkSteMbo: {
        required: true,
      },
      fkSteGra: {
        required: true,
      },
      fkSteEdp: {
        required: true,
      },
      fkSteEpl: {
        required: true,
      },
      ste_MainBookOrderNumber: {
        required: true,
        number: true,
        minlength: 2,
        maxlength: 5,
      },
      fkSteSye: {
        required: true,
      }
    },
    submitHandler: function (form, event) {
      event.preventDefault();
      showLoader(true);
      var formData = new FormData($(form)[0]);
      $.ajax({
        url: base_url + '/employee/enrollstudents',
        type: 'POST',
        processData: false,
        contentType: false,
        cache: false,
        data: formData,
        success: function (result) {
          if (result.status) {
            toastr.success(result.message);
            redirectPage("employee/students");
          } else {
            toastr.error(result.message);
          }

          showLoader(false);
        },
        error: function (data) {
          toastr.error($('#something_wrong_txt').val());
          showLoader(false);
        }
      });
    }
  });

});

function selEmp(elem) {

  if ($('.sel_emp_table .ocg').length != 0) {
    toastr.error($('#student_sel_valid_txt').val());
    return;
  }
  $('#students_id').val($(elem).attr('stu-id'));
  $('.sel_emp_table tr').after('<tr class="ocg ocg_' + $(elem).attr('stu-id') + '"><td>1</td><td>' + $(elem).attr('stu-uid') + '</td><td>' + $(elem).attr('stu-name') + '</td><td>' + $(elem).attr('stu-surname') + '</td><td><a data-id="' + $(elem).attr('stu-id') + '" onclick="removeSelEmp(this)" href="javascript:void(0)" class="theme_btn red_btn min_btn">' + $('#delete_txt').val() + '</a></td></tr>');

  $("#eid").val($(elem).attr('emp-id'));
  $(".sel_emp_div").show();
}

function removeSelEmp(elem) {
  $('.ocg').remove();
  $("#eid").val('');
  $(".sel_emp_div").hide();
  $('#students_id').val('');
}

$("#fkSteEdp").on('change', function () {
  var edp_value = $(this).val();
  if (edp_value) {
    showLoader(true);
    $.ajax({
      url: fetch_edu_url,
      type: 'POST',
      data: { 'fkSteEdp': edp_value },
      success: function (result) {
        if (result.status == true) {
          $('#fkSteEpl').html('');
          $("#fkSteEpl").append(`<option value="" selected>Select</option>`);
          $(result.educationPlans).each(function (index, element) {
            $('#fkSteEpl').append(`<option value="${element.pkEpl}">${element.epl_EducationPlanName}</option>`);
          });
        } else {
          toastr.error($('#something_wrong_txt').val());
          showLoader(false);
        }
        showLoader(false);
      },
      error: function (data) {
        toastr.error($('#something_wrong_txt').val());
        showLoader(false);
      }
    });
  } else {
    $('#fkSteEpl').html(`<option value="" selected>Select</option>`);
    $('#fkSteGra').html(`<option value="" selected>Select</option>`);
  }
});


$("#fkSteEpl").on('change', function () {
  $('#fkSteGra').find('option').not(':first').remove();
  var epl_value = $(this).val();
  if (epl_value) {
    showLoader(true);
    $.ajax({
      url: base_url + '/employee/fetch-grades',
      type: 'POST',
      data: { 'fkSteEpl': epl_value },
      success: function (result) {
        if (result.status == true) {

          //$('#fkSteGra').html(`<option value="${result.grades.pkGra}">${result.grades.gra_GradeName}</option>`);
          $.each(result.grades, function (key, value) {
            $('#fkSteGra').append($("<option></option>")
              .attr("value", value.pkGra)
              .text(value.gra_GradeNumeric));
          });

        } else {
          toastr.error($('#something_wrong_txt').val());
          showLoader(false);
        }
        showLoader(false);
      },
      error: function (data) {
        toastr.error($('#something_wrong_txt').val());
        showLoader(false);
      }
    });
  } else {
    $('#fkSteGra').html(`<option value="" selected>Select</option>`);
  }
});

function viewEduPlan() {

  var e = document.getElementById("fkSteEpl");
  var selectedPlan = e.options[e.selectedIndex].value;
  if (selectedPlan) {
    window.open($('#web_base_url').val() + '/employee/view/educationplan/' + selectedPlan);
  } else {
    toastr.error($('#msg_select_education_plan').val());
  }
}

$('#ste_MainBookOrderNumber').keypress(function (e) {
  var tval = $('#ste_MainBookOrderNumber').val(),
    tlength = tval.length,
    set = 5,
    remain = parseInt(set - tlength);
  if (remain <= 0 && e.which !== 0 && e.charCode !== 0) {
    $('#ste_MainBookOrderNumber').val((tval).substring(0, tlength - 1))
  }
})