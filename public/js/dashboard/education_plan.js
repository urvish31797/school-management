/**
* Education Plan
*
* This file is used for admin JS
*
* @package    Laravel
* @subpackage JS
* @since      1.0
*/
$(function () {

  $('.pl-5.pr-5 .profile_info_container .select2drp').select2({
    "language": {
      "noResults": function () {
        return $('#no_result_text').val();
      }
    },
  });

  $('.select2_drop').select2({
    "language": {
      "noResults": function () {
        return $('#no_result_text').val();
      }
    },
    placeholder: $('#select_txt').val(),
    allowClear: true
  });

  $('#add_ep_gra').on('click', function () {
    if ($('#gra_sel').val() == '') {
      toastr.error($('#grade_validate_txt').val());
      return;
    }

    if ($('.ep_gra_' + $('#gra_sel').val()).length) {
      toastr.error($('#ep_grade_validate_txt').val());
      return;
    }

    var ed = $('.ep_add_element .profile_info_container').clone();
    var ind = $('.pl-5.pr-5 .profile_info_container').length + 1;
    ed.find('.rm_eng').attr('data-eed', ind);
    ed.find('select,input,textarea,button,strong,a,table').each(function (key, value) {
      var attr = $(this).attr('data-id');
      if (typeof attr !== typeof undefined && attr !== false) {
        $(this).attr('data-id', ind);
      }
      if ($(this).attr('class') == 'gra_name') {
        $(this).html('Grade ' + $("#gra_sel option:selected").text());
      }
      if ($(this).hasClass('dropdown_control')) {
        $(this).addClass('select2drp');
      }

      if ($(this).hasClass('mcg_table')) {
        $(this).addClass('mcg_table_' + $('#gra_sel').val());
      }

      if ($(this).hasClass('ocg_table')) {
        $(this).addClass('ocg_table_' + $('#gra_sel').val());
      }

      if ($(this).hasClass('fcg_table')) {
        $(this).addClass('fcg_table_' + $('#gra_sel').val());
      }

      if ($(this).hasClass('addElemBtn')) {
        $(this).attr('data-elem-id', ind);
        $(this).attr('data-elem-gra', $('#gra_sel').val());
      }

      if ($(this).hasClass('ids_only')) {
        this.id = this.id + '_' + ind;
      } else if ($(this).attr('class') != 'gra_name' && !$(this).hasClass('addElemBtn') && !$(this).hasClass('color_table') && !$(this).hasClass('hours_input') && !$(this).hasClass('title_label') && !$(this).hasClass('cu_order') && !$(this).hasClass('hidden_inputs')) {
        this.id = this.id + '_' + ind;
        this.name = this.name + '_' + ind;
      }

    });

    $('.pl-5.pr-5 .text-center:first').before(ed);
    $('#mcg_select_' + ind).closest(".profile_info_container").addClass('ep_gra_' + $('#gra_sel').val())
    $('#mcg_select_' + ind).closest(".profile_info_container").attr('id', 'ep_gra_' + $('#gra_sel').val());
    $('.select2drp').select2({
      "language": {
        "noResults": function () {
          return $('#no_result_text').val();
        }
      },
    });

  });

  $(document).on('click', '.rm_eng', function () {
    $(this).closest('.profile_info_container').remove();
    var inc = 1;
    $('.pl-5.pr-5 .profile_info_container').each(function (i, obj) {
      $(this).find('select,input,textarea,button,a').each(function (key, value) {
        var old_id = this.id.split("_");
        old_id.splice(-1, 1);
        old_id.push(inc);
        var new_id = old_id.join('_');
        var aid = new_id.replace("_", "");

        if ($(this).hasClass('addElemBtn')) {
          $(this).attr('data-elem-id', aid);
        }

        if ($(this).hasClass('ids_only')) {
          this.id = new_id;
        } else if (!$(this).hasClass('addElemBtn') && !$(this).hasClass('hours_input') && !$(this).hasClass('title_label') && !$(this).hasClass('cu_order') && !$(this).hasClass('hidden_inputs')) {
          this.id = new_id;
          this.name = new_id;
        }

      });
      inc++;
    });
  });


  $('#education_plan_listing').on('processing.dt', function (e, settings, processing) {
    if (processing) {
      showLoader(true);
    } else {
      showLoader(false);
    }
  }).DataTable({
    "language": {
      "sLengthMenu": $('#show_txt').val() + " _MENU_ " + $('#entries_txt').val(),
      "info": $('#showing_txt').val() + " _START_ " + $('#to_txt').val() + " _END_ " + $('#of_txt').val() + " _TOTAL_ " + $('#entries_txt').val(),
      "emptyTable": $('#msg_no_data_available_table').val(),
      "paginate": {
        "previous": $('#previous_txt').val(),
        "next": $('#next_txt').val()
      }
    },
    "lengthMenu": [10, 20, 30, 50],
    "searching": false,
    "serverSide": true,
    "deferRender": true,
    "ajax": {
      "url": listing_url,
      "type": "POST",
      "dataType": 'json',
      "data": function (d) {
        d.search = $('#search_education_plan').val();
      }
    },
    columns: [
      { "data": "index", className: "text-center" },
      { "data": "epl_Uid" },
      { "data": "epl_EducationPlanName" },
      {
        "data": "epl_EducationPlanName", className: "text-center", sortable: !1,
        render: function (data, type, grades) {
          return grades.grades;
        }
      },
      {
        "data": "epl_EducationPlanName", sortable: !1,
        render: function (data, type, country) {
          return '<a class="ajax_request no_sidebar_active" href="educationplan/' + country.pkEpl + '"><img src="' + imagepath + 'ic_eye_color.png"></a>\t\t\t\t\t\t<a class="ajax_request no_sidebar_active" href="educationplan/' + country.pkEpl + '/edit"><img src="' + imagepath + 'ic_mode_edit.png"></a>\t\t\t\t\t\t<a onclick="triggerDelete(' + country.pkEpl + ')" href="javascript:void(0)"><img src="' + imagepath + 'ic_delete.png"></a>'
        }
      },
    ],

  });

  $('#fkEplEdp').on('change', function () {
    if ($(this).val() == '') {
      $('.edp_label').text('');
    } else {
      $('.edp_label').text($("#fkEplEdp option:selected").text().trim());
    }
  })

  $("#search_education_plan").on('keyup', function () {
    //if($(this).val() != ''){
    $('#education_plan_listing').DataTable().ajax.reload()
    // }
  });

  $('#add_new').on('hidden.bs.modal', function () {
    var validator = $("form").validate();
    validator.resetForm();
    $("form").trigger("reset");
    $("#pkEpl").val('');
  })

  $('#delete_prompt').on('hidden.bs.modal', function () {
    $("#did").val('');
  })

  $("form[name='add-education-plan-form']").validate({
    errorClass: "error_msg",
    errorPlacement: function (error, element) {
      if (element.hasClass('select2_drop') && element.next('.select2-container').length) {
        error.insertAfter(element.next('.select2-container'));
      } else {
        error.insertAfter(element);
      }
    },
    rules: {
      epl_EducationPlanName: {
        required: true
      },
      fkEplEdp: {
        required: true,
      },
      fkEplCan: {
        required: true,
      },
      fkEplNep: {
        required: true,
      },
      fkEplGra: {
        required: true,
      },
      fkCbp: {
        required: true
      }
    },
    submitHandler: function (form, event) {
      event.preventDefault();
      var ep_submit = true;

      if ($('.pl-5.pr-5 .profile_info_container').length == 0) {
        toastr.error($('#ep_course_validate_txt').val());
        return;
      }

      if (MCG.length == 0) {
        toastr.error($('#MCG_validate_txt').val());
        return;
      }

      $('.pl-5.pr-5 .profile_info_container').each(function (i, obj) {

        if ($(this).find('.mcg_item').length == 0) {
          toastr.error($('#all_mandatory_validate_txt').val());
          ep_submit = false;
        }
      });

      if (!$("form[name='add-education-plan-form']")[0].checkValidity()) {
        $("form[name='add-education-plan-form']")[0].reportValidity();
        return;
      }

      if (!ep_submit) {
        return;
      }

      showLoader(true);
      var formData = new FormData($(form)[0]);
      var url = base_url + '/admin/educationplan';
      if ($('#eid').length) {
        formData.append("pkEpl", $('#eid').val());
      }
      $.ajax({
        url: url,
        type: 'POST',
        processData: false,
        contentType: false,
        cache: false,
        data: formData,
        success: function (result) {
          if (result.status) {
            toastr.success(result.message);
            redirectPage("admin/educationplan");
          } else {
            toastr.error(result.message);
          }

          showLoader(false);
        },
        error: function (data) {
          toastr.error($('#something_wrong_txt').val());
          showLoader(false);
        }
      });
    }
  });

});

function confirmDelete() {
  showLoader(true);
  var cid = $('#did').val();
  $.ajax({
    url: base_url + '/admin/educationplan/' + cid,
    type: 'DELETE',
    dataType: 'json',
    cache: false,
    success: function (result) {
      $('#delete_prompt').modal('hide');
      if (result.status) {
        toastr.success(result.message);
        $('#education_plan_listing').DataTable().ajax.reload();
      } else {
        toastr.error(result.message);
      }

      showLoader(false);
    },
    error: function (data) {
      toastr.error($('#something_wrong_txt').val());
      showLoader(false);
    }
  });
}


function triggerEdit(cid) {
  showLoader(true);
  $.ajax({
    url: base_url + '/admin/getEducationPlan',
    type: 'POST',
    dataType: 'json',
    cache: false,
    data: { 'cid': cid },
    success: function (result) {
      if (result.status) {
        $.each(result.data, function (index, value) {
          $("#" + index).val(value);
        });
        $(".show_modal").click();
      } else {
        toastr.error(result.message);
      }

      showLoader(false);
    },
    error: function (data) {
      toastr.error($('#something_wrong_txt').val());
      showLoader(false);
    }
  });
}

function triggerDelete(cid) {
  $('#did').val(cid);
  $(".show_delete_modal").click();
}

function addFCG(elem) {

  if ($('#fcg_select_' + $(elem).attr('data-elem-id')).val() == '') {
    toastr.error($('#FCG_validate_txt').val());
    return;
  }

  if ($('.fcg_table_' + $(elem).attr('data-elem-gra') + ' #efc_order').val() == '') {
    toastr.error($('#course_order_validate_txt').val());
    return;
  }

  if ($('.fcg_table_' + $(elem).attr('data-elem-gra') + ' #fcg_hrs').val() == '') {
    toastr.error($('#hours_validate_txt').val());
    return;
  }

  if ($('.fcg_ord_' + $('.fcg_table_' + $(elem).attr('data-elem-gra') + ' #efc_order').val() + '.fcg_gra_' + $(elem).attr('data-elem-gra')).length) {
    toastr.error($('#order_grade_validate_txt').val());
    return;
  }

  if ($('.fcg_' + $('#fcg_select_' + $(elem).attr('data-elem-id')).val() + '.fcg_gra_' + $(elem).attr('data-elem-gra')).length) {
    toastr.error($('#course_grade_validate_txt').val());
    return;
  }

  var foriegn_course_name = $('#fcg_select_' + $(elem).attr('data-elem-id') + " option:selected").text();
  var name_data = foriegn_course_name.split("-");
  var course_order = $('.fcg_table_' + $(elem).attr('data-elem-gra') + ' #efc_order').val();
  var numeric_name = name_data[1].trim();

  if (numeric_name != course_order) {
    toastr.error($('#ep_fog_ordername_validate_txt').val());
    return;
  }

  var currfcg = $('#fcg_select_' + $(elem).attr('data-elem-id')).val();

  $('.fcg_table_' + $(elem).attr('data-elem-gra') + ' tr:last').before('<tr class="fcg_item fcg fcg_' + $('#fcg_select_' + $(elem).attr('data-elem-id')).val() + ' fcg_gra_' + $(elem).attr('data-elem-gra') + ' fcg_ord_' + $('.fcg_table_' + $(elem).attr('data-elem-gra') + ' #efc_order').val() + '"><td>' + $('.fcg_table_' + $(elem).attr('data-elem-gra') + ' tr.fcg').length + '</td><td>' + $('#fcg_select_' + $(elem).attr('data-elem-id') + ' option:selected').text() + '</td><td><input class="hidden_inputs" type="hidden" name="FCG[]" value="' + $('#fcg_select_' + $(elem).attr('data-elem-id')).val() + '"><input type="hidden" class="hidden_inputs" name="FCG_order[]" value="' + $('.fcg_table_' + $(elem).attr('data-elem-gra') + ' #efc_order').val() + '">' + $('.fcg_table_' + $(elem).attr('data-elem-gra') + ' #efc_order option:selected').text() + '</td><td><input class="ids_only form-control form-control-border" maxlength="3" onkeyup="cleanHrs(this)" required name="fcg_hrs[]" maxlength="3" type="text" id="fcg_hrs_' + $('#fcg_select_' + $(elem).attr('data-elem-id')).val() + '_' + $(elem).attr('data-elem-gra') + '" value="' + $('.fcg_table_' + $(elem).attr('data-elem-gra') + ' #fcg_hrs').val() + '"></td><td><input class="hidden_inputs" type="hidden" name="fcg_gra[]" value="' + $(elem).attr('data-elem-gra') + '"><a sel-id="' + $(elem).attr('data-elem-id') + '" data-id="' + $('#fcg_select_' + $(elem).attr('data-elem-id')).val() + '" data-gra="' + $(elem).attr('data-elem-gra') + '" onclick="removeFCG(this)" href="javascript:void(0)" class="theme_btn red_btn min_btn">' + $('#delete_txt').val() + '</a></td><td><div class="form-group form-check"><input type="checkbox" class="form-check-input" name="default_fcg[]" id="default_fcg_' + $('#fcg_select_' + $(elem).attr('data-elem-id')).val() + '" value="' + $('#fcg_select_' + $(elem).attr('data-elem-id')).val() + '_' + $(elem).attr('data-elem-gra') + '"><label class="custom_checkbox"></label></div></td></tr>');
  $('.fcg_table_' + $(elem).attr('data-elem-gra') + ' .fcg_main td:first').text($('.fcg_table_' + $(elem).attr('data-elem-gra') + ' tr.fcg').length);
  // $("#fcg_select option[value='"+ $('#fcg_select').val() + "']").attr('disabled', true);
  if (currfcg != '') {
    FCG.push(currfcg);
  }
  $('#fcg_select_' + $(elem).attr('data-elem-id')).val('').trigger('change')
  $('.fcg_table_' + $(elem).attr('data-elem-gra') + ' #fcg_hrs').val('');
  if ($('#fcg_select_' + $(elem).attr('data-elem-id') + ' option:not(:disabled)').length == 1) {
    $(".fcg_main").hide();
  }
}


function removeFCG(elem) {
  var i = 1;
  var id = $(elem).attr('data-id');
  var gra = $(elem).attr('data-gra');
  var sel_id = $(elem).attr('sel-id');
  $("#fcg_select_" + sel_id + " option[value='" + id + "']").attr('disabled', false);
  $('.fcg_' + id + '.fcg_gra_' + gra).remove();
  $('.fcg_table_' + gra + ' .fcg_main td:first').text($('.fcg_table_' + gra + ' tr.fcg').length);
  if ($('#fcg_select_' + sel_id + ' option:not(:disabled)').length != 1) {
    $('.fcg_table_' + gra + " .fcg_main").show();
  }
  FCG = jQuery.grep(FCG, function (value) {
    return value != id;
  });
  $.each($('.fcg_table_' + gra + ' .fcg:not(:last-child)'), function (index, value) {
    $(value).children('td:first').text(i++);
  });
}

function addOCG(elem) {

  if ($('#ocg_select_' + $(elem).attr('data-elem-id')).val() == '') {
    toastr.error($('#OCG_validate_txt').val());
    return;
  }

  if ($('.ocg_table_' + $(elem).attr('data-elem-gra') + ' #eoc_order').val() == '') {
    toastr.error($('#course_order_validate_txt').val());
    return;
  }

  if ($('.ocg_table_' + $(elem).attr('data-elem-gra') + ' #ocg_hrs').val() == '') {
    toastr.error($('#hours_validate_txt').val());
    return;
  }

  if ($('.ocg_ord_' + $('.ocg_table_' + $(elem).attr('data-elem-gra') + ' #eoc_order').val() + '.ocg_gra_' + $(elem).attr('data-elem-gra')).length) {
    toastr.error($('#order_grade_validate_txt').val());
    return;
  }

  if ($('.ocg_' + $('#ocg_select_' + $(elem).attr('data-elem-id')).val() + '.ocg_gra_' + $(elem).attr('data-elem-gra')).length) {
    toastr.error($('#course_grade_validate_txt').val());
    return;
  }

  var optional_course_name = $('#ocg_select_' + $(elem).attr('data-elem-id') + " option:selected").text();
  var name_data = optional_course_name.split("-");
  var course_order = $('.ocg_table_' + $(elem).attr('data-elem-gra') + ' #eoc_order').val();
  var numeric_name = name_data[1].trim();

  if (numeric_name != course_order) {
    toastr.error($('#ep_ocg_ordername_validate_txt').val());
    return;
  }

  var currOcg = $('#ocg_select_' + $(elem).attr('data-elem-id')).val();

  $('.ocg_table_' + $(elem).attr('data-elem-gra') + ' tr:last').before('<tr class="ocg_item ocg ocg_' + $('#ocg_select_' + $(elem).attr('data-elem-id')).val() + ' ocg_gra_' + $(elem).attr('data-elem-gra') + ' ocg_ord_' + $('.ocg_table_' + $(elem).attr('data-elem-gra') + ' #eoc_order').val() + '"><td>' + $('.ocg_table_' + $(elem).attr('data-elem-gra') + ' tr.ocg').length + '</td><td>' + $('#ocg_select_' + $(elem).attr('data-elem-id') + ' option:selected').text() + '</td><td><input class="hidden_inputs" type="hidden" name="OCG[]" value="' + $('#ocg_select_' + $(elem).attr('data-elem-id')).val() + '"><input class="hidden_inputs" type="hidden" name="OCG_order[]" value="' + $('.ocg_table_' + $(elem).attr('data-elem-gra') + ' #eoc_order').val() + '">' + $('.ocg_table_' + $(elem).attr('data-elem-gra') + ' #eoc_order option:selected').text() + '</td><td><input maxlength="3" class="ids_only form-control form-control-border" onkeyup="cleanHrs(this)" required name="ocg_hrs[]" maxlength="3" type="text" id="ocg_hrs_' + $('#ocg_select_' + $(elem).attr('data-elem-id')).val() + '_' + $(elem).attr('data-elem-gra') + '" value="' + $('.ocg_table_' + $(elem).attr('data-elem-gra') + ' #ocg_hrs').val() + '"></td><td><input class="hidden_inputs" type="hidden" name="ocg_gra[]" value="' + $(elem).attr('data-elem-gra') + '"><a sel-id="' + $(elem).attr('data-elem-id') + '" data-id="' + $('#ocg_select_' + $(elem).attr('data-elem-id')).val() + '" data-gra="' + $(elem).attr('data-elem-gra') + '" onclick="removeOCG(this)" href="javascript:void(0)" class="theme_btn red_btn min_btn">' + $('#delete_txt').val() + '</a></td><td><div class="form-group form-check"><input type="checkbox" class="form-check-input" name="default_ocg[]" id="default_ocg_' + $('#ocg_select_' + $(elem).attr('data-elem-id')).val() + '" value="' + $('#ocg_select_' + $(elem).attr('data-elem-id')).val() + '_' + $(elem).attr('data-elem-gra') + '"><label class="custom_checkbox"></label></div></td></tr>');
  $('.ocg_table_' + $(elem).attr('data-elem-gra') + ' .ocg_main td:first').text($('.ocg_table_' + $(elem).attr('data-elem-gra') + ' tr.ocg').length);
  // $("#ocg_select option[value='"+ $('#ocg_select').val() + "']").attr('disabled', true);
  if (currOcg != '') {
    OCG.push(currOcg);
  }
  $('#ocg_select_' + $(elem).attr('data-elem-id')).val('').trigger('change');
  $('.ocg_table_' + $(elem).attr('data-elem-gra') + ' #ocg_hrs').val('');
  if ($('#ocg_select_' + $(elem).attr('data-elem-id') + ' option:not(:disabled)').length == 1) {
    $(".ocg_main").hide();
  }

}


function removeOCG(elem) {
  var i = 1;
  var id = $(elem).attr('data-id');
  var gra = $(elem).attr('data-gra');
  var sel_id = $(elem).attr('sel-id');
  $("#ocg_select_" + sel_id + " option[value='" + id + "']").attr('disabled', false);
  $('.ocg_' + id + '.ocg_gra_' + gra).remove();
  $('.ocg_table_' + gra + ' .ocg_main td:first').text($('.ocg_table_' + gra + ' tr.ocg').length);
  if ($('#ocg_select_' + sel_id + ' option:not(:disabled)').length != 1) {
    $('.ocg_table_' + gra + " .ocg_main").show();
  }
  OCG = jQuery.grep(OCG, function (value) {
    return value != id;
  });
  $.each($('.ocg_table_' + gra + ' .ocg:not(:last-child)'), function (index, value) {
    $(value).children('td:first').text(i++);
  });
}

function selectCourse(elem) {

  // var id = $(elem).closest('.profile_info_container').attr('id');
  // console.log($(elem).closest('.profile_info_container').find("#mcg_hrs").val());
  if ($(elem).find(':selected').data('alternativename') == 'Stručna praksa') {
    // console.log(1);
    $(elem).closest('.profile_info_container').find("#mcg_hrs").attr("readonly", true);
    $(elem).closest('.profile_info_container').find("#mcg_hrs").addClass('input_disable');
    $(elem).closest('.profile_info_container').find("#mcg_hrs").val('');

    $(elem).closest('.profile_info_container').find("#mcg_weekHourRateTeacher").attr("readonly", true);
    $(elem).closest('.profile_info_container').find("#mcg_weekHourRateTeacher").addClass('input_disable');
    $(elem).closest('.profile_info_container').find("#mcg_weekHourRateTeacher").val('');

    $(elem).closest('.profile_info_container').find("#mcg_summerHolidayHourRate").attr("readonly", false);
    $(elem).closest('.profile_info_container').find("#mcg_summerHolidayHourRate").removeClass('input_disable');
  }
  else {
    // console.log(2);
    $(elem).closest('.profile_info_container').find("#mcg_hrs").attr("readonly", false);
    $(elem).closest('.profile_info_container').find("#mcg_hrs").removeClass('input_disable');

    $(elem).closest('.profile_info_container').find("#mcg_weekHourRateTeacher").attr("readonly", false);
    $(elem).closest('.profile_info_container').find("#mcg_weekHourRateTeacher").removeClass('input_disable');

    $(elem).closest('.profile_info_container').find("#mcg_summerHolidayHourRate").attr("readonly", true);
    $(elem).closest('.profile_info_container').find("#mcg_summerHolidayHourRate").addClass('input_disable');
    $(elem).closest('.profile_info_container').find("#mcg_summerHolidayHourRate").val('');
  }

}

function addMCG(elem) {

  //validate for course field
  if ($('#mcg_select_' + $(elem).attr('data-elem-id')).val() == '') {
    toastr.error($('#MCG_validate_txt').val());
    return;
  }
  //validation for hours field
  if ($('.mcg_table_' + $(elem).attr('data-elem-gra') + ' #mcg_hrs').val() == '') {
    if ($('#mcg_select_' + $(elem).attr('data-elem-id')).find(':selected').data('alternativename') != 'Stručna praksa') {
      toastr.error($('#hours_validate_txt').val());
      return;
    }
  }
  //validation for week hours teacher rate field

  // if($('.mcg_table_'+$(elem).attr('data-elem-gra')+' #mcg_weekHourRateTeacher').val() == ''){
  //   if($('#mcg_select_'+$(elem).attr('data-elem-id')).find(':selected').data('alternativename') != 'Stručna praksa')
  //   {
  //     toastr.error($('#hours_rate_validate_txt').val());
  //     return;
  //   }
  // }

  //validation for summer holiday hour
  if ($('.mcg_table_' + $(elem).attr('data-elem-gra') + ' #mcg_summerHolidayHourRate').val() == '') {
    if ($('#mcg_select_' + $(elem).attr('data-elem-id')).find(':selected').data('alternativename') == 'Stručna praksa') {
      toastr.error($('#summer_holiday_validate_txt').val());
      return;
    }
  }

  if ($('.mcg_' + $('#mcg_select_' + $(elem).attr('data-elem-id')).val() + '.mcg_gra_' + $(elem).attr('data-elem-gra')).length) {
    toastr.error($('#course_grade_validate_txt').val());
    return;
  }

  var currMcg = $('#mcg_select_' + $(elem).attr('data-elem-id')).val();

  if ($('#mcg_select_' + $(elem).attr('data-elem-id')).find(':selected').data('alternativename') == 'Stručna praksa') {
    $('.mcg_table_' + $(elem).attr('data-elem-gra') + ' tr:last').before('<tr class="mcg_item mcg mcg_' + $('#mcg_select_' + $(elem).attr('data-elem-id')).val() + ' mcg_gra_' + $(elem).attr('data-elem-gra') + '"><td>' + $('.mcg_table_' + $(elem).attr('data-elem-gra') + ' tr.mcg').length + '</td><td>' + $('#mcg_select_' + $(elem).attr('data-elem-id') + ' option:selected').text() + '</td><td><input class="ids_only form-control form-control-border input_disable" onkeyup="cleanHrs(this)" name="mcg_hrs[]" type="text" id="mcg_hrs_' + $('#mcg_select_' + $(elem).attr('data-elem-id')).val() + '_' + $(elem).attr('data-elem-gra') + '" value="' + $('.mcg_table_' + $(elem).attr('data-elem-gra') + ' #mcg_hrs').val() + '" maxlength="3" readonly="readonly"></td><td><input class="ids_only form-control form-control-border input_disable" onkeyup="cleanHrs(this)" name="mcg_weekHourRateTeacher[]" type="text" id="mcg_weekHourRateTeacher_' + $('#mcg_select_' + $(elem).attr('data-elem-id')).val() + '_' + $(elem).attr('data-elem-gra') + '" value="' + $('.mcg_table_' + $(elem).attr('data-elem-gra') + ' #mcg_weekHourRateTeacher').val() + '" maxlength="3" readonly="readonly"></td><td><input class="ids_only form-control form-control-border" onkeyup="cleanHrs(this)" name="mcg_summerHolidayHourRate[]" type="text" id="mcg_summerHolidayHourRate_' + $('#mcg_select_' + $(elem).attr('data-elem-id')).val() + '_' + $(elem).attr('data-elem-gra') + '" value="' + $('.mcg_table_' + $(elem).attr('data-elem-gra') + ' #mcg_summerHolidayHourRate').val() + '" maxlength="3"></td><td><a sel-id="' + $(elem).attr('data-elem-id') + '" data-id="' + $('#mcg_select_' + $(elem).attr('data-elem-id')).val() + '" data-gra="' + $(elem).attr('data-elem-gra') + '" onclick="removeMCG(this)" href="javascript:void(0)" class="theme_btn red_btn min_btn">' + $('#delete_txt').val() + '</a></td><td><input type="hidden" class="hidden_inputs" name="mcg_gra[]" value="' + $(elem).attr('data-elem-gra') + '"><input type="hidden" class="hidden_inputs" name="MCG[]" value="' + $('#mcg_select_' + $(elem).attr('data-elem-id')).val() + '"><div class="form-group form-check"><input type="checkbox" class="form-check-input" name="default_mcg[]" id="default_mcg_' + $('#mcg_select_' + $(elem).attr('data-elem-id')).val() + '" value="' + $('#mcg_select_' + $(elem).attr('data-elem-id')).val() + '_' + $(elem).attr('data-elem-gra') + '"><label class="custom_checkbox"></label></div></td></tr>');
  }
  else {
    $('.mcg_table_' + $(elem).attr('data-elem-gra') + ' tr:last').before('<tr class="mcg_item mcg mcg_' + $('#mcg_select_' + $(elem).attr('data-elem-id')).val() + ' mcg_gra_' + $(elem).attr('data-elem-gra') + '"><td>' + $('.mcg_table_' + $(elem).attr('data-elem-gra') + ' tr.mcg').length + '</td><td>' + $('#mcg_select_' + $(elem).attr('data-elem-id') + ' option:selected').text() + '</td><td><input class="ids_only form-control form-control-border" onkeyup="cleanHrs(this)" name="mcg_hrs[]" type="text" id="mcg_hrs_' + $('#mcg_select_' + $(elem).attr('data-elem-id')).val() + '_' + $(elem).attr('data-elem-gra') + '" value="' + $('.mcg_table_' + $(elem).attr('data-elem-gra') + ' #mcg_hrs').val() + '" maxlength="3"></td><td><input class="ids_only form-control form-control-border" onkeyup="cleanHrs(this)" name="mcg_weekHourRateTeacher[]" type="text" id="mcg_weekHourRateTeacher_' + $('#mcg_select_' + $(elem).attr('data-elem-id')).val() + '_' + $(elem).attr('data-elem-gra') + '" value="' + $('.mcg_table_' + $(elem).attr('data-elem-gra') + ' #mcg_weekHourRateTeacher').val() + '" maxlength="3"></td><td><input class="ids_only form-control form-control-border input_disable" onkeyup="cleanHrs(this)" name="mcg_summerHolidayHourRate[]" type="text" id="mcg_summerHolidayHourRate_' + $('#mcg_select_' + $(elem).attr('data-elem-id')).val() + '_' + $(elem).attr('data-elem-gra') + '" value="' + $('.mcg_table_' + $(elem).attr('data-elem-gra') + ' #mcg_summerHolidayHourRate').val() + '" maxlength="3" readonly="readonly"></td><td><a sel-id="' + $(elem).attr('data-elem-id') + '" data-id="' + $('#mcg_select_' + $(elem).attr('data-elem-id')).val() + '" data-gra="' + $(elem).attr('data-elem-gra') + '" onclick="removeMCG(this)" href="javascript:void(0)" class="theme_btn red_btn min_btn">' + $('#delete_txt').val() + '</a></td><td><input type="hidden" class="hidden_inputs" name="mcg_gra[]" value="' + $(elem).attr('data-elem-gra') + '"><input type="hidden" class="hidden_inputs" name="MCG[]" value="' + $('#mcg_select_' + $(elem).attr('data-elem-id')).val() + '"><div class="form-group form-check"><input type="checkbox" class="form-check-input" name="default_mcg[]" id="default_mcg_' + $('#mcg_select_' + $(elem).attr('data-elem-id')).val() + '" value="' + $('#mcg_select_' + $(elem).attr('data-elem-id')).val() + '_' + $(elem).attr('data-elem-gra') + '"><label class="custom_checkbox"></label></div></td></tr>');
  }

  $('.mcg_table_' + $(elem).attr('data-elem-gra') + ' .mcg_main td:first').text($('.mcg_table_' + $(elem).attr('data-elem-gra') + ' tr.mcg').length);
  if (currMcg != '') {
    MCG.push(currMcg);
  }

  $('#mcg_select_' + $(elem).attr('data-elem-id')).val('').trigger('change');
  $('.mcg_table_' + $(elem).attr('data-elem-gra') + ' #mcg_hrs').val('');
  $('.mcg_table_' + $(elem).attr('data-elem-gra') + ' #mcg_weekHourRateTeacher').val('');
  $('.mcg_table_' + $(elem).attr('data-elem-gra') + ' #mcg_summerHolidayHourRate').val('');
  // if($('#mcg_select_'+$(elem).attr('data-elem-id')+' option:not(:disabled)').length == 1){
  // $(".  ").hide();
  // }
}


function removeMCG(elem) {
  var i = 1;
  var id = $(elem).attr('data-id');
  var gra = $(elem).attr('data-gra');
  var sel_id = $(elem).attr('sel-id');
  $("#mcg_select_" + sel_id + " option[value='" + id + "']").attr('disabled', false);
  $('.mcg_' + id + '.mcg_gra_' + gra).remove();
  $('.mcg_table_' + gra + ' .mcg_main td:first').text($('.mcg_table_' + gra + ' tr.mcg').length);
  if ($('#mcg_select_' + sel_id + ' option:not(:disabled)').length != 1) {
    $('.mcg_table_' + gra + " .mcg_main").show();
  }
  MCG = jQuery.grep(MCG, function (value) {
    return value != id;
  });
  $.each($('.mcg_table_' + gra + ' .mcg:not(:last-child)'), function (index, value) {
    $(value).children('td:first').text(i++);
  });
}

function cleanHrs(val) {
  var vv = val.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');
  $(val).val(vv);
}
