/**
* School
*
* This file is used for admin JS
*
* @package    Laravel
* @subpackage JS
* @since      1.0
*/

var empType = 0;
$(function () {
  $('#school_listing').on('processing.dt', function (e, settings, processing) {
    if (processing) {
      showLoader(true);
    } else {
      showLoader(false);
    }
  }).DataTable({
    "language": {
      "sLengthMenu": $('#show_txt').val() + " _MENU_ " + $('#entries_txt').val(),
      "info": $('#showing_txt').val() + " _START_ " + $('#to_txt').val() + " _END_ " + $('#of_txt').val() + " _TOTAL_ " + $('#entries_txt').val(),
      "emptyTable": $('#msg_no_data_available_table').val(),
      "paginate": {
        "previous": $('#previous_txt').val(),
        "next": $('#next_txt').val()
      }
    },
    "lengthMenu": [10, 20, 30, 50],
    "searching": false,
    "serverSide": true,
    "deferRender": true,
    "ajax": {
      "url": listing_url,
      "type": "POST",
      "dataType": 'json',
      "data": function (d) {
        d.search = $('#search_school').val();
      }
    },
    columns: [
      { "data": "index", className: "text-center" },
      { "data": "sch_Uid" },
      { "data": "sch_SchoolName" },
      { "data": "sch_MinistryApprovalCertificate" },
      {
        "data": "sch_SchoolName", className: "text-center", sortable: !1,
        render: function (data, type, school) {
          return school.employees_engagement[0].employee.emp_EmployeeName + " " + school.employees_engagement[0].employee.emp_EmployeeSurname;
        }
      },
      {
        "data": "sch_SchoolName", className: "text-center", sortable: !1,
        render: function (data, type, school) {
          return school.employees_engagement[0].employee.email;
        }
      },
      {
        "data": "sch_SchoolName", sortable: !1,
        render: function (data, type, country) {
          return '<a class="no_sidebar_active" href="schools/' + country.pkSch + '"><img src="' + imagepath + 'ic_eye_color.png"></a>\t\t\t\t\t\t<a class="ajax_request no_sidebar_active" href="schools/' + country.pkSch + '/edit"><img src="' + imagepath + 'ic_mode_edit.png"></a>\t\t\t\t\t\t<a onclick="triggerDelete(' + country.pkSch + ')" href="javascript:void(0)"><img src="' + imagepath + 'ic_delete.png"></a>'
        }
      },
    ],
  });

  $("#search_school").on('keyup', function () {
    //if($(this).val() != ''){
    $('#school_listing').DataTable().ajax.reload()
    // }
  });

  $("#fkEmpMun").on('change', function () {
    var id = $(this).children('option:selected').attr('data-countryId');
    $("#fkEmpCny").val(id);
  });

  $("[name='sel_exists_employee']").on('change', function () {
    if ($(this).val() == 'Yes') {
      empType = 1;
      $('.exists_employee').show();
      $('.add_new_sc').hide();
    } else {
      empType = 0;
      $("#principal_sel").val('');
      $('.add_new_sc').show();
      $('.exists_employee').hide();
    }
  });

  $('.select2drp').select2({
    "language": {
      "noResults": function () {
        return $('#no_result_text').val();
      }
    },
  });

  $('.start_date').datepicker({ format: "dd/mm/yyyy", autoclose: true });
  $('.end_date').datepicker({ format: "dd/mm/yyyy", autoclose: true });
  $('.datepicker').datepicker({ format: "dd/mm/yyyy", autoclose: true, endDate: '+0d', });

  $("#edit_profile").click(function () {
    $("#view_sc").hide();
    $("#edit_sc").show();
  });

  $('#delete_prompt').on('hidden.bs.modal', function () {
    $("#did").val('');
  })

  $("form[name='add-school-form']").validate({
    errorClass: "error_msg",
    errorPlacement: function (error, element) {
      if (element.hasClass('select2drp') && element.next('.select2-container').length) {
        error.insertAfter(element.next('.select2-container'));
      } else {
        error.insertAfter(element);
      }
    },
    rules: {
      sch_SchoolName: {
        required: true,
        minlength: 3
      },
      sch_SchoolId: {
        required: true
      },
      sch_SchoolEmail: {
        required: true
      },
      sch_PhoneNumber: {
        required: true
      },
      fkSchOty: {
        required: true
      },
      sch_Founder: {
        required: true
      },
      sch_Address: {
        required: true
      },
      sch_FoundingDate: {
        required: true
      },
      fkSchEdp: {
        required: true
      },
      sch_MinistryApprovalCertificate: {
        required: true,
        minlength: 5,
        maxlength: 25
      },
      emp_EmployeeName: {
        required: true,
        minlength: 2,
        maxlength: 25
      },
      emp_EmployeeSurname: {
        required: true,
        minlength: 2,
        maxlength: 25
      },
      emp_DateOfBirth: {
        required: true
      },
      sch_CoordEmail: {
        required: true,
        minlength: 5,
        maxlength: 25,
        email: true
      },
      emp_PhoneNumber: {
        required: true,
        maxlength: 10,
        minlength: 10
      },
      fkEmpCny: {
        required: true
      },
      fkEmpMun: {
        required: true
      },
      fkEmpNat: {
        required: true
      },
      fkEmpCtz: {
        required: true
      },
      fkEmpPof: {
        required: true
      },
      fkEedEde: {
        required: true
      },
      fkEmpRel: {
        required: true
      },
      start_date: {
        required: true
      },
      fkSchPof: {
        required: true
      },
      emp_EmployeeGender: {
        required: true
      },
      emp_EmployeeID: {
        required: true
      },
      ewh_WeeklyHoursRate: {
        required: true
      },
      sc_id: {
        required: function () {
          if (empType == 1)
            return true;
        }
      }

    },
    submitHandler: function (form, event) {
      event.preventDefault();
      var status = false;
      if ($("#havent_identification_number").prop("checked") == true) {
        status = true;
      }
      else {
        if ($("input[name='sel_exists_employee']:checked").val() == "No") {
          status = checkEmployeeID();
        }
        else {
          status = true;
        }
      }

      if (SP.length == 0) {
        toastr.error($('#education_plan_validate_txt').val());
        return;
      }

      if (status) {
        showLoader(true);
        var formData = new FormData($(form)[0]);
        formData.append("SP", SP);
        var url = base_url + '/admin/schools';
        if ($('#eid').val()) {
          formData.append("pkSch", $('#eid').val());
        }
        $.ajax({
          url: url,
          type: 'POST',
          processData: false,
          contentType: false,
          cache: false,
          data: formData,
          success: function (result) {
            if (result.status) {
              toastr.success(result.message);
              redirectPage("admin/schools");
            } else {
              toastr.error(result.message);
            }

            showLoader(false);
          },
          error: function (data) {
            toastr.error($('#something_wrong_txt').val());
            showLoader(false);
          }
        });

      }
    }
  });
});

function checkEmployeeID() {
  var birthDate = $("#emp_DateOfBirth").val();
  var gender = $("#emp_EmployeeGender").val();
  var employeeId = $('.isValidEmpID').val();
  var status = false;

  if (gender != "" && birthDate != "" && employeeId != "" && employeeId.length == 13) {
    var dd = employeeId.substring(0, 2);
    var mm = employeeId.substring(2, 4);
    var yy = employeeId.substring(4, 7);
    var rr = employeeId.substring(7, 9);
    var bb = employeeId.substring(9, 12);

    var bdata = birthDate.split("/");
    var d = bdata[0];
    var m = bdata[1];
    var y = bdata[2].substring(1, 4);

    if (dd == d && mm == m && yy == y && rr <= 19 && rr >= 10) {
      if (gender == "Male" && bb <= 499 && bb >= 0) {
        status = true;
      }
      else {
        if (gender == "Female" && bb <= 999 && bb >= 500) {
          status = true;
        }
      }
    }
  }

  if (status == false && employeeId != "" && gender != "" && birthDate != "") {
    $('.isValidEmpID').val("");
    toastr.error($("#employeemsgerror").val());
  }

  return status;
}

function fetchEPlan(pid) {
  $('#eplan').find('option').not(':first').remove();
  if (pid != '') {
    showLoader(true);
    $.ajax({
      url: fetch_edu_url,
      type: 'POST',
      dataType: 'json',
      cache: false,
      data: { 'pid': pid },
      success: function (result) {
        if (result.status) {
          $.each(result.data, function (key, value) {
            var QAD = "";
            if (value.qualification_degree != null) {
              QAD = value.qualification_degree.qde_QualificationDegreeName;
            }

            var EPR = "";
            if (value.education_profile != null) {
              EPR = value.education_profile.epr_EducationProfileName;
            }

            $('#eplan').append($("<option></option>")
              .attr("value", value.pkEpl)
              .attr("EPmain", result.EPmain)
              .attr("EPparent", result.EPparent)
              .attr("NEP", value.national_education_plan.nep_NationalEducationPlanName)
              .attr("QAD", QAD)
              .attr("EPR", EPR)
              .text(value.epl_EducationPlanName));
          });
          // $('#school_listing').DataTable().ajax.reload();
        } else {
          toastr.error($('#something_wrong_txt').val());
        }

        showLoader(false);
      },
      error: function (data) {
        toastr.error($('#something_wrong_txt').val());
        showLoader(false);
      }
    });
  }
}

function confirmDelete() {
  showLoader(true);
  var cid = $('#did').val();
  $.ajax({
    url: base_url + '/admin/schools/' + cid,
    type: 'DELETE',
    dataType: 'json',
    cache: false,
    success: function (result) {
      $('#delete_prompt').modal('hide');
      if (result.status) {
        toastr.success(result.message);
        $('#school_listing').DataTable().ajax.reload();
      } else {
        toastr.error(result.message);
      }

      showLoader(false);
    },
    error: function (data) {
      toastr.error($('#something_wrong_txt').val());
      showLoader(false);
    }
  });
}

function addPlan() {
  if ($('#eplan').val() != '') {
    var currSP = $('#eplan').val()


    if ($('.sch_' + $('#eplan').val()).length != 0) {
      toastr.error($('#education_plan_add_validate_txt').val());
      return;
    }
    if (currSP != '') {
      SP.push(currSP);
    }

    if ($('#eid').val() == "")
      $('.school_plan_table tbody').append('<tr class="sch sch_' + $('#eplan').val() + '"><td>' + ($('tr.sch').length + 1) + '</td><td>' + $('#eplan option:selected').attr('EPparent') + '</td><td>' + $('#eplan option:selected').attr('EPmain') + '</td><td>' + $('#eplan option:selected').text() + '</td><td>' + $('#eplan option:selected').attr('NEP') + '</td><td>' + $('#eplan option:selected').attr('QAD') + '</td><td>' + $('#eplan option:selected').attr('EPR') + '</td><td><div class="form-group form-check"><input type="checkbox" class="form-check-input" name="sep_Status[]" value="' + $('#eplan').val() + '" id="exampleCheck' + $('#eplan').val() + '"><label class="custom_checkbox"></label><label class="form-check-label label-text" for="exampleCheck' + $('#eplan').val() + '"><strong></strong></label><a target="_blank" href="' + base_url + '/admin/educationplan/' + $('#eplan').val() + '"><i class="fa fa-info-circle" aria-hidden="true"></i></a></div><a data-id="' + $('#eplan').val() + '" onclick="removePlanRow(this)" href="javascript:void(0)"><i style="color:red" class="fa fa-trash" aria-hidden="true"></i></a></td></tr>');
    else
      $('.school_plan_table tbody').append('<tr class="sch sch_' + $('#eplan').val() + '"><td>' + ($('tr.sch').length + 1) + '</td><td>' + $('#eplan option:selected').attr('EPparent') + '</td><td>' + $('#eplan option:selected').attr('EPmain') + '</td><td>' + $('#eplan option:selected').text() + '</td><td>' + $('#eplan option:selected').attr('NEP') + '</td><td>' + $('#eplan option:selected').attr('QAD') + '</td><td>' + $('#eplan option:selected').attr('EPR') + '</td><td><div class="form-group form-check"><input type="checkbox" class="form-check-input" name="sep_Status[]" value="' + $('#eplan').val() + '" id="exampleCheck' + $('#eplan').val() + '"><label class="custom_checkbox"></label><label class="form-check-label label-text" for="exampleCheck' + $('#eplan').val() + '"><strong></strong></label><a target="_blank" href="' + base_url + '/admin/educationplan/' + $('#eplan').val() + '"><i class="fa fa-info-circle" aria-hidden="true"></i></a></div><a data-id="' + $('#eplan').val() + '" onclick="removePlan(this)" href="javascript:void(0)"><i style="color:red" class="fa fa-trash" aria-hidden="true"></i></a></td></tr>');

    if ($('tr.sch').length != 0) {
      $(".school_plan_table").show();
    }

  } else {
    toastr.error($('#education_plan_validate_txt').val());
  }
}


function triggerDelete(cid) {
  $('#did').val(cid);
  $(".show_delete_modal").click();
}

function removePlanRow(elem) {
  var id = $(elem).attr('data-id');
  $('.sch_' + id).remove();
  SP = jQuery.grep(SP, function (value) {
    return value != id;
  });

  var i = 1;
  $.each($('.sch'), function (index, value) {
    $(value).children('td:first').text(i);
    i++
  });

  if ($('tr.sch').length == 0) {
    $(".school_plan_table").hide();
  }
}

function removePlan(elem) {
  var id = $(elem).attr('data-id');
  showLoader(true);
  $.ajax({
    url: base_url + '/admin/educationPlanDeleteCheck',
    type: 'POST',
    dataType: 'json',
    cache: false,
    data: { 'cid': id, 'eid': $('#eid').val() },
    success: function (result) {
      showLoader(false);
      if (!result.status) {
        toastr.error(result.message);
      } else {

        $('.sch_' + id).remove();
        SP = jQuery.grep(SP, function (value) {
          return value != id;
        });

        var i = 1;
        $.each($('.sch'), function (index, value) {
          $(value).children('td:first').text(i);
          i++
        });

        if ($('tr.sch').length == 0) {
          $(".school_plan_table").hide();
        }
      }

    },
    error: function (data) {
      toastr.error($('#something_wrong_txt').val());
      showLoader(false);
    }
  });
}

function cleanHrs(val) {
  var vv = val.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');
  $(val).val(vv);
}

$('#havent_identification_number').click(function () {
  if ($(this).prop("checked") == true) {
    $("#emp_EmployeeID").val('');
    $('.opt_id').addClass('hide_content');
    $('.opt_tmp_id').removeClass('hide_content');
  }
  else if ($(this).prop("checked") == false) {
    $("#emp_TempCitizenId").val('');
    $('.opt_tmp_id').addClass('hide_content');
    $('.opt_id').removeClass('hide_content');
  }
});