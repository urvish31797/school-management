var page_url = base_url + '/admin/schoolyear';
var form = $("form[name='add-schoolYear-form']");
var selectedCantons = [];

$(function () {
  $(".datepicker-year").datepicker({
    format: "yyyy",
    viewMode: "years",
    autoclose: true,
    minViewMode: "years",
  });

  $('.pl-5.pr-5 .profile_info_container .select2drp').select2({
    "language": {
      "noResults": function () {
        return $('#no_result_text').val();
      }
    },
  });

  $("#search_schoolYear").on('keyup', function () {
    $('.schoolYear_listing').DataTable().ajax.reload()
  });

  $('#add_new').on('hidden.bs.modal', function () {
    $("form").trigger("reset");
    $("#pkSye").val('');
  })

  $('#add_new').on('shown.bs.modal', function () {
    $("form").data('validator').resetForm();
  })

  $('#delete_prompt').on('hidden.bs.modal', function () {
    $("#did").val('');
  });
});

function save() {
  if (form.valid()) {
    if (!$(".selectedweekhrs").length) {
      toastr.error($("#add_week_hrs_msg").val());
      return;
    }
    showLoader(true);
    var formData = new FormData($(form)[0]);
    formData.append("pkSye", $("#pkSye").val());
    $.ajax({
      url: page_url,
      type: 'POST',
      processData: false,
      contentType: false,
      cache: false,
      data: formData,
      success: function (result) {
        toastr.success(result.message);
        setTimeout(() => {
          redirectPage('admin/schoolyear');
        }, 1500);
      },
      error: function (jqXHR) {
        if (jqXHR.status == 400) {
          toastr.error(jqXHR.responseJSON.message);
        }
        else if (jqXHR.status == 422) {
          setvalidationmessages(jqXHR.responseJSON.errors);
        }
        else {
          toastr.error($('#something_wrong_txt').val());
        }
      }
    });
    showLoader(false);
  }
}

function confirmDelete() {
  showLoader(true);
  var cid = $('#did').val();
  $.ajax({
    url: page_url + '/' + cid,
    type: 'DELETE',
    dataType: 'json',
    cache: false,
    success: function (result) {
      $('#delete_prompt').modal('hide');
      toastr.success(result.message);
      $('.schoolYear_listing').DataTable().ajax.reload();
    },
    error: function (jqXHR) {
      if (jqXHR.status == 400) {
        toastr.error(jqXHR.responseJSON.message);
      }
      else {
        toastr.error($('#something_wrong_txt').val());
      }
    }
  });
  showLoader(false);
}

function confirmchangeYear() {
  showLoader(true);
  var cyid = $('#cyid').val();
  $.ajax({
    url: change_url,
    type: 'POST',
    dataType: 'json',
    cache: false,
    data: { 'cyid': cyid },
    success: function (result) {
      $('#year_prompt').modal('hide');
      toastr.success(result.message);
      $('.schoolYear_listing').DataTable().ajax.reload();
    },
    error: function (jqXHR) {
      if (jqXHR.status == 400) {
        toastr.error(jqXHR.responseJSON.message);
      }
      else {
        toastr.error($('#something_wrong_txt').val());
      }
    }
  });
  showLoader(false);
}

function triggerDelete(cid) {
  $('#did').val(cid);
  $(".show_delete_modal").click();
}

function changeYearModal(cyid) {
  $("#cyid").val(cyid);
  $(".show_year_modal").click();
}

function cleanWeeks(val) {
  var vv = val.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');
  $(val).val(vv);
}

function addWeek(elem) {
  if ($('#fkSywCan').val() == '') {
    toastr.error($('#canton_validate_msg').val());
    return;
  }

  if ($('#sye_NumberofWeek').val() == '') {
    toastr.error($('#week_hour_validate_msg').val());
    return;
  }

  var currCanton = $('#fkSywCan').val();

  if ($('.weekhrs_' + currCanton).length) {
    toastr.error($('#canton_added_validate_msg').val());
    return;
  }

  $('.weekhour_table tr:last').before('<tr class="main_weekhour selectedweekhrs weekhrs_' + currCanton + '"><td>' + $('.weekhour_table tr.main_weekhour').length + '</td><td>' + $('#fkSywCan option:selected').text() + '</td><td><input class="form-control form-control-border" maxlength="2" type="text" id="sye_NumberofWeek_' + currCanton + '" required="true" name="sye_NumberofWeek[]" value="' + $("#sye_NumberofWeek").val() + '"></td><td><input type="hidden" name="fkSywCan[]" value="' + currCanton + '"><a data-id="' + currCanton + '" onclick="removeWeeks(this)" href="javascript:void(0)" class="theme_btn red_btn min_btn">' + $('#delete_txt').val() + '</a></td></tr>');
  $('.weekhour_table .main_weekhour tr:first td:first').text(1);
  $('.sr').text($('.weekhour_table tr.main_weekhour').length);
  if (currCanton != '') {
    selectedCantons.push(currCanton);
  }

  $('#fkSywCan').val('').trigger('change');
  $('#sye_NumberofWeek').val('');
}

function removeWeeks(elem) {
  var i = 1;
  var id = $(elem).attr('data-id');

  $('.weekhrs_' + id).remove();
  $('.weekhour_table .main_weekhour td:first').text($('.weekhour_table tr.mcg').length);

  selectedCantons = jQuery.grep(selectedCantons, function (value) {
    return value != id;
  });
  $.each($('.weekhour_table .main_weekhour:not(:last-child)'), function (index, value) {
    $(value).children('td:first').text(i++);
  });
  $('.sr').text($('.weekhour_table tr.main_weekhour').length);
}
