/**
* Engage Employee
*
* This file is used for admin JS
*
* @package    Laravel
* @subpackage JS
* @since      1.0
*/
var selectedWeeks = [];
var form = $("form[name='add-classcalendar-workingweeks-form']");

$(function () {

    showLoader(false);

    $('.start_datepicker').datepicker({
        format: "dd/mm/yyyy",
        autoclose: true
    });

    $('.end_datepicker').datepicker({
        format: "dd/mm/yyyy",
        autoclose: true
    });

    $("#search_working_weeks").on('keyup', function () {
        $('.class_working_week_listing').DataTable().ajax.reload()
    });

    $('#delete_prompt').on('hidden.bs.modal', function () {
        $("#did").val('');
    });

    form.validate({
        errorClass: "error_msg",
        rules: {
            fkCcwShi: {
                required: true,
            }
        },
        submitHandler: function (f, event) {
            event.preventDefault();

            if (!$(".selectedweekhrs").length) {
                toastr.error($("#add_class_week_msg").val());
                return;
            }

            if ($("#pkCcw").val() != 0) {
                if ($("#fkCcwWek").val() == ""
                    || $("#fkCcwSteOne").val() == ""
                    || $("#fkCcwSteTwo").val() == ""
                    || $("#start_date").val() == ""
                    || $("#end_date").val() == "") {
                    toastr.error($("#please_fillup_required_fields_txt").val());
                    return;
                }
            }

            showLoader(true);
            var formData = new FormData(form[0]);
            formData.append("pkCcw", $("#pkCcw").val());
            $.ajax({
                url: base_url + '/employee/class-working-weeks',
                type: 'POST',
                processData: false,
                contentType: false,
                cache: false,
                data: formData,
                success: function (result) {
                    if (result.status) {
                        toastr.success(result.message);
                        setTimeout(() => {
                            window.location.href = base_url + '/employee/class-working-weeks';
                        }, 1500);
                    } else {
                        toastr.error(result.message);
                    }

                    showLoader(false);
                },
                error: function (data) {
                    toastr.error($('#something_wrong_txt').val());
                    showLoader(false);
                }
            });
        }
    });

});

function addWeek() {
    if ($('#fkCcwWek').val() == '') {
        toastr.error($('#week_select_msg').val());
        return;
    }

    if ($('#fkCcwSteOne').val() == '') {
        toastr.error($('#stu_one_select_msg').val());
        return;
    }

    if ($('#fkCcwSteTwo').val() == '') {
        toastr.error($('#stu_two_select_msg').val());
        return;
    }

    if ($('#start_date').val() == '') {
        toastr.error($('#start_date_select_msg').val());
        return;
    }

    if ($('#end_date').val() == '') {
        toastr.error($('#end_date_select_msg').val());
        return;
    }

    var current_week = $('#fkCcwWek').val();
    var current_student1 = $('#fkCcwSteOne').val();
    var current_student2 = $('#fkCcwSteTwo').val();

    if ($('.weekhrs_' + current_week).length) {
        toastr.error($('#week_already_selected_msg').val());
        return;
    }

    var html = '<tr class="main_weekhour selectedweekhrs weekhrs_' + current_week + '">' +
        '<td>' + $('.classworkingweek_table tr.main_weekhour').length + '</td>' +
        '<td>' + $('#fkCcwWek option:selected').text() + '</td>' +
        '<td>' + $('#fkCcwSteOne option:selected').text() + '</td>' +
        '<td>' + $('#fkCcwSteTwo option:selected').text() + '</td>' +
        '<td>' +
        '<input class="form-control start_datepicker" type="text" id="start_date_' + current_week + '" required="true" name="ccw_startDate[]" value="' + $("#start_date").val() + '">' +
        '</td>' +
        '<td>' +
        '<input class="form-control end_datepicker" type="text" id="end_date_' + current_week + '" required="true" name="ccw_endDate[]" value="' + $("#end_date").val() + '">' +
        '</td>' +
        '<td>' +
        '<input class="form-control form-control-border" type="text" id="ccw_notes_' + current_week + '" required="true" name="ccw_notes[]" value="' + $("#ccw_notes").val() + '">' +
        '</td>' +
        '<td>' +
        '<input type="hidden" name="fkCcwWek[]" value="' + current_week + '">' +
        '<input type="hidden" name="fkCcwSteOne[]" value="' + current_student1 + '">' +
        '<input type="hidden" name="fkCcwSteTwo[]" value="' + current_student2 + '">' +
        '<a data-id="' + current_week + '" onclick="removeWeeks(this)" href="javascript:void(0)" class="theme_btn red_btn min_btn">' + $('#delete_txt').val() + '</a>' +
        '</td>' +
        '</tr>';

    $('.classworkingweek_table tr:last').before(html);
    $('.classworkingweek_table .main_weekhour tr:first td:first').text(1);
    $('.sr').text($('.classworkingweek_table tr.main_weekhour').length);
    if (current_week != '') {
        selectedWeeks.push(current_week);
    }

    $('#fkCcwWek').val('').trigger('change');
    $('#fkCcwSteOne').val('').trigger('change');
    $('#fkCcwSteTwo').val('').trigger('change');
    $('#start_date').val('');
    $('#end_date').val('');
    $('#ccw_notes').val('');

    $('.start_datepicker').datepicker({
        format: "dd/mm/yyyy",
        autoclose: true
    });

    $('.end_datepicker').datepicker({
        format: "dd/mm/yyyy",
        autoclose: true
    });

}

function removeWeeks(elem) {
    var i = 1;
    var id = $(elem).attr('data-id');

    $('.weekhrs_' + id).remove();
    $('.classworkingweek_table .main_weekhour td:first').text($('.classworkingweek_table tr.mcg').length);

    selectedWeeks = jQuery.grep(selectedWeeks, function (value) {
        return value != id;
    });
    $.each($('.classworkingweek_table .main_weekhour:not(:last-child)'), function (index, value) {
        $(value).children('td:first').text(i++);
    });
    $('.sr').text($('.classworkingweek_table tr.main_weekhour').length);
}

function triggerDelete(cid) {
    $('#did').val(cid);
    $(".show_delete_modal").click();
}

function confirmDelete() {
    showLoader(true);
    var cid = $('#did').val();
    $.ajax({
        url: base_url + '/employee/class-working-weeks/' + cid,
        type: 'DELETE',
        dataType: 'json',
        cache: false,
        success: function (result) {
            $('#delete_prompt').modal('hide');
            if (result.status) {
                toastr.success(result.message);
                $('.class_working_week_listing').DataTable().ajax.reload();
            } else {
                toastr.error(result.message);
            }

            showLoader(false);
        },
        error: function (data) {
            toastr.error($('#something_wrong_txt').val());
            showLoader(false);
        }
    });
}