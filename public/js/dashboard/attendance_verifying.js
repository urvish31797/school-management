var page_url = base_url + '/employee/attendance-verifying';
var delete_lecture_url = base_url + '/employee/delete-attendance-verifying-lecture';
var class_detail_url = base_url + '/employee/attendance-class-detail';
var form = $("form[name='edit-attend-accom-form']");

$(function () {

    $('#delete_prompt').on('hidden.bs.modal', function () {
        $("#did").val('');
    });

});

function confirmDelete() {
    showLoader(true);
    var cid = $('#did').val();
    $.ajax({
        url: page_url + "/" + cid,
        type: 'DELETE',
        dataType: 'json',
        cache: false,
        success: function (result) {
            $('#delete_prompt').modal('hide');
            $('#confirm_delete_prompt').modal('hide');
            toastr.success(result.message);
            $('.attendance_listing').DataTable().ajax.reload();
        },
        error: function (jqXHR) {
            if (jqXHR.status == 400) {
                toastr.error(jqXHR.responseJSON.message);
            }
            else {
                toastr.error($('#something_wrong_txt').val());
            }
        }
    });
    showLoader(false);
}

function getclass_detail(id) {
    showLoader(true);
    $.ajax({
        url: class_detail_url,
        type: 'POST',
        dataType: 'json',
        cache: false,
        data: { 'pkDbl': id },
        success: function (result) {
            if (result.status) {
                $(".lecture_tab").html('');
                $(".lecture_tab").html(result.data);
                $(".lecture_tab").show();
            }
        },
        error: function (jqXHR) {
            if (jqXHR.status == 400) {
                toastr.error(jqXHR.responseJSON.message);
            }
            else {
                toastr.error($('#something_wrong_txt').val());
            }
        }
    });
    showLoader(false);
}

function save() {
    var checkbox = [];
    var verified_status = true;
    $('.radio_status').each(function () {
        if ($(this).find('input[type="radio"]:checked').length > 0) {
        }
        else {
            checkbox.push(true);
        }
    });

    console.log(verified_status);

    if ($('.radio_status').length == checkbox.length) {
        toastr.error($('#student_attendance_status_msg').val());
        checkbox = [];
        return;
    }
    else if (checkbox.length != 0 && checkbox.length < $('.radio_status').length) {
        verified_status = false;
    }
    else if (checkbox.length == 0) {
        verified_status = true;
    }

    console.log(verified_status);

    showLoader(true);
    var form = document.querySelector('form');
    var formData = new FormData(form);
    formData.append('verified_status', verified_status);

    $.ajax({
        url: page_url,
        type: 'POST',
        processData: false,
        contentType: false,
        cache: false,
        data: formData,
        success: function (result) {
            toastr.success(result.message);
            $('#add_new').modal('hide');
            setTimeout(() => {
                window.location.reload();
            }, 1200);
        },
        error: function (jqXHR) {
            if (jqXHR.status == 400) {
                toastr.error(jqXHR.responseJSON.message);
            }
            else if (jqXHR.status == 422) {
                setvalidationmessages(jqXHR.responseJSON.errors);
            }
            else {
                toastr.error($('#something_wrong_txt').val());
            }
        }
    });
    showLoader(false);
}

function confirmDeleteLectureDetail() {
    showLoader(true);
    var cid = $('#lecture_did').val();
    $.ajax({
        url: delete_lecture_url,
        type: 'POST',
        dataType: 'json',
        data: { 'pkDbl': cid },
        cache: false,
        success: function (result) {
            $('#confirm_delete_prompt').modal('hide');
            $('#delete_prompt').modal('hide');
            toastr.success(result.message);
            setTimeout(() => {
                window.location.reload();
            }, 1200);
        },
        error: function (jqXHR) {
            if (jqXHR.status == 400) {
                toastr.error(jqXHR.responseJSON.message);
            }
            else {
                toastr.error($('#something_wrong_txt').val());
            }
        }
    });
    showLoader(false);
}

function triggerDelete(cid) {
    $('#did').val(cid);
    $(".show_delete_modal").click();
}

function confirmDeleteLecture(lid) {
    $('#lecture_did').val(lid);
    $(".show_delete_modal").click();
}