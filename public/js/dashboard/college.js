/**
* Colleges
*
* This file is used for admin JS
*
* @package    Laravel
* @subpackage JS
* @since      1.0
*/
var page_url = base_url + '/admin/colleges';
var form = $("form[name='add-college-form']");

$(function () {

  $(".datepicker-year").datepicker({
    format: "yyyy",
    viewMode: "years",
    autoclose: true,
    endDate: '+0d',
    minViewMode: "years"
  });

  $("#upload_profile").on('change', function () {
    if (document.getElementById("upload_profile").files.length == 0) {
      $('#college_img').attr('src', $('#img_tmp').val());
    }
    selectProfileImage(this);
  });

  $("[name='col_BelongsToUniversity']").on('change', function () {
    if ($(this).val() == 'Yes') {
      $('.university_option').show();
    } else {
      $("#fkColUni").val('');
      $('.university_option').hide();
    }
  });

  $("#search_college").on('keyup', function () {
    $('.colleges_listing').DataTable().ajax.reload();
  });

  $("#country_filter").on('change', function () {
    $('.colleges_listing').DataTable().ajax.reload()
  });

  $("#year_filter").on('change', function () {
    $('.colleges_listing').DataTable().ajax.reload()
  });

  $("#ownership_filter").on('change', function () {
    $('.colleges_listing').DataTable().ajax.reload()
  });

  $('#university_filter').on('change', function () {
    $('.colleges_listing').DataTable().ajax.reload()
  });

  $('#add_new').on('hidden.bs.modal', function () {
    var validator = form.validate();
    validator.resetForm();
    $('#college_img').attr('src', $('#img_tmp').val());
    $("form").trigger("reset");
    $("#pkCol").val('');
  });

  $('#add_new').on('shown.bs.modal', function () {
    $("form").data('validator').resetForm();
  });

  $('#delete_prompt').on('hidden.bs.modal', function () {
    $("#did").val('');
  });
});

function save() {
  if (form.valid()) {
    showLoader(true);
    var formData = new FormData($(form)[0]);
    formData.append("pkCol", $("#pkCol").val());
    $.ajax({
      url: page_url,
      type: 'POST',
      processData: false,
      contentType: false,
      cache: false,
      data: formData,
      success: function (result) {
        toastr.success(result.message);
        $('#add_new').modal('hide');
        $('.colleges_listing').DataTable().ajax.reload();
      },
      error: function (jqXHR) {
        if (jqXHR.status == 400) {
          toastr.error(jqXHR.responseJSON.message);
        }
        else if (jqXHR.status == 422) {
          setvalidationmessages(jqXHR.responseJSON.errors);
        }
        else {
          toastr.error($('#something_wrong_txt').val());
        }
      }
    });
    showLoader(false);
  }
}

function confirmDelete() {
  showLoader(true);
  var cid = $('#did').val();
  $.ajax({
    url: page_url + '/' + cid,
    type: 'DELETE',
    dataType: 'json',
    cache: false,
    success: function (result) {
      $('#delete_prompt').modal('hide');
      toastr.success(result.message);
      $('.colleges_listing').DataTable().ajax.reload();
    },
    error: function (jqXHR) {
      if (jqXHR.status == 400) {
        toastr.error(jqXHR.responseJSON.message);
      }
      else {
        toastr.error($('#something_wrong_txt').val());
      }
    }
  });
  showLoader(false);
}

function triggerEdit(cid) {
  showLoader(true);
  $.ajax({
    url: page_url + '/' + cid + '/edit',
    type: 'GET',
    dataType: 'json',
    cache: false,
    success: function (result) {
      $.each(result.data, function (index, value) {
        $("#" + index).val(value);
      });
      $("#pkCol").val(result.data.pkCol);
      if (result.data.col_PicturePath != null && result.data.col_PicturePath != '') {
        $('#college_img').attr('src', result.data.col_PicturePath);
      }

      $("#col_YearStartedFounded").val(result.data.col_YearStartedFounded);
      $("input[name=col_BelongsToUniversity][value='" + result.data.col_BelongsToUniversity + "']").prop("checked", true);
      console.log("university: ", result.data.col_BelongsToUniversity);
      if (result.data.col_BelongsToUniversity == 'Yes') {
        $('.university_option').show();
      } else {
        $('.university_option').hide();
      }
      $(".show_modal").click();
    },
    error: function (jqXHR) {
      if (jqXHR.status == 400) {
        toastr.error(jqXHR.responseJSON.message);
      }
      else {
        toastr.error($('#something_wrong_txt').val());
      }
    }
  });
  showLoader(false);
}

function triggerDelete(cid) {
  $('#did').val(cid);
  $(".show_delete_modal").click();
}

function selectProfileImage(input) {
  if (input.files && input.files[0]) {
    var reader = new FileReader();

    reader.onload = function (e) {
      jQuery('#college_img').attr('src', e.target.result);
    }

    reader.readAsDataURL(input.files[0]);
  }
}
