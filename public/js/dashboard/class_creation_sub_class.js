var CSA = [];
$(function () {

  if ($('.select2_multi_view').length) {
    $('.select2_multi_view').select2();
  }
  showLoader(false);
  $('.select2_drop').select2({
    "language": {
      "noResults": function () {
        return $('#no_result_text').val();
      }
    },
    placeholder: $('#select_txt').val(),
    allowClear: true
  });

  $('#student_listing').on('processing.dt', function (e, settings, processing) {
    if (processing) {
      showLoader(true);
    } else {
      showLoader(false);
    }
  }).DataTable({
    "columnDefs": [{
      "targets": 4,
      "createdCell": function (td, cellData, rowData, row, col) {
        if (cellData == 'Active') {
          $(td).addClass('active_status');
        } else {
          $(td).addClass('disable_status');
        }
      }
    }],
    "language": {
      "sLengthMenu": $('#show_txt').val() + " _MENU_ " + $('#entries_txt').val(),
      "info": $('#showing_txt').val() + " _START_ " + $('#to_txt').val() + " _END_ " + $('#of_txt').val() + " _TOTAL_ " + $('#entries_txt').val(),
      "emptyTable": $('#msg_no_data_available_table').val(),
      "paginate": {
        "previous": $('#previous_txt').val(),
        "next": $('#next_txt').val()
      }
    },
    "lengthMenu": [500, 600, 700, 800],
    "searching": false,
    "serverSide": true,
    "deferRender": true,
    "ajax": {
      "url": base_url + "/employee/fetch-subclass-students-list",
      "type": "POST",
      "dataType": 'json',
      "data": function (d) {
        d.search = $('#search_student').val();
        d.pkCcs = $('#pkCcs').val();
      }
    },
    columns: [
      { "data": "index", className: "text-center" },
      {
        "data": "index", sortable: !1,
        render: function (data, type, mb) {
          if (mb.student.stu_StudentID == '' || mb.student.stu_StudentID == null) {
            return mb.student.stu_TempCitizenId;
          } else {
            return mb.student.stu_StudentID;
          }
        }
      },
      {
        "data": "index", sortable: !1,
        render: function (data, type, mb) {
          return mb.student.stu_StudentName + ' ' + mb.student.stu_StudentSurname;
        }
      },
      {
        "data": "index", sortable: !1,
        render: function (data, type, mb) {
          return mb.grade.gra_GradeNumeric;
        }
      },
      {
        "data": "index", sortable: !1,
        render: function (data, type, mb) {
          return mb.education_program.edp_Name + ' - ' + mb.education_plan.epl_EducationPlanName;
        }
      },
      {
        "data": "cla_ClassName", sortable: !1,
        render: function (data, type, mb) {
          var suid = null;
          if (mb.student.stu_StudentID == '' || mb.student.stu_StudentID == null) {
            suid = mb.student.stu_TempCitizenId;
          } else {
            suid = mb.student.stu_StudentID;
          }
          return '<button type="button" cid="' + mb.pkSte + '" suid="' + suid + '" sname="' + mb.student.stu_StudentName + ' ' + mb.student.stu_StudentSurname + '" gra_name="' + mb.grade.gra_GradeNumeric + '" gra_id="' + mb.grade.pkGra + '" ep_id="' + mb.education_plan.pkEpl + '" ep_name="' + mb.education_program.edp_Name + ' - ' + mb.education_plan.epl_EducationPlanName + '" class="theme_btn min_btn" onclick="addStudent(this)">' + $('#add_txt').val() + '</button>'
        }
      },
    ],

  });

  $('#sub_class_view_listing').on('processing.dt', function (e, settings, processing) {
    if (processing) {
      showLoader(true);
    } else {
      showLoader(false);
    }
  }).DataTable({
    "columnDefs": [{
      "targets": 2,
      "createdCell": function (td, cellData, rowData, row, col) {
        if (cellData == 'Active') {
          $(td).addClass('active_status');
        } else {
          $(td).addClass('disable_status');
        }
      }
    }],
    "language": {
      "sLengthMenu": $('#show_txt').val() + " _MENU_ " + $('#entries_txt').val(),
      "info": $('#showing_txt').val() + " _START_ " + $('#to_txt').val() + " _END_ " + $('#of_txt').val() + " _TOTAL_ " + $('#entries_txt').val(),
      "emptyTable": $('#msg_no_data_available_table').val(),
      "paginate": {
        "previous": $('#previous_txt').val(),
        "next": $('#next_txt').val()
      }
    },
    "lengthMenu": [10, 20, 30, 50],
    "searching": false,
    "serverSide": true,
    "deferRender": true,
    "ajax": {
      "url": base_url + "/employee/subclass-list",
      "type": "POST",
      "dataType": 'json',
      "data": function (d) {
        d.search = $('#search_sub_class').val();
        d.pkClr = $('#pkClr').val();
      }
    },
    columns: [
      { "data": "index", className: "text-center" },
      {
        "data": "index", sortable: !1,
        render: function (data, type, mb) {
          return mb.sub_class_group.scg_Name;
        }
      },
      {
        "data": "index", sortable: !1,
        render: function (data, type, mb) {
          return mb.class_creation_courses.crs_CourseName;
        }
      },
      {
        "data": "cla_ClassName", sortable: !1,
        render: function (data, type, mb) {

          return '<a sgid="' + mb.fkCsaScg + '" cid="' + mb.fkCsaEmc + '" class="ajax_request no_sidebar_active" href="' + base_url + '/employee/classcreation/view-subclass-students/' + mb.pkCsa + '" title="' + $('#view_detail_text').val() + '"><img src="' + imagepath + 'ic_eye_color.png"></a>'
        }
      },
    ],

  });

  $("#search_student").on('keyup', function () {
    $('#student_listing').DataTable().ajax.reload()
  });

  $("#search_sub_class").on('keyup', function () {
    $('#sub_class_view_listing').DataTable().ajax.reload()
  });

  $('#delete_prompt').on('hidden.bs.modal', function () {
    $("#did").val('');
  });

  // $("#course_select").on('change',function(){
  //   if($('.stu').length > 0)
  //   {
  //     $('.stu').remove();
  //     $(".sel_stu_elem").addClass('hide_content');
  //   }
  // });

  $("form[name='sub_class_form']").validate({
    errorClass: "error_msg",
    errorPlacement: function (error, element) {
      if (element.hasClass('select2_multi5') && element.next('.select2-container').length) {
        error.insertAfter(element.next('.select2-container'));
      } else {
        error.insertAfter(element);
      }
    },
    submitHandler: function (form, event) {
      event.preventDefault();
      if ($('.stu').length == 0) {
        toastr.error($('#stu_sel_txt').val());
        return;
      }
      showLoader(true);
      var formData = new FormData($(form)[0]);
      formData.append("pkCcs", $("#pkCcs").val());
      formData.append("CSA", CSA);
      $.ajax({
        url: base_url + '/employee/subclass/create',
        type: 'POST',
        processData: false,
        contentType: false,
        cache: false,
        data: formData,
        success: function (result) {
          if (result.status) {
            toastr.success(result.message);
            redirectPage("employee/classcreation");
          } else {
            toastr.error(result.message);
          }

          showLoader(false);
        },
        error: function (data) {
          toastr.error($('#something_wrong_txt').val());
          showLoader(false);
        }
      });

    }
  });




});

function addStudent(elem) {

  // if($('.stu_'+$(elem).attr('cid')+'.sub_'+$('#sub_select').val()+'.course_'+$('#course_select').val()).length != 0){
  //   toastr.error($('#subclass_create_txt').val());
  //   return;
  // }

  if ($('#sub_select').val() == "") {
    toastr.error($('#subclass_txt').val());
    return;
  }

  if ($('#course_select').val() == "") {
    toastr.error($('#subclass_course_txt').val());
    return;
  }

  if ($('#teacher_select').val() == "") {
    toastr.error($('#subclass_teacher_txt').val());
    return;
  }

  if ($('.stu_' + $(elem).attr('cid')).length != 0) {
    toastr.error($('#stu_sel_valid_txt').val());
    return;
  }

  if ($('#course_select option:selected').attr('data-gra') != $(elem).attr('gra_id')) {
    toastr.error($('#subclass_grade_txt').val());
    return;
  }

  var count = $('.stu').length;
  count = count + 1;

  $('.class_seleted_students tr:last').after('<tr class="stu stu_' + $(elem).attr('cid') + ' sub_' + $('#sub_select').val() + ' teacher_' + $('#teacher_select').val() + ' course_' + $('#course_select').val() + '"><td>' + count + '</td><td><input type="hidden" name="subClass[]" value="' + $('#sub_select').val() + '">' + $('#sub_select option:selected').text() + '</td><td>' + $(elem).attr('suid') + '</td><td>' + $(elem).attr('sname') + '</td><td><input type="hidden" name="course[]" value="' + $('#course_select').val() + '">' + $('#course_select option:selected').text() + '</td><td><input type="hidden" name="teacher[]" value="' + $('#teacher_select').val() + '">' + $('#teacher_select option:selected').text() + '</td><td><input type="hidden" name="grades[]" value="' + $(elem).attr('gra_id') + '"><input type="hidden" name="stu_ids[]" value="' + $(elem).attr('cid') + '"><a data-id="' + $(elem).attr('cid') + '" data-remove=".stu_' + $(elem).attr('cid') + '.sub_' + $('#sub_select').val() + '.teacher_' + $('#teacher_select').val() + '.course_' + $('#course_select').val() + '" onclick="removeSelStu(this)" href="javascript:void(0)" class="theme_btn red_btn min_btn">' + $('#delete_txt').val() + '</a></td></tr>');

  $("#eid").val($(elem).attr('emp-id'));
  $(".sel_stu_elem").removeClass('hide_content');
}

function removeSelStu(elem) {
  var i = 1;
  var id = $(elem).attr('data-remove');
  CSA.push($(elem).attr('data-csa'));
  $(id).remove();

  if ($('.stu').length == 0) {
    $(".sel_stu_elem").addClass('hide_content');
  }

  $.each($('.stu'), function (index, value) {
    $(value).children('td:first').text(i++);
  });
}

function triggerEduPlan(elem) {
  showLoader(true);

  var pkEpl = $(elem).attr('data-edu');
  var pkSen = $(elem).attr('data-stu');
  var pkGra = $(elem).attr('data-gra');
  var pkClr = $('#pkClr').val();
  var newData = { 'pkEpl': pkEpl, 'pkSen': pkSen, 'pkClr': pkClr, 'pkGra': pkGra }

  $.ajax({
    url: base_url + '/employee/fetchClassEducationPlan',
    type: 'POST',
    data: newData,
    success: function (result) {
      if (result.status) {
        $('.course_details').html(result.course_details);
        $("#trigger_edu_pop").click();
      } else {
        toastr.error(result.message);
      }

      showLoader(false);
    },
    error: function (data) {
      toastr.error($('#something_wrong_txt').val());
      showLoader(false);
    }
  });
}



function prevTab(tab) {
  var currTab = tab + 1;
  $('.tab-pane').removeClass('active show');
  $('#menu' + tab).addClass('active show');
  $('.btn-round-' + currTab).removeClass('active');
}

function activaTab(tab) {
  $('.tab-pane').removeClass('active show');
  $('#menu' + tab).addClass('active show');
  $('.btn-round-' + tab).addClass('active');
};

function confirmDelete() {
  showLoader(true);
  var cid = $('#did').val();
  $.ajax({
    url: base_url + '/admin/deleteClass',
    type: 'POST',
    dataType: 'json',
    cache: false,
    data: { 'cid': cid },
    success: function (result) {
      $('#delete_prompt').modal('hide');
      if (result.status) {
        toastr.success(result.message);
        $('#classes_listing').DataTable().ajax.reload();
      } else {
        toastr.error($('#something_wrong_txt').val());
      }

      showLoader(false);
    },
    error: function (data) {
      toastr.error($('#something_wrong_txt').val());
      showLoader(false);
    }
  });
}


function triggerDelete(cid) {
  $('#did').val(cid);
  $(".show_delete_modal").click();
}

$('#is_village_school').click(function () {
  $('#fkClrVsc').val('');
  if ($(this).prop("checked") == true) {
    $('.village_schools_drp').removeClass('hide_content');
  }
  else if ($(this).prop("checked") == false) {
    $('.village_schools_drp').addClass('hide_content');
  }
});

// Class Creation listing Delete
function triggerDeleteClsCre(cid) {
  $('#did').val(cid);
  $(".show_delete_modal").click();
}

function triggerStudentCourseDelete(id) {
  $('#did').val(id);
  $(".show_delete_modal").click();
}

function courseDelete(flag) {
  showLoader(true);
  $.ajax({
    url: base_url + '/employee/delete-subclass-course',
    type: 'POST',
    data: { 'flag': flag, 'did': $('#did').val() },
    success: function (result) {
      $('#delete_prompt').modal('hide');
      if (result.status) {
        toastr.success(result.message);
        setTimeout(() => {
          window.location.href = base_url + "/employee/classcreation";
        }, 1000);

      } else {
        toastr.error($('#something_wrong_txt').val());
      }
    },
    error: function (data) {
      toastr.error($('#something_wrong_txt').val());
      showLoader(false);
    }
  });
}