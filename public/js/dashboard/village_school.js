var page_url = '';
if($('.is_HSA').length == 0){
  page_url = base_url+"/employee/villageschools";
}else{
  page_url = base_url+"/admin/villageschools";
}
var form = $("form[name='add-village-school-form']");

$(function() {

  $("#search_village_school").on('keyup', function () {
    $('.village_school_listing').DataTable().ajax.reload();
  });

  $('#add_new').on('hidden.bs.modal', function () {
    $("form").trigger("reset");
    $("#pkVsc").val('');
  })

  $('#add_new').on('shown.bs.modal', function () {
    $("form").data('validator').resetForm();
  });

  $('#delete_prompt').on('hidden.bs.modal', function () {
    $("#did").val('');
  });

});

function save()
{
  if(form.valid())
  {
    showLoader(true);    
    var formData = new FormData($(form)[0]);
    formData.append("pkVsc", $("#pkVsc").val());
    if($('.is_HSA').length == 0){
      formData.append("fkVscSch", $("#fkVscSch").val());
    }
    $.ajax({
        url: page_url,
        type: 'POST',
        processData: false,
        contentType: false,
        cache: false,
        data: formData,
        success: function(result)
        {  
          toastr.success(result.message);
          $('#add_new').modal('hide');
          $('.village_school_listing').DataTable().ajax.reload();   
        },
        error: function (jqXHR) {
        if (jqXHR.status == 400) {
          toastr.error(jqXHR.responseJSON.message);
        }
        else if (jqXHR.status == 422) {
          setvalidationmessages(jqXHR.responseJSON.errors);
        }
        else {
          toastr.error($('#something_wrong_txt').val());
        }
      }
    });
    showLoader(false);
  }
}

function confirmDelete(){
  showLoader(true);
  var cid = $('#did').val();
  $.ajax({
    url: page_url+"/"+cid,
    type: 'DELETE',
    dataType:'json',
    cache: false,
    success: function(result)
    {
      $('#delete_prompt').modal('hide');
      toastr.success(result.message);
      $('.village_school_listing').DataTable().ajax.reload();
    },
    error: function (jqXHR) {
      if (jqXHR.status == 400) {
        toastr.error(jqXHR.responseJSON.message);
      }
      else {
        toastr.error($('#something_wrong_txt').val());
      }
    }
  });
  showLoader(false);
}

function triggerEdit(cid){
  showLoader(true);
  $.ajax({
    url: page_url+"/"+cid+"/edit",
    type: 'GET',
    dataType:'json',
    cache: false,
    data: {'cid':cid},
    success: function(result)
    {
      $.each(result.data, function(index, value) {
        $("#"+index).val(value);
      });
      $( ".show_modal" ).click();
    },
    error: function (jqXHR) {
      if (jqXHR.status == 400) {
        toastr.error(jqXHR.responseJSON.message);
      }
      else {
        toastr.error($('#something_wrong_txt').val());
      }
    }
  });
  showLoader(false);
}

function triggerDelete(cid){
   $('#did').val(cid);
   $( ".show_delete_modal" ).click();
}
