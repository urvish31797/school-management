/**
* Employee
*
* This file is used for admin JS
*
* @package    Laravel
* @subpackage JS
* @since      1.0
*/
$(function () {

  showLoader(false);
  var addEmp = '';
  if ($('#is_admin').val() == 1) {
    addEmp = base_url + '/admin/employees';
  } else {
    addEmp = base_url + '/employee/employees';
  }
  $("#upload_profile").on('change', function () {
    if (document.getElementById("upload_profile").files.length == 0) {
      $('#user_img').attr('src', $('#img_tmp').val());
    }
    selectProfileImage(this);
  });

  $("#fkEmpMun").on('change', function () {
    var id = $(this).children('option:selected').attr('data-countryId');
    $("#fkEmpCny").val(id);
  });

  $("#fkEenEpty").on('change', function () {
    var id = $(this).val();
    if (id == 1) {
      $("#principal_checkbox").show();
    }
    else {
      $("#principal_checkbox").hide();
    }

  });

  $(".datepicker-year").datepicker({
    format: "yyyy",
    viewMode: "years",
    autoclose: true,
    minViewMode: "years"
  });

  $(document).on('change', '.diploma_file', function () {
    var currData = $(this).attr('id');
    var ids = currData.split("_");
    var currId = ids[2];
    if ($(this).val() != '') {
      var file = $(this)[0].files[0];
      $('#file_name_' + currId).val(file.name);
    } else {
      $('#file_name_' + currId).val('');
      $('#eed_DiplomaPicturePath_' + currId).prop('required', true);
    }
  });

  $('#Current-tab').on('show.bs.tab', function () {
    $(".datepicker-year").datepicker({
      format: "yyyy",
      viewMode: "years",
      autoclose: true,
      endDate: '+0d',
      minViewMode: "years"
    });
  })

  $('#add_qa').on('click', function () {

    var ind = $('.profile_de_details_add .profile_info_container').length + 1;
    var prev = ind - 1;

    var universities = $("#fkEedUni_" + prev).val();
    var collage = $("#fkEedCol_" + prev).val();
    var acedemic = $("#fkEedAcd_" + prev).val();
    var qualification = $("#fkEedQde_" + prev).val();
    var designation = $("#fkEedEde_" + prev).val();
    var yearofpass = $("#eed_YearsOfPassing_" + prev).val();
    var shorttitle = $("#eed_ShortTitle_" + prev).val();
    var semesternumber = $("#eed_SemesterNumbers_" + prev).val();
    var ectpoint = $("#eed_EctsPoints_" + prev).val();
    var image = '';
    if ($("#eid").val() != "") {
      image = $("#file_name_" + prev).val();
      if (image == "Upload")
        image = "";
    }
    else
      image = $("#eed_DiplomaPicturePath_" + prev).val();

    if (universities == "" || collage == "" || acedemic == "" || qualification == "" || designation == "" || yearofpass == "" || shorttitle == "" || semesternumber == "" || ectpoint == "" || image == "") {
      toastr.error($('#please_fillup_required_fields').val());
      return;
    }
    var $ed = $('#profile_de_details .profile_info_container').clone();
    $ed.find('.rm_ed').attr('data-eed', ind);
    $ed.find('select,input').each(function (key, value) {
      this.id = this.id + '_' + ind;
      this.name = this.name + '_' + ind;
    });

    $('.profile_de_details_add .text-center:first').before($ed);
    $(".datepicker-year").datepicker({
      format: "yyyy",
      viewMode: "years",
      autoclose: true,
      endDate: '+0d',
      minViewMode: "years"
    });
  });

  $(document).on('click', '.rm_ed', function () {
    $(this).closest('.profile_info_container').remove();
    var inc = 1;
    $('.profile_de_details_add .profile_info_container').each(function (i, obj) {
      $(this).find('select,input').each(function (key, value) {
        var old_id = this.id.split("_");
        old_id.splice(-1, 1);
        old_id.push(inc);
        var new_id = old_id.join('_');

        this.id = new_id;
        this.name = new_id;
      });
      inc++;
    });
  });

  $('#teacher_listing').on('processing.dt', function (e, settings, processing) {
    if (processing) {
      showLoader(true);
    } else {
      showLoader(false);
    }
  }).DataTable({
    "columnDefs": [{
      "targets": 6,
      "createdCell": function (td, cellData, rowData, row, col) {
        if (cellData == 'Active') {
          $(td).addClass('active_status');
        } else {
          $(td).addClass('disable_status');
        }
      }
    }],
    "autoWidth": false,
    "language": {
      "sLengthMenu": $('#show_txt').val() + " _MENU_ " + $('#entries_txt').val(),
      "info": $('#showing_txt').val() + " _START_ " + $('#to_txt').val() + " _END_ " + $('#of_txt').val() + " _TOTAL_ " + $('#entries_txt').val(),
      "emptyTable": $('#msg_no_data_available_table').val(),
      "paginate": {
        "previous": $('#previous_txt').val(),
        "next": $('#next_txt').val()
      }
    },
    "lengthMenu": [10, 20, 30, 50],
    "searching": false,
    "serverSide": true,
    "deferRender": true,
    "ajax": {
      "url": listing_url,
      "type": "POST",
      "dataType": 'json',
      "data": function (d) {
        d.search = $('#search_teacher').val();
        d.emp_type = $('#employee_types').val();
        d.emp_eng_type = $('#employee_eng_types').val();
      }
    },
    columns: [
      { "data": "index", className: "text-center" },
      {
        "data": "emp_EmployeeID",
        render: function (data, type, emp) {
          if (emp.emp_EmployeeID != null) {
            return emp.emp_EmployeeID;
          }
          else {
            return emp.emp_TempCitizenId;
          }
        }
      },
      {
        "data": "emp_EmployeeName",
        render: function (data, type, emp) {
          return emp.emp_EmployeeName + " " + emp.emp_EmployeeSurname;
        }
      },
      { "data": "email" },
      {
        "data": "emp_EmployeeName",
        render: function (data, type, emp) {
          var types = emp.type.split(",");
          var roles = [];
          for (var i = 0; i < types.length; i++) {
            var role = types[i].trim();
            if (role == 'Teacher') {
              roles.push($("#teacher_txt").val());
            } else if (role == 'SchoolCoordinator') {
              roles.push($("#school_coordinator_txt").val());
            } else if (role == 'Principal') {
              roles.push($("#principal_txt").val());
            } else if (role == '') {
              roles.push($("#emp_not_engaged").val());
            }
          }

          return roles.toString();

        }
      },
      { "data": "emp_Status" },
      {
        "data": "emp_EmployeeName", sortable: !1,
        render: function (data, type, emp) {
          var view = '';
          var edit = '';
          if (window.location == base_url + '/employee/employees') {
            view = '<a class="ajax_request no_sidebar_active" href="' + base_url + '/employee/employees/' + emp.id + '"><img src="' + imagepath + 'ic_eye_color.png"></a>\t\t\t\t\t\t'
            edit = '<a class="ajax_request no_sidebar_active" href="' + base_url + '/employee/employees/' + emp.id + '/edit"><img src="' + imagepath + 'ic_mode_edit.png"></a>'
          } else {
            view = '<a class="ajax_request no_sidebar_active" href="' + base_url + '/admin/employees/' + emp.id + '"><img src="' + imagepath + 'ic_eye_color.png"></a>\t\t\t\t\t\t'
            edit = '<a class="ajax_request no_sidebar_active" href="' + base_url + '/admin/employees/' + emp.id + '/edit"><img src="' + imagepath + 'ic_mode_edit.png"></a>'
          }
          return view + edit;
        }
      },
    ],

  });

  $('#delete_prompt').on('hidden.bs.modal', function () {
    $("#did").val('');
  })

  $("#search_teacher").on('keyup', function () {
    //if($(this).val() != ''){
    $('#teacher_listing').DataTable().ajax.reload()
    // }
  });

  $("#employee_eng_types").on('change', function () {
    $('#teacher_listing').DataTable().ajax.reload();
  });

  $("#employee_types").on('change', function () {
    $('#teacher_listing').DataTable().ajax.reload();
  });

  $("#start_date").datepicker({
    format: "dd/mm/yyyy",
    autoclose: true,
    // startDate: '+0d'
  }).on('changeDate', function () {
    $('#end_date').datepicker('setStartDate', new Date($(this).val()));
  });

  $('#end_date').datepicker({
    format: "dd/mm/yyyy",
    autoclose: true,
    startDate: $("#start_date").val(),
  }).on('changeDate', function () {
    if ($("#start_date").val() != '') {
      $('#start_date').datepicker('setEndDate', new Date($(this).val()));
    }
  });

  $('.datepicker').datepicker({ format: "dd/mm/yyyy", autoclose: true, endDate: '+0d', });

  $("form[name='add-teacher-form']").validate({
    errorClass: "error_msg",
    rules: {
      email: {
        required: true,
        email: true,
        emailfull: true
      },
      emp_EmployeeName: {
        required: true,
        minlength: 2,
        maxlength: 30
      },
      emp_EmployeeSurname: {
        required: true,
        minlength: 2,
        maxlength: 30
      },
      emp_EmployeeID: {
        required: true,
        minlength: 13,
        maxlength: 13
      },
      emp_TempCitizenId: {
        required: true,
        minlength: 13,
        maxlength: 20
      },
      emp_PlaceOfBirth: {
        required: true,
        minlength: 3,
        maxlength: 30
      },
      emp_DateOfBirth: {
        required: true
      },
      emp_EmployeeGender: {
        required: true,
      },
      fkEmpMun: {
        required: true
      },
      fkEmpNat: {
        required: true
      },
      fkEmpRel: {
        required: true
      },
      fkEmpCtz: {
        required: true
      },
      fkEmpPof: {
        required: true
      },
    },
    submitHandler: function (form, event) {
      event.preventDefault();
      var status = false;
      if ($("#havent_identification_number").prop("checked") == true) {
        status = true;
      }
      else {
        status = checkEmployeeID();
      }
      if (status) {
        var formData = new FormData($(form)[0]);
        showLoader(true);
        var url = '';
        if ($("#eid").val() != "") {
          url = addEmp;
          formData.append("id", $('#eid').val());
          formData.append("engid", $('#engid').val());
        } else {
          url = addEmp;
        }

        formData.append("total_details", $('.profile_de_details_add .profile_info_container').length);
        formData.append("sid", $('#sid').val());
        $.ajax({
          url: url,
          type: 'POST',
          processData: false,
          contentType: false,
          cache: false,
          data: formData,
          success: function (result) {
            if (result.status) {
              toastr.success(result.message);
              setTimeout(() => {
                window.location.href = result.redirect;
              }, 1200);
            } else {
              toastr.error(result.message);
            }

            showLoader(false);
          },
          error: function (data) {
            toastr.error($('#something_wrong_txt').val());
            showLoader(false);
          }
        });
      }
    }
  });

  $("form[name='engage-teacher-form']").validate({
    errorClass: "error_msg",
    rules: {
      email: {
        required: true,
        email: true,
        emailfull: true
      },
      emp_EmployeeName: {
        required: true,
        minlength: 2,
        maxlength: 30
      },
      emp_EmployeeID: {
        required: true,
        minlength: 5,
        maxlength: 30
      },
      ewh_WeeklyHoursRate: {
        required: true,
        maxlength: 10
      },
      fkEenEty: {
        required: true,
      },
      start_date: {
        required: true
      }
    },
    submitHandler: function (form, event) {
      event.preventDefault();
      showLoader(true);
      var formData = new FormData($(form)[0]);
      var url = base_url + '/employee/employees';
      if ($("#eid").val() != "") {
        url = base_url + '/employee/editEmployee';
        formData.append("id", $('#eid').val());
      } else {
        url = base_url + '/employee/addEmployee';
      }

      formData.append("sid", $('#sid').val());
      $.ajax({
        url: url,
        type: 'POST',
        processData: false,
        contentType: false,
        cache: false,
        data: formData,
        success: function (result) {
          if (result.status) {
            toastr.success(result.message);
            setTimeout(() => {
              redirectPage("employee/employees");
            }, 2500);
          } else {
            toastr.error(result.message);
          }

          showLoader(false);
        },
        error: function (data) {
          toastr.error($('#something_wrong_txt').val());
          showLoader(false);
        }
      });
    }
  });

  $("form[name='edit_eng_form']").validate({
    errorClass: "error_msg",
    rules: {
      ewh_WeeklyHoursRate: {
        required: true,
        maxlength: 10
      },
      fkEenEty: {
        required: true,
      },
      fkEenEpty: {
        required: true,
      },
      start_date: {
        required: true
      }
    },
    submitHandler: function (form, event) {
      event.preventDefault();

      showLoader(true);
      var formData = new FormData($(form)[0]);
      var url = addEmp;
      $.ajax({
        url: url,
        type: 'POST',
        processData: false,
        contentType: false,
        cache: false,
        data: formData,
        success: function (result) {
          if (result.status) {
            toastr.success(result.message);
            if ($('#is_admin').val() == 1) {
              redirectPage("admin/employees");
            } else {
              redirectPage("employee/employees");
            }
          } else {
            toastr.error(result.message);
          }

          showLoader(false);
        },
        error: function (data) {
          toastr.error($('#something_wrong_txt').val());
          showLoader(false);
        }
      });
    }
  });


  jQuery.validator.addMethod("emailfull", function (value, element) {
    return this.optional(element) || /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i.test(value);
  }, $('#email_validate_txt').val());

});

function checkEmployeeID() {
  var birthDate = $("#emp_DateOfBirth").val();
  var gender = $("#emp_EmployeeGender").val();
  var employeeId = $('.isValidEmpID').val();
  var status = false;

  if (gender != "" && birthDate != "" && employeeId != "" && employeeId.length == 13) {
    var dd = employeeId.substring(0, 2);
    var mm = employeeId.substring(2, 4);
    var yy = employeeId.substring(4, 7);
    var rr = employeeId.substring(7, 9);
    var bb = employeeId.substring(9, 12);

    var bdata = birthDate.split("/");
    var d = bdata[0];
    var m = bdata[1];
    var y = bdata[2].substring(1, 4);

    if (dd == d && mm == m && yy == y && rr <= 19 && rr >= 10) {
      if (gender == "Male" && bb <= 499 && bb >= 0) {
        status = true;
      }
      else {
        if (gender == "Female" && bb <= 999 && bb >= 500) {
          status = true;
        }
      }
    }
  }

  if (status == false && employeeId != "" && gender != "" && birthDate != "") {
    $('.isValidEmpID').val("");
    toastr.error($("#employeemsgerror").val());
  }

  return status;
}

function selectProfileImage(input) {
  if (input.files && input.files[0]) {
    var reader = new FileReader();
    var filename = input.files[0].name;
    var fileExtension = filename.substr((filename.lastIndexOf('.') + 1));
    var fileExtensionCase = fileExtension.toLowerCase();
    if (fileExtensionCase == 'png' || fileExtensionCase == 'jpeg' || fileExtensionCase == 'jpg') {
      reader.onload = function (e) {
        jQuery('#user_img').attr('src', e.target.result);
      }
      reader.readAsDataURL(input.files[0]);
    } else {
      toastr.error($('#image_validation_msg').val());
      $('#upload_profile').val('');
      var user_img = imagepath + "user.png";

      $('#user_img').attr('src', user_img);

    }
  }
}

function fetchCollege(val) {
  var currData = $(val).attr('id');
  var ids = currData.split("_");
  var currId = ids[1];

  $('#fkEedCol_' + currId).find('option').not(':first').remove();
  var cid = val.value;
  if (cid != '') {
    showLoader(true);
    $.ajax({
      url: base_url + '/employee/fetchCollege',
      type: 'POST',
      dataType: 'json',
      cache: false,
      data: { 'cid': cid },
      success: function (result) {
        if (result.status) {
          $.each(result.data, function (index, value) {
            $('#fkEedCol_' + currId).append($("<option></option>")
              .attr("value", value.pkCol)
              .text(value.col_CollegeName));
          });
        } else {
          toastr.error(result.message);
        }

        showLoader(false);
      },
      error: function (data) {
        toastr.error($('#something_wrong_txt').val());
        showLoader(false);
      }
    });

  }
}

$('#add_eng').on('click', function () {

  $('#profile_eng_details').show();
  $('#profile_detail3').hide();
  $('.datepicker_future').datepicker({ format: "dd/mm/yyyy", autoclose: true, startDate: '+0d', });
  $('#add_eng').hide();

});

$('.rm_eng').on('click', function () {
  if ($(this).attr('data-eed')) {
    var data_eed = $(this).attr('data-eed');
    $('#profile_eng_details_' + data_eed).remove();
    var eng_box = $('#profile_eng_details').find('.rm_eng');
    if (eng_box.length == 0) {
      $('#profile_detail3').show();
      $('#profile_eng_details').hide();
      $('#add_eng').show();
    }
  } else {
    $('#profile_detail3').show();
    $('#profile_eng_details').hide();
    $('#add_eng').show();
  }
});

$('.engage_cancel').on('click', function () {
  $('#profile_detail3').show();
  $('#profile_eng_details').hide();
  $('#add_eng').show();
});

$('#havent_identification_number').click(function () {
  if ($(this).prop("checked") == true) {
    $("#emp_EmployeeID").val('');
    $('.opt_id').addClass('hide_content');
    $('.opt_tmp_id').removeClass('hide_content');
  }
  else if ($(this).prop("checked") == false) {
    $("#emp_TempCitizenId").val('');
    $('.opt_tmp_id').addClass('hide_content');
    $('.opt_id').removeClass('hide_content');
  }
});