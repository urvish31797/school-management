/**
* Sub Admin
*
* This file is used for admin JS
*
* @package    Laravel
* @subpackage JS
* @since      1.0
*/
$(function () {

  $("#upload_profile").on('change', function () {
    if (document.getElementById("upload_profile").files.length == 0) {
      $('#user_img').attr('src', $('#img_tmp').val());
    }
    selectProfileImage(this);
  });

  $('#havent_identification_number').click(function () {
    if ($(this).prop("checked") == true) {
      $("#adm_GovId").val('');
      $('.opt_id').addClass('hide_content');
      $('.opt_tmp_id').removeClass('hide_content');
    }
    else if ($(this).prop("checked") == false) {
      $("#adm_TempGovId").val('');
      $('.opt_tmp_id').addClass('hide_content');
      $('.opt_id').removeClass('hide_content');
    }
  });

  if ($('.datepicker').length) {
    $('.datepicker').datepicker({ format: "dd/mm/yyyy", autoclose: true });
  }

  $("form[name='add-ministry-form']").validate({
    errorClass: "error_msg",
    rules: {
      email: {
        required: true,
        email: true,
        emailfull: true
      },
      fname: {
        required: true,
        minlength: 3
      },
      lname: {
        required: true,
        minlength: 3
      },
      adm_Gender: {
        required: true,
      },
      fkAdmCan: {
        required: true,
      },
      adm_Status: {
        required: true,
      },
      adm_GovId: {
        required: true,
        minlength: 13,
        maxlength: 13
      },
      adm_TempGovId: {
        required: true,
        minlength: 13,
        maxlength: 13
      },
      adm_DOB: {
        required: true,
      },
    },
    submitHandler: function (form, event) {
      event.preventDefault();
      var status = false;
      if ($("#havent_identification_number").prop("checked") == true) {
        status = true;
      }
      else {
        status = checkEmployeeID();
      }
      if (status) {
        showLoader(true);
        var formData = new FormData($(form)[0]);
        var url = base_url + '/admin/subadmin';
        if ($('#aid').length) {
          formData.append("id", $('#aid').val());
        }
        $.ajax({
          url: url,
          type: 'POST',
          processData: false,
          contentType: false,
          cache: false,
          data: formData,
          success: function (result) {
            if (result.status) {
              toastr.success(result.message);
              redirectPage("admin/subadmin");
            } else {
              toastr.error(result.message);
            }
          },
          error: function (data) {
            toastr.error('Something went wrong');
          }
        });
        showLoader(false);
      }
    }
  });

  jQuery.validator.addMethod("emailfull", function (value, element) {
    return this.optional(element) || /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i.test(value);
  }, "Please enter valid email address!");

});

function selectProfileImage(input) {
  if (input.files && input.files[0]) {
    var reader = new FileReader();

    reader.onload = function (e) {
      jQuery('#user_img').attr('src', e.target.result);
    }

    reader.readAsDataURL(input.files[0]);
  }
}

$('#sub_admin_listing').on('processing.dt', function (e, settings, processing) {
  if (processing) {
    showLoader(true);
  } else {
    showLoader(false);
  }
}).DataTable({
  "columnDefs": [{
    "targets": 6,
    "createdCell": function (td, cellData, rowData, row, col) {
      if (cellData == 'Active') {
        $(td).addClass('active_status');
      } else {
        $(td).addClass('disable_status');
      }
    }
  }],
  "language": {
    "sLengthMenu": $('#show_txt').val() + " _MENU_ " + $('#entries_txt').val(),
    "info": $('#showing_txt').val() + " _START_ " + $('#to_txt').val() + " _END_ " + $('#of_txt').val() + " _TOTAL_ " + $('#entries_txt').val(),
    "emptyTable": $('#msg_no_data_available_table').val(),
    "paginate": {
      "previous": $('#previous_txt').val(),
      "next": $('#next_txt').val()
    }
  },
  "lengthMenu": [10, 20, 30, 50],
  "searching": false,
  "serverSide": true,
  "deferRender": true,
  "ajax": {
    "url": listing_url,
    "type": "POST",
    "dataType": 'json',
    "data": function (d) {
      d.search = $('#search_sub_admin').val();
    }
  },
  columns: [
    { "data": "index", className: "text-center" },
    { "data": "adm_Uid" },
    { "data": "adm_Name" },
    {
      "data": "adm_GovId",
      render: function (data, type, admin) {
        if (admin.adm_GovId != null) {
          return admin.adm_GovId;
        }
        else {
          return admin.adm_TempGovId;
        }
      }
    },
    { "data": "email" },
    {
      "data": "adm_Statu", className: "status", sortable: !1,
      render: function (data, type, admin) {
        return '<span></span>' + admin.adm_Status + '';
      }
    },
    {
      "data": "adm_Status", className: "status", sortable: !1,
      render: function (data, type, admin) {
        return '<button class="btn btn-success" data_id="' + admin.id + '">' + $('#access_txt').val() + '</button>';
      }
    },
    {
      "data": "adm_Name", sortable: !1,
      render: function (data, type, admin) {
        return '<a class="ajax_request no_sidebar_active" href="subadmin/' + admin.id + '"><img src="' + imagepath + 'ic_eye_color.png"></a>\t\t\t\t\t\t<a class="ajax_request no_sidebar_active" href="subadmin/' + admin.id + '/edit"><img src="' + imagepath + 'ic_mode_edit.png"></a>\t\t\t\t\t\t<a onclick="triggerDelete(' + admin.id + ')" href="javascript:void(0)"><img src="' + imagepath + 'ic_delete.png"></a>'
      }
    },
  ],
});

$('#delete_prompt').on('hidden.bs.modal', function () {
  $("#did").val('');
})

$("#search_sub_admin").on('keyup', function () {
  $('#sub_admin_listing').DataTable().ajax.reload();
});

function triggerDelete(cid) {
  $('#did').val(cid);
  $(".show_delete_modal").click();
}

function confirmDelete() {
  showLoader(true);
  var cid = $('#did').val();
  $.ajax({
    url: base_url + '/admin/subadmin/' + cid,
    type: 'DELETE',
    dataType: 'json',
    cache: false,
    success: function (result) {
      if (result.status) {
        $('#delete_prompt').modal('hide');
        $('#sub_admin_listing').DataTable().ajax.reload();
      } else {
        toastr.error('Something went wrong');
      }

      showLoader(false);
    },
    error: function (data) {
      toastr.error('Something went wrong');
      showLoader(false);
    }
  });
}

function checkEmployeeID() {
  var birthDate = $("#adm_DOB").val();
  var gender = $("#adm_Gender").val();
  var employeeId = $('.isValidEmpID').val();
  var status = false;

  if (gender != "" && birthDate != "" && employeeId != "" && employeeId.length == 13) {
    var dd = employeeId.substring(0, 2);
    var mm = employeeId.substring(2, 4);
    var yy = employeeId.substring(4, 7);
    var rr = employeeId.substring(7, 9);
    var bb = employeeId.substring(9, 12);

    var bdata = birthDate.split("/");
    var d = bdata[0];
    var m = bdata[1];
    var y = bdata[2].substring(1, 4);

    if (dd == d && mm == m && yy == y && rr <= 19 && rr >= 10) {
      if (gender == "Male" && bb <= 499 && bb >= 0) {
        status = true;
      }
      else {
        if (gender == "Female" && bb <= 999 && bb >= 500) {
          status = true;
        }
      }
    }
  }

  if (status == false && employeeId != "" && gender != "" && birthDate != "") {
    $('.isValidEmpID').val("");
    toastr.error($("#employeemsgerror").val());
  }

  return status;
}