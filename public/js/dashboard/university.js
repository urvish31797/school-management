/**
* University
*
* This file is used for admin JS
*
* @package    Laravel
* @subpackage JS
* @since      1.0
*/
var page_url = base_url + '/admin/universities';
var form = $("form[name='add-university-form']");

$(function () {

  $(".datepicker-year").datepicker({
    format: "yyyy",
    viewMode: "years",
    autoclose: true,
    endDate: '+0d',
    minViewMode: "years"
  });

  $("#upload_profile").on('change', function () {
    if (document.getElementById("upload_profile").files.length == 0) {
      $('#university_img').attr('src', $('#img_tmp').val());
    }
    selectProfileImage(this);
  });

  $("#search_university").on('keyup', function () {
    $('.universities_listing').DataTable().ajax.reload()
  });

  $("#country_filter").on('change', function () {
    $('.universities_listing').DataTable().ajax.reload()
  });

  $("#year_filter").on('change', function () {
    $('.universities_listing').DataTable().ajax.reload()
  });

  $("#ownership_filter").on('change', function () {
    $('.universities_listing').DataTable().ajax.reload()
  });

  $('#add_new').on('hidden.bs.modal', function () {
    var validator = form.validate();
    validator.resetForm();
    $('#university_img').attr('src', $('#img_tmp').val());
    $("form").trigger("reset");
    $("#pkUni").val('');
  })

  $('#add_new').on('shown.bs.modal', function () {
    $("form").data('validator').resetForm();
  })

  $('#delete_prompt').on('hidden.bs.modal', function () {
    $("#did").val('');
  });
});

function save() {
  if (form.valid()) {
    showLoader(true);
    var formData = new FormData($(form)[0]);
    formData.append("pkUni", $("#pkUni").val());
    $.ajax({
      url: page_url,
      type: 'POST',
      processData: false,
      contentType: false,
      cache: false,
      data: formData,
      success: function (result) {
        toastr.success(result.message);
        $('#add_new').modal('hide');
        $('.universities_listing').DataTable().ajax.reload();
      },
      error: function (jqXHR) {
        if (jqXHR.status == 400) {
          toastr.error(jqXHR.responseJSON.message);
        }
        else if (jqXHR.status == 422) {
          setvalidationmessages(jqXHR.responseJSON.errors);
        }
        else {
          toastr.error($('#something_wrong_txt').val());
        }
      }
    });
    showLoader(false);
  }
}

function confirmDelete() {
  showLoader(true);
  var cid = $('#did').val();
  $.ajax({
    url: page_url + '/' + cid,
    type: 'DELETE',
    dataType: 'json',
    cache: false,
    success: function (result) {
      $('#delete_prompt').modal('hide');
      toastr.success(result.message);
      $('.universities_listing').DataTable().ajax.reload();
    },
    error: function (jqXHR) {
      if (jqXHR.status == 400) {
        toastr.error(jqXHR.responseJSON.message);
      }
      else {
        toastr.error($('#something_wrong_txt').val());
      }
    }
  });
  showLoader(false);
}

function triggerEdit(cid) {
  showLoader(true);
  $.ajax({
    url: page_url + '/' + cid + '/edit',
    type: 'GET',
    dataType: 'json',
    cache: false,
    success: function (result) {
      $.each(result.data, function (index, value) {
        $("#" + index).val(value);
      });
      $("#pkUni").val(result.data.pkUni);
      if (result.data.uni_PicturePath != null && result.data.uni_PicturePath != '') {
        $('#university_img').attr('src', result.data.uni_PicturePath);
      }
      $(".show_modal").click();
    },
    error: function (jqXHR) {
      if (jqXHR.status == 400) {
        toastr.error(jqXHR.responseJSON.message);
      }
      else {
        toastr.error($('#something_wrong_txt').val());
      }
    }
  });
  showLoader(false);
}

function triggerDelete(cid) {
  $('#did').val(cid);
  $(".show_delete_modal").click();
}

function selectProfileImage(input) {
  if (input.files && input.files[0]) {
    var reader = new FileReader();

    reader.onload = function (e) {
      jQuery('#university_img').attr('src', e.target.result);
    }

    reader.readAsDataURL(input.files[0]);
  }
}
