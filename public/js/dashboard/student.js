/**
* Students
*
* This file is used for admin JS
*
* @package    Laravel
* @subpackage JS
* @since      1.0
*/

var $type = '';
var editStu = '';
var viewStu = '';
var enrol_url = '';
var enrol_fetch_url = base_url + '/employee/fetch-grades';
var checkStuIdUrl = '';
if ($('#is_HSA').length != 0) {
  $type = 'admin';
  editStu = base_url + '/admin/students';
  viewStu = base_url + '/admin/students';
  enrol_url = base_url + "/admin/studentenroll-update";
  checkStuIdUrl = base_url + '/admin/check-student-id';
} else {
  $type = 'employee';
  editStu = base_url + '/employee/students';
  viewStu = base_url + '/employee/students';
  enrol_url = base_url + "/employee/studentenroll-update";
  checkStuIdUrl = base_url + '/employee/check-student-id';
}

var oldEng = [];

$(function () {

  $('#add_eng').on('click', function () {
    var $ed = $('#profile_eng_details .profile_info_container').clone();
    var ind = $('#edit_profile_detail3 .profile_info_container').length + 1;
    $ed.find('.rm_eng').attr('data-eed', ind);
    $ed.find('select,input,textarea,button').each(function (key, value) {
      var attr = $(this).attr('data-id');
      if (typeof attr !== typeof undefined && attr !== false) {
        $(this).attr('data-id', ind);
      }
      // if ($(this).attr('class') != 'pkSte') {
      this.id = this.id + '_' + ind;
      this.name = this.name + '_' + ind;
      // }
    });

    $('#edit_profile_detail3 .profile_eng_details_add .text-center:first').before($ed);
    $('.datepicker_norm').datepicker({ format: "dd/mm/yyyy", autoclose: true, startDate: '+0d', });
    $('.datepicker').datepicker({ format: "dd/mm/yyyy", autoclose: true });

    if ($('#edit_profile_detail3 .new_emp_eng').length != 0) {
      $('.add_eng_btn').hide();
    }

  });

  $(document).on('click', '.rm_eng', function () {
    oldEng.push($(this).attr('data-eng'));

    $(this).closest('.profile_info_container').remove();
    // var ind = $('#edit_profile_detail2 .profile_info_container').length
    var inc = 1;
    $('#edit_profile_detail3 .profile_info_container').each(function (i, obj) {
      $(this).find('select,input,textarea,button').each(function (key, value) {
        var old_id = this.id.split("_");
        old_id.splice(-1, 1);
        old_id.push(inc);
        var new_id = old_id.join('_');

        var attr = $(this).attr('data-id');
        if (typeof attr !== typeof undefined && attr !== false) {
          var aid = new_id.replace("_", "");
          $(this).attr('data-id', aid);
        }

        this.id = new_id;
        this.name = new_id;
      });
      inc++;
    });

    if ($('#edit_profile_detail3 .new_emp_eng').length == 0) {
      $('.add_eng_btn').show();
    }
  });

  $("#fkStuMun").on('change', function () {
    var id = $(this).children('option:selected').attr('data-countryId');
    $("#fkStuCny").val(id);
  });

  $("#upload_profile").on('change', function () {
    if (document.getElementById("upload_profile").files.length == 0) {
      $('#user_img').attr('src', $('#img_tmp').val());
    }
    selectProfileImage(this);
  });


  if ($('.datepicker').length) {
    $('.datepicker').datepicker({ format: "dd/mm/yyyy", autoclose: true });
  }

  $('.datepicker_norm').datepicker({ format: "dd/mm/yyyy", autoclose: true, endDate: '+0d' });

  $("form[name='enroll-student-form']").validate({
    errorClass: "error_msg",
    submitHandler: function (form, event) {
      event.preventDefault();
      showLoader(true);
      // var array = $('.pkSte').map(function () {
      //   return $(this).val()
      // }).get();

      var formData = new FormData($(form)[0]);
      formData.append("oldEng", oldEng);
      // formData.append("pkSte", array);
      formData.append("total_details", $('.profile_eng_details_add .profile_info_container').length);
      formData.append("fkSteStu", $('#fkSteStu').val());
      $.ajax({
        url: enrol_url,
        type: 'POST',
        processData: false,
        contentType: false,
        cache: false,
        data: formData,
        success: function (result) {
          if (result.status) {
            toastr.success(result.message);
            if ($('#is_HSA').length != 0) {
              redirectPage("admin/students");
            } else {
              redirectPage("employee/students");
            }

          } else {
            toastr.error(result.message);
          }

          showLoader(false);
        },
        error: function (data) {
          toastr.error($('#something_wrong_txt').val());
          showLoader(false);
        }
      });
    }
  });

  $("form[name='add-student-form']").validate({
    errorClass: "error_msg",
    rules: {
      stu_StudentName: {
        required: true,
        minlength: 3
      },
      stu_StudentSurname: {
        required: true,
        minlength: 1
      },
      stu_DateOfBirth: {
        required: true
      },
      stu_StudentGender: {
        required: true
      },
      stu_StudentID: {
        required: true,
        minlength: 13,
        maxlength: 13
      },
      stu_TempCitizenId: {
        required: true,
        minlength: 13
      },
      stu_PlaceOfBirth: {
        required: true,
        minlength: 3
      },
      fkStuMun: {
        required: true
      },
      fkStuNat: {
        required: true
      },
      fkStuRel: {
        required: true
      },
      fkStuPof: {
        required: true
      },
      stu_MotherName: {
        required: true
      }
    },
    submitHandler: function (form, event) {
      event.preventDefault();

      var status = false;
      if ($("#havent_identification_number").prop("checked") == true) {
        status = true;
      }
      else {
        status = checkStudentID();
      }
      if (status) {
        showLoader(true);
        var formData = new FormData($(form)[0]);
        var url = '';
        if ($('#aid').length) {
          url = editStu;
          formData.append("id", $('#aid').val());
        } else {
          url = base_url + '/employee/students';
        }
        $.ajax({
          url: url,
          type: 'POST',
          processData: false,
          contentType: false,
          cache: false,
          data: formData,
          success: function (result) {
            if (result.status) {
              toastr.success(result.message);
              setTimeout(() => {
                window.location.href = result.redirect;
              }, 1200);

            } else {
              toastr.error(result.message);
            }

            showLoader(false);
          },
          error: function (data) {
            toastr.error($('#something_wrong_txt').val());
            showLoader(false);
          }
        });
      }
    }
  });

});

function fetchEducationPlan(elem) {
  var edp_value = $(elem).val();
  var tmp = $(elem).attr('id').split("_");
  $('#fkSteEpl_' + tmp[1]).find('option').not(':first').remove();
  $('#fkSteGra_' + tmp[1]).find('option').not(':first').remove();
  if (edp_value) {
    showLoader(true);
    $.ajax({
      url: fetch_edu_url,
      type: 'POST',
      data: { 'fkSteEdp': edp_value },
      success: function (result) {
        if (result.status == true) {
          $(result.educationPlans).each(function (index, element) {
            $('#fkSteEpl_' + tmp[1]).append(`<option value="${element.pkEpl}">${element.epl_EducationPlanName}</option>`);
          });
        } else {
          toastr.error($('#something_wrong_txt').val());
          showLoader(false);
        }
        showLoader(false);
      },
      error: function (data) {
        toastr.error($('#something_wrong_txt').val());
        showLoader(false);
      }
    });
  } else {
    $('#fkSteEpl').html(`<option value="" selected>Select</option>`);
    $('#fkSteGra').html(`<option value="" selected>Select</option>`);
  }
}

function fetchGrades(elem) {

  var tmp = $(elem).attr('id').split("_");
  $('#fkSteGra_' + tmp[1]).find('option').not(':first').remove();
  var epl_value = $(elem).val();
  if (epl_value) {
    showLoader(true);
    $.ajax({
      url: enrol_fetch_url,
      type: 'POST',
      data: { 'fkSteEpl': epl_value },
      success: function (result) {
        if (result.status == true) {

          //$('#fkSteGra').html(`<option value="${result.grades.pkGra}">${result.grades.gra_GradeName}</option>`);
          $.each(result.grades, function (key, value) {
            $('#fkSteGra_' + tmp[1]).append($("<option></option>")
              .attr("value", value.pkGra)
              .text(value.gra_GradeNumeric));
          });

        } else {
          toastr.error($('#something_wrong_txt').val());
          showLoader(false);
        }
        showLoader(false);
      },
      error: function (data) {
        toastr.error($('#something_wrong_txt').val());
        showLoader(false);
      }
    });
  } else {
    $('#fkSteGra').html(`<option value="" selected>Select</option>`);
  }
}


function selectProfileImage(input) {
  if (input.files && input.files[0]) {
    var reader = new FileReader();
    var filename = input.files[0].name;
    var fileExtension = filename.substr((filename.lastIndexOf('.') + 1));
    var fileExtensionCase = fileExtension.toLowerCase();
    if (fileExtensionCase == 'png' || fileExtensionCase == 'jpeg' || fileExtensionCase == 'jpg') {
      reader.onload = function (e) {
        jQuery('#user_img').attr('src', e.target.result);
      }
      reader.readAsDataURL(input.files[0]);
    } else {
      toastr.error($('#image_validation_msg').val());
      $('#upload_profile').val('');
      var user_img = imagepath + "user.png";

      $('#user_img').attr('src', user_img);

    }
  }
}

function checkStudentID() {
  var birthDate = $("#stu_DateOfBirth").val();
  var gender = $("#stu_StudentGender").val();
  var stuId = $('.isValidStuID').val();
  var status = false;

  if (gender != "" && birthDate != "" && stuId != "" && stuId.length == 13) {
    var dd = stuId.substring(0, 2);
    var mm = stuId.substring(2, 4);
    var yy = stuId.substring(4, 7);
    var rr = stuId.substring(7, 9);
    var bb = stuId.substring(9, 12);

    var bdata = birthDate.split("/");
    var d = bdata[0];
    var m = bdata[1];
    var y = bdata[2].substring(1, 4);

    if (dd == d && mm == m && yy == y && rr <= 19 && rr >= 10) {
      if (gender == "Male" && bb <= 499 && bb >= 0) {
        status = true;
      }
      else {
        if (gender == "Female" && bb <= 999 && bb >= 500) {
          status = true;
        }
      }

      if (status) {
        checkStuId(stuId);
      }
    }
  }

  if (status == false && stuId != "" && gender != "" && birthDate != "") {
    $('.isValidStuID').val("");
    toastr.error($("#studentmsgerror").val());
  }

  return status;
}

function checkStuId(val) {
  showLoader(true);
  var edit = '';
  if ($('#aid').length) {
    edit = $('#aid').val();
  }
  $.ajax({
    url: checkStuIdUrl,
    type: 'POST',
    dataType: 'json',
    cache: false,
    data: { 'stuId': val, id: edit },
    success: function (result) {
      if (!result.status) {
        $(".isValidStuID").val("");
        toastr.error(result.message);
      }

      showLoader(false);
    },
    error: function (data) {
      toastr.error($('#something_wrong_txt').val());
      showLoader(false);
    }
  });
}

$('#student_listing').on('processing.dt', function (e, settings, processing) {
  if (processing) {
    showLoader(true);
  } else {
    showLoader(false);
  }
}).DataTable({
  "columnDefs": [{
    "targets": 6,
    "createdCell": function (td, cellData, rowData, row, col) {
      if (cellData == 'Active') {
        $(td).addClass('active_status');
      } else {
        $(td).addClass('disable_status');
      }
    }
  }],
  "language": {
    "sLengthMenu": $('#show_txt').val() + " _MENU_ " + $('#entries_txt').val(),
    "info": $('#showing_txt').val() + " _START_ " + $('#to_txt').val() + " _END_ " + $('#of_txt').val() + " _TOTAL_ " + $('#entries_txt').val(),
    "emptyTable": $('#msg_no_data_available_table').val(),
    "paginate": {
      "previous": $('#previous_txt').val(),
      "next": $('#next_txt').val()
    }
  },
  "lengthMenu": [10, 20, 30, 50],
  "searching": false,
  "serverSide": true,
  "deferRender": true,
  "ajax": {
    "url": listing_url,
    "type": "POST",
    "dataType": 'json',
    "data": function (d) {
      d.search = $('#search_student').val();
      // d.state = $('#state_filter').val();
      // d.country = $('#country_filter').val();
    }
  },
  columns: [
    { "data": "index", className: "text-center" },
    {
      "data": "stu_StudentID",
      render: function (data, type, row) {
        if (row.stu_StudentID == '' || row.stu_StudentID == null) {
          return row.stu_TempCitizenId;
        } else {
          return row.stu_StudentID;
        }
      }
    },
    {
      "data": "stu_StudentName",
      render: function (data, type, row) {
        var name = row.stu_StudentName + ' ' + row.stu_StudentSurname
        return name;
      }
    },
    {
      "data": "stu_PlaceOfBirth",
      render: function (data, type, row) {
        var name = row.stu_PlaceOfBirth;
        return name;
      }
    },
    { "data": "stu_StudentGender" },
    { "data": "stu_Address" },
    {
      "data": "stu_StudentName", sortable: !1,
      render: function (data, type, row) {
        var delet = '';
        var edit = '';
        var view = '';
        if (window.location == base_url + '/employee/students') {
          view = '<a class="no_sidebar_active" href="students/' + row.id + '"><img src="' + imagepath + 'ic_eye_color.png"></a>\t\t\t\t\t\t'
          edit = '<a class="no_sidebar_active" href="students/' + row.id + '/edit"><img src="' + imagepath + 'ic_mode_edit.png"></a>\t\t\t\t\t\t'
        } else {
          delet = '<a onclick="triggerDelete(' + row.id + ')" href="javascript:void(0)"><img src="' + imagepath + 'ic_delete.png"></a>'
          view = '<a class="no_sidebar_active" href="students/' + row.id + '"><img src="' + imagepath + 'ic_eye_color.png"></a>\t\t\t\t\t\t'
          edit = '<a class="no_sidebar_active" href="students/' + row.id + '/edit"><img src="' + imagepath + 'ic_mode_edit.png"></a>\t\t\t\t\t\t'

        }
        return view + edit + delet;
      }
    },
  ],

});

$('#delete_prompt').on('hidden.bs.modal', function () {
  $("#did").val('');
})

$("#search_student").on('keyup', function () {
  //if($(this).val() != ''){
  $('#student_listing').DataTable().ajax.reload()
  // }
});

function triggerDelete(cid) {
  $('#did').val(cid);
  $(".show_delete_modal").click();
}

function confirmDelete() {
  showLoader(true);
  var cid = $('#did').val();
  $.ajax({
    url: base_url + '/admin/students/' + cid,
    type: 'DELETE',
    dataType: 'json',
    cache: false,
    success: function (result) {
      if (result.status) {
        $('#delete_prompt').modal('hide');
        $('#student_listing').DataTable().ajax.reload();
      } else {
        toastr.error($('#something_wrong_txt').val());
      }

      showLoader(false);
    },
    error: function (data) {
      toastr.error($('#something_wrong_txt').val());
      showLoader(false);
    }
  });
}

function viewEduPlan(elem) {

  var selectedPlan = $('#fkSteEpl_' + $(elem).attr('data-id')).val();
  if (selectedPlan) {
    window.open($('#web_base_url').val() + '/employee/view/educationplan/' + selectedPlan);
  } else {
    toastr.error($('#msg_select_education_plan').val());
  }
}

$('#havent_identification_number').click(function () {
  if ($(this).prop("checked") == true) {
    $('#stu_TempCitizenId').show();
    $('input[name=stu_StudentID]').val('');
    $('input[name=stu_StudentID]').prop('disabled', true);
    $('input[name=stu_TempCitizenId]').prop('disabled', false);
    $('.opt_tmp_id').removeClass('hide_content');
  }
  else if ($(this).prop("checked") == false) {
    $('#stu_StudentID').show();
    $('input[name=stu_TempCitizenId]').val('');
    $('input[name=stu_TempCitizenId]').prop('disabled', true);
    $('input[name=stu_StudentID]').prop('disabled', false);
    $('.opt_tmp_id').addClass('hide_content');
  }
});