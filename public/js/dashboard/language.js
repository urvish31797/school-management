var form = $("form[name='add-language-form']");
var page_url = base_url + '/admin/languages';

$(function () {

  $("#upload_flag").on('change', function () {
    if (document.getElementById("upload_flag").files.length == 0) {
      $('#flag_img').attr('src', $('#img_tmp').val());
    }
    selectFlagImage(this);
  });

  $("#search_language").on('keyup', function () {
    $('.languages_listing').DataTable().ajax.reload();
  });

  $('#add_new').on('hidden.bs.modal', function () {
    $("form").trigger("reset");
    $("#id").val('');
    $('#flag_img').attr('src', $('#img_tmp').val());
  })

  $('#add_new').on('shown.bs.modal', function () {
    $("form").data('validator').resetForm();
  })

  $('#delete_prompt').on('hidden.bs.modal', function () {
    $("#did").val('');
  });
});

function save()
{
  if(form.valid())
  {
    showLoader(true);
    var formData = new FormData($(form)[0]);
    formData.append("id", $("#id").val());
    $.ajax({
      url: page_url,
      type: 'POST',
      processData: false,
      contentType: false,
      cache: false,
      data: formData,
      success: function (result) {
        toastr.success(result.message);
        $('#add_new').modal('hide');
        $('.languages_listing').DataTable().ajax.reload();
      },
      error: function (jqXHR) {
        if (jqXHR.status == 400) {
          toastr.error(jqXHR.responseJSON.message);
        }
        else if (jqXHR.status == 422) {
          setvalidationmessages(jqXHR.responseJSON.errors);
        }
        else {
          toastr.error($('#something_wrong_txt').val());
        }
      }
    });
    showLoader(false);
  }
}

function selectFlagImage(input) {
  if (input.files && input.files[0]) {
    var reader = new FileReader();
    reader.onload = function (e) {
      jQuery('#flag_img').attr('src', e.target.result);
    }
    reader.readAsDataURL(input.files[0]);
  }
}

function confirmDelete() {
  showLoader(true);
  var cid = $('#did').val();
  $.ajax({
    url: page_url + '/' + cid,
    type: 'DELETE',
    dataType: 'json',
    cache: false,
    success: function (result) {
        toastr.success(result.message);
        $('#delete_prompt').modal('hide');
        $('.languages_listing').DataTable().ajax.reload();
    },
    error: function (jqXHR) {
      if (jqXHR.status == 400) {
        toastr.error(jqXHR.responseJSON.message);
      }
      else {
        toastr.error($('#something_wrong_txt').val());
      }
    }
  });
  showLoader(false);
}

function triggerEdit(cid) {
  showLoader(true);
  $.ajax({
    url: page_url + '/' + cid + '/edit',
    type: 'GET',
    dataType: 'json',
    cache: false,
    success: function (result) {
      
        $("#id").val(result.data.id);
        $("#language_key").val(result.data.language_key);
        $("#language_name").val(result.data.language_name);
        if (result.data.flag != null && result.data.flag != '') {
          $('#flag_img').attr('src', result.data.flag);
        }
        $(".show_modal").click();
      
    },
    error: function (jqXHR) {
      if (jqXHR.status == 400) {
        toastr.error(jqXHR.responseJSON.message);
      }
      else {
        toastr.error($('#something_wrong_txt').val());
      }
    }
  });
  showLoader(false);
}

function triggerDelete(cid) {
  $('#did').val(cid);
  $(".show_delete_modal").click();
}
