$(function () {

  $('.select2_multi').select2({
    "language": {
      "noResults": function () {
        return $('#no_result_text').val();
      }
    },
    placeholder: $('#select_txt').val(),
    allowClear: true
  });

  $('.select2_drop').select2({
    "language": {
      "noResults": function () {
        return $('#no_result_text').val();
      }
    },
  });

  showLoader(false);

  $('#student_listing').on('processing.dt', function (e, settings, processing) {
    if (processing) {
      showLoader(true);
    } else {
      showLoader(false);
    }
  }).DataTable({
    "columnDefs": [{
      "targets": 4,
      "createdCell": function (td, cellData, rowData, row, col) {
        if (cellData == 'Active') {
          $(td).addClass('active_status');
        } else {
          $(td).addClass('disable_status');
        }
      }
    }],
    "language": {
      "sLengthMenu": $('#show_txt').val() + " _MENU_ " + $('#entries_txt').val(),
      "info": $('#showing_txt').val() + " _START_ " + $('#to_txt').val() + " _END_ " + $('#of_txt').val() + " _TOTAL_ " + $('#entries_txt').val(),
      "emptyTable": $('#msg_no_data_available_table').val(),
      "paginate": {
        "previous": $('#previous_txt').val(),
        "next": $('#next_txt').val()
      }
    },
    "lengthMenu": [10, 20, 30, 50],
    "searching": false,
    "serverSide": true,
    "deferRender": true,
    "ajax": {
      "url": base_url + "/employee/coursegroup-students-list",
      "type": "POST",
      "dataType": 'json',
      "data": function (d) {
        d.search = $('#search_student').val();
        d.schoolYear = $('#fkCgaSye').val();
        d.grades = $('#fkClrGra').val();
        d.classes = $('#fkClrCla').val();
        d.currentEdp = $("#fkCgaEdu").val();
        d.villageschool = $("#fkCgaViSch").val();
      }
    },
    columns: [
      { "data": "index", className: "text-center" },
      {
        "data": "index", sortable: !1,
        render: function (data, type, mb) {
          if (mb.student.stu_StudentID == '' || mb.student.stu_StudentID == null) {
            return mb.student.stu_TempCitizenId;
          } else {
            return mb.student.stu_StudentID;
          }
        }
      },
      {
        "data": "index", sortable: !1,
        render: function (data, type, mb) {
          return mb.student.stu_StudentName + ' ' + mb.student.stu_StudentSurname;
        }
      },
      {
        "data": "index", sortable: !1,
        render: function (data, type, mb) {
          return mb.grade.gra_GradeNumeric;
        }
      },
      {
        "data": "index", sortable: !1,
        render: function (data, type, mb) {
          if (mb.class_students[0].class_creation_semester != null) {
            if (mb.class_students[0].class_creation_semester.class_creation != null)
              return mb.class_students[0].class_creation_semester.class_creation.class_creation_classes.cla_ClassName;
            else
              return '';
          } else {
            return '';
          }
        }
      },
      {
        "data": "index", sortable: !1,
        render: function (data, type, mb) {
          return mb.education_program.edp_Name + ' - ' + mb.education_plan.epl_EducationPlanName;
        }
      },
      {
        "data": "cla_ClassName", sortable: !1,
        render: function (data, type, mb) {
          var suid = null;
          if (mb.student.stu_StudentID == '' || mb.student.stu_StudentID == null) {
            suid = mb.student.stu_TempCitizenId;
          } else {
            suid = mb.student.stu_StudentID;
          }
          return '<div class="form-group form-check"><input csid="' + mb.class_students[0].class_creation_semester.pkCcs + '" cid="' + mb.pkSte + '" suid="' + suid + '" sem_Id="' + mb.class_students[0].pkSem + '" clr_Id="' + mb.class_students[0].fkSemClr + '" sname="' + mb.student.stu_StudentName + ' ' + mb.student.stu_StudentSurname + '" gra_name="' + mb.grade.gra_GradeNumeric + '" gra_id="' + mb.grade.pkGra + '" ep_id="' + mb.education_plan.pkEpl + '" ep_name="' + mb.education_program.edp_Name + ' - ' + mb.education_plan.epl_EducationPlanName + '" type="checkbox" class="form-check-input add_all_stu" name="all_stu_sel[]" value="' + mb.pkSte + '"><label class="custom_checkbox"></label></div>';
        }
      },
    ],

  });

  $("#search_student").on('keyup', function () {
    $('#student_listing').DataTable().ajax.reload()
  });

  $("#fkClrGra").on('change', function () {
    $(".checkAll").prop("checked", false);
    $('#student_listing').DataTable().ajax.reload()
  });

  $("#fkClrCla").on('change', function () {
    $(".checkAll").prop("checked", false);
    $('#student_listing').DataTable().ajax.reload()
  });

  $("#fkCgaViSch").on('change', function () {
    $(".checkAll").prop("checked", false);
    $('#student_listing').DataTable().ajax.reload()
  });

  $('#add_new').on('hidden.bs.modal', function () {
    $("form").trigger("reset");
    $("#pkCla").val('');
  })

  $('#delete_prompt').on('hidden.bs.modal', function () {
    $("#did").val('');
  })

  $('#add_new').on('shown.bs.modal', function () {
    $("form").data('validator').resetForm();
  })

  $("form[name='course_group_form']").validate({
    errorClass: "error_msg",
    rules: {
      course_order: {
        required: true,
      },
      course_group_sub: {
        required: true,
      },
      cg_hour: {
        required: true,
      },
      subject: {
        required: true,
      },
      course_group_main: {
        required: true,
      }
    },
    errorPlacement: function (error, element) {
      if ((element.hasClass('select2_multi') || element.hasClass('select2_drop')) && element.next('.select2-container').length) {
        error.insertAfter(element.next('.select2-container'));
      } else {
        error.insertAfter(element);
      }
    },
    submitHandler: function (form, event) {
      event.preventDefault();

      if (!$("#pkCga").length && !$('.stu').length) {
        toastr.error($('#student_sel_valid_txt').val());
        return;
      }

      showLoader(true);
      $('#fkCgaSye').prop('disabled', false);
      $('#fkCgaEdu').prop('disabled', false);
      var formData = new FormData($(form)[0]);
      if ($("#pkCga").length) {
        formData.append("pkCga", $("#pkCga").val());
      }

      $.ajax({
        url: base_url + '/employee/coursegroup',
        type: 'POST',
        processData: false,
        contentType: false,
        cache: false,
        data: formData,
        success: function (result) {
          if (result.status) {
            toastr.success(result.message);
            redirectPage('employee/coursegroup');
          } else {
            toastr.error(result.message);
          }

          showLoader(false);
        },
        error: function (data) {
          toastr.error($('#something_wrong_txt').val());
          showLoader(false);
        }
      });
      $('#fkCgaSye').prop('disabled', true);
    }
  });

  //Course Groups Listing
  $('#course_group_listing').on('processing.dt', function (e, settings, processing) {
    if (processing) {
      showLoader(true);
    } else {
      showLoader(false);
    }
  }).DataTable({
    "columnDefs": [{


    }],
    "language": {
      "sLengthMenu": $('#show_txt').val() + " _MENU_ " + $('#entries_txt').val(),
      "info": $('#showing_txt').val() + " _START_ " + $('#to_txt').val() + " _END_ " + $('#of_txt').val() + " _TOTAL_ " + $('#entries_txt').val(),
      "emptyTable": $('#msg_no_data_available_table').val(),
      "paginate": {
        "previous": $('#previous_txt').val(),
        "next": $('#next_txt').val()
      }
    },
    "lengthMenu": [10, 20, 30, 50],
    "searching": false,
    "serverSide": true,
    "deferRender": true,
    "ajax": {
      "url": listing_url,
      "type": "POST",
      "dataType": 'json',
      "data": function (d) {
        d.search = $('#search_course_group').val();
        d.grade = $('#searchGrade').val();
        d.schoolYear = $('#search_sch_year').val();
        d.semester = $("#current_semester").val();
      }
    },
    columns: [
      { "data": "index", className: "text-center" },
      { "data": "cga_Uid", sortable: !1 },
      { "data": "course_group", sortable: !1 },
      {
        "data": "", sortable: !1,
        render: function (data, type, mb) {
          if ('course' in mb) {
            return mb.course;
          } else {
            return '-';
          }
        }
      },
      {
        "data": "sye_NameCharacter", sortable: !1,
        render: function (data, type, mb) {
          return mb.course_group_school_year.sye_NameCharacter;
        }
      },
      { "data": "cga_WeekHours", sortable: !1 },
      {
        "data": "cga_Uid", sortable: !1,
        render: function (data, type, mb) {
          if ('teachers' in mb) {
            return mb.teachers;
          } else {
            return '-';
          }
        }
      },
      { "data": "total_stu", sortable: !1 },
      {
        "data": "pkCga", sortable: !1,
        render: function (data, type, mb) {
          var cc = '\t\t\t\t\t\t';
          return cc + '<a class="no_sidebar_active" href="coursegroup/' + mb.pkCga + '"><img src="' + imagepath + 'ic_eye_color.png"></a>\t\t\t\t\t\t<a class="ajax_request no_sidebar_active" href="coursegroup/' + mb.pkCga + '/edit"><img src="' + imagepath + 'ic_mode_edit.png"></a>\t\t\t\t\t\t<a onclick="triggerDeleteClsCre(' + mb.pkCga + ')" href="javascript:void(0)"><img src="' + imagepath + 'ic_delete.png"></a>';
        }
      },
    ],

  });

  $("#current_semester").on('change', function () {
    $('#course_group_listing').DataTable().ajax.reload()
  });

  $("#search_course_group").on('keyup', function () {
    $('#course_group_listing').DataTable().ajax.reload()
  });

  $("#searchGrade").on('change', function () {
    $('#course_group_listing').DataTable().ajax.reload()
  });

  $("#search_sch_year").on('change', function () {
    $('#course_group_listing').DataTable().ajax.reload()
  });

});

function fetchCourseGroup(val) {
  $('#course_group_sub').find('option').not(':first').remove();
  $('#subject').find('option').not(':first').remove();
  $('.stu').remove();
  $(".sel_stu_elem").addClass('hide_content');

  if (val == 'fcg' || val == 'gpg') {
    $('.fcg_hour').removeClass('hide_content');
  } else {
    $('.fcg_hour').addClass('hide_content');
    $('#cg_hour').val('');
  }

  showLoader(true);
  $.ajax({
    url: base_url + '/employee/fetch-coursegroup',
    type: 'POST',
    dataType: 'json',
    cache: false,
    data: { 'ctype': val },
    success: function (result) {

      if (result.status) {
        $.each(result.data.courseGroup, function (key, value) {
          $('#course_group_sub').append($("<option></option>")
            .attr("value", value.id)
            .text(value.group_Name));
        });

        $.each(result.data.courses, function (key, value) {
          $('#subject').append($("<option></option>")
            .attr("value", value.pkCrs)
            .text(value.crs_CourseName));
        });
      }

      showLoader(false);
    },
    error: function (data) {
      toastr.error($('#something_wrong_txt').val());
      showLoader(false);
    }
  });
};

function addAllStu() {
  if (!$(".add_all_stu:checked").length) {
    toastr.error('Please select a student');
  } else {
    $.each($(".add_all_stu:checked"), function () {
      addStudent(this);
    });
  }
}

function checkAllStu(elem) {
  if ($(elem).prop("checked") == true) {
    $(".add_all_stu").prop("checked", true);
  } else if ($(elem).prop("checked") == false) {
    $(".add_all_stu").prop("checked", false);
  }
}

function addStudent(elem) {

  if ($('.stu_' + $(elem).attr('cid')).length != 0) {
    toastr.error($('#stu_sel_valid_txt').val());
    return;
  }

  if ($('#course_order').val() == '') {
    toastr.error($('#course_order_valid_txt').val());
    return;
  }

  var stuElem = '<tr class="stu stu_' + $(elem).attr('cid') + ' epl_' + $(elem).attr('ep_id') + ' gra_' + $(elem).attr('gra_id') + '"><td>' + ($('.stu').length + 1) + '</td><td>' + $(elem).attr('suid') + '</td><td>' + $(elem).attr('sname') + '</td><td>' + $(elem).attr('gra_name') + '</td><td>' + $(elem).attr('ep_name') + '</td><td><input type="hidden" name="fkCtcCcs[]" value="' + $(elem).attr('csid') + '"><input type="hidden" name="stu_ids[]" value="' + $(elem).attr('cid') + '"><input type="hidden" name="sem_ids[]" value="' + $(elem).attr('sem_id') + '"><input type="hidden" name="gra_ids[]" value="' + $(elem).attr('gra_id') + '"><input type="hidden" name="epl_ids[]" value="' + $(elem).attr('ep_id') + '"><a data-id="' + $(elem).attr('cid') + '" onclick="removeSelStu(this)" href="javascript:void(0)" class="theme_btn red_btn min_btn">' + $('#delete_txt').val() + '</a></td></tr>';

  if ($('#course_group_main').val() != '') {
    if ($('#course_group_main').val() == 'flg' || $('#course_group_main').val() == 'ocg') {
      if ($('.stu').length && !$('.gra_' + $(elem).attr('gra_id') + '.epl_' + $(elem).attr('ep_id')).length) {
        toastr.error($('#grade_eplan_valid_txt').val());
        return;
      }
      showLoader(true);
      $.ajax({
        url: base_url + '/employee/fetch-educationplan-hours',
        type: 'POST',
        data: { 'ep_plan': $(elem).attr('ep_id'), 'ep_order': $('#course_order').val(), 'ep_gra': $(elem).attr('gra_id'), 'ctype': $('#course_group_main').val() },
        success: function (result) {
          if (result.status) {
            var stuElems = '<tr class="stu stu_' + $(elem).attr('cid') + ' epl_' + $(elem).attr('ep_id') + ' gra_' + $(elem).attr('gra_id') + '"><td>' + ($('.stu').length + 1) + '</td><td>' + $(elem).attr('suid') + '</td><td>' + $(elem).attr('sname') + '</td><td>' + $(elem).attr('gra_name') + '</td><td>' + $(elem).attr('ep_name') + '</td><td><input type="hidden" name="fkCtcCcs[]" value="' + $(elem).attr('csid') + '"><input type="hidden" name="clr_hrs[]" value="' + result.data + '"><input type="hidden" name="stu_ids[]" value="' + $(elem).attr('cid') + '"><input type="hidden" name="sem_ids[]" value="' + $(elem).attr('sem_id') + '"><input type="hidden" name="gra_ids[]" value="' + $(elem).attr('gra_id') + '"><input type="hidden" name="epl_ids[]" value="' + $(elem).attr('ep_id') + '"><a data-id="' + $(elem).attr('cid') + '" onclick="removeSelStu(this)" href="javascript:void(0)" class="theme_btn red_btn min_btn">' + $('#delete_txt').val() + '</a></td></tr>';

            $('.class_seleted_students tr:last').after(stuElems);
            $(".sel_stu_elem").removeClass('hide_content');

          } else {
            toastr.error(result.message);
          }

          showLoader(false);
        },
        error: function (data) {
          toastr.error($('#something_wrong_txt').val());
          showLoader(false);
        }
      });

    } else {
      $('.class_seleted_students tr:last').after(stuElem);
      $(".sel_stu_elem").removeClass('hide_content');
    }

  } else {
    toastr.error($('#cgtype_sel_valid_txt').val());
  }

}

function clearStuds() {
  $('.stu').remove();
}

function removeSelStu(elem) {
  var i = 1;
  var id = $(elem).attr('data-id');
  $('.stu_' + id).remove();

  if ($('.stu').length == 0) {
    $(".sel_stu_elem").addClass('hide_content');
  }

  $.each($('.stu'), function (index, value) {
    $(value).children('td:first').text(i++);
  });
}


function confirmDelete() {
  showLoader(true);
  var cid = $('#did').val();
  $.ajax({
    url: base_url + '/employee/coursegroup/' + cid,
    type: 'DELETE',
    dataType: 'json',
    cache: false,
    success: function (result) {
      $('#delete_prompt').modal('hide');
      if (result.status) {
        toastr.success(result.message);
        $('#course_group_listing').DataTable().ajax.reload();
      } else {
        toastr.error($('#something_wrong_txt').val());
      }

      showLoader(false);
    },
    error: function (data) {
      toastr.error($('#something_wrong_txt').val());
      showLoader(false);
    }
  });
}


function triggerDelete(cid) {
  $('#did').val(cid);
  $(".show_delete_modal").click();
}

$('#is_village_school').click(function () {
  $('#fkClrVsc').val('');
  if ($(this).prop("checked") == true) {
    $('.village_schools_drp').removeClass('hide_content');
  }
  else if ($(this).prop("checked") == false) {
    $('.village_schools_drp').addClass('hide_content');
  }
});

// Class Creation listing Delete
function triggerDeleteClsCre(cid) {
  $('#did').val(cid);
  $(".show_delete_modal").click();
}
