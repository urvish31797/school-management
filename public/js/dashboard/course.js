var form = $("form[name='add-course-form']");
var page_url = base_url + '/admin/courses';

$(function () {
  $("#search_course").on('keyup', function () {
    $('.courses_listing').DataTable().ajax.reload()
  });

  $('#add_new').on('hidden.bs.modal', function () {
    $("form").trigger("reset");
    $("#pkCrs").val('');
  });

  $('#add_new').on('shown.bs.modal', function () {
    $("form").data('validator').resetForm();
  });

  $('#delete_prompt').on('hidden.bs.modal', function () {
    $("#did").val('');
  });

});

function save() {
  if (form.valid()) {
    showLoader(true);
    var formData = new FormData($(form)[0]);
    formData.append("pkCrs", $("#pkCrs").val());
    $.ajax({
      url: page_url,
      type: 'POST',
      processData: false,
      contentType: false,
      cache: false,
      data: formData,
      dataType: "JSON",
      success: function (result) {
        toastr.success(result.message);
        $('#add_new').modal('hide');
        $('.courses_listing').DataTable().ajax.reload();
      },
      error: function (jqXHR) {
        if (jqXHR.status == 400) {
          toastr.error(jqXHR.responseJSON.message);
        }
        else if (jqXHR.status == 422) {
          setvalidationmessages(jqXHR.responseJSON.errors);
        }
        else {
          toastr.error($('#something_wrong_txt').val());
        }
      }
    });
    showLoader(false);
  }
}

function confirmDelete() {
  showLoader(true);
  var cid = $('#did').val();
  $.ajax({
    url: page_url + '/' + cid,
    type: 'DELETE',
    dataType: 'json',
    cache: false,
    success: function (result) {
      $('#delete_prompt').modal('hide');
      toastr.success(result.message);
      $('.courses_listing').DataTable().ajax.reload();
    },
    error: function (jqXHR) {
      if (jqXHR.status == 400) {
        toastr.error(jqXHR.responseJSON.message);
      }
      else {
        toastr.error($('#something_wrong_txt').val());
      }
    }
  });
  showLoader(false);
}

function triggerEdit(cid) {
  showLoader(true);
  $.ajax({
    url: page_url + '/' + cid + '/edit',
    type: 'GET',
    dataType: 'json',
    cache: false,
    success: function (result) {
      $("#crs_CourseAlternativeName").removeClass("input_disable");
      $("#crs_CourseAlternativeName").removeAttr("readonly");
      $.each(result.data, function (index, value) {
        if (index == "crs_CourseAlternativeName" && value == "Stručna praksa") {
          $("#" + index).val(value);
          $("#" + index).addClass("input_disable");
          $("#" + index).attr("readonly", "readonly");
        }
        else {
          $("#" + index).val(value);
        }

      });
      $("input[name=crs_IsForeignLanguage][value='" + result.data.crs_IsForeignLanguage + "']").prop("checked", true);
      $(".show_modal").click();
    },
    error: function (jqXHR) {
      if (jqXHR.status == 400) {
        toastr.error(jqXHR.responseJSON.message);
      }
      else {
        toastr.error($('#something_wrong_txt').val());
      }
    }
  });
  showLoader(false);
}

function triggerDelete(cid) {
  $('#did').val(cid);
  $(".show_delete_modal").click();
}

