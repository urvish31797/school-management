$(function () {

    showLoader(false);

    $("#search_student").on('change', function () {
        $('#student_listing').DataTable().ajax.reload();
    });

    $("#fkClrGra").on('change', function () {
        $('#student_listing').DataTable().ajax.reload()
    });

    $('#add_new').on('hidden.bs.modal', function () {
        $("form").trigger("reset");
        $("#pkCla").val('');
    })

    $('#delete_prompt').on('hidden.bs.modal', function () {
        $("#did").val('');
    })

    $('#add_new').on('shown.bs.modal', function () {
        $("form").data('validator').resetForm();
    })

    $("form[name='course_order_form']").validate({
        errorClass: "error_msg",
        rules: {
            epl: {
                required: true,
            },
        },
        errorPlacement: function (error, element) {
            if ((element.hasClass('select2_multi') || element.hasClass('select2_drop')) && element.next('.select2-container').length) {
                error.insertAfter(element.next('.select2-container'));
            } else {
                error.insertAfter(element);
            }
        },
        submitHandler: function (form, event) {
            event.preventDefault();

            showLoader(true);

            $('#epl').prop('disabled', false);
            var formData = new FormData($(form)[0]);
            if ($("#pkCor").length) {
                formData.append("pkCor", $("#pkCor").val());
            }

            $.ajax({
                url: base_url + '/employee/courseorders',
                type: 'POST',
                processData: false,
                contentType: false,
                cache: false,
                data: formData,
                success: function (result) {
                    if (result.status) {
                        toastr.success(result.message);

                        redirectPage("employee/courseorders");
                    } else {
                        toastr.error(result.message);
                    }

                    showLoader(false);
                },
                error: function (data) {
                    toastr.error($('#something_wrong_txt').val());
                    showLoader(false);
                }
            });

            if ($("#pkCor").length) {
                $('#epl').prop('disabled', true);
            }
        }
    });

    //Course Orders Listing
    $('#course_order_listing').on('processing.dt', function (e, settings, processing) {
        if (processing) {
            showLoader(true);
        } else {
            showLoader(false);
        }
    }).DataTable({
        "columnDefs": [{


        }],
        "language": {
            "sLengthMenu": $('#show_txt').val() + " _MENU_ " + $('#entries_txt').val(),
            "info": $('#showing_txt').val() + " _START_ " + $('#to_txt').val() + " _END_ " + $('#of_txt').val() + " _TOTAL_ " + $('#entries_txt').val(),
            "emptyTable": $('#msg_no_data_available_table').val(),
            "paginate": {
                "previous": $('#previous_txt').val(),
                "next": $('#next_txt').val()
            }
        },
        "lengthMenu": [10, 20, 30, 50],
        "searching": false,
        "serverSide": true,
        "deferRender": true,
        "ajax": {
            "url": listing_url,
            "type": "POST",
            "dataType": 'json',
            "data": function (d) {
                d.search = $('#search_course_order').val();
            }
        },
        columns: [{
            "data": "index",
            className: "text-center"
        },
        {
            "data": "cor_Uid",
            sortable: !1
        },
        {
            "data": "cor_Uid",
            sortable: !1,
            render: function (data, type, mb) {
                return mb.education_program.edp_Name + ' - ' + mb.education_plan.epl_EducationPlanName;
            }
        },
        {
            "data": "pkCor",
            sortable: !1,
            render: function (data, type, mb) {
                return '\t\t\t\t\t\t<a class="ajax_request no_sidebar_active" href="courseorders/' + mb.pkCor + '"><img src="' + imagepath + '/ic_eye_color.png"></a>\t\t\t\t\t\t<a class="ajax_request no_sidebar_active" href="courseorders/' + mb.pkCor + '/edit"><img src="' + imagepath + '/ic_mode_edit.png"></a>\t\t\t\t\t\t<a onclick="triggerDelete(' + mb.pkCor + ')" href="javascript:void(0)"><img src="' + imagepath + '/ic_delete.png"></a>';
            }
        },
        ],
    });

    $("#search_course_order").on('keyup', function () {
        $('#course_order_listing').DataTable().ajax.reload();
    });

    $("#searchGrade").on('change', function () {
        $('#course_order_listing').DataTable().ajax.reload()
    });

    $("#search_sch_year").on('change', function () {
        $('#course_order_listing').DataTable().ajax.reload()
    });

});

function fetchCourses(val) {
    $('.profile_info_container').remove();

    showLoader(true);
    $.ajax({
        url: courses_url,
        type: 'POST',
        dataType: 'json',
        cache: false,
        data: {
            'epl': val
        },
        success: function (result) {

            if (result.status) {
                //For mandatory course
                var grades = [];
                var gradesTmp = [];
                var mc_counts = [];
                var count_mc_gra = [];
                //For Optional course
                var oc_counts = [];
                var count_oc_gra = [];

                $.each(result.data.foreign_language_course, function (key, value) {
                    count_mc_gra.push(value.fkEflGra);
                });

                $.each(result.data.mandatory_course, function (key, value) {
                    count_mc_gra.push(value.fkEmcGra);
                    // gradesTmp.push(value.fkEmcGra);
                    //gradesTmp[value.fkEmcGra] = value.grade.gra_GradeName;
                    grades[value.fkEmcGra] = value.grade.gra_GradeNumeric;
                });

                $.each(result.data.optional_course, function (key, value) {
                    count_oc_gra.push(value.fkEocGra);
                });

                // var grades = gradesTmp.filter( onlyUnique );

                grades.forEach(function (value, key) {
                    var mc_course = '';
                    var fl_course = '';
                    var oc_course = '';
                    var m = 1;
                    var o = 1;
                    $.each(result.data.mandatory_course, function (km, vm) {

                        if (key == vm.grade.pkGra) {
                            mc_course = mc_course + '<tr class="mc_elem course_' + vm.fkEplCrs + ' gra_' + vm.fkEmcGra + '"><td>' + m + '</td><td>' + vm.mandatory_course_group.crs_CourseName + '</td><td>' + vm.grade.gra_GradeNumeric + '</td><td><input type="hidden" name="crs_ids[]" value="' + vm.fkEplCrs + '"><input type="hidden" name="epr_ids[]" value="' + result.data.fkEplEdp + '"><input type="hidden" name="gra_ids[]" value="' + vm.fkEmcGra + '"><select id="sel_mc_' + vm.fkEmcGra + '_' + m + '" name="sel_mc_' + vm.fkEmcGra + '_' + m + '" data-sel-gra="mc_gra_' + vm.fkEmcGra + '" onchange="courseOrderSelect(this)" required class="form-control icon_control dropdown_control mc_gra_' + vm.fkEmcGra + '"><option value="">' + $('#select_txt').val() + '</option></select></td></tr>';
                            m++;
                        }
                    });

                    $.each(result.data.foreign_language_course, function (kf, vf) {
                        if (key == vf.grade.pkGra) {
                            fl_course = fl_course + '<tr class="mc_elem course_' + vf.fkEflCrs + ' gra_' + vf.fkEflGra + '"><td>' + m + '</td><td>' + vf.foreign_language_group.crs_CourseName + '</td><td>' + vf.grade.gra_GradeNumeric + '</td><td><input type="hidden" name="crs_ids[]" value="' + vf.fkEflCrs + '"><input type="hidden" name="epr_ids[]" value="' + result.data.fkEplEdp + '"><input type="hidden" name="gra_ids[]" value="' + vf.fkEflGra + '"><select id="sel_mc_' + vf.fkEflGra + '_' + m + '" name="sel_mc_' + vf.fkEflGra + '_' + m + '" data-sel-gra="mc_gra_' + vf.fkEflGra + '" onchange="courseOrderSelect(this)" required class="form-control icon_control dropdown_control mc_gra_' + vf.fkEflGra + '"><option value="">' + $('#select_txt').val() + '</option></select></td></tr>';
                            m++;
                        }
                    });


                    $.each(result.data.optional_course, function (ko, vo) {
                        if (key == vo.grade.pkGra) {
                            oc_course = oc_course + '<tr class="oc_elem course_' + vo.fkEocCrs + ' gra_' + vo.fkEocGra + '"><td>' + o + '</td><td>' + vo.optional_courses_group.crs_CourseName + '</td><td>' + vo.grade.gra_GradeNumeric + '</td><td><input type="hidden" name="crs_ids[]" value="' + vo.fkEocCrs + '"><input type="hidden" name="epr_ids[]" value="' + result.data.fkEplEdp + '"><input type="hidden" name="gra_ids[]" value="' + vo.fkEocGra + '"><select id="sel_oc_' + vo.fkEocGra + '_' + o + '" name="sel_oc_' + vo.fkEocGra + '_' + o + '" data-sel-gra="oc_gra_' + vo.fkEocGra + '" onchange="courseOrderSelect(this)" required class="form-control icon_control dropdown_control oc_gra_' + vo.fkEocGra + '"><option value="">' + $('#select_txt').val() + '</option></select></td></tr>';
                            o++;
                        }
                    });

                    //
                    var cuOrderElem = '<div class="profile_info_container full_width ep_gra_' + key + '"><div class="row"><div class="col-4"></div><div class="col-4" style="text-align: center;"><strong class="gra_name">Grade - ' + value + '</strong></div><div class="col-4 text-right"></div><div class="col-lg-6"><p class="mt-2"><strong>' + $('#gn_mandatory_courses').val() + '</strong></p><div class="table-responsive mt-2 mb-3"><table class="color_table mc_course "><tbody><tr><th>#</th><th>' + $('#gn_courses').val() + '</th><th>' + $('#gn_grade').val() + '</th><th>' + $('#gn_course_order').val() + '</th></tr>' + mc_course + fl_course + '</tbody></table></div></div><div class="col-lg-6"><p class="mt-2"><strong>' + $('#gn_optional_courses').val() + '</strong></p><div class="table-responsive mt-2 mb-3"><table class="color_table oc_course "><tbody><tr><th>#</th><th>' + $('#gn_courses').val() + '</th><th>' + $('#gn_grade').val() + '</th><th>' + $('#gn_course_order').val() + '</th></tr>' + oc_course + '</tbody></table></div></div></div></div>'

                    $('.white_box.p-5.pl-3.pr-3 .col-md-12').before(cuOrderElem);
                });

                count_mc_gra.forEach(function (x) {
                    mc_counts[x] = (mc_counts[x] || 0) + 1;
                });

                $.each(mc_counts, function (key, value) {
                    for (var i = 1; i <= value; i++) {
                        $('.mc_gra_' + key).append($("<option></option>")
                            .attr("value", i)
                            .text(i));
                    }
                });

                count_oc_gra.forEach(function (x) { oc_counts[x] = (oc_counts[x] || 0) + 1; });

                $.each(oc_counts, function (key, value) {
                    for (var i = 1; i <= value; i++) {
                        $('.oc_gra_' + key).append($("<option></option>")
                            .attr("value", i)
                            .text(i));
                    }
                });
            }
            else {
                toastr.error(result.message);
            }

            showLoader(false);
        },
        error: function (data) {
            toastr.error($('#something_wrong_txt').val());
            showLoader(false);
        }
    });
}

function onlyUnique(value, index, self) {
    return self.indexOf(value) === index;
}

function courseOrderSelect(elem) {
    var selClass = '.' + $(elem).attr('data-sel-gra');
    var currId = $(elem).attr('id');
    $(selClass + ":not(#" + currId + ") option[value='" + $(elem).attr('currVal') + "']").attr('disabled', false);
    if (elem.value != '') {
        $(elem).attr('currVal', elem.value);
        $(selClass + ":not(#" + currId + ") option[value='" + elem.value + "']").attr('disabled', true);
    } else {
        $(elem).attr('currVal', '');
    }
}

function confirmDelete() {
    showLoader(true);
    var cid = $('#did').val();
    $.ajax({
        url: base_url + '/employee/courseorders/' + cid,
        type: 'DELETE',
        dataType: 'json',
        cache: false,
        success: function (result) {
            $('#delete_prompt').modal('hide');
            if (result.status) {
                toastr.success(result.message);
                $('#course_order_listing').DataTable().ajax.reload();
            } else {
                toastr.error($('#something_wrong_txt').val());
            }

            showLoader(false);
        },
        error: function (data) {
            toastr.error($('#something_wrong_txt').val());
            showLoader(false);
        }
    });
}


function triggerDelete(cid) {
    $('#did').val(cid);
    $(".show_delete_modal").click();
}
