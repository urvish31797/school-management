var base_url = $('#web_base_url').val();
// var second_step_grades = [];
$(function () {
  if ($('.select2_multi_view').length) {
    $('.select2_multi_view').select2();
  }

  showLoader(false);
  $('.select2_drop').select2({
    "language": {
      "noResults": function () {
        return $('#no_result_text').val();
      }
    },
    placeholder: $('#select_txt').val(),
    allowClear: true
  });

  $("#start_date").datepicker({
    format: "dd/mm/yyyy",
    autoclose: true,
    startDate: '+0d'
  }).on('changeDate', function () {
    $('#end_date').datepicker('setStartDate', new Date($(this).val()));
  });

  $('#end_date').datepicker({
    format: "dd/mm/yyyy",
    autoclose: true,
    startDate: $("#start_date").val(),
  }).on('changeDate', function () {
    if ($("#start_date").val() != '') {
      $('#start_date').datepicker('setEndDate', new Date($(this).val()));
    }
  });

  $("#fkCcsSen").select2({
    allowClear: true,
    placeholder: 'Select',
    ajax: {
      type: 'POST',
      url: base_url + "/employee/schoolyear-students-list",
      dataType: 'json',
      delay: 250,
      cache: true,
      beforeSend: function () {
        showLoader(true);
      },
      data: function (params) {
        return {
          term: params.term || '',
          page: params.page || 1,
          grades: $("#fkClrGra").val(),
          year: $("#fkClrSye").val(),
          school: $("#fkClrSch").val(),
          villageschool: $("#fkClrVsc").val(),
        }
      },
      success: function () {
        showLoader(false);
      },
      error: function () {
        showLoader(false);
      }
    },
    language: {
      searching: function () {
        return "Searching......";
      }
    },
  });

  var fkCcsSen = $("#edit_fkCcsSen").val();
  if (fkCcsSen != "") {
    var name = $("#edit_fkCcsSen").data('name');
    var gra = $("#edit_fkCcsSen").data('gra');
    var gratext = $("#gradetxt").val();
    var label = name + " - " + gratext + " " + gra;
    var $select = $('#fkCcsSen');
    var $option = $('<option selected>' + label + '</option>').val(fkCcsSen);

    $select.append($option).trigger('change');
  }

  $("#fkCcsSent").select2({
    allowClear: true,
    placeholder: 'Select',
    ajax: {
      type: 'POST',
      url: base_url + "/employee/schoolyear-students-list",
      dataType: 'json',
      delay: 250,
      cache: true,
      beforeSend: function () {
        showLoader(true);
      },
      data: function (params) {
        return {
          term: params.term || '',
          page: params.page || 1,
          grades: $("#fkClrGra").val(),
          year: $("#fkClrSye").val(),
          school: $("#fkClrSch").val(),
          villageschool: $("#fkClrVsc").val(),
        }
      },
      success: function () {
        showLoader(false);
      },
      error: function () {
        showLoader(false);
      }
    },
    language: {
      searching: function () {
        return "Searching......";
      }
    },
  });

  var fkCcsSent = $("#edit_fkCcsSent").val();
  if (fkCcsSent != "") {
    var name = $("#edit_fkCcsSent").data('name');
    var gra = $("#edit_fkCcsSent").data('gra');
    var gratext = $("#gradetxt").val();
    var label = name + " - " + gratext + " " + gra;
    var $select = $('#fkCcsSent');
    var $option = $('<option selected>' + label + '</option>').val(fkCcsSent);

    $select.append($option).trigger('change');
  }

  $('#student_listing').on('processing.dt', function (e, settings, processing) {
    if (processing) {
      showLoader(true);
    } else {
      showLoader(false);
    }
  }).DataTable({
    "columnDefs": [{
      "targets": 4,
      "createdCell": function (td, cellData, rowData, row, col) {
        if (cellData == 'Active') {
          $(td).addClass('active_status');
        } else {
          $(td).addClass('disable_status');
        }
      }
    }],
    "language": {
      "sLengthMenu": $('#show_txt').val() + " _MENU_ " + $('#entries_txt').val(),
      "info": $('#showing_txt').val() + " _START_ " + $('#to_txt').val() + " _END_ " + $('#of_txt').val() + " _TOTAL_ " + $('#entries_txt').val(),
      "emptyTable": $('#msg_no_data_available_table').val(),
      "paginate": {
        "previous": $('#previous_txt').val(),
        "next": $('#next_txt').val()
      }
    },
    "lengthMenu": [200, 500, 1000],
    "searching": false,
    "serverSide": true,
    "deferRender": true,
    "ajax": {
      "url": base_url + "/employee/fetch-class-students-list",
      "type": "POST",
      "dataType": 'json',
      "data": function (d) {
        d.search = $('#search_student').val();
        d.pkClr = $('#pkClr').val();
        d.pkCcs = $('#pkCcs').val();
        d.fkClrEdp = $('#fkClrEdp').val();
        d.fkClrVsc = $("#fkClrVsc").val();
      }
    },
    columns: [
      { "data": "index", className: "text-center" },
      {
        "data": "index", sortable: !1,
        render: function (data, type, mb) {
          if (mb.student.stu_StudentID == '' || mb.student.stu_StudentID == null) {
            return mb.student.stu_TempCitizenId;
          } else {
            return mb.student.stu_StudentID;
          }
        }
      },
      {
        "data": "stu_StudentName",
        render: function (data, type, mb) {
          return mb.student.stu_StudentName + ' ' + mb.student.stu_StudentSurname;
        }
      },
      {
        "data": "gra_GradeName_en",
        render: function (data, type, mb) {
          return mb.grade.gra_GradeNumeric;
        }
      },
      {
        "data": "index", sortable: !1,
        render: function (data, type, mb) {
          return mb.education_program.edp_Name + ' - ' + mb.education_plan.epl_EducationPlanName;
        }
      },
      {
        "data": "cla_ClassName", sortable: !1,
        render: function (data, type, mb) {
          var suid = null;
          if (mb.student.stu_StudentID == '' || mb.student.stu_StudentID == null) {
            suid = mb.student.stu_TempCitizenId;
          } else {
            suid = mb.student.stu_StudentID;
          }
          return '<button type="button" cid="' + mb.pkSte + '" suid="' + suid + '" sname="' + mb.student.stu_StudentName + ' ' + mb.student.stu_StudentSurname + '" gra_name="' + mb.grade.gra_GradeNumeric + '" gra_id="' + mb.grade.pkGra + '" ep_id="' + mb.education_plan.pkEpl + '" ep_name="' + mb.education_program.edp_Name + ' - ' + mb.education_plan.epl_EducationPlanName + '" class="theme_btn min_btn" onclick="addStudent(this)">' + $('#add_txt').val() + '</button>'
        }
      },
    ],

  });

  $("#search_student").on('keyup', function () {
    //if($(this).val() != ''){
    $('#student_listing').DataTable().ajax.reload()
    // }
  });

  $('#add_new').on('hidden.bs.modal', function () {
    $("form").trigger("reset");
    $("#pkCla").val('');
  })

  $('#delete_prompt').on('hidden.bs.modal', function () {
    $("#did").val('');
  })

  $('#add_new').on('shown.bs.modal', function () {
    $("form").data('validator').resetForm();
  })

  $("#filter_students_btn").on('click', function () {
    var oldClass = $("#oldClassDropdown").val();
    if (oldClass == "") {
      toastr.error($("#bothOptionMessage").val());
    }
    else {
      fetchPreviousGradeStudents(oldClass);
    }
  });

  $("#fkClrGra").on('change', function () {
    var selectedItems = $("#fkClrGra").select2('data');
    var status = true;
    if ($("#is_village_school").prop('checked') == true && $("#fkClrVsc").val() != "") {
      if (selectedItems.length > 0) {
        for (var i = 0; i < selectedItems.length; i++) {
          if (selectedItems[i].text > 5) {
            status = false;
            $(this).find('[value=' + selectedItems[i].id + ']').prop("selected", false).parent().trigger("change");
          }
        }

        if (status == false) {
          toastr.error($("#combineerror").val());
        }
      }
    }
    else {
      if (selectedItems.length > 1) {
        for (var i = 0; i < selectedItems.length; i++) {
          $("#fkClrGra option:selected").attr("selected", false);
          status = false;
          $(this).find('[value=' + selectedItems[i].id + ']').prop("selected", false).parent().trigger("change");
        }

        if (status == false) {
          toastr.error($("#combinevillageerror").val());
        }
      }
    }
  })

  $("#is_village_school").on('click', function () {
    if ($("#is_village_school").prop('checked') == false) {
      var selectedItems = $("#fkClrGra").select2('data');
      if (selectedItems.length > 0) {
        for (var i = 0; i < selectedItems.length; i++) {
          $("#fkClrGra option:selected").attr("selected", false);
          $("#fkClrGra").find('[value=' + selectedItems[i].id + ']').prop("selected", false).parent().trigger("change");
        }
      }
    }
  })

  $("form[name='class-creation-step-1']").validate({
    errorClass: "error_msg",
    rules: {
      fkClrSye: {
        required: true,
      },
      fkClrEdp: {
        required: true,
      },
      fkClrGra: {
        required: true,
      },
      fkClrCla: {
        required: true,
      },
      fkClrVsc: {
        required: true,
      },
    },
    errorPlacement: function (error, element) {
      if (element.hasClass('select2_drop') && element.next('.select2-container').length) {
        error.insertAfter(element.next('.select2-container'));
      } else {
        error.insertAfter(element);
      }
    },
    submitHandler: function (form, event) {
      event.preventDefault();
      $('#fkClrSye').prop('disabled', false);
      showLoader(true);
      var formData = new FormData($(form)[0]);
      formData.append("fkClrSch", $("#fkClrSch").val());
      formData.append("pkClr", $("#pkClr").val());
      formData.append("pkCcs", $("#pkCcs").val());
      formData.append("class_step", 1);
      $.ajax({
        url: base_url + '/employee/classcreation',
        type: 'POST',
        processData: false,
        contentType: false,
        cache: false,
        data: formData,
        success: function (result) {
          if (result.status) {
            toastr.success(result.message);
            if (result.graChanged) {
              $('.sel_stu_elem').addClass('hide_content');
              $('.sel_stu_elem .stu').remove();
            } else {
              $('.step_2_resp').html(result.step_2_data);
            }

            $('#oldClassDropdown').html('');
            if (result.oldClassCreationData != "") {
              var gradeName = result.previousGradeName;

              $("#oldClassDropdown").append(`<option value="" selected>Select</option>`);
              $(result.oldClassCreationData).each(function (index, element) {
                var value = "";
                if (element.class_creation.village_school != null) {
                  value = gradeName + "." + element.class_creation.class_creation_classes.cla_ClassName + " - " + element.class_creation.village_school.vsc_VillageSchoolName;
                }
                else {
                  value = gradeName + "." + element.class_creation.class_creation_classes.cla_ClassName;
                }
                $('#oldClassDropdown').append(`<option value="${element.pkCcs}">${value}</option>`);
              });

              $("#oldClassDropdownDiv").show();
              $("#filterbtnDiv").show();
            }
            else {
              $("#oldClassDropdownDiv").hide();
              $("#filterbtnDiv").hide();
            }

            activaTab(2);
            $('#pkClr').val(result.pkClr);
            $('#pkCcs').val(result.pkCcs);
            $('#student_listing').DataTable().ajax.reload()

          } else {
            toastr.error(result.message);
          }

          showLoader(false);
        },
        error: function (data) {
          toastr.error($('#something_wrong_txt').val());
          showLoader(false);
        }
      });
      $('#fkClrSye').prop('disabled', true);
    }
  });

  $("form[name='class-creation-step-2']").validate({
    errorClass: "error_msg",

    submitHandler: function (form, event) {
      event.preventDefault();
      if ($('.stu').length == 0) {
        toastr.error($('#stu_sel_txt').val());
        return;
      }
      $('#fkClrSye').prop('disabled', false);
      showLoader(true);
      // var values = $("input[name='stu_ids[]']").map(function(){return $(this).val();}).get();
      var formData = new FormData($(form)[0]);
      formData.append("pkClr", $("#pkClr").val());
      formData.append("pkCcs", $('#pkCcs').val());
      // formData.append("second_step_grades",second_step_grades.toString());
      formData.append("class_step", 2);
      $.ajax({
        url: base_url + '/employee/classcreation',
        type: 'POST',
        processData: false,
        contentType: false,
        cache: false,
        data: formData,
        success: function (result) {
          if (result.status) {
            toastr.success(result.message);
            activaTab(3);
            $('#pkClr').val(result.pkClr);
            $('.step_3_resp').html(result.step_3_data);
            $('.select2_multi').select2({
              "language": {
                "noResults": function () {
                  return $('#no_result_text').val();
                }
              },
              placeholder: $('#select_txt').val(),
              allowClear: true
            });
          } else {
            toastr.error(result.message);
          }

          showLoader(false);
        },
        error: function (data) {
          toastr.error($('#something_wrong_txt').val());
          showLoader(false);
        }
      });

    }
  });

  $("form[name='class-creation-step-3']").validate({
    errorClass: "error_msg",
    submitHandler: function (form, event) {
      event.preventDefault();

      showLoader(true);
      var formData = new FormData($(form)[0]);
      formData.append("pkClr", $("#pkClr").val());
      formData.append("pkCcs", $('#pkCcs').val());
      formData.append("class_step", 3);
      $.ajax({
        url: base_url + '/employee/classcreation',
        type: 'POST',
        processData: false,
        contentType: false,
        cache: false,
        data: formData,
        success: function (result) {
          if (result.status) {
            toastr.success(result.message);
            activaTab(4);
            $('#pkClr').val(result.pkClr);
            $('.step_4_resp').html(result.step_4_data);
          } else {
            toastr.error(result.message);
          }

          showLoader(false);
        },
        error: function (data) {
          toastr.error($('#something_wrong_txt').val());
          showLoader(false);
        }
      });

    }
  });

  $("form[name='class-creation-step-4']").validate({
    errorClass: "error_msg",
    errorPlacement: function (error, element) {
      if (element.hasClass('select2_multi') && element.next('.select2-container').length) {
        error.insertAfter(element.next('.select2-container'));
      } else {
        error.insertAfter(element);
      }
    },
    submitHandler: function (form, event) {
      event.preventDefault();

      showLoader(true);
      var formData = new FormData($(form)[0]);
      formData.append("pkClr", $("#pkClr").val());
      formData.append("pkCcs", $('#pkCcs').val());
      formData.append("class_step", 4);
      $.ajax({
        url: base_url + '/employee/classcreation',
        type: 'POST',
        processData: false,
        contentType: false,
        cache: false,
        data: formData,
        success: function (result) {
          if (result.status) {
            toastr.success(result.message);
            activaTab(5);
            $('#pkClr').val(result.pkClr);
            $('.step_5_resp').html(result.step_5_data);
            $('.select2_multi5').select2({
              "language": {
                "noResults": function () {
                  return $('#no_result_text').val();
                }
              },
              placeholder: $('#select_txt').val(),
              allowClear: true
            });
          } else {
            toastr.error(result.message);
          }

          showLoader(false);
        },
        error: function (data) {
          toastr.error($('#something_wrong_txt').val());
          showLoader(false);
        }
      });

    }
  });

  $("form[name='class-creation-step-5']").validate({
    errorClass: "error_msg",
    errorPlacement: function (error, element) {
      if (element.hasClass('select2_single') && element.next('.select2-container').length) {
        error.insertAfter(element.next('.select2-container'));
      } else {
        error.insertAfter(element);
      }
    },
    submitHandler: function (form, event) {
      event.preventDefault();

      showLoader(true);
      var formData = new FormData($(form)[0]);
      formData.append("pkClr", $("#pkClr").val());
      formData.append("pkCcs", $('#pkCcs').val());
      formData.append("class_step", 5);
      $.ajax({
        url: base_url + '/employee/classcreation',
        type: 'POST',
        processData: false,
        contentType: false,
        cache: false,
        data: formData,
        success: function (result) {
          if (result.status) {
            toastr.success(result.message);
            redirectPage("employee/classcreation");
          } else {
            toastr.error(result.message);
          }

          showLoader(false);
        },
        error: function (data) {
          toastr.error($('#something_wrong_txt').val());
          showLoader(false);
        }
      });

    }
  });

  // $("form[name='course_details']").validate({
  //     errorClass: "error_msg",
  //     submitHandler: function(form, event) {
  //       event.preventDefault();
  //       if($(".courses_select:checked").length == 0){
  //         toastr.error($('#course_select_txt').val());
  //         return;
  //       }
  //       var unchecked = [];
  //       $("form#course_details input:checkbox:not(:checked)").each(function () {
  //         unchecked.push($(this).val());
  //       });
  //       showLoader(true);
  //       var formData = new FormData($(form)[0]);
  //       formData.append("pkClr", $("#pkClr").val());
  //       formData.append("uncheckedCourse", unchecked);
  //       $.ajax({
  //           url: base_url+'/employee/course-class-selection',
  //           type: 'POST',
  //           processData: false,
  //           contentType: false,
  //           cache: false,
  //           data: formData,
  //           success: function(result)
  //           {
  //               if(result.status){
  //                 toastr.success(result.message);
  //                 $('#eplan_prompt').modal('hide');
  //               }else{
  //                 toastr.error(result.message);
  //               }

  //               showLoader(false);
  //           },
  //           error: function(data)
  //           {
  //               toastr.error($('#something_wrong_txt').val());
  //               showLoader(false);
  //           }
  //       });

  //   }
  // });

  //Class Creation Listing
  $('#class_creation_listing').on('processing.dt', function (e, settings, processing) {
    if (processing) {
      showLoader(true);
    } else {
      showLoader(false);
    }
  }).DataTable({
    "columnDefs": [{
    }],
    "language": {
      "sLengthMenu": $('#show_txt').val() + " _MENU_ " + $('#entries_txt').val(),
      "info": $('#showing_txt').val() + " _START_ " + $('#to_txt').val() + " _END_ " + $('#of_txt').val() + " _TOTAL_ " + $('#entries_txt').val(),
      "emptyTable": $('#msg_no_data_available_table').val(),
      "paginate": {
        "previous": $('#previous_txt').val(),
        "next": $('#next_txt').val()
      }
    },
    "lengthMenu": [10, 20, 30, 50],
    "searching": false,
    "serverSide": true,
    "deferRender": true,
    "bDestroy": true,
    "ajax": {
      "url": listing_url,
      "type": "POST",
      "dataType": 'json',
      "data": function (d) {
        d.search = $('#search_class_creations').val();
        d.grade = $('#searchGrade').val();
        d.schoolYear = $('#search_sch_year').val();
      }
    },
    columns: [
      { "data": "index", className: "text-center" },
      { "data": "grade", sortable: !1 },
      { "data": "class_creation_classes.cla_ClassName", sortable: !1 },
      {
        "data": "village_school", sortable: !1,
        render: function (data, type, mb) {
          if (mb.village_school != null) {
            return mb.village_school.vsc_VillageSchoolName;
          }
          else {
            return '-';
          }
        }
      },
      { "data": "class_creation_school_year.sye_NameCharacter", sortable: !1 },
      { "data": "clr_Status_txt", sortable: !1 },
      {
        "data": "pkClr", sortable: !1,
        render: function (data, type, mb) {
          var cc = '\t\t\t\t\t\t';
          return cc + '<a class="ajax_request no_sidebar_active" href="class-creation-semester/' + mb.pkClr + '"><img src="' + imagepath + 'ic_eye_color.png"></a></a>';
        }
      },
    ],
  });

  //Class Creation Listing
  $('#class_creation_semester_listing').on('processing.dt', function (e, settings, processing) {
    if (processing) {
      showLoader(true);
    } else {
      showLoader(false);
    }
  }).DataTable({
    "columnDefs": [{


    }],
    "language": {
      "sLengthMenu": $('#show_txt').val() + " _MENU_ " + $('#entries_txt').val(),
      "info": $('#showing_txt').val() + " _START_ " + $('#to_txt').val() + " _END_ " + $('#of_txt').val() + " _TOTAL_ " + $('#entries_txt').val(),
      "emptyTable": $('#msg_no_data_available_table').val(),
      "paginate": {
        "previous": $('#previous_txt').val(),
        "next": $('#next_txt').val()
      }
    },
    "lengthMenu": [10, 20, 30, 50],
    "searching": false,
    "serverSide": true,
    "deferRender": true,
    "bDestroy": true,
    "ajax": {
      "url": class_creation_sem_url,
      "type": "POST",
      "dataType": 'json',
      "data": function (d) {
        d.search = $('#search_class_creations').val();
        d.grade = $('#searchGrade').val();
        d.schoolYear = $('#search_sch_year').val();
        d.pkClr = $('#pkClr').val();
      }
    },
    columns: [
      { "data": "index", className: "text-center" },
      { "data": "grade", sortable: !1 },
      { "data": "class_creation.class_creation_classes.cla_ClassName", sortable: !1 },
      { "data": "semester.edp_EducationPeriodName", sortable: !1 },
      { "data": "total_stu", sortable: !1 },
      { "data": "class_creation.class_creation_school_year.sye_NameCharacter", sortable: !1 },
      {
        "data": "fkClrSye", sortable: !1,
        render: function (data, type, mb) {
          if (mb.home_room_teacher != '' && mb.home_room_teacher != null) {
            return mb.home_room_teacher[0].employees_engagement.employee.emp_EmployeeName + " " + mb.home_room_teacher[0].employees_engagement.employee.emp_EmployeeSurname;
          } else {
            return '-';
          }
        }
      },
      {
        "data": "pkCcs", sortable: !1,
        render: function (data, type, mb) {

          if (mb.class_creation.clr_Status == 'Pending') {
            return '<div class="btn-group" role="group">' +
              '<button id="btnGroupDrop1" type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">' + $("#actions_txt").val() + '</button>' +
              '<div class="dropdown-menu" aria-labelledby="btnGroupDrop1">' +
              '<a class="ajax_request no_sidebar_active dropdown-item" title="' + $('#edit_class_text').val() + '" href="' + base_url + '/employee/classcreation/' + mb.pkCcs + '/edit"><i class="fa fa-pencil-alt" aria-hidden="true"></i>&nbsp;' + $('#edit_class_text').val() + '</a>' +
              '<a class="dropdown-item" title="' + $('#delete_class_text').val() + '" onclick="triggerDeleteClsCre(' + mb.pkCcs + ')" href="javascript:void(0)"><i class="fa fa-trash" aria-hidden="true"></i>&nbsp;' + $('#delete_class_text').val() + '</a>' +
              '</div>' +
              '</div>';
          }
          return '<div class="btn-group" role="group">' +
            '<button id="btnGroupDrop1" type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">' + $("#actions_txt").val() + '</button>' +
            '<div class="dropdown-menu" aria-labelledby="btnGroupDrop1">' +
            '<a class="ajax_request no_sidebar_active dropdown-item" title="' + $('#final_marks_txt').val() + '" href="' + base_url + '/employee/finalmarks/' + mb.pkCcs + '"><i class="fa fa-paperclip" aria-hidden="true"></i> ' + $('#final_marks_txt').val() + '</a>' +
            '<a class="ajax_request no_sidebar_active dropdown-item" title="' + $('#sub_class_txt').val() + '" href="' + base_url + '/employee/classcreation/view-subclass/' + mb.pkCcs + '"><i class="fa fa-list-alt" aria-hidden="true"></i> ' + $('#sub_class_txt').val() + '</a>' +
            '<a class="ajax_request no_sidebar_active dropdown-item" title="' + $('#view_detail_text').val() + '" href="' + base_url + '/employee/classcreation/' + mb.pkCcs + '"><i class="fa fa-list-alt" aria-hidden="true"></i>&nbsp;' + $('#view_detail_text').val() + '</a>' +
            '<a class="ajax_request no_sidebar_active dropdown-item" title="' + $('#edit_class_text').val() + '" href="' + base_url + '/employee/classcreation/' + mb.pkCcs + '/edit"><i class="fa fa-pencil-alt" aria-hidden="true"></i>&nbsp;' + $('#edit_class_text').val() + '</a>' +
            '<a class="dropdown-item" title="' + $('#delete_class_text').val() + '" onclick="triggerDeleteClsCre(' + mb.pkCcs + ')" href="javascript:void(0)"><i class="fa fa-trash" aria-hidden="true"></i>&nbsp;' + $('#delete_class_text').val() + '</a>' +
            '<a class="ajax_request no_sidebar_active dropdown-item" title="' + $('#homeroomteacher_history_txt').val() + '" href="' + base_url + '/employee/view-homeroomteacher/' + mb.pkCcs + '"><i class="fa fa-users" aria-hidden="true"></i>&nbsp;' + $('#homeroomteacher_history_txt').val() + '</a>' +
            '</div>' +
            '</div>';
        }
      },
    ],

  });

  $("#search_class_creations").on('keyup', function () {
    $('#class_creation_listing').DataTable().ajax.reload()
  });

  $("#searchGrade").on('change', function () {
    $('#class_creation_listing').DataTable().ajax.reload()
  });

  $("#search_sch_year").on('change', function () {
    $('#class_creation_listing').DataTable().ajax.reload()
  });

});

$('#is_village_school').click(function () {
  $('#fkClrVsc').val('');
  if ($(this).prop("checked") == true) {
    $('.village_schools_drp').removeClass('hide_content');
  }
  else if ($(this).prop("checked") == false) {
    $('.village_schools_drp').addClass('hide_content');
  }
});

$('body').on("click", '.defaultCrsTeacherChckbox', function () {
  var index = $(this).val();
  if ($(this).prop("checked") == true) {
    $('#fkCtcEeg_' + index).attr('required', true);
    $('#fkCtcEeg_' + index).prop("disabled", false);
  }
  else if ($(this).prop("checked") == false) {
    $('#fkCtcEeg_' + index).removeAttr('required');
    $('#fkCtcEeg_' + index).prop("disabled", true);
  }
});

$("form[name='update_hometeacher_form']").validate({
  errorClass: "error_msg",
  rules: {
    homeroomteacher_sel: {
      required: true,
    },
  },
  submitHandler: function (form, event) {
    event.preventDefault();
    showLoader(true);
    var formData = new FormData($(form)[0]);
    $.ajax({
      url: base_url + '/employee/update-homeroomteacher',
      type: 'POST',
      processData: false,
      contentType: false,
      cache: false,
      data: formData,
      success: function (result) {
        if (result.status) {
          toastr.success(result.message);
          redirectPage("employee/classcreation");
        } else {
          toastr.error(result.message);
        }
        showLoader(false);
      },
      error: function (data) {
        toastr.error($('#something_wrong_txt').val());
        showLoader(false);
      }
    });
  }
});

$("#cancel_btn").on('click', function () {
  $("#update_hometeacher").show();
  $("#update_hometeacher_div").hide();
});

// Class Creation listing Delete
function triggerDeleteClsCre(cid) {
  $('#did').val(cid);
  $(".show_delete_modal").click();
}

function addStudent(elem) {

  if ($('.stu_' + $(elem).attr('cid')).length != 0) {
    toastr.error($('#stu_sel_valid_txt').val());
    return;
  }

  var gra_id = $(elem).attr('gra_id');
  var count = $('.stu').length;
  count = count + 1;
  $('.class_seleted_students tr:last').after('<tr class="stu stu_' + $(elem).attr('cid') + '"><td>' + count + '</td><td>' + $(elem).attr('suid') + '</td><td>' + $(elem).attr('sname') + '</td><td>' + $(elem).attr('gra_name') + '</td><td>' + $(elem).attr('ep_name') + '</td><td><input type="hidden" name="stu_ids[]" value="' + $(elem).attr('gra_id') + '_' + $(elem).attr('cid') + '"><input type="hidden" name="gra_id[]" value="' + gra_id + '"><input type="hidden" name="epl_ids[]" value="' + $(elem).attr('ep_id') + '"><a data-gra="' + $(elem).attr('gra_id') + '" data-edu="' + $(elem).attr('ep_id') + '" data-id="' + $(elem).attr('cid') + '" onclick="removeSelStu(this)" href="javascript:void(0)" class="theme_btn red_btn min_btn">' + $('#delete_txt').val() + '</a></td></tr>');

  $("#eid").val($(elem).attr('emp-id'));
  $(".sel_stu_elem").removeClass('hide_content');
}

function addBulkStudents(data) {
  if ($('.stu_' + data['cid']).length == 0) {
    var count = $('.stu').length;
    count = count + 1;
    //<button onclick="triggerEduPlan(this)" data-gra="'+data['gra_id']+'" data-stu="'+data['cid']+'" data-edu="'+data['ep_id']+'" type="button" class="theme_btn min_btn btn_margin">'+$('#view_courses_txt').val()+'</button>
    $('.class_seleted_students tr:last').after('<tr class="stu stu_' + data['cid'] + '"><td>' + count + '</td><td>' + data['suid'] + '</td><td>' + data['sname'] + '</td><td>' + data['gra_name'] + '</td><td>' + data['ep_name'] + '</td><td><input type="hidden" name="stu_ids[]" value="' + data['gra_id'] + '_' + data['cid'] + '"><input type="hidden" name="gra_id[]" value="' + data['gra_id'] + '"><input type="hidden" name="epl_ids[]" value="' + data['ep_id'] + '"><a data-gra="' + data['gra_id'] + '" data-edu="' + data['ep_id'] + '" data-id="' + data['cid'] + '" onclick="removeSelStu(this)" href="javascript:void(0)" class="theme_btn red_btn min_btn">' + $('#delete_txt').val() + '</a></td></tr>');
    $(".sel_stu_elem").removeClass('hide_content');
  }

}

function fetchPreviousGradeStudents(pkCcs) {
  var pkClr = $('#pkClr').val();
  var newData = { "pkCcs": pkCcs, 'pkClr': pkClr };
  $.ajax({
    url: base_url + '/employee/fetch-previousgrade-students',
    type: 'POST',
    data: newData,
    success: function (result) {
      if (result.status) {
        for (var i = 0; i < result.data.length; i++) {
          var data = [];
          data['sname'] = result.data[i].student.stu_StudentName + " " + result.data[i].student.stu_StudentSurname;
          data['cid'] = result.data[i].pkSte;
          data['gra_name'] = result.data[i].grade.gra_GradeNumeric;
          data['gra_id'] = result.data[i].grade.pkGra;
          data['ep_id'] = result.data[i].education_plan.pkEpl;
          data['ep_name'] = result.data[i].education_program.edp_Name + ' - ' + result.data[i].education_plan.epl_EducationPlanName;
          var suid = null;
          if (result.data[i].student.stu_StudentID == '' || result.data[i].student.stu_StudentID == null) {
            suid = result.data[i].student.stu_TempCitizenId;
          } else {
            suid = result.data[i].student.stu_StudentID;
          }
          data['suid'] = suid;

          addBulkStudents(data);
        }
      } else {
        toastr.error(result.message);
      }

      showLoader(false);
    },
    error: function (data) {
      toastr.error($('#something_wrong_txt').val());
      showLoader(false);
    }
  });
}

function triggerEduPlan(elem) {
  showLoader(true);

  var pkEpl = $(elem).attr('data-edu');
  var pkSen = $(elem).attr('data-stu');
  var pkGra = $(elem).attr('data-gra');
  var pkClr = $('#pkClr').val();
  var pkCcs = $('#pkCcs').val();
  var newData = { 'pkEpl': pkEpl, 'pkSen': pkSen, 'pkClr': pkClr, 'pkGra': pkGra, 'pkCcs': pkCcs }

  $.ajax({
    url: base_url + '/employee/fetchClassEducationPlan',
    type: 'POST',
    data: newData,
    success: function (result) {
      if (result.status) {
        $('.course_details').html(result.course_details);
        $("#trigger_edu_pop").click();
      } else {
        toastr.error(result.message);
      }

      showLoader(false);
    },
    error: function (data) {
      toastr.error($('#something_wrong_txt').val());
      showLoader(false);
    }
  });
}

function removeSelStu(elem) {
  showLoader(true);
  var i = 1;
  var id = $(elem).attr('data-id');

  $('.stu_' + id).remove();

  if ($('.stu').length == 0) {
    $(".sel_stu_elem").addClass('hide_content');
  }

  $.each($('.stu'), function (index, value) {
    $(value).children('td:first').text(i++);
  });
  showLoader(false);
}

function prevTab(tab) {
  var currTab = tab + 1;
  $('.tab-pane').removeClass('active show');
  $('#menu' + tab).addClass('active show');
  $('.btn-round-' + currTab).removeClass('active');
}

function activaTab(tab) {
  $('.tab-pane').removeClass('active show');
  $('#menu' + tab).addClass('active show');
  $('.btn-round-' + tab).addClass('active');
}

function confirmDelete() {
  showLoader(true);
  var cid = $('#did').val();
  $.ajax({
    url: base_url + '/employee/classcreation/' + cid,
    type: 'DELETE',
    dataType: 'json',
    cache: false,
    success: function (result) {
      $('#delete_prompt').modal('hide');
      if (result.status) {
        toastr.success(result.message);
        setTimeout(() => {
          window.location.href = result.redirect
        }, 1000);

      } else {
        toastr.error($('#something_wrong_txt').val());
      }

      showLoader(false);
    },
    error: function (data) {
      toastr.error($('#something_wrong_txt').val());
      showLoader(false);
    }
  });
}

function triggerDelete(cid) {
  $('#did').val(cid);
  $(".show_delete_modal").click();
}

function updateTeacher() {
  $("#update_hometeacher").hide();
  $("#update_hometeacher_div").show();
  $('#homeroomteacher_sel').select2({
    "language": {
      "noResults": function () {
        return $('#no_result_text').val();
      }
    },
    placeholder: $('#select_txt').val(),
    allowClear: true
  });
}

function cloneSemester() {
  showLoader(true);
  var currentSemesterId = $('#currentSemesterId').val();
  var nextSemesterId = $('#nextSemesterId').val();
  $.ajax({
    url: base_url + '/employee/clonesemester',
    type: 'POST',
    dataType: 'json',
    cache: false,
    data: { 'currentSemesterId': currentSemesterId, 'nextSemesterId': nextSemesterId },
    success: function (result) {
      $('#clone_semester').modal('hide');
      if (result.status) {
        toastr.success(result.message);
        $('#class_creation_semester_listing').DataTable().ajax.reload();
      } else {
        toastr.error($('#something_wrong_txt').val());
      }

      showLoader(false);
    },
    error: function (data) {
      toastr.error($('#something_wrong_txt').val());
      showLoader(false);
    }
  });
}

function triggerCourseDelete(cid) {
  $('#did').val(cid);
  $(".show_delete_modal").click();
}

function confirmCourseDelete() {
  showLoader(true);
  var cid = $('#did').val();
  $.ajax({
    url: base_url + '/employee/delete-student-course',
    type: 'POST',
    dataType: 'json',
    cache: false,
    data: { 'cid': cid },
    success: function (result) {
      $('#delete_prompt').modal('hide');
      if (result.status) {
        toastr.success(result.message);
        setTimeout(() => {
          window.location.href = $(location).attr("href");
        }, 1500);
      } else {
        toastr.error($('#something_wrong_txt').val());
      }

      showLoader(false);
    },
    error: function (data) {
      toastr.error($('#something_wrong_txt').val());
      showLoader(false);
    }
  });
}

function autoTeacherSelection(obj) {
  var id = $(obj).data('hometeacherid');
  if (id != "") {
    $('.select2_multi:not(:disabled)').each(function () {
      $("#" + $(this).attr("id")).find("option[value=" + id + "]").prop("selected", "selected").change();
    });
  }
  else {
    toastr.error($('#something_wrong_txt').val());
  }
}