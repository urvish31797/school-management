/**
* Admin Canton
*
* This file is used for admin JS
*
* @package    Laravel
* @subpackage JS
* @since      1.0
*/
var page_url = base_url + '/admin/canton';
var form = $("form[name='add-canton-form']");

$(function () {

  $("#search_canton").on('keyup', function () {
    $('.cantons_listing').DataTable().ajax.reload();
  });

  $("#country_filter").on('change', function () {
    $('.cantons_listing').DataTable().ajax.reload();
  });

  $("#state_filter").on('change', function () {
    $('.cantons_listing').DataTable().ajax.reload();
  });

  $('#add_new').on('hidden.bs.modal', function () {
    $("form").trigger("reset");
    $("#pkCan").val('');
  })

  $('#delete_prompt').on('hidden.bs.modal', function () {
    $("#did").val('');
  })

  $('#add_new').on('shown.bs.modal', function () {
    $("form").data('validator').resetForm();
  });
});

function save() {
  if (form.valid()) {
    showLoader(true);
    var formData = new FormData($(form)[0]);
    formData.append("pkCan", $("#pkCan").val());
    $.ajax({
      url: page_url,
      type: 'POST',
      processData: false,
      contentType: false,
      cache: false,
      data: formData,
      success: function (result) {
        toastr.success(result.message);
        $('#add_new').modal('hide');
        $('.cantons_listing').DataTable().ajax.reload();
      },
      error: function (jqXHR) {
        if (jqXHR.status == 400) {
          toastr.error(jqXHR.responseJSON.message);
        }
        else if (jqXHR.status == 422) {
          setvalidationmessages(jqXHR.responseJSON.errors);
        }
        else {
          toastr.error($('#something_wrong_txt').val());
        }
      }
    });
    showLoader(false);
  }
}

function confirmDelete() {
  showLoader(true);
  var cid = $('#did').val();
  $.ajax({
    url: page_url + '/' + cid,
    type: 'DELETE',
    dataType: 'json',
    cache: false,
    success: function (result) {
      $('#delete_prompt').modal('hide');
      toastr.success(result.message);
      $('.cantons_listing').DataTable().ajax.reload();
    },
    error: function (jqXHR) {
      if (jqXHR.status == 400) {
        toastr.error(jqXHR.responseJSON.message);
      }
      else {
        toastr.error($('#something_wrong_txt').val());
      }
    }
  });
  showLoader(false);
}

function triggerEdit(cid) {
  showLoader(true);
  $.ajax({
    url: page_url + '/' + cid + '/edit',
    type: 'GET',
    dataType: 'json',
    cache: false,
    success: function (result) {
      $.each(result.data, function (index, value) {
        $("#" + index).val(value);
      });
      fetchState(result.data.selCountry, 2, result.data.fkCanSta);
      $(".show_modal").click();
    },
    error: function (jqXHR) {
      if (jqXHR.status == 400) {
        toastr.error(jqXHR.responseJSON.message);
      }
      else {
        toastr.error($('#something_wrong_txt').val());
      }
    }
  });
  showLoader(false);
}

function fetchState(cid, type, set = '') {
  var select = '';
  if (type == 1) {
    select = "state_filter";
  } else {
    select = "fkCanSta";
  }
  $('#' + select).find('option').not(':first').remove();
  showLoader(true);
  $.ajax({
    url: statebycountry_url,
    type: 'POST',
    dataType: 'json',
    cache: false,
    data: { 'cid': cid },
    success: function (result) {
      $.each(result.data, function (key, value) {
        $('#' + select).append($("<option></option>")
          .attr("value", value.pkSta)
          .text(value.sta_StateName));
      });
      if (set != '') {
        $("#fkCanSta").val(set);
      }
    },
    error: function (jqXHR) {
      if (jqXHR.status == 400) {
        if (select == "fkCanSta") {
          toastr.error(jqXHR.responseJSON.message);
        }
      }
      else {
        toastr.error($('#something_wrong_txt').val());
      }
    }
  });
  showLoader(false);
}

function triggerDelete(cid) {
  $('#did').val(cid);
  $(".show_delete_modal").click();
}
