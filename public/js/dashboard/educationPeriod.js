var form = $("form[name='add-educationPeriod-form']");
var page_url = base_url + '/admin/educationperiod';
var change_semester_url = base_url + '/admin/changeSemester';

$(function () {

  $("#search_educationPeriod").on('keyup', function () {
    $('.educationPeriod_listing').DataTable().ajax.reload();
  });

  $('#add_new').on('hidden.bs.modal', function () {
    $("form").trigger("reset");
    $("#pkEdp").val('');
  })

  $('#delete_prompt').on('hidden.bs.modal', function () {
    $("#did").val('');
  });
});

function save() {
  if (form.valid()) {
    showLoader(true);
    var formData = new FormData($(form)[0]);
    formData.append("pkEdp", $("#pkEdp").val());
    $.ajax({
      url: page_url,
      type: 'POST',
      processData: false,
      contentType: false,
      cache: false,
      data: formData,
      success: function (result) {
        toastr.success(result.message);
        $('#add_new').modal('hide');
        $('.educationPeriod_listing').DataTable().ajax.reload();
      },
      error: function (jqXHR) {
        if (jqXHR.status == 400) {
          toastr.error(jqXHR.responseJSON.message);
        }
        else if (jqXHR.status == 422) {
          setvalidationmessages(jqXHR.responseJSON.errors);
        }
        else {
          toastr.error($('#something_wrong_txt').val());
        }
      }
    });
    showLoader(false);
  }
}

function confirmDelete() {
  showLoader(true);
  var cid = $('#did').val();
  $.ajax({
    url: page_url + '/' + cid,
    type: 'DELETE',
    dataType: 'json',
    cache: false,
    success: function (result) {
      $('#delete_prompt').modal('hide');
      toastr.success(result.message);
      $('.educationPeriod_listing').DataTable().ajax.reload();
    },
    error: function (jqXHR) {
      if (jqXHR.status == 400) {
        toastr.error(jqXHR.responseJSON.message);
      }
      else {
        toastr.error($('#something_wrong_txt').val());
      }
    }
  });
  showLoader(false);
}

function triggerEdit(cid) {
  showLoader(true);
  $.ajax({
    url: page_url + '/' + cid + '/edit',
    type: 'GET',
    dataType: 'json',
    cache: false,
    success: function (result) {
      $.each(result.data, function (index, value) {
        $("#" + index).val(value);
      });
      $(".show_modal").click();
    },
    error: function (jqXHR) {
      if (jqXHR.status == 400) {
        toastr.error(jqXHR.responseJSON.message);
      }
      else {
        toastr.error($('#something_wrong_txt').val());
      }
    }
  });
  showLoader(false);
}

function triggerDelete(cid) {
  $('#did').val(cid);
  $(".show_delete_modal").click();
}

function changePeriod(cid) {
  $("#cid").val(cid);
  $(".show_period_modal").click();
}

function changeActivePeriod() {
  showLoader(true);
  var cid = $('#cid').val();
  $.ajax({
    url: change_semester_url,
    type: 'POST',
    dataType: 'json',
    cache: false,
    data: { 'cid': cid },
    success: function (result) {
      $('#period_prompt').modal('hide');
      if (result.status) {
        toastr.success(result.message);
        $('.educationPeriod_listing').DataTable().ajax.reload();
      } else {
        toastr.error(result.message);
      }
    },
    error: function (jqXHR) {
      if (jqXHR.status == 400) {
        toastr.error(jqXHR.responseJSON.message);
      }
      else {
        toastr.error($('#something_wrong_txt').val());
      }
    }
  });
}
