/**
* Ministry
*
* This file is used for admin JS
*
* @package    Laravel
* @subpackage JS
* @since      1.0
*/

var page_url = base_url + '/admin/ministries';
var form = $("form[name='add-ministry-form']");

$(function () {

  $("#upload_profile").on('change', function () {
    if (document.getElementById("upload_profile").files.length == 0) {
      $('#user_img').attr('src', $('#img_tmp').val());
    }
    selectProfileImage(this);
  });

  if ($('.datepicker').length) {
    $('.datepicker').datepicker({ format: "dd/mm/yyyy", autoclose: true });
  }

  $('#havent_identification_number').click(function () {
    if ($(this).prop("checked") == true) {
      $("#adm_GovId").val('');
      $('.opt_id').addClass('hide_content');
      $('.opt_tmp_id').removeClass('hide_content');
    }
    else if ($(this).prop("checked") == false) {
      $("#adm_TempGovId").val('');
      $('.opt_tmp_id').addClass('hide_content');
      $('.opt_id').removeClass('hide_content');
    }
  });

});

function save() {
  if (form.valid()) {
    var status = false;
    if ($("#havent_identification_number").prop("checked") == true) {
      status = true;
    }
    else {
      status = checkEmployeeID();
    }
    if (status) {
      showLoader(true);
      var formData = new FormData($(form)[0]);
      if ($('#aid').length) {
        formData.append("id", $('#aid').val());
      }
      $.ajax({
        url: page_url,
        type: 'POST',
        processData: false,
        contentType: false,
        cache: false,
        data: formData,
        success: function (result) {
          toastr.success(result.message);
          setTimeout(() => {
            redirectPage('admin/ministries');
          }, 1000);
        },
        error: function (jqXHR) {
          if (jqXHR.status == 400) {
            toastr.error(jqXHR.responseJSON.message);
          }
          else if (jqXHR.status == 422) {
            setvalidationmessages(jqXHR.responseJSON.errors);
          }
          else {
            toastr.error($('#something_wrong_txt').val());
          }
        }
      });
      showLoader(false);
    }
  }
}

function selectProfileImage(input) {
  if (input.files && input.files[0]) {
    var reader = new FileReader();

    reader.onload = function (e) {
      jQuery('#user_img').attr('src', e.target.result);
    }

    reader.readAsDataURL(input.files[0]);
  }
}

$('#delete_prompt').on('hidden.bs.modal', function () {
  $("#did").val('');
})

$("#search_ministry").on('keyup', function () {
  $('.ministry_listing').DataTable().ajax.reload();
});

function confirmDelete() {
  showLoader(true);
  var cid = $('#did').val();
  $.ajax({
    url: page_url + '/' + cid,
    type: 'DELETE',
    dataType: 'json',
    cache: false,
    success: function (result) {
      $('#delete_prompt').modal('hide');
      toastr.success(result.message);
      $('.ministry_listing').DataTable().ajax.reload();
    },
    error: function (jqXHR) {
      if (jqXHR.status == 400) {
        toastr.error(jqXHR.responseJSON.message);
      }
      else {
        toastr.error($('#something_wrong_txt').val());
      }
    }
  });
  showLoader(false);
}

function triggerDelete(cid) {
  $('#did').val(cid);
  $(".show_delete_modal").click();
}

function checkEmployeeID() {
  var birthDate = $("#adm_DOB").val();
  var gender = $("#adm_Gender").val();
  var employeeId = $('.isValidEmpID').val();
  var status = false;

  if (gender != "" && birthDate != "" && employeeId != "" && employeeId.length == 13) {
    var dd = employeeId.substring(0, 2);
    var mm = employeeId.substring(2, 4);
    var yy = employeeId.substring(4, 7);
    var rr = employeeId.substring(7, 9);
    var bb = employeeId.substring(9, 12);

    var bdata = birthDate.split("/");
    var d = bdata[0];
    var m = bdata[1];
    var y = bdata[2].substring(1, 4);

    if (dd == d && mm == m && yy == y && rr <= 19 && rr >= 10) {
      if (gender == "Male" && bb <= 499 && bb >= 0) {
        status = true;
      }
      else {
        if (gender == "Female" && bb <= 999 && bb >= 500) {
          status = true;
        }
      }
    }
  }

  if (status == false && employeeId != "" && gender != "" && birthDate != "") {
    $('.isValidEmpID').val("");
    toastr.error($("#employeemsgerror").val());
  }

  return status;
}