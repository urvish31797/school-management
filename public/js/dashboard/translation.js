var page_url = base_url + '/admin/translations';
var form = $("form[name='add-translation-form']");

$(function () {

  $("#search_translation").on('keyup', function () {
    $('.translations_listing').DataTable().ajax.reload();
  });

  $('#add_new').on('hidden.bs.modal', function () {
    $("form").trigger("reset");
    $("#id").val('');
    $('#key').attr('readonly', false);
    $('#key').removeClass('input-disabled');
  })

  $('#add_new').on('shown.bs.modal', function () {
    $("form").data('validator').resetForm();
  })

  $('#delete_prompt').on('hidden.bs.modal', function () {
    $("#did").val('');
  });
});

function save()
{
  if(form.valid())
  {
    showLoader(true);
    var formData = new FormData($(form)[0]);
    formData.append("id", $("#id").val());
    $.ajax({
      url: page_url,
      type: 'POST',
      processData: false,
      contentType: false,
      cache: false,
      data: formData,
      success: function (result) {
        toastr.success(result.message);
        $('#add_new').modal('hide');
        $('.translations_listing').DataTable().ajax.reload();
      },
      error: function (jqXHR) {
        if (jqXHR.status == 400) {
          toastr.error(jqXHR.responseJSON.message);
        }
        else if (jqXHR.status == 422) {
          setvalidationmessages(jqXHR.responseJSON.errors);
        }
        else {
          toastr.error($('#something_wrong_txt').val());
        }
      }
    });
    showLoader(false);
  }
}

function confirmDelete() {
  showLoader(true);
  var cid = $('#did').val();
  $.ajax({
    url: page_url + '/' + cid,
    type: 'DELETE',
    dataType: 'json',
    cache: false,
    success: function (result) {
      toastr.success(result.message);
      $('#delete_prompt').modal('hide');
      $('.translations_listing').DataTable().ajax.reload();
    },
    error: function (jqXHR) {
      if (jqXHR.status == 400) {
        toastr.error(jqXHR.responseJSON.message);
      }
      else {
        toastr.error($('#something_wrong_txt').val());
      }
    }
  });
  showLoader(false);
}

function triggerEdit(cid) {
  showLoader(true);
  $.ajax({
    url: page_url + '/' + cid + '/edit',
    type: 'GET',
    dataType: 'json',
    cache: false,
    success: function (result) {
      $.each(result.data, function (index, value) {
        $("#" + index).val(value);
      });
      $('#key').attr('readonly', true);
      $('#key').addClass('input-disabled');
      $(".show_modal").click();
    },
    error: function (jqXHR) {
      if (jqXHR.status == 400) {
        toastr.error(jqXHR.responseJSON.message);
      }
      else {
        toastr.error($('#something_wrong_txt').val());
      }
    }
  });
  showLoader(false);
}

function triggerDelete(cid) {
  $('#did').val(cid);
  $(".show_delete_modal").click();
}

function removeSpace(txt) {
  var str = txt.replace(/ /g, '');
  $('#key').val(str);
}
