var base_url = $('#web_base_url').val();
$(function () {
  if ($('.select2_multi_view').length) {
    $('.select2_multi_view').select2();
  }
  $('.datepicker').datepicker({ format: "dd/mm/yyyy", autoclose: true });

  showLoader(false);
  $('.select2_drop').select2({
    "language": {
      "noResults": function () {
        return $('#no_result_text').val();
      }
    },
    placeholder: $('#select_txt').val(),
    allowClear: true
  });

  $(document).on('change', '.cal_avg', function () {
    var avgSuccess = 0;
    var tmpAvg = 0;
    $('#sem_GeneralSuccess').val(Math.round($(this).val()));
    $(".cal_avg").each(function () {
      if ($(this).val() != '') {
        tmpAvg = tmpAvg + parseInt($(this).val());
      }
    });

    avgSuccess = tmpAvg / $('.cal_avg').length;
    avgSuccess = Math.round(avgSuccess * 10) / 10;
    $('#sem_AvgGeneralSuccess').val(avgSuccess);
    $('#sem_GeneralSuccess').val(Math.round(avgSuccess));
  });

  $('#student_listing').on('processing.dt', function (e, settings, processing) {
    if (processing) {
      showLoader(true);
    } else {
      showLoader(false);
    }
  }).DataTable({
    "columnDefs": [{
      "targets": 4,
      "createdCell": function (td, cellData, rowData, row, col) {
        if (cellData == 'Active') {
          $(td).addClass('active_status');
        } else {
          $(td).addClass('disable_status');
        }
      }
    }],
    "language": {
      "sLengthMenu": $('#show_txt').val() + " _MENU_ " + $('#entries_txt').val(),
      "info": $('#showing_txt').val() + " _START_ " + $('#to_txt').val() + " _END_ " + $('#of_txt').val() + " _TOTAL_ " + $('#entries_txt').val(),
      "emptyTable": $('#msg_no_data_available_table').val(),
      "paginate": {
        "previous": $('#previous_txt').val(),
        "next": $('#next_txt').val()
      }
    },
    "lengthMenu": [10, 20, 30, 50],
    "searching": false,
    "serverSide": true,
    "deferRender": true,
    "ajax": {
      "url": base_url + "/employee/fetch-class-students-list",
      "type": "POST",
      "dataType": 'json',
      "data": function (d) {
        d.search = $('#search_student').val();
        d.pkClr = $('#pkClr').val();
      }
    },
    columns: [
      { "data": "index", className: "text-center" },
      {
        "data": "index", sortable: !1,
        render: function (data, type, mb) {
          if (mb.student.stu_StudentID == '' || mb.student.stu_StudentID == null) {
            return mb.student.stu_TempCitizenId;
          } else {
            return mb.student.stu_StudentID;
          }
        }
      },
      {
        "data": "index", sortable: !1,
        render: function (data, type, mb) {
          return mb.student.stu_StudentName + ' ' + mb.student.stu_StudentSurname;
        }
      },
      {
        "data": "index", sortable: !1,
        render: function (data, type, mb) {
          return mb.grade.gra_GradeNumeric;
        }
      },
      {
        "data": "index", sortable: !1,
        render: function (data, type, mb) {
          return mb.education_program.edp_Name + ' - ' + mb.education_plan.epl_EducationPlanName;
        }
      },
      {
        "data": "cla_ClassName", sortable: !1,
        render: function (data, type, mb) {
          var suid = '';
          if (mb.student.stu_StudentID == '' || mb.student.stu_StudentID == null) {
            suid = mb.student.stu_TempCitizenId;
          } else {
            suid = mb.student.stu_StudentID;
          }
          return '<button type="button" cid="' + mb.pkSte + '" suid="' + suid + '" sname="' + mb.student.stu_StudentName + ' ' + mb.student.stu_StudentSurname + '" gra_name="' + mb.grade.gra_GradeNumeric + '" gra_id="' + mb.grade.pkGra + '" ep_id="' + mb.education_plan.pkEpl + '" ep_name="' + mb.education_program.edp_Name + ' - ' + mb.education_plan.epl_EducationPlanName + '" class="theme_btn min_btn" onclick="addStudent(this)">' + $('#add_txt').val() + '</button>'
        }
      },
    ],

  });

  $("#search_student").on('keyup', function () {
    $('#student_listing').DataTable().ajax.reload()
  });

  $('#add_new').on('hidden.bs.modal', function () {
    $("form").trigger("reset");
    $("#pkCla").val('');
  })

  $('#delete_prompt').on('hidden.bs.modal', function () {
    $("#did").val('');
  })

  $('#add_new').on('shown.bs.modal', function () {
    $("form").data('validator').resetForm();
  })

  $("form[name='final_mark_form']").validate({
    errorClass: "error_msg",
    rules: {
      scr_CertificateNo: {
        required: true,
      },
      scr_Date: {
        required: true,
      },
      fkScrCty: {
        required: true,
      },
      sem_UnjustifiedAbsenceHours: {
        required: true,
      },
      sem_JustifiedAbsenceHours: {
        required: true,
      },
      gan: {
        required: true,
      },
      fkStbSbe: {
        required: true,
      },
      // fkCsaDfm:{
      //   required:true,
      // },
      sem_generalSuccessDescriptiveFinalmarks: {
        required: true,
      }
    },
    errorPlacement: function (error, element) {
      if (element.hasClass('select2_drop') && element.next('.select2-container').length) {
        error.insertAfter(element.next('.select2-container'));
      } else {
        error.insertAfter(element);
      }
    },
    submitHandler: function (form, event) {
      event.preventDefault();
      showLoader(true);
      var formData = new FormData($(form)[0]);
      formData.append("fkClrSch", $("#fkClrSch").val());
      formData.append("pkSem", $("#pkSem").val());
      formData.append("pkCcs", $("#pkCcs").val());
      $.ajax({
        url: base_url + '/employee/finalmarks',
        type: 'POST',
        processData: false,
        contentType: false,
        cache: false,
        data: formData,
        success: function (result) {
          if (result.status) {
            toastr.success(result.message);
            // redirectPage('employee/finalmarks');
            $('.final_mark_redirect').trigger("click");
          } else {
            toastr.error(result.message);
          }

          showLoader(false);
        },
        error: function (data) {
          toastr.error($('#something_wrong_txt').val());
          showLoader(false);
        }
      });

    }
  });

});


function addStudent(elem) {

  if ($('.stu_' + $(elem).attr('cid')).length != 0) {
    toastr.error($('#stu_sel_valid_txt').val());
    return;
  }

  var count = $('.stu').length;
  count = count + 1;

  $('.class_seleted_students tr:last').after('<tr class="stu stu_' + $(elem).attr('cid') + '"><td>' + count + '</td><td>' + $(elem).attr('suid') + '</td><td>' + $(elem).attr('sname') + '</td><td>' + $(elem).attr('gra_name') + '</td><td>' + $(elem).attr('ep_name') + '</td><td><input type="hidden" name="stu_ids[]" value="' + $(elem).attr('gra_id') + '_' + $(elem).attr('cid') + '"><input type="hidden" name="epl_ids[]" value="' + $(elem).attr('ep_id') + '"><button onclick="triggerEduPlan(this)" data-gra="' + $(elem).attr('gra_id') + '" data-stu="' + $(elem).attr('cid') + '" data-edu="' + $(elem).attr('ep_id') + '" type="button" class="theme_btn min_btn btn_margin">' + $('#view_courses_txt').val() + '</button><a data-id="' + $(elem).attr('cid') + '" onclick="removeSelStu(this)" href="javascript:void(0)" class="theme_btn red_btn min_btn">' + $('#delete_txt').val() + '</a></td></tr>');

  $("#eid").val($(elem).attr('emp-id'));
  $(".sel_stu_elem").removeClass('hide_content');
}

function triggerEduPlan(elem) {
  showLoader(true);

  var pkEpl = $(elem).attr('data-edu');
  var pkSen = $(elem).attr('data-stu');
  var pkGra = $(elem).attr('data-gra');
  var pkClr = $('#pkClr').val();
  var pkCcs = $('#pkCcs').val();
  var newData = { 'pkEpl': pkEpl, 'pkSen': pkSen, 'pkClr': pkClr, 'pkGra': pkGra, 'pkCcs': pkCcs }

  $.ajax({
    url: base_url + '/employee/fetchClassEducationPlan',
    type: 'POST',
    data: newData,
    success: function (result) {
      if (result.status) {
        $('.course_details').html(result.course_details);
        $("#trigger_edu_pop").click();
      } else {
        toastr.error(result.message);
      }

      showLoader(false);
    },
    error: function (data) {
      toastr.error($('#something_wrong_txt').val());
      showLoader(false);
    }
  });
}

function removeSelStu(elem) {
  var i = 1;
  var id = $(elem).attr('data-id');
  $('.stu_' + id).remove();

  if ($('.stu').length == 0) {
    $(".sel_stu_elem").addClass('hide_content');
  }

  $.each($('.stu'), function (index, value) {
    $(value).children('td:first').text(i++);
  });
}

function confirmDelete() {
  showLoader(true);
  var cid = $('#did').val();
  $.ajax({
    url: base_url + '/employee/deleteClassCreations',
    type: 'POST',
    dataType: 'json',
    cache: false,
    data: { 'cid': cid },
    success: function (result) {
      $('#delete_prompt').modal('hide');
      if (result.status) {
        toastr.success(result.message);
        $('#class_creation_semester_listing').DataTable().ajax.reload();
      } else {
        toastr.error($('#something_wrong_txt').val());
      }

      showLoader(false);
    },
    error: function (data) {
      toastr.error($('#something_wrong_txt').val());
      showLoader(false);
    }
  });
}


function triggerDelete(cid) {
  $('#did').val(cid);
  $(".show_delete_modal").click();
}

function printCertificate() {
  var data = { "pkSem": $("#pkSem").val() };
  var url = "/employee/generate-certificate";
  commonAjax(data, url, successCertificateResponse, errorCertificateResponse);
}

function successCertificateResponse(result) {
  if (result.status) {
    window.open(result.filepath);
  }
  else {
    toastr.error(result.message);
  }
}

function errorCertificateResponse() {
  toastr.error($('#something_wrong_txt').val());
}

$('#manual_dfm_check').click(function () {
  $('#fkCsaDfm').val('');
  $('#sem_generalSuccessDescriptiveFinalmarks').val('');
  if ($(this).prop("checked") == true) {
    $('#manual_dfm').removeClass('hide_content');
    $('#drp_dfm').addClass('hide_content');
  }
  else if ($(this).prop("checked") == false) {
    $('#manual_dfm').addClass('hide_content');
    $('#drp_dfm').removeClass('hide_content');
  }
});

$('.dfm_check').click(function () {
  // $('#fkCsaDfm').val('');
  $('#csa_ManualDescriptMarks_' + $(this).attr('data-dfm')).val();
  if ($(this).prop("checked") == true) {
    $('#manual_dfm_' + $(this).attr('data-dfm')).removeClass('hide_content');
    $('#dropdown_dfm_' + $(this).attr('data-dfm')).addClass('hide_content');
    // $('#drp_dfm').addClass('hide_content');
  }
  else if ($(this).prop("checked") == false) {
    $('#manual_dfm_' + $(this).attr('data-dfm')).addClass('hide_content');
    $('#dropdown_dfm_' + $(this).attr('data-dfm')).removeClass('hide_content');
    // $('#drp_dfm').removeClass('hide_content');
  }
});

// Class Creation listing Delete
function triggerDeleteClsCre(cid) {
  $('#did').val(cid);
  $(".show_delete_modal").click();
}

function openFinalMarks(id) {
  $.ajax({
    url: base_url + '/employee/finalmarks/' + id + '/edit',
    type: 'GET',
    dataType: 'json',
    success: function (result) {
      if (result.status !== undefined) {
        toastr.error(result.message);
      } else {
        window.location.href = base_url + '/employee/finalmarks/' + id;
      }

      showLoader(false);
    },
    error: function (data) {
      toastr.error($('#something_wrong_txt').val());
      showLoader(false);
    }
  });
}

function printFinalMarks(id) {
  var data = { "pkSem": id };
  var url = "/employee/generate-certificate";
  commonAjax(data, url, successCertificateResponse, errorCertificateResponse);
}
