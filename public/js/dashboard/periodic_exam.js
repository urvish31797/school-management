var page_url = base_url + '/employee/periodic-exam';
var gradesId = [];

$(function () {
    $("#pex_Date").datepicker({
        format: "dd/mm/yyyy",
        autoclose: true,
        startDate: '-2d',
        endDate: "today"
    });

    $("#search_periodic_exam").on('keyup', function () {
        $('.periodic_exam_listing').DataTable().ajax.reload()
    });

    $(".all_grades").each(function (index, obj) {
        gradesId.push(obj.value);
    });

    $('#students_listing').on('processing.dt', function (e, settings, processing) {
        if (processing) {
            showLoader(true);
        } else {
            showLoader(false);
        }
    }).DataTable({
        "language": {
            "sLengthMenu": $('#show_txt').val() + " _MENU_ " + $('#entries_txt').val(),
            "info": $('#showing_txt').val() + " _START_ " + $('#to_txt').val() + " _END_ " + $('#of_txt').val() + " _TOTAL_ " + $('#entries_txt').val(),
            "emptyTable": $('#msg_no_data_available_table').val(),
            "paginate": {
                "previous": $('#previous_txt').val(),
                "next": $('#next_txt').val()
            }
        },
        "lengthMenu": [100, 200, 300, 500],
        "searching": false,
        "serverSide": true,
        "deferRender": true,
        "ajax": {
            "url": class_stu_url,
            "type": "POST",
            "dataType": 'json',
            "data": function (d) {
                d.search = $('#search_student').val();
                d.grades = gradesId;
            }
        },
        'createdRow': function (row, type, index) {
            $(row).attr('id', index + 1);
        },
        columnDefs: [{
            orderable: false,
            className: '',
            targets: 0
        }],
        select: {
            style: 'multi',
            selector: 'td:first-child'
        },
        buttons: [
            'selectAll',
            'selectNone'
        ],
        "order": [[1, 'asc']],
        dom: 'Bfrtip',
        columns: [
            {
                "data": "stu_StudentName",
                render: function (row, type, stu) {
                    var checkbox = '<div class="form-group form-check">' + stu.hidden_checkbox + '<input type="checkbox" class="sr_checkbox form-check-input" id=' + stu.index + '><label class="custom_checkbox"></label><label class="form-check-label label-text" for= "exampleCheck1"></label></div>';
                    return checkbox;
                }
            },
            {
                "data": "stu_StudentName", sortable: !1,
                render: function (data, type, stu) {
                    return '<span title=' + stu.studentId + '>' + stu.stu_StudentName + '</span>';
                }
            },
            { "data": "gra_GradeNumeric" },
            { "data": "numeric_mark", sortable: !1 },
            { "data": "descriptive_final_mark", sortable: !1 },
            { "data": "exam_points_number", sortable: !1 },
        ],

    });

    $("#select-all").click(function () {
        if ($(this).prop("checked")) {

            $('#delete_btn_div').show();
            $("input:checkbox").prop('checked', true);
        }
        else {

            $('#delete_btn_div').hide();
            $("input:checkbox").prop('checked', false);
        }
    });

    $("form[name='add-periodic-exam-form']").validate({
        errorClass: "error_msg",
        ignore: [],
        rules: {
            pex_Date: {
                required: true
            },
            fkPexPew: {
                required: true
            },
            fkPexPet: {
                required: true,
            },
            pex_ExamNumericMark: {
                required: true,
            },
            pex_ExamContent: {
                required: true,
            },
            fkPexDpem: {
                required: true,
            },
        },
        submitHandler: function (form, event) {
            event.preventDefault();

            showLoader(true);
            var formData = new FormData($(form)[0]);
            // $('input[name="hour' + lecture_number + '[]"]:unchecked').map(function () {
            //     formData.append('absent_students[]', this.value);
            // });

            $.ajax({
                url: page_url,
                type: 'POST',
                processData: false,
                contentType: false,
                cache: false,
                data: formData,
                success: function (result) {
                    toastr.success(result.message);
                    setTimeout(() => {
                        redirectPage("employee/periodic-exam");
                    }, 1200);
                },
                error: function (jqXHR) {
                    if (jqXHR.status == 400) {
                        toastr.error(jqXHR.responseJSON.message);
                    }
                    else {
                        toastr.error($('#something_wrong_txt').val());
                    }
                }
            });
            showLoader(false);
        }
    });
});

function triggerDelete(cid) {
    $('#did').val(cid);
    $(".show_delete_modal").click();
}

function selected_delete() {
    var chk = [];
    var allchk = $('.sr_checkbox').length;
    $('.sr_checkbox').each(function () {
        if (this.checked) {
            chk.push(true);
        }
    });
    console.log(chk.length);
    if (allchk == chk.length) {
        toastr.error($("#msg_cannot_delete_all_students").val());
        chk = [];
    }
    else {
        $('.sr_checkbox').each(function () {
            if (this.checked) {
                $('#' + this.id).remove();
            }
        });
    }
    $("input:checkbox").prop('checked', false);
}

function confirmDelete() {
    showLoader(true);
    var cid = $('#did').val();
    $.ajax({
        url: page_url + "/" + cid,
        type: 'DELETE',
        dataType: 'json',
        cache: false,
        success: function (result) {
            $('#confirm_delete_prompt').modal('hide');
            toastr.success(result.message);
            $('.periodic_exam_listing').DataTable().ajax.reload();
        },
        error: function (jqXHR) {
            if (jqXHR.status == 400) {
                toastr.error(jqXHR.responseJSON.message);
            }
            else {
                toastr.error($('#something_wrong_txt').val());
            }
        }
    });
    showLoader(false);
}