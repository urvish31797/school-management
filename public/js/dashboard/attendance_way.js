var page_url = base_url + '/admin/attendanceway';
var form = $("form[name='add-attendance-way-form']");

$(function () {

  $("#search_attendance_way").on('keyup', function () {
    $('.attendance_way_listing').DataTable().ajax.reload()
  });

  $('#add_new').on('hidden.bs.modal', function () {
    var validator = $("form").validate();
    validator.resetForm();
    $("form").trigger("reset");
    $("#pkAw").val('');
  });

  $('#add_new').on('shown.bs.modal', function () {
    $("form").data('validator').resetForm();
  });

  $('#delete_prompt').on('hidden.bs.modal', function () {
    $("#did").val('');
  });

});

function save() {
  if (form.valid()) {
    showLoader(true);
    var formData = new FormData($(form)[0]);
    formData.append("pkAw", $("#pkAw").val());
    $.ajax({
      url: page_url,
      type: 'POST',
      processData: false,
      contentType: false,
      cache: false,
      data: formData,
      success: function (result) {
        toastr.success(result.message);
        $('#add_new').modal('hide');
        $('.attendance_way_listing').DataTable().ajax.reload();
      },
      error: function (jqXHR) {
        if (jqXHR.status == 400) {
          toastr.error(jqXHR.responseJSON.message);
        }
        else if (jqXHR.status == 422) {
          setvalidationmessages(jqXHR.responseJSON.errors);
        }
        else {
          toastr.error($('#something_wrong_txt').val());
        }
      }
    });
    showLoader(false);
  }
}

function confirmDelete() {
  showLoader(true);
  var cid = $('#did').val();
  $.ajax({
    url: page_url + '/' + cid,
    type: 'DELETE',
    dataType: 'json',
    cache: false,
    success: function (result) {
      $('#delete_prompt').modal('hide');
      toastr.success(result.message);
      $('.attendance_way_listing').DataTable().ajax.reload();
    },
    error: function (jqXHR) {
      if (jqXHR.status == 400) {
        toastr.error(jqXHR.responseJSON.message);
      }
      else {
        toastr.error($('#something_wrong_txt').val());
      }
    }
  });
  showLoader(false);
}

function triggerEdit(cid) {
  showLoader(true);
  $.ajax({
    url: page_url + '/' + cid + '/edit',
    type: 'GET',
    dataType: 'json',
    cache: false,
    success: function (result) {
      $.each(result.data, function (index, value) {
        $("#" + index).val(value);
      });
      $(".show_modal").click();
    },
    error: function (jqXHR) {
      if (jqXHR.status == 400) {
        toastr.error(jqXHR.responseJSON.message);
      }
      else {
        toastr.error($('#something_wrong_txt').val());
      }
    }
  });
  showLoader(false);
}

function confirmchange() {
  showLoader(true);
  var id = $('#default_id').val();
  $.ajax({
    url: change_url,
    type: 'POST',
    dataType: 'json',
    cache: false,
    data: { 'id': id },
    success: function (result) {
      $('#default_prompt').modal('hide');
      toastr.success(result.message);
      $('.attendance_way_listing').DataTable().ajax.reload();
    },
    error: function (jqXHR) {
      if (jqXHR.status == 400) {
        toastr.error(jqXHR.responseJSON.message);
      }
      else {
        toastr.error($('#something_wrong_txt').val());
      }
    }
  });
  showLoader(false);
}

function triggerDelete(pkAw) {
  $('#did').val(pkAw);
  $(".show_delete_modal").click();
}

function changeDefaultModal(id) {
  $("#default_id").val(id);
  $(".show_default_modal").click();
}
