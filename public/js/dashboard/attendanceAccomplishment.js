/**
* Attendance & Accomplishment
*
* This file is used for admin JS
*
* @package    Laravel
* @subpackage JS
* @since      1.0
*/
var gradesId = [];
var courseId = "";
var classLabelId = "";
var page_url = base_url + '/employee/attendance-accomplishment';
var today_date = '';
var tomorrow_date = '';

$(function () {
  $("#dbk_Date").datepicker({
    format: "dd/mm/yyyy",
    autoclose: true,
    startDate: '-2d',
    endDate: "today"
  });

  var currentdate = new Date();
  var today = currentdate.toJSON().slice(0,10);
  currentdate.setDate(currentdate.getDate() + 1);
  tomorrow = currentdate.toJSON().slice(0, 10);
  today_date = reformatDate(today);
  tomorrow_date = reformatDate(tomorrow);

  $("#course_history_search").on('keyup', function () {
    triggerCoursePrompt(false);
  });

  $(".courseunitnumber_input").on('blur', function () {
    var inputId = $(this).attr('id').split("_");
    var id = inputId[2];

    if ($(this).val() != "") {
      $("#sya_CourseContent_" + id).html($(this).val());
    }
    else {
      $("#sya_CourseContent_" + id).html("");
    }
  });

  $('#students_listing').on('processing.dt', function (e, settings, processing) {
    if (processing) {
      showLoader(true);
    } else {
      showLoader(false);
    }
  }).DataTable({
    "language": {
      "sLengthMenu": $('#show_txt').val() + " _MENU_ " + $('#entries_txt').val(),
      "info": $('#showing_txt').val() + " _START_ " + $('#to_txt').val() + " _END_ " + $('#of_txt').val() + " _TOTAL_ " + $('#entries_txt').val(),
      "emptyTable": $('#msg_no_data_available_table').val(),
      "paginate": {
        "previous": $('#previous_txt').val(),
        "next": $('#next_txt').val()
      }
    },
    "lengthMenu": [100, 200, 300, 500],
    "searching": false,
    "serverSide": true,
    "deferRender": true,
    "ajax": {
      "url": class_stu_url,
      "type": "POST",
      "dataType": 'json',
      "data": function (d) {
        d.search = $('#search_student').val();
        d.grades = gradesId;
        d.lecture_number = $("#dbl_LectureNumber").val();
      }
    },
    columns: [
      { "data": "index", className: "text-center" },
      {
        "data": "stu_StudentName", sortable: !1,
        render: function (data, type, stu) {
          return '<span title=' + stu.studentId + '>' + stu.stu_StudentName + '</span>';
        }
      },
      { "data": "gra_GradeNumeric" },
      { "data": "hour1", sortable: !1 },
      { "data": "hour2", sortable: !1 },
      { "data": "hour3", sortable: !1 },
      { "data": "hour4", sortable: !1 },
      { "data": "hour5", sortable: !1 },
      { "data": "hour6", sortable: !1 },
      { "data": "hour7", sortable: !1 },
    ],

  });

  $('#attendance_listing').on('processing.dt', function (e, settings, processing) {
    if (processing) {
      showLoader(true);
    } else {
      showLoader(false);
    }
  }).DataTable({
    "language": {
      "sLengthMenu": $('#show_txt').val() + " _MENU_ " + $('#entries_txt').val(),
      "info": $('#showing_txt').val() + " _START_ " + $('#to_txt').val() + " _END_ " + $('#of_txt').val() + " _TOTAL_ " + $('#entries_txt').val(),
      "emptyTable": $('#msg_no_data_available_table').val(),
      "paginate": {
        "previous": $('#previous_txt').val(),
        "next": $('#next_txt').val()
      }
    },
    "lengthMenu": [100, 200, 300, 500],
    "searching": false,
    "serverSide": true,
    "deferRender": true,
    "ajax": {
      "url": attendance_url,
      "type": "POST",
      "dataType": 'json',
      "data": function (d) {
        d.search = $('#search_attendance').val();
      }
    },
    columns: [
      { "data": "index", className: "text-center" },
      { "data": "shiftName", sortable: !1 },
      { "data": "dbk_Date" },
      { "data": "lectureNumber", sortable: !1 },
      { "data": "villageSchoolName", sortable: !1 },
      {
        "data": "pkDbk", sortable: !1,
        render: function (data, type, attendance) {
          
          if(today_date == attendance.dbk_Date || tomorrow_date == attendance.dbk_Date){
            var row = '<div class="btn-group" role="group">' +
            '<button id="btnGroupDrop1" type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">' + $("#actions_txt").val() + '</button>' +
            '<div class="dropdown-menu" aria-labelledby="btnGroupDrop1">' +
            '<a class="dropdown-item" title="' + $('#view_detail_text').val() + '" href="attendance-accomplishment/' + attendance.pkDbl + '"><i class="fa fa-list-alt" aria-hidden="true"></i>&nbsp;' + $('#view_detail_text').val() + '</a>' +
            '<a class="dropdown-item" title="' + $('#gn_edit').val() + '" href="attendance-accomplishment/' + attendance.pkDbl + '/edit"><i class="fa fa-pencil-alt" aria-hidden="true"></i>&nbsp;' + $('#gn_edit').val() + '</a>' +
            '<a class="dropdown-item" title="' + $('#student_behaviour_label').val() + '" href="student-behaviour-notes/' + attendance.pkDbl + '/edit"><i class="fa fa-plus" aria-hidden="true"></i>&nbsp;' + $('#student_behaviour_label').val() + '</a>' +
            '<a class="dropdown-item" onclick="triggerDelete(' + attendance.pkDbl + ')" href="javascript:void(0)"><i class="fa fa-trash" aria-hidden="true"></i>&nbsp;' + $('#gn_delete').val() + '</a>' +
            '</div>' +
            '</div>';
          } else {
            var row = '<div class="btn-group" role="group">' +
            '<button id="btnGroupDrop1" type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">' + $("#actions_txt").val() + '</button>' +
            '<div class="dropdown-menu" aria-labelledby="btnGroupDrop1">' +
            '<a class="dropdown-item" title="' + $('#view_detail_text').val() + '" href="attendance-accomplishment/' + attendance.pkDbl + '"><i class="fa fa-list-alt" aria-hidden="true"></i>&nbsp;' + $('#view_detail_text').val() + '</a>' +
            '<a class="dropdown-item" title="' + $('#student_behaviour_label').val() + '" href="student-behaviour-notes/' + attendance.pkDbl + '/edit"><i class="fa fa-plus" aria-hidden="true"></i>&nbsp;' + $('#student_behaviour_label').val() + '</a>' +
            '<a class="dropdown-item" onclick="triggerDelete(' + attendance.pkDbl + ')" href="javascript:void(0)"><i class="fa fa-trash" aria-hidden="true"></i>&nbsp;' + $('#gn_delete').val() + '</a>' +
            '</div>' +
            '</div>';
          }

          return row;
        }
      },
    ],
  });

  $('#lecture_detail_listing').on('processing.dt', function (e, settings, processing) {
    if (processing) {
      showLoader(true);
    } else {
      showLoader(false);
    }
  }).DataTable({
    "language": {
      "sLengthMenu": $('#show_txt').val() + " _MENU_ " + $('#entries_txt').val(),
      "info": $('#showing_txt').val() + " _START_ " + $('#to_txt').val() + " _END_ " + $('#of_txt').val() + " _TOTAL_ " + $('#entries_txt').val(),
      "emptyTable": $('#msg_no_data_available_table').val(),
      "paginate": {
        "previous": $('#previous_txt').val(),
        "next": $('#next_txt').val()
      }
    },
    "lengthMenu": [100, 200, 300, 500],
    "searching": false,
    "serverSide": true,
    "deferRender": true,
    "ajax": {
      "url": class_attendance_detail_url,
      "type": "POST",
      "dataType": 'json',
      "data": function (d) {
        d.search = $('#search_student').val();
        d.pkDbl = $("#pkDbl").val();
      }
    },
    columns: [
      { "data": "index", className: "text-center" },
      { "data": "studentId" },
      { "data": "stu_StudentName" },
      { "data": "hour", sortable: !1 },
      { "data": "sae_StudentTDesc", sortable: !1 },
      { "data": "sae_StudentHTDesc", sortable: !1 },
      { "data": "sae_LecutreStudentStatus", sortable: !1 },
    ],

  });

  $("#search_attendance").on('keyup', function () {
    $('#attendance_listing').DataTable().ajax.reload();
  });

  $("#dbl_LectureNumber").on('change', function () {
    var hour = $("#dbl_LectureNumber").val();
    if (hour != "") {
      for (var i = 1; i <= 7; i++) {
        if (hour != i) {
          $(".check_all_" + i).attr("disabled", true);
          $(".check_all_" + i).attr("checked", false);
        }
        else {
          $(".check_all_" + hour).attr("disabled", false);
          $(".check_all_" + hour).attr("checked", true);
        }
      }
    }
    else {
      for (var i = 1; i <= 7; i++) {
        $(".check_all_" + i).attr("disabled", true);
        $(".check_all_" + i).attr("checked", false);
      }
    }
  });

  $("form[name='add-attend-accom-form']").validate({
    errorClass: "error_msg",
    ignore: [],
    rules: {
      sya_CourseUnitNumber: {
        required: true
      },
      sya_CourseContent: {
        required: true
      },
      dbk_Date: {
        required: true,
      },
      fkDblEw: {
        required: true,
      },
      fkDblAw: {
        required: true,
      },
      dbl_LectureNumber: {
        required: true,
      }
    },
    submitHandler: function (form, event) {
      event.preventDefault();
      var lecture_number = $("#dbl_LectureNumber").val();

      if ($("#student_listing_div").attr('class').split(" ").includes("hide_content")) {
        toastr.error($("#no_students_error_msg").val());
        return;
      }

      if ($(".check_all_" + lecture_number + ":checked").length == 0) {
        toastr.error($("#no_students_error_msg").val());
        return;
      }

      showLoader(true);
      var formData = new FormData($(form)[0]);
      $('input[name="hour' + lecture_number + '[]"]:unchecked').map(function () {
        formData.append('absent_students[]', this.value);
      });

      $.ajax({
        url: page_url,
        type: 'POST',
        processData: false,
        contentType: false,
        cache: false,
        data: formData,
        success: function (result) {
          if (result.status) {
            toastr.success(result.message);
            redirectPage("employee/attendance-accomplishment");
          } else {
            toastr.error(result.message);
          }
        },
        error: function () {
          toastr.error($('#something_wrong_txt').val());
        }
      });
      showLoader(false);
    }
  });

  $("form[name='edit-attend-accom-form']").validate({
    errorClass: "error_msg",
    ignore: [],
    rules: {
      dbk_Date: {
        required: true,
      },
      dbl_LectureNumber: {
        required: true
      },
      fkDblEw: {
        required: true,
      },
      fkDblAw: {
        required: true,
      },
    },
    submitHandler: function (form, event) {
      event.preventDefault();
      var lecture_number = $("#dbl_LectureNumber").val();

      if ($(".check_all_" + lecture_number + ":checked").length == 0) {
        toastr.error($("#no_students_error_msg").val());
        return;
      }

      showLoader(true);
      var formData = new FormData($(form)[0]);
      $('input[name="hour' + lecture_number + '[]"]:unchecked').map(function () {
        formData.append('absent_students[]', this.value);
      });

      $.ajax({
        url: page_url,
        type: 'POST',
        processData: false,
        contentType: false,
        cache: false,
        data: formData,
        success: function (result) {
          if (result.status) {
            toastr.success(result.message);
            redirectPage('employee/attendance-accomplishment');
          } else {
            toastr.error(result.message);
          }
        },
        error: function (data) {
          toastr.error($('#something_wrong_txt').val());
        }
      });
      showLoader(false);
    }
  });
});

function reformatDate(dateStr)
{
  dArr = dateStr.split("-");
  return dArr[2]+ "/" +dArr[1]+ "/" +dArr[0];
}

function fetchCourseHours(course_id, grade_id) {

  showLoader(true);
  $.ajax({
    url: fetch_course_hours_url,
    type: 'POST',
    dataType: 'JSON',
    cache: false,
    data: { "course_id": course_id, "grade_id": grade_id, "classDetail": $("#classDetail").val() },
    success: function (result) {
      $("#prescribed_hours_" + grade_id).html(result.prescribed_total_hrs);
      $("#hour_number_" + grade_id).html(result.running_class_hour);
      $("#sya_LectureOrderNumber_" + grade_id).val(result.running_class_hour);
    },
    error: function () {
      toastr.error($('#something_wrong_txt').val());
    }
  });
  showLoader(false);
}

function triggerDelete(cid) {
  $('#did').val(cid);
  $(".show_delete_modal").click();
}

function removeGrade(gradeId) {
  $('#delete_gradeid').val(gradeId);
  $(".delete_grade_modal").click();
}

function confirmDeleteGrade() {
  var id = $("#delete_gradeid").val();
  if ($(".grade_rows").length == 1) {
    toastr.error($("#msg_cannot_delete_entry").val());
    $('#delete_grade_prompt').modal('hide');
  }
  else {
    $('#grade_row_' + id).remove();
    $('#delete_grade_prompt').modal('hide');
  }
}

function confirmDelete() {
  showLoader(true);
  var cid = $('#did').val();
  $.ajax({
    url: page_url + '/' + cid,
    type: 'DELETE',
    dataType: 'json',
    cache: false,
    success: function (result) {
      $('#delete_prompt').modal('hide');
      if (result.status) {
        toastr.success(result.message);
        $('#attendance_listing').DataTable().ajax.reload();
      } else {
        toastr.error(result.message);
      }
    },
    error: function (data) {
      toastr.error($('#something_wrong_txt').val());
    }
  });
  showLoader(false);
}

function triggerCoursePrompt(isfirstTime, gradeId) {
  showLoader(true);
  if (isfirstTime) {
    courseId = $("#courses_" + gradeId).val();
    classLabelId = $("#class_" + gradeId).val();
    $("#temp_classlableId").val(classLabelId);
    $("#temp_courseId").val(courseId);
    $("#temp_gradeId").val(gradeId);
  }

  if (courseId == "" || classLabelId == "" || gradeId === undefined) {
    classLabelId = $("#temp_classlableId").val();
    courseId = $("#temp_courseId").val();
    gradeId = $("#temp_gradeId").val();
  }

  $.ajax({
    url: previous_unit_history_url,
    type: 'POST',
    dataType: 'JSON',
    cache: false,
    data: { 'search': $("#course_history_search").val(), 'classLabel': classLabelId, 'gradeId': gradeId, 'courseId': courseId },
    success: function (result) {
      if (result.status) {
        $(".course_details").html("");
        $(".course_details").html(result.data);
      } else {
        toastr.error(result.message);
      }
    },
    error: function (data) {
      toastr.error($('#something_wrong_txt').val());
    }
  });
  showLoader(false);

  if (isfirstTime) {
    $(".show_modal").click();
  }

}

function getStudentsList() {
  gradesId = [];
  $(".all_grades").each(function (index, obj) {
    gradesId.push(obj.value);
  });
  $("#student_listing_div").removeClass('hide_content');
  $('#students_listing').DataTable().ajax.reload();
  // $("#get_students_btn_div").hide();
}