
var update_profile_url = base_url + '/admin/edit-profile';
var update_password_url = base_url + '/change-password-submit';

$(function () {

  showLoader(false);

  $("#upload_profile").on('change', function () {
    if (document.getElementById("upload_profile").files.length == 0) {
      $('#user_img').attr('src', $('#img_tmp').val());
    }
    selectProfileImage(this);
  });

  $('#change_pass').on('hidden.bs.modal', function () {
    var validator = $("form[name='change-password-form']").validate();
    validator.resetForm();
    $("form").trigger("reset");
  });

  $('#havent_identification_number').click(function () {
    if ($(this).prop("checked") == true) {
      $("#adm_GovId").val('');
      $('.opt_id').addClass('hide_content');
      $('.opt_tmp_id').removeClass('hide_content');
    }
    else if ($(this).prop("checked") == false) {
      $("#adm_TempGovId").val('');
      $('.opt_tmp_id').addClass('hide_content');
      $('.opt_id').removeClass('hide_content');
    }
  });

  $('.datepicker').datepicker({ format: "dd/mm/yyyy", autoclose: true, endDate: '+0d', });

  $("form[name='loginForm']").validate({
    // Specify validation rules
    rules: {
      name: {
        required: true,
      },
      email: {
        required: true,
        email: true
      },
    },
    submitHandler: function (form) {
      form.submit();
    }
  });

  $("form[name='edit-profile']").validate({
    errorClass: "error_msg",
    rules: {
      email: {
        required: true,
        email: true,
        emailfull: true
      },
      name: {
        required: true,
        minlength: 5
      },
      govt_id: {
        required: true,
        minlength: 13,
        maxlength: 13
      },
      adm_TempGovId: {
        required: true,
        minlength: 13,
        maxlength: 13
      },
      title: {
        required: true,
        minlength: 5
      },
    },
    submitHandler: function (form, event) {
      event.preventDefault();
      showLoader(true);
      var formData = new FormData($(form)[0]);
      $.ajax({
        url: update_profile_url,
        type: 'POST',
        processData: false,
        contentType: false,
        cache: false,
        data: formData,
        success: function (result) {
          toastr.success(result.message);
          redirectPage("admin/profile");
          $('.profile-cover img').attr('src', $('#user_img').attr('src'));
        },
        error: function (jqXHR) {
          if (jqXHR.status == 400) {
            toastr.error(jqXHR.responseJSON.message);
          }
          else if (jqXHR.status == 422) {
            setvalidationmessages(jqXHR.responseJSON.errors);
          }
          else {
            toastr.error($('#something_wrong_txt').val());
          }
        }
      });
      showLoader(false);
    }
  });

  jQuery.validator.addMethod("emailfull", function (value, element) {
    return this.optional(element) || /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i.test(value);
  }, $('#email_validate_txt').val());

  //Validate form for Change Password
  $("form[name='change-password-form']").validate({
    errorClass: "error_msg",
    rules: {
      old_password: {
        required: true,
        minlength: 6,
        maxlength: 15
      },
      new_password: {
        required: true,
        minlength: 6,
        maxlength: 15,
        passwordCheck: true
      },
      confirm_password: {
        required: true,
        equalTo: "#new_password",
        minlength: 6,
        maxlength: 15
      },
    },
    messages: {
      confirm_password: {
        equalTo: $('#validate_password_equalto_txt').val(),
      },
    },
    submitHandler: function (form, event) {
      event.preventDefault();
      showLoader(true);
      var formData = new FormData($(form)[0]);
      $.ajax({
        url: update_password_url,
        type: 'POST',
        processData: false,
        contentType: false,
        cache: false,
        data: formData,
        success: function (result) {
          if (result.status) {
            showMessage(result.message, true);
            window.location.href = result.redirect;
          } else {
            showMessage(result.message, false);
          }

          showLoader(false);
        },
        error: function (data) {
          showMessage(result.message, false);
          showLoader(false);
        }
      });
    }
  });
});

jQuery.validator.addMethod("passwordCheck",
  function (value, element, param) {
    if (this.optional(element)) {
      return true;
    } else if (!/[A-Z]/.test(value)) {
      return false;
    } else if (!/[a-z]/.test(value)) {
      return false;
    } else if (!/[0-9]/.test(value)) {
      return false;
    } else if (!/[\+\-\_\@\#\$\%\&\*\!]/.test(value)) {
      return false;
    }

    return true;
  },
  $('#validate_password_txt').val());

//Validate file image or not
/*$(document).on('change','input[type="file"]',function(){
  var file = document.getElementById($(this).attr('id')).files[0];
    if(file && (file['type'] == "image/jpeg" || file['type'] == "image/png" || file['type'] == "application/jpg" || file['type'] == "application/pdf")){
      $(this).closest('form').find(':submit').attr('disabled',false);
      $(this).next('.errorImage').html('');
    }else{
      $(this).next('.errorImage').html('Please upload only .jpg, .jpeg, .svg, .png format').css('color','red');
      $(this).closest('form').find(':submit').attr('disabled',true);

    }
});*/

function selectProfileImage(input) {
  if (input.files && input.files[0]) {
    var reader = new FileReader();

    reader.onload = function (e) {
      jQuery('#user_img').attr('src', e.target.result);
    }

    reader.readAsDataURL(input.files[0]);
  }
}

function checkEmployeeID() {
  var birthDate = $("#adm_DOB").val();
  var gender = $("#adm_Gender").val();
  var employeeId = $('.isValidEmpID').val();
  var status = false;

  if (gender != "" && birthDate != "" && employeeId != "" && employeeId.length == 13) {
    var dd = employeeId.substring(0, 2);
    var mm = employeeId.substring(2, 4);
    var yy = employeeId.substring(4, 7);
    var rr = employeeId.substring(7, 9);
    var bb = employeeId.substring(9, 12);

    var bdata = birthDate.split("/");
    var d = bdata[0];
    var m = bdata[1];
    var y = bdata[2].substring(1, 4);

    if (dd == d && mm == m && yy == y && rr <= 19 && rr >= 10) {
      if (gender == "Male" && bb <= 499 && bb >= 0) {
        status = true;
      }
      else {
        if (gender == "Female" && bb <= 999 && bb >= 500) {
          status = true;
        }
      }
    }
  }

  if (status == false && employeeId != "" && gender != "" && birthDate != "") {
    $('.isValidEmpID').val("");
    toastr.error($("#employeemsgerror").val());
  }

  return status;
}