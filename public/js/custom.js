var imagepath = $('#web_base_url').val() + '/images/';

$('#preloader').css('display', '');
$(window).on('load', function () {
    $('#preloader').css('display', 'none');
    $('#preloader').css('opacity', '0');
    $('#contents').css('opacity', '1');
});

$(document).ready(function () {
    jQuery('form').attr('autocomplete', 'off');
    jQuery('.custom-drop-down').append('<div class="button"></div>');
    jQuery('.custom-drop-down').append('<ul class="select-list"></ul>');
    jQuery('.custom-drop-down select option').each(function () {
        var bg = jQuery(this).css('background-image');
        jQuery('.select-list').append('<li class="clsAnchor"><span value="' + jQuery(this).val() + '" class="' + jQuery(this).attr('class') + '" style=background-image:' + bg + '>' + jQuery(this).text() + '</span></li>');
    });
    jQuery('.custom-drop-down .button').html('<span class="custom_flag" style=background-image:' + jQuery('.custom-drop-down select').find(':selected').css('background-image') + '>' + jQuery('.custom-drop-down select').find(':selected').text() + '</span>' + '<a href="javascript:void(0);" class="select-list-link"><i class="fa fa-caret-down" aria-hidden="true"></i></a>');
    jQuery('.custom-drop-down ul li').each(function () {
        if (jQuery(this).find('span').text() == jQuery('.custom-drop-down select').find(':selected').text()) {
            jQuery(this).addClass('active');
        }
    });
    jQuery('.custom-drop-down .select-list span').on('click', function () {
        var dd_text = jQuery(this).text();
        var dd_img = jQuery(this).css('background-image');
        var dd_val = jQuery(this).attr('value');
        jQuery('.custom-drop-down .button').html('<span class="custom_flag" style=background-image:' + dd_img + '>' + dd_text + '</span>' + '<a href="javascript:void(0);" class="select-list-link"><i class="fa fa-caret-down" aria-hidden="true"></i></a>');
        jQuery('.custom-drop-down .select-list span').parent().removeClass('active');
        jQuery(this).parent().addClass('active');
        $('.custom-drop-down select').val(dd_val).trigger('change');
        $('.custom-drop-down select[name=options]').val(dd_val);
        $('.custom-drop-down .select-list li').slideUp();
        //$('.custom-drop-down .select-list li').hide();
    });
    jQuery('.custom-drop-down .button').on('click', 'a.select-list-link', function () {
        jQuery('.custom-drop-down ul li').slideToggle();
        //jQuery('.custom-drop-down ul li').toggle();
    });

    $('#sidebarCollapse').on('click', function () {
        var hidden = $('#sidebar');
        var hidden1 = $('.overlay');
        // $('#sidebar').toggleClass('active');
        if (hidden.hasClass('visible')) {
            hidden.animate({ "right": "-2500px" }, 500).removeClass('visible');
            $(".overlay").css('display', 'none');
        } else {
            // $('#sidebar').toggleClass('active');
            hidden.animate({ "right": "0px" }, 500).addClass('active');
            hidden1.fadeIn(500);
            // $( "body" ).addClass( "noscroll" );
        }
    });

    $('.overlay').click(function () {
        var hidden = $('#sidebar');
        var hidden1 = $('.overlay');
        hidden.animate({ "right": "-250px" }, 500).removeClass('visible');
        hidden1.fadeOut(500);
        // $( "body" ).removeClass( "noscroll" );
    });
});

$(document).on('click', '.remove_scroll', function () {
    $('body').removeClass('no_scroll');
});

$(document).ready(function () {

    $('.components li.active .list-unstyled').prev('a').children('.cfa').attr('class', "cfa fas fa-chevron-up right-arrow");

    $('.list-unstyled').on('hide.bs.collapse', function () {
        $(this).prev('a').children('.cfa').remove();
        $(this).prev('a').children('img:last').after('<i class="cfa fas fa-chevron-down right-arrow"></i>')
    });

    $('.list-unstyled').on('show.bs.collapse', function () {
        $(this).prev('a').children('.cfa').remove();
        $(this).prev('a').children('img:last').after('<i class="cfa fas fa-chevron-down right-arrow"></i>')
    });

    $('html').click(function (e) {
        //if clicked element is not your element and parents aren't your div
        if (e.target.id != 'custom-flag-drop-down' && $(e.target).parents('#custom-flag-drop-down').length == 0) {
            $('.clsAnchor').hide();
        }
    });

    // $(document).on('click', '.ajax_request', function (e) {
    //     var base_url = $('#web_base_url').val();
    //     e.preventDefault();
    //     showLoader(true);
    //     var currentRequest = null;
    //     var slug = $(this).attr('data-slug');
    //     var url = $(this).attr('href');

    //     $.ajax({
    //         url: url,
    //         type: 'GET',
    //         dataType: 'json',
    //         beforeSend: function () {
    //             if (currentRequest != null) {
    //                 currentRequest.abort();
    //             }
    //         },
    //         success: function (result) {
    //             if (result) {
    //                 // $("#content").remove();
    //                 $(".section").remove();
    //                 // $(result.content).insertAfter("#sidebar");
    //                 $(result.content).insertAfter(".navbar");
    //                 var web_url = base_url + '/' + slug;
    //                 ChangeUrl(slug, web_url);
    //                 document.title = result.title.replace("&amp;", "&") + " - Hertronic";
    //                 //$('#datatable_script').attr('src',result.data.script);
    //                 $("#dataTable_script").empty();
    //                 if (result.script) {
    //                     appendScript(result.script);
    //                 }

    //             } else {
    //                 //toastr.error(result.message);
    //             }
    //             if (!result.script) {
    //                 showLoader(false);
    //             }
    //             showLoader(false);
    //         },
    //         error: function (jqXHR, textStatus, errorThrown) {
    //             if (jqXHR.status == 401) {
    //                 toastr.error(errorThrown);
    //                 setTimeout(function () {
    //                     window.location.href = base_url;
    //                 }, 1000);
    //             }
    //             else {
    //                 toastr.error('Something went wrong');
    //                 showLoader(false);
    //             }
    //         }
    //     });

    //     if (!$(this).hasClass("no_sidebar_active")) {
    //         $('.components li').removeClass('active');
    //         // $('.components li .list-unstyled').prev('a').children('.cfa').attr('class',"cfa fas fa-chevron-down right-arrow");
    //         // $('.components li').attr("aria-expanded",false);
    //         $('.list-unstyled').removeClass('show');
    //         //$(".custom_ap").append('<i class="fas fa-chevron-up right-arrow"></i>');
    //         $('#pageSubmenu1 li').removeClass('active');
    //         $(this).parent('li').addClass('active');
    //         $(this).closest('ul').parent('li').addClass('active');
    //         $('.components li.active .list-unstyled').prev('a').children('.cfa').attr('class', "cfa fas fa-chevron-up right-arrow");
    //         //$(this).closest('ul').parent('li').children("i").attr("class", "fas fa-chevron-up right-arrow");
    //         $(this).closest('ul').addClass('show');
    //     }

    // });
});

// responsive menu
$(document).ready(function () {

    /* range slider with updated value*/
    $('.range_val').on('input change', '.range_val', function () {
        var range = $(this).val();
        $(this).next('p.value').html(range + ' Star');
    })

    /* setting edit profile */
    $(document).on('click', '#edit_profile', function () {
        $("#edit_profile_detail").css('display', 'block');
        $("#profile_detail").css('display', 'none');
    });

    $(document).on('click', '#cancel_edit_profile', function () {
        $("#edit_profile_detail").css('display', 'none');
        $("#profile_detail").css('display', 'block');
    });

    /* setting edit profile */
    $(document).on('click', "#edit_profile2", function () {
        $("#edit_profile_detail2").css('display', 'block');
        $("#profile_detail2").css('display', 'none');
    });

    $(document).on('click', '#cancel_edit_profile2', function () {
        $("#edit_profile_detail2").css('display', 'none');
        $("#profile_detail2").css('display', 'block');
    });

    $(document).on('click', '#edit_profile3', function () {
        $("#edit_profile_detail3").css('display', 'block');
        $("#profile_detail3").css('display', 'none');
    });

    $(document).on('click', '#cancel_edit_profile3', function () {
        $("#edit_profile_detail3").css('display', 'none');
        $("#profile_detail3").css('display', 'block');
    });

    $(document).on('click', '#edit_profile4', function () {
        $("#edit_profile_detail4").css('display', 'block');
        $("#profile_detail4").css('display', 'none');
    });

    $(document).on('click', '#cancel_edit_profile4', function () {
        $("#edit_profile_detail4").css('display', 'none');
        $("#profile_detail4").css('display', 'block');
    });

    $(".numbersonly").bind("keyup keypress blur change keydown paste", function (event) {
        // Prevent shift key since its not needed
        if (event.shiftKey == true) {
            event.preventDefault();
        }
        // Allow Only: keyboard 0-9, numpad 0-9, backspace, tab, left arrow, right arrow, delete
        if ((event.keyCode >= 48 && event.keyCode <= 57) || (event.keyCode >= 96 && event.keyCode <= 105) || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 37 || event.keyCode == 39 || event.keyCode == 46) {
            // Allow normal operation
        } else {
            // Prevent the rest
            event.preventDefault();
        }
    });


    $('#slide').click(function () {
        var hidden = $('.sideoff-off');
        var hidden1 = $('.overlay');
        if (hidden.hasClass('visible')) {
            hidden.animate({ "right": "-1300px" }, 500).removeClass('visible');
            $(".overlay").css('display', 'none');
        } else {
            hidden.animate({ "right": "0px" }, 500).addClass('visible');
            hidden1.fadeIn(500);
            // $( "body" ).addClass( "noscroll" );
        }
    });

    $('.navbar-nav a.nav-link').click(function () {
        var hidden = $('.sideoff-off');
        var hidden1 = $('.overlay');
        hidden.animate({ "right": "-1300px" }, 500).removeClass('visible');
        hidden1.fadeOut(500);
        // $( "body" ).removeClass( "noscroll" );
    });

    $("#add_qualification").click(function () {
        var txtNewInputBox = document.createElement('div');
        txtNewInputBox.innerHTML = "<div class='mt-3'>" + $("#qualification .single_qualification").html() + "</div>";
        document.getElementById("qualification").appendChild(txtNewInputBox);
    });
});

$(window).scroll(collapseNavbar);
$(document).ready(collapseNavbar);

function appendScript(url) {
    var script = document.createElement('script');
    script.type = 'text/javascript';
    script.src = url;

    $("#dataTable_script").append(script);
}

function ChangeUrl(page, url) {
    if (typeof (history.pushState) != "undefined") {
        var obj = { Page: page, Url: url };
        history.pushState(obj, obj.Page, obj.Url);
    } else {
        alert("Browser does not support HTML5.");
    }
}

function langSwitch(val) {
    showLoaderFull(true);
    $.ajax({
        url: base_url + '/switch-language',
        data: {
            "lang": val,
        },
        type: 'POST',
        success: function (data) {
            showLoaderFull(false);
            if (data.status == true) {
                location.reload();
            }
        },
        error: function (jqXhr, textStatus, errorThrown) {

        }
    });
}

function roleSwitch() {
    showLoaderFull(true);
    $.ajax({
        url: '/employee/switch-role',
        data: {
            "role": $("#role_drop_down").find(':selected').val(),
            "sid": $("#role_drop_down").find(':selected').attr('data-sch'),
            "eid": $("#role_drop_down").find(':selected').attr('data-eid')
        },
        type: 'POST',
        success: function (data) {
            showLoaderFull(false);
            if (data.status == true) {
                window.location.href = data.redirect;
            }
        },
        error: function (jqXhr, textStatus, errorThrown) {

        }
    });
}

function roleEmployeeSwitch() {
    showLoaderFull(true);
    var role = $("#role_drop_down").find(':selected').val();
    var school = $("#role_drop_down").find(':selected').attr('data-sch');
    var eid = $("#role_drop_down").find(':selected').attr('data-eid');
    var hid = $("#role_drop_down").find(':selected').attr('data-hid');
    var csemid = $("#role_drop_down").find(':selected').attr('data-csemid');
    var eptid = $("#role_drop_down").find(':selected').attr('data-eptid');
    var year = $("#year_drop_down").val();
    var schoolyear = $("#year_drop_down").find(':selected').val();
    var semesterId = $("#sem_drop_down").find(':selected').val();
    var classId = $("#class_drop_down").find(':selected').val();
    var courseGroup = $("#class_drop_down").find(':selected').data('coursegroup');
    var subclass = $("#class_drop_down").find(':selected').data('subclass');
    var ccsgroups = $("#class_drop_down").find(':selected').data('ccsgroups');
    var villageschool = $("#class_drop_down").find(':selected').data('villageschool');
    var courseid = $("#class_drop_down").find(':selected').data('courseid');
    var courseLabel = $("#class_drop_down").find(':selected').data('label');
    var shiftId = $("#shift_dropdown").find(':selected').val();

    $.ajax({
        url: base_url + '/employee/switch-role',
        data: {
            "role": role,
            "sid": school,
            "eid": eid,
            "hid": hid,
            "csemid": csemid,
            "eptid": eptid,
            "year": year,
            "schoolyear": schoolyear,
            "semesterId": semesterId,
            "classId": classId,
            "courseId": courseid,
            "courseGroup": courseGroup,
            "subClass": subclass,
            "ccsgroups": ccsgroups,
            "villageSchool": villageschool,
            "shiftId": shiftId,
            "courseLabel": courseLabel
        },
        type: 'POST',
        success: function (data) {
            showLoaderFull(false);
            if (data.status == true) {
                window.location.href = data.redirect;
            }
        },
        error: function (jqXhr, textStatus, errorThrown) {

        }
    });
}

function redirectPage(url) {
    window.location.href = base_url + "/" + url;
}

function closeOverlay() {
    var hidden = $('.sideoff-off');
    var hidden1 = $('.overlay');
    hidden.animate({ "right": "-1000px" }, 500).removeClass('visible');
    hidden1.fadeOut(500);
    // $( "body" ).removeClass( "noscroll" );
}
// navbar fixed on top
// jQuery to collapse the navbar on scroll
function collapseNavbar() {
    if ($(".navbar").length) {
        if ($(".navbar").offset().top > 50) {
            $(".fixed-top").addClass("top-nav-collapse");
        } else {
            $(".fixed-top").removeClass("top-nav-collapse");
        }
    }
}

function showLoader($show) {
    if ($show) {
        $('#preloader_new').show();
        $('#preloader_new').css('opacity', 1);
    } else {
        $('#preloader_new').hide();
        $('#preloader_new').css('opacity', 0);
    }
}

function showLoaderFull($show) {
    if ($show) {
        $('#preloader').show();
        $('#preloader').css('opacity', 1);
    } else {
        $('#preloader').hide();
        $('#preloader').css('opacity', 0);
    }
}

function showMessage($msg, $type) {
    if ($type) {
        $('.custom_error_msg').text('');
        $('.custom_success_msg').text($msg);
    } else {
        $('.custom_error_msg').text($msg);
        $('.custom_success_msg').text('');
    }
}

function commonAjax(data, url, success, error) {
    showLoader(true);
    $.ajax({
        type: "POST",
        url: base_url + url,
        data: data,
        success: function (result) {
            success(result);
            showLoader(false);
        }, error: function () {
            error();
            showLoader(false);
        }
    });
}

function setvalidationmessages(response) {
    toastr.error($("#given_data_invalid_txt").val());
    $.each(response, function (index, value) {
        var element = $("#" + index);
        $(element).parent().removeClass('has-success').addClass('has-error');
        $("#" + index + '-error').html(value[0]);
    });
}

$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});