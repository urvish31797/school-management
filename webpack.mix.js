const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.styles([
    'public/css/bootstrap.min.css',
    'public/css/toastr.min.css',
    'public/css/jquery.dataTables.min.css',
    'public/plugins/select2/select2.min.css',
    'public/css/msdropdown/dd.min.css',
    'public/plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.css'
], 'public/css/theme_components.min.css')
    .options({
        processCssUrls: false
    });

mix.styles([
    'public/css/custom.css',
    'public/css/custom-rt.css',
], 'public/css/custom.min.css')
    .options({
        processCssUrls: false
    });

mix.styles([
    'public/css/bootstrap.min.css',
    'public/css/msdropdown/dd.min.css'
], 'public/css/login.min.css')
    .options({
        processCssUrls: false
    });

mix.scripts([
    'public/js/all.min.js',
    'public/js/popper.min.js',
    'public/js/bootstrap.min.js',
    'public/js/toastr.min.js',
    'public/plugins/jquery-slimscroll/jquery.slimscroll.min.js',
    'public/js/jquery.dataTables.min.js',
    'public/js/jquery.validate.min.js',
    'public/js/custom_validation_msg.js',
    'public/plugins/select2/select2.full.min.js',
    'public/plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js',
    'public/js/msdropdown/jquery.dd.min.js'
], 'public/js/theme_components.min.js');

mix.scripts([
    'public/js/custom.js',
], 'public/js/custom.min.js');

mix.scripts([
    'public/js/all.min.js',
    'public/js/bootstrap.min.js',
    'public/js/jquery.validate.js',
    'public/js/custom_validation_msg.js',
    'public/js/msdropdown/jquery.dd.min.js',
], 'public/js/login.min.js');