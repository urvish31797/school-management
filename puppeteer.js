const fs = require('fs')
const path = require('path')
const utils = require('util')
const puppeteer = require('puppeteer')
const hb = require('handlebars')
const readFile = utils.promisify(fs.readFile)

const url = process.argv[2];
const save_url = process.argv[3];
const filename = process.argv[4];

// return;
if (!url) {
    throw "Please provide URL";
    console.log(false);
}

async function getTemplateHtml() {

    console.log("Loading template file in memory")
    try {
        const invoicePath = path.resolve("./invoice.html");
        return await readFile(invoicePath, 'utf8');
    } catch (err) {
        return Promise.reject("Could not load html template");
    }
}


async function generatePdf() {

    let data = {};

    /*getTemplateHtml()
        .then(async (res) => {*/
            // Now we have the html code of our template in res object
            // you can check by logging it on console
            // console.log(res)

            const template = hb.compile('res', { strict: true });
            // we have compile our code with handlebars
            // const result = template(data);
            // We can use this to add dyamic data to our handlebas template at run time from database or API as per need. you can read the official doc to learn more https://handlebarsjs.com/
            // const html = result;
            // const url='http://gaudeamus.xyz/';
            // we are using headless mode 
            const browser = await puppeteer.launch({
                // executablePath:path.resolve(__dirname,'/node_modules/puppeteer/.local-chromium/linux-650583/chrome-linux/chrome'),
                headless: true,args: ['--no-sandbox', '--disable-setuid-sandbox']
            });
            const page = await browser.newPage()

            // We set the page content as the generated html by handlebars
            // await page.setContent(html)
            await page.goto(url,{waitUntil: 'networkidle2'});    

            await page.waitFor(200);

            const height = await page.evaluate(
                () => document.documentElement.offsetHeight
            );
            // console.log("height",height);
            // height=height-50;
            // console.log("height new",height);

            // we Use pdf function to generate the pdf in the same folder as this file.
            // await page.pdf({ path: 'brochure.pdf', format: 'A4',printBackground: true });
            await page.pdf({ path: save_url ,preferCSSPageSize: true, printBackground: true,margin: {top: "0cm",right: "0cm", bottom: "0cm", left: "0cm"}});

            await browser.close();
            console.log(true);
            console.log(save_url);
            console.log(filename);

        /*})
        .catch(err => {
            console.error(err)
        });*/
}

generatePdf();