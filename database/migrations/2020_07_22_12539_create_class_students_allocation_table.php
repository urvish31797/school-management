<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClassStudentsAllocationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ClassStudentsCourseAllocation', function (Blueprint $table) {
            $table->integer('pkCsa')->autoIncrement();
            $table->integer('fkCsaSem')->nullable();
            $table->integer('fkCsaEmc')->nullable();
            $table->integer('fkCsaScg')->nullable();
            $table->integer('fkCsaCga')->nullable();
            $table->integer('fkCsaDfm')->nullable();
            $table->enum('csa_FinalMarks', ['1', '2', '3', '4', '5'])->nullable();
            $table->string('csa_AveragePeriodic', 50)->nullable();
            $table->string('csa_ManualDescriptMarks', 255)->nullable();
            $table->softDeletes('deleted_at', 0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ClassStudentsCourseAllocation');
    }
}
