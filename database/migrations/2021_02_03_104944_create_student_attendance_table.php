<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStudentAttendanceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
    */
    public function up()
    {
        Schema::create('DailyStudentAttendance', function (Blueprint $table) {
            $table->bigIncrements('pkSae');
            $table->integer('fkSaeSya')->nullable()->index()->comment = "DailySyllabusAccomplishment";
            $table->integer('fkSaeSem')->nullable()->index()->comment = "ClassStudentsSemester";
            $table->integer('fkSaeCsa')->nullable()->index()->comment = "ClassStudentsCourseAllocation";
            $table->enum('sae_LectureAttendance', array('P','A'))->default('A');
            $table->enum('sae_LecutreStudentStatus', array('Justified','Unjustified','Pending'))->default('Pending');
            $table->string('sae_StudentTDesc',100)->nullable();
            $table->string('sae_StudentHTDesc',100)->nullable();
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrent();
            $table->softDeletes('deleted_at', 0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
    */
    public function down()
    {
        Schema::dropIfExists('DailyStudentAttendance');
    }
}
