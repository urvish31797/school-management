<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWorkingWeeksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('WorkingWeeks', function (Blueprint $table) {
            $table->integer('pkWek',11)->autoIncrement();
            $table->string('wek_weekName_en')->nullable();
            $table->string('wek_weekName_sr')->nullable();
            $table->string('wek_weekName_ba')->nullable();
            $table->string('wek_weekName_hr')->nullable();
            $table->string('wek_notes',1000)->nullable();
            $table->softDeletes('deleted_at', 0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('WorkingWeeks');
    }
}
