<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCertificateBluePrintsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('CertificateBluePrints', function (Blueprint $table) {
            $table->integer('pkCbp')->autoIncrement();
            $table->string('cbp_BluePrintName_en', 60)->nullable();
            $table->string('cbp_BluePrintName_sr', 60)->nullable();
            $table->string('cbp_BluePrintName_ba', 60)->nullable();
            $table->string('cbp_BluePrintName_hr', 60)->nullable();
            $table->longText('cbp_htmlContent_en')->nullable();
            $table->longText('cbp_htmlContent_sr')->nullable();
            $table->longText('cbp_htmlContent_ba')->nullable();
            $table->longText('cbp_htmlContent_hr')->nullable();
            $table->string('cbp_keywords', 1000)->nullable();
            $table->string('cbp_Cbpstatus',10)->nullable()->default("Active");
            $table->dateTime('cbp_startdate')->nullable();
            $table->dateTime('cbp_enddate')->nullable();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('CertificateBluePrints');
    }
}
