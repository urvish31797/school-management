<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePeriodicExamsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('PeriodicExams', function (Blueprint $table) {
            $table->bigIncrements('pkPex');
            $table->integer('fkPexEdp')->nullable()->index()->comment = "Education Period";
            $table->integer('fkPexPet')->nullable()->index()->comment = "Periodic Exam Type";
            $table->integer('fkPexEen')->nullable()->index()->comment = "Employee Engagement";
            $table->integer('fkPexPew')->nullable()->index()->comment = "Periodic Exam Ways";
            $table->integer('fkPexCsa')->nullable()->index()->comment = "Class Student Course Allocation";
            $table->integer('fkPexSem')->nullable()->index()->comment = "Class Student Semester";
            $table->integer('fkPexDpem')->nullable()->index()->comment = "Descriptive periodic exam mark";
            $table->date('pex_Date')->nullable();
            $table->string('pex_ExamContent')->nullable();
            $table->string('pex_ExamNumericMark')->nullable();
            $table->string('pex_ExamPointsNumber')->nullable();
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrent();
            $table->softDeletes('deleted_at', 0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('PeriodicExams');
    }
}
