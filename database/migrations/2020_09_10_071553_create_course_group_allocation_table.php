<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCourseGroupAllocationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('CourseGroupAllocation', function (Blueprint $table) {
            $table->integer('pkCga',11)->autoIncrement();
            $table->string('cga_Uid',50)->nullable();
            $table->integer('fkCgaSch',11)->nullable();
            $table->integer('fkCgaViSch',11)->nullable();
            $table->integer('fkCgaSye',11)->nullable();
            $table->integer('fkCgaEdu',11)->nullable();
            $table->integer('fkCgaCrs',11)->nullable();
            $table->integer('fkCgaGpg',11)->nullable();
            $table->integer('fkCgaFcg',11)->nullable();
            $table->integer('fkCgaFlg',11)->nullable();
            $table->integer('fkCgaOcg',11)->nullable();
            $table->string('cga_WeekHours',50)->nullable();
            $table->enum('cga_OrderNumber', ['1', '2', '3', '4', '5'])->nullable();
            $table->softDeletes('deleted_at', 0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('CourseGroupAllocation');
    }
}
