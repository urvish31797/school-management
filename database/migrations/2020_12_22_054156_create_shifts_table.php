<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShiftsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Shifts', function (Blueprint $table) {
            $table->integer('pkShi')->autoIncrement();
            $table->string('shi_ShiftName_en', 20)->nullable();
            $table->string('shi_ShiftName_sr', 20)->nullable();
            $table->string('shi_ShiftName_ba', 20)->nullable();
            $table->string('shi_ShiftName_hr', 20)->nullable();
            $table->string('shi_ShiftNote', 100)->nullable();
            $table->softDeletes('deleted_at', 0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Shifts');
    }
}
