<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePeriodicExamWayTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('PeriodicExamWay', function (Blueprint $table) {
            $table->integer('pkPew')->autoIncrement();
            $table->string('pew_Uid', 5)->nullable();
            $table->string('pew_Name_en', 200)->nullable();
            $table->string('pew_Name_sr', 200)->nullable();
            $table->string('pew_Name_ba', 200)->nullable();
            $table->string('pew_Name_hr', 200)->nullable();
            $table->tinyInteger('pew_Default')->default('0');
            $table->string('pew_Note', 200)->nullable();
            $table->timestamp('deleted_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('PeriodicExamWay');
    }
}
