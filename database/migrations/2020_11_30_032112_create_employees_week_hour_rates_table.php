<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployeesWeekHourRatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('EmployeesWeekHourRates', function (Blueprint $table) {
            $table->integer('pkEwh')->autoIncrement();
            $table->integer('fkEwhEen')->nullable();
            $table->date('ewh_StartDate')->nullable();
            $table->date('ewh_EndDate')->nullable();
            $table->float('ewh_WeeklyHoursRate')->nullable();
            $table->text('ewh_Notes')->nullable();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('EmployeesWeekHourRates');
    }
}