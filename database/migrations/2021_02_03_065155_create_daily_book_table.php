<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDailyBookTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('DailyBook', function (Blueprint $table) {
            $table->bigIncrements('pkDbk');
            $table->integer('fkDbkSye')->nullable()->index()->comment = "SchoolYear";
            $table->integer('fkDbkVsc')->nullable()->index()->comment = "VillageSchool";
            $table->integer('fkDbkShi')->nullable()->index()->comment = "Shift";
            $table->integer('fkDbkCcs')->nullable()->index()->comment = "ClassCreationSemester";
            $table->date('dbk_Date')->nullable();
            $table->enum('dbk_Verified', array('YES', 'NO'))->default('NO');
            // $table->tinyInteger('dbk_Locked')->default(0);
            $table->string('dbk_Notes')->nullable();
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrent();
            $table->softDeletes('deleted_at', 0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('DailyBook');
    }
}
