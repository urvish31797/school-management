<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSchoolYearTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('school_year', function (Blueprint $table) {
            $table->increments('pkSye');
            $table->tinyInteger('sye_DefaultYear',1)->default('0');
            $table->string('sye_Uid')->nullable();
            $table->string('sye_NameCharacter_en')->nullable();
            $table->year('sye_NameNumeric')->nullable();
            $table->string('sye_ShortName')->nullable();
            $table->softDeletes('deleted_at', 0);
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('school_year');
    }
}
