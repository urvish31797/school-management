<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSyllabusAccomplishment extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('DailySyllabusAccomplishment', function (Blueprint $table) {
            $table->bigIncrements('pkSya');
            $table->integer('fkSyaDbl')->nullable()->index()->comment = "Dailybook Lecture";
            $table->integer('fkSyaCrs')->nullable()->index()->comment = "Course";
            $table->integer('fkSyaScg')->nullable()->index()->comment = "Subclassgroup";
            $table->integer('fkSyaCga')->nullable()->index()->comment = "Course Group";
            $table->integer('fkSyaEen')->nullable()->index()->comment = "Employee Engagement";
            $table->integer('fkSyaGra')->nullable()->index()->comment = "Grade";
            $table->integer('fkSyaCla')->nullable()->index()->comment = "Class";
            $table->integer('sya_LectureOrderNumber')->nullable();
            $table->string('sya_CourseUnitNumber',100)->nullable();
            $table->string('sya_CourseContent',1000)->nullable();
            // $table->string('sya_Notes',500)->nullable();
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrent();
            $table->softDeletes('deleted_at', 0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('DailySyllabusAccomplishment');
    }
}
