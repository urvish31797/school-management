<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClassTeachersAndCourseAllocationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ClassTeachersCourseAllocation', function (Blueprint $table) {
            $table->integer('pkCtc')->autoIncrement();
            $table->integer('fkCtcCsa')->nullable();
            $table->integer('fkCtcEmc')->nullable();
            $table->integer('fkCtcCcs')->nullable();
            $table->integer('fkCtcEeg')->nullable();
            $table->timestamp('Ctc_start')->nullable();
            $table->timestamp('Ctc_end')->nullable();
            $table->softDeletes('deleted_at', 0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ClassTeachersCourseAllocation');
    }
}
