<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDailyBookLectureTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('DailyBookLecture', function (Blueprint $table) {
            $table->bigIncrements('pkDbl');
            $table->integer('fkDblDbk')->nullable()->index()->comment = "DailyBook";
            $table->integer('fkDblEw')->nullable()->index()->comment = "Education Way";
            $table->integer('fkDblAw')->nullable()->index()->comment = "Attendance Way";
            $table->enum('dbl_LectureNumber',array(1,2,3,4,5,6,7));
            // $table->integer('dbl_LectureOrderNumber')->nullable();
            $table->string('dbl_Notes',1000)->nullable();
            $table->string('dbl_HomeroomTeacherNotes',1000)->nullable();
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrent();
            $table->softDeletes('deleted_at', 0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('DailyBookLecture');
    }
}
