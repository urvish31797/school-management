<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClassSubClassGroupTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('SubClassGroups', function (Blueprint $table) {
            $table->integer('pkScg',11)->autoIncrement();
            $table->string('scg_Uid',30)->nullable();
            $table->string('scg_Name_en',100)->nullable();
            $table->text('scg_Notes')->nullable();
            $table->softDeletes('deleted_at', 0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('SubClassGroups');
    }
}
