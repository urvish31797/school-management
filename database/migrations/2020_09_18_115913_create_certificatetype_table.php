<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCertificateTypeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('CertificateType', function (Blueprint $table) {
            $table->integer('pkCty')->autoIncrement();
            $table->string('cty_Uid', 30)->nullable();
            $table->string('cty_Name_en', 200)->nullable();
            $table->string('cty_Name_sr', 200)->nullable();
            $table->string('cty_Name_ba', 200)->nullable();
            $table->string('cty_Name_hr', 200)->nullable();
            $table->timestamp('deleted_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('CertificateType');
    }
}
