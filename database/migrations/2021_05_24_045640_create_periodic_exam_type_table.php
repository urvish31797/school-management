<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePeriodicExamTypeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('PerodicExamType', function (Blueprint $table) {
            $table->integer('pkPet')->autoIncrement();
            $table->string('pet_Name_en', 200)->nullable();
            $table->string('pet_Name_sr', 200)->nullable();
            $table->string('pet_Name_ba', 200)->nullable();
            $table->string('pet_Name_hr', 200)->nullable();
            $table->string('pet_Note', 200)->nullable();
            $table->timestamp('deleted_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('PerodicExamType');
    }
}
