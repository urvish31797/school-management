<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SchoolYearWeekNumber extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('SchoolYearWeekNumber', function (Blueprint $table) {
            $table->integer('pkSyw')->autoIncrement();
            $table->integer('fkSywSye')->nullable();
            $table->integer('fkSywCan')->nullable();
            $table->tinyInteger('sye_NumberofWeek')->nullable();
            $table->softDeletes('deleted_at', 0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('SchoolYearWeekNumber');
    }
}
