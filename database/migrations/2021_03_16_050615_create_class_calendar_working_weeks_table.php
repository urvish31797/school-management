<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClassCalendarWorkingWeeksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ClassCalendarWorkingWeeks', function (Blueprint $table) {
            $table->bigIncrements('pkCcw');
            $table->integer('fkCcwShi')->index()->comment = "Shift";
            $table->integer('fkCcwSye')->index()->comment = "School Year";
            $table->integer('fkCcwEen')->index()->comment = "Employee Engagement";
            $table->integer('fkCcwCcs')->index()->comment = "Class Creation Semester";
            $table->integer('fkCcwWek')->index()->comment = "Working Weeks";
            $table->integer('fkCcwSteOne')->index()->comment = "Student Enrollment One";
            $table->integer('fkCcwSteTwo')->index()->comment = "Student Enrollment Two";
            $table->date('ccw_startDate')->nullable();
            $table->date('ccw_endDate')->nullable();
            $table->string('ccw_notes',1000)->nullable();
            $table->timestamps();
            $table->softDeletes('deleted_at', 0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ClassCalendarWorkingWeeks');
    }
}
