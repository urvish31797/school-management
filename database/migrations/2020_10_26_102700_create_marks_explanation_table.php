<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMarksExplanationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('MarksExplanation', function (Blueprint $table) {
            $table->integer('pkMe')->autoIncrement();
            $table->integer('me_NumericExplanationMark')->nullable();
            $table->string('me_Explanation_en', 30)->nullable();
            $table->string('me_Explanation_sr', 30)->nullable();
            $table->string('me_Explanation_ba', 30)->nullable();
            $table->string('me_Explanation_hr', 30)->nullable();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('MarksExplanation');
    }
}
