<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDescriptiveFinalMarksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('DescriptiveFinalMarks', function (Blueprint $table) {
            $table->integer('pkDfm')->autoIncrement();
            $table->string('dfm_Uid', 30)->nullable();
            $table->string('dfm_Text', 255)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('DescriptiveFinalMarks');
    }
}
