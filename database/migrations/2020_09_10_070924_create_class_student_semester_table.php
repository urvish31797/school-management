<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClassStudentSemesterTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ClassStudentsSemester', function (Blueprint $table) {
            $table->integer('pkSem',11)->autoIncrement();
            $table->integer('fkSemCcs',11)->nullable();
            $table->integer('fkSemSen',11)->nullable();
            $table->integer('sem_GeneralSuccess',1)->nullable();
            $table->string('sem_AvgGeneralSuccess',30)->nullable();
            $table->integer('sem_UnjustifiedAbsenceHours',3)->nullable();
            $table->integer('sem_JustifiedAbsenceHours',3)->nullable();
            $table->integer('fkSemGan',11)->nullable();
            $table->integer('fkCcaDfm',11)->nullable();
            $table->string('sem_generalSuccessDescriptiveFinalmarks',100)->nullable();
            // $table->string('sem_attempts_en',30)->nullable();
            $table->softDeletes('deleted_at', 0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ClassStudentsSemester');
    }
}
