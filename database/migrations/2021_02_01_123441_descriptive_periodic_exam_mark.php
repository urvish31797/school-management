<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DescriptivePeriodicExamMark extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('DescriptivePeriodicExamMark', function (Blueprint $table) {
            $table->integer('pkDpem')->autoIncrement();
            $table->string('dm_Uid', 5)->nullable();
            $table->string('dm_Name_en', 200)->nullable();
            $table->string('dm_Name_sr', 200)->nullable();
            $table->string('dm_Name_ba', 200)->nullable();
            $table->string('dm_Name_hr', 200)->nullable();
            $table->string('dm_Note', 200)->nullable();
            $table->timestamp('deleted_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('DescriptivePeriodicExamMark');
    }
}
