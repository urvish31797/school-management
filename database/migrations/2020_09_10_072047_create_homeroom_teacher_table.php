<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHomeroomTeacherTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('HomeRoomTeacher', function (Blueprint $table) {
            $table->integer('pkHrt')->autoIncrement();
            $table->integer('fkHrtEen')->nullable();
            $table->integer('fkHrtClr')->nullable();
            $table->dateTime('hrt_start_date')->nullable();
            $table->dateTime('hrt_end_date')->nullable();
            $table->text('hrt_Notes')->nullable();
            $table->softDeletes('deleted_at', 0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('HomeRoomTeacher');
    }
}
