<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCourseOrderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('CoursesOrders', function (Blueprint $table) {
            $table->integer('pkCor')->autoIncrement();
            $table->integer('fkCorSch')->nullable();
            $table->integer('fkCorEdp')->nullable();
            $table->integer('fkCorEpl')->nullable();
            $table->string('cor_Uid', 50)->nullable();
            $table->softDeletes('deleted_at', 0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('CoursesOrders');
    }
}
