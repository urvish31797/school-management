<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGradeAttemptsNumberTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('GradeAttemptsNumber', function (Blueprint $table) {
            $table->integer('pkGan')->autoIncrement();
            $table->string('gan_Uid', 30)->nullable();
            $table->string('gan_Name_en', 200)->nullable();
            $table->string('gan_Name_sr', 200)->nullable();
            $table->string('gan_Name_ba', 200)->nullable();
            $table->string('gan_Name_hr', 200)->nullable();
            $table->timestamp('deleted_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('GradeAttemptsNumber');
    }
}
