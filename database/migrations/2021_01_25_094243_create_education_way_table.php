<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEducationWayTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('EducationWay', function (Blueprint $table) {
            $table->integer('pkEw')->autoIncrement();
            $table->string('ew_Uid', 5)->nullable();
            $table->string('ew_Name_en', 200)->nullable();
            $table->string('ew_Name_sr', 200)->nullable();
            $table->string('ew_Name_ba', 200)->nullable();
            $table->string('ew_Name_hr', 200)->nullable();
            $table->tinyInteger('ew_Default')->default('0');
            $table->string('ew_Note', 200)->nullable();
            $table->timestamp('deleted_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('EducationWay');
    }
}
