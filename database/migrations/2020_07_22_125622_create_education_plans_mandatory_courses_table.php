<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEducationPlansMandatoryCoursesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('EducationPlansMandatoryCourses', function (Blueprint $table) {
            $table->integer('pkEmc')->autoIncrement();
            $table->integer('fkEmcEpl')->nullable();
            $table->integer('fkEplCrs')->nullable();
            $table->integer('fkEmcGra')->nullable();
            $table->integer('emc_hours',3)->nullable();
            $table->integer('emc_weekHourRateTeacher',3)->nullable();
            $table->integer('emc_summerHolidayHourRate',3)->nullable();
            $table->enum('emc_default', ['Yes', 'No'])->default('No');
            $table->softDeletes('deleted_at', 0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('EducationPlansMandatoryCourses');
    }
}
