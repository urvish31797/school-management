<?php
/**
* EducationPlan 
*
* Model for EducationPlan Table
* 
* @package    Laravel
* @subpackage Model
* @since      1.0
*/


namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class EducationPlan extends Model
{
	use SoftDeletes;
    protected $table = 'EducationPlans';
    public $timestamps = false;   

    public function mandatoryCourse()
    {
        return $this->hasMany(EducationPlansMandatoryCourse::class, 'fkEmcEpl', 'pkEpl');
    }

    public function groupedByGrades()
    {
        return $this->mandatoryCourse()->groupBy(['fkEmcGra']);
    }

    public function optionalCourse()
    {
        return $this->hasMany(EducationPlansOptionalCourse::class, 'fkEocEpl', 'pkEpl');
    }

     public function courseOrder()
    {
        return $this->hasMany(CourseOrder::class, 'fkCorEpl', 'pkEpl');
    }

    public function foreignLanguageCourse()
    {
        return $this->hasMany(EducationPlansForeignLanguage::class, 'fkEflEpl', 'pkEpl');
    }

    public function schoolEducationPlanAssignment()
    {
        return $this->hasMany(SchoolEducationPlanAssignment::class, 'fkSepEpl', 'pkEpl');
    }

    public function enrollStudent()
    {
        return $this->hasMany(EnrollStudent::class, 'fkSteEpl', 'pkEpl');
    }

    public function educationProgram()
    {
        return $this->belongsTo(EducationProgram::class, 'fkEplEdp', 'pkEdp');
    }

    public function nationalEducationPlan()
    {
        return $this->belongsTo(NationalEducationPlan::class, 'fkEplNep', 'pkNep');
    }

    public function educationProfile()
    {
        return $this->belongsTo(EducationProfile::class, 'fkEplEpr', 'pkEpr');
    }

    public function QualificationDegree()
    {
        return $this->belongsTo(QualificationDegree::class, 'fkEplQde', 'pkQde');
    }

    public function CertificateBluePrints()
    {
        return $this->belongsTo(CertificateBluePrints::class, 'fkCbp', 'pkCbp');
    }

    public function grades()
    {
        return $this->belongsTo(Grade::class, 'fkEplGra', 'pkGra');
    }

    public function canton()
    {
        return $this->belongsTo(Canton::class, 'fkEplCan', 'pkCan');
    }

    protected $fillable = array('pkEpl', 'fkEplEdp', 'fkEplCan', 'fkEplNep', 'fkEplEpr', 'fkEplQde', 'fkEplGra', 'epl_Uid', 'epl_EducationPlanName');
}