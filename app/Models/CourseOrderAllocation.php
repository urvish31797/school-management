<?php
/**
 * CourseOrderAllocation
 *
 * Model for CoursesOrdersAllocation Table
 *
 * @package    Laravel
 * @subpackage Model
 * @since      1.0
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CourseOrderAllocation extends Model
{
    use SoftDeletes;
    protected $table = 'CoursesOrdersAllocation';
    public $timestamps = false;

    public function courseOrder()
    {
        return $this->belongsTo(CourseOrder::class, 'fkCoaCor', 'pkCor');
    }

    public function grade()
    {
        return $this->belongsTo(Grade::class, 'fkCoaGra', 'pkGra');
    }

    public function course()
    {
        return $this->belongsTo(Course::class, 'fkCoaCrs', 'pkCrs');
    }
}
