<?php
/**
 * Village School
 *
 * Model for Country Table
 *
 * @package    Laravel
 * @subpackage Model
 * @since      1.0
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class VillageSchool extends Model
{
    use SoftDeletes;
    protected $table = 'VillageSchools';
    public $timestamps = false;

    public function school()
    {
        return $this->belongsTo(School::class, 'fkVscSch', 'pkSch');
    }

    public function classCreation()
    {
        return $this->hasMany(ClassCreation::class, 'fkClrVsc', 'pkVsc');
    }

    public function scopeGetVillageSchoolName($query, $data = [])
    {
        extract($data);
        $villageschool = $query->select('vsc_VillageSchoolName_' . $language . ' AS vsc_VillageSchoolName')->where('pkVsc', $villageschoolId)->first();
        return $villageschool ? $villageschool->vsc_VillageSchoolName : '-';
    }

    public function scopeGetVillageSchoolList($query,$data = [])
    {
        extract($data);
        return $query->select('pkVsc','vsc_VillageSchoolName_'.$language.' AS vsc_VillageSchoolName')->where('fkVscSch',$fkVscSch)->get();
    }

}
