<?php
/**
 * MarksExplanation
 *
 * Model for MarksExplanation Table
 *
 * @package    Laravel
 * @subpackage Model
 * @since      1.0
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MarksExplanation extends Model
{
    use SoftDeletes;
    protected $table = 'MarksExplanation';
    public $timestamps = false;

    protected $guarded = ['pkMe'];
}
