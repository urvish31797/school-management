<?php
/**
 * StudentsBehaviour
 *
 * Model for StudentsBehaviour Table
 *
 * @package    Laravel
 * @subpackage Model
 * @since      1.0
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class StudentsBehaviour extends Model
{
    use SoftDeletes;
    protected $table = 'StudentsBehaviour';
    public $timestamps = false;

    public function studentBehaviour()
    {
        return $this->belongsTo(StudentBehaviour::class, 'fkStbSbe', 'pkSbe');
    }

    public function studentSemester()
    {
        return $this->belongsTo(ClassStudentsSemester::class, 'fkSebSem', 'pkSem');
    }

}
