<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SchoolYearWeekNumber extends Model
{
    use SoftDeletes;
    protected $table = 'SchoolYearWeekNumber';
    public $timestamps = false;
    protected $primaryKey = 'pkSye';

    protected $fillable = array('pkSyw', 'fkSywSye', 'fkSywCan', 'sye_NumberofWeek');

    public function cantons()
    {
        return $this->belongsTo(Canton::class, 'fkSywCan', 'pkCan');
    }

    public function schoolYear()
    {
        return $this->belongsTo(SchoolYear::class, 'fkSywSye', 'pkSye');
    }
}
