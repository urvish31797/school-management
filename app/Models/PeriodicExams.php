<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PeriodicExams extends Model
{
    use SoftDeletes;
    protected $table = 'PeriodicExams';
    public $timestamps = true;
    protected $primaryKey = 'pkPex';

    public function periodicexamtype()
    {
        return $this->belongsTo(PeriodicExamType::class, 'fkPexPet', 'pkPet');
    }

    public function periodicexamways()
    {
        return $this->belongsTo(PeriodicExamWay::class, 'fkPexPew', 'pkPew');
    }

    public function classstudentcourseallocation()
    {
        return $this->belongsTo(ClassStudentsAllocation::class, 'fkPexCsa', 'pkCsa');
    }

    public function classstudentsemester()
    {
        return $this->belongsTo(ClassStudentsSemester::class, 'fkPexSem', 'pkSem');
    }

    public function educationperiod()
    {
        return $this->belongsTo(EducationPeriod::class, 'fkPexEdp', 'pkEdp');
    }

    public function descriptiveexammark()
    {
        return $this->belongsTo(DescriptivePeriodicExamMark::class, 'fkPexDpem', 'pkDpem');
    }
}
