<?php
/**
 * ClassTeachersCourseAllocation
 *
 * Model for ClassTeachersCourseAllocation Table
 *
 * @package    Laravel
 * @subpackage Model
 * @since      1.0
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ClassTeachersCourseAllocation extends Model
{
    use SoftDeletes;
    protected $table = 'ClassTeachersCourseAllocation';
    public $timestamps = false;
    protected $primaryKey = 'pkCtc';

    protected $fillable = array('pkCtc', 'fkCtcCsa', 'fkCtcEeg', 'Ctc_start', 'Ctc_end', 'fkCtcCcs');

    public function classCreation()
    {
        return $this->belongsTo(ClassCreation::class, 'fkCtcClr', 'pkClr');
    }

    public function employeesEngagement()
    {
        return $this->belongsTo(EmployeesEngagement::class, 'fkCtcEeg', 'pkEen');
    }

    public function classCreationStudents()
    {
        return $this->belongsTo(ClassStudentsAllocation::class, 'fkCtcCsa', 'pkCsa');
    }

    public function classCreationSemester()
    {
        return $this->belongsTo(ClassCreationSemester::class, 'fkCtcCcs', 'pkCcs');
    }

}
