<?php
/**
 * CourseOrder
 *
 * Model for CoursesOrders Table
 *
 * @package    Laravel
 * @subpackage Model
 * @since      1.0
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CourseOrder extends Model
{
    use SoftDeletes;
    protected $table = 'CoursesOrders';
    public $timestamps = false;

    public function courseOrderAllocation()
    {
        return $this->hasMany(CourseOrderAllocation::class, 'fkCoaCor', 'pkCor');
    }

    public function school()
    {
        return $this->belongsTo(School::class, 'fkCorSch', 'pkSch');
    }

    public function educationPlan()
    {
        return $this->belongsTo(EducationPlan::class, 'fkCorEpl', 'pkEpl');
    }

    public function educationProgram()
    {
        return $this->belongsTo(EducationProgram::class, 'fkCorEdp', 'pkEdp');
    }

}
