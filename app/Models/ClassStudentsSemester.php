<?php
/**
 * ClassStudentsSemester
 *
 * Model for ClassStudentsSemester Table
 *
 * @package    Laravel
 * @subpackage Model
 * @since      1.0
 */
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ClassStudentsSemester extends Model
{
    use SoftDeletes;
    protected $table = 'ClassStudentsSemester';
    public $timestamps = false;
    protected $primaryKey = 'pkSem';

    public function classStudentsAllocation()
    {
        return $this->hasMany(ClassStudentsAllocation::class, 'fkCsaSem', 'pkSem');
    }

    public function classStudentsBehaviour()
    {
        return $this->hasMany(StudentsBehaviour::class, 'fkSebSem', 'pkSem');
    }

    public function classStudentCertificate()
    {
        return $this->hasMany(StudentCertificate::class, 'fkScrSem', 'pkSem');
    }

    public function classCreationSemester()
    {
        return $this->belongsTo(ClassCreationSemester::class, 'fkSemCcs', 'pkCcs');
    }

    public function studentEnroll()
    {
        return $this->belongsTo(EnrollStudent::class, 'fkSemSen', 'pkSte');
    }

    public function gradeAttemptNumber()
    {
        return $this->belongsTo(GradeAttemptsNumber::class, 'fkSemGan', 'pkGan');
    }

}
