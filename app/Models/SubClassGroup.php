<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SubClassGroup extends Model
{
    use SoftDeletes;
    protected $table = 'SubClassGroups';
    public $timestamps = false;

    public function classCreationStudent()
    {
        return $this->hasMany(ClassStudentsAllocation::class, 'fkCsaScg', 'pkScg');
    }

    public function scopeGetSubClassName($query, $data = [])
    {
        extract($data);
        $subclass = $query->select('scg_Name_' . $language . ' AS scg_Name')->where('pkScg', $subclassId)->first();
        return $subclass ? $subclass->scg_Name : '';
    }
}
