<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DailyBook extends Model
{
    use SoftDeletes;
    protected $table = 'DailyBook';
    public $timestamps = true;
    protected $primaryKey = 'pkDbk';

    protected $fillable = array('pkDbk', 'fkDbkSye', 'fkDbkVsc', 'fkDbkShi', 'fkDbkCcs', 'dbk_Date', 'dbk_Notes');

    public function shift()
    {
        return $this->belongsTo(Shifts::class, 'fkDbkShi', 'pkShi');
    }

    public function classCreationSemester()
    {
        return $this->belongsTo(ClassCreationSemester::class, 'fkDbkCcs', 'pkCcs');
    }

    public function dailyBookLecture()
    {
        return $this->hasMany(DailyBookLecture::class, 'fkDblDbk', 'pkDbk');
    }

    public function villageSchool()
    {
        return $this->belongsTo(VillageSchool::class, 'fkDbkVsc', 'pkVsc');
    }

    public function getDbkDateAttribute($value)
    {
        return Carbon::parse($value)->format('d/m/Y');
    }

}
