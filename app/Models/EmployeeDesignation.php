<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class EmployeeDesignation extends Model
{
    use SoftDeletes;
    protected $table = 'EmployeeDesignations';
    public $timestamps = false;

    public function employeeEducation()
    {
        return $this->hasMany(EmployeesEducationDetail::class, 'fkEedEde', 'pkEde');
    }
}
