<?php
/**
 * ClassCreationSemester
 *
 * Model for ClassCreationSemester Table
 *
 * @package    Laravel
 * @subpackage Model
 * @since      1.0
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class ClassCreationSemester extends Model
{
    use SoftDeletes;
    protected $table = 'ClassCreationSemester';
    public $timestamps = false;
    protected $primaryKey = 'pkCcs';

    public function classCreation()
    {
        return $this->belongsTo(ClassCreation::class, 'fkCcsClr', 'pkClr');
    }

    public function homeRoomTeacher()
    {
        return $this->hasMany(HomeRoomTeacher::class, 'fkHrtCcs', 'pkCcs');
    }

    public function classStudentsSemester()
    {
        return $this->hasMany(ClassStudentsSemester::class, 'fkSemCcs', 'pkCcs');
    }

    public function groupedStudents()
    {
        return $this->classStudentsSemester()->groupBy(['fkCsaSem']);
    }

    public function chiefStudent()
    {
        return $this->belongsTo(EnrollStudent::class, 'fkCcsSen', 'pkSte');
    }

    public function semester()
    {
        return $this->belongsTo(EducationPeriod::class, 'fkCcsEdp', 'pkEdp');
    }

    public function treasureStudent()
    {
        return $this->belongsTo(EnrollStudent::class, 'fkCcsSent', 'pkSte');
    }

    public function classTeachers()
    {
        return $this->hasMany(ClassTeachersCourseAllocation::class, 'fkCtcCcs', 'pkCcs');
    }

    public function scopeGetGradewithClass($query, $data = [])
    {
        extract($data);
        return ClassCreationSemester::with([
            'classCreation.classCreationGrades.grade' => function ($q) {
                $q->select('pkGra', 'gra_GradeNumeric');
            },
            'classCreation.classCreationClasses' => function ($q) use ($language) {
                $q->select('pkCla', 'cla_ClassName_' . $language . ' AS cla_ClassName');
            },
            'classCreation.villageSchool'=>function($q) use($language){
                $q->select('pkVsc','vsc_VillageSchoolName_'.$language.' AS vsc_VillageSchoolName');
            }
        ])->where('pkCcs', $classId)->get();
    }

    public function scopeGetSemesterClasses($query, $fkCcsEdp, $classesId)
    {
        return $query->where('fkCcsEdp', $fkCcsEdp)
                ->whereIn('fkCcsClr', $classesId)
                ->pluck('pkCcs')
                ->all();
    }

}
