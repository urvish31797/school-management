<?php
/**
 * DescriptivePeriodicExamMark
 *
 * Model for DescriptivePeriodicExamMark Table
 *
 * @package    Laravel
 * @subpackage Model
 * @since      1.0
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DescriptivePeriodicExamMark extends Model
{
    use SoftDeletes;
    protected $table = 'DescriptivePeriodicExamMark';
    public $timestamps = false;
    protected $primaryKey = 'pkDpem';

    protected $fillable = array('pkDpem', 'dm_Uid', 'dm_Note');
}
