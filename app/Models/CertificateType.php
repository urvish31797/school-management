<?php
/**
 * CertificateType
 *
 * Model for CertificateType Table
 *
 * @package    Laravel
 * @subpackage Model
 * @since      1.0
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CertificateType extends Model
{
    use SoftDeletes;
    protected $table = 'CertificateType';
    public $timestamps = false;

    protected $fillable = array('pkCty', 'cty_Uid');
}
