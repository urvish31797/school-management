<?php
/**
 * AttendanceWay
 *
 * Model for AttendanceWay Table
 *
 * @package    Laravel
 * @subpackage Model
 * @since      1.0
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AttendanceWay extends Model
{
    use SoftDeletes;
    protected $table = 'AttendanceWay';
    public $timestamps = false;
    protected $primaryKey = 'pkAw';

    protected $fillable = array('pkAw', 'aw_Uid', 'aw_Note', 'aw_Default');

    public function scopeCurrentAttendanceWay($query)
    {
        return $query->where('aw_Default',1);
    }
}
