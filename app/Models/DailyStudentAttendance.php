<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DailyStudentAttendance extends Model
{
    use SoftDeletes;
    protected $table = 'DailyStudentAttendance';
    public $timestamps = true;
    protected $primaryKey = 'pkSae';
    protected $appends = ['full_attendance_status'];

    protected $fillable = array('pkSae', 'fkSaeSya', 'fkSaeSem', 'fkSaeCsa', 'sae_LectureAttendance', 'sae_LecutreStudentStatus', 'sae_StudentTDesc', 'sae_StudentHTDesc');

    public function syllabusAccomplishment()
    {
        return $this->belongsTo(DailySyllabusAccomplishment::class, 'fkSaeSya', 'pkSya');
    }

    public function classStudentsSemester()
    {
        return $this->belongsTo(ClassStudentsSemester::class, 'fkSaeSem', 'pkSem');
    }

    public function classStudentsCourse()
    {
        return $this->belongsTo(ClassStudentsAllocation::class, 'fkSaeCsa', 'pkCsa');
    }

    public function getFullAttendanceStatusAttribute()
    {
        if($this->sae_LectureAttendance == "P"){
            return trans('general.gn_present');
        } else {
            return trans('general.gn_absent');
        }


    }

}
