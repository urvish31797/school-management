<?php
/**
 * Municipality
 *
 * Model for Municipality Table
 *
 * @package    Laravel
 * @subpackage Model
 * @since      1.0
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Municipality extends Model
{
    use SoftDeletes;
    protected $table = 'Municipalities';
    public $timestamps = false;

    public function canton()
    {
        return $this->belongsTo(Canton::class, 'fkMunCan', 'pkCan');
    }

    public function postalCode()
    {
        return $this->hasMany(PostalCode::class, 'fkPofMun', 'pkMun');
    }

    public function employee()
    {
        return $this->hasMany(Employee::class, 'fkEmpMun', 'pkMun');
    }

    public function student()
    {
        return $this->hasMany(Student::class, 'fkStuMun', 'id');
    }

    protected $fillable = array('pkMun', 'fkMunCan', 'mun_Uid', 'mun_MunicipalityName', 'mun_MunicipalityNameGenitive', 'mun_Notes');
}
