<?php
/**
 * Employee
 *
 * Model for Employee Table
 *
 * @package    Laravel
 * @subpackage Model
 * @since      1.0
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Employee extends Authenticatable
{
    use SoftDeletes;
    protected $table = 'Employees';
    public $timestamps = false;

    public function EmployeesEngagement()
    {
        return $this->hasMany(EmployeesEngagement::class, 'fkEenEmp', 'id');
    }

    public function employeeEducation()
    {
        return $this->hasMany(EmployeesEducationDetail::class, 'fkEedEmp', 'id');
    }

    public function homeRoomTeacher()
    {
        return $this->hasMany(HomeRoomTeacher::class, 'fkHrtEmp', 'id');
    }

    public function municipality()
    {
        return $this->belongsTo(Municipality::class, 'fkEmpMun', 'pkMun');
    }

    public function postalCode()
    {
        return $this->belongsTo(PostalCode::class, 'fkEmpPof', 'pkPof');
    }

    public function country()
    {
        return $this->belongsTo(Country::class, 'fkEmpCny', 'pkCny');
    }

    public function nationality()
    {
        return $this->belongsTo(Nationality::class, 'fkEmpNat', 'pkNat');
    }

    public function religion()
    {
        return $this->belongsTo(Religion::class, 'fkEmpRel', 'pkRel');
    }

    public function citizenship()
    {
        return $this->belongsTo(Citizenship::class, 'fkEmpCtz', 'pkCtz');
    }

    public function getFullNameAttribute()
    {
        return $this->emp_EmployeeName . ' ' . $this->emp_EmployeeSurname;
    }

    public function scopeGetEmployeewithId($query)
    {
        $query->addSelect('id',
                \DB::raw("CONCAT(CONCAT(emp_EmployeeSurname, ' ',emp_EmployeeName),
                ' - ',
                (CASE
                WHEN emp_EmployeeID IS NOT NULL THEN emp_EmployeeID
                ELSE emp_TempCitizenId
                END)) AS empName")
            );
    }

    protected $fillable = array('id', 'fkEmpMun', 'fkEmpPof', 'fkEmpCny', 'fkEmpNat', 'fkEmpRel', 'fkEmpCtz', 'emp_EmployeeID', 'emp_EmployeeName', 'emp_EmployeeSurname', 'emp_TempCitizenId', 'emp_DateOfBirth', 'emp_PlaceOfBirth', 'emp_EmployeeGender', 'emp_Address', 'email', 'email_verification_key', 'email_verified_at', 'password', 'remember_token', 'emp_PhoneNumber', 'emp_PicturePath', 'emp_Notes');
}
