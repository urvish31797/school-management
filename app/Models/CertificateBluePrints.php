<?php
/**
 * CertificateBluePrints
 *
 * Model for CertificateBluePrints Table
 *
 * @package    Laravel
 * @subpackage Model
 * @since      1.0
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use UtilHelper;

class CertificateBluePrints extends Model
{
    use SoftDeletes;
    protected $table = 'CertificateBluePrints';
    protected $primaryKey = 'pkCbp';
    public $timestamps = false;

    protected $guarded = ['pkCbp'];

    public function getCbpStartdateAttribute($value)
    {
        if (!empty($value)) {
            return UtilHelper::hertronicDate($value);
        } else {
            return "-";
        }
    }

    public function getCbpEnddateAttribute($value)
    {
        if (!empty($value)) {
            return UtilHelper::hertronicDate($value);
        }
    }

    public function setCbpStartdateAttribute($value)
    {
        $this->attributes['cbp_startdate'] = UtilHelper::sqlDate($value);
    }

    public function setCbpEnddateAttribute($value)
    {
        $this->attributes['cbp_enddate'] = UtilHelper::sqlDate($value);
    }
}
