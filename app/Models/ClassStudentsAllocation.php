<?php
/**
 * ClassStudentsAllocation
 *
 * Model for ClassStudentsCourseAllocation Table
 *
 * @package    Laravel
 * @subpackage Model
 * @since      1.0
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ClassStudentsAllocation extends Model
{
    use SoftDeletes;
    protected $table = 'ClassStudentsCourseAllocation';
    public $timestamps = false;
    protected $primaryKey = 'pkCsa';
    protected $fillable = ['fkCsaScg'];

    public function classSemester()
    {
        return $this->belongsTo(ClassStudentsSemester::class, 'fkCsaSem', 'pkSem');
    }

    public function classCreationTeachers()
    {
        return $this->hasMany(ClassTeachersCourseAllocation::class, 'fkCtcCsa', 'pkCsa');
    }

    public function classCreationCourses()
    {
        return $this->belongsTo(Course::class, 'fkCsaEmc', 'pkCrs');
    }

    public function subClassGroup()
    {
        return $this->belongsTo(SubClassGroup::class, 'fkCsaScg', 'pkScg');
    }

    public function courseGroup()
    {
        return $this->belongsTo(CourseGroupAllocation::class, 'fkCsaCga', 'pkCga');
    }

    public function scopeGetTeacherCourseClass($query, $data = [])
    {
        extract($data);
        $classAlloted = $query->select('pkCsa', 'fkCsaSem', 'fkCsaEmc', 'fkCsaScg', 'fkCsaCga')
            ->whereHas('classCreationTeachers', function ($query) use ($id, $engagementId) {
                $query->where('fkCtcEeg', $engagementId)
                    ->where('fkCtcCcs', $id);
            })
            ->with([
                'classCreationTeachers.classCreationSemester.classCreation.classCreationGrades.grade' => function ($query) {
                    $query->select('pkGra', 'gra_GradeNumeric');
                }
                , 'classCreationTeachers.classCreationSemester.classCreation.classCreationClasses' => function ($query) use ($language) {
                    $query->select('pkCla', 'cla_ClassName_' . $language . ' AS cla_ClassName');
                }
                , 'classCreationTeachers.classCreationSemester.classCreation.villageSchool' => function ($query) use ($language) {
                    $query->select('pkVsc', 'fkVscSch', 'vsc_VillageSchoolName_' . $language . ' AS vsc_VillageSchoolName');
                }
                , 'classCreationCourses' => function ($query) use ($language) {
                    $query->select('pkCrs', 'crs_CourseName_' . $language . ' as crs_CourseName');
                }
                , 'subClassGroup'=>function($query) use($language){
                    $query->select('pkScg','scg_Name_'.$language.' AS scg_Name');
                }
                // ,'courseGroup'=>function($query){
                //     $query->select('pkCga','fkCgaGpg','fkCgaFcg','fkCgaFlg','fkCgaOcg');
                // }
                // ,'courseGroup.OCG'=>function($query){
                //     $query->select('pkOcg','ocg_Name_'.$this->current_language.' AS ocg_Name');
                // }
                // ,'courseGroup.GPG'=>function($query){
                //     $query->select('pkGpg','gpg_Name_'.$this->current_language.' AS gpg_Name');
                // }
                // ,'courseGroup.FLG'=>function($query){
                //     $query->select('pkFon','fon_Name_'.$this->current_language.' AS fon_Name');
                // }
                // ,'courseGroup.FCG'=>function($query){
                //     $query->select('pkFcg','fcg_Name_'.$this->current_language.' AS fcg_Name');
                // }
            ])
            ->where(function ($q) use ($where) {
                if (!empty($where)) {
                    $q->whereRaw($where);
                }
            })
            ->whereIn('fkCsaEmc', $courses);

        if ($i != 2) {
            $classAlloted = $classAlloted->groupBy('fkCsaEmc');
        }

        return $classAlloted->get();
    }
}
