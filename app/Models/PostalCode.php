<?php
/**
 * PostalCode
 *
 * Model for PostalCode Table
 *
 * @package    Laravel
 * @subpackage Model
 * @since      1.0
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PostalCode extends Model
{
    use SoftDeletes;
    protected $table = 'PostOffices';
    public $timestamps = false;

    public function municipality()
    {
        return $this->belongsTo(Municipality::class, 'fkPofMun', 'pkMun');
    }

    public function school()
    {
        return $this->hasMany(School::class, 'fkSchPof', 'pkPof');
    }

    public function employee()
    {
        return $this->hasMany(Employee::class, 'fkEmpPof', 'pkPof');
    }

    public function scopeGetCantonPostoffice($query, $data)
    {
        extract($data);
        return $query->whereHas('municipality.canton', function ($q) use ($cantonId) {
                    $q->where('pkCan', $cantonId);
                })->get();
    }

    protected $fillable = array('pkPof', 'fkPofMun', 'pof_Uid', 'pof_PostOfficeName', 'pof_PostOfficeNumber', 'pof_Notes');
}
