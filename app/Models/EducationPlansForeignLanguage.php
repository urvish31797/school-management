<?php
/**
 * EducationPlansForeignLanguage
 *
 * Model for EducationPlansForeignLanguage Table
 *
 * @package    Laravel
 * @subpackage Model
 * @since      1.0
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class EducationPlansForeignLanguage extends Model
{
    use SoftDeletes;
    protected $table = 'EducationPlansForeignLanguage';
    public $timestamps = false;

    public function educationPlan()
    {
        return $this->belongsTo(EducationPlan::class, 'fkEflEpl', 'pkEpl');
    }

    public function foreignLanguageGroup()
    {
        return $this->belongsTo(Course::class, 'fkEflCrs', 'pkCrs');
    }

    public function grade()
    {
        return $this->belongsTo(Grade::class, 'fkEflGra', 'pkGra');
    }

    public function scopeFindForiegnCourse($query, $data = [])
    {
        extract($data);
        return $query->select($select)->where($where)->first();
    }

    protected $fillable = array('pkEfl', 'fkEflEpl', 'fkEflCrs', 'fkEflGra', 'efc_hours');
}
