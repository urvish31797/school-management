<?php
/**
 * NationalEducationPlan
 *
 * Model for NationalEducationPlan Table
 *
 * @package    Laravel
 * @subpackage Model
 * @since      1.0
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class NationalEducationPlan extends Model
{
    use SoftDeletes;
    protected $table = 'NationalEducationPlans';
    public $timestamps = false;

    public function educationPlan()
    {
        return $this->hasMany(NationalEducationPlan::class, 'fkEplNep', 'pkNep');
    }

    public function language()
    {
        return $this->belongsTo(Language::class, 'fkLid', 'id');
    }

    protected $fillable = array('pkNep', 'nep_Uid', 'nep_NationalEducationPlanName', 'nep_Notes', 'nep_Status');
}
