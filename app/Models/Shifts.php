<?php
/**
 * Shifts
 *
 * Model for Shifts Table
 *
 * @package    Laravel
 * @subpackage Model
 * @since      1.0
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Shifts extends Model
{
    use SoftDeletes;
    protected $table = 'Shifts';
    public $timestamps = false;
    protected $primaryKey = 'pkShi';
}
