<?php
/**
 * StudentCertificate
 *
 * Model for StudentCertificate Table
 *
 * @package    Laravel
 * @subpackage Model
 * @since      1.0
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class StudentCertificate extends Model
{
    use SoftDeletes;
    protected $table = 'StudentCertificate';
    public $timestamps = false;
    protected $primaryKey = 'pkScr';

    public function studentSemester()
    {
        return $this->belongsTo(ClassStudentsSemester::class, 'fkScrSem', 'pkSem');
    }

    public function certificateType()
    {
        return $this->belongsTo(CertificateType::class, 'fkScrCty', 'pkCty');
    }
}
