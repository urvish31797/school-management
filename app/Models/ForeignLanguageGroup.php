<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ForeignLanguageGroup extends Model
{
    use SoftDeletes;
    protected $table = 'ForeignLanguageGroups';
    public $timestamps = false;

    public function courseGroupAllocation()
    {
        return $this->hasMany(CourseGroupAllocation::class, 'fkCgaFlg', 'pkFon');
    }
}
