<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DailySyllabusAccomplishment extends Model
{
    use SoftDeletes;
    protected $table = 'DailySyllabusAccomplishment';
    public $timestamps = true;
    protected $primaryKey = 'pkSya';

    protected $fillable = array('pkSya', 'fkSyaDbl', 'fkSyaCrs', 'fkSyaScg', 'fkSyaCga', 'fkSyaEen', 'fkSyaGra', 'sya_CourseUnitNumber', 'sya_CourseContent');

    public function dailyBookLecture()
    {
        return $this->belongsTo(DailyBookLecture::class, 'fkSyaDbl', 'pkDbl');
    }

    public function studentAttendance()
    {
        return $this->hasMany(DailyStudentAttendance::class, 'fkSaeSya', 'pkSya');
    }

    public function course()
    {
        return $this->belongsTo(Course::class, 'fkSyaCrs', 'pkCrs');
    }

    public function subClassgroup()
    {
        return $this->belongsTo(SubClassGroup::class, 'fkSyaScg', 'pkScg');
    }

    public function otherCourseGroup()
    {
        return $this->belongsTo(CourseGroupAllocation::class, 'fkSyaCga', 'pkCga');
    }

    public function employeeEngagement()
    {
        return $this->belongsTo(EmployeesEngagement::class, 'fkSyaEen', 'pkEen');
    }

    public function grade()
    {
        return $this->belongsTo(Grade::class, 'fkSyaGra', 'pkGra');
    }

    public function class()
    {
        return $this->belongsTo(Classes::class, 'fkSyaCla', 'pkCla');
    }

    public static function boot()
    {
        parent::boot();
        self::deleting(function ($syllabus) { // before delete() method call this
            $syllabus->studentAttendance()->each(function ($attendance) {
                $attendance->delete(); // <-- direct deletion
            });
            // do the rest of the cleanup...
        });
    }
}
