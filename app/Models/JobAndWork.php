<?php
/**
 * JobAndWork
 *
 * Model for JobAndWork Table
 *
 * @package    Laravel
 * @subpackage Model
 * @since      1.0
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class JobAndWork extends Model
{
    use SoftDeletes;
    protected $table = 'JobAndWork';
    public $timestamps = false;
    public $guarded = ['pkJaw'];

    public static function boot()
    {
        parent::boot();
        self::saved(function ($jobandwork) {
            if($jobandwork->id){
                $jobandwork->where('pkJaw',$jobandwork->id)->update(['jaw_Uid' => "JOB" . $jobandwork->id]);
            }
        });
    }
}
