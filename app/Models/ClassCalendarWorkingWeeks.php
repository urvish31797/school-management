<?php

namespace App\Models;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ClassCalendarWorkingWeeks extends Model
{
    use SoftDeletes;
    protected $table = 'ClassCalendarWorkingWeeks';
    public $timestamps = true;
    protected $primaryKey = 'pkCcw';
    public $fillable = ['fkCcwShi','fkCcwSye','fkCcwEen','fkCcwCcs','fkCcwWek','fkCcwSteOne','fkCcwSteTwo','ccw_startDate','ccw_endDate','ccw_notes'];
    protected $appends = ['csd','ced'];

    public function setCcwStartDateAttribute($value)
    {
        $value = str_replace('/', '-', $value);
        $this->attributes['ccw_startDate'] = Carbon::parse($value)->format('Y/m/d');
    }

    public function setCcwEndDateAttribute($value)
    {
        $value = str_replace('/', '-', $value);
        $this->attributes['ccw_endDate'] = Carbon::parse($value)->format('Y/m/d');
    }

    public function getCsdAttribute()
    {
        return Carbon::parse($this->ccw_startDate)->format('d/m/Y');
    }

    public function getCedAttribute()
    {
        return Carbon::parse($this->ccw_endDate)->format('d/m/Y');
    }

    public function school_year()
    {
        return $this->belongsTo(SchoolYear::class, 'fkCcwSye', 'pkSye');
    }

    public function shift()
    {
        return $this->belongsTo(Shifts::class, 'fkCcwShi', 'pkShi');
    }

    public function employee_engagement()
    {
        return $this->belongsTo(EmployeesEngagement::class, 'fkCcwEen', 'pkEen');
    }

    public function class_creation_semester()
    {
        return $this->belongsTo(ClassCreationSemester::class, 'fkCcwCcs', 'pkCcs');
    }

    public function working_week()
    {
        return $this->belongsTo(WorkingWeeks::class, 'fkCcwWek', 'pkWek');
    }

    public function first_student_enrollment()
    {
        return $this->belongsTo(EnrollStudent::class, 'fkCcwSteOne', 'pkSte');
    }

    public function second_student_enrollment()
    {
        return $this->belongsTo(EnrollStudent::class, 'fkCcwSteTwo', 'pkSte');
    }
}
