<?php
/**
 * GradeAttemptsNumber
 *
 * Model for GradeAttemptsNumber Table
 *
 * @package    Laravel
 * @subpackage Model
 * @since      1.0
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class GradeAttemptsNumber extends Model
{
    use SoftDeletes;
    protected $table = 'GradeAttemptsNumber';
    public $timestamps = false;

    protected $fillable = array('pkGan', 'gan_Uid');
}
