<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PeriodicExamType extends Model
{
    use SoftDeletes;
    protected $table = 'PerodicExamType';
    public $timestamps = false;
    protected $primaryKey = 'pkPet';

    protected $fillable = array('pkPet','pet_Note');
}
