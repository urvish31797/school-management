<?php
/**
 * College
 *
 * Model for College Table
 *
 * @package    Laravel
 * @subpackage Model
 * @since      1.0
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class College extends Model
{
    use SoftDeletes;
    protected $table = 'Colleges';
    public $timestamps = false;

    public function employeeEducation()
    {
        return $this->hasMany(EmployeesEducationDetail::class, 'fkEedCol', 'pkCol');
    }

    public function ownershipType()
    {
        return $this->belongsTo(OwnershipType::class, 'fkColOty', 'pkOty');
    }

    public function university()
    {
        return $this->belongsTo(University::class, 'fkColUni', 'pkUni');
    }

    public function country()
    {
        return $this->belongsTo(Country::class, 'fkColCny', 'pkCny');
    }

    protected $fillable = array('pkCol', 'fkColUni', 'fkColCny', 'fkColOty', 'col_Uid', 'col_CollegeName', 'col_YearStartedFounded', 'col_BelongsToUniversity', 'col_PicturePath', 'col_Notes');
}
