<?php
/**
 * AcademicDegree
 *
 * Model for AcademicDegree Table
 *
 * @package    Laravel
 * @subpackage Model
 * @since      1.0
 */

namespace App\Models;

use App;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\AcademicDegree
 *
 * @property int $pkAcd
 * @property string|null $acd_Uid
 * @property string|null $acd_AcademicDegreeName_en
 * @property string|null $acd_AcademicDegreeName_sr
 * @property string|null $acd_AcademicDegreeName_ba
 * @property string|null $acd_AcademicDegreeName_hr
 * @property string|null $acd_Notes
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\EmployeesEducationDetail[] $employeeEducation
 * @property-read int|null $employee_education_count
 * @method static \Illuminate\Database\Eloquent\Builder|AcademicDegree newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|AcademicDegree newQuery()
 * @method static \Illuminate\Database\Query\Builder|AcademicDegree onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|AcademicDegree query()
 * @method static \Illuminate\Database\Eloquent\Builder|AcademicDegree whereAcdAcademicDegreeNameBa($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AcademicDegree whereAcdAcademicDegreeNameEn($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AcademicDegree whereAcdAcademicDegreeNameHr($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AcademicDegree whereAcdAcademicDegreeNameSr($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AcademicDegree whereAcdNotes($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AcademicDegree whereAcdUid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AcademicDegree whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AcademicDegree wherePkAcd($value)
 * @method static \Illuminate\Database\Query\Builder|AcademicDegree withTrashed()
 * @method static \Illuminate\Database\Query\Builder|AcademicDegree withoutTrashed()
 * @mixin \Eloquent
 */
class AcademicDegree extends Model
{
    use SoftDeletes;
    protected $table = 'AcademicDegrees';
    public $timestamps = false;
    protected $fillable = ['pkAcd', 'acd_Uid', 'acd_AcademicDegreeName', 'acd_Notes'];

    public function employeeEducation()
    {
        return $this->hasMany(EmployeesEducationDetail::class, 'fkEedAcd', 'pkAcd');
    }
}
