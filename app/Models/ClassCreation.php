<?php
/**
 * ClassCreation
 *
 * Model for ClassCreation Table
 *
 * @package    Laravel
 * @subpackage Model
 * @since      1.0
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ClassCreation extends Model
{
    use SoftDeletes;
    protected $table = 'ClassCreation';
    public $timestamps = false;

    public function school()
    {
        return $this->belongsTo(School::class, 'fkClrVsc', 'pkSch');
    }

    public function villageSchool()
    {
        return $this->belongsTo(VillageSchool::class, 'fkClrVsc', 'pkVsc');
    }

    public function classCreationSemester()
    {
        return $this->hasMany(ClassCreationSemester::class, 'fkCcsClr', 'pkClr');
    }

    public function classCreationGrades()
    {
        return $this->hasMany(ClassCreationGrades::class, 'fkCcgClr', 'pkClr');
    }

    public function classCreationClasses()
    {
        return $this->belongsTo(Classes::class, 'fkClrCla', 'pkCla');
    }

    public function classCreationSchoolYear()
    {
        return $this->belongsTo(SchoolYear::class, 'fkClrSye', 'pkSye');
    }

    public function scopeGetSchoolClasses($query, $where)
    {
        return $query->where($where)->where('clr_Status', 'Publish');
    }

    // public function scopePublishedClasses($query)
    // {
    //     return $query->where('clr_Status','Publish');
    // }
}
