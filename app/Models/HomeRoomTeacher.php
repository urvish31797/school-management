<?php
/**
 * Home Room Teacher
 *
 * Model for HomeRoomTeacher Table
 *
 * @package    Laravel
 * @subpackage Model
 * @since      1.0
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class HomeRoomTeacher extends Model
{
    use SoftDeletes;
    protected $table = 'HomeRoomTeacher';
    public $timestamps = false;
    protected $primaryKey = 'pkHrt';

    public function employeesEngagement()
    {
        return $this->belongsTo(EmployeesEngagement::class, 'fkHrtEen', 'pkEen');
    }

    public function classCreationSemester()
    {
        return $this->belongsTo(ClassCreationSemester::class, 'fkHrtCcs', 'pkCcs');
    }

    public function engagementType()
    {
        return $this->belongsTo(EngagementType::class, 'fkHrtEty', 'pkEty');
    }

}
