<?php
/**
 * Grade
 *
 * Model for Grade Table
 *
 * @package    Laravel
 * @subpackage Model
 * @since      1.0
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Grade extends Model
{
    use SoftDeletes;
    protected $table = 'Grades';
    public $timestamps = false;

    public function educationPlan()
    {
        return $this->hasMany(EducationPlan::class, 'fkEplGra', 'pkGra');
    }

    public function courseOrderAllocation()
    {
        return $this->hasMany(CourseOrderAllocation::class, 'fkCoaGra', 'pkGra');
    }

    public function enrollStudent()
    {
        return $this->hasMany(EnrollStudent::class, 'fkSteGra', 'pkGra');
    }

    public function classCreationGrade()
    {
        return $this->hasMany(ClassCreationGrades::class, 'fkCcgGra', 'pkGra');
    }

    public function educationPlansMandatoryCourse()
    {
        return $this->hasMany(EducationPlansMandatoryCourse::class, 'fkEmcGra', 'pkGra');
    }

    public function educationPlansForeignLanguage()
    {
        return $this->hasMany(EducationPlansForeignLanguage::class, 'fkEflGra', 'pkGra');
    }

    public function educationPlansOptionalCourse()
    {
        return $this->hasMany(EducationPlansOptionalCourse::class, 'fkEocGra', 'pkGra');
    }

    protected $fillable = array('pkGra', 'gra_Uid', 'gra_GradeName', 'gra_GradeNameRoman', 'gra_GradeNumeric', 'gra_Notes');
}
