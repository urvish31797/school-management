<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class EmployeesEducationDetail extends Model
{
    use SoftDeletes;
    protected $table = 'EmployeesEducationDetails';
    public $timestamps = false;

    public function employee()
    {
        return $this->belongsTo(Employee::class, 'fkEedEmp', 'id');
    }

    public function university()
    {
        return $this->belongsTo(University::class, 'fkEedUni', 'pkUni');
    }

    public function college()
    {
        return $this->belongsTo(College::class, 'fkEedCol', 'pkCol');
    }

    public function academicDegree()
    {
        return $this->belongsTo(AcademicDegree::class, 'fkEedAcd', 'pkAcd');
    }

    public function qualificationDegree()
    {
        return $this->belongsTo(QualificationDegree::class, 'fkEedQde', 'pkQde');
    }

    public function employeeDesignation()
    {
        return $this->belongsTo(EmployeeDesignation::class, 'fkEedEde', 'pkEde');
    }
}
