<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class EducationPeriod extends Model
{
    use SoftDeletes;
    protected $table = 'EducationPeriods';
    public $timestamps = false;
    protected $primaryKey = 'pkEdp';

    public function classCreation()
    {
        return $this->hasMany(ClassCreation::class, 'fkClrEdp', 'pkEdp');
    }
}
