<?php
/**
 * CourseGroupAllocation
 *
 * Model for CourseGroupAllocation Table
 *
 * @package    Laravel
 * @subpackage Model
 * @since      1.0
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CourseGroupAllocation extends Model
{
    use SoftDeletes;
    protected $table = 'CourseGroupAllocation';
    public $timestamps = false;
    protected $primaryKey = 'pkCga';

    public function classStudents()
    {
        return $this->hasMany(ClassStudentsAllocation::class, 'fkCsaCga', 'pkCga');
    }

    public function GPG()
    {
        return $this->belongsTo(GeneralPurposeGroup::class, 'fkCgaGpg', 'pkGpg');
    }

    public function OCG()
    {
        return $this->belongsTo(OptionalCoursesGroup::class, 'fkCgaOcg', 'pkOcg');
    }

    public function FLG()
    {
        return $this->belongsTo(ForeignLanguageGroup::class, 'fkCgaFlg', 'pkFon');
    }

    public function FCG()
    {
        return $this->belongsTo(FacultativeCoursesGroup::class, 'fkCgaFcg', 'pkFcg');
    }

    public function villageschool()
    {
        return $this->belongsTo(VillageSchool::class, 'fkCgaViSch', 'pkVsc');
    }

    public function getCourse()
    {
        return $this->belongsTo(Course::class, 'fkCgaCrs', 'pkCrs');
    }

    public function courseGroupSchoolYear()
    {
        return $this->belongsTo(SchoolYear::class, 'fkCgaSye', 'pkSye');
    }

    public function scopeGetCourseGroupName($query, $data)
    {
        $grp_name = '';
        extract($data);
        $courseGroup = $query->select('pkCga', 'fkCgaGpg', 'fkCgaFcg', 'fkCgaFlg', 'fkCgaOcg')->find($coursegroupId);
        $fkCgaGpg = $courseGroup->fkCgaGpg;
        $fkCgaFcg = $courseGroup->fkCgaFcg;
        $fkCgaFlg = $courseGroup->fkCgaFlg;
        $fkCgaOcg = $courseGroup->fkCgaOcg;

        if (!empty($courseGroup)) {
            if (!empty($fkCgaGpg)) {
                $gpg = GeneralPurposeGroup::select('gpg_Name_' . $language . ' AS group_name')->where('pkGpg', $fkCgaGpg)->first();
                if (!empty($gpg)) {
                    $grp_name = $gpg->group_name;
                }
            }
            elseif (!empty($fkCgaFcg)) {
                $fcg = FacultativeCoursesGroup::select('fcg_Name_' . $language . ' AS group_name')->where('pkFcg', $fkCgaFcg)->first();
                if (!empty($fcg)) {
                    $grp_name = $fcg->group_name;
                }
            }
            elseif (!empty($fkCgaFlg)) {
                $fog = ForeignLanguageGroup::select('fon_Name_' . $language . ' AS group_name')->where('pkFon', $fkCgaFlg)->first();
                if (!empty($fog)) {
                    $grp_name = $fog->group_name;
                }
            }
            elseif (!empty($fkCgaOcg)) {
                $ocg = OptionalCoursesGroup::select('ocg_Name_' . $language . ' AS group_name')->where('pkOcg', $fkCgaOcg)->first();
                if (!empty($ocg)) {
                    $grp_name = $ocg->group_name;
                }
            }
        }

        return $grp_name;
    }
}
