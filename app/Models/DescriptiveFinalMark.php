<?php
/**
 * DescriptiveFinalMark
 *
 * Model for DescriptiveFinalMarks Table
 *
 * @package    Laravel
 * @subpackage Model
 * @since      1.0
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DescriptiveFinalMark extends Model
{
    use SoftDeletes;
    protected $table = 'DescriptiveFinalMarks';
    public $timestamps = false;

}
