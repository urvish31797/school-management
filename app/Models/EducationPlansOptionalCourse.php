<?php
/**
 * EducationPlansOptionalCourse
 *
 * Model for EducationPlansOptionalCourse Table
 *
 * @package    Laravel
 * @subpackage Model
 * @since      1.0
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class EducationPlansOptionalCourse extends Model
{
    use SoftDeletes;
    protected $table = 'EducationPlansOptionalCourses';
    public $timestamps = false;

    public function educationPlan()
    {
        return $this->belongsTo(EducationPlan::class, 'fkEocEpl', 'pkEpl');
    }

    public function optionalCoursesGroup()
    {
        return $this->belongsTo(Course::class, 'fkEocCrs', 'pkCrs');
    }

    public function grade()
    {
        return $this->belongsTo(Grade::class, 'fkEocGra', 'pkGra');
    }

    public function scopeFindOptionalCourse($query, $data = [])
    {
        extract($data);
        return $query->select($select)->where($where)->first();
    }

    protected $fillable = array('pkEoc', 'fkEocEpl', 'fkEocCrs', 'fkEocGra', 'eoc_hours');
}
