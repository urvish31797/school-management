<?php
/**
 * PeriodicExamWay
 *
 * Model for PeriodicExamWay Table
 *
 * @package    Laravel
 * @subpackage Model
 * @since      1.0
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PeriodicExamWay extends Model
{
    use SoftDeletes;
    protected $table = 'PeriodicExamWay';
    public $timestamps = false;
    protected $primaryKey = 'pkPew';

    protected $fillable = array('pkPew', 'pew_Uid', 'pew_Note');

    public function scopeCurrentPeriodicWay($query)
    {
        return $query->where('pew_Default',1);
    }
}
