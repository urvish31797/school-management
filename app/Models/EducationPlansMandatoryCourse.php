<?php
/**
 * EducationPlansMandatoryCourse
 *
 * Model for EducationPlansMandatoryCourse Table
 *
 * @package    Laravel
 * @subpackage Model
 * @since      1.0
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class EducationPlansMandatoryCourse extends Model
{
    use SoftDeletes;
    protected $table = 'EducationPlansMandatoryCourses';
    public $timestamps = false;

    public function educationPlan()
    {
        return $this->belongsTo(EducationPlan::class, 'fkEmcEpl', 'pkEpl');
    }

    public function mandatoryCourseGroup()
    {
        return $this->belongsTo(Course::class, 'fkEplCrs', 'pkCrs');
    }

    public function grade()
    {
        return $this->belongsTo(Grade::class, 'fkEmcGra', 'pkGra');
    }

    protected $fillable = array('pkEmc', 'fkEmcEpl', 'fkEplCrs', 'fkEmcGra', 'emc_hours', 'emc_weekHourRateTeacher', 'emc_summerHolidayHourRate');
}
