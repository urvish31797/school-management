<?php

namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Carbon\Carbon;

class Admin extends Model implements AuthenticatableContract
{
    use Authenticatable;
    use Notifiable;
    use SoftDeletes;

    protected $table = "Admin";

    public function getCanton()
    {
        return $this->belongsTo(Canton::class, 'fkAdmCan', 'pkCan');
    }

    public function getAdmDOBAttribute($value)
    {
        return Carbon::parse($value)->format('d/m/Y');
    }

    public function setAdmDOBAttribute($value)
    {
        $value = str_replace('/', '-', $value);
        $this->attributes['adm_DOB'] = Carbon::parse($value)->format('Y/m/d');
    }

    protected $fillable = array('id', 'fkAdmCan', 'adm_Uid', 'adm_Name', 'adm_Photo', 'email', 'password', 'remember_token', 'adm_Title', 'adm_Phone', 'adm_GovId', 'adm_Gender', 'adm_DOB', 'adm_Address', 'email_verification_key', 'adm_Status', 'type', 'deleted_at');
}
