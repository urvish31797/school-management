<?php
/**
 * School
 *
 * Model for School Table
 *
 * @package    Laravel
 * @subpackage Model
 * @since      1.0
 */

namespace App\Models;

use UtilHelper;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class School extends Model
{
    use SoftDeletes;
    protected $table = 'Schools';
    public $timestamps = false;
    protected $primaryKey = 'pkSch';
    protected $fillable = array('pkSch', 'fkSchPof', 'fkSchOty', 'sch_Uid', 'sch_SchoolLogo', 'sch_SchoolName', 'sch_SchoolId', 'sch_SchoolEmail', 'sch_Founder', 'sch_FoundingDate', 'sch_Address', 'sch_PhoneNumber', 'sch_MinistryApprovalCertificate', 'sch_AboutSchool');

    public function getSchFoundingDateAttribute($value)
    {
        return UtilHelper::hertronicDate($value);
    }

    public function classCreation()
    {
        return $this->hasMany(ClassCreation::class, 'fkClrVsc', 'pkSch');
    }

    public function courseOrder()
    {
        return $this->hasMany(CourseOrder::class, 'fkCorSch', 'pkSch');
    }

    public function schoolEducationPlanAssignment()
    {
        return $this->hasMany(SchoolEducationPlanAssignment::class, 'fkSepSch', 'pkSch');
    }

    public function employeesEngagement()
    {
        return $this->hasMany(EmployeesEngagement::class, 'fkEenSch', 'pkSch');
    }

    public function schoolPhoto()
    {
        return $this->hasMany(SchoolPhoto::class, 'fkSphSch', 'pkSch');
    }

    public function schoolPrincipal()
    {
        return $this->hasMany(SchoolPrincipal::class, 'fkScpSch', 'pkSch');
    }

    public function villageSchool()
    {
        return $this->hasMany(VillageSchool::class, 'fkVscSch', 'pkSch');
    }

    public function mainBook()
    {
        return $this->hasMany(MainBook::class, 'fkMboSch', 'pkSch');
    }

    public function postalCode()
    {
        return $this->belongsTo(PostalCode::class, 'fkSchPof', 'pkPof');
    }

    public function ownershipType()
    {
        return $this->belongsTo(OwnershipType::class, 'fkSchOty', 'pkOty');
    }

    public function educationperiod()
    {
        return $this->belongsTo(EducationPeriod::class, 'fkSchEdp', 'pkEdp');
    }

    public function scopeGetSchoolCanton($query, $schoolid)
    {
        $val = null;
        if (!empty($schoolid)) {
            $school = School::select('fkSchPof')->find($schoolid);
            $postoffice = $school ? $school->fkSchPof : 0;
            $canton = Canton::select('pkCan')->whereHas('municipality.postalCode', function ($q) use ($postoffice) {
                $q->where('pkPof', $postoffice);
            })->first();

            $val = $canton ? $canton->pkCan : null;
        }

        return $val;
    }
}
