<?php
/**
 * Translation
 *
 * Model for Translation Table
 *
 * @package    Laravel
 * @subpackage Model
 * @since      1.0
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\TranslationLoader\LanguageLine as LanguageLine;

class Translation extends LanguageLine
{
    protected $table = 'Translations';
    public $timestamps = true;

    public function scopeGetSelectedKeyword($query, $data = [])
    {
        return $query->whereIn('key', $data['keys']);
    }
}
