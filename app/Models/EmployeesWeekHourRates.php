<?php
/**
 * EmployeesWeekHourRates
 *
 * Model for EmployeesWeekHourRates Table
 *
 * @package    Laravel
 * @subpackage Model
 * @since      1.0
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use UtilHelper;

class EmployeesWeekHourRates extends Model
{
    use SoftDeletes;
    protected $table = 'EmployeesWeekHourRates';
    public $timestamps = false;
    protected $primaryKey = 'pkEwh';

    public function getEmployeeEngagement()
    {
        return $this->belongsTo(EmployeesEngagement::class, 'fkEwhEen', 'pkEen');
    }

    public function getEwhStartDateAttribute($value)
    {
        if (!empty($value)) {
            return UtilHelper::hertronicDate($value);
        } else {
            return "-";
        }
    }

    public function getEwhEnddateAttribute($value)
    {
        if (!empty($value)) {
            return UtilHelper::hertronicDate($value);
        }
    }

    public static function addHourlyRate($enagementId, $hourRate, $note = '', $start_date = '')
    {
        if (empty($start_date)) {
            $start_date = date('Y-m-d');
        }

        if (!empty($hourRate)) {
            $getExisting = EmployeesWeekHourRates::where('fkEwhEen', $enagementId)->orderBy('pkEwh', 'DESC')->first();
            if (!empty($getExisting)) {
                if ($getExisting->ewh_WeeklyHoursRate != $hourRate) {
                    $getExisting->ewh_EndDate = date('Y-m-d');
                    $getExisting->save();

                    $ewhr = new EmployeesWeekHourRates;
                    $data['fkEwhEen'] = $enagementId;
                    $data['ewh_StartDate'] = $start_date;
                    $data['ewh_WeeklyHoursRate'] = $hourRate;
                    $data['ewh_Notes'] = $note;
                    return $ewhr->insertGetId($data);
                } else {
                    $getExisting->ewh_Notes = $note;
                    $getExisting->save();
                }
            } else {
                $ewhr = new EmployeesWeekHourRates;
                $data['fkEwhEen'] = $enagementId;
                $data['ewh_StartDate'] = $start_date;
                $data['ewh_WeeklyHoursRate'] = $hourRate;
                $data['ewh_Notes'] = $note;
                return $ewhr->insertGetId($data);
            }
        }
    }
}
