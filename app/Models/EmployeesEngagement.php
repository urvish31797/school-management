<?php
/**
 * EmployeesEngagement
 *
 * Model for EmployeesEngagement Table
 *
 * @package    Laravel
 * @subpackage Model
 * @since      1.0
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use UtilHelper;

class EmployeesEngagement extends Model
{
    use SoftDeletes;
    protected $table = 'EmployeesEngagement';
    public $timestamps = false;
    protected $primaryKey = 'pkEen';

    public function getEenDateofengagementAttribute($value)
    {
        if (!empty($value)) {
            return UtilHelper::hertronicDate($value);
        } else {
            return "-";
        }
    }

    public function getEenDateoffinishengagementAttribute($value)
    {
        if (!empty($value)) {
            return UtilHelper::hertronicDate($value);
        }
    }

    public function homeRoomTeacher()
    {
        return $this->hasMany(HomeRoomTeacher::class, 'fkHrtEen', 'pkEen');
    }

    public function employeeType()
    {
        return $this->belongsTo(EmployeeType::class, 'fkEenEpty', 'pkEpty');
    }

    public function engagementType()
    {
        return $this->belongsTo(EngagementType::class, 'fkEenEty', 'pkEty');
    }

    public function employee()
    {
        return $this->belongsTo(Employee::class, 'fkEenEmp', 'id');
    }

    public function school()
    {
        return $this->belongsTo(School::class, 'fkEenSch', 'pkSch');
    }

    public function classCreationTeachers()
    {
        return $this->hasMany(ClassTeachersCourseAllocation::class, 'fkCtcEeg', 'pkEen');
    }

    public function getAllHourRates()
    {
        return $this->hasMany(EmployeesWeekHourRates::class, 'fkEwhEen', 'pkEen');
    }

    public function scopeCheckExistActiveRole($query,$emp_id)
    {
        return $query->where('fkEenEmp',$emp_id)
        ->whereNull('een_DateOfFinishEngagement');
    }

    public function getLatestHourRates()
    {
        return $this->hasMany(EmployeesWeekHourRates::class, 'fkEwhEen', 'pkEen')
            ->whereNull('ewh_EndDate')
            ->orderBy('pkEwh', 'DESC');
    }
}
