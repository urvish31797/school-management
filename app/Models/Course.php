<?php
/**
 * Course
 *
 * Model for Course Table
 *
 * @package    Laravel
 * @subpackage Model
 * @since      1.0
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Course extends Model
{
    use SoftDeletes;
    protected $table = 'Courses';
    public $timestamps = false;

    protected $fillable = array('pkCrs', 'crs_Uid', 'crs_CourseName', 'crs_CourseAlternativeName', 'crs_CourseType', 'crs_IsForeignLanguage', 'crs_Notes');

    public function EPMandatoryCourseGroup()
    {
        return $this->hasMany(EducationPlansMandatoryCourse::class, 'fkEplCrs', 'pkCrs');
    }

    public function classStudentsAllocation()
    {
        return $this->hasMany(ClassStudentsAllocation::class, 'fkCsaEmc', 'pkCrs');
    }

    public function courseOrderAllocation()
    {
        return $this->hasMany(CourseOrderAllocation::class, 'fkCoaCrs', 'pkCrs');
    }

    public function scopeGetCourseName($query, $data = [])
    {
        extract($data);
        $course = $query->select('crs_CourseName_' . $language . ' AS crs_CourseName')->where('pkCrs', $courseId)->first();
        return $course ? $course->crs_CourseName : '';
    }
}
