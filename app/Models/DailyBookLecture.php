<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DailyBookLecture extends Model
{
    use SoftDeletes;
    protected $table = 'DailyBookLecture';
    public $timestamps = true;
    protected $primaryKey = 'pkDbl';

    protected $fillable = array('pkDbl', 'fkDblDbk', 'fkDblEw', 'fkDblAw', 'dbl_LectureNumber', 'dbl_Notes', 'dbl_HomeroomTeacherNotes');

    public function dailyBook()
    {
        return $this->belongsTo(DailyBook::class, 'fkDblDbk', 'pkDbk');
    }

    public function educationWay()
    {
        return $this->belongsTo(EducationWay::class, 'fkDblEw', 'pkEw');
    }

    public function attendanceWay()
    {
        return $this->belongsTo(AttendanceWay::class, 'fkDblAw', 'pkAw');
    }

    public function syllabusAccomplishment()
    {
        return $this->hasMany(DailySyllabusAccomplishment::class, 'fkSyaDbl', 'pkDbl');
    }

    public static function boot()
    {
        parent::boot();
        self::deleting(function ($dailyBookLecture) { // before delete() method call this
            $dailyBookLecture->syllabusAccomplishment()->each(function ($syllabus) {
                $syllabus->delete(); // <-- direct deletion
            });
            // do the rest of the cleanup...
        });
    }
}
