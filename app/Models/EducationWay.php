<?php
/**
 * EducationWay
 *
 * Model for EducationWay Table
 *
 * @package    Laravel
 * @subpackage Model
 * @since      1.0
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class EducationWay extends Model
{
    use SoftDeletes;
    protected $table = 'EducationWay';
    public $timestamps = false;
    protected $primaryKey = 'pkEw';

    protected $fillable = array('pkEw', 'ew_Uid', 'ew_Note', 'ew_Default');

    public function scopeCurrentEducationWay($query)
    {
        return $query->where('ew_Default',1);
    }
}
