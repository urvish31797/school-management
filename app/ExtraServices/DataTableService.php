<?php

namespace App\ExtraServices;
use Yajra\DataTables\Html\Builder;

class DataTableService{

    public $builder;

    public function __construct($builder_params, Builder $builder)
    {
        $this->builder = $builder;
        $this->createBuilder($builder_params);
    }

    public function createBuilder($builder_params)
    {
        extract($builder_params);
        $ajax['type'] = config('datatables-html.method');

        return $this->builder->addIndex()
        ->columns($columns)
        ->ajax($ajax)
        ->parameters([
            'processing' => false,
            'searching' => false,
            'language' => [
                'emptyTable' => trans('datatable.dt_empty_table'),
                'info' => trans('datatable.dt_info'),
                "infoEmpty" => trans('datatable.dt_info_empty'),
                "infoFiltered" => trans('datatable.info_filtered'),
                "infoThousands" => ",",
                'lengthMenu' => trans('datatable.dt_length_menu'),
                "loadingRecords"=> trans('datatable.dt_loading_records'),
                "processing"=> trans('datatable.dt_processing'),
                "search"=> trans('datatable.dt_search'),
                "zeroRecords"=> trans('datatable.dt_zero_records'),
                "thousands"=> ",",
                'paginate' => [
                    'first' => trans('datatable.dt_first'),
                    'last' => trans('datatable.dt_last'),
                    'next' => trans('datatable.dt_next'),
                    'previous' => trans('datatable.dt_previous')
                ]
            ],
        ]);
    }

}