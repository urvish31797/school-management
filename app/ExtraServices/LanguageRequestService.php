<?php

namespace App\ExtraServices;
use App\Models\Language;
use App\Http\Traits\FileTrait;
use Cache;

Class LanguageRequestService
{
    use FileTrait;
    private $lang_col = [];
    private $lang_col_msg = [];
    private $col = [];
    private $col_msg = [];

    public $fields = [];
    public $fields_messages = [];

    public function __construct($multi_lang_ary, $normal_fields_ary)
    {
        if(count($multi_lang_ary) > 0){
            $languages = Cache::get('languages');
            foreach ($languages as $lang_value) {
                foreach ($multi_lang_ary as $key => $value) {
                    $label = $key."_".$lang_value->language_key;
                    $this->lang_col[$label] = $value;
                }
            }
        }

        if(count($normal_fields_ary) > 0)
        {
            foreach ($normal_fields_ary as $key => $value) {
                $this->col[$key] = $value;
            }
        }

        self::createMessages();

        $this->fields = array_merge($this->lang_col,$this->col);
        $this->fields_messages = array_merge($this->lang_col_msg,$this->col_msg);
    }

    public function createMessages()
    {
        if(!empty($this->lang_col))
        {
            foreach ($this->lang_col as $key => $value) {

                $rules = explode("|",$value);
                foreach ($rules as $val) {
                    $messages_detail = self::makeCustomMessages($val);
                    $key_label = $key.".".$messages_detail[0];
                    $this->lang_col_msg[$key_label] = $messages_detail[1];
                }
            }
        }

        if(!empty($this->col))
        {
            foreach ($this->col as $key => $value) {

                $rules = explode("|",$value);
                foreach ($rules as $val) {
                    $messages_detail = self::makeCustomMessages($val);
                    $key_label = $key.".".$messages_detail[0];
                    $this->col_msg[$key_label] = $messages_detail[1];
                }
            }
        }
    }

    public function makeCustomMessages($val)
    {
        $attributes = explode(':',$val);
        $attribute = $attributes[0];
        $attribute_value = isset($attributes[1]) ? $attributes[1] : null;

        switch($attribute)
        {
            case 'max':
               $message = self::getMaxMessage($attribute_value);
            break;

            case 'min':
                $message = self::getMinMessage($attribute_value);
            break;

            case 'required':
                $message = self::getRequiredMessage();
            break;

            case 'required_if':
                $message = self::getRequiredMessage();
            break;

            case 'email':
                $message = self::getEmailMessage();
            break;

            case 'date':
                $message = self::getDateMessage();
            break;

            case 'in':
                $message = self::getInMessage();
            break;

            case 'size':
                $message = self::getSizeMessage($attribute_value);
            break;

            case 'mimes':
                $message = self::getMimesMessage($attribute_value);
            break;

            case 'numeric':
                $message = self::getNumericMessage();
            break;

            case 'digits':
                $message = self::getDigitMessage($attribute_value);
            break;

            default:
                $message = "";
            break;
        }

        return [$attribute,$message];
    }

    public function getRequiredMessage(){
        return trans('validation.validate_field_required');
    }

    public function getDigitMessage($value){
        return str_replace('{0}',$value,trans('validation.validate_digit_field_max'));
    }

    public function getNumericMessage(){
        return trans('validation.validate_numeric_allowed');
    }

    public function getMaxMessage($value){
        return str_replace('{0}',$value,trans('validation.validate_maxlength'));
    }

    public function getMinMessage($value){
        return str_replace('{0}',$value,trans('validation.validate_minlength'));
    }

    public function getInMessage(){
        return trans('message.msg_please_select_available_options');
    }

    public function getSizeMessage($value){
        return str_replace('{0}',$this->kbtomb($value),trans('message.msg_max_file_size'));
    }

    public function getMimesMessage($value){
        return str_replace('{0}',$value,trans('message.msg_upload_file_type'));
    }

    public function getDateMessage(){
        return trans('message.msg_please_enter_correct_date');
    }

    public function getEmailMessage(){
        return trans('validation.validate_email_field');
    }

}