<?php
namespace App\ExtraServices;

use App;
use App\Models\AttendanceWay;
use App\Models\Canton;
use App\Models\Country;
use App\Models\EducationPeriod;
use App\Models\EducationWay;
use App\Models\Employee;
use App\Models\Municipality;
use App\Models\OwnershipType;
use App\Models\PostalCode;
use App\Models\University;
use App\Models\VillageSchool;
use App\Models\Shifts;
use App\Models\PeriodicExamWay;
use App\Models\PeriodicExamType;
use Collective\Html\FormBuilder;

class DropdownService extends FormBuilder
{
    public function selectPeriodicExamWay($name, $selected = null, $options = array())
    {
        $list = PeriodicExamWay::select('pkPew', 'pew_Name_' . $options['lang'] . ' AS pew_Name')
        ->pluck('pew_Name', 'pkPew')
        ->prepend(trans('general.gn_select'), '');

        return $this->select($name, $list, $selected, $options);
    }

    public function selectPeriodicExamType($name, $selected = null, $options = array())
    {
        $list = PeriodicExamType::select('pkPet', 'pet_Name_' . $options['lang'] . ' AS pet_Name')
        ->pluck('pet_Name', 'pkPet')
        ->prepend(trans('general.gn_select'), '');

        return $this->select($name, $list, $selected, $options);
    }

    public function selectAttendanceWay($name, $selected = null, $options = array())
    {
        $list = AttendanceWay::select('pkAw', 'aw_Name_' . $options['lang'] . ' AS aw_Name')
            ->pluck('aw_Name', 'pkAw')
            ->prepend(trans('general.gn_select'), '');

        return $this->select($name, $list, $selected, $options);
    }

    public function selectEducationWay($name, $selected = null, $options = array())
    {
        $list = EducationWay::select('pkEw', 'ew_Name_' . $options['lang'] . ' AS ew_Name')
            ->pluck('ew_Name', 'pkEw')
            ->prepend(trans('general.gn_select'), '');

        return $this->select($name, $list, $selected, $options);
    }

    public function selectCountry($name, $selected = null, $options = array())
    {
        $list = Country::select('pkCny', 'cny_CountryName_' . $options['lang'] . ' as cny_CountryName')
            ->pluck('cny_CountryName', 'pkCny')
            ->prepend(trans('general.gn_select'), '');

        return $this->select($name, $list, $selected, $options);
    }

    public function selectCanton($name, $selected = null, $options = array())
    {
        $list = Canton::select('pkCan', 'can_CantonName_' . $options['lang'] . ' as can_CantonName')
            ->pluck('can_CantonName', 'pkCan')
            ->prepend(trans('general.gn_select'), '');

        return $this->select($name, $list, $selected, $options);
    }

    public function selectCantonwithState($name, $selected = null, $options = array())
    {
        $list = Canton::with(['state' => function ($q) use ($options) {
            $q->select('pkSta', 'sta_StateName_' . $options['lang'] . ' AS sta_StateName');
        }])->select('pkCan', 'fkCanSta', 'can_CantonName_' . $options['lang'] . ' as can_CantonName')
            ->get()
            ->map(function ($item) {
                return ['id' => $item->pkCan,
                    'name' => $item->can_CantonName . ", " . $item->state->sta_StateName];
            })
            ->pluck('name', 'id')
            ->prepend(trans('general.gn_select'), '');

        return $this->select($name, $list, $selected, $options);
    }

    public function selectMunicipality($name, $selected = null, $options = array())
    {
        $list = Municipality::select('pkMun', 'mun_MunicipalityName_' . $options['lang'] . ' as mun_MunicipalityName')
            ->pluck('mun_MunicipalityName', 'pkMun')
            ->prepend(trans('general.gn_select'), '');

        return $this->select($name, $list, $selected, $options);
    }

    public function selectCantonEmployee($name, $selected = null, $options = array())
    {
        $cantonId = $options['cantonId'];
        $list = [];

        $postoffices = PostalCode::GetCantonPostoffice(compact('cantonId'))
            ->pluck('pkPof')
            ->all();

        if (count($postoffices)) {
            $list = Employee::GetEmployeewithId()
                ->whereHas('EmployeesEngagement', function ($q) {
                    $q->whereNull('een_DateOfFinishEngagement');
                })
                ->whereIn('fkEmpPof', $postoffices)
                ->where('emp_Status', 'Active')
                ->orderBy('emp_EmployeeName', 'ASC')
                ->pluck('empName', 'id')
                ->prepend(trans('general.gn_select'), '');
        }

        return $this->select($name, $list, $selected, $options);
    }

    public function selectSchoolwithCantonEmployees($name, $selected = null, $options = array())
    {
        $school_id = $options['school_id'];
        $cantonId = $options['cantonId'];

        $school_emp_list = Employee::GetEmployeewithId()
            ->whereHas('EmployeesEngagement', function ($q) use($school_id){
                $q->where('fkEenSch',$school_id)->whereNull('een_DateOfFinishEngagement');
            })
            ->get();

        $postoffices = PostalCode::GetCantonPostoffice(compact('cantonId'))
            ->pluck('pkPof')
            ->all();

        $canton_employee_list = Employee::GetEmployeewithId()
                ->whereHas('EmployeesEngagement', function ($q) {
                    $q->whereNull('een_DateOfFinishEngagement');
                })
                ->whereIn('fkEmpPof', $postoffices)
                ->where('emp_Status', 'Active')
                ->orderBy('emp_EmployeeName', 'ASC')
                ->get();

        $list = $school_emp_list->toBase()->merge($canton_employee_list)
        ->pluck('empName','id')
        ->prepend(trans('general.gn_select'), '');

        return $this->select($name, $list, $selected, $options);
    }

    public function selectVillageSchool($name, $selected = null, $options = array())
    {
        $params = ['language' => $options['lang'], 'fkVscSch' => $options['school_id']];
        $list = VillageSchool::GetVillageSchoolList($params)
            ->pluck('vsc_VillageSchoolName', 'pkVsc')
            ->prepend(trans('general.gn_select'), '');

        return $this->select($name, $list, $selected, $options);
    }

    public function selectOwnershipType($name, $selected = null, $options = array())
    {
        $list = OwnershipType::select('pkOty', 'oty_OwnershipTypeName_' . $options['lang'] . ' as oty_OwnershipTypeName')
            ->pluck('oty_OwnershipTypeName', 'pkOty')
            ->prepend(trans('general.gn_select'), '');

        return $this->select($name, $list, $selected, $options);
    }

    public function selectUniversities($name, $selected = null, $options = array())
    {
        $list = University::select('pkUni', 'uni_UniversityName_' . $options['lang'] . ' as uni_UniversityName')
            ->pluck('uni_UniversityName', 'pkUni')
            ->prepend(trans('general.gn_select'), '');

        return $this->select($name, $list, $selected, $options);
    }

    public function selectEducationPeriod($name, $selected = null, $options = array())
    {
        $list = EducationPeriod::select('pkEdp', 'edp_EducationPeriodName_' . $options['lang'] . ' AS edp_EducationPeriodName')
            ->pluck('edp_EducationPeriodName', 'pkEdp')
            ->prepend(trans('general.gn_select'), '');

        return $this->select($name, $list, $selected, $options);
    }

    public function selectShifts($name, $selected = null, $options = array())
    {
        $list = Shifts::select('pkShi','shi_ShiftName_'.$options['lang'].' AS shi_ShiftName')
        ->pluck('shi_ShiftName','pkShi')
        ->prepend(trans('general.gn_select'), '');

        return $this->select($name, $list, $selected, $options);
    }
}
