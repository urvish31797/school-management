<?php
namespace App\Helpers;

class CustomLogHelper
{
    public $fileHandle = null;
    public $logType = "";

    public static $logTypeToName = [
        'default' => "general_log_{date}",
        'class_creation' => 'class_creation_{datetime}',
    ];

    public function __construct($dataArray = [])
    {
        $currentDateTime = date("Y-m-d H:i:s");
        $currentDate = date("Y-m-d");
        $this->logType = $logType = $dataArray['type'] ?? "default";

        // ."/".$this->logType
        $logPath = base_path() . "/system_logs/" . $currentDate;
        if (!file_exists($logPath)) {
            mkdir($logPath);
            chmod($logPath, 0777);
        }

        $logFileNameRaw = self::$logTypeToName[$this->logType] ?? $this->logType;
        $logFileName = str_replace("{date}", $currentDate, str_replace("{datetime}", $currentDateTime, $logFileNameRaw)) . ".log";
        $this->fileHandle = fopen($logPath . "/" . $logFileName, "a");
    }

    public function addToLog($string)
    {
        $breakLine = "\n";
        $addString = "[ " . date("Y-m-d H:i:s") . " ] " . $string . $breakLine;
        fwrite($this->fileHandle, $addString);
    }

}
