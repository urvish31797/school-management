<?php

namespace App\Helpers;

use Carbon\Carbon;

class UtilHelper
{
    public static function currentDateTime()
    {
        return Carbon::now()->toDateTimeString();
    }

    public static function hertronicDate($date)
    {
        if (!empty($date)) {
            return Carbon::parse($date)->format('d/m/Y');
        } else {
            return null;
        }
    }

    public static function sqlDate($date, $type = 1)
    {
        if (!empty($date)) {
            $date = str_replace('/', '-', $date);
            if ($type == 1) {
                return Carbon::parse($date)->format("Y-m-d h:i:s");
            } else {
                return Carbon::parse($date)->format("Y-m-d");
            }
        } else {
            return null;
        }
    }

    public static function certiDate($date, $multilang = null)
    {
        if (!empty($date)) {
            if ($multilang) {
                $day = Carbon::parse($date)->format("d");
                $month = Carbon::parse($date)->format("M");
                $year = Carbon::parse($date)->format("Y");

                $month = $multilang['month'][$month];

                return $day . ". " . $month . " " . $year . ". " . $multilang['year']['gn_year'];
            } else {
                return Carbon::parse($date)->format("d M Y") . ". Year";
            }
        } else {
            return null;
        }
    }

    public static function createClassSyntaxLine($grades, $class)
    {
        $gradeclass = "-";
        if (!empty($grades) && !empty($class)) {
            if (is_array($class)) {
                foreach ($grades as $key => $value) {
                    $name = $value . $class[$key];
                    $gradeclasses[] = $name;
                }
                $gradeclass = implode(", ", $gradeclasses);
            } else {
                $allgrades = array_map(function ($val) use ($class) {return $val . $class;}, $grades);
                $gradeclass = implode(", ", $allgrades);
            }

        }

        return $gradeclass;
    }

    public static function beautifyPrint($data, $type = 1)
    {
        //type 1: array, type 2:as it is, type 3:print sql,data
        if ($type == 1) {
            echo "<pre>";
            dump($data->toArray());
            echo "</pre>";
        } elseif ($type == 2) {
            echo "<pre>";
            dump($data);
            echo "</pre>";
        } elseif ($type == 3) {
            echo "<pre>";
            $query = str_replace(array('?'), array('\'%s\''), $data->toSql());
            $query = vsprintf($query, $data->getBindings());
            dump($query);
            echo "</pre>";
        }
        exit;
    }
}
