<?php

namespace App\Helpers;

use Illuminate\Contracts\View\View;
use Log;
use Mail;

class MailHelper
{

    public static $dataArray = [];

    public static function sendOTPEmail($user, $otp)
    {

        $content = view('email.password_reset_otp')->render();
        $content = str_replace('{NAME}', $user->name, $content);
        $content = str_replace('{OTP}', $otp, $content);

        MailHelper::sendEmail($user->email, 'Password reset OTP', $content);
    }

    public static function sendNewCredentials($user)
    {

        $content = view('email.new_credentials')->render();
        $content = str_replace('{NAME}', $user['name'], $content);
        $content = str_replace('{VERIFY_KEY}', $user['verify_key'], $content);
        $content = str_replace('{RESET_PASS_LINK}', $user['reset_pass_link'], $content);
        $content = str_replace('{EMAIL}', $user['email'], $content);

        MailHelper::sendEmail($user['email'], $user['subject'], $content);
    }

    public static function schoolCoordinatorCredentials($data)
    {

        if ($data['existEmployee'] == 'No') {
            $content = view('email.new_school_cordinator_assign')->render();
            $content = str_replace('{NAME}', $data['name'], $content);
            $content = str_replace('{VERIFY_KEY}', $data['verify_key'], $content);
            $content = str_replace('{RESET_PASS_LINK}', $data['reset_pass_link'], $content);
            $content = str_replace('{EMAIL}', $data['email'], $content);
            $content = str_replace('{SCHOOL}', $data['school'], $content);

            MailHelper::sendEmail($data['email'], $data['subject'], $content);
        } else {
            $content = view('email.old_school_cordinator_assign')->render();
            $content = str_replace('{NAME}', $data['name'], $content);
            $content = str_replace('{EMAIL}', $data['email'], $content);
            $content = str_replace('{SCHOOL}', $data['school'], $content);

            MailHelper::sendEmail($data['email'], $data['subject'], $content);
        }

    }

    public static function sendNewPrincipalCredentials($user)
    {

        $content = view('email.new_principal_credentials')->render();
        $content = str_replace('{NAME}', $user['name'], $content);
        $content = str_replace('{SCHOOLNAME}', $user['school'], $content);
        $content = str_replace('{VERIFY_KEY}', $user['verify_key'], $content);
        $content = str_replace('{RESET_PASS_LINK}', $user['reset_pass_link'], $content);
        $content = str_replace('{EMAIL}', $user['email'], $content);

        MailHelper::sendEmail($user['email'], $user['subject'], $content);
    }

    public static function sendNewTeacherCredentials($user)
    {

        $content = view('email.new_school_teacher_credentials')->render();
        $content = str_replace('{NAME}', $user['name'], $content);
        $content = str_replace('{SCHOOLNAME}', $user['school'], $content);
        $content = str_replace('{VERIFY_KEY}', $user['verify_key'], $content);
        $content = str_replace('{RESET_PASS_LINK}', $user['reset_pass_link'], $content);
        $content = str_replace('{EMAIL}', $user['email'], $content);

        MailHelper::sendEmail($user['email'], $user['subject'], $content);
    }

    public static function sendNewSchoolSubAdminCredentials($user)
    {

        $content = view('email.new_school_subAdmin_credentials')->render();
        $content = str_replace('{NAME}', $user['name'], $content);
        $content = str_replace('{SCHOOLNAME}', $user['school'], $content);
        $content = str_replace('{VERIFY_KEY}', $user['verify_key'], $content);
        $content = str_replace('{RESET_PASS_LINK}', $user['reset_pass_link'], $content);
        $content = str_replace('{EMAIL}', $user['email'], $content);

        MailHelper::sendEmail($user['email'], $user['subject'], $content);
    }

    public static function sendNewPrincipalAssign($user)
    {

        $content = view('email.new_principal_assign')->render();
        $content = str_replace('{NAME}', $user['name'], $content);
        $content = str_replace('{SCHOOLNAME}', $user['school'], $content);
        $content = str_replace('{EMAIL}', $user['email'], $content);

        MailHelper::sendEmail($user['email'], $user['subject'], $content);
    }

    public static function sendNewTeacherAssign($user)
    {

        $content = view('email.new_teacher_assign')->render();
        $content = str_replace('{NAME}', $user['name'], $content);
        $content = str_replace('{SCHOOLNAME}', $user['school'], $content);
        $content = str_replace('{EMAIL}', $user['email'], $content);

        MailHelper::sendEmail($user['email'], $user['subject'], $content);
    }

    public static function sendHomeroomTeacherAssign($user)
    {

        $content = view('email.new_homeroom_teacher_assign')->render();
        $content = str_replace('{NAME}', $user['name'], $content);
        $content = str_replace('{SCHOOLNAME}', $user['school'], $content);
        $content = str_replace('{EMAIL}', $user['email'], $content);

        MailHelper::sendEmail($user['email'], $user['subject'], $content);
    }

    public static function sendNewMinistryIDPass($user)
    {

        $content = view('email.new_credentials')->render();
        $content = str_replace('{NAME}', $user['name'], $content);
        $content = str_replace('{VERIFY_KEY}', $user['verify_key'], $content);
        $content = str_replace('{RESET_PASS_LINK}', $user['reset_pass_link'], $content);
        $content = str_replace('{EMAIL}', $user['email'], $content);

        MailHelper::sendEmail($user['email'], $user['subject'], $content);
    }

    public static function sendEmailVerification($user)
    {

        $content = view('email.verification_mail')->render();
        $content = str_replace('{NAME}', $user['name'], $content);
        $content = str_replace('{VERIFY_KEY}', $user['verify_key'], $content);
        $content = str_replace('{EMAIL}', $user['email'], $content);

        MailHelper::sendEmail($user['email'], $user['subject'], $content);

    }

    public static function sendForgotPassAdmin($dataArray = [])
    {

        $content = view('email.forgot_password_mail')->render();
        $content = str_replace('{FIRSTNAME}', $dataArray['firstname'], $content);
        $content = str_replace('{RESET_PASS_LINK}', $dataArray['reset_pass_link'], $content);

        MailHelper::sendEmail($dataArray['email'], 'Forgot Password Notification', $content);
    }

    public static function sendEmail($to, $subject, $content, $cc = [], $pdf_link = '')
    {

        $header = view('layout.email.email_header')->render();
        $header = str_replace('{DOMAIN}', config('services.DOMAIN'), $header);
        $header = str_replace('{LOGO}', config('services.DOMAIN') . '/images/ic_login_logo.png', $header);
        $footer = view('layout.email.email_footer')->render();
        $footer = str_replace('{YEAR}', date('Y'), $footer);

        if ($pdf_link != '') {
            $pdf_link = url('') . '/' . $pdf_link;
        }

        $data = array(
            'header' => $header,
            'content' => $content,
            'footer' => $footer,
        );

        try {
            Mail::send('layout.email.email_master', $data, function ($message) use ($to, $subject, $cc) {

                $message->from(config('mail.from.address'), config('mail.from.name'));
                $message->to($to)->subject($subject);
                $message->replyTo(config('services.MAIL_REPLY'), config('mail.from.name'));
                $message->cc($cc);

                if (isset(self::$dataArray['pdf_path']) && self::$dataArray['pdf_path']) {
                    $message->attach(self::$dataArray['pdf_path']);
                }

                foreach ($cc as $bcc) {
                    $message->bcc($bcc);
                }
            });
        } catch (\Exception $e) {
            Log::info("Mail Delivery Failed: " . $e->getMessage());
        }
    }

}
