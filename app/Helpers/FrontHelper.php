<?php

namespace App\Helpers;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class FrontHelper
{

    public static $months = ['01' => 'january', '02' => 'february', '03' => 'march', '04' => 'april', '05' => 'may', '06' => 'june', '07' => 'july', '08' => 'august', '09' => 'september', '10' => 'october', '11' => 'november', '12' => 'december'];

    public static function generatePassword($length = 8, $add_dashes = false, $available_sets = 'luds')
    {
        $sets = array();
        if (strpos($available_sets, 'l') !== false) {
            $sets[] = 'abcdefghjkmnpqrstuvwxyz';
        }
        if (strpos($available_sets, 'u') !== false) {
            $sets[] = 'ABCDEFGHJKMNPQRSTUVWXYZ';
        }
        if (strpos($available_sets, 'd') !== false) {
            $sets[] = '23456789';
        }
        if (strpos($available_sets, 's') !== false) {
            $sets[] = '!@#$%&*?';
        }

        $all = '';
        $password = '';
        foreach ($sets as $set) {
            $password .= $set[array_rand(str_split($set))];
            $all .= $set;
        }
        $all = str_split($all);
        for ($i = 0; $i < $length - count($sets); $i++) {
            $password .= $all[array_rand($all)];
        }
        $password = str_shuffle($password);
        if (!$add_dashes) {
            return $password;
        }
        $dash_len = floor(sqrt($length));
        $dash_str = '';
        while (strlen($password) > $dash_len) {
            $dash_str .= substr($password, 0, $dash_len) . '-';
            $password = substr($password, $dash_len);
        }
        $dash_str .= $password;
        return $dash_str;
    }

    public static function addColumn($table, $column, $after)
    {
        \DB::statement("ALTER TABLE $table CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci");
        Schema::table($table, function ($table1) use ($table, $column, $after) {
            if (!Schema::hasColumn($table, $column)) {
                if (!empty($after)) {
                    if (Schema::hasColumn($table, $after)) {
                        $table1->text($column)->after($after)->nullable();
                    } else {
                        $table1->text($column)->nullable();
                    }
                } else {
                    $table1->text($column)->nullable();
                }
            }

        });
    }

    public static function updateColumn($table, $oldCol, $newCol, $after = '')
    {
        \DB::statement("ALTER TABLE $table CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci");
        Schema::table($table, function ($table1) use ($table, $oldCol, $newCol, $after) {
            if (Schema::hasColumn($table, $oldCol)) {
                $table1->renameColumn($oldCol, $newCol);
            }
        });
    }

    public static function dropColumn($table, $col)
    {
        Schema::table($table, function ($t) use ($col) {
            $t->dropColumn($col);
        });
    }

    public static function addLanguageColumn($table, $column, $after, $dataType)
    {

        \DB::statement("ALTER TABLE $table CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci");
        Schema::table($table, function () use ($table, $column, $after, $dataType) {
            if (!Schema::hasColumn($table, $column)) {
                if (!empty($after)) {
                    \DB::statement("ALTER TABLE $table ADD $column $dataType AFTER $after");
                } else {
                    \DB::statement("ALTER TABLE $table ADD $column $dataType");
                }
            }

        });
    }

    public static function updateLanguageColumn($table, $oldCol, $newCol, $dataType)
    {
        \DB::statement("ALTER TABLE $table CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci");
        Schema::table($table, function () use ($table, $oldCol, $newCol, $dataType) {
            if (Schema::hasColumn($table, $oldCol)) {
                \DB::statement("ALTER TABLE $table CHANGE $oldCol $newCol $dataType");
            }
        });
    }

    //Function for build tree structure
    public static function buildtree($src_arr, $parent_id = 0, $tree = array())
    {
        foreach ($src_arr as $idx => $row) {
            if ($row['edp_ParentId'] == $parent_id) {
                foreach ($row as $k => $v) {
                    $tree[$row['pkEdp']][$k] = $v;
                }
                unset($src_arr[$idx]);
                $tree[$row['pkEdp']]['children'] = FrontHelper::buildtree($src_arr, $row['pkEdp']);
            }
        }
        ksort($tree);
        return $tree;
    }

}
