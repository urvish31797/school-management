<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class sendHomeroomTeacherAssign extends Mailable
{
    use Queueable, SerializesModels;
    protected $maildata;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($input)
    {   
        $this->maildata = $input;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $this->view('email.new_homeroom_teacher_assign')
            ->from(env('MAIL_FROM'), env('MAIL_NAME'))
            ->subject('New Home Room Teacher Assign')
            ->with($this->maildata);
    }
}
