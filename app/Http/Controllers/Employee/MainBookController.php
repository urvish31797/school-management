<?php
/**
 * MainBookController
 *
 * @package    Laravel
 * @subpackage Controller
 * @since      1.0
 */

namespace App\Http\Controllers\Employee;

use App\Helpers\UtilHelper;
use App\Http\Controllers\Controller;
use App\Models\Employee;
use App\Models\EnrollStudent;
use App\Models\MainBook;
use App\Models\Student;
use Illuminate\Http\Request;

class MainBookController extends Controller
{
    /**
     * Mainbook Listing Page
     * @param  Request $request
     * @return view - employee.mainBook.mainBooks
     */
    public function index(Request $request)
    {
        $mainSchool = $this
            ->logged_user->sid;
        if (request()
            ->ajax()) {
            return \View::make('employee.mainBook.mainBooks')
                ->with(['mainSchool' => $mainSchool])->renderSections();
        }
        return view('employee.mainBook.mainBooks')
            ->with(['mainSchool' => $mainSchool]);
    }

    /**
     * View Mainbook Page
     * @param  Int $id
     * @return view - employee.mainBook.student
     */
    public function show($id)
    {
        return view('employee.mainBook.student', ['mid' => $id]);
    }

    /**
     * Save Main Book
     * @param Request $request
     * @param JSON $response
     */
    public function store(Request $request)
    {
        $input = $request->all();
        $response = [];
        $input['mbo_OpeningDate'] = UtilHelper::sqlDate($input['mbo_OpeningDate']);
        if (!empty($input['pkMbo'])) {
            $checkPrev = MainBook::where('mbo_MainBookNameRoman', $input['mbo_MainBookNameRoman'])->where('pkMbo', '!=', $input['pkMbo'])->first();
            if (!empty($checkPrev)) {
                $response['status'] = false;
                $response['message'] = trans('message.msg_main_book_exist');
            } else {
                $id = MainBook::where('pkMbo', $input['pkMbo'])->update($input);
                $response['status'] = true;
                $response['message'] = trans('message.msg_main_book_update_success');
            }
        } else {
            $checkPrev = MainBook::where('mbo_MainBookNameRoman', $input['mbo_MainBookNameRoman'])->first();
            if (!empty($checkPrev)) {
                $response['status'] = false;
                $response['message'] = trans('message.msg_main_book_exist');
            } else {
                $id = MainBook::insertGetId($input);
                if (!empty($id)) {
                    $id = MainBook::where('pkMbo', $id)->update(['mbo_Uid' => "MBN" . $id]);
                    $response['status'] = true;
                    $response['message'] = trans('message.msg_main_book_add_success');

                } else {
                    $response['status'] = false;
                    $response['message'] = trans('message.msg_something_wrong');
                }
            }
        }

        return response()
            ->json($response);
    }

    /**
     * Get Mainbook
     * @param  Request $request
     * @return JSON $response
     */
    public function edit(Request $request, $cid)
    {
        $response = [];
        $cdata = MainBook::where('pkMbo', '=', $cid)->first();

        if (empty($cid) || empty($cdata)) {
            $response['status'] = false;
        } else {
            $cdata->mbo_OpeningDate = date('d/m/Y', strtotime($cdata->mbo_OpeningDate));
            $response['status'] = true;
            $response['data'] = $cdata;
        }

        return response()->json($response);
    }

    /**
     * Main Book Student fetch data
     * @param  Request $request
     * @return JSON $result
     */
    public function mainbooks_student_list(Request $request)
    {
        $data = $request->all();

        $perpage = !empty($data['length']) ? (int) $data['length'] : 10;

        $filter = isset($data['search']) && is_string($data['search']) ? $data['search'] : '';

        $sort_type = isset($data['order'][0]['dir']) && is_string($data['order'][0]['dir']) ? $data['order'][0]['dir'] : '';

        $sort_col = $data['order'][0]['column'];

        $sort_field = $data['columns'][$sort_col]['data'];

        $date_filter = $data['date_filter'];

        $fkSteMbo = $data['fkSteMbo'];

        $mainSchool = $this
            ->logged_user->sid;

        $MainBook = EnrollStudent::whereHas('student', function ($q) use ($filter) {
            if (!empty($filter)) {
                $q->where(function ($query) use ($filter) {
                    $query->where('stu_StudentName', 'LIKE', '%' . $filter . '%')->orWhere('stu_StudentSurname', 'LIKE', '%' . $filter . '%')->orWhere('stu_FatherName', 'LIKE', '%' . $filter . '%')->orWhere('stu_MotherName', 'LIKE', '%' . $filter . '%');
                });
            }
        })->with(['student', 'grade' => function ($q) {
            $q->select('pkGra', 'gra_GradeNameRoman', 'gra_GradeNumeric');
        },
        ])
            ->where('fkSteMbo', $fkSteMbo)->where('ste_FinishingDate', null);

        if ($date_filter) {
            $MainBook = $MainBook->whereDate('ste_EnrollmentDate', '=', date('Y-m-d', strtotime($date_filter)));
        }

        if ($sort_col != 0) {
            $MainBook = $MainBook->orderBy($sort_field, $sort_type);
        }

        $total_mainBooks = $MainBook->count();

        $offset = $data['start'];

        $counter = $offset;
        $MainBookdata = [];
        $mainBooks = $MainBook->offset($offset)->limit($perpage)->get()
            ->toArray();

        foreach ($mainBooks as $key => $value) {
            $value['index'] = $counter + 1;
            $value['stu_DateOfBirth'] = date('d/m/Y', strtotime($value['student']['stu_DateOfBirth']));
            $value['ste_EnrollmentDate'] = date('d/m/Y', strtotime($value['ste_EnrollmentDate']));
            $value['ste_status'] = trans('general.gn_active');
            $MainBookdata[$counter] = $value;
            $counter++;
        }

        $price = array_column($MainBookdata, 'index');

        if ($sort_col == 0) {
            if ($sort_type == 'desc') {
                array_multisort($price, SORT_DESC, $MainBookdata);
            } else {
                array_multisort($price, SORT_ASC, $MainBookdata);
            }
        }
        $MainBookdata = array_values($MainBookdata);
        $result = array(
            "draw" => $data['draw'],
            "recordsTotal" => $total_mainBooks,
            "recordsFiltered" => $total_mainBooks,
            'data' => $MainBookdata,
        );

        return response()->json($result);
    }

    /**
     * Get Mainbooks fetch data
     * @param  Request $request
     * @return JSON $result
     */
    public function mainbooks_list(Request $request)
    {
        $data = $request->all();

        $perpage = !empty($data['length']) ? (int) $data['length'] : 10;

        $filter = isset($data['search']) && is_string($data['search']) ? $data['search'] : '';

        $sort_type = isset($data['order'][0]['dir']) && is_string($data['order'][0]['dir']) ? $data['order'][0]['dir'] : '';

        $sort_col = $data['order'][0]['column'];

        $sort_field = $data['columns'][$sort_col]['data'];

        $date_filter = $data['date_filter'];
        $mainSchool = $this
            ->logged_user->sid;
        $MainBook = new MainBook;

        if ($filter) {
            $MainBook = $MainBook->where('mbo_Uid', 'LIKE', '%' . $filter . '%')->orWhere('mbo_MainBookNameRoman', 'LIKE', '%' . $filter . '%');
        }

        if ($date_filter) {
            $MainBook = $MainBook->whereDate('mbo_OpeningDate', '=', date('Y-m-d', strtotime($date_filter)));
        }

        $MainBookQuery = $MainBook->where('fkMboSch', $mainSchool);

        if ($sort_col != 0) {
            $MainBookQuery = $MainBookQuery->orderBy($sort_field, $sort_type);
        }

        $total_mainBooks = $MainBookQuery->count();

        $offset = $data['start'];

        $counter = $offset;
        $MainBookdata = [];
        $mainBooks = $MainBookQuery->offset($offset)->limit($perpage)->get()
            ->toArray();

        foreach ($mainBooks as $key => $value) {
            $value['index'] = $counter + 1;
            $value['mbo_OpeningDate'] = date('d/m/Y', strtotime($value['mbo_OpeningDate']));
            $MainBookdata[$counter] = $value;
            $counter++;
        }

        $price = array_column($MainBookdata, 'index');

        if ($sort_col == 0) {
            if ($sort_type == 'desc') {
                array_multisort($price, SORT_DESC, $MainBookdata);
            } else {
                array_multisort($price, SORT_ASC, $MainBookdata);
            }
        }
        $result = array(
            "draw" => $data['draw'],
            "recordsTotal" => $total_mainBooks,
            "recordsFiltered" => $total_mainBooks,
            'data' => $MainBookdata,
        );

        return response()->json($result);
    }

    /**
     * Delete Mainbook
     * @param  Request $request
     * @return JSON $response
     */
    public function destroy(Request $request, $cid)
    {
        $response = [];

        if (empty($cid)) {
            $response['status'] = false;
        } else {
            MainBook::where('pkMbo', $cid)->delete();
            $response['status'] = true;
            $response['message'] = trans('message.msg_main_book_delete_success');
        }

        return response()->json($response);
    }

}
