<?php
/**
 * SchoolCoordinatorController
 *
 * @package    Laravel
 * @subpackage Controller
 * @since      1.0
 */

namespace App\Http\Controllers\Employee;

use App\Helpers\MailHelper;
use App\Helpers\UtilHelper;
use App\Http\Controllers\Controller;
use App\Http\Traits\CommonTrait;
use App\Http\Traits\FileTrait;
use App\Models\Admin;
use App\Models\EducationPeriod;
use App\Models\Employee;
use App\Models\EmployeesEngagement;
use App\Models\EmployeeType;
use App\Models\OwnershipType;
use App\Models\PostalCode;
use App\Models\School;
use App\Models\SchoolPhoto;
use Illuminate\Http\Request;

class SchoolCoordinatorController extends Controller
{
    use CommonTrait,FileTrait;
    /**
     * My school page
     * @param  Request $request
     * @return view - employee.school.mySchool
     */
    public function index(Request $request)
    {
        $allowedTypes = $this->getSelectedRoles([$this->employees[0], $this->employees[1], $this->employees[2]]);
        $educationPeriods = EducationPeriod::select('pkEdp', 'edp_EducationPeriodName_' . $this->current_language . ' as edp_EducationPeriodName')
            ->get();

        $OwnershipTypes = OwnershipType::select('pkOty', 'oty_OwnershipTypeName_' . $this->current_language . ' as oty_OwnershipTypeName')
            ->get();
        $SchoolDetail = School::with('schoolPhoto',
            'postalCode',
            'employeesEngagement.employeeType',
            'employeesEngagement.employee',
            'schoolEducationPlanAssignment.educationPlan.educationProfile',
            'schoolEducationPlanAssignment.educationProgram.parent'
        )->with(['employeesEngagement' => function ($query) use ($allowedTypes) {
            $query->whereNull('een_DateOfFinishEngagement')->whereIn('fkEenEpty', $allowedTypes);
        },
        ])->whereHas('employeesEngagement', function ($subQuery) {
            $subQuery->whereHas('employeeType', function ($q) {
                $q->where('epty_Name', 'SchoolCoordinator')
                    ->orWhere('epty_Name', 'SchoolSubAdmin');
            })
                ->where('fkEenEmp', $this
                        ->logged_user
                        ->id)
                    ->where('pkEen', $this
                        ->logged_user
                        ->eid);

            })
                ->first();
            $municipality = $SchoolDetail->postalCode->fkPofMun ?? null;
            $PostalCodes = PostalCode::select('pkPof', 'pof_PostOfficeNumber', 'pof_PostOfficeName_' . $this->current_language . ' as pof_PostOfficeName')
            ->where('fkPofMun',$municipality)
            ->get();

            if (request()
            ->ajax()) {
            return \View::make('employee.school.mySchool')
                ->with(compact('SchoolDetail','PostalCodes','OwnershipTypes','educationPeriods'))->renderSections();
        }
        return view('employee.school.mySchool', compact('SchoolDetail','PostalCodes','OwnershipTypes','educationPeriods'));
    }

    /**
     * update myschool data
     * @param  Request $request
     * @return JSON $response
     */
    public function store(Request $request)
    {
        $response = [];
        $input = $request->all();
        $pkSch = $input['sid'];

        if (isset($input['sch_images'])) {
            $sch_images = $input['sch_images'];
        } else {
            $sch_images = [];
        }
        //Images which need to be deleted
        if (isset($input['old_img'])) {
            $old_imgs = $input['old_img'];
        } else {
            $old_imgs = '';
        }
        //If basic details are updated
        if (isset($input['sch_SchoolEmail'])) {

            $emailExist = School::where('sch_SchoolEmail', '=', $input['sch_SchoolEmail'])->where('pkSch', '!=', $input['sid'])->get();
            $schoolIdExist = School::where('sch_SchoolId', '=', $input['sch_SchoolId'])->where('pkSch', '!=', $input['sid'])->get();
            $emailAdmExist = Admin::where('email', '=', $input['sch_SchoolEmail'])->get();
            $emailEmpExist = Employee::where('email', '=', $input['sch_SchoolEmail'])->get();
            $checkPrev = School::where('sch_SchoolName_' . $this->current_language, $input['sch_SchoolName_' . $this
                    ->current_language])
                    ->where('pkSch', '!=', $input['sid'])->first();

            $input['sch_FoundingDate'] = UtilHelper::sqlDate($input['sch_FoundingDate']);
            $emailExistCount = $emailExist->count();
            $emailAdmExistCount = $emailAdmExist->count();
            $emailEmpExistCount = $emailEmpExist->count();
            $schoolIdExistCount = $schoolIdExist->count();

            if ($emailExistCount != 0 || $emailAdmExistCount != 0 || $emailEmpExistCount != 0) {
                $response['status'] = false;
                $response['message'] = trans('message.msg_email_exist');
                return response()
                    ->json($response);
            }

            if ($schoolIdExistCount != 0) {
                $response['status'] = false;
                $response['message'] = trans('message.msg_school_id_exist');
                return response()
                    ->json($response);
            }

            if (!empty($checkPrev)) {
                $response['status'] = false;
                $response['message'] = trans('message.msg_school_exist');
                return response()->json($response);
            }

            if($request->file('upload_profile')){
                $imgData = School::select('sch_SchoolLogo')->where('pkSch', $input['sid'])->first();
                if (!empty($imgData->sch_SchoolLogo)) {
                    $unlink_file = $imgData->sch_SchoolLogo;
                }

                $upload_params = [
                    'folder_path'=>config('assetpath.schools_images'),
                    'unlink_file'=>$unlink_file ?? ''
                ];
                $image = $request->file('upload_profile');
                $filename = $this->uploadFiletoStorage($image,$upload_params);

                if(!empty($filename)){
                    $input['sch_SchoolLogo'] = $filename;
                }
            }
        }

        if (isset($input['principal_sel']) && !empty($input['principal_sel'])) {
            $empType = EmployeeType::select('pkEpty')->where('epty_Name', '=', 'Principal')
                ->whereNull('epty_ParentId')
                ->first();
            $mainSchool = $this->logged_user->sid;
            $SchoolData = School::find($mainSchool);
            $same_principal_exist = EmployeesEngagement::where('fkEenSch', $mainSchool)
                ->where('fkEenEpty', '=', $empType->pkEpty)
                ->whereNull('een_DateOfFinishEngagement')
                ->first();

            $existEmployee = false;
            if (!empty($same_principal_exist) && $same_principal_exist->pkEen == $input['principal_sel']) {
                $existEmployee = true;
            }

            if (isset($input['een_ActingAsPrincipal'])) {
                $acting = "Yes";
            } else {
                $acting = "No";
            }

            if (!$existEmployee) {
                //old principal engagement ended
                if (isset($same_principal_exist)) {
                    $same_principal_exist->een_DateOfFinishEngagement = date('Y-m-d H:i:s');
                    $same_principal_exist->save();
                }

                $oldEmpEng = EmployeesEngagement::find($input['principal_sel']);
                $EmpData = Employee::find($oldEmpEng->fkEenEmp);

                $newEmpEng = $oldEmpEng->replicate();
                $newEmpEng->een_DateOfEngagement = UtilHelper::sqlDate($input['start_date']);
                $newEmpEng->fkEenEmp = $EmpData->id;
                $newEmpEng->fkEenEpty = $empType->pkEpty;
                $newEmpEng->een_ActingAsPrincipal = $acting;
                $newEmpEng->save();

                $data = ['email' => $EmpData->email,
                    'name' => $EmpData->emp_EmployeeName . " " . $EmpData->emp_EmployeeSurname,
                    'school' => $SchoolData->sch_SchoolName_en,
                    'subject' => 'New School Principal Assign'];
                MailHelper::sendNewPrincipalAssign($data);
            } else {
                $oldEmpEng = EmployeesEngagement::find($input['principal_sel']);
                $oldEmpEng->een_DateOfEngagement = UtilHelper::sqlDate($input['start_date']);
                $oldEmpEng->een_ActingAsPrincipal = $acting;
                $oldEmpEng->save();
            }
        }

        if (!empty($old_imgs)) {
            $old_imgs = explode(",", $old_imgs);
            $oldImgData = SchoolPhoto::whereIn('pkSph', $old_imgs)->get();
            foreach ($oldImgData as $key => $value) {
                $filepath = storage_path('app/public/images/schools') . "/" . $value->sph_SchoolPhoto;
                if (file_exists($filepath)) {
                    unlink($filepath);
                }
            }
            $oldImgData = SchoolPhoto::whereIn('pkSph', $old_imgs)->forceDelete();
        }

        if (!empty($sch_images)) {
            $images = [];
            foreach ($sch_images as $key => $image) {
                $destinationPath = storage_path('app/public/images/schools');
                $filename = time() . $key . '.' . $image->getClientOriginalExtension();
                $image->move($destinationPath, $filename);
                $images[] = ['fkSphSch' => $pkSch, 'sph_SchoolPhoto' => $filename];
            }
            if (!empty($images)) {
                $iupdate = SchoolPhoto::insert($images);
            }
        }

        unset($input['sel_exists_employee']);
        unset($input['start_date']);
        unset($input['end_date']);
        unset($input['principal_sel']);
        unset($input['principal_name']);
        unset($input['principal_email']);
        unset($input['old_img']);
        unset($input['deleted_file_ids']);
        unset($input['school_imgs']);
        unset($input['sch_images']);
        unset($input['upload_profile']);
        unset($input['sid']);
        unset($input['een_ActingAsPrincipal']);

        if (!empty($input)) {
            $fupdate = School::where('pkSch', $pkSch)->update($input);
        }

        $response['status'] = true;
        $response['message'] = trans('message.msg_school_update_success');

        return response()
            ->json($response);
    }
}
