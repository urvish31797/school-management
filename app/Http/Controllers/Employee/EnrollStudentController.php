<?php
namespace App\Http\Controllers\Employee;

use App\Helpers\UtilHelper;
use App\Http\Controllers\Controller;
use App\Models\EducationPlan;
use App\Models\EducationProgram;
use App\Models\Employee;
use App\Models\EnrollStudent;
use App\Models\Grade;
use App\Models\MainBook;
use App\Models\SchoolEducationPlanAssignment;
use App\Models\SchoolYear;
use App\Models\Student;
use Illuminate\Http\Request;

class EnrollStudentController extends Controller
{
    /**
     * Enroll Student Page
     * @param  Request $request
     * @return view - employee.enrollStudent.enrollStudent
     */
    public function create(Request $request)
    {
        $educationPro = [];
        $eduPlan = [];
        $educationPrograms = '';
        $grades = [];
        $eduProg = [];
        $input = $request->all();
        //Get School ID
        // $schoolDetail = Employee::with('EmployeesEngagement', 'EmployeesEngagement.employeeType')->whereHas('EmployeesEngagement', function ($query) {
        //     $query->whereHas('employeeType', function ($query) {
        //         $query->where('epty_Name', 'SchoolCoordinator')
        //             ->orWhere('epty_Name', 'SchoolSubAdmin');
        //     })
        //         ->where('fkEenEmp', $this
        //                 ->logged_user
        //                 ->id)->where(function ($query) {
        //         $query->where('een_DateOfFinishEngagement', '=', null)
        //             ->orWhere('een_DateOfFinishEngagement', '>=', now());
        //     });
        // })
        // ->first();

        $mainSchool = $this->logged_user->sid;

        //Get Education Data
        $sclEduPlnAssinment = SchoolEducationPlanAssignment::where('fkSepSch', $mainSchool)
            ->where('sep_Status', 'Active')
            ->get()
            ->toArray();

        foreach ($sclEduPlnAssinment as $key => $value) {
            $eduProg[] = $value['fkSepEdp'];
            $eduPlan[] = $value['fkSepEpl'];
        }
        $educationProg = EducationProgram::select('pkEdp', 'edp_Uid', 'edp_Name_' . $this->current_language . ' as edp_Name')
            ->whereIn('pkEdp', $eduProg)->get();

        $mainBooks = MainBook::select('pkMbo', 'mbo_Uid', 'mbo_MainBookNameRoman')->where('fkMboSch', $mainSchool)->get();

        $schoolYear = SchoolYear::select('pkSye', 'sye_NameNumeric', 'sye_DefaultYear', 'sye_NameCharacter_' . $this->current_language . ' AS sye_NameCharacter')->get();
        if (request()
            ->ajax()) {
            return \View::make('employee.enrollStudent.enrollStudent')
                ->with(['schoolYear' => $schoolYear, 'mainBooks' => $mainBooks, 'grades' => $grades, 'educationPrograms' => $educationPrograms, 'educationProg' => $educationProg, 'school_id' => $mainSchool])->renderSections();
        }
        return view('employee.enrollStudent.enrollStudent', ['schoolYear' => $schoolYear, 'mainBooks' => $mainBooks, 'grades' => $grades, 'educationPrograms' => $educationPrograms, 'educationProg' => $educationProg, 'school_id' => $mainSchool]);
    }

    /**
     * Save Student Enrollment
     * @param  Request $request
     * @return JSON $response
     */
    public function store(Request $request)
    {
        $response = [];
        $input = $request->all();

        $studentsDet = '';
        if (isset($input['select_student']) && $input['select_student'] != null && !empty($input['select_student'])) {
            $studentsDet = Student::select('id', 'stu_DistanceInKilometers')->where('id', $input['select_student'])->first();
        } else {
            return response()
                ->json(['status' => false, 'message' => trans('message.msg_please_add_student')]);
        }

        $mainSchool = $this
            ->logged_user->sid;

        if (isset($studentsDet) && !empty($studentsDet)) {
            $checkEnroll = EnrollStudent::where('fkSteStu', $studentsDet->id)
                ->where('fkSteSye', $input['fkSteSye'])
                ->where('fkSteGra', $input['fkSteGra'])
                ->first();
            if (!empty($checkEnroll)) {
                return response()->json(['status' => false, 'message' => trans('message.msg_student_already_selected_same_school')]);
            }
        }

        $villageschool = !empty($input['fkSteViSch']) ? $input['fkSteViSch'] : null;
        $ee = new EnrollStudent;
        $ee->fkSteStu = isset($studentsDet->id) ? $studentsDet->id : '';
        $ee->fkSteMbo = $input['fkSteMbo'];
        $ee->fkSteGra = $input['fkSteGra'];
        $ee->fkSteEdp = $input['fkSteEdp'];
        $ee->fkSteEpl = $input['fkSteEpl'];
        $ee->fkSteSye = $input['fkSteSye'];
        $ee->fkSteSch = $mainSchool;
        $ee->fkSteViSch = $villageschool;
        $ee->ste_DistanceInKilometers = !empty($studentsDet->stu_DistanceInKilometers) ? $studentsDet->stu_DistanceInKilometers : '';
        $ee->ste_MainBookOrderNumber = $input['ste_MainBookOrderNumber'];
        $ee->ste_EnrollmentDate = !empty($input['ste_EnrollmentDate']) ? UtilHelper::sqlDate($input['ste_EnrollmentDate']) : '';
        $ee->ste_EnrollBasedOn = $input['ste_EnrollBasedOn'];
        $ee->ste_Reason = $input['ste_Reason'];
        $ee->ste_FinishingDate = !empty($input['ste_FinishingDate']) ? UtilHelper::sqlDate($input['ste_FinishingDate']) : null;
        $ee->ste_BreakingDate = !empty($input['ste_BreakingDate']) ? UtilHelper::sqlDate($input['ste_BreakingDate']) : null;
        $ee->ste_ExpellingDate = !empty($input['ste_ExpellingDate']) ? UtilHelper::sqlDate($input['ste_ExpellingDate']) : null;

        if ($ee->save()) {
            //Check student enrollment already exist for this school. and if available so we need to end enroll date for previous grade
            $previousGrade = EnrollStudent::select('pkSte')->where('fkSteStu', $studentsDet->id)
                ->where('fkSteSch', $mainSchool)->where('pkSte', '!=', $ee->pkSte)
                ->orderBy('pkSte', 'DESC')
                ->first();

            if (!empty($previousGrade)) {
                $previousGrade->ste_EnrollmentFinishDate = date('Y-m-d h:i:s');
                $previousGrade->save();
            }

            $response['status'] = true;
            $response['message'] = trans('message.msg_student_enrolled_success');
        } else {
            $response['status'] = false;
            $response['message'] = trans('message.msg_something_wrong');
        }

        return response()
            ->json($response);
    }

    /**
     * View Education Plan Page
     * @param  Int $id
     * @return view - employee.enrollStudent.viewEducationPlan
     */
    public function viewEducationPlan($id)
    {
        $mdata = '';
        if (!empty($id)) {
            $mdata = EducationPlan::with(['grades' => function ($query) {
                $query->select('pkGra', 'gra_GradeNameRoman', 'gra_GradeNumeric');
            }
                , 'educationProfile' => function ($query) {
                    $query->select('pkEpr', 'epr_EducationProfileName_' . $this->current_language . ' as epr_EducationProfileName');
                }
                , 'educationProgram' => function ($query) {
                    $query->select('pkEdp', 'edp_Name_' . $this->current_language . ' as edp_Name');
                }
                , 'nationalEducationPlan' => function ($query) {
                    $query->select('pkNep', 'nep_NationalEducationPlanName_' . $this->current_language . ' as nep_NationalEducationPlanName');
                }
                , 'QualificationDegree' => function ($query) {
                    $query->select('pkQde', 'qde_QualificationDegreeName_' . $this->current_language . ' as qde_QualificationDegreeName');
                }
                , 'mandatoryCourse.mandatoryCourseGroup' => function ($query) {
                    $query->select('pkCrs', 'crs_CourseName_' . $this->current_language . ' as crs_CourseName')
                        ->where('crs_CourseType', '!=', 'DummyOptionalCourse')
                        ->where('crs_CourseType', '!=', 'DummyForeignCourse');
                }
                , 'optionalCourse.optionalCoursesGroup' => function ($query) {
                    $query->select('pkCrs', 'crs_CourseName_' . $this->current_language . ' as crs_CourseName')
                        ->where('crs_CourseType', '=', 'DummyOptionalCourse');
                }
                , 'foreignLanguageCourse.foreignLanguageGroup' => function ($query) {
                    $query->select('pkCrs', 'crs_CourseName_' . $this->current_language . ' as crs_CourseName')
                        ->where('crs_CourseType', '=', 'DummyForeignCourse');
                },
            ]);
            $mdata = $mdata->where('pkEpl', '=', $id)->where('deleted_at', '=', null)
                ->first();

        }
        if (request()
            ->ajax()) {
            return \View::make('employee.enrollStudent.viewEducationPlan')
                ->with('mdata', $mdata)->renderSections();
        }
        return view('employee.enrollStudent.viewEducationPlan', ['mdata' => $mdata]);
    }

    /**
     * Fetch Enroll Student Data
     * @param  Request $request
     * @return JSON $result
     */
    public function enrollstudents_list(Request $request)
    {
        $data = $request->all();

        $perpage = !empty($data['length']) ? (int) $data['length'] : 10;

        $filter = isset($data['search']) && is_string($data['search']) ? $data['search'] : '';

        $sort_type = isset($data['order'][0]['dir']) && is_string($data['order'][0]['dir']) ? $data['order'][0]['dir'] : '';
        $sort_col = isset($data['order'][0]['column']) ? $data['order'][0]['column'] : '';

        $sort_field = isset($data['columns'][$sort_col]['data']) ? $data['columns'][$sort_col]['data'] : '';

        $student = new Student;

        if ($filter) {
            $student = $student->where('stu_StudentID', 'LIKE', '%' . $filter . '%')->orWhere('stu_StudentName', 'LIKE', '%' . $filter . '%');
        }
        $studentQuery = $student;

        if ($sort_col != 0) {
            $studentQuery = $studentQuery->orderBy($sort_field, $sort_type);
        }

        $total_students = $studentQuery->count();

        $offset = $data['start'];

        $counter = $offset;
        $studentdata = [];
        $students = $studentQuery->offset($offset)->limit($perpage)->get()
            ->toArray();

        foreach ($students as $key => $value) {
            $value['index'] = $counter + 1;
            $studentdata[$counter] = $value;
            $counter++;
        }

        $price = array_column($studentdata, 'index');

        if ($sort_col == 0) {
            if ($sort_type == 'desc') {
                array_multisort($price, SORT_DESC, $studentdata);
            } else {
                array_multisort($price, SORT_ASC, $studentdata);
            }
        }
        $result = array(
            "draw" => $data['draw'],
            "recordsTotal" => $total_students,
            "recordsFiltered" => $total_students,
            "data" => $studentdata,
        );
        return response()->json($result);
    }

    public function fetchEducationPlan(Request $request)
    {
        $input = $request->all();
        $mainSchool = $this
            ->logged_user->sid;
        //Get Education Education Program from  SchoolEducationPlanAssignment
        if (isset($input['fkSteEdp']) && $input['fkSteEdp']) {
            $sclEduPlnAssinmen = SchoolEducationPlanAssignment::where('fkSepSch', $mainSchool)
                ->where('fkSepEdp', $input['fkSteEdp'])
                ->where('sep_Status', 'Active')
                ->get()
                ->toArray();
            foreach ($sclEduPlnAssinmen as $key => $value) {
                $eduPlan1[] = $value['fkSepEpl'];
            }

            $educationPlans = EducationPlan::select('pkEpl', 'epl_EducationPlanName_' . $this->current_language . ' as epl_EducationPlanName')
                ->whereIn('pkEpl', $eduPlan1)->get()
                ->toArray();

            return response()
                ->json(['status' => true, 'educationPlans' => $educationPlans]);
        } else {
            return response()
                ->json(['status' => false]);
        }
    }

    public function fetchGrades(Request $request)
    {
        $input = $request->all();
        $mainSchool = $this
            ->logged_user->sid;
        //Get Grade List From Education plan
        if (isset($input['fkSteEpl']) && $input['fkSteEpl']) {

            $gradIdeduPlan = EducationPlan::with(['mandatoryCourse'])->where('pkEpl', $input['fkSteEpl'])->first();

            $gradesArr = [];
            foreach ($gradIdeduPlan->mandatoryCourse as $k => $v) {
                $tmpGrade[] = $v->fkEmcGra;
            }
            $gradesArr = array_unique($tmpGrade);

            $grades = Grade::select('pkGra', 'gra_GradeNumeric')
                ->whereIn('pkGra', $gradesArr)->get()
                ->toArray();

            return response()
                ->json(['status' => true, 'grades' => $grades]);
        } else {
            return response()
                ->json(['status' => false]);
        }
    }
}
