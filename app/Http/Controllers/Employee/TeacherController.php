<?php
/**
 * TeacherController
 *
 * @package    Laravel
 * @subpackage Controller
 * @since      1.0
 */

namespace App\Http\Controllers\employee;

use App\Helpers\FrontHelper;
use App\Helpers\MailHelper;
use App\Helpers\UtilHelper;
use App\Http\Controllers\Controller;
use App\Http\Traits\CommonTrait;
use App\Models\AcademicDegree;
use App\Models\Admin;
use App\Models\Citizenship;
use App\Models\College;
use App\Models\Country;
use App\Models\Employee;
use App\Models\EmployeeDesignation;
use App\Models\EmployeesEducationDetail;
use App\Models\EmployeesEngagement;
use App\Models\EmployeesWeekHourRates;
use App\Models\EmployeeType;
use App\Models\EngagementType;
use App\Models\Nationality;
use App\Models\QualificationDegree;
use App\Models\Religion;
use App\Models\School;
use App\Models\University;
use Illuminate\Http\Request;

class TeacherController extends Controller
{
    use CommonTrait;
    /**
     * View Employees Listing Page
     * @param  Request $request
     * @return view - employee.employees.employees
     */
    public function index(Request $request)
    {
        $employeeType = EmployeeType::where('epty_Name', '!=', 'HomeroomTeacher')->where('epty_Name', '!=', 'SchoolCoordinator')
            ->where('epty_Name', '!=', 'SchoolSubAdmin')
            ->get();
        if (request()
            ->ajax()) {
            return \View::make('employee.employees.employees')
                ->with('employeeType', $employeeType)->renderSections();
        }
        return view('employee.employees.employees', ['employeeType' => $employeeType]);
    }

    /**
     * Add Employee Page
     * @param Request $request
     * @return view - employee.employees.addEmployee
     */
    public function create(Request $request)
    {
        $AcademicDegrees = AcademicDegree::select('pkAcd', 'acd_AcademicDegreeName_' . $this->current_language . ' as acd_AcademicDegreeName')
            ->get();
        $Colleges = College::select('pkCol', 'col_CollegeName_' . $this->current_language . ' as col_CollegeName')
            ->get();
        $Countries = Country::select('pkCny', 'cny_CountryName_' . $this->current_language . ' as cny_CountryName')
            ->get();
        $QualificationDegrees = QualificationDegree::select('pkQde', 'qde_QualificationDegreeName_' . $this->current_language . ' as qde_QualificationDegreeName')
            ->get();
        $Citizenships = Citizenship::select('pkCtz', 'ctz_CitizenshipName_' . $this->current_language . ' as ctz_CitizenshipName')
            ->get();
        $Nationalities = Nationality::select('pkNat', 'nat_NationalityName_' . $this->current_language . ' as nat_NationalityName')
            ->get();
        $Religions = Religion::select('pkRel', 'rel_ReligionName_' . $this->current_language . ' as rel_ReligionName')
            ->get();
        $Universities = University::select('pkUni', 'uni_UniversityName_' . $this->current_language . ' as uni_UniversityName')
            ->get();
        $EmployeeDesignations = EmployeeDesignation::select('pkEde', 'ede_EmployeeDesignationName_' . $this->current_language . ' as ede_EmployeeDesignationName')
            ->get();
        $Schools = School::select('pkSch', 'sch_SchoolName_' . $this->current_language . ' as sch_SchoolName')
            ->get();
        $EngagementTypes = EngagementType::select('pkEty', 'ety_EngagementTypeName_' . $this->current_language . ' as ety_EngagementTypeName')
            ->get();
        $Municipalities = $this->getMuncipalityDropdownData($this->current_language);
        $PostalCodes = $this->getPostalCodeDropdownData($this->current_language);

        $employeeType = EmployeeType::select('pkEpty', 'epty_Name')->where('epty_Name', '!=', 'SchoolSubAdmin')
            ->where('epty_Name', '!=', 'SchoolCoordinator')
            ->get();

        $mainSchool = $this
            ->logged_user->sid;

        if (request()
            ->ajax()) {
            return \View::make('employee.employees.addEmployee')
                ->with(['employeeType' => $employeeType, 'Countries' => $Countries, 'Municipalities' => $Municipalities, 'PostalCodes' => $PostalCodes, 'Citizenships' => $Citizenships, 'Nationalities' => $Nationalities, 'Religions' => $Religions, 'Universities' => $Universities, 'Colleges' => $Colleges, 'EmployeeDesignations' => $EmployeeDesignations, 'QualificationDegrees' => $QualificationDegrees, 'AcademicDegrees' => $AcademicDegrees, 'Schools' => $Schools, 'EngagementTypes' => $EngagementTypes, 'mainSchool' => $mainSchool])->renderSections();
        }
        return view('employee.employees.addEmployee', ['employeeType' => $employeeType, 'Countries' => $Countries, 'Municipalities' => $Municipalities, 'PostalCodes' => $PostalCodes, 'Citizenships' => $Citizenships, 'Nationalities' => $Nationalities, 'Religions' => $Religions, 'Universities' => $Universities, 'Colleges' => $Colleges, 'EmployeeDesignations' => $EmployeeDesignations, 'QualificationDegrees' => $QualificationDegrees, 'AcademicDegrees' => $AcademicDegrees, 'Schools' => $Schools, 'EngagementTypes' => $EngagementTypes, 'mainSchool' => $mainSchool]);
    }

    /**
     * Save Employee
     * @param Request $request
     * @return JSON $response
     */
    public function store(Request $request)
    {
        $response = [];
        $input = $request->all();

        if (isset($input['emp_engagment_id']) && !empty($input['emp_engagment_id'])) {

            $inData = [];
            foreach ($input as $key => $value) {
                foreach ($value as $k => $v) {
                    $inData[$k][$key] = $v;
                }
            }
            $engData = [];

            foreach ($inData as $key => $inDatav) {
                if (isset($inDatav['een_ActingAsPrincipal']) && $inDatav['fkEenEpty'] == 1) {
                    $acting = "Yes";
                } else {
                    $acting = "No";
                }

                $engData['een_DateOfEngagement'] = UtilHelper::sqlDate($inDatav['start_date']);
                $engData['fkEenEty'] = $inDatav['fkEenEty'];
                $engData['fkEenEpty'] = $inDatav['fkEenEpty'];
                $engData['een_Notes'] = $inDatav['een_Notes'];
                $engData['een_ActingAsPrincipal'] = $acting;
                if (isset($inDatav['end_date']) && !empty($inDatav['end_date'])) {
                    $engData['een_DateOfFinishEngagement'] = UtilHelper::sqlDate($inDatav['end_date']);
                } else {
                    $engData['een_DateOfFinishEngagement'] = null;
                }

                EmployeesEngagement::where('pkEen', $inDatav['emp_engagment_id'])->update($engData);
                EmployeesWeekHourRates::addHourlyRate($inDatav['emp_engagment_id'], $inDatav['ewh_WeeklyHoursRate'], $inDatav['ewh_Notes'], UtilHelper::sqlDate($inDatav['start_date']));
            }
            if ($inDatav['emp_engagment_id']) {
                $response['status'] = true;
                $response['message'] = trans('message.msg_Teacher_update_success');
                $response['redirect'] = url('/employee/employees');
            } else {
                $response['status'] = false;
                $response['message'] = trans('message.msg_something_wrong');
            }
            return response()
                ->json($response);

        }

        if (isset($input['id']) && !empty($input['id'])) {
            $image = $request->file('upload_profile');
            $id = $input['id'];
            $pkSch = $input['sid'];
            $pkEen = $input['engid'];

            $govIdExistCount = 0;
            if (!empty($input['emp_EmployeeID'])) {
                $govIdExistCount = Employee::where('emp_EmployeeID', '=', $input['emp_EmployeeID'])
                    ->where('id', '!=', $id)
                    ->count();
            }

            $tempIdExistCount = 0;
            if (!empty($input['emp_TempCitizenId'])) {
                $tempIdExistCount = Employee::where('emp_TempCitizenId', '=', $input['emp_TempCitizenId'])
                    ->where('emp_TempCitizenId', '!=', null)
                    ->where('id', '!=', $id)
                    ->count();
            }

            $emailExistCount = Employee::where('email', '=', $input['email'])->where('id', '!=', $id)->count();
            $emailAdmExistCount = Admin::where('email', '=', $input['email'])->count();

            if ($emailExistCount != 0 || $emailAdmExistCount != 0) {
                $response['status'] = false;
                $response['message'] = trans('message.msg_email_exist');
                return response()->json($response);
            }

            if ($govIdExistCount != 0) {
                $response['status'] = false;
                $response['message'] = trans('message.msg_employee_id_exist');
                return response()->json($response);
            }

            if ($tempIdExistCount != 0) {
                $response['status'] = false;
                $response['message'] = trans('message.msg_temp_citizen_id_exist');
                return response()
                    ->json($response);
            }

            $user = Employee::find($id);
            $user->emp_EmployeeName = $input['emp_EmployeeName'];
            $user->emp_EmployeeSurname = $input['emp_EmployeeSurname'];
            $user->emp_EmployeeID = $input['emp_EmployeeID'];
            $user->emp_TempCitizenId = $input['emp_TempCitizenId'];
            $user->emp_EmployeeGender = $input['emp_EmployeeGender'];
            $user->emp_DateOfBirth = UtilHelper::sqlDate($input['emp_DateOfBirth']);
            $user->emp_PlaceOfBirth = $input['emp_PlaceOfBirth'];
            $user->emp_Address = $input['emp_Address'];
            $user->emp_PhoneNumber = $input['emp_PhoneNumber'];
            $user->emp_Notes = $input['emp_Notes'];
            $user->fkEmpMun = $input['fkEmpMun'];
            $user->fkEmpPof = $input['fkEmpPof'];
            $user->fkEmpCny = $input['fkEmpCny'];
            $user->fkEmpNat = $input['fkEmpNat'];
            $user->fkEmpRel = $input['fkEmpRel'];
            $user->fkEmpCtz = $input['fkEmpCtz'];
            $user->emp_Status = $input['emp_Status'];

            $empType = EmployeeType::select('pkEpty')->where('epty_Name', '=', 'Teacher')
                ->where('epty_ParentId', '=', null)
                ->first();
            $SchoolData = School::select('sch_SchoolName_en')->where('pkSch', $pkSch)->first();

            if (!empty($image)) {
                $input['emp_PicturePath'] = time() . '.' . $image->getClientOriginalExtension();
                $destinationPath = storage_path('app/public/images/users');
                $image->move($destinationPath, $input['emp_PicturePath']);
                if (!empty($user->emp_PicturePath)) {
                    $filepath = storage_path('app/public/images/users') . "/" . $user->emp_PicturePath;
                    if (file_exists($filepath)) {
                        unlink($filepath);
                    }
                }
                $user->emp_PicturePath = $input['emp_PicturePath'];
            }

            $tdata = $input['total_details'];
            $details = [];
            for ($i = 1; $i <= $tdata; $i++) {
                $filename = '';
                $image = '';
                if ($request->hasFile('eed_DiplomaPicturePath_' . $i)) {
                    $image = $request->file('eed_DiplomaPicturePath_' . $i);
                    $filename = time() . $i . '.' . $image->getClientOriginalExtension();
                    $destinationPath = storage_path('app/public/files/users');
                    $image->move($destinationPath, $filename);
                } else {
                    $filename = $input['file_name_' . $i];
                }
                $details[] = ['fkEedEmp' => $user->id, 'fkEedCol' => $input['fkEedCol_' . $i], 'fkEedUni' => $input['fkEedUni_' . $i], 'fkEedAcd' => $input['fkEedAcd_' . $i], 'fkEedQde' => $input['fkEedQde_' . $i], 'fkEedEde' => $input['fkEedEde_' . $i], 'eed_ShortTitle' => $input['eed_ShortTitle_' . $i], 'eed_SemesterNumbers' => $input['eed_SemesterNumbers_' . $i], 'eed_EctsPoints' => $input['eed_EctsPoints_' . $i], 'eed_YearsOfPassing' => $input['eed_YearsOfPassing_' . $i], 'eed_Notes' => $input['eed_Notes_' . $i], 'eed_PicturePath' => $filename];
            }

            EmployeesEducationDetail::where('fkEedEmp', $id)->delete();

            EmployeesEducationDetail::insert($details);

            if ($user->email != $input['email']) {
                $current_time = date("Y-m-d H:i:s");
                $verification_key = md5(FrontHelper::generatePassword(20));

                $reset_pass_token = base64_encode($input['email'] . '&&Teacher&&' . $current_time);
                $data = ['email' => $input['email'], 'name' => $input['emp_EmployeeName'] . " " . $input['emp_EmployeeSurname'], 'school' => $SchoolData->sch_SchoolName_en, 'verify_key' => $verification_key, 'reset_pass_link' => $reset_pass_token, 'subject' => 'New School Teacher Credentials'];

                MailHelper::sendNewTeacherCredentials($data);

                $user->email = $input['email'];
                $user->email_verified_at = null;
                $user->email_verification_key = $verification_key;
            }

            if ($user->save()) {
                $response['status'] = true;
                $response['message'] = trans('message.msg_Teacher_update_success');
                $response['redirect'] = url('/employee/employees');
            } else {
                $response['status'] = false;
                $response['message'] = trans('message.msg_something_wrong');
            }
        } else {
            $image = $request->file('upload_profile');
            $pkSch = $input['sid'];

            $temp_pass = FrontHelper::generatePassword(10);
            $verification_key = md5(FrontHelper::generatePassword(20));

            $govIdExistCount = 0;
            if (!empty($input['emp_EmployeeID'])) {
                $govIdExistCount = Employee::where('emp_EmployeeID', '=', $input['emp_EmployeeID'])->count();
            }
            $tempIdExistCount = 0;
            if (!empty($input['emp_TempCitizenId'])) {
                $tempIdExistCount = Employee::where('emp_TempCitizenId', '=', $input['emp_TempCitizenId'])->where('emp_TempCitizenId', '!=', null)
                    ->count();
            }
            $emailExistCount = Employee::where('email', '=', $input['email'])->count();
            $emailAdmExistCount = Admin::where('email', '=', $input['email'])->count();

            if ($emailExistCount != 0 || $emailAdmExistCount != 0) {
                $response['status'] = false;
                $response['message'] = trans('message.msg_email_exist');
                return response()
                    ->json($response);
            }

            if ($govIdExistCount != 0) {
                $response['status'] = false;
                $response['message'] = trans('message.msg_employee_id_exist');
                return response()
                    ->json($response);
            }

            if ($tempIdExistCount != 0) {
                $response['status'] = false;
                $response['message'] = trans('message.msg_temp_citizen_id_exist');
                return response()
                    ->json($response);
            }

            $user = new Employee;
            $user->email = $input['email'];
            $user->emp_EmployeeName = $input['emp_EmployeeName'];
            $user->emp_EmployeeSurname = $input['emp_EmployeeSurname'];
            $user->emp_EmployeeID = $input['emp_EmployeeID'];
            $user->emp_TempCitizenId = $input['emp_TempCitizenId'];
            $user->emp_EmployeeGender = $input['emp_EmployeeGender'];
            $user->emp_DateOfBirth = UtilHelper::sqlDate($input['emp_DateOfBirth']);
            $user->emp_PlaceOfBirth = $input['emp_PlaceOfBirth'];
            $user->emp_Address = $input['emp_Address'];
            $user->emp_PhoneNumber = $input['emp_PhoneNumber'];
            $user->emp_Notes = $input['emp_Notes'];
            $user->fkEmpMun = $input['fkEmpMun'];
            $user->fkEmpPof = $input['fkEmpPof'];
            $user->fkEmpCny = $input['fkEmpCny'];
            $user->fkEmpNat = $input['fkEmpNat'];
            $user->fkEmpRel = $input['fkEmpRel'];
            $user->fkEmpCtz = $input['fkEmpCtz'];
            $user->email_verification_key = $verification_key;

            if (!empty($image)) {
                $input['emp_PicturePath'] = time() . '.' . $image->getClientOriginalExtension();
                $destinationPath = storage_path('app/public/images/users');
                $image->move($destinationPath, $input['emp_PicturePath']);
                $user->emp_PicturePath = $input['emp_PicturePath'];
            }

            $SchoolData = School::select('sch_SchoolName_en')->where('pkSch', $pkSch)->first();

            if ($user->save()) {
                $SchoolCoordinator = '';
                $Principal = '';
                $Teacher = '';

                if (!empty($input['fkEenEpty'])) {
                    $EmployeeTypes = $this->getSelectedRoles([$this->employees[0], $this->employees[1], $this->employees[2]], false);

                    foreach ($EmployeeTypes as $k => $v) {
                        if ($v->epty_Name == 'SchoolCoordinator') {
                            $SchoolCoordinator = $v->pkEpty;
                        }
                        if ($v->epty_Name == 'Principal') {
                            $Principal = $v->pkEpty;
                        }
                        if ($v->epty_Name == 'Teacher') {
                            $Teacher = $v->pkEpty;
                        }
                    }

                    $ee = new EmployeesEngagement;
                    $ee->fkEenSch = $pkSch;
                    $ee->fkEenEmp = $user->id;
                    $ee->fkEenEty = $input['fkEenEty'];
                    if (isset($input['een_ActingAsPrincipal']) && $input['fkEenEpty'] == 1) {
                        $ee->een_ActingAsPrincipal = "Yes";
                    }
                    $ee->fkEenEpty = $input['fkEenEpty'];
                    $ee->een_Notes = $input['een_Notes'];
                    $ee->een_DateOfEngagement = UtilHelper::sqlDate($input['start_date']);
                    if (isset($input['end_date']) && !empty($input['end_date'])) {
                        $ee->een_DateOfFinishEngagement = UtilHelper::sqlDate($input['end_date']);
                    }
                    $ee->save();

                    if ($ee->save()) {
                        EmployeesWeekHourRates::addHourlyRate($ee->pkEen, $input['ewh_WeeklyHoursRate'], $input['ewh_Notes'], UtilHelper::sqlDate($input['start_date']));
                    }
                }

                $tdata = $input['total_details'];
                if ($tdata > 0) {
                    $details = [];
                    for ($i = 1; $i <= $tdata; $i++) {
                        $filename = '';
                        $image = '';
                        if ($request->hasFile('eed_DiplomaPicturePath_' . $i)) {
                            $image = $request->file('eed_DiplomaPicturePath_' . $i);
                            $filename = time() . $i . '.' . $image->getClientOriginalExtension();
                            $destinationPath = storage_path('app/public/files/users');
                            $image->move($destinationPath, $filename);
                        } else {
                            $filename = $input['file_name_' . $i];
                        }
                        $details[] = ['fkEedEmp' => $user->id, 'fkEedCol' => $input['fkEedCol_' . $i], 'fkEedUni' => $input['fkEedUni_' . $i], 'fkEedAcd' => $input['fkEedAcd_' . $i], 'fkEedQde' => $input['fkEedQde_' . $i], 'fkEedEde' => $input['fkEedEde_' . $i], 'eed_ShortTitle' => $input['eed_ShortTitle_' . $i], 'eed_SemesterNumbers' => $input['eed_SemesterNumbers_' . $i], 'eed_EctsPoints' => $input['eed_EctsPoints_' . $i], 'eed_YearsOfPassing' => $input['eed_YearsOfPassing_' . $i], 'eed_Notes' => $input['eed_Notes_' . $i], 'eed_PicturePath' => $filename];
                    }

                    EmployeesEducationDetail::insert($details);
                }

                $current_time = date("Y-m-d H:i:s");
                $reset_pass_token = base64_encode($input['email'] . '&&Teacher&&' . $current_time);
                $data = ['email' => $input['email'], 'name' => $input['emp_EmployeeName'] . " " . $input['emp_EmployeeSurname'], 'school' => $SchoolData->sch_SchoolName_en, 'verify_key' => $verification_key, 'reset_pass_link' => $reset_pass_token];

                if (!empty($input['fkEenEpty'])) {
                    if ($input['fkEenEpty'] == $Principal) {
                        $data['subject'] = 'New School Principal Credentials';
                        MailHelper::sendNewPrincipalCredentials($data);
                        EmployeesEngagement::where('fkEenSch', $pkSch)->where('fkEenEpty', $Principal)->where('fkEenEmp', '!=', $user->id)
                            ->where('een_DateOfFinishEngagement', '=', null)
                            ->update(['een_DateOfFinishEngagement' => UtilHelper::sqlDate($input['start_date'])]);

                    } elseif ($input['fkEenEpty'] == $Teacher) {
                        $data['subject'] = 'New School Teacher Credentials';
                        MailHelper::sendNewTeacherCredentials($data);
                    }
                }

                $response['status'] = true;
                $response['message'] = trans('message.msg_Teacher_add_success');
                $response['redirect'] = url('/employee/employees');
            } else {
                $response['status'] = false;
                $response['message'] = trans('message.msg_something_wrong');
            }
        }

        return response()->json($response);

    }

    /**
     * Edit Employee Page
     * @param  int $id
     * @return view - employee.employees.editEmployee
     */
    public function edit($id)
    {
        $response = [];
        $AcademicDegrees = AcademicDegree::select('pkAcd', 'acd_AcademicDegreeName_' . $this->current_language . ' as acd_AcademicDegreeName')
            ->get();
        $Colleges = College::select('pkCol', 'col_CollegeName_' . $this->current_language . ' as col_CollegeName')
            ->get();
        $Countries = Country::select('pkCny', 'cny_CountryName_' . $this->current_language . ' as cny_CountryName')
            ->get();
        $QualificationDegrees = QualificationDegree::select('pkQde', 'qde_QualificationDegreeName_' . $this->current_language . ' as qde_QualificationDegreeName')
            ->get();

        $Municipalities = $this->getMuncipalityDropdownData($this->current_language);
        $PostalCodes = $this->getPostalCodeDropdownData($this->current_language);

        $Citizenships = Citizenship::select('pkCtz', 'ctz_CitizenshipName_' . $this->current_language . ' as ctz_CitizenshipName')
            ->get();
        $Nationalities = Nationality::select('pkNat', 'nat_NationalityName_' . $this->current_language . ' as nat_NationalityName')
            ->get();
        $Religions = Religion::select('pkRel', 'rel_ReligionName_' . $this->current_language . ' as rel_ReligionName')
            ->get();
        $Universities = University::select('pkUni', 'uni_UniversityName_' . $this->current_language . ' as uni_UniversityName')
            ->get();
        $EmployeeDesignations = EmployeeDesignation::select('pkEde', 'ede_EmployeeDesignationName_' . $this->current_language . ' as ede_EmployeeDesignationName')
            ->get();
        $Schools = School::select('pkSch', 'sch_SchoolName_' . $this->current_language . ' as sch_SchoolName')
            ->get();
        $EngagementTypes = EngagementType::select('pkEty', 'ety_EngagementTypeName_' . $this->current_language . ' as ety_EngagementTypeName')
            ->get();
        $mainSchool = $this
            ->logged_user->sid;
        $employeeType = EmployeeType::select('pkEpty', 'epty_Name')->where('epty_Name', '!=', 'SchoolSubAdmin')
            ->where('epty_Name', '!=', 'SchoolCoordinator')
            ->get();

        $EmployeesDetail = Employee::with(['employeeEducation',
            'employeeEducation.academicDegree' => function ($query) {
                $query->select('pkAcd', 'acd_AcademicDegreeName_' . $this->current_language . ' as acd_AcademicDegreeName');
            }
            , 'employeeEducation.college' => function ($query) {
                $query->select('pkCol', 'col_CollegeName_' . $this->current_language . ' as col_CollegeName');
            }
            , 'employeeEducation.university' => function ($query) {
                $query->select('pkUni', 'uni_UniversityName_' . $this->current_language . ' as uni_UniversityName');
            }
            , 'employeeEducation.university.college', 'employeeEducation.qualificationDegree' => function ($query) {
                $query->select('pkQde', 'qde_QualificationDegreeName_' . $this->current_language . ' as qde_QualificationDegreeName');
            }
            , 'employeeEducation.employeeDesignation' => function ($query) {
                $query->select('pkEde', 'ede_EmployeeDesignationName_' . $this->current_language . ' as ede_EmployeeDesignationName');
            }
            , 'municipality' => function ($query) {
                $query->select('pkMun', 'mun_MunicipalityName_' . $this->current_language . ' as mun_MunicipalityName');
            }
            , 'postalCode' => function ($query) {
                $query->select('pkPof', 'pof_PostOfficeNumber');
            }
            , 'country' => function ($query) {
                $query->select('pkCny', 'cny_CountryName_' . $this->current_language . ' as cny_CountryName');
            }
            , 'nationality' => function ($query) {
                $query->select('pkNat', 'nat_NationalityName_' . $this->current_language . ' as nat_NationalityName');
            }
            , 'religion' => function ($query) {
                $query->select('pkRel', 'rel_ReligionName_' . $this->current_language . ' as rel_ReligionName');
            }
            , 'citizenship' => function ($query) {
                $query->select('pkCtz', 'ctz_CitizenshipName_' . $this->current_language . ' as ctz_CitizenshipName');
            }
            , "EmployeesEngagement" => function ($q) use ($id, $mainSchool) {
                $q->whereNull('een_DateOfFinishEngagement')->where('fkEenEmp', $id)->where('fkEenSch', $mainSchool);
            }
            , 'EmployeesEngagement.engagementType' => function ($query) {
                $query->select('pkEty', 'ety_EngagementTypeName_' . $this->current_language . ' as ety_EngagementTypeName');
            }
            , 'EmployeesEngagement.getLatestHourRates'
            , "employeesEngagement.getAllHourRates" => function ($q) {
                $q->orderBy('pkEwh', 'DESC');
            }
            , 'EmployeesEngagement.school' => function ($query) {
                $query->select('pkSch', 'sch_SchoolName_' . $this->current_language . ' as sch_SchoolName');
            }
            , 'EmployeesEngagement.employeeType',
        ]);
        $EmployeesDetail = $EmployeesDetail->where('id', '=', $id)->first();

        $datas = ['employeeType' => $employeeType, 'Countries' => $Countries, 'Municipalities' => $Municipalities, 'PostalCodes' => $PostalCodes, 'Citizenships' => $Citizenships, 'Nationalities' => $Nationalities, 'Religions' => $Religions, 'Universities' => $Universities, 'Colleges' => $Colleges, 'EmployeeDesignations' => $EmployeeDesignations, 'QualificationDegrees' => $QualificationDegrees, 'AcademicDegrees' => $AcademicDegrees, 'EmployeesDetail' => $EmployeesDetail, 'Schools' => $Schools, 'EngagementTypes' => $EngagementTypes, 'mainSchool' => $mainSchool];

        if (request()
            ->ajax()) {
            return \View::make('employee.employees.editEmployee')
                ->with($datas)->renderSections();
        }
        return view('employee.employees.editEmployee')
            ->with($datas);
    }

    /**
     * View Employee Page
     * @param  int $id
     * @return view - employee.employees.viewEmployee
     */
    public function show($id)
    {
        $mdata = '';
        if (!empty($id)) {

            $mainSchool = $this
                ->logged_user->sid;

            $mdata = Employee::with(['country' => function ($query) {
                $query->select('pkCny', 'cny_CountryName_' . $this->current_language . ' as cny_CountryName');
            }
                , "employeesEngagement" => function ($q) use ($id, $mainSchool) {
                    $q->whereNull('een_DateOfFinishEngagement')->where('fkEenEmp', $id)->where('fkEenSch', $mainSchool);
                }
                , "employeesEngagement.getLatestHourRates"
                , "employeesEngagement.getAllHourRates" => function ($q) {
                    $q->orderBy('pkEwh', 'DESC');
                }
                , "employeesEngagement.school" => function ($q) {
                    $q->select('pkSch', 'sch_SchoolName_' . $this->current_language . ' as sch_SchoolName');
                }
                , 'employeesEngagement.employeeType', 'employeesEngagement.engagementType' => function ($query) {
                    $query->select('pkEty', 'ety_EngagementTypeName_' . $this->current_language . ' as ety_EngagementTypeName');
                }
            ])
                ->where('id', $id)->first();

        }

        if (request()
            ->ajax()) {
            return \View::make('employee.employees.viewEmployee')
                ->with('mdata', $mdata)->renderSections();
        }
        return view('employee.employees.viewEmployee', ['mdata' => $mdata]);
    }

    /**
     * Delete Employee
     * @param  Request $request
     * @return JSON $response
     */
    public function destroy($cid)
    {
        $response = [];

        if (empty($cid)) {
            $response['status'] = false;
        } else {
            Employee::where('id', $cid)->where('type', 'TeacherAdmin')
                ->delete();
            $response['status'] = true;
        }

        return response()->json($response);
    }

    /**
     * Engage Employee Page
     * @param  Request $request
     * @return view - employee.employees.engageEmployee
     */
    public function engageEmployee(Request $request)
    {
        $mainSchool = $this
            ->logged_user->sid;

        $employeeType = EmployeeType::select('pkEpty', 'epty_Name')->where('epty_Name', '!=', 'SchoolSubAdmin')
            ->where('epty_Name', '!=', 'SchoolCoordinator')
            ->get();

        $EngagementTypes = EngagementType::select('pkEty', 'ety_EngagementTypeName_' . $this->current_language . ' as ety_EngagementTypeName')
            ->get();

        $EmployeeTypes = EmployeeType::where('epty_Name', '=', 'Teacher')->orWhere('epty_Name', '=', 'Principal')
            ->get();

        if (request()
            ->ajax()) {
            return \View::make('employee.employees.engageEmployee')
                ->with(['employeeType' => $employeeType, 'EngagementTypes' => $EngagementTypes, 'EmployeeTypes' => $EmployeeTypes])->renderSections();
        }
        return view('employee.employees.engageEmployee', ['employeeType' => $employeeType, 'EngagementTypes' => $EngagementTypes, 'EmployeeTypes' => $EmployeeTypes]);
    }

    /**
     * Save Employee Engagement
     * @param  Request $request
     * @return JSON $response
     */
    public function engageEmployeePost(Request $request)
    {
        try {
            $response = [];
            $input = $request->all();

            $mainSchool = $this
                ->logged_user->sid;
            $SchoolData = School::select('sch_SchoolName_en')->where('pkSch', $mainSchool)->first();

            $EmployeeData = Employee::find($input['eid']);

            $count = EmployeesEngagement::where('fkEenSch', $mainSchool)
                ->where('fkEenEmp', $input['eid'])
                ->where('fkEenEpty', $input['fkEenEpty'])
                ->whereNull('een_DateOfFinishEngagement')
                ->count();

            if ($count) {
                $response['status'] = false;
                $response['message'] = trans('message.msg_employee_enagement_exist');
                return response()
                    ->json($response);
            }

            if (isset($EmployeeData) && $EmployeeData->emp_EmployeeID == null && $EmployeeData->emp_TempCitizenId == null) {
                $response['status'] = false;
                $response['message'] = trans('message.msg_id_does_not_exist');
                return response()
                    ->json($response);
            }

            $data = ['email' => $EmployeeData->email, 'name' => $EmployeeData->emp_EmployeeName . " " . $EmployeeData->emp_EmployeeSurname, 'school' => $SchoolData->sch_SchoolName_en];

            if ($input['fkEenEpty'] == 1) {
                EmployeesEngagement::where('fkEenEpty', $input['fkEenEpty'])
                    ->where('fkEenSch', $mainSchool)
                    ->update(['een_DateOfFinishEngagement' => UtilHelper::sqlDate($input['start_date'])]);
            }

            // if ($input['fkEenEpty'] != 1)
            // {
            //     $count = EmployeesEngagement::where('fkEenSch',$mainSchool)
            //     ->where('fkEenEmp',$input['eid'])
            //     ->where('fkEenEpty',$input['fkEenEpty'])
            //     ->whereNull('een_DateOfFinishEngagement')
            //     ->count();

            //     if($count)
            //     {
            //         $response['status'] = false;
            //         $response['message'] = trans('message.msg_employee_enagement_exist');
            //         return response()
            //             ->json($response);
            //     }
            // }

            if (isset($input['een_ActingAsPrincipal']) && $input['fkEenEpty'] == 1) {
                $acting = "Yes";
            } else {
                $acting = "No";
            }

            $ee = new EmployeesEngagement;
            $ee->fkEenSch = $mainSchool;
            $ee->een_ActingAsPrincipal = $acting;
            $ee->fkEenEmp = $input['eid'];
            $ee->fkEenEty = $input['fkEenEty'];
            $ee->fkEenEpty = $input['fkEenEpty'];
            $ee->een_notes = $input['een_notes'];
            $ee->een_DateOfEngagement = UtilHelper::sqlDate($input['start_date']);
            if (isset($input['end_date']) && !empty($input['end_date'])) {
                $ee->een_DateOfFinishEngagement = UtilHelper::sqlDate($input['end_date']);
            }

            if ($ee->save()) {
                EmployeesWeekHourRates::addHourlyRate($ee->pkEen, $input['ewh_WeeklyHoursRate'], $input['ewh_Notes'], UtilHelper::sqlDate($input['start_date']));
                if ($input['fkEenEpty'] == 1) {
                    $data['subject'] = 'New Principal Assign';
                    MailHelper::sendNewPrincipalAssign($data);
                }
                if ($input['fkEenEpty'] == 4) {
                    $data['subject'] = 'New Teacher Assign';
                    MailHelper::sendNewTeacherAssign($data);
                }
                $response['status'] = true;
                $response['message'] = trans('message.msg_employee_engage_success');
            } else {
                $response['status'] = false;
                $response['message'] = trans('message.msg_something_wrong');
            }

            return response()
                ->json($response);
        } catch (\Exception $e) {

            $response['status'] = false;
            $response['message'] = trans('message.msg_something_wrong');

            return response()
                ->json($response);
        }
    }

    /**
     * Fetch Employee Data
     * @param  Request $request
     * @return JSON $result
     */
    public function employees_list(Request $request)
    {
        $data = $request->all();

        $perpage = !empty($data['length']) ? (int) $data['length'] : 10;

        $filter = isset($data['search']) && is_string($data['search']) ? $data['search'] : '';

        $sort_type = isset($data['order'][0]['dir']) && is_string($data['order'][0]['dir']) ? $data['order'][0]['dir'] : '';
        $sort_col = isset($data['order'][0]['column']) ? $data['order'][0]['column'] : '';

        $sort_field = isset($data['columns'][$sort_col]['data']) ? $data['columns'][$sort_col]['data'] : '';
        $mainSchool = $this
            ->logged_user->sid;
        $start_date = '';

        $end_date = '';
        $emp_type = [];
        if (isset($data['emp_type']) && !empty($data['emp_type'])) {
            $emp_type[] = $data['emp_type'];
        }
        $allowedIn = isset($data['emp_type']) ? $emp_type : [$this->employees[0], $this->employees[1]];
        $allowedTypes = $this->getSelectedRoles($allowedIn);

        $Teacher = Employee::whereHas("employeesEngagement", function ($q) use ($filter, $allowedTypes, $mainSchool) {
            $q->where('een_DateOfFinishEngagement', '=', null)
                ->whereIn('fkEenEpty', $allowedTypes)->where('fkEenSch', $mainSchool);
        })->with(["employeesEngagement" => function ($q) use ($filter, $allowedTypes, $mainSchool) {
            $q->where('een_DateOfFinishEngagement', '=', null)
                ->whereIn('fkEenEpty', $allowedTypes)->where('fkEenSch', $mainSchool);
        }
            , 'employeesEngagement.employeeType', 'employeesEngagement.engagementType' => function ($query) {
                $query->select('pkEty', 'ety_EngagementTypeName_' . $this->current_language . ' as ety_EngagementTypeName');
            }
        ])
            ->where('id', '!=', $this
                    ->logged_user
                    ->id);

            if ($filter) {
            $Teacher = $Teacher->where(function ($query) use ($filter) {
                $query->where('emp_EmployeeID', 'LIKE', '%' . $filter . '%')->orWhere('emp_TempCitizenId', 'LIKE', '%' . $filter . '%')->orWhere('emp_EmployeeName', 'LIKE', '%' . $filter . '%')->orWhere('emp_EmployeeSurname', 'LIKE', '%' . $filter . '%')->orWhere('email', 'LIKE', '%' . $filter . '%');
            });
        }

        if ($sort_col != 0) {
            $Teacher = $Teacher->orderBy($sort_field, $sort_type);
        }

        $total_employees = $Teacher->count();

        $offset = $data['start'];
        $counter = $offset;
        $Teacher = $Teacher->offset($offset)->limit($perpage)->get()
            ->toArray();

        $teacherdata = [];

        foreach ($Teacher as $key => $value) {
            if (!empty($value['employees_engagement'])) {

                $type = [];
                foreach ($value['employees_engagement'] as $k => $v) {
                    if ($v['employee_type']['epty_subCatName'] != '') {
                        $type[] = $v['employee_type']['epty_subCatName'];
                    } else {
                        $type[] = $v['employee_type']['epty_Name'];
                    }
                }
                $etype = implode(", ", $type);

                if ($value['emp_Status'] == 'Active') {
                    $emp_status = trans('general.gn_active');
                } else {
                    $emp_status = trans('general.gn_inactive');
                }

                $found = array_column($teacherdata, 'id');
                if (!in_array($value['id'], $found)) {
                    $teacherdata[] = ['index' => $counter + 1, 'id' => $value['id'], 'emp_EmployeeID' => $value['emp_EmployeeID'], 'emp_TempCitizenId' => $value['emp_TempCitizenId'], 'type' => $etype, 'email' => $value['email'], 'emp_EmployeeSurname' => $value['emp_EmployeeSurname'], 'emp_EmployeeName' => $value['emp_EmployeeName'], 'emp_Status' => $emp_status];
                    $counter++;
                }
            }
        }
        //get all employee engage or not engage

        $price = array_column($teacherdata, 'index');

        if ($sort_col == 0) {
            if ($sort_type == 'desc') {
                array_multisort($price, SORT_DESC, $teacherdata);
            } else {
                array_multisort($price, SORT_ASC, $teacherdata);
            }
        }
        $teacherdata = array_values($teacherdata);
        $result = array(
            "draw" => $data['draw'],
            "recordsTotal" => $total_employees,
            "recordsFiltered" => $total_employees,
            "data" => $teacherdata,
        );

        return response()->json($result);
    }

    /**
     * Fetch Employee Engagment data
     * @param  Request $request
     * @return JSON $result
     */
    public function engage_employees_list(Request $request)
    {
        $data = $request->all();

        $perpage = !empty($data['length']) ? (int) $data['length'] : 10;

        $filter = isset($data['search']) && is_string($data['search']) ? $data['search'] : '';

        $sort_type = isset($data['order'][0]['dir']) && is_string($data['order'][0]['dir']) ? $data['order'][0]['dir'] : '';
        $sort_col = $data['order'][0]['column'];

        $sort_field = $data['columns'][$sort_col]['data'];
        $mainSchool = $this
            ->logged_user->sid;

        $start_date = '';

        $end_date = '';

        if (isset($data['emp_type']) && !empty($data['emp_type'])) {
            $emp_type[] = $data['emp_type'];
        }

        $allowedTypes = $this->getSelectedRoles([$this->employees[0], $this->employees[1], $this->employees[2]]);

        $Teacher = Employee::with(["employeesEngagement" => function ($q) use ($filter, $allowedTypes, $mainSchool) {
            $q->where('een_DateOfFinishEngagement', '=', null)
                ->whereIn('fkEenEpty', $allowedTypes)->where('fkEenSch', $mainSchool);
        }
            , 'employeesEngagement.employeeType', 'employeesEngagement.engagementType' => function ($query) {
                $query->select('pkEty', 'ety_EngagementTypeName_' . $this->current_language . ' as ety_EngagementTypeName');
            }
        ])
            ->where('id', '!=', $this
                    ->logged_user
                    ->id);

            if (isset($data['emp_type']) && !empty($data['emp_type']) || $data['emp_eng_type'] == 1) {

            $Teacher = $Teacher->whereHas('employeesEngagement', function ($query) use ($filter, $allowedTypes, $mainSchool) {
                $query->where('een_DateOfFinishEngagement', '=', null)
                    ->whereIn('fkEenEpty', $allowedTypes)->where('fkEenSch', $mainSchool);
            });

        }

        if ($filter) {
            $Teacher = $Teacher->where(function ($query) use ($filter) {
                $stringexplode = explode(" ", $filter);
                $firstname = $stringexplode[0] ?? $filter;
                $surname = $stringexplode[1] ?? $filter;

                $query->where('emp_EmployeeID', 'LIKE', '%' . $filter . '%')
                    ->orWhere('emp_EmployeeName', 'LIKE', '%' . $firstname . '%')
                    ->orWhere('emp_EmployeeSurname', 'LIKE', $surname . '%')
                    ->orWhere('email', 'LIKE', '%' . $filter . '%');
            });
        }

        if ($sort_col != 0) {
            $Teacher = $Teacher->orderBy($sort_field, $sort_type);
        }

        $total_admins = $Teacher->count();

        $offset = $data['start'];

        $counter = $offset;
        $teacherdata = [];

        $Teacher = $Teacher->offset($offset)->limit($perpage)->get()
            ->toArray();

        foreach ($Teacher as $key => $value) {
            $type1 = [];
            $engType = '';
            $engid = '';
            if (!empty($value['employees_engagement'])) {
                foreach ($value['employees_engagement'] as $k => $v) {
                    if ($v['employee_type']['epty_subCatName'] != '') {
                        $type = $v['employee_type']['epty_subCatName'];
                    } else {
                        $type = $v['employee_type']['epty_Name'];
                    }
                    $engType = $v['engagement_type']['ety_EngagementTypeName'];
                    $engid = $v['pkEen'];
                    $type1[] = $type;

                }
            }
            if ($data['emp_eng_type'] != 2) {
                $value['type'] = !empty($type1) ? $type1 : '';
                $value['engType'] = $engType;
                $value['eid'] = $engid;
                $value['index'] = $counter + 1;
                $teacherdata[$counter] = $value;
                $counter++;
            }
            if ($data['emp_eng_type'] == 2 && empty($value['employees_engagement'])) {
                $value['type'] = '';
                $value['engType'] = '';
                $value['eid'] = '';
                $value['index'] = $counter + 1;
                $teacherdata[$counter] = $value;
                $counter++;
            }
        }

        $price = array_column($teacherdata, 'index');

        if ($sort_col == 0) {
            if ($sort_type == 'desc') {
                array_multisort($price, SORT_DESC, $teacherdata);
            } else {
                array_multisort($price, SORT_ASC, $teacherdata);
            }
        }

        $teacherdata = array_values($teacherdata);
        $result = array(
            "draw" => $data['draw'],
            "recordsTotal" => $total_admins,
            "recordsFiltered" => $total_admins,
            "data" => $teacherdata,
        );

        return response()->json($result);
    }
}
