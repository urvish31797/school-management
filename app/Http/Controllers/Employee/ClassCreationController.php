<?php
/**
 * ClassCreationController
 *
 * @package    Laravel
 * @subpackage Controller
 * @since      1.0
 */

namespace App\Http\Controllers\Employee;

use App\Helpers\CustomLogHelper;
use App\Helpers\MailHelper;
use App\Helpers\UtilHelper;
use App\Http\Controllers\Controller;
use App\Http\Services\ClassCreationService;
use App\Http\Traits\CommonTrait;
use App\Models\ClassCreation;
use App\Models\ClassCreationGrades;
use App\Models\ClassCreationSemester;
use App\Models\Classes;
use App\Models\ClassStudentsAllocation;
use App\Models\ClassStudentsSemester;
use App\Models\ClassTeachersCourseAllocation;
use App\Models\EducationPeriod;
use App\Models\EducationPlan;
use App\Models\EducationPlansMandatoryCourse;
use App\Models\EducationProgram;
use App\Models\Employee;
use App\Models\EmployeesEngagement;
use App\Models\EnrollStudent;
use App\Models\Grade;
use App\Models\HomeRoomTeacher;
use App\Models\School;
use App\Models\SchoolYear;
use App\Models\Student;
use App\Models\VillageSchool;
use Illuminate\Http\Request;
use Session;

class ClassCreationController extends Controller
{
    use CommonTrait;
    public $forHomeroomTeacherallowedTypes = [];
    public $selectedCoursesd = [];
    public $g = '';
    public $result = [];
    public $gradeData = [];

    /**
     * Class Creations Listing Page
     * @param  Request $request
     * @return view - employee.classCreation.classCreations
     */
    public function index(Request $request)
    {
        $searchSchYear = SchoolYear::select('pkSye', 'sye_NameNumeric', 'sye_DefaultYear', 'sye_NameCharacter_' . $this->current_language . ' AS sye_NameCharacter')
            ->get()
            ->toArray();
        $searchGrade = Grade::select('pkGra', 'gra_GradeNumeric')
            ->get()
            ->toArray();

        /** This session needs to be forget because we can leave from half form in class creation so there is no other way to forget so don't remove */
        if (Session::has('homeroometeacherset')) {
            Session::forget('homeroometeacherset');
        }

        /** This session needs to be forget because we can leave from half form in class creation so there is no other way to forget so don't remove */
        if (Session::has('selected_grades')) {
            Session::forget('selected_grades');
        }

        /** This session needs to be forget because we can leave from half form in class creation so there is no other way to forget so don't remove */
        if (Session::has('pkClr')) {
            $pkClr = Session::get('pkClr');
            Session::forget('pkClr');

            if (Session::has('stu_Courses_' . $pkClr)) {
                Session::forget('stu_Courses_' . $pkClr);
            }

            if (Session::has('step_2_filter_courses_' . $pkClr)) {
                Session::forget('step_2_filter_courses_' . $pkClr);
            }

            if (Session::has('cc_step_2_' . $pkClr)) {
                Session::forget('cc_step_2_' . $pkClr);
            }

            if (Session::has('cc_step_2_epl_' . $pkClr)) {
                Session::forget('cc_step_2_epl_' . $pkClr);
            }

            if (Session::has('cc_step_3_' . $pkClr)) {
                Session::forget('cc_step_3_' . $pkClr);
            }

            if (Session::has('cc_step_3_selected_courses_' . $pkClr)) {
                Session::forget('cc_step_3_selected_courses_' . $pkClr);
            }
        }

        $mainSchool = $this->logged_user->sid;
        $currentSemRow = $this->getCurrentSemester($mainSchool);
        $showCloneButton = false;
        $nextSemester = '';
        $currentSemester = '';
        $currentSemesterId = 0;
        $nextSemesterId = 0;
        if ($currentSemRow->fkSchEdp < 3) {
            $showCloneButton = true;
            $currentSemesterId = $currentSemRow->fkSchEdp;
            $currentSemesterRow = EducationPeriod::find($currentSemesterId);
            $currentSemester = $currentSemesterRow['edp_EducationPeriodName_' . $this->current_language];
            $nextSemesterId = $currentSemesterId + 1;
            $nextSemesterRow = EducationPeriod::find($nextSemesterId);
            $nextSemester = $nextSemesterRow['edp_EducationPeriodName_' . $this->current_language];
        }

        $data = compact('searchSchYear', 'searchGrade', 'showCloneButton', 'nextSemester', 'currentSemester', 'currentSemesterId', 'nextSemesterId');

        return view('employee.classCreation.classCreations')
            ->with($data);
    }

    /**
     * Used for Class Creation
     * @param  Request $request
     * @return view - employee.classCreation.classCreation
     */
    public function create(Request $request)
    {
        $mainSchool = $this
            ->logged_user->sid;
        $this->forHomeroomTeacherallowedTypes = $this->getSelectedRoles([$this->employees[0], $this->employees[1], $this->employees[2]]);
        $schoolYears = SchoolYear::select('pkSye', 'sye_NameNumeric', 'sye_DefaultYear', 'sye_NameCharacter_' . $this->current_language . ' AS sye_NameCharacter')->get();
        $grades = Grade::select('pkGra', 'gra_GradeNumeric')
            ->get();
        $classes = Classes::select('pkCla', 'cla_ClassName_' . $this->current_language . ' as cla_ClassName')
            ->get();
        $semesters = EducationPeriod::select('pkEdp', 'edp_EducationPeriodName_' . $this->current_language . ' as edp_EducationPeriodName')
            ->get();

        $currentSemData = $this->getCurrentSemester($mainSchool);
        $currentSem = $currentSemData->fkSchEdp;

        $villageSchools = VillageSchool::select('pkVsc', 'vsc_VillageSchoolName_' . $this->current_language . ' as vsc_VillageSchoolName')
            ->where('fkVscSch', $mainSchool)->get();

        $employees = Employee::select('id',
            \DB::raw('(CASE
                        WHEN emp_EmployeeID IS NOT NULL THEN emp_EmployeeID
                        ELSE emp_TempCitizenId
                        END) AS empId'),
            \DB::raw("CONCAT(emp_EmployeeName, ' ', emp_EmployeeSurname) AS empName")
        )
            ->whereHas('EmployeesEngagement', function ($q) use ($mainSchool) {
                $q->whereIn('fkEenEpty', $this->forHomeroomTeacherallowedTypes)
                    ->where('fkEenSch', $mainSchool)
                    ->whereNull('een_DateOfFinishEngagement');
            })->get();

        $pageData = [
            'currentSem' => $currentSem,
            'classes' => $classes,
            'mainSchool' => $mainSchool,
            'schoolYears' => $schoolYears,
            'grades' => $grades,
            'semesters' => $semesters,
            'villageSchools' => $villageSchools,
            'employees' => $employees,
        ];

        if (request()
            ->ajax()) {
            return \View::make('employee.classCreation.classCreation')->with($pageData)->renderSections();
        }
        return view('employee.classCreation.classCreation')->with($pageData);
    }

    /**
     * View Class Creation Page
     * @param  Int $id
     * @return view - employee.classCreation.viewClassCreation
     */
    public function show($id)
    {
        $mdata = '';
        if (!empty($id)) {
            \DB::statement("SET SQL_MODE=''");
            $mdata = ClassCreation::whereHas('classCreationSemester', function ($q) use ($id) {
                $q->where('pkCcs', $id);
            })->with(['classCreationSemester' => function ($q) use ($id) {
                $q->where('pkCcs', $id);
            }
                , 'classCreationSchoolYear' => function ($q) {
                    $q->select('pkSye', 'sye_NameNumeric', 'sye_NameCharacter_' . $this->current_language . ' AS sye_NameCharacter');
                }
                , 'classCreationGrades.grade' => function ($q) {
                    $q->select('pkGra', 'gra_GradeNameRoman', 'gra_GradeNumeric');
                }
                , 'classCreationSemester.homeRoomTeacher.employeesEngagement.employee' => function ($q) {
                    $q->select('id', 'emp_EmployeeName', 'emp_EmployeeSurname');
                }
                , 'classCreationSemester.chiefStudent.student', 'classCreationSemester.treasureStudent.student', 'classCreationClasses' => function ($q) {
                    $q->select('pkCla', 'cla_ClassName_' . $this->current_language . ' as cla_ClassName');
                }
                , 'classCreationSemester.semester' => function ($q) {
                    $q->select('pkEdp', 'edp_EducationPeriodName_' . $this->current_language . ' as edp_EducationPeriodName');
                }
                , 'classCreationSemester.classStudentsSemester.studentEnroll.student', 'classCreationSemester.classStudentsSemester.studentEnroll.grade' => function ($q) {
                    $q->select('pkGra', 'gra_GradeNameRoman', 'gra_GradeNumeric');
                }
                , 'classCreationSemester.classStudentsSemester.studentEnroll.educationProgram' => function ($q) {
                    $q->select('pkEdp', 'edp_Name_' . $this->current_language . ' as edp_Name');
                }
                , 'classCreationSemester.classStudentsSemester.studentEnroll.educationPlan' => function ($q) {
                    $q->select('pkEpl', 'epl_EducationPlanName_' . $this->current_language . ' as epl_EducationPlanName');
                }
                ,
            ])
                ->first();

            $oldStuAlloc = ClassStudentsSemester::with([
                'studentEnroll',
                'classStudentsAllocation' => function ($q) {
                    $q->whereNull('fkCsaScg')->whereNull('fkCsaCga');
                },
                'classStudentsAllocation.classCreationTeachers',
            ])->where('fkSemCcs', '=', $id)->get();

            $createdCourses = [];
            $courseAlloc = $oldStuAlloc->pluck('classStudentsAllocation')->all();

            foreach ($courseAlloc as $key => $value) {
                foreach ($value as $data) {
                    if (!in_array($data['fkCsaEmc'], $createdCourses)) {
                        $createdCourses[] = $data['fkCsaEmc'];
                    }
                }

            }

            $oldTeachersSel = [];
            foreach ($oldStuAlloc as $k => $v) {
                foreach ($v->classStudentsAllocation as $kc => $vc) {
                    foreach ($vc->classCreationTeachers as $kt => $vt) {
                        $oldTeachersSel[$v
                                ->studentEnroll
                                ->fkSteGra][$v->studentEnroll->fkSteEpl][$vc->fkCsaEmc][] = $vt->fkCtcEeg;
                    }
                }
            }

            $oldTeachers = ClassTeachersCourseAllocation::select('pkCtc', 'fkCtcEeg', 'fkCtcCsa')
                ->where('fkCtcCcs', $id)->get();
            $oldTeachersId = $oldTeachers->pluck('fkCtcEeg')->all();

            $employees = EmployeesEngagement::with(['employee' => function ($q) { //Employee engagment id with employee
                $q->select('id', \DB::raw("CONCAT(emp_EmployeeName, ' ', emp_EmployeeSurname) AS empName"));
            },
            ])
                ->whereNull('een_DateOfFinishEngagement')
                ->whereIn('pkEen', $oldTeachersId)->get()
                ->pluck('employee.empName', 'pkEen');

            $ccdata = ClassCreationSemester::select("fkCcsClr")->where('pkCcs', $id)->first();
            $grades = ClassCreationGrades::where('fkCcgClr', $ccdata->fkCcsClr)
                ->get()
                ->pluck('fkCcgGra')
                ->toArray();
            asort($grades);

            $selectetTeachers = [];

            $mainSchool = $this
                ->logged_user->sid;

            //fetch school courses
            $stuEnrollsIds = ClassStudentsSemester::where('fkSemCcs', $id)->pluck('fkSemSen')
                ->all();

            $educationPlans = EnrollStudent::whereIn('pkSte', $stuEnrollsIds)->get()
                ->pluck("fkSteEpl")
                ->all();

            $courses = Grade::select('*', 'gra_GradeNumeric')
                ->with(['educationPlansMandatoryCourse' => function ($query) use ($grades, $educationPlans, $createdCourses) {
                    $query->whereIn('fkEmcGra', $grades)
                        ->whereIn('fkEplCrs', $createdCourses)
                        ->whereIn('fkEmcEpl', $educationPlans);
                }
                    , 'educationPlansMandatoryCourse.mandatoryCourseGroup' => function ($query) use ($grades, $educationPlans) {
                        $query->select('pkCrs', 'crs_CourseName_' . $this->current_language . ' as crs_CourseName');
                    }
                    , 'educationPlansMandatoryCourse.educationPlan' => function ($query) use ($grades) {
                        $query->select('pkEpl', 'epl_EducationPlanName_' . $this->current_language . ' as epl_EducationPlanName');
                    },
                ])->whereIn('pkGra', $grades)->get();
        }

        if (request()
            ->ajax()) {
            return \View::make('employee.classCreation.viewClassCreation')
                ->with(['mdata' => $mdata, 'courses' => $courses, 'grades_ids' => $grades, 'employees' => $employees, 'existTeacher' => $oldTeachersSel])->renderSections();
        }
        return view('employee.classCreation.viewClassCreation', ['mdata' => $mdata, 'courses' => $courses, 'grades_ids' => $grades, 'employees' => $employees, 'existTeacher' => $oldTeachersSel]);
    }

    /**
     * Save Class Creation
     * @param  Request $request
     * @return JSON $response
     */
    public function store(Request $request)
    {
        $logHelper = new CustomLogHelper(['type' => "class_creation"]);
        $logHelper->addToLog("Class Creation Starts");

        $input = $request->all();
        $response = [];

        $mainSchool = $this->logged_user->sid;

        $allowedTypes = $this->getSelectedRoles([$this->employees[0]]);

        $class_step = $input['class_step'];
        $pkClr = $input['pkClr'];
        $pkCcs = $input['pkCcs'];

        unset($input['pkClr']);
        unset($input['pkCcs']);
        unset($input['class_step']);

        if ($class_step == 1) {
            try {
                $logHelper->addToLog("Start Step-1");
                $logHelper->addToLog("Step-1 Request:\n");
                $logHelper->addToLog(print_r($input, 1));

                $fkClrEdp = $input['fkClrEdp'];
                $grades = $input['fkClrGra'];
                $classId = $input['fkClrCla'];

                $empId = $input['empId'];
                $start_date = $input['start_date'];
                $een_Notes = $input['een_Notes'];
                $chiefStudent = $input['fkCcsSen'];
                $treasureStudent = $input['fkCcsSent'];
                unset($input['empId']);
                unset($input['start_date']);
                unset($input['een_Notes']);
                unset($input['fkCcsSen']);
                unset($input['fkCcsSent']);

                $oldClassCreationData = array();
                $previousGradeName = '';
                $selectedClass = '';
                $EndOfSchoolYearId = 3;
                if (count($grades) == 1) {
                    $gradData = Grade::where('pkGra', $grades[0])->first();
                    $gradId = $gradData->gra_GradeNumeric;
                    $previousGrade = 0;
                    if ($gradId > 1) {
                        $previousGrade = $gradId - 1;
                    }

                    $schoolYearData = SchoolYear::find($input['fkClrSye']);
                    $currentSchoolYear = $schoolYearData->sye_NameNumeric;
                    $previouseSchoolYear = $currentSchoolYear - 1;

                    $previouseSchoolYearData = SchoolYear::where('sye_NameNumeric', $previouseSchoolYear)->first();
                    $previousGradeData = Grade::where('gra_GradeNumeric', $previousGrade)->first();
                    if (!empty($previouseSchoolYearData) && !empty($previousGradeData)) {
                        $previousSchoolYearId = $previouseSchoolYearData->pkSye;
                        $previousGradeName = $previousGradeData->gra_GradeNameRoman;

                        $selectedClassData = Classes::select('pkCla', 'cla_ClassName_' . $this->current_language . ' as cla_ClassName')->where('pkCla', $classId)->first();
                        $selectedClass = $selectedClassData->cla_ClassName;

                        $oldClassCreationData = ClassCreationSemester::with([
                            'classCreation.classCreationClasses' => function ($q) {
                                $q->select('pkCla', 'cla_ClassName_' . $this->current_language . ' as cla_ClassName');
                            },
                            'classCreation.villageSchool' => function ($q) {
                                $q->select('pkVsc', 'vsc_VillageSchoolName_' . $this->current_language . ' as vsc_VillageSchoolName');
                            }])->whereHas('classCreation', function ($q) use ($previousSchoolYearId, $mainSchool) {
                            $q->where('fkClrSye', $previousSchoolYearId)
                                ->where('fkClrSch', $mainSchool)
                            // ->where('fkClrCla',$classId)
                                ->where('clr_Status', 'Publish');
                        })->where('fkCcsEdp', $EndOfSchoolYearId)->get()->toArray();
                    }
                }

                unset($input['fkClrGra']);

                $checkPrevSem = 0;

                if (!empty($pkClr)) {
                    //if updating existing class creation
                    $checkPrev = ClassCreation::select('pkClr')
                        ->where('fkClrSye', $input['fkClrSye'])
                        ->where('fkClrCla', $input['fkClrCla'])
                        ->where('fkClrSch', $input['fkClrSch'])
                        ->where('pkClr', '!=', $pkClr)
                        ->latest('pkClr');

                    if (!empty($input['fkClrVsc'])) {
                        $checkPrev = $checkPrev->where('fkClrVsc', $input['fkClrVsc'])->first();
                    } else {
                        $checkPrev = $checkPrev->first();
                    }

                    $oldEdpIds = ClassCreationSemester::where('fkCcsClr', '!=', $pkClr)->where('fkCcsEdp', $input['fkClrEdp'])->first();
                    if (!empty($oldEdpIds)) {
                        $checkPrevSem = 1;
                    }

                } else {
                    $checkPrev = ClassCreation::select('pkClr')
                        ->where('fkClrSye', $input['fkClrSye'])
                        ->where('fkClrCla', $input['fkClrCla'])
                        ->where('fkClrSch', $input['fkClrSch'])
                        ->latest('pkClr');

                    if (!empty($input['fkClrVsc'])) {
                        $checkPrev = $checkPrev->where('fkClrVsc', $input['fkClrVsc'])->first();
                    } else {
                        $checkPrev = $checkPrev->first();
                    }

                    if (!empty($checkPrev)) {
                        $oldEdpIds = ClassCreationSemester::where('fkCcsClr', $checkPrev->pkClr)
                            ->where('fkCcsEdp', $input['fkClrEdp'])->first();
                        if (!empty($oldEdpIds)) {
                            $checkPrevSem = 1;
                        }
                    }
                }

                $checkPrevGrades = 0;

                if (!empty($checkPrev)) {
                    $oldGradesIds = ClassCreationGrades::where('fkCcgClr', $checkPrev->pkClr)
                        ->get()
                        ->pluck("fkCcgGra")
                        ->all();

                    sort($grades);
                    sort($oldGradesIds);

                    if ($grades == $oldGradesIds) {
                        $checkPrevGrades = 1;
                    }

                }

                if ($checkPrevGrades != 0 && $checkPrevSem != 0) {
                    $response['status'] = false;
                    $response['message'] = trans('message.msg_class_creation_exist');
                } else {
                    $edp_id = $input['fkClrEdp'];
                    unset($input['fkClrEdp']);
                    if (!empty($pkClr)) {
                        ClassCreation::where('pkClr', $pkClr)->update($input);
                        $cid = $pkCcs;
                        $id = $pkClr;
                        ClassCreationSemester::where('pkCcs', $pkCcs)->update(['fkCcsEdp' => $fkClrEdp, 'fkCcsSen' => $chiefStudent, 'fkCcsSent' => $treasureStudent]);

                        //old homerroomteacher will be deleted
                        $homroomTeacherOld = HomeRoomTeacher::where('fkHrtCcs', $pkCcs)->first();
                        if (!empty($homroomTeacherOld)) {
                            $oldEngagementId = $homroomTeacherOld->fkHrtEen;
                            $hrtId = $homroomTeacherOld->pkHrt;

                            HomeRoomTeacher::find($hrtId)->delete();
                            EmployeesEngagement::find($oldEngagementId)->delete();
                        }
                    } else {
                        $id = ClassCreation::insertGetId($input);
                        $cid = ClassCreationSemester::insertGetId(['fkCcsEdp' => $fkClrEdp, 'fkCcsClr' => $id, 'fkCcsSen' => $chiefStudent, 'fkCcsSent' => $treasureStudent]);
                    }

                    $hcount = 0;
                    $existEnagement = EmployeesEngagement::where('fkEenEmp', $empId)
                        ->where('fkEenSch', $mainSchool)
                        ->whereNull('een_DateOfFinishEngagement')
                        ->orderBy('pkEen', 'DESC')
                        ->first();
                    $existHomeroom = EmployeesEngagement::where('pkEen', $existEnagement->pkEen)
                        ->where('fkEenEpty', 6)
                        ->first();

                    if (!empty($existHomeroom)) {
                        if (!empty($pkClr)) {
                            $classSemId = $pkCcs;
                        } else {
                            $classSemId = $cid;
                        }

                        $classesIds = ClassCreation::GetSchoolClasses(array('fkClrSch' => $input['fkClrSch'], 'fkClrSye' => $input['fkClrSye'], 'fkClrVsc' => $input['fkClrVsc']))->pluck('pkClr')->all();

                        if (!empty($classesIds)) {
                            $semester_ids = ClassCreationSemester::GetSemesterClasses($edp_id, $classesIds);
                            $hcount = HomeRoomTeacher::where('fkHrtCcs', '!=', $classSemId)
                                ->whereIn('fkHrtCcs', $semester_ids)
                                ->where('fkHrtEen', $existHomeroom->pkEen)
                                ->count();

                        }
                    }

                    if ($hcount > 0) {
                        $response['status'] = false;
                        $response['message'] = trans('message.msg_homeroomteacher_assigned');

                        return response()
                            ->json($response);
                    } else {
                        $TeacherEmp = EmployeesEngagement::select('pkEen')->where('fkEenSch', $mainSchool)
                            ->whereNull('een_DateOfFinishEngagement')
                            ->whereIn('fkEenEpty', $allowedTypes)
                            ->where('fkEenEmp', $existEnagement->fkEenEmp)
                            ->first();

                        if (!empty($TeacherEmp)) {
                            Session::put('homeroomteacheremp', $TeacherEmp->pkEen);
                        }

                        $replicateRow = $existEnagement->replicate();

                        //create new homeroomteacher
                        $replicateRow->fkEenEpty = 6; //6->homeroometeacher
                        $replicateRow->een_DateOfEngagement = UtilHelper::sqlDate($start_date);
                        $replicateRow->een_Notes = $een_Notes;
                        $replicateRow->save();

                        if ($replicateRow->save()) {
                            if (!empty($pkClr)) {
                                $ee = new HomeRoomTeacher;
                                $ee->fkHrtEen = $replicateRow->pkEen;
                                $ee->fkHrtCcs = $pkCcs;
                                $ee->save();
                                $heid = $ee->pkHrt;
                                Session::put('homeroometeacherset', $heid);
                            } else {
                                $ee = new HomeRoomTeacher;
                                $ee->fkHrtEen = $replicateRow->pkEen;
                                $ee->fkHrtCcs = $cid;
                                $ee->save();
                                $heid = $ee->pkHrt;
                                Session::put('homeroometeacherset', $heid);
                            }
                        }
                    }

                    if (!empty($id)) {
                        $oldGrades = [];
                        if (!empty($pkClr)) {
                            //if updating existing class creation
                            $oldGrades = ClassCreationGrades::where('fkCcgClr', $id)->get()
                                ->pluck("fkCcgGra")
                                ->all();

                            $oldStus = ClassStudentsSemester::with(['studentEnroll.student', 'studentEnroll.grade' => function ($q) {
                                $q->select('pkGra', 'gra_GradeNameRoman', 'gra_GradeNumeric');
                            }
                                , 'studentEnroll.educationProgram' => function ($q) {
                                    $q->select('pkEdp', 'edp_Name_' . $this->current_language . ' as edp_Name');
                                }
                                , 'studentEnroll.educationPlan' => function ($q) {
                                    $q->select('pkEpl', 'epl_EducationPlanName_' . $this->current_language . ' as epl_EducationPlanName');
                                },
                            ])
                                ->where('fkSemCcs', $cid)->get()
                                ->groupBy('fkSemSen');

                            $nextData = \View::make('employee.classCreation.classCreationHelper')->with(['data2' => $oldStus])->renderSections();
                            ClassCreationGrades::where('fkCcgClr', $id)->delete();
                        }

                        foreach ($grades as $k => $v) {
                            $gradesData[] = ['fkCcgClr' => $id, 'fkCcgGra' => $v];
                        }

                        $resultGra = array_diff($grades, $oldGrades);
                        if (!empty($resultGra)) {
                            $graChanged = true;
                        } else {
                            $graChanged = false;
                            if ($oldStus->count() != 0) {
                                $response['step_2_data'] = $nextData['step_2'];
                            }
                        }

                        ClassCreationGrades::insert($gradesData);

                        $response['status'] = true;
                        $response['message'] = trans('message.msg_class_creation_step_1_success');
                        $response['pkClr'] = $id;
                        $response['pkCcs'] = $cid;
                        $response['graChanged'] = $graChanged;
                        $response['selectedClass'] = $selectedClass;
                        $response['previousGradeName'] = $previousGradeName;
                        $response['oldClassCreationData'] = $oldClassCreationData;
                    } else {
                        $response['status'] = false;
                        $response['message'] = trans('message.msg_something_wrong');
                    }
                }

                $logHelper->addToLog("Step-1 Response:\n");
                $logHelper->addToLog(print_r($response, 1));
            } catch (\Exception $e) {
                $logHelper->addToLog("Class Creation Step-1 Error: \n" . print_r($e->getTraceAsString(), 1));
            }
        }

        if ($class_step == 2) {
            try {
                $logHelper->addToLog("Start Step-2");
                $logHelper->addToLog("Step-2 Request:\n");
                $logHelper->addToLog(print_r($input, 1));

                $stuIds = $input['stu_ids'];
                $input['pkClr'] = $pkClr;
                Session::put('pkClr', $pkClr);
                $educationPlans = $input['epl_ids'];
                $grades = $input['gra_id'];

                $checkSelectedStudentsStatus = (new ClassCreationService())->checkSelectedStudents($pkClr, $grades);
                if ($checkSelectedStudentsStatus) {
                    $response['status'] = false;
                    $response['message'] = trans('validation.validate_msg_select_students_from_selected_grade');

                    return response()->json($response);
                }

                $whereQry = '';
                for ($i = 0; $i < count($educationPlans); $i++) {
                    if (!empty($whereQry)) {
                        $whereQry .= " OR fkEmcGra='" . $grades[$i] . "' and fkEmcEpl='" . $educationPlans[$i] . "'";
                    } else {
                        $whereQry .= "fkEmcGra='" . $grades[$i] . "' and fkEmcEpl='" . $educationPlans[$i] . "'";
                    }
                }
                $grades = array_unique($grades);

                if (count(array_unique($educationPlans)) > 1 && count($grades) > 1) {
                    $response['status'] = false;
                    $response['message'] = trans('message.msg_combine_grade_edp_error');

                    return response()->json($response);
                }

                Session::put('selected_grades', $grades);
                Session::put('cc_step_2_epl_' . $pkClr, $input['epl_ids']);
                unset($input['epl_ids']);
                unset($input['student_listing_length']);
                Session::put('cc_step_2_' . $pkClr, $input);
                unset($input['stu_ids']);
                $logHelper->addToLog(print_r(Session::get('cc_step_2_' . $pkClr), 1));
                unset($input['gra_id']);

                asort($grades);
                $educationPlanDetails = Grade::select('*', 'gra_GradeNumeric')
                    ->with(['educationPlansMandatoryCourse' => function ($query) use ($whereQry) {
                        $query->whereRaw($whereQry)->orderBy('fkEmcEpl', 'ASC');
                    }
                        , 'educationPlansMandatoryCourse.mandatoryCourseGroup' => function ($query) use ($grades) {
                            $query->select('pkCrs', 'crs_CourseName_' . $this->current_language . ' as crs_CourseName')->orderBy('crs_CourseName', 'ASC');
                        }
                        , 'educationPlansMandatoryCourse.educationPlan' => function ($query) use ($grades) {
                            $query->select('pkEpl', 'epl_EducationPlanName_' . $this->current_language . ' as epl_EducationPlanName')->orderBy('epl_EducationPlanName', 'ASC');
                        },
                    ])
                    ->whereIn('pkGra', $grades)
                    ->get();

                //please dont remove this declaration. this is called deep clone
                //this clone use for show to remaining course list in step-3
                $educationPlanDetailsBackup = unserialize(serialize($educationPlanDetails));
                $mixedCourseClass = false;
                $multipleeducationPlan = false;
                $showBulkTeacherSelectionButton = false;

                // If we have different kind of education plans for same grade then
                // we need to find same courseid with same hours and creating array grade wise
                if (count(array_unique($educationPlans)) > 1 && count($grades) == 1 && !empty($educationPlanDetails)) {
                    $multipleeducationPlan = true;
                    $selectedCoursesdata = array();
                    $filteredSameGradeCourse = array();
                    foreach ($educationPlanDetails as $key => $val) {
                        foreach ($val->educationPlansMandatoryCourse as $ek => $vk) {
                            $data['crs'] = $vk->mandatoryCourseGroup->pkCrs;
                            $data['hrs'] = $vk->emc_hours;
                            $data['default'] = $vk->emc_default;
                            $data['pkEmc'] = $vk->pkEmc;
                            $data['fkEmcEpl'] = $vk->fkEmcEpl;
                            $selectedCoursesdata[$val->pkGra][] = $data;
                        }
                    }

                    $this->selectedCoursesd = $selectedCoursesdata;
                    $totalEducationPlan = count(array_unique($educationPlans));
                    foreach ($selectedCoursesdata as $k => $val) {
                        $this->g = $k;
                        array_walk($selectedCoursesdata[$k], array('self', "findUniqueCoursewithHours"));

                        foreach ($this->gradeData as $m => $d) {
                            // get unique pkemc value and push grade wise
                            $filteredSameGradeCourse[$m] = array_values(array_unique($d));
                        }
                    }

                    // Remove from course from collection if not found in filteredSameGradeCourse array
                    if (!empty($filteredSameGradeCourse)) {
                        //creating new array course wise and append courses array with different education plan in single course
                        $filterCourse = [];
                        foreach ($filteredSameGradeCourse as $key => $value) {
                            foreach ($value as $k => $v) {
                                foreach ($selectedCoursesdata as $sck => $scv) {
                                    foreach ($scv as $sk => $sv) {
                                        if ($v == $sv['pkEmc']) {
                                            $filterCourse[$sv['crs']][] = $sv;
                                        }
                                    }
                                }
                            }
                        }

                        //need to match 3 conditions Course ID, Course Hours, Default selected Yes only those course will be added in list
                        $selectedPkEmc = [];
                        foreach ($filterCourse as $key => $value) {
                            if (count($filterCourse[$key]) == $totalEducationPlan) {
                                $hrs = array_unique(array_column($filterCourse[$key], "hrs"));
                                $default = array_unique(array_column($filterCourse[$key], "default"));

                                if (count($hrs) == 1 && count($default) == 1 && $default[0] == "Yes") {
                                    $pkEmcs = array_column($filterCourse[$key], "pkEmc");
                                    foreach ($pkEmcs as $key => $value) {
                                        $selectedPkEmc[$this->g][] = $value;
                                    }
                                }
                            }
                        }

                        if (!empty($selectedPkEmc)) {
                            //remove pkemc which is not exists in selectedPkEmc
                            foreach ($filteredSameGradeCourse as $key => $value) {
                                foreach ($value as $k => $v) {
                                    if (!in_array($v, $selectedPkEmc[$key])) {
                                        unset($filteredSameGradeCourse[$key][$k]);
                                    }
                                }
                            }

                            $uniqueCourseOnly = [];
                            $removedCourse = [];
                            //remove courses from educationPlanDetails which pkEmc is not in exists in filteredSameGradeCourse
                            foreach ($educationPlanDetails as $key => $value) {
                                foreach ($value->educationPlansMandatoryCourse as $ekm => $vkm) {
                                    $grade = $value->pkGra;
                                    $course = $vkm->mandatoryCourseGroup->pkCrs;

                                    if (isset($filteredSameGradeCourse[$grade]) && !in_array($vkm->pkEmc, $filteredSameGradeCourse[$grade])) {
                                        $removedCourse[] = $vkm->pkEmc;
                                        $value->educationPlansMandatoryCourse->forget($ekm);
                                    } else {
                                        if (!in_array($course, $uniqueCourseOnly)) {
                                            $uniqueCourseOnly[] = $course;
                                        } else {
                                            $removedCourse[] = $vkm->pkEmc;
                                            $value->educationPlansMandatoryCourse->forget($ekm);
                                        }
                                    }
                                }

                                $uniqueCourseOnly = [];
                            }

                            //just need to show remaining course list which not matched with above criteria
                            if (!empty($removedCourse)) {
                                foreach ($educationPlanDetailsBackup as $k => $v) {
                                    foreach ($v->educationPlansMandatoryCourse as $ekm => $vkm) {
                                        $pkEmc = $vkm->pkEmc;
                                        if (!in_array($pkEmc, $removedCourse)) {
                                            $v->educationPlansMandatoryCourse->forget($ekm);
                                        }
                                    }
                                }
                            }
                            $mixedCourseClass = true;
                        } else {
                            $response['status'] = false;
                            $response['message'] = trans('message.msg_no_matches_courses');

                            return response()->json($response);
                        }
                    }

                    Session::put('step_2_filter_courses_' . $pkClr, $educationPlanDetails);
                }

                // If we have combine grade class with same education plan
                // we need to find same course in different grade and create course array
                if (count($grades) > 1 && count(array_unique($educationPlans)) == 1 && !empty($educationPlanDetails)) {
                    $selectedCoursesdata = array();
                    $totalUniqueGrades = count($grades);

                    // this loop to get course id and pkemc id
                    foreach ($educationPlanDetails as $key => $val) {
                        foreach ($val->educationPlansMandatoryCourse as $ek => $vk) {
                            $data['crs'] = $vk->mandatoryCourseGroup->pkCrs;
                            $data['pkEmc'] = $vk->pkEmc;
                            $data['default'] = $vk->emc_default;
                            $selectedCoursesdata[$val->pkGra][] = $data;
                        }
                    }

                    $mixedGradeCourses = [];
                    //combine different grades course in single array
                    foreach ($selectedCoursesdata as $k => $v) {
                        foreach ($v as $d => $m) {
                            $mixedGradeCourses[] = $m;
                        }
                    }

                    $coursesAry = [];
                    foreach ($mixedGradeCourses as $key => $value) {
                        $coursesAry[$value['crs']][] = $value;
                    }

                    $duplicateCourses = [];
                    foreach ($coursesAry as $key => $value) {
                        if (count($coursesAry[$key]) == $totalUniqueGrades) {
                            $default = array_unique(array_column($value, 'default'));
                            if (count($default) == 1 && $default[0] == "Yes") {
                                $duplicateCourses[] = $value[0]['crs'];
                            }
                        }
                    }

                    if (!empty($duplicateCourses)) {
                        $data = array_column($mixedGradeCourses, 'crs', 'pkEmc');
                        $finalpkEmc = array();
                        // this loop search in duplicate course and get pkemc
                        foreach ($data as $key => $value) {
                            if (in_array($value, $duplicateCourses)) {
                                $finalpkEmc[] = $key;
                            }
                        }

                        if (!empty($finalpkEmc)) {
                            $removedCourse = [];
                            foreach ($educationPlanDetails as $key => $value) {
                                foreach ($value->educationPlansMandatoryCourse as $ekm => $vkm) {
                                    if (!in_array($vkm->pkEmc, $finalpkEmc)) {
                                        $removedCourse[] = $vkm->pkEmc;
                                        $value->educationPlansMandatoryCourse->forget($ekm);
                                    }
                                }
                            }

                            //just need to show remaining course list which not matched with above criteria
                            if (!empty($removedCourse)) {
                                foreach ($educationPlanDetailsBackup as $k => $v) {
                                    foreach ($v->educationPlansMandatoryCourse as $ekm => $vkm) {
                                        $pkEmc = $vkm->pkEmc;
                                        if (!in_array($pkEmc, $removedCourse)) {
                                            $v->educationPlansMandatoryCourse->forget($ekm);
                                        }
                                    }
                                }
                                $mixedCourseClass = true;
                            }
                        }

                        Session::put('step_2_filter_courses_' . $pkClr, $educationPlanDetails);
                    } else {
                        $response['status'] = false;
                        $response['message'] = trans('message.msg_no_matches_courses');

                        return response()->json($response);
                    }
                }

                //add new button for bulk selection of homeroomteacher in step-3
                //condition : education program should be Elementry School & for 1 to 5 Grades Only
                if (!$multipleeducationPlan) {
                    $allowedGradeStatus = true;
                    $allowedEduStatus = false;
                    $allowedGrades = [1, 2, 3, 4, 5];
                    $gradesAry = $educationPlanDetails->toArray();
                    $grades = array_column($gradesAry, 'gra_GradeNumeric');
                    foreach ($grades as $val) {
                        if (!in_array($val, $allowedGrades)) {
                            $allowedGradeStatus = false;
                        }
                    }

                    $educationPlan = EducationPlan::where('pkEpl', $educationPlans[0])->first();
                    $educationprogram = EducationProgram::select('edp_Name_en')->where('pkEdp', $educationPlan->fkEplEdp)->first();

                    if ($educationprogram->edp_Name_en == "Elementary School") {
                        $allowedEduStatus = true;
                    }

                    if ($allowedGradeStatus && $allowedEduStatus) {
                        $showBulkTeacherSelectionButton = true;
                    }
                }

                $oldStuSemAllocId = ClassStudentsSemester::where('fkSemCcs', $pkCcs)->pluck('pkSem')
                    ->all();
                $oldStuAllocId = ClassStudentsAllocation::whereIn('fkCsaSem', $oldStuSemAllocId)->get()
                    ->pluck('pkCsa')
                    ->all();
                $oldStuSubClass = ClassStudentsAllocation::whereIn('fkCsaSem', $oldStuSemAllocId)->get()
                    ->pluck('fkCsaScg', 'fkCsaEmc')
                    ->all();

                $oldTeachers = ClassTeachersCourseAllocation::select('fkCtcEeg', 'fkCtcCsa')->whereNull('Ctc_end')
                    ->whereIn('fkCtcCsa', $oldStuAllocId)->get();
                $oldStuAlloc = ClassStudentsSemester::with([
                    'studentEnroll',
                    'classStudentsAllocation.classCreationTeachers',
                ])->where('fkSemCcs', '=', $pkCcs)->get();

                $oldTeachersSel = [];
                foreach ($oldStuAlloc as $k => $v) {
                    foreach ($v->classStudentsAllocation as $kc => $vc) {
                        foreach ($vc->classCreationTeachers as $kt => $vt) {
                            if (isset($oldTeachersSel[$v->studentEnroll->fkSteGra][$vc->fkCsaEmc])) {
                                if (!in_array($vt->fkCtcEeg, $oldTeachersSel[$v
                                    ->studentEnroll
                                    ->fkSteGra][$vc->fkCsaEmc])) {
                                    $oldTeachersSel[$v
                                            ->studentEnroll
                                            ->fkSteGra][$vc->fkCsaEmc][] = $vt->fkCtcEeg;
                                }
                            } else {
                                $oldTeachersSel[$v
                                        ->studentEnroll
                                        ->fkSteGra][$vc->fkCsaEmc][] = $vt->fkCtcEeg;
                            }

                        }
                    }
                }

                $employees = EmployeesEngagement::with(['employee' => function ($q) {
                    //Employee engagment id with employee
                    $q->select('emp_EmployeeName', 'emp_EmployeeSurname', 'id');
                },
                ])->where('fkEenSch', $mainSchool)->whereNull('een_DateOfFinishEngagement')->whereIn('fkEenEpty', $allowedTypes)->get();

                $step2data = ['data3' => $educationPlanDetails,
                    'employees' => $employees,
                    'grades' => $grades,
                    'mixedCourseClass' => $mixedCourseClass,
                    'educationPlanDetailsBackup' => $educationPlanDetailsBackup,
                    'existTeacher' => $oldTeachersSel,
                    'showBulkTeacherSelectionButton' => $showBulkTeacherSelectionButton,
                    'homeroomteacher' => Session::get('homeroomteacheremp'),
                ];

                $nextData = \View::make('employee.classCreation.classCreationHelper')->with($step2data)->renderSections();

                $response['status'] = true;
                $response['message'] = trans('message.msg_class_creation_step_2_success');
                $response['pkClr'] = $pkClr;
                $response['step_3_data'] = $nextData['step_3'];

                $logHelper->addToLog("Step-2 Response:\n");
                $logHelper->addToLog(print_r($response, 1));
            } catch (\Exception $e) {
                $logHelper->addToLog("Class Creation Step-2 Error: \n" . print_r($e->getTraceAsString(), 1));
            }
        }

        if ($class_step == 3) {
            try {
                $logHelper->addToLog("Start Step-3");
                $logHelper->addToLog("Step-3 Request:\n");
                $logHelper->addToLog(print_r($input, 1));

                Session::put('cc_step_3_' . $pkClr, $input);
                $selectedCourses = [];
                foreach ($input['courses'] as $key => $value) {
                    if (isset($input['fkCtcEeg_' . $key])) {
                        $selectedCourses[] = $value;
                    }
                }
                Session::put('cc_step_3_selected_courses_' . $pkClr, $selectedCourses);

                $selectedStudents = Session::get('cc_step_2_' . $pkClr);
                $logHelper->addToLog("Step-3 Students: \n" . print_r($selectedStudents, 1));
                if (!empty($selectedStudents) && !empty($pkClr)) {
                    $enrollIds = [];
                    foreach ($selectedStudents['stu_ids'] as $key => $value) {
                        $stuId = explode("_", $value);
                        $enrollIds[] = $stuId[1];
                    }

                    $students = EnrollStudent::with([
                        'student' => function ($q) {
                            $q->select('id', \DB::raw("CONCAT(stu_StudentName, ' ', stu_StudentSurname) AS stuName"));
                        },
                        'educationPlan' => function ($q) {
                            $q->select('pkEpl', 'epl_EducationPlanName_' . $this->current_language . ' AS epl_EducationPlanName');
                        },
                        'grade' => function ($q) {
                            $q->select('pkGra', 'gra_GradeNumeric');
                        },
                        'educationProgram' => function ($q) {
                            $q->select('pkEdp', 'edp_Name_' . $this->current_language . ' AS edp_Name');
                        },
                    ])->whereIn('pkSte', $enrollIds)->get()->toArray();

                    $studentsList = [];
                    if (!empty($students)) {
                        foreach ($students as $key => $value) {
                            $data['stuName'] = $value['student']['stuName'];
                            $data['edp_Name'] = $value['education_program']['edp_Name'] . " - " . $value['education_plan']['epl_EducationPlanName'];
                            $data['pkEpl'] = $value['education_plan']['pkEpl'];
                            $data['pkGra'] = $value['grade']['pkGra'];
                            $data['pkSte'] = $value['pkSte'];

                            $studentsList[$value['grade']['gra_GradeNumeric']][] = $data;
                        }

                        $step3data = ['data4' => $studentsList,
                            'pkClr' => $pkClr];

                        $nextData = \View::make('employee.classCreation.classCreationHelper')
                            ->with($step3data)->renderSections();
                    }

                    $response['status'] = true;
                    $response['message'] = trans('message.msg_class_creation_step_3_success');
                    $response['pkClr'] = $pkClr;
                    $response['step_4_data'] = $nextData['step_4'];
                } else {
                    $response['status'] = false;
                    $response['message'] = trans('message.msg_something_wrong');
                }

                $logHelper->addToLog("Step-3 Response:\n");
                $logHelper->addToLog(print_r($response, 1));
            } catch (\Exception $e) {
                $logHelper->addToLog("Class Creation Step-3 Error: \n" . print_r($e->getTraceAsString(), 1));
            }
        }

        if ($class_step == 4) {
            try {
                $logHelper->addToLog("Start Step-4");
                $logHelper->addToLog("Step-4 Request:\n");
                $logHelper->addToLog(print_r($input, 1));

                $step2data = Session::get('cc_step_2_' . $pkClr);
                $stus = $step2data['stu_ids'];
                $logHelper->addToLog("Step-4 Students: \n" . print_r($stus, 1));
                $tmp = [];
                foreach ($stus as $k => $v) {
                    $v = explode('_', $v);
                    $tmp[] = $v[1];
                }
                $logHelper->addToLog("Step-4 Students: \n" . print_r($tmp, 1));

                $heid = Session::get('homeroometeacherset');
                if (empty($heid)) {
                    $existTeacher = HomeRoomTeacher::where('fkHrtCcs', '=', $pkCcs)->where('fkHrtEen', $input['fkHrtEen'])->first();
                    $heid = $existTeacher->pkHrt;
                }

                if (isset($heid)) {
                    $classData = ClassCreation::whereHas('ClassCreationSemester', function ($q) use ($heid) {
                        $q->whereHas('homeRoomTeacher', function ($q1) use ($heid) {
                            $q1->where('pkHrt', $heid);
                        });
                    })->with(['classCreationSchoolYear' => function ($q) {
                        $q->select('pkSye', 'sye_NameNumeric', 'sye_NameCharacter_' . $this->current_language . ' AS sye_NameCharacter');
                    }
                        , 'classCreationGrades.grade' => function ($q) {
                            $q->select('pkGra', 'gra_GradeNameRoman', 'gra_GradeNumeric');
                        }
                        , 'classCreationSemester.homeRoomTeacher.employeesEngagement.employee' => function ($q) {
                            $q->select('id', 'emp_EmployeeName', 'emp_EmployeeSurname');
                        }
                        , 'classCreationSemester.chiefStudent.student', 'classCreationSemester.treasureStudent.student', 'classCreationClasses' => function ($q) {
                            $q->select('pkCla', 'cla_ClassName_' . $this->current_language . ' as cla_ClassName');
                        }
                        , 'classCreationSemester.semester' => function ($q) {
                            $q->select('pkEdp', 'edp_EducationPeriodName_' . $this->current_language . ' as edp_EducationPeriodName');
                        },
                    ])->where('pkClr', $pkClr)->first();

                    if (Session::get('selected_grades')) {
                        $grades = Session::get('selected_grades');
                        asort($grades);
                    } else {
                        $grades = ClassCreationGrades::where('fkCcgClr', $pkClr)->get()
                            ->pluck('fkCcgGra')
                            ->toArray();
                        asort($grades);
                    }

                    //fetch selected students
                    $classStudents = EnrollStudent::with(['student', 'grade' => function ($q) {
                        $q->select('pkGra', 'gra_GradeNameRoman', 'gra_GradeNumeric');
                    }
                        , 'educationProgram' => function ($q) {
                            $q->select('pkEdp', 'edp_Name_' . $this->current_language . ' as edp_Name');
                        }
                        , 'educationPlan' => function ($q) {
                            $q->select('pkEpl', 'epl_EducationPlanName_' . $this->current_language . ' as epl_EducationPlanName');
                        },
                    ])
                        ->whereIn('pkSte', $tmp)->get();
                    $selectetTeachers = [];

                    $educationPlans = Session::get('cc_step_2_epl_' . $pkClr);

                    //fetch school courses
                    if (Session::has('step_2_filter_courses_' . $pkClr)) {
                        $courses = Session::get('step_2_filter_courses_' . $pkClr);
                    } else {
                        $courses = Grade::select('*', 'gra_GradeNumeric')->with(['educationPlansMandatoryCourse' => function ($query) use ($grades, $educationPlans) {
                            $query->whereIn('fkEmcGra', $grades)->whereIn('fkEmcEpl', $educationPlans);
                        }
                            , 'educationPlansMandatoryCourse.mandatoryCourseGroup' => function ($query) use ($grades, $educationPlans) {
                                $query->select('pkCrs', 'crs_CourseName_' . $this->current_language . ' as crs_CourseName');
                            }
                            , 'educationPlansMandatoryCourse.educationPlan' => function ($query) use ($grades) {
                                $query->select('pkEpl', 'epl_EducationPlanName_' . $this->current_language . ' as epl_EducationPlanName');
                            },
                        ])
                            ->whereIn('pkGra', $grades)->get();
                    }

                    $employees = EmployeesEngagement::with(['employee' => function ($q) { //Employee engagment id with employee
                        $q->select('id', \DB::raw("CONCAT(emp_EmployeeName, ' ', emp_EmployeeSurname) AS empName"));
                    },
                    ])
                        ->where('fkEenSch', $mainSchool)->whereNull('een_DateOfFinishEngagement')
                        ->whereIn('fkEenEpty', $allowedTypes)->get()
                        ->pluck('employee.empName', 'pkEen');

                    $step4data = ['data5' => $classData,
                        'students' => $classStudents,
                        'courses' => $courses,
                        'employees' => $employees,
                        'selectetTeachers' => $selectetTeachers,
                        'grades_ids' => $grades,
                        'pkClr' => $pkClr,
                    ];

                    $nextData = \View::make('employee.classCreation.classCreationHelper')->with($step4data)->renderSections();

                    $response['message'] = trans('message.msg_class_creation_step_4_success');
                    $response['pkClr'] = $pkClr;
                    $response['status'] = true;
                    $response['step_5_data'] = $nextData['step_5'];

                } else {
                    $response['status'] = false;
                    $response['message'] = trans('message.msg_something_wrong');
                }

                $logHelper->addToLog("Step-4 Response:\n");
                $logHelper->addToLog(print_r($response, 1));
            } catch (\Exception $e) {
                $logHelper->addToLog("Class Creation Step-4 Error: \n" . print_r($e->getTraceAsString(), 1));
            }
        }

        if ($class_step == 5) {
            try {
                $logHelper->addToLog("Start Step-5");
                $logHelper->addToLog("Step-5 Request:\n");
                $logHelper->addToLog(print_r($input, 1));

                Session::forget('pkClr');
                Session::forget('selected_grades');
                $step2data = Session::get('cc_step_2_' . $pkClr);
                $stuCourses = Session::get('stu_Courses_' . $pkClr);
                $courses = $input['courses'];
                unset($input['courses']);
                $insert = [];

                if (!empty($pkClr) && !empty($step2data['stu_ids'])) {
                    $stus = $step2data['stu_ids'];
                    $logHelper->addToLog("Step-5 Students: \n" . print_r($stus, 1));
                    $stuData = [];
                    $courses_id = [];
                    $coTeData = [];
                    $stu_ids = [];
                    $del_stu = [];

                    $oldStuIds = ClassStudentsSemester::where('fkSemCcs', $pkCcs)->pluck('fkSemSen')
                        ->all();

                    foreach ($stus as $k => $v) {
                        $vs = explode('_', $v);
                        $stu_ids[] = $vs[1];
                    }
                    $logHelper->addToLog("Step-5 Students: \n" . print_r($stu_ids, 1));
                    $del_stu = array_diff($oldStuIds, $stu_ids);
                    $logHelper->addToLog("Step-5 del stu Students: \n" . print_r($del_stu, 1));

                    $oldStuSemAllocId = ClassStudentsSemester::where('fkSemCcs', $pkCcs)->pluck('pkSem')
                        ->all();
                    $oldStuAlloc = ClassStudentsAllocation::whereIn('fkCsaSem', $oldStuSemAllocId)->where('fkCsaCga', null)
                        ->get()
                        ->pluck('pkCsa')
                        ->all();
                    $courseAlloc = ClassTeachersCourseAllocation::whereIn('fkCtcCsa', $oldStuAlloc)->get()
                        ->pluck('Ctc_start', 'fkCtcEeg')
                        ->all();

                    ClassStudentsSemester::where('fkSemCcs', $pkCcs)->whereIn('fkSemSen', $del_stu)->delete();

                    ClassStudentsAllocation::whereIn('fkCsaSem', $oldStuSemAllocId)->where('fkCsaCga', null)
                        ->delete();

                    ClassTeachersCourseAllocation::whereIn('fkCtcCsa', $oldStuAlloc)->update(['deleted_at' => now(), 'Ctc_end' => now()]);

                    foreach ($stus as $k => $v) {
                        // for student course allocation
                        $vsexp = explode('_', $v);

                        $oldStudExist = ClassStudentsSemester::select('pkSem')
                            ->where('fkSemCcs', $pkCcs)
                            ->where('fkSemSen', $vsexp[1])->first();

                        if (!empty($oldStudExist)) {
                            $pkSem = $oldStudExist->pkSem;
                        } else {
                            $pkSem = ClassStudentsSemester::insertGetId(['fkSemCcs' => $pkCcs, 'fkSemSen' => $vsexp[1]]);
                        }

                        foreach ($courses as $ks => $vs) {
                            $vcexp = explode('_', $vs);
                            $courses_id[] = $vcexp[1];

                            if ($vcexp[0] == $vsexp[0]) {
                                $stuData[] = ['fkCsaSem' => $pkSem, 'fkGra' => $vcexp[0], 'fkCsaEmc' => $vcexp[1], 'fkCsaSen' => $vsexp[1], 'pkEpl' => $vcexp[2]];
                            }

                        }
                    }

                    foreach ($courses as $k => $v) {
                        // for teacher course allocation
                        $ve = explode('_', $v);
                        if (isset($input['fkCtcEeg_' . $k])) {
                            foreach ($input['fkCtcEeg_' . $k] as $key => $value) {
                                if (array_key_exists($value, $courseAlloc)) {
                                    $coTeData[] = ['fkCtcEeg' => $value, 'fkGra' => $ve[0], 'fkCtcEmc' => $ve[1], 'fkCtcClr' => $pkClr, 'Ctc_start' => $courseAlloc[$value]];
                                } else {
                                    $coTeData[] = ['fkCtcEeg' => $value, 'fkGra' => $ve[0], 'fkCtcEmc' => $ve[1], 'fkCtcClr' => $pkClr, 'Ctc_start' => now()];
                                }
                            }
                        }
                    }

                    $logHelper->addToLog("Step-5 stuData:\n");
                    $logHelper->addToLog(print_r($stuData, 1));

                    foreach ($stuData as $k => $v) {
                        $tmpGra = $stuData[$k]['fkGra'];
                        unset($stuData[$k]['fkGra']);
                        if (isset($stuCourses[$stuData[$k]['fkCsaSen']])) {
                            $lastInsertId = '';
                            if (in_array($stuData[$k]['fkCsaEmc'], $stuCourses[$stuData[$k]['fkCsaSen']])) {
                                unset($stuData[$k]['fkCsaSen']);
                                unset($stuData[$k]['pkEpl']);
                                $lastInsertId = ClassStudentsAllocation::insertGetId($stuData[$k]);
                            }
                        } else {
                            unset($stuData[$k]['fkCsaSen']);
                            unset($stuData[$k]['pkEpl']);
                            $lastInsertId = ClassStudentsAllocation::insertGetId($stuData[$k]);
                        }

                        if (!empty($lastInsertId)) {
                            foreach ($coTeData as $kt => $vt) {
                                if ($v['fkCsaEmc'] == $vt['fkCtcEmc'] && $tmpGra == $vt['fkGra']) {
                                    unset($coTeData[$kt]['fkCtcClr']);
                                    $newTea = new ClassTeachersCourseAllocation;
                                    $newTea->fkCtcCsa = $lastInsertId;
                                    $newTea->fkCtcCcs = $pkCcs;
                                    $newTea->fkCtcEeg = $coTeData[$kt]['fkCtcEeg'];
                                    $newTea->Ctc_start = $coTeData[$kt]['Ctc_start'];
                                    $newTea->save();
                                }
                            }
                        }
                    }
                    ClassCreation::where('pkClr', $pkClr)->update(['clr_status' => 'Publish']);

                    if (Session::has('homeroometeacherset')) {
                        $hrtId = Session::get('homeroometeacherset');
                        $homeTeacher = HomeRoomTeacher::where('pkHrt', $hrtId)->first();
                        $employeeEng = EmployeesEngagement::find($homeTeacher->fkHrtEen);
                        $emp = Employee::find($employeeEng->fkEenEmp);
                        $school = School::where('pkSch', $employeeEng->fkEenSch)
                            ->first();

                        $dataemp = ['email' => $emp->email, 'name' => $emp->emp_EmployeeName . " " . $emp->emp_EmployeeSurname, 'school' => $school->sch_SchoolName_en, 'subject' => 'New Home Room Teacher Assign'];
                        MailHelper::sendHomeroomTeacherAssign($dataemp);
                        Session::forget('homeroometeacherset');

                        $logHelper->addToLog("Send Email to Homeroomteacher");
                        $logHelper->addToLog(print_r($dataemp, 1));
                    }

                    Session::forget('stu_Courses_' . $pkClr);
                    Session::forget('cc_step_2_' . $pkClr);
                    Session::forget('cc_step_3_' . $pkClr);
                    Session::forget('cc_step_2_epl_' . $pkClr);
                    Session::forget('step_2_filter_courses_' . $pkClr);

                    $response['status'] = true;
                    $response['message'] = trans('message.msg_class_creation_add_success');
                    $response['pkClr'] = $pkClr;

                } else {
                    $response['status'] = false;
                    $response['message'] = trans('message.msg_something_wrong');
                }
            } catch (\Exception $e) {
                $logHelper->addToLog("Class Creation Step-5 Error: \n" . print_r($e->getTraceAsString(), 1));
            }
        }

        $logHelper->addToLog("Class Creation End");
        $logHelper->addToLog(print_r($response, 1));

        return response()
            ->json($response);
    }

    /**
     * Edit Class Creation
     * @param  Int $id
     * @return view - employee.classCreation.classCreation
     */
    public function edit($id)
    {
        $this->forHomeroomTeacherallowedTypes = $this->getSelectedRoles([$this->employees[0], $this->employees[1], $this->employees[2]]);
        $clrData = ClassCreationSemester::select('fkCcsClr')->where('pkCcs', $id)->first();

        $classDetails = ClassCreation::with(['classCreationSchoolYear' => function ($q) {
            $q->select('pkSye', 'sye_NameNumeric', 'sye_NameCharacter_' . $this->current_language . ' AS sye_NameCharacter');
        }
            , 'classCreationGrades' => function ($q) {
                $q->select('pkCcg', 'fkCcgClr', 'fkCcgGra');
            }
            , 'classCreationSemester.chiefStudent.grade' => function ($q) {
                $q->select('pkGra', 'gra_GradeNumeric');
            }
            , 'classCreationSemester.chiefStudent.student' => function ($q) {
                $q->select('id', \DB::raw("CONCAT(stu_StudentName, ' ', stu_StudentSurname) AS stuName"));
            }
            , 'classCreationSemester.treasureStudent.grade' => function ($q) {
                $q->select('pkGra', 'gra_GradeNumeric');
            }
            , 'classCreationSemester.treasureStudent.student' => function ($q) {
                $q->select('id', \DB::raw("CONCAT(stu_StudentName, ' ', stu_StudentSurname) AS stuName"));
            }
            , 'classCreationSemester.classStudentsSemester.classStudentsAllocation'
            , 'classCreationSemester.classStudentsSemester.classStudentsAllocation.classCreationTeachers' => function ($q) {
                $q->whereNull('Ctc_end');
            },
        ])
            ->where('pkClr', $clrData->fkCcsClr)
            ->first();

        $mainSchool = $this
            ->logged_user->sid;

        $schoolYears = SchoolYear::select('pkSye', 'sye_NameNumeric', 'sye_DefaultYear', 'sye_NameCharacter_' . $this->current_language . ' AS sye_NameCharacter')->get();
        $grades = Grade::select('pkGra', 'gra_GradeNumeric')
            ->get();
        $classes = Classes::select('pkCla', 'cla_ClassName_' . $this->current_language . ' as cla_ClassName')
            ->get();
        $semesters = EducationPeriod::select('pkEdp', 'edp_EducationPeriodName_' . $this->current_language . ' as edp_EducationPeriodName')
            ->get();
        $villageSchools = VillageSchool::select('pkVsc', 'vsc_VillageSchoolName_' . $this->current_language . ' as vsc_VillageSchoolName')
            ->where('fkVscSch', $mainSchool)->get();

        $employees = Employee::select('id',
            \DB::raw('(CASE
                        WHEN emp_EmployeeID IS NOT NULL THEN emp_EmployeeID
                        ELSE emp_TempCitizenId
                        END) AS empId'),
            \DB::raw("CONCAT(emp_EmployeeName, ' ', emp_EmployeeSurname) AS empName")
        )
            ->whereHas('EmployeesEngagement', function ($q) use ($mainSchool) {
                $q->whereIn('fkEenEpty', $this->forHomeroomTeacherallowedTypes)
                    ->where('fkEenSch', $mainSchool)
                    ->whereNull('een_DateOfFinishEngagement');
            })->get();

        $HomeRoomTeacher = HomeRoomTeacher::select('fkHrtEen')->where('fkHrtCcs', $id)->first();
        $TeacherEmp = 0;
        $start_date = '';
        $een_Notes = '';
        if (!empty($HomeRoomTeacher)) {
            $employeeEngagement = EmployeesEngagement::select('fkEenEmp', 'een_DateOfEngagement', 'een_Notes')->find($HomeRoomTeacher->fkHrtEen);
            $TeacherEmp = $employeeEngagement->fkEenEmp;
            $start_date = $employeeEngagement->een_DateOfEngagement;
            $een_Notes = $employeeEngagement->een_Notes;
        }

        if (request()
            ->ajax()) {
            return \View::make('employee.classCreation.classCreation')
                ->with(['classDetails' => $classDetails, 'classes' => $classes, 'mainSchool' => $mainSchool, 'schoolYears' => $schoolYears, 'grades' => $grades, 'semesters' => $semesters, 'villageSchools' => $villageSchools, 'employees' => $employees, 'TeacherEmp' => $TeacherEmp, 'start_date' => $start_date, 'een_Notes' => $een_Notes])->renderSections();
        }
        return view('employee.classCreation.classCreation')
            ->with(['classDetails' => $classDetails, 'classes' => $classes, 'mainSchool' => $mainSchool, 'schoolYears' => $schoolYears, 'grades' => $grades, 'semesters' => $semesters, 'villageSchools' => $villageSchools, 'employees' => $employees, 'TeacherEmp' => $TeacherEmp, 'start_date' => $start_date, 'een_Notes' => $een_Notes]);
    }

    /**
     * Delete Class Creation
     * @param  Request $request
     * @return JSON $response
     */
    public function destroy(Request $request, $cid)
    {
        $response = [];

        if (empty($cid)) {
            $response['status'] = false;
        } else {
            $cls = ClassCreationSemester::select('fkCcsClr')->where('pkCcs', $cid)->first();
            $classCreation = $cls->fkCcsClr;

            $count = ClassCreationSemester::where('fkCcsClr', $classCreation)->where('pkCcs', '!=', $cid)->count();

            if ($count == 0) {
                //if no semester exist in class creation then we need to remove from classcreation
                $eed = ClassCreation::where('pkClr', $classCreation)->delete();
            }

            $eed = ClassCreationSemester::where('pkCcs', $cid)->delete();

            $oldStuSemAllocId = ClassStudentsSemester::where('fkSemCcs', $cid)->pluck('pkSem')
                ->all();
            $oldStuAlloc = ClassStudentsAllocation::whereIn('fkCsaSem', $oldStuSemAllocId)->get()
                ->pluck('pkCsa')
                ->all();

            ClassStudentsSemester::where('fkSemCcs', $cid)->delete();

            ClassStudentsAllocation::whereIn('fkCsaSem', $oldStuSemAllocId)->delete();

            ClassTeachersCourseAllocation::whereIn('fkCtcCsa', $oldStuAlloc)->update(['deleted_at' => now(), 'Ctc_end' => now()]);

            $homeTeacherEngagements = HomeRoomTeacher::where('fkHrtCcs', $cid)->get()->pluck('fkHrtEen');
            if (!empty($homeTeacherEngagements)) {
                EmployeesEngagement::whereIn('pkEen', $homeTeacherEngagements)->delete();
            }

            HomeRoomTeacher::where('fkHrtCcs', $cid)->delete();

            if ($eed) {
                $response['status'] = true;
                $response['message'] = trans('message.msg_class_creation_delete_success');
                $response['redirect'] = url('/employee/classcreation');
            } else {
                $response['status'] = false;
                $response['message'] = trans('message.msg_class_creation_delete_prompt');
            }
        }

        return response()
            ->json($response);
    }

    /**
     * student detail page
     *
     * @param Int $id
     * @return view - employee.classCreation.viewStudentDetail
     */
    public function viewStudentDetail($id)
    {
        if (!empty($id)) {
            $studentSem = ClassStudentsSemester::with(['studentEnroll'])->where('pkSem', $id)->first();
            if (!empty($studentSem)) {
                $stuEnrollId = $studentSem->studentEnroll->pkSte;
                $stuEpl = $studentSem->studentEnroll->fkSteEpl;
                $stuSem = $studentSem->fkSemCcs;

                $mdata = ClassCreation::whereHas(
                    'classCreationSemester', function ($q) use ($stuSem) {
                        $q->where('pkCcs', $stuSem);
                    })
                    ->with(['classCreationSemester' => function ($q) use ($stuSem) {
                        $q->where('pkCcs', $stuSem);
                    }
                        , 'classCreationSchoolYear' => function ($q) {
                            $q->select('pkSye', 'sye_NameNumeric', 'sye_NameCharacter_' . $this->current_language . ' AS sye_NameCharacter');
                        }
                        , 'classCreationClasses' => function ($q) {
                            $q->select('pkCla', 'cla_ClassName_' . $this->current_language . ' as cla_ClassName');
                        }
                        , 'classCreationSemester.semester' => function ($q) {
                            $q->select('pkEdp', 'edp_EducationPeriodName_' . $this->current_language . ' as edp_EducationPeriodName');
                        }
                        , 'classCreationSemester.classStudentsSemester' => function ($q) use ($stuEnrollId) {
                            $q->where('fkSemSen', $stuEnrollId);
                        }
                        , 'classCreationSemester.classStudentsSemester.studentEnroll.student'
                        , 'classCreationSemester.classStudentsSemester.studentEnroll.grade' => function ($q) {
                            $q->select('pkGra', 'gra_GradeNameRoman', 'gra_GradeNumeric');
                        }
                        , 'classCreationSemester.classStudentsSemester.studentEnroll.educationProgram' => function ($q) {
                            $q->select('pkEdp', 'edp_Name_' . $this->current_language . ' as edp_Name');
                        }
                        , 'classCreationSemester.classStudentsSemester.studentEnroll.educationPlan' => function ($q) {
                            $q->select('pkEpl', 'epl_EducationPlanName_' . $this->current_language . ' as epl_EducationPlanName');
                        },
                    ])->first();

                $studentCourses = ClassStudentsAllocation::with([
                    'classCreationCourses' => function ($q) {
                        $q->select('pkCrs', 'crs_CourseName_' . $this->current_language . ' AS crs_CourseName');
                    },
                    'subClassGroup' => function ($q) {
                        $q->select('pkScg', 'scg_Name_' . $this->current_language . ' AS scg_Name');
                    },
                    'courseGroup.OCG' => function ($q) {
                        $q->select('pkOcg', 'ocg_Name_' . $this->current_language . ' AS ocg_Name');
                    },
                    'courseGroup.GPG' => function ($q) {
                        $q->select('pkGpg', 'gpg_Name_' . $this->current_language . ' AS gpg_Name');
                    },
                    'courseGroup.FLG' => function ($q) {
                        $q->select('pkFon', 'fon_Name_' . $this->current_language . ' AS fon_Name');
                    },
                    'courseGroup.FCG' => function ($q) {
                        $q->select('pkFcg', 'fcg_Name_' . $this->current_language . ' AS fcg_Name');
                    },
                    'classCreationTeachers' => function ($q) {
                        $q->select('pkCtc', 'fkCtcCsa', 'fkCtcEeg', 'fkCtcCcs');
                    },
                    'classCreationTeachers.employeesEngagement' => function ($q) {
                        $q->select('pkEen', 'fkEenSch', 'fkEenEmp');
                    },
                    'classCreationTeachers.employeesEngagement.employee' => function ($q) {
                        $q->select('id', \DB::raw("CONCAT(emp_EmployeeName, ' ', emp_EmployeeSurname) AS empName"));
                    },
                ])->where('fkCsaSem', $id)->get()->toArray();

                $mandatoryCourses = [];

                $subclassCourses = [];
                $optionalCourses = [];
                $foriegnCourses = [];
                $facultativeCourses = [];
                $generalCourses = [];
                if (!empty($studentCourses)) {
                    foreach ($studentCourses as $sck => $scv) {
                        $scv['pkCrs'] = $scv['class_creation_courses']['pkCrs'];
                        $scv['crs_CourseName'] = $scv['class_creation_courses']['crs_CourseName'];
                        unset($scv['class_creation_courses']);

                        $crs_Teachers = '';
                        $teachers = [];

                        foreach ($scv['class_creation_teachers'] as $tk => $tv) {
                            $teachers[] = $tv['employees_engagement']['employee']['empName'];
                        }
                        if (count($teachers)) {
                            $crs_Teachers = implode(",", $teachers);
                        }
                        unset($scv['class_creation_teachers']);
                        $scv['crs_Teachers'] = $crs_Teachers;

                        //mandatory courses
                        if (empty($scv['sub_class_group']) && empty($scv['course_group'])) {
                            $mandatoryCourses[] = $scv;
                        }

                        //subclass courses
                        if (!empty($scv['sub_class_group'])) {
                            $scv['pkScg'] = $scv['sub_class_group']['pkScg'];
                            $scv['scg_Name'] = $scv['sub_class_group']['scg_Name'];
                            unset($scv['sub_class_group']);

                            $subclassCourses[$scv['pkScg']][] = $scv;
                        }

                        //optional courses
                        if (!empty($scv['course_group']) && !empty($scv['course_group']['o_c_g'])) {
                            $scv['pkOcg'] = $scv['course_group']['o_c_g']['pkOcg'];
                            $scv['ocg_Name'] = $scv['course_group']['o_c_g']['ocg_Name'];
                            unset($scv['course_group']['o_c_g']);

                            $optionalCourses[$scv['pkOcg']][] = $scv;
                        }

                        //facultative courses
                        if (!empty($scv['course_group']) && !empty($scv['course_group']['f_c_g'])) {
                            $scv['pkFcg'] = $scv['course_group']['f_c_g']['pkFcg'];
                            $scv['fcg_Name'] = $scv['course_group']['f_c_g']['fcg_Name'];
                            unset($scv['course_group']['f_c_g']);

                            $facultativeCourses[$scv['pkFcg']][] = $scv;
                        }

                        //foriegn courses
                        if (!empty($scv['course_group']) && !empty($scv['course_group']['f_l_g'])) {
                            $scv['pkFon'] = $scv['course_group']['f_l_g']['pkFon'];
                            $scv['fon_Name'] = $scv['course_group']['f_l_g']['fon_Name'];
                            unset($scv['course_group']['f_l_g']);

                            $foriegnCourses[$scv['pkFon']][] = $scv;
                        }

                        //general courses
                        if (!empty($scv['course_group']) && !empty($scv['course_group']['g_p_g'])) {
                            $scv['pkGpg'] = $scv['course_group']['g_p_g']['pkGpg'];
                            $scv['gpg_Name'] = $scv['course_group']['g_p_g']['gpg_Name'];
                            unset($scv['course_group']['g_p_g']);

                            $generalCourses[$scv['pkGpg']][] = $scv;
                        }
                    }
                }

                $results = compact('mdata', 'mandatoryCourses', 'subclassCourses', 'optionalCourses', 'foriegnCourses', 'facultativeCourses', 'generalCourses');

                return view('employee.classCreation.viewStudentDetail', $results);
            }
        } else {
            $response['status'] = false;
            $response['message'] = trans('message.msg_something_wrong');

            return response()->json($response);
        }
    }

    /**
     * Used for fetch student education plan
     * @param  Request $request
     * @return JSON $response
     */
    public function fetchClassEducationPlan(Request $request)
    {
        $input = $request->all();
        $pkEpl = $input['pkEpl'];
        $pkSen = $input['pkSen'];
        $pkGra = $input['pkGra'];
        $pkClr = $input['pkClr'];
        $pkCcs = $input['pkCcs'];

        $step3data = Session::get('cc_step_3_selected_courses_' . $pkClr);

        $selctedCourses = [];
        if (!empty($step3data)) {
            foreach ($step3data as $k => $v) {
                $data = explode("_", $v);
                if ($data[0] == $pkGra) {
                    $selctedCourses[] = $data[1];
                }
            }
        }

        $educationPlan = EducationPlan::with(['mandatoryCourse.mandatoryCourseGroup' => function ($q) use ($pkGra) {
            $q->select('pkCrs', 'crs_CourseName_' . $this->current_language . ' as crs_CourseName')
                ->where('crs_CourseType', '!=', 'DummyOptionalCourse')
                ->where('crs_CourseType', '!=', 'DummyForeignCourse');
        }
            , 'mandatoryCourse' => function ($query) use ($pkGra, $selctedCourses) {
                $query->where('fkEmcGra', '=', $pkGra)->whereIn('fkEplCrs', $selctedCourses);
            },
        ])->where('pkEpl', $pkEpl)->first();

        $existCourse = [];
        $existCourseTmp = ClassStudentsSemester::with(['classStudentsAllocation'])->where('fkSemCcs', $pkCcs)->where('fkSemSen', $pkSen)->get();
        if (isset($existCourseTmp[0])) {
            foreach ($existCourseTmp[0]->classStudentsAllocation as $k => $v) {
                $existCourse[$pkSen][] = $v->fkCsaEmc;
            }
        }

        if (!empty($educationPlan
            ->mandatoryCourse
            ->count())) {
            $datas = ['course_detail' => $educationPlan,
                'pkSen' => $pkSen,
                'existCourse' => $existCourse,
                'pkClr' => $pkClr,
                'pkEpl' => $pkEpl,
                'pkGra' => $pkGra];
            $nextData = \View::make('employee.classCreation.classCreationHelper')->with($datas)->renderSections();
            $response['status'] = true;
            $response['course_details'] = $nextData['course_detail'];

        } else {
            $response['status'] = false;
            $response['message'] = trans('message.msg_course_not_found');
        }

        return response()
            ->json($response);
    }

    /**
     * Compare course with week hours
     *
     * @param [type] $value
     * @param [type] $key
     * @return void
     */
    public function findUniqueCoursewithHours($value, $key)
    {
        $v1 = $value;
        foreach ($this->selectedCoursesd[$this->g] as $k => $v) {
            if ($k > $key) {
                $v2 = $this->selectedCoursesd[$this->g][$k] ?? null;

                if ($v1['crs'] === $v2['crs'] && $v1['hrs'] === $v2['hrs']) {
                    $this->result[$this->g][$v1['crs'] . $v1['hrs']][] = [$v1['pkEmc'], $v2['pkEmc']];
                }
            }
        }

        $filteredAry = array();
        if (!empty($this->result[$this->g])) {
            foreach ($this->result[$this->g] as $km => $vm) {
                foreach ($vm as $kvm => $vvm) {
                    foreach ($vvm as $d) {
                        $filteredAry[$this->g][] = $d;
                    }
                }
            }
        }
        $this->gradeData = $filteredAry;
    }

    /**
     * Fetch previous school year and previous grade student and use for bulk add students
     * @param Request $request
     * @return JSON $result
     */
    public function fetchPreviousGradeStudent(Request $request)
    {
        $data = $request->all();
        $results = ['status' => false];
        if (!empty($data['pkCcs'])) {
            $stuIdAry = array();
            $previousclassStudents = ClassStudentsSemester::with(['studentEnroll.student'])->where('fkSemCcs', $data['pkCcs'])->get()->toArray();
            $enrollstudentAry = array_column($previousclassStudents, 'student_enroll');
            $studentAry = array_column($enrollstudentAry, 'student');
            $stuIdAry = array_column($studentAry, 'id');

            if (!empty($stuIdAry)) {
                $pkClr = $data['pkClr'];
                $mainSchool = $this->logged_user->sid;

                if (!empty($pkClr)) {
                    $classGradesTmp = ClassCreationGrades::select('fkCcgGra')->where('fkCcgClr', $pkClr)->get();
                    $classGrades = [];
                    foreach ($classGradesTmp as $key => $value) {
                        $classGrades[] = $value->fkCcgGra;
                    }

                    $schoolYearData = $this->getCurrentYear();
                    $currentYearId = $schoolYearData->pkSye;

                    $classStudents = EnrollStudent::whereHas('student', function ($q) use ($stuIdAry) {
                        $q->whereIn('id', $stuIdAry);
                    })
                        ->with(['student', 'grade' => function ($q) {
                            $q->select('pkGra', 'gra_GradeNameRoman', 'gra_GradeNumeric');
                        }
                            , 'educationProgram' => function ($q) {
                                $q->select('pkEdp', 'edp_Name_' . $this->current_language . ' as edp_Name');
                            }
                            , 'educationPlan' => function ($q) {
                                $q->select('pkEpl', 'epl_EducationPlanName_' . $this->current_language . ' as epl_EducationPlanName');
                            },
                        ])
                        ->whereIn('fkSteGra', $classGrades)
                        ->where('fkSteSch', $mainSchool)
                        ->where('fkSteSye', $currentYearId)
                        ->whereNull('ste_FinishingDate')
                        ->whereNull('ste_EnrollmentFinishDate')
                        ->get()
                        ->toArray();

                    $classStudentdata = [];
                    foreach ($classStudents as $key => $value) {
                        $value['stu_DateOfBirth'] = date('d/m/Y', strtotime($value['student']['stu_DateOfBirth']));
                        $value['ste_EnrollmentDate'] = date('d/m/Y', strtotime($value['ste_EnrollmentDate']));
                        $value['ste_status'] = trans('general.gn_active');
                        $value['stu_StudentName'] = $value['student']['stu_StudentName'] . " " . $value['student']['stu_StudentSurname'];
                        $value['gra_GradeName_en'] = $value['grade']['gra_GradeNumeric'];
                        $classStudentdata[] = $value;
                    }
                    $results = ['status' => true, 'data' => $classStudentdata];
                }
            }

        }

        return response()->json($results);
    }

    /**
     * Used for class Students Listing
     * @param  Request $request
     * @return JSON $result
     */
    public function class_student_list(Request $request)
    {
        $data = $request->all();
        $perpage = !empty($data['length']) ? (int) $data['length'] : 10;
        $filter = isset($data['search']) && is_string($data['search']) ? $data['search'] : '';
        $sort_type = isset($data['order'][0]['dir']) && is_string($data['order'][0]['dir']) ? $data['order'][0]['dir'] : '';
        $sort_col = $data['order'][0]['column'];
        $sort_field = $data['columns'][$sort_col]['data'];
        $pkClr = $data['pkClr'];
        $pkCcs = $data['pkCcs'];
        $fkClrEdp = $data['fkClrEdp'];
        $fkClrVsc = $data['fkClrVsc'];

        $mainSchool = $this
            ->logged_user->sid;

        $cdata = ClassCreation::where('pkClr', $pkClr)->first();

        if (!empty($cdata)) {
            $fkClrSch = $cdata->fkClrSch;
            $fkClrSye = $cdata->fkClrSye;
        } else {
            $fkClrSch = 0;
            $fkClrSye = 0;
        }

        $classGrades = [];
        if (!empty($pkClr)) {
            $classGrades = ClassCreationGrades::select('fkCcgGra')
                ->where('fkCcgClr', $pkClr)
                ->get()
                ->pluck('fkCcgGra')
                ->all();
        }

        $exceptsStudents = (new ClassCreationService())->getAlreadyEnrolledStudent(compact('fkClrEdp', 'classGrades', 'fkClrSch', 'fkClrSye', 'fkClrVsc', 'pkCcs'));
        $classStudent = EnrollStudent::whereHas('student', function ($q) use ($filter) {
            if (!empty($filter)) {
                $q->where(function ($query) use ($filter) {
                    $query->where('stu_StudentName', 'LIKE', '%' . $filter . '%')->orWhere('stu_StudentSurname', 'LIKE', '%' . $filter . '%')->orWhere('stu_StudentID', 'LIKE', '%' . $filter . '%')->orWhere('stu_TempCitizenId', 'LIKE', '%' . $filter . '%');
                });
            }
        })->with(['student', 'grade' => function ($q) {
            $q->select('pkGra', 'gra_GradeNameRoman', 'gra_GradeNumeric');
        }
            , 'educationProgram' => function ($q) {
                $q->select('pkEdp', 'edp_Name_' . $this->current_language . ' as edp_Name');
            }
            , 'educationPlan' => function ($q) {
                $q->select('pkEpl', 'epl_EducationPlanName_' . $this->current_language . ' as epl_EducationPlanName');
            },
        ])
            ->whereIn('fkSteGra', $classGrades)
            ->where('fkSteSch', $fkClrSch)
            ->where('fkSteViSch', $fkClrVsc)
            ->where('fkSteSye', $fkClrSye)
            ->whereNull('ste_FinishingDate')
            ->where(function ($q) use ($exceptsStudents) {
                if (count($exceptsStudents)) {
                    $q->whereNotIn('pkSte', $exceptsStudents);
                }
            })
            ->where('ste_EnrollmentFinishDate');

        $total_mainBooks = $classStudent->count();

        $offset = $data['start'];

        $counter = $offset;
        $classStudentdata = [];
        $classStudents = $classStudent->offset($offset)->limit($perpage)->get()
            ->toArray();

        foreach ($classStudents as $key => $value) {
            $value['index'] = $counter + 1;
            $value['stu_DateOfBirth'] = date('d/m/Y', strtotime($value['student']['stu_DateOfBirth']));
            $value['ste_EnrollmentDate'] = date('d/m/Y', strtotime($value['ste_EnrollmentDate']));
            $value['ste_status'] = trans('general.gn_active');
            $value['stu_StudentName'] = $value['student']['stu_StudentName'] . " " . $value['student']['stu_StudentSurname'];
            $value['gra_GradeName_en'] = $value['grade']['gra_GradeNumeric'];
            $classStudentdata[$counter] = $value;
            $counter++;
        }

        if ($sort_col != 0) {
            $price = array_column($classStudentdata, $sort_field);
        } else {
            $price = array_column($classStudentdata, 'index');
        }

        if ($sort_col != 0) {
            if ($sort_type == 'desc') {
                array_multisort($price, SORT_DESC, SORT_STRING, $classStudentdata);
            } else {
                array_multisort($price, SORT_ASC, SORT_STRING, $classStudentdata);
            }
        } else {
            if ($sort_type == 'desc') {
                array_multisort($price, SORT_DESC, $classStudentdata);
            } else {
                array_multisort($price, SORT_ASC, $classStudentdata);
            }
        }
        $classStudentdata = array_values($classStudentdata);

        $results = array(
            "draw" => $data['draw'],
            "recordsTotal" => $total_mainBooks,
            "recordsFiltered" => $total_mainBooks,
            'data' => $classStudentdata,
        );

        return response()->json($results);
    }

    /**
     * Class creation semester listing page
     * @param  Int $id
     * @return view - employee.classCreation.classCreationSemesters
     */
    public function classCreationSemesters($id)
    {
        $searchSchYear = SchoolYear::select('pkSye', 'sye_NameNumeric', 'sye_DefaultYear', 'sye_NameCharacter_' . $this->current_language . ' AS sye_NameCharacter')->get()
            ->toArray();
        $searchGrade = Grade::select('pkGra', 'gra_GradeNumeric')
            ->get()
            ->toArray();

        if (request()
            ->ajax()) {
            return \View::make('employee.classCreation.classCreationSemesters')
                ->with(['searchSchYear' => $searchSchYear, 'searchGrade' => $searchGrade, 'pkClr' => $id])->renderSections();
        }
        return view('employee.classCreation.classCreationSemesters')
            ->with(['searchSchYear' => $searchSchYear, 'searchGrade' => $searchGrade, 'pkClr' => $id]);
    }

    /**
     * Delete Student Course
     *
     * @param \Illuminate\Http\Request $request
     * @return JSON $response
     */
    public function deleteStudentCourse(Request $request)
    {
        $input = $request->all();
        $pkCsa = $input['cid'];

        ClassStudentsAllocation::find($pkCsa)->delete();
        ClassTeachersCourseAllocation::where('fkCtcCsa', $pkCsa)->delete();

        $response['message'] = trans('message.msg_course_deleted');
        $response['status'] = true;

        return response()->json($response);
    }

    /**
     * Fetch class creation semester data
     * @param  Request $request
     * @return JSON $result
     */
    public function classcreationsem_lists(Request $request)
    {
        $data = $request->all();

        $perpage = !empty($data['length']) ? (int) $data['length'] : 10;

        $filter = isset($data['search']) && is_string($data['search']) ? $data['search'] : '';

        $filterGra = isset($data['grade']) ? $data['grade'] : '';

        $filterSye = isset($data['schoolYear']) ? $data['schoolYear'] : '';

        $pkClr = $data['pkClr'];

        $sort_type = isset($data['order'][0]['dir']) && is_string($data['order'][0]['dir']) ? $data['order'][0]['dir'] : '';
        $sort_col = isset($data['order'][0]['column']) ? $data['order'][0]['column'] : '';

        $sort_field = isset($data['columns'][$sort_col]['data']) ? $data['columns'][$sort_col]['data'] : '';

        $classCreationSemester = ClassCreationSemester::with(['classCreation.classCreationGrades.grade' => function ($q) {
            $q->select('pkGra', 'gra_Uid', 'gra_GradeNumeric');
        }
            , 'semester' => function ($q) {
                $q->select('pkEdp', 'edp_EducationPeriodName_' . $this->current_language . ' as edp_EducationPeriodName');
            }
            , 'classCreation.classCreationSchoolYear' => function ($q) {
                $q->select('pkSye', 'sye_NameNumeric', 'sye_NameCharacter_' . $this->current_language . ' AS sye_NameCharacter');
            }
            , 'classStudentsSemester.classStudentsAllocation', 'classCreation.classCreationClasses' => function ($q) {
                $q->select('pkCla', 'cla_ClassName_' . $this->current_language . ' as cla_ClassName', 'cla_Uid');
            }
            , 'homeRoomTeacher.employeesEngagement.employee' => function ($q) {
                $q->select('id', 'emp_EmployeeName', 'emp_EmployeeSurname');
            },
        ]);

        $classCreationQuery = $classCreationSemester->where('fkCcsClr', $pkClr);

        if ($sort_col != 0) {
            $classCreationQuery = $classCreationQuery->orderBy($sort_field, $sort_type);
        }

        $total_class_creation = $classCreationQuery->count();

        $offset = isset($data['start']) ? $data['start'] : '';

        $counter = $offset;
        $classCreationdata = [];
        $classCreations = $classCreationQuery->offset($offset)->limit($perpage)->get()
            ->toArray();
        $counter = 0;
        foreach ($classCreations as $key => $value) {
            $aGrade = [];
            $aStud = [];

            foreach ($value['class_creation']['class_creation_grades'] as $v_key => $V_value) {
                $aGrade[] = $V_value['grade']['gra_GradeNumeric'];
            }

            $an = array_unique($aGrade);
            $an = array_values($an);
            $value['grade'] = isset($an) ? $an : '';
            if (isset($value['class_students_semester'])) {
                $tmp = [];
                foreach ($value['class_students_semester'] as $k => $v) {
                    $tmp[] = $v['fkSemSen'];
                }
                $aStud = array_unique($tmp);
            }
            $value['total_stu'] = count($aStud);
            $value['index'] = $counter + 1;
            $classCreationdata[$counter] = $value;
            $counter++;
        }

        $price = array_column($classCreationdata, 'index');
        if ($sort_col == 0) {
            if ($sort_type == 'desc') {
                array_multisort($price, SORT_DESC, $classCreationdata);
            } else {
                array_multisort($price, SORT_ASC, $classCreationdata);
            }
        }

        $results = array(
            "draw" => $data['draw'],
            "recordsTotal" => $total_class_creation,
            "recordsFiltered" => $total_class_creation,
            'data' => $classCreationdata,
        );

        return response()->json($results);
    }

    /**
     * Fetch classcreation data
     * @param  Request $request
     * @return JSON $result
     */
    public function classcreation_list(Request $request)
    {
        $data = $request->all();

        $perpage = !empty($data['length']) ? (int) $data['length'] : 10;

        $filter = isset($data['search']) && is_string($data['search']) ? $data['search'] : '';

        $filterGra = isset($data['grade']) ? $data['grade'] : '';

        $filterSye = isset($data['schoolYear']) ? $data['schoolYear'] : '';

        $sort_type = isset($data['order'][0]['dir']) && is_string($data['order'][0]['dir']) ? $data['order'][0]['dir'] : '';
        $sort_col = isset($data['order'][0]['column']) ? $data['order'][0]['column'] : '';

        $sort_field = isset($data['columns'][$sort_col]['data']) ? $data['columns'][$sort_col]['data'] : '';

        $mainSchool = $this
            ->logged_user->sid;

        $classCreation = ClassCreation::with(['classCreationGrades.grade' => function ($q) {
            $q->select('pkGra', 'gra_Uid', 'gra_GradeNumeric');
        }
            , 'classCreationSchoolYear' => function ($q) {
                $q->select('pkSye', 'sye_NameNumeric', 'sye_NameCharacter_' . $this->current_language . ' AS sye_NameCharacter');
            }
            , 'classCreationSemester.classStudentsSemester.classStudentsAllocation', 'classCreationClasses' => function ($q) {
                $q->select('pkCla', 'cla_ClassName_' . $this->current_language . ' as cla_ClassName', 'cla_Uid');
            }
            , 'classCreationSemester.homeRoomTeacher.employeesEngagement.employee' => function ($q) {
                $q->select('id', 'emp_EmployeeName', 'emp_EmployeeSurname');
            }, 'villageSchool' => function ($q) {
                $q->select('pkVsc', 'vsc_VillageSchoolName_' . $this->current_language . ' as vsc_VillageSchoolName');
            },
        ])
            ->where('fkClrSch', $mainSchool);

        if ($filter) {
            $classCreation = $classCreation->whereHas('classCreationClasses', function ($q) use ($filter) {
                $q->where('cla_ClassName_' . $this->current_language, 'LIKE', '%' . $filter . '%');
            })->orWhere('pkClr', 'LIKE', '%' . $filter . '%')->orWhereHas('classCreationGrades.grade', function ($qe) use ($filter) {
                $qe->where('gra_GradeName_' . $this->current_language, 'LIKE', '%' . $filter . '%');
            })->orWhereHas('classCreationSchoolYear', function ($qee) use ($filter) {
                $qee->where('sye_NameNumeric', 'LIKE', '%' . $filter . '%');
            });
        }

        if ($filterGra) {
            $classCreation = $classCreation->whereHas('classCreationGrades.grade', function ($qgra) use ($filterGra) {
                $qgra->where('gra_GradeNumeric', $filterGra);
            });
        }

        if ($filterSye) {
            $classCreation = $classCreation->whereHas('classCreationSchoolYear', function ($qsye) use ($filterSye) {
                $qsye->where('pkSye', $filterSye);
            });
        }

        $classCreationQuery = $classCreation;

        if ($sort_col != 0) {
            $classCreationQuery = $classCreationQuery->orderBy($sort_field, $sort_type);
        }

        $total_class_creation = $classCreationQuery->count();

        $offset = isset($data['start']) ? $data['start'] : '';

        $counter = $offset;
        $classCreationdata = [];
        $classCreations = $classCreationQuery->offset($offset)->limit($perpage)->get()
            ->toArray();
        $counter = 0;
        foreach ($classCreations as $key => $value) {
            $aGrade = [];
            $aStud = [];
            foreach ($value['class_creation_grades'] as $v_key => $V_value) {
                $aGrade[] = $V_value['grade']['gra_GradeNumeric'];
            }
            if ($value['clr_Status'] == 'Publish') {
                $value['clr_Status_txt'] = '<span class="custom_badge badge badge-success">' . trans('general.gn_publish') ?? $value['clr_Status'] . '</span>';
            } else {
                $value['clr_Status_txt'] = '<span class="custom_badge badge badge-warning">' . trans('general.gn_pending') ?? $value['clr_Status'] . '</span>';
            }
            $an = array_unique($aGrade);
            $an = array_values($an);
            $value['grade'] = isset($an) ? $an : '';
            if (isset($value['class_students_semester'])) {
                $tmp = [];
                foreach ($value['class_students_semester'] as $k => $v) {
                    $tmp[] = $v['fkSemSen'];
                }
                $aStud = array_unique($tmp);
            }
            $value['total_stu'] = count($aStud);
            $value['index'] = $counter + 1;
            $classCreationdata[$counter] = $value;
            $counter++;
        }

        $price = array_column($classCreationdata, 'index');
        if ($sort_col == 0) {
            if ($sort_type == 'desc') {
                array_multisort($price, SORT_DESC, $classCreationdata);
            } else {
                array_multisort($price, SORT_ASC, $classCreationdata);
            }
        }
        $results = array(
            "draw" => $data['draw'],
            "recordsTotal" => $total_class_creation,
            "recordsFiltered" => $total_class_creation,
            'data' => $classCreationdata,
        );

        return response()->json($results);
    }

    /**
     * View Courses selected for student in class creation
     * @param  Request $request
     * @return JSON $response
     */
    // public function classCourseSelection(Request $request)
    // {
    //     $input = $request->all();

    //     //selected courses set in session
    //     if (Session::has('stu_Courses_' . $input['pkClr']))
    //     {
    //         $tmpStuCourse = Session::get('stu_Courses_' . $input['pkClr']);
    //         $newKeys = array_keys($input['courses_stu']);
    //         foreach ($newKeys as $key => $value)
    //         {
    //             $tmpStuCourse[$value] = $input['courses_stu'][$value];
    //         }
    //         Session::put('stu_Courses_' . $input['pkClr'], $tmpStuCourse);
    //     }
    //     else
    //     {
    //         Session::put('stu_Courses_' . $input['pkClr'], $input['courses_stu']);
    //     }

    //     $response['status'] = true;
    //     $response['message'] = trans('message.msg_course_sel_success');

    //     return response()
    //         ->json($response);
    // }
}
