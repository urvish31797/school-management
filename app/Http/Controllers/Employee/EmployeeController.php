<?php
/**
 * EmployeeController
 *
 * @package    Laravel
 * @subpackage Controller
 * @since      1.0
 */

namespace App\Http\Controllers\Employee;

use App\Helpers\MailHelper;
use App\Helpers\UtilHelper;
use App\Http\Controllers\Controller;
use App\Http\Traits\CommonTrait;
use App\Models\AcademicDegree;
use App\Models\Admin;
use App\Models\Citizenship;
use App\Models\College;
use App\Models\EducationPeriod;
use App\Models\Employee;
use App\Models\EmployeeDesignation;
use App\Models\EmployeesEducationDetail;
use App\Models\EmployeesEngagement;
use App\Models\EmployeesWeekHourRates;
use App\Models\EmployeeType;
use App\Models\EngagementType;
use App\Models\Nationality;
use App\Models\QualificationDegree;
use App\Models\Religion;
use App\Models\School;
use App\Models\Shifts;
use App\Models\University;
use App\Http\Services\EmployeeService;
use Illuminate\Http\Request;
use Session;

class EmployeeController extends Controller
{
    use CommonTrait;

    /**
     * Employee Dashbord Page
     * @param  Request $request
     * @return view - employee.dashboard.dashboard
     * @return view - employee.dashboard.Teacher.dashboard
     */
    public function index(Request $request)
    {
        $SubAdminCount = 0;
        $view = '';
        $data = [];
        $EngagementType = EngagementType::select('pkEty', 'ety_EngagementTypeName_' . $this->current_language . ' as ety_EngagementTypeName')->get();
        $EducationPeriod = EducationPeriod::select('pkEdp', 'edp_EducationPeriodName_' . $this->current_language . ' as edp_EducationPeriodName')->get();
        $Shifts = Shifts::select('pkShi', 'shi_ShiftName_' . $this->current_language . ' as shi_ShiftName')->get();

        $mainSchool = $this->logged_user->sid;
        $syid = $this->logged_user->syid;
        if (empty($syid)) {
            $schoolYearData = $this->getCurrentYear();
            $schoolYear = $schoolYearData->pkSye;
            $this->logged_user->syid = $schoolYear;
            $syid = $schoolYear;
            Session::put('curr_school_year', $schoolYear);
        }

        $eid = $this->logged_user->eid;
        $semId = $this->logged_user->semid;
        if (empty($semId)) {
            $defaultSemdata = $this->getCurrentSemester($mainSchool);
            $defaultSem = $defaultSemdata->fkSchEdp;
            $this->logged_user->semid = $defaultSem;
            $semId = $defaultSem;
            Session::put('curr_school_sem', $defaultSem);
        }

        $coursedata = $this->logged_user->cosid;
        $shiftid = $this->logged_user->shiftid;
        if (empty($shiftid)) {
            Session::put('curr_shift', $Shifts->first()->pkShi);
        }

        if (isset($semId) && !empty($semId)) {
            $defaultSem = $semId;
        } else {
            $defaultSemdata = $this->getCurrentSemester($mainSchool);
            $defaultSem = $defaultSemdata->fkSchEdp;
        }

        $param = ['schoolYear' => $syid,
            'schoolId' => $mainSchool,
            'semesterId' => $semId,
            'engagementId' => $eid,
            'language' => $this->current_language];

        $data = [
            'EngagementType' => $EngagementType,
            'SubAdminCount' => $SubAdminCount,
            'EducationPeriod' => $EducationPeriod,
            'defaultSemester' => $defaultSem,
            'Shifts' => $Shifts,
            'coursedata' => $coursedata,
            'shiftid' => $shiftid,
        ];

        if ($this->logged_user->type == 'Teacher' || $this->logged_user->type == 'Principal') {

            $classesList = (new EmployeeService())->getTeachersClasses($param);
            // dd($classesList);
            $data['classesList'] = $classesList;

            $view = 'employee.dashboard.Teacher.dashboard';
        } elseif ( $this->logged_user->type == 'HomeroomTeacher') {

            $classname = (new EmployeeService())->getHomerroomTeacherClass($param);
            $data['class'] = $classname;

            $view = 'employee.dashboard.Homeroomteacher.dashboard';
        } else {

            $view = 'employee.dashboard.dashboard';
        }

        return view($view, $data);
    }

    /**
     * Employee Profile Page
     * @param  Request $request
     * @return view - employee.dashboard.profile
     */
    public function profile(Request $request)
    {
        $AcademicDegrees = AcademicDegree::select('pkAcd', 'acd_AcademicDegreeName_' . $this->current_language . ' as acd_AcademicDegreeName')
            ->get();
        $Colleges = College::select('pkCol', 'col_CollegeName_' . $this->current_language . ' as col_CollegeName')
            ->get();
        $QualificationDegrees = QualificationDegree::select('pkQde', 'qde_QualificationDegreeName_' . $this->current_language . ' as qde_QualificationDegreeName')
            ->get();

        $Municipalities = $this->getMuncipalityDropdownData($this->current_language);
        $PostalCodes = $this->getPostalCodeDropdownData($this->current_language);

        $Citizenships = Citizenship::select('pkCtz', 'ctz_CitizenshipName_' . $this->current_language . ' as ctz_CitizenshipName')
            ->get();
        $Nationalities = Nationality::select('pkNat', 'nat_NationalityName_' . $this->current_language . ' as nat_NationalityName')
            ->get();
        $Religions = Religion::select('pkRel', 'rel_ReligionName_' . $this->current_language . ' as rel_ReligionName')
            ->get();
        $Universities = University::select('pkUni', 'uni_UniversityName_' . $this->current_language . ' as uni_UniversityName')
            ->get();
        $EmployeeDesignations = EmployeeDesignation::select('pkEde', 'ede_EmployeeDesignationName_' . $this->current_language . ' as ede_EmployeeDesignationName')
            ->get();

        $schoolDetail = Employee::with('EmployeesEngagement', 'EmployeesEngagement.employeeType')->whereHas('EmployeesEngagement', function ($query) {
            $query->whereHas('employeeType', function ($query) {
                $query->where('epty_Name', 'SchoolCoordinator')
                    ->orWhere('epty_Name', 'SchoolSubAdmin')
                    ->orWhere('epty_Name', 'Teacher');
            })
                ->where('fkEenEmp', $this
                        ->logged_user
                        ->id)->where(function ($query) {
                $query->where('een_DateOfFinishEngagement', '=', null)
                    ->orWhere('een_DateOfFinishEngagement', '>=', now());
            });
        })
            ->first();

        $mainSchool = $this
            ->logged_user->sid;
        $EngagementTypes = EngagementType::select('pkEty', 'ety_EngagementTypeName_' . $this->current_language . ' as ety_EngagementTypeName')
            ->get();

        $EmployeeTypes = $this->getSelectedRoles([$this->employees[0], $this->employees[1], $this->employees[2], $this->employees[4]], false);

        $currentEmpType = EmployeeType::where('epty_Name', $this
                ->logged_user
                ->type)
                ->first();

        $EmployeesEngagements = EmployeesEngagement::where('fkEenEmp', $this
                ->logged_user
                ->id)
                ->where('fkEenSch', $mainSchool)->where('een_DateOfFinishEngagement', null)
            ->where('fkEenEpty', '!=', $currentEmpType->pkEpty)
            ->get();

        $EmployeesDetail = Employee::with([
            'employeeEducation',
            'employeeEducation.academicDegree' => function ($query) {
                $query->select('pkAcd', 'acd_AcademicDegreeName_' . $this->current_language . ' as acd_AcademicDegreeName');
            }
            , 'employeeEducation.college' => function ($query) {
                $query->select('pkCol', 'col_CollegeName_' . $this->current_language . ' as col_CollegeName');
            }
            , 'employeeEducation.university' => function ($query) {
                $query->select('pkUni', 'uni_UniversityName_' . $this->current_language . ' as uni_UniversityName');
            }
            , 'employeeEducation.university.college', 'employeeEducation.qualificationDegree' => function ($query) {
                $query->select('pkQde', 'qde_QualificationDegreeName_' . $this->current_language . ' as qde_QualificationDegreeName');
            }
            , 'employeeEducation.employeeDesignation' => function ($query) {
                $query->select('pkEde', 'ede_EmployeeDesignationName_' . $this->current_language . ' as ede_EmployeeDesignationName');
            }
            , 'municipality' => function ($query) {
                $query->select('pkMun', 'mun_MunicipalityName_' . $this->current_language . ' as mun_MunicipalityName');
            }
            , 'postalCode' => function ($query) {
                $query->select('pkPof', 'pof_PostOfficeNumber');
            }
            , 'country' => function ($query) {
                $query->select('pkCny', 'cny_CountryName_' . $this->current_language . ' as cny_CountryName');
            }
            , 'nationality' => function ($query) {
                $query->select('pkNat', 'nat_NationalityName_' . $this->current_language . ' as nat_NationalityName');
            }
            , 'religion' => function ($query) {
                $query->select('pkRel', 'rel_ReligionName_' . $this->current_language . ' as rel_ReligionName');
            }
            , 'citizenship' => function ($query) {
                $query->select('pkCtz', 'ctz_CitizenshipName_' . $this->current_language . ' as ctz_CitizenshipName');
            }
            , 'EmployeesEngagement' => function ($q) use ($mainSchool) {
                $q->where('fkEenSch', $mainSchool);
            }
            , 'EmployeesEngagement.getLatestHourRates'
            , 'EmployeesEngagement.engagementType' => function ($query) {
                $query->select('pkEty', 'ety_EngagementTypeName_' . $this->current_language . ' as ety_EngagementTypeName');
            }
            , 'EmployeesEngagement.school' => function ($query) {
                $query->select('pkSch', 'sch_SchoolName_' . $this->current_language . ' as sch_SchoolName');
            }
            , 'EmployeesEngagement.employeeType',

        ]);
        $EmployeesDetail = $EmployeesDetail->where('id', '=', $this
                ->logged_user
                ->id)
                ->first();

        if (request()
            ->ajax()) {
            return \View::make('employee.dashboard.profile')
                ->with(['Municipalities' => $Municipalities, 'PostalCodes' => $PostalCodes, 'Citizenships' => $Citizenships, 'Nationalities' => $Nationalities, 'Religions' => $Religions, 'Universities' => $Universities, 'Colleges' => $Colleges, 'EmployeeDesignations' => $EmployeeDesignations, 'QualificationDegrees' => $QualificationDegrees, 'AcademicDegrees' => $AcademicDegrees, 'EmployeesDetail' => $EmployeesDetail, 'EngagementTypes' => $EngagementTypes, 'EmployeesEngagements' => $EmployeesEngagements, 'EmployeeTypes' => $EmployeeTypes, 'currentEmpType' => $currentEmpType->pkEpty, 'MainSchool' => $mainSchool])->renderSections();
        }
        return view('employee.dashboard.profile')
            ->with(['Municipalities' => $Municipalities, 'PostalCodes' => $PostalCodes, 'Citizenships' => $Citizenships, 'Nationalities' => $Nationalities, 'Religions' => $Religions, 'Universities' => $Universities, 'Colleges' => $Colleges, 'EmployeeDesignations' => $EmployeeDesignations, 'QualificationDegrees' => $QualificationDegrees, 'AcademicDegrees' => $AcademicDegrees, 'EmployeesDetail' => $EmployeesDetail, 'EngagementTypes' => $EngagementTypes, 'EmployeesEngagements' => $EmployeesEngagements, 'EmployeeTypes' => $EmployeeTypes, 'currentEmpType' => $currentEmpType->pkEpty, 'MainSchool' => $mainSchool]);
    }

    /**
     * Update Profile
     * @param  Request $request
     * @return JSON $response
     */
    public function editProfile(Request $request)
    {

        $response = [];
        $input = $request->all();
        $image = $request->file('upload_profile');

        $emailExist = Employee::where('email', '=', $input['email'])->where('id', '!=', $this
                ->logged_user
                ->id)
                ->get();
        $govIdExist = Employee::where('emp_EmployeeID', '=', $input['emp_EmployeeID'])->where('id', '!=', $this
                ->logged_user
                ->id)
                ->get();
        $tempIdExist = Employee::where('emp_TempCitizenId', '=', $input['emp_TempCitizenId'])->where('emp_TempCitizenId', '!=', '')
            ->where('id', '!=', $this
                    ->logged_user
                    ->id)
                ->get();
        $emailAdmExist = Admin::where('email', '=', $input['email'])->get();

        $emailExistCount = $emailExist->count();
        $emailAdmExistCount = $emailAdmExist->count();
        $tempIdExistCount = $tempIdExist->count();
        $govIdExistCount = $govIdExist->count();

        if ($emailExistCount != 0 || $emailAdmExistCount != 0) {
            $response['status'] = false;
            $response['message'] = trans('message.msg_email_exist');
            return response()
                ->json($response);
        }

        if ($govIdExistCount != 0) {
            $response['status'] = false;
            $response['message'] = trans('message.msg_employee_id_exist');
            return response()
                ->json($response);
        }

        if ($tempIdExistCount != 0) {
            $response['status'] = false;
            $response['message'] = trans('message.msg_temp_citizen_id_exist');
            return response()
                ->json($response);
        }

        $user = Employee::findorfail($this
                ->logged_user
                ->id);

            $user->email = $input['email'];
            $user->emp_EmployeeName = $input['emp_EmployeeName'];
            $user->emp_EmployeeSurname = $input['emp_EmployeeSurname'];
            $user->emp_EmployeeID = $input['emp_EmployeeID'];
            $user->emp_TempCitizenId = $input['emp_TempCitizenId'];
            $user->emp_EmployeeGender = $input['emp_EmployeeGender'];
            $user->emp_DateOfBirth = UtilHelper::sqlDate($input['emp_DateOfBirth']);
        $user->emp_PlaceOfBirth = $input['emp_PlaceOfBirth'];
        $user->emp_Address = $input['emp_Address'];
        $user->emp_PhoneNumber = $input['emp_PhoneNumber'];
        $user->emp_Notes = $input['emp_Notes'];
        $user->fkEmpMun = $input['fkEmpMun'];
        $user->fkEmpPof = $input['fkEmpPof'];
        $user->fkEmpCny = $input['fkEmpCny'];
        $user->fkEmpNat = $input['fkEmpNat'];
        $user->fkEmpRel = $input['fkEmpRel'];
        $user->fkEmpCtz = $input['fkEmpCtz'];

        if (!empty($image)) {
            $input['emp_PicturePath'] = time() . '.' . $image->getClientOriginalExtension();
            $destinationPath = storage_path('app/public/images/users');
            $image->move($destinationPath, $input['emp_PicturePath']);
            if (!empty($user->emp_PicturePath)) {
                $filepath = storage_path('app/public/images/users') . "/" . $user->emp_PicturePath;
                if (file_exists($filepath)) {
                    unlink($filepath);
                }
            }
            $user->emp_PicturePath = $input['emp_PicturePath'];
        }

        if ($user->save()) {
            $this->logged_user = $user;
            $this
                ->logged_user->utype = 'employee';
            $mdata = Employee::with('employeesEngagement.employeeType')->where('id', '=', $this
                    ->logged_user
                    ->id)
                    ->first();
            $this
                ->logged_user->type = $mdata->employeesEngagement[0]
                ->employeeType->epty_Name;
            $response['status'] = true;
            $response['message'] = trans('message.msg_profile_update_success');

        } else {
            $response['status'] = false;
            $response['message'] = trans('message.msg_something_wrong');
        }

        return response()
            ->json($response);
    }

    /**
     * Fetch College
     * @param  Request $request
     * @return JSON $response
     */
    public function fetchCollege(Request $request)
    {
        $response = [];

        $data = $request->all();

        $Colleges = College::select('pkCol', 'col_CollegeName_' . $this->current_language . ' as col_CollegeName')
            ->where('fkColUni', $data['cid'])->get();

        if (!empty($Colleges)) {
            $response['status'] = true;
            $response['data'] = $Colleges;
        } else {
            $response['status'] = false;
        }
        return response()->json($response);
    }

    /**
     * Update Education details
     * @param  Request $request
     * @return JSON $response
     */
    public function editEducationDetails(Request $request)
    {
        /**
         * Used for Updating employee education details
         */
        $response = [];

        $data = $request->all();
        $tdata = $data['total_details'];
        $details = [];
        for ($i = 1; $i <= $tdata; $i++) {
            $filename = '';
            $image = '';
            if ($request->hasFile('eed_DiplomaPicturePath_' . $i)) {
                $image = $request->file('eed_DiplomaPicturePath_' . $i);
                $filename = time() . $i . '.' . $image->getClientOriginalExtension();
                $destinationPath = storage_path('app/public/files/users');
                $image->move($destinationPath, $filename);
            } else {
                $filename = $data['file_name_' . $i];
            }
            $details[] = ['fkEedEmp' => $this
                    ->logged_user->id, 'fkEedCol' => $data['fkEedCol_' . $i], 'fkEedUni' => $data['fkEedUni_' . $i], 'fkEedAcd' => $data['fkEedAcd_' . $i], 'fkEedQde' => $data['fkEedQde_' . $i], 'fkEedEde' => $data['fkEedEde_' . $i], 'eed_ShortTitle' => $data['eed_ShortTitle_' . $i], 'eed_SemesterNumbers' => $data['eed_SemesterNumbers_' . $i], 'eed_EctsPoints' => $data['eed_EctsPoints_' . $i], 'eed_YearsOfPassing' => $data['eed_YearsOfPassing_' . $i], 'eed_Notes' => $data['eed_Notes_' . $i], 'eed_PicturePath' => $filename];
        }

        EmployeesEducationDetail::where('fkEedEmp', $this->logged_user->id)->delete();

        $id = EmployeesEducationDetail::insert($details);

        if (!empty($id)) {
            $response['status'] = true;
            $response['message'] = trans('message.msg_qualification_update_success');
        } else {
            $response['status'] = false;
            $response['message'] = trans('message.msg_something_wrong');
        }

        return response()
            ->json($response);
    }

    /**
     * Update Engagement details
     * @param  Request $request
     * @return JSON $response
     */
    public function editEngagementDetails(Request $request)
    {
        $response = [];
        $data = $request->all();

        $tdata = $data['total_details'];
        $details = [];

        $EmployeeTypes = $this->getSelectedRoles([$this->employees[0], $this->employees[1], $this->employees[2]], false);
        $SchoolData = School::select('sch_SchoolName_en')->where('pkSch', $data['sid'])->first();

        $SchoolCoordinator = '';
        $Principal = '';
        $Teacher = '';

        foreach ($EmployeeTypes as $k => $v) {
            if ($v->epty_Name == 'SchoolCoordinator') {
                $SchoolCoordinator = $v->pkEpty;
            }
            if ($v->epty_Name == 'Principal') {
                $Principal = $v->pkEpty;
            }
            if ($v->epty_Name == 'Teacher') {
                $Teacher = $v->pkEpty;
            }
        }

        for ($i = 1; $i <= $tdata; $i++) {
            if ($data['fkEenEpty_' . $i] == $Principal) {
                $ee = EmployeesEngagement::where('fkEenSch', $data['sid'])
                    ->where('fkEenEpty', $Principal)
                    ->where('fkEenEmp', $this->logged_user->id)
                    ->where('een_DateOfFinishEngagement', '=', null)
                    ->first();

                if (!empty($ee)) {
                    $ee->fkEenEty = $data['fkEenEty_' . $i];
                    $ee->een_DateOfEngagement = UtilHelper::sqlDate($data['start_date_' . $i]);
                    $ee->een_Notes = $data['note_' . $i];
                    if (isset($data['end_date_' . $i]) && !empty($data['end_date_' . $i])) {
                        $ee->een_DateOfFinishEngagement = UtilHelper::sqlDate($data['end_date_' . $i]);
                    }
                    $ee->save();

                    EmployeesWeekHourRates::addHourlyRate($ee->pkEen, $data['ewh_WeeklyHoursRate_' . $i], $data['ewh_Notes_' . $i], UtilHelper::sqlDate($data['start_date_' . $i]));
                } else {
                    $ee = new EmployeesEngagement;
                    $ee->fkEenSch = $data['sid'];
                    $ee->fkEenEmp = $this
                        ->logged_user->id;
                    $ee->fkEenEty = $data['fkEenEty_' . $i];
                    $ee->fkEenEpty = $data['fkEenEpty_' . $i];
                    $ee->een_DateOfEngagement = UtilHelper::sqlDate($data['start_date_' . $i]);
                    $ee->een_Notes = $data['note_' . $i];
                    if (isset($data['end_date_' . $i]) && !empty($data['end_date_' . $i])) {
                        $ee->een_DateOfFinishEngagement = UtilHelper::sqlDate($data['end_date_' . $i]);
                    }
                    $ee->save();

                    EmployeesWeekHourRates::addHourlyRate($ee->pkEen, $data['ewh_WeeklyHoursRate_' . $i], $data['ewh_Notes_' . $i], UtilHelper::sqlDate($data['start_date_' . $i]));

                    EmployeesEngagement::where('fkEenSch', $data['sid'])
                        ->where('fkEenEpty', $Principal)
                        ->where('pkEen', '!=', $ee->id)
                        ->where('fkEenEmp', '!=', $this->logged_user->id)
                        ->where('een_DateOfFinishEngagement', '=', null)
                        ->update(['een_DateOfFinishEngagement' => UtilHelper::sqlDate($data['start_date_' . $i])]);

                    $dataemp = ['email' => $this
                            ->logged_user->email, 'name' => $this
                            ->logged_user->emp_EmployeeName . " " . $this->logged_user->emp_EmployeeSurname, 'school' => $SchoolData->sch_SchoolName_en, 'subject' => 'New School Principal Assign'];

                    MailHelper::sendNewPrincipalAssign($dataemp);
                }

            }

            if ($data['fkEenEpty_' . $i] == $SchoolCoordinator) {
                $ee = EmployeesEngagement::where('fkEenSch', $data['sid'])
                    ->where('fkEenEpty', $SchoolCoordinator)
                    ->where('fkEenEmp', $this->logged_user->id)
                    ->where('een_DateOfFinishEngagement', '=', null)
                    ->first();

                if (!empty($ee)) {
                    $ee->fkEenEty = $data['fkEenEty_' . $i];
                    $ee->een_DateOfEngagement = UtilHelper::sqlDate($data['start_date_' . $i]);
                    $ee->een_Notes = $data['note_' . $i];
                    if (isset($data['end_date_' . $i]) && !empty($data['end_date_' . $i])) {
                        $ee->een_DateOfFinishEngagement = UtilHelper::sqlDate($data['end_date_' . $i]);
                    }
                    $ee->save();

                    EmployeesWeekHourRates::addHourlyRate($ee->pkEen, $data['ewh_WeeklyHoursRate_' . $i], $data['ewh_Notes_' . $i], UtilHelper::sqlDate($data['start_date_' . $i]));
                }
            }

            if ($data['fkEenEpty_' . $i] == $Teacher) {
                $ee = EmployeesEngagement::where('fkEenSch', $data['sid'])->where('fkEenEpty', $Teacher)->where('fkEenEmp', $this
                        ->logged_user
                        ->id)
                        ->where('een_DateOfFinishEngagement', '=', null)
                    ->first();

                if (!empty($ee)) {
                    $ee->fkEenEty = $data['fkEenEty_' . $i];
                    $ee->een_DateOfEngagement = UtilHelper::sqlDate($data['start_date_' . $i]);
                    $ee->een_Notes = $data['note_' . $i];
                    if (isset($data['end_date_' . $i]) && !empty($data['end_date_' . $i])) {
                        $ee->een_DateOfFinishEngagement = UtilHelper::sqlDate($data['end_date_' . $i]);
                    }
                    $ee->save();

                    EmployeesWeekHourRates::addHourlyRate($ee->pkEen, $data['ewh_WeeklyHoursRate_' . $i], $data['ewh_Notes_' . $i], UtilHelper::sqlDate($data['start_date_' . $i]));
                } else {
                    $ee = new EmployeesEngagement;
                    $ee->fkEenSch = $data['sid'];
                    $ee->fkEenEmp = $this
                        ->logged_user->id;
                    $ee->fkEenEty = $data['fkEenEty_' . $i];
                    $ee->fkEenEpty = $data['fkEenEpty_' . $i];
                    $ee->een_Notes = $data['note_' . $i];
                    $ee->een_DateOfEngagement = UtilHelper::sqlDate($data['start_date_' . $i]);
                    if (isset($data['end_date_' . $i]) && !empty($data['end_date_' . $i])) {
                        $ee->een_DateOfFinishEngagement = UtilHelper::sqlDate($data['end_date_' . $i]);
                    }
                    $ee->save();

                    EmployeesWeekHourRates::addHourlyRate($ee->pkEen, $data['ewh_WeeklyHoursRate_' . $i], $data['ewh_Notes_' . $i], UtilHelper::sqlDate($data['start_date_' . $i]));

                    $dataemp = ['email' => $this
                            ->logged_user->email, 'name' => $this
                            ->logged_user->emp_EmployeeName . " " . $this->logged_user->emp_EmployeeSurname, 'school' => $SchoolData->sch_SchoolName_en, 'subject' => 'New School Teacher Assign'];

                    MailHelper::sendNewTeacherAssign($dataemp);
                }
            }
        }

        $response['status'] = true;
        $response['message'] = trans('message.msg_work_exp_update_success');

        return response()
            ->json($response);
    }

    /**
     * Switch Role
     * @param  Request $request
     * @return JSON $response
     */
    public function switchRole(Request $request)
    {
        $response = [];
        $data = $request->all();

        $this->logged_user->type = $data['role'];
        Session::put('curr_emp_type', $data['role']);

        $this->logged_user->sid = $data['sid'];
        Session::put('curr_emp_sid', $data['sid']);

        $empType = $data['eid'];
        $this->logged_user->eid = $empType;
        Session::put('curr_emp_eid', $data['eid']);

        if (isset($data['schoolyear']) && !empty($data['schoolyear'])) {
            $currentYear = $data['schoolyear'];
            $this->logged_user->syid = $currentYear;
            Session::put('curr_school_year', $currentYear);
        } else {
            $schoolYearData = $this->getCurrentYear();
            $schoolYear = $schoolYearData->pkSye;
            $this->logged_user->syid = $schoolYear;
            Session::put('curr_school_year', $schoolYear);
        }

        if (isset($data['semesterId'])) {
            $currentSem = $data['semesterId'];
            $this->logged_user->semid = $currentSem;
            Session::put('curr_school_sem', $currentSem);
        } else {
            $defaultSemdata = $this->getCurrentSemester($data['sid']);
            $defaultSem = $defaultSemdata->fkSchEdp;
            $this->logged_user->semid = $defaultSem;
            Session::put('curr_school_sem', $defaultSem);
        }

        if (isset($data['classId']) && !empty($data['classId'])) {
            $param = ['classId' => $data['classId'] ?? 0,
                'courseId' => $data['courseId'] ?? 0,
                'ccsGroups' => $data['ccsgroups'] ?? '',
                'coursegroupId' => $data['courseGroup'] ?? 0,
                'subclassId' => $data['subClass'] ?? 0,
                'villageschoolId' => $data['villageSchool'] ?? 0,
                'courseName' => $data['courseLabel'] ?? "",
            ];

            $this->logged_user->cosid = $param;
            Session::put('curr_course_class', $param);
        }

        if (isset($data['shiftId']) && !empty($data['shiftId'])) {
            $this->logged_user->shiftid = $data['shiftId'];
            Session::put('curr_shift', $data['shiftId']);
        }

        $response['status'] = true;
        $response['redirect'] = url($this
                ->logged_user->utype . '/dashboard');
            return response()->json($response);
    }

}
