<?php
/**
 * ClassCreationSubClassController
 *
 * @package    Laravel
 * @subpackage Controller
 * @since      1.0
 */

namespace App\Http\Controllers\Employee;

use App\Helpers\MailHelper;
use App\Http\Controllers\Controller;
use App\Http\Traits\CommonTrait;
use App\Models\ClassCreation;
use App\Models\ClassCreationGrades;
use App\Models\ClassCreationSemester;
use App\Models\ClassStudentsAllocation;
use App\Models\ClassStudentsSemester;
use App\Models\ClassTeachersCourseAllocation;
use App\Models\Course;
use App\Models\Employee;
use App\Models\EmployeesEngagement;
use App\Models\EmployeesWeekHourRates;
use App\Models\EnrollStudent;
use App\Models\Grade;
use App\Models\HomeRoomTeacher;
use App\Models\School;
use App\Models\SubClassGroup;
use Illuminate\Http\Request;

class ClassCreationSubClassController extends Controller
{
    use CommonTrait;

    /**
     * Fetch SchoolYear Enrolled Students
     *
     * @param \Illuminate\Http\Request $request
     * @return JSON - $response
     */
    public function school_year_students_list(Request $request)
    {
        $schoolYear = $request->year;
        $school = $request->school;
        $villageschool = !empty($request->villageschool) ? $request->villageschool : null;
        $search = $request->term;
        $grades = $request->grades ? $request->grades : [];
        $page = $request->page;

        $perpage = 50;
        $offset = ($page - 1) * $perpage;

        $query = EnrollStudent::with([
                'student' => function ($q) {
                    $q->GetStudentName();
                },
                'grade' => function ($q) {
                    $q->select('pkGra', 'gra_GradeNumeric');
                }])
            ->where('fkSteSch', $school)
            ->whereIn('fkSteGra', $grades)
            ->where('fkSteViSch', $villageschool)
            ->where('fkSteSye', $schoolYear);

        if (!empty($search)) {
            $query = $query->whereHas('student', function ($q) use ($search) {
                $q->where('stu_StudentName', 'like', '%' . $search . '%')
                    ->orWhere('stu_StudentSurname', 'like', '%' . $search . '%');
            });
        }

        $Students = $query->orderBy('fkSteGra', 'ASC')
            ->paginate($perpage);

        $hasMorePages = $Students->hasMorePages();
        $result = [];

        foreach ($Students as $val) {
            $temp['id'] = $val->pkSte;
            $temp['text'] = $val->student->studentname. " - " . trans('general.gn_grade') . " " . $val->grade->gra_GradeNumeric;
            $result[] = $temp;
        }

        $response['pagination']['more'] = $hasMorePages;
        $response['results'] = $result;

        return response()->json($response);
    }

    /**
     * View Subclass Page
     *
     * @param \Illuminate\Http\Request $request
     * @param [int] $id
     * @return view - employee.classCreation.viewSubclass
     */
    public function viewSubClass($id)
    {
        if (!empty($id)) {
            
            return view('employee.classCreation.viewSubclass', ['id' => $id]);
        }
    }

    /**
     * View Subclass Student Page
     *
     * @param [int] $id
     * @return view - employee.classCreation.viewSubclassStudents
     */
    public function viewSubclassStudents($id)
    {
        if (!empty($id)) {
            $classStudentdata = ClassStudentsAllocation::whereNotNull('fkCsaScg')->find($id);
            if (!empty($classStudentdata)) {
                $fkCsaSem = $classStudentdata->fkCsaSem;
                $fkCsaEmc = $classStudentdata->fkCsaEmc;
                $fkCsaScg = $classStudentdata->fkCsaScg;

                $courseData = Course::select('crs_CourseName_' . $this->current_language . ' AS crs_CourseName')->where('pkCrs', $fkCsaEmc)->first();
                $course = $courseData->crs_CourseName;

                $subclassData = SubClassGroup::select('scg_Name_' . $this->current_language . ' AS scg_Name')->where('pkScg', $fkCsaScg)->first();
                $subclass = $subclassData->scg_Name;

                $semData = ClassStudentsSemester::select('fkSemCcs')->where('pkSem', $fkCsaSem)->first();
                $semId = $semData->fkSemCcs;

                $mdata = ClassCreation::whereHas(
                    'classCreationSemester', function ($q) use ($semId) {
                        $q->where('pkCcs', $semId);
                    })
                    ->with([
                        'classCreationSemester' => function ($q) use ($semId) {
                            $q->where('pkCcs', $semId);
                        }
                        , 'classCreationSchoolYear' => function ($q) {
                            $q->select('pkSye', 'sye_NameNumeric', 'sye_NameCharacter_' . $this->current_language . ' AS sye_NameCharacter');
                        }
                        , 'classCreationClasses' => function ($q) {
                            $q->select('pkCla', 'cla_ClassName_' . $this->current_language . ' as cla_ClassName');
                        }
                        , 'classCreationSemester.semester' => function ($q) {
                            $q->select('pkEdp', 'edp_EducationPeriodName_' . $this->current_language . ' as edp_EducationPeriodName');
                        }
                        , 'classCreationSemester.classStudentsSemester.studentEnroll.grade' => function ($q) {
                            $q->select('pkGra', 'gra_GradeNameRoman', 'gra_GradeNumeric');
                        },
                    ])->first();

                $classStudents = ClassStudentsAllocation::whereHas('classSemester.classCreationSemester', function ($q) use ($semId) {
                    $q->where('pkCcs', $semId);
                })->with([
                    'classSemester' => function ($q) {
                        $q->select('pkSem', 'fkSemSen');
                    },
                    'classSemester.studentEnroll.student' => function ($q) {
                        $q->select('id', \DB::raw("CONCAT(stu_StudentName, ' ', stu_StudentSurname) AS stuName"));
                    },
                    'classSemester.studentEnroll.grade' => function ($q) {
                        $q->select('pkGra', 'gra_GradeNameRoman', 'gra_GradeNumeric');
                    },
                    'classCreationTeachers.employeesEngagement.employee' => function ($q) {
                        $q->select('id', \DB::raw("CONCAT(emp_EmployeeName, ' ', emp_EmployeeSurname) AS empName"));
                    },
                ])->where('fkCsaEmc', $fkCsaEmc)
                    ->where('fkCsaScg', $fkCsaScg)
                    ->get()
                    ->toArray();

                $classData = [];
                if (!empty($classStudents)) {
                    foreach ($classStudents as $key => $value) {
                        $data['pkCsa'] = $value['pkCsa'];
                        $data['grade'] = $value['class_semester']['student_enroll']['grade']['gra_GradeNumeric'];
                        $data['stuName'] = $value['class_semester']['student_enroll']['student']['stuName'];
                        $data['empName'] = $value['class_creation_teachers'][0]['employees_engagement']['employee']['empName'];
                        $classData[] = $data;
                    }
                }

                $pageData = compact('mdata', 'classData', 'course', 'subclass');
                if (request()
                    ->ajax()) {
                    return \View::make('employee.classCreation.viewSubclassStudents')
                        ->with($pageData)->renderSections();
                }

                return view('employee.classCreation.viewSubclassStudents', $pageData);
            }
        }
    }

    /**
     * Create Sub class
     * @param  Request $request
     * @return JSON $response
     */
    public function createSubClass(Request $request)
    {
        $input = $request->all();
        $pkCcs = $input['pkCcs'];

        if (count($input['stu_ids'])) {
            for ($i = 0; $i < count($input['stu_ids']); $i++) {
                $fkSemSen = $input['stu_ids'][$i];
                $courseId = $input['course'][$i];
                $subClassId = $input['subClass'][$i];
                $teacherId = $input['teacher'][$i];

                $classStudent = ClassStudentsSemester::select('pkSem')->where('fkSemCcs', $pkCcs)->where('fkSemSen', $fkSemSen)->first();
                if (!empty($classStudent)) {
                    $pkSem = $classStudent->pkSem;
                    $subClassCourse = ClassStudentsAllocation::where('fkCsaSem', $pkSem)->where('fkCsaEmc', $courseId)->first();
                    if (!empty($subClassCourse)) {
                        //update course teacher and update subclassgroup
                        $subClassCourse->fkCsaScg = $subClassId;
                        $subClassCourse->save();
                        if ($subClassCourse->save()) {
                            $fkCtcCsa = $subClassCourse->pkCsa;

                            //delete old teachers
                            ClassTeachersCourseAllocation::where('fkCtcCsa', $fkCtcCsa)->delete();

                            $newTeacher = new ClassTeachersCourseAllocation;
                            $newTeacher->fkCtcCsa = $fkCtcCsa;
                            $newTeacher->fkCtcEeg = $teacherId;
                            $newTeacher->fkCtcCcs = $pkCcs;
                            $newTeacher->Ctc_start = now();
                            $newTeacher->save();
                        }
                    } else {
                        //add course
                        $lastInsertId = ClassStudentsAllocation::insertGetId(['fkCsaEmc' => $courseId, 'fkCsaSem' => $pkSem, 'fkCsaScg' => $subClassId]);
                        if (!empty($lastInsertId)) {
                            $newTea = new ClassTeachersCourseAllocation;
                            $newTea->fkCtcCsa = $lastInsertId;
                            $newTea->fkCtcEeg = $teacherId;
                            $newTea->fkCtcCcs = $pkCcs;
                            $newTea->Ctc_start = now();
                            $newTea->save();
                        }
                    }
                }
            }
        }

        $response['status'] = true;
        $response['message'] = trans('message.msg_subclass_create_success');

        return response()
            ->json($response);
    }

    /**
     * Delete Subclass Course
     *
     * @param \Illuminate\Http\Request $request
     * @return JSON - $response
     */
    public function deleteSubClassCourse(Request $request)
    {
        $input = $request->all();
        $flag = $input['flag'];
        $pkCsa = $input['did'];

        if ($flag == 1) {
            ClassStudentsAllocation::find($pkCsa)->delete();
            ClassTeachersCourseAllocation::where('fkCtcCsa', $pkCsa)->delete();
            $response['message'] = trans('message.msg_course_deleted');
        } else {
            ClassStudentsAllocation::find($pkCsa)->update(['fkCsaScg' => null]);
            $response['message'] = trans('message.msg_course_subclass_deleted');
        }

        $response['status'] = true;

        return response()->json($response);
    }

    /**
     * Class creation sub class page
     * @param  Int $id
     * @return view - employee.classCreation.classCreationSubClass
     */
    public function classCreationSubClass($id)
    {
        $mdata = '';
        if (!empty($id)) {
            \DB::statement("SET SQL_MODE=''");
            $mdata = ClassCreation::whereHas('classCreationSemester', function ($q) use ($id) {
                $q->where('pkCcs', $id);
            })->with(['classCreationSchoolYear' => function ($q) {
                $q->select('pkSye', 'sye_NameNumeric', 'sye_NameCharacter_' . $this->current_language . ' AS sye_NameCharacter');
            }
                , 'classCreationGrades.grade' => function ($q) {
                    $q->select('pkGra', 'gra_GradeNameRoman', 'gra_GradeNumeric');
                }
                , 'classCreationSemester.homeRoomTeacher.employeesEngagement.employee' => function ($q) {
                    $q->select('id', 'emp_EmployeeName', 'emp_EmployeeSurname');
                }
                , 'classCreationSemester.chiefStudent.student', 'classCreationSemester.treasureStudent.student', 'classCreationClasses' => function ($q) {
                    $q->select('pkCla', 'cla_ClassName_' . $this->current_language . ' as cla_ClassName');
                }
                , 'classCreationSemester' => function ($q) use ($id) {
                    $q->where('pkCcs', $id);
                }
                , 'classCreationSemester.semester' => function ($q) {
                    $q->select('pkEdp', 'edp_EducationPeriodName_' . $this->current_language . ' as edp_EducationPeriodName');
                }
                , 'classCreationSemester.classStudentsSemester.studentEnroll.student', 'classCreationSemester.classStudentsSemester.studentEnroll.grade' => function ($q) {
                    $q->select('pkGra', 'gra_GradeNameRoman', 'gra_GradeNumeric');
                }
                , 'classCreationSemester.classStudentsSemester.studentEnroll.educationProgram' => function ($q) {
                    $q->select('pkEdp', 'edp_Name_' . $this->current_language . ' as edp_Name');
                }
                , 'classCreationSemester.classStudentsSemester.studentEnroll.educationPlan' => function ($q) {
                    $q->select('pkEpl', 'epl_EducationPlanName_' . $this->current_language . ' as epl_EducationPlanName');
                }
                ,
            ])->first();

            $subClassGroups = SubClassGroup::select('pkScg', 'scg_Name_' . $this->current_language . ' as scg_Name')
                ->get();

            $oldStuSemAllocId = ClassStudentsSemester::where('fkSemCcs', $id)->pluck('pkSem')
                ->all();
            $oldStuAllocId = ClassStudentsAllocation::whereIn('fkCsaSem', $oldStuSemAllocId)->get()
                ->pluck('pkCsa')
                ->all();
            $oldStuAlloc = ClassStudentsSemester::with(['studentEnroll', 'classStudentsAllocation.classCreationTeachers'])->where('fkSemCcs', '=', $id)->get();

            $oldTeachers = ClassTeachersCourseAllocation::select('fkCtcEeg', 'fkCtcCsa')->whereIn('fkCtcCsa', $oldStuAllocId)->get();

            $existTeacher = [];

            foreach ($oldStuAlloc as $k => $v) {
                foreach ($v->classStudentsAllocation as $kc => $vc) {
                    foreach ($vc->classCreationTeachers as $kt => $vt) {
                        $existTeacher[$v
                                ->studentEnroll
                                ->fkSteGra][$vc->fkCsaEmc][] = $vt->fkCtcEeg;
                    }
                }

            }

            $oldTeachersId = [];
            foreach ($oldTeachers as $key => $value) {
                $oldTeachersId[] = $value->fkCtcEeg;
            }

            $employees = EmployeesEngagement::with(['employee' => function ($q) { //Employee engagment id with employee
                $q->select('id', \DB::raw("CONCAT(emp_EmployeeName, ' ', emp_EmployeeSurname) AS empName"));
            },
            ])
                ->where('een_DateOfFinishEngagement', null)
                ->whereIn('pkEen', $oldTeachersId)->get()
                ->pluck('employee.empName', 'pkEen');

            $grades = ClassCreationGrades::where('fkCcgClr', $mdata->pkClr)->get()
                ->pluck('fkCcgGra')
                ->toArray();
            asort($grades);

            $selectetTeachers = [];

            $mainSchool = $this
                ->logged_user->sid;

            //fetch school courses
            $stuEnrollsIds = ClassStudentsSemester::where('fkSemCcs', $id)->pluck('fkSemSen')
                ->all();

            $educationPlans = EnrollStudent::whereIn('pkSte', $stuEnrollsIds)->get()
                ->pluck("fkSteEpl")
                ->all();

            $courses = Grade::select('*', 'gra_GradeNumeric')->with(['educationPlansMandatoryCourse' => function ($query) use ($grades, $educationPlans) {
                $query->whereIn('fkEmcGra', $grades)->whereIn('fkEmcEpl', $educationPlans);
            }
                , 'educationPlansMandatoryCourse.mandatoryCourseGroup' => function ($query) use ($grades, $educationPlans) {
                    $query->select('pkCrs', 'crs_CourseName_' . $this->current_language . ' as crs_CourseName');
                }
                , 'educationPlansMandatoryCourse.educationPlan' => function ($query) use ($grades) {
                    $query->select('pkEpl', 'epl_EducationPlanName_' . $this->current_language . ' as epl_EducationPlanName');
                },
            ])
                ->whereIn('pkGra', $grades)->get();

            $existingSubClass = EnrollStudent::whereHas('classStudents', function ($q) use ($id) {
                $q->where('fkSemCcs', $id)->where('fkSemCcs', '!=', null);
            })->with(['student', 'classStudents.classStudentsAllocation.classCreationTeachers.employeesEngagement.employee', 'classStudents' => function ($q) use ($id) {
                $q->where('fkSemCcs', $id)->where('fkSemCcs', '!=', null);
            }
                , 'classStudents.classStudentsAllocation.subClassGroup' => function ($q) use ($id) {
                    $q->select('pkScg', 'scg_Name_' . $this->current_language . ' as scg_Name');
                }
                , 'classStudents.classStudentsAllocation.classCreationCourses' => function ($q) use ($id) {
                    $q->select('pkCrs', 'crs_CourseName_' . $this->current_language . ' as crs_CourseName');
                }
                , 'grade' => function ($q) {
                    $q->select('pkGra', 'gra_GradeNameRoman', 'gra_GradeNumeric');
                }
                , 'educationProgram' => function ($q) {
                    $q->select('pkEdp', 'edp_Name_' . $this->current_language . ' as edp_Name');
                }
                , 'educationPlan' => function ($q) {
                    $q->select('pkEpl', 'epl_EducationPlanName_' . $this->current_language . ' as epl_EducationPlanName');
                },
            ])
                ->get();

        }
        $results = compact('mdata', 'courses', 'employees', 'existTeacher', 'subClassGroups', 'existingSubClass', 'id');

        if (request()
            ->ajax()) {
            return \View::make('employee.classCreation.classCreationSubClass')
                ->with($results)->renderSections();
        }
        return view('employee.classCreation.classCreationSubClass', $results);
    }

    /**
     * View Homeroomteacher History Page
     * @param  Request $request
     * @param  Int  $id
     * @return view - employee.classCreation.viewHomeRoomTeacher
     */
    public function viewHomeRoomTeacher(Request $request, $id)
    {
        if (!empty($id)) {
            $mainSchool = $this
                ->logged_user->sid;
            \DB::statement("SET SQL_MODE=''");
            $mdata = ClassCreation::whereHas('classCreationSemester', function ($q) use ($id) {
                $q->where('pkCcs', $id);
            })->with(['classCreationSchoolYear' => function ($q) {
                $q->select('pkSye', 'sye_NameNumeric', 'sye_NameCharacter_' . $this->current_language . ' AS sye_NameCharacter');
            }
                , 'classCreationGrades.grade' => function ($q) {
                    $q->select('pkGra', 'gra_GradeNameRoman', 'gra_GradeNumeric');
                }
                , 'classCreationSemester.homeRoomTeacher.employeesEngagement.employee' => function ($q) {
                    $q->select('id', 'emp_EmployeeName', 'emp_EmployeeSurname');
                }
                , 'classCreationSemester.chiefStudent.student'
                , 'classCreationSemester.treasureStudent.student'
                , 'classCreationClasses' => function ($q) {
                    $q->select('pkCla', 'cla_ClassName_' . $this->current_language . ' as cla_ClassName');
                }
                , 'classCreationSemester.semester' => function ($q) {
                    $q->select('pkEdp', 'edp_EducationPeriodName_' . $this->current_language . ' as edp_EducationPeriodName');
                }
                , 'classCreationSemester.classStudentsSemester.studentEnroll.grade' => function ($q) {
                    $q->select('pkGra', 'gra_GradeNameRoman', 'gra_GradeNumeric');
                },
            ])->first();

            $ccdata = ClassCreationSemester::select("fkCcsClr")->where('pkCcs', $id)->first();
            $Grades = ClassCreationGrades::with(['grade' => function ($q) {
                $q->select('pkGra', 'gra_GradeNameRoman', 'gra_GradeNumeric');
            }])->where('fkCcgClr', $ccdata->fkCcsClr)
                ->get()
                ->pluck('grade.gra_GradeNumeric')
                ->toArray();
            asort($Grades);
            $HomeRoomTeacher = HomeRoomTeacher::with([
                'employeesEngagement',
                'employeesEngagement.getLatestHourRates',
                'employeesEngagement.getAllHourRates',
                'employeesEngagement.engagementType' => function ($q) {
                    $q->select('pkEty', 'ety_EngagementTypeName_' . $this->current_language . ' as ety_EngagementTypeName');
                }
                , 'employeesEngagement.employee'])
                ->where('fkHrtCcs', $id)->withTrashed()->get();

            $schoolYearData = $this->getCurrentYear();
            $schoolYear = $schoolYearData->pkSye;

            $currentSemData = $this->getCurrentSemester($mainSchool);
            $currentSem = $currentSemData->fkSchEdp;

            $classCreationSemester = ClassCreationSemester::with(['classCreation' => function ($q) use ($schoolYear, $mainSchool) {
                $q->where('fkClrSye', $schoolYear)->where('fkClrSch', $mainSchool);
            },
            ])->where('fkCcsEdp', $currentSem)->get()
                ->pluck('pkCcs');

            $homeTeacherTypes = $this->getSelectedRoles([$this->employees[0], $this->employees[1], $this->employees[2]]);
            $employees = Employee::select('id',
                \DB::raw('(CASE
                    WHEN emp_EmployeeID IS NOT NULL THEN emp_EmployeeID
                    ELSE emp_TempCitizenId
                    END) AS empId'),
                \DB::raw("CONCAT(emp_EmployeeName, ' ', emp_EmployeeSurname) AS empName")
            )
                ->whereHas('EmployeesEngagement', function ($q) use ($mainSchool, $homeTeacherTypes) {
                    $q->whereIn('fkEenEpty', $homeTeacherTypes)
                        ->where('fkEenSch', $mainSchool)
                        ->whereNull('een_DateOfFinishEngagement');
                })->get();

            $activeHomeRoomTeacher = HomeRoomTeacher::with(['employeesEngagement'])->whereHas('employeesEngagement', function ($q) use ($mainSchool) {
                $q->whereNull('een_DateOfFinishEngagement')
                    ->where('fkEenEpty', 6)
                    ->where('fkEenSch', $mainSchool);
            })->where('fkHrtCcs', $id)->first();

            $engId = 0;
            $hrtId = 0;
            if (!empty($activeHomeRoomTeacher)) {
                $hrtId = $activeHomeRoomTeacher->pkHrt;
                $engId = $activeHomeRoomTeacher->employeesEngagement->fkEenEmp;
            }

            return view('employee.classCreation.viewHomeRoomTeacher')
                ->with(compact('HomeRoomTeacher', 'mdata', 'Grades', 'employees', 'hrtId', 'engId', 'id'));
        } else {
            $response['status'] = false;
            $response['message'] = trans('message.msg_something_wrong');
            return response()->json($response);
        }
    }

    /**
     * Update Homeroom teacher
     * @param  Request $request
     * @return JSON $response
     */
    public function updateHomeroomTeacher(Request $request)
    {
        $input = $request->all();
        $oldEngId = $input['engId'];
        $newEngId = $input['homeroomteacher_sel'];
        $pkCcs = $input['pkCcs'];
        $hrtId = $input['hrtId'];

        if ($oldEngId != $newEngId) {
            $existEngagement = EmployeesEngagement::where('fkEenEmp', $newEngId)
                ->whereNull('een_DateOfFinishEngagement')
                ->orderBy('pkEen', 'DESC')
                ->first();

            if (!empty($existEngagement)) {
                $replicateRow = $existEngagement->replicate();
                $replicateRow->fkEenEpty = 6;
                $replicateRow->een_DateOfEngagement = date('Y-m-d H:i:s');
                $replicateRow->save();

                if ($replicateRow->save()) {
                    $oldHourRate = EmployeesWeekHourRates::where('fkEwhEen', $existEngagement->pkEen)->whereNull('ewh_EndDate')->first();
                    if (!empty($oldHourRate)) {
                        EmployeesWeekHourRates::addHourlyRate($replicateRow->pkEen, $oldHourRate->ewh_WeeklyHoursRate, '');
                    }

                    $ee = new HomeRoomTeacher;
                    $ee->fkHrtEen = $replicateRow->pkEen;
                    $ee->fkHrtCcs = $pkCcs;
                    $ee->save();
                }

                //old engagement, homeroomteacher, EmployeesWeekHourRates table date end/deactivate
                $oldTeacher = HomeRoomTeacher::find($hrtId);
                $oldEen = $oldTeacher->fkHrtEen;
                $oldTeacher->delete();

                EmployeesEngagement::where('pkEen', $oldEen)->update(['een_DateOfFinishEngagement' => date('Y-m-d H:i:s')]);

                //old hourly rate end date update
                $oldEng = EmployeesWeekHourRates::where('fkEwhEen', $oldEen)->orderBy('pkEwh', 'DESC')->first();
                if (!empty($oldEng)) {
                    $oldEng->ewh_EndDate = date('Y-m-d H:i:s');
                    $oldEng->save();
                }
                //old teacher end

                //start send mail to new homeroomteacher
                $emp = Employee::find($newEngId);
                $school = School::where('pkSch', $existEngagement->fkEenSch)
                    ->first();

                $dataemp = ['email' => $emp->email, 'name' => $emp->emp_EmployeeName . " " . $emp->emp_EmployeeSurname, 'school' => $school->sch_SchoolName_en, 'subject' => 'New Home Room Teacher Assign'];

                MailHelper::sendHomeroomTeacherAssign($dataemp);
                //end send mail to new homeroomteacher

                $response['status'] = true;
                $response['message'] = trans('message.msg_homeroomteacher_updated');
            } else {
                $response['status'] = false;
                $response['message'] = trans('message.msg_something_wrong');
            }
        } else {
            $response['status'] = true;
            $response['message'] = trans('message.msg_homeroomteacher_updated');
        }

        return response()->json($response);
    }

    /**
     * Fetch Subclass Listing data
     *
     * @param \Illuminate\Http\Request $request
     * @return JSON
     */
    public function subclass_list(Request $request)
    {
        $data = $request->all();

        $perpage = !empty($data['length']) ? (int) $data['length'] : 10;

        $filter = isset($data['search']) && is_string($data['search']) ? $data['search'] : '';

        $sort_type = isset($data['order'][0]['dir']) && is_string($data['order'][0]['dir']) ? $data['order'][0]['dir'] : '';

        $sort_col = $data['order'][0]['column'];

        $sort_field = $data['columns'][$sort_col]['data'];

        $pkClr = $data['pkClr'];

        \DB::statement("SET SQL_MODE=''");
        $Subclasses = ClassStudentsAllocation::with([
            'classCreationCourses' => function ($q) use ($filter) {
                $q->select('pkCrs', 'crs_CourseName_' . $this->current_language . ' AS crs_CourseName');
            },
            'subClassGroup' => function ($q) {
                $q->select('pkScg', 'scg_Name_' . $this->current_language . ' AS scg_Name');
            },
        ])
            ->whereHas('classSemester', function ($q) use ($pkClr) {
                $q->where('fkSemCcs', $pkClr);
            })
            ->whereNotNull('fkCsaScg');

        if ($filter) {
            $Subclasses = $Subclasses
                ->whereHas('classCreationCourses', function ($query) use ($filter) {
                    $query->where('crs_CourseName_' . $this->current_language, 'LIKE', '%' . $filter . '%');
                })
                ->orWhereHas('subClassGroup', function ($query) use ($filter) {
                    $query->where('scg_Name_' . $this->current_language, 'LIKE', '%' . $filter . '%');
                });
        }

        $Subclasses = $Subclasses->groupBy(['fkCsaEmc', 'fkCsaScg']);

        if ($sort_col != 0) {
            $Subclasses = $Subclasses->orderBy($sort_field, $sort_type);
        }

        $total_Subclassess = $Subclasses->count();

        $offset = $data['start'];

        $counter = $offset;
        $Subclassesdata = [];
        $Subclassess = $Subclasses->offset($offset)
            ->limit($perpage)
            ->get()
            ->toArray();

        foreach ($Subclassess as $key => $value) {
            $value['index'] = $counter + 1;
            $Subclassesdata[$counter] = $value;
            $counter++;
        }

        $price = array_column($Subclassesdata, 'index');

        if ($sort_col == 0) {
            if ($sort_type == 'desc') {
                array_multisort($price, SORT_DESC, $Subclassesdata);
            } else {
                array_multisort($price, SORT_ASC, $Subclassesdata);
            }
        }
        $result = array(
            "draw" => $data['draw'],
            "recordsTotal" => $total_Subclassess,
            "recordsFiltered" => $total_Subclassess,
            'data' => $Subclassesdata,
        );

        return response()->json($result);

    }

    /**
     * Used for sub class Students Listing
     * @param  Request $request
     * @return JSON $result
     */
    public function subclass_students_list(Request $request)
    {
        $data = $request->all();

        $perpage = !empty($data['length']) ? (int) $data['length'] : 10;

        $filter = isset($data['search']) && is_string($data['search']) ? $data['search'] : '';

        $sort_type = isset($data['order'][0]['dir']) && is_string($data['order'][0]['dir']) ? $data['order'][0]['dir'] : '';

        $sort_col = $data['order'][0]['column'];

        $sort_field = $data['columns'][$sort_col]['data'];

        $pkCcs = $data['pkCcs'];

        $stuEnrollsIds = ClassStudentsSemester::where('fkSemCcs', $pkCcs)->pluck('fkSemSen')
            ->all();

        $mainSchool = $this
            ->logged_user->sid;

        $classStudent = EnrollStudent::whereHas('student', function ($q) use ($filter) {
            if (!empty($filter)) {
                $q->where(function ($query) use ($filter) {
                    $query->where('stu_StudentName', 'LIKE', '%' . $filter . '%')->orWhere('stu_StudentSurname', 'LIKE', '%' . $filter . '%')->orWhere('stu_StudentID', 'LIKE', '%' . $filter . '%')->orWhere('stu_TempCitizenId', 'LIKE', '%' . $filter . '%');
                });
            }
        })->with(['student', 'grade' => function ($q) {
            $q->select('pkGra', 'gra_GradeNameRoman', 'gra_GradeNumeric');
        }
            , 'educationProgram' => function ($q) {
                $q->select('pkEdp', 'edp_Name_' . $this->current_language . ' as edp_Name');
            }
            , 'educationPlan' => function ($q) {
                $q->select('pkEpl', 'epl_EducationPlanName_' . $this->current_language . ' as epl_EducationPlanName');
            },
        ])
            ->whereIn('pkSte', $stuEnrollsIds);

        if ($sort_col != 0) {
            $classStudent = $classStudent->orderBy($sort_field, $sort_type);
        }

        $total_mainBooks = $classStudent->count();

        $offset = $data['start'];

        $counter = $offset;
        $classStudentdata = [];
        $classStudents = $classStudent->offset($offset)->limit($perpage)->get()
            ->toArray();

        foreach ($classStudents as $key => $value) {
            $value['index'] = $counter + 1;
            $classStudentdata[$counter] = $value;
            $counter++;
        }

        $price = array_column($classStudentdata, 'index');

        if ($sort_col == 0) {
            if ($sort_type == 'desc') {
                array_multisort($price, SORT_DESC, $classStudentdata);
            } else {
                array_multisort($price, SORT_ASC, $classStudentdata);
            }
        }
        $result = array(
            "draw" => $data['draw'],
            "recordsTotal" => $total_mainBooks,
            "recordsFiltered" => $total_mainBooks,
            'data' => $classStudentdata,
        );

        return response()->json($result);
    }
}
