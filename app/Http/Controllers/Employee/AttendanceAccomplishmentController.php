<?php

namespace App\Http\Controllers\Employee;

use App\Http\Controllers\Controller;
use App\Http\Services\AttendanceAccomplishmentService;
use App\Http\Services\ClassWorkingWeekService;
use App\Models\AttendanceWay;
use App\Models\DailyBook;
use App\Models\DailyBookLecture;
use App\Models\DailyStudentAttendance;
use App\Models\DailySyllabusAccomplishment;
use App\Models\EducationWay;
use Illuminate\Http\Request;
use Session;
use UtilHelper;

class AttendanceAccomplishmentController extends Controller
{

    public function index()
    {
        return view('employee.attendanceAccomplishment.index');
    }

    public function create(Request $request)
    {
        $classData = self::selectedClassData();

        $classDetail = $classData['current_lecture_data'];
        $language = $this->current_language;

        if (!empty($classDetail['classId']) && !empty($classData)) {

            $components = (new AttendanceAccomplishmentService())->createClassComponents($classData, $language);
            $classDetailJSON = json_encode($classData);
            $attendanceWay = AttendanceWay::CurrentAttendanceWay()->first();
            $default_attendance_way = $attendanceWay->pkAw;
            $educationWay = EducationWay::CurrentEducationWay()->first();
            $default_education_way = $educationWay->pkEw;

            $pagedata = compact(
                'components',
                'classDetailJSON',
                'attendanceWay',
                'educationWay',
                'default_attendance_way',
                'default_education_way'
            );

            return view('employee.attendanceAccomplishment.create')->with($pagedata);
        } else {
            return redirect('employee/dashboard');
        }
    }

    public function store(Request $request)
    {
        $input = $request->all();
        $hourinput = 'hour' . $input['dbl_LectureNumber'];

        if(!empty($input['pkDbl']))
        {
            $db_lecture = DailyBookLecture::find($input['pkDbl']);
            $dbl_exist = DailyBookLecture::where('fkDblDbk', $db_lecture->fkDblDbk)
                ->where('dbl_LectureNumber', $input['dbl_LectureNumber'])
                ->where('pkDbl','!=',$input['pkDbl'])
                ->count();

            if ($dbl_exist) {
                $response['status'] = false;
                $response['message'] = trans('message.msg_lecture_is_already_reserved');

                return response()->json($response);
            }

            $db_lecture->fkDblEw = $input['fkDblEw'];
            $db_lecture->fkDblAw = $input['fkDblAw'];
            $db_lecture->dbl_LectureNumber = $input['dbl_LectureNumber'];
            $db_lecture->dbl_Notes = $input['dbl_Notes'];
            $db_lecture->save();

            foreach ($input['lectures'] as $value) {
                $unitnumber = $input['sya_CourseUnitNumber_'.$value];
                $coursecontent = $input['sya_CourseContent_'.$value];
                $dbsa = DailySyllabusAccomplishment::find($value);
                $dbsa->sya_CourseUnitNumber = $unitnumber;
                $dbsa->sya_CourseContent = $coursecontent;
                $dbsa->save();
            }

            foreach ($input[$hourinput] as $val) {
                $dsa = DailyStudentAttendance::find($val);
                $dsa->sae_LectureAttendance = 'P';
                $dsa->save();
            }

            if(!empty($input['absent_students'])){
                foreach ($input['absent_students'] as $val) {
                    $dsa = DailyStudentAttendance::find($val);
                    $dsa->sae_LectureAttendance = 'A';
                    $dsa->save();
                }
            }

            $response['status'] = true;
            $response['message'] = trans('message.msg_lecture_update_sucess');
        }
        else
        {
            $pkDbk = 0;
            $selected_grades_students = [];
            if (isset($input[$hourinput]) && !empty($input[$hourinput])) {
                foreach ($input[$hourinput] as $val) {
                    $stu_data = explode("-", $val);
                    if (!in_array($stu_data[2], $selected_grades_students)) {
                        $selected_grades_students[] = $stu_data[2];
                    }
                }
            }

            if (!empty($input['dbk_Date']) && !empty($input['dbl_LectureNumber']) && !empty($input['classDetail']) && count($selected_grades_students)) {
                $classData = json_decode($input['classDetail'], true);
                extract($classData);

                $classId = $current_lecture_data['classId'];
                $villageschoolId = $current_lecture_data['villageschoolId'];
                $courseId = $current_lecture_data['courseId'];
                $subclassId = $current_lecture_data['subclassId'];
                $coursegroupId = $current_lecture_data['coursegroupId'];

                $DailyBookExist = DailyBook::where('fkDbkShi', $current_shift)
                    ->where('fkDbkCcs', $classId)
                    ->where('dbk_Date', UtilHelper::sqlDate($input['dbk_Date'], 2))
                    ->first();

                if (!empty($DailyBookExist)) {
                    $pkDbk = $DailyBookExist->pkDbk;
                    $DailyBookLectureExist = DailyBookLecture::where('fkDblDbk', $pkDbk)
                        ->where('dbl_LectureNumber', $input['dbl_LectureNumber'])
                        ->count();

                    if ($DailyBookLectureExist) {
                        $response['status'] = false;
                        $response['message'] = trans('message.msg_lecture_is_already_reserved');

                        return response()->json($response);
                    }
                } else {
                    $dailyBookAry['fkDbkSye'] = $current_school_year;
                    $dailyBookAry['fkDbkVsc'] = ($villageschoolId > 0) ? $villageschoolId : null;
                    $dailyBookAry['fkDbkShi'] = $current_shift;
                    $dailyBookAry['fkDbkCcs'] = $classId;
                    $dailyBookAry['dbk_Date'] = UtilHelper::sqlDate($input['dbk_Date'], 2);
                    $pkDbk = DailyBook::insertGetId($dailyBookAry);
                }

                if (!empty($pkDbk)) {
                    $dailyBookLectureAry['fkDblDbk'] = $pkDbk;
                    $dailyBookLectureAry['fkDblEw'] = $input['fkDblEw'];
                    $dailyBookLectureAry['fkDblAw'] = $input['fkDblAw'];
                    $dailyBookLectureAry['dbl_LectureNumber'] = $input['dbl_LectureNumber'];
                    $dailyBookLectureAry['dbl_Notes'] = $input['dbl_Notes'];
                    $pkDbl = DailyBookLecture::insertGetId($dailyBookLectureAry);

                    if (!empty($pkDbl)) {
                        $StudentAttendanceData = [];
                        $syllabusData = [];
                        foreach ($input['grades'] as $key => $value) {
                            if (isset($input['grades'][$key]) && in_array($input['grades'][$key], $selected_grades_students)) {

                                $grade = $value;
                                $course_id = $input['courses_' . $key];
                                $sydata['fkSyaDbl'] = $pkDbl;
                                $sydata['fkSyaCrs'] = $course_id;
                                $sydata['fkSyaScg'] = ($subclassId > 0) ? $subclassId : null;
                                $sydata['fkSyaCga'] = ($coursegroupId > 0) ? $coursegroupId : null;
                                $sydata['fkSyaEen'] = $current_employee;
                                $sydata['fkSyaGra'] = $grade;
                                $sydata['fkSyaCla'] = $input['class'][$key];
                                $sydata['sya_LectureOrderNumber'] = $input['sya_LectureOrderNumber_' . $grade];
                                $sydata['sya_CourseUnitNumber'] = $input['sya_CourseUnitNumber_' . $grade];
                                $sydata['sya_CourseContent'] = $input['sya_CourseContent_' . $grade];

                                $pkSya = DailySyllabusAccomplishment::insertGetId($sydata);
                                $syllabusData[$grade] = $pkSya;
                            }
                        }

                        foreach($syllabusData as $k => $v){
                            if (isset($syllabusData[$k])) {
                                $pkSya = $v;
                                //this is for present students
                                for ($j = 0; $j < count($input[$hourinput]); $j++) {
                                    $studentVal = $input[$hourinput][$j];
                                    $studentDetail = explode("-", $studentVal);

                                    if ($k == $studentDetail[2]) {
                                        $StudentAttendance['fkSaeSya'] = $pkSya;
                                        $StudentAttendance['fkSaeSem'] = $studentDetail[0];
                                        $StudentAttendance['fkSaeCsa'] = $studentDetail[1];
                                        $StudentAttendance['sae_LectureAttendance'] = 'P';
                                        $StudentAttendanceData[] = $StudentAttendance;
                                    }
                                }

                                if (isset($input['absent_students']) && !empty($input['absent_students'])) {
                                    //this is for absent students
                                    for ($i = 0; $i < count($input['absent_students']); $i++) {
                                        $studentVal = $input['absent_students'][$i];
                                        $studentDetail = explode("-", $studentVal);

                                        if ($k == $studentDetail[2]) {
                                            $StudentAttendance['fkSaeSya'] = $pkSya;
                                            $StudentAttendance['fkSaeSem'] = $studentDetail[0];
                                            $StudentAttendance['fkSaeCsa'] = $studentDetail[1];
                                            $StudentAttendance['sae_LectureAttendance'] = 'A';
                                            $StudentAttendanceData[] = $StudentAttendance;
                                        }
                                    }
                                }
                            }
                        }

                        if (count($StudentAttendanceData)) {
                            DailyStudentAttendance::insert($StudentAttendanceData);
                        }
                    }

                    $response['status'] = true;
                    $response['pkDbl'] = $pkDbl;
                    $response['message'] = trans('message.msg_student_attendace_success');
                } else {
                    $response['status'] = false;
                    $response['message'] = trans('message.msg_something_wrong');
                }
            }
        }

        return response()->json($response);
    }

    public function show($id)
    {
        if (!empty($id)) {
            $language = $this->current_language;
            $dailyBookLectureData = (new AttendanceAccomplishmentService())->getTeacherClassDetails($id, $language);
            extract($dailyBookLectureData);

            $lecture_details = $class_details->pluck('lecture_details')->toArray();
            $attendance_details = $class_details->pluck('attendance_details')->toArray();

            $date = UtilHelper::sqlDate($basic_details['date'],2);
            $week_params = ['language'=>$language,
                            'shift_id'=>$basic_details['shift_id'],
                            'class_sem_id'=>$basic_details['class_sem_id'],
                            'date'=>$date];
            $working_weeks_details = (new ClassWorkingWeekService())->getOnDutyStudents($week_params);

            return view('employee.attendanceAccomplishment.view')
                ->with(compact('lecture_details','attendance_details','basic_details','working_weeks_details'));
        }
    }

    public function edit($id)
    {
        $language = $this->current_language;
        $dailyBookLectureData = (new AttendanceAccomplishmentService())->getTeacherClassDetails($id, $language);
        extract($dailyBookLectureData);

        $lecture_details = $class_details->pluck('lecture_details')->toArray();
        $attendance_details = $class_details->pluck('attendance_details')->toArray();
        
        if(!empty($id)) {
            return view('employee.attendanceAccomplishment.edit')->with(compact('id','lecture_details','attendance_details','basic_details'));
        } else {
            return redirect('employee/dashboard');
        }
    }

    public function destroy($id)
    {
        if (!empty($id)) {
            $status = DailyBookLecture::find($id)->delete();
            if ($status) {
                $response['status'] = true;
                $response['message'] = trans('message.msg_lecture_is_deleted_successfully');
            } else {
                $response['status'] = false;
                $response['message'] = trans('message.msg_something_wrong');
            }

            return response()->json($response);
        }
    }

    /**
     * Fetch previous unit history
     *
     * @param \Illuminate\Http\Request $request
     * @return void
     */
    public function previous_unit_history_list(Request $request)
    {
        $filter = $request->search;
        $course = $request->courseId;
        $classLabelId = $request->classLabel;
        $grade = $request->gradeId;
        $courseData = self::selectedClassData();

        $lectures = (new AttendanceAccomplishmentService())->fetchPreviousSyllabusData($courseData, $course, $grade, $classLabelId, $filter);
        if ($lectures['count'] > 0) {
            $data = array('status' => true, 'count' => $lectures['count'], 'data' => $lectures['row']);
        } else {
            $row = '<div class="card w-100"><div class="card-body"><h6 class="text-center">' . trans('general.gn_no_history_found') . '</h6></div></div>';
            $data = array('status' => true, 'count' => count($lectures), 'data' => $row);
        }

        return response()->json($data);
    }

    public function selectedClassData()
    {
        $data['current_lecture_data'] = Session::get('curr_course_class');
        $data['current_employee'] = Session::get('curr_emp_eid');
        $data['current_school_year'] = Session::get('curr_school_year');
        $data['current_shift'] = Session::get('curr_shift');
        $data['current_sem'] = Session::get('curr_school_sem');
        $data['current_school'] = Session::get('curr_emp_sid');

        return $data;
    }
}
