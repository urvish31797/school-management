<?php
/**
 * SubAdminController
 *
 * @package    Laravel
 * @subpackage Controller
 * @since      1.0
 */

namespace App\Http\Controllers\Employee;

use App\Helpers\FrontHelper;
use App\Helpers\MailHelper;
use App\Helpers\UtilHelper;
use App\Http\Controllers\Controller;
use App\Http\Traits\CommonTrait;
use App\Models\Admin;
use App\Models\Citizenship;
use App\Models\Employee;
use App\Models\EmployeeDesignation;
use App\Models\EmployeesEngagement;
use App\Models\EmployeesWeekHourRates;
use App\Models\EmployeeType;
use App\Models\EngagementType;
use App\Models\Nationality;
use App\Models\Religion;
use App\Models\School;
use App\Models\University;
use App\Http\Traits\UserTrait;
use Illuminate\Http\Request;

class SubAdminController extends Controller
{
    use CommonTrait,UserTrait;

    /**
     * SubAdmin Page
     * @param Request $request
     * @return view - employee.subAdmins.subAdmins
     */
    public function index(Request $request)
    {
        return view('employee.subAdmins.subAdmins');
    }

    /**
     * Add Subadmin page
     * @param Request $request
     * @return view - employee.subAdmins.addSubAdmin
     */
    public function create(Request $request)
    {
        $EngagementTypes = EngagementType::select('pkEty', 'ety_EngagementTypeName_' . $this->current_language . ' as ety_EngagementTypeName')
            ->get();
        $not_show_empType = ['Principal', 'Teacher', 'SchoolCoordinator', 'HomeroomTeacher'];
        $employeeType = EmployeeType::select('pkEpty', 'epty_Name', 'epty_subCatName', 'epty_ParentId')->whereNotIn('epty_Name', $not_show_empType)->get();

        $Municipalities = $this->getMuncipalityDropdownData($this->current_language);
        $PostalCodes = $this->getPostalCodeDropdownData($this->current_language);

        $Citizenships = Citizenship::select('pkCtz', 'ctz_CitizenshipName_' . $this->current_language . ' as ctz_CitizenshipName')
            ->get();
        $Nationalities = Nationality::select('pkNat', 'nat_NationalityName_' . $this->current_language . ' as nat_NationalityName')
            ->get();
        $Religions = Religion::select('pkRel', 'rel_ReligionName_' . $this->current_language . ' as rel_ReligionName')
            ->get();
        $Universities = University::select('pkUni', 'uni_UniversityName_' . $this->current_language . ' as uni_UniversityName')
            ->get();
        $EmployeeDesignations = EmployeeDesignation::select('pkEde', 'ede_EmployeeDesignationName_' . $this->current_language . ' as ede_EmployeeDesignationName')
            ->get();

        $mainSchool = $this->logged_user->sid;

        $datas = ['Nationalities' => $Nationalities,
            'Citizenships' => $Citizenships,
            'Religions' => $Religions,
            'Universities' => $Universities,
            'EmployeeDesignations' => $EmployeeDesignations,
            'employeeType' => $employeeType,
            'EngagementTypes' => $EngagementTypes,
            'mainSchool' => $mainSchool,
            'Municipalities' => $Municipalities,
            'PostalCodes' => $PostalCodes];

        if (request()
            ->ajax()) {
            return \View::make('employee.subAdmins.addSubAdmin')
                ->with($datas)->renderSections();
        }
        return view('employee.subAdmins.addSubAdmin', $datas);
    }

    /**
     * Save subadmin
     * @param Request $request
     * @return JSON $response
     */
    public function store(Request $request)
    {
        $response = [];
        $input = $request->all();
        $image = $request->file('upload_profile');
        $pkSch = $input['sid'];

        if (isset($input['id']) && !empty($input['id'])) {
            $id = $input['id'];

            // $emailExist = Employee::where('email', '=', $input['email'])->where('id', '!=', $id)->get();
            // $govIdExist = Employee::where('emp_EmployeeID', '=', $input['emp_EmployeeID'])->where('id', '!=', $id)->get();
            // $tempIdExist = Employee::where('emp_TempCitizenId', '=', $input['emp_TempCitizenId'])->where('emp_TempCitizenId', '!=', '')
            //     ->where('id', '!=', $id)->get();
            // $emailAdmExist = Admin::where('email', '=', $input['email'])->get();

            // $emailExistCount = $emailExist->count();
            // $emailAdmExistCount = $emailAdmExist->count();
            // $tempIdExistCount = $tempIdExist->count();
            // $govIdExistCount = $govIdExist->count();

            // if ($emailExistCount != 0 || $emailAdmExistCount != 0) {
            //     $response['status'] = false;
            //     $response['message'] = trans('message.msg_email_exist');
            //     return response()
            //         ->json($response);
            // }

            // if ($govIdExistCount != 0) {
            //     $response['status'] = false;
            //     $response['message'] = trans('message.msg_employee_id_exist');
            //     return response()
            //         ->json($response);
            // }

            // if ($tempIdExistCount != 0) {
            //     $response['status'] = false;
            //     $response['message'] = trans('message.msg_temp_citizen_id_exist');
            //     return response()
            //         ->json($response);
            // }

            $check_user_params = ['type'=>'Employee',
                                 'id'=>$id,
                                 'email'=>$input['email'],
                                 'gov_id'=>$input['emp_EmployeeID'],
                                 'temp_id'=>$input['emp_TempCitizenId']];
            $check_user_status = $this->checkUserEmailorUidExist($check_user_params);

            if($check_user_status['email_exist']){
                $response['message'] = trans('message.msg_email_exist');
                return response()->json($response,400);
            }

            if($check_user_status['gov_id_exist']){
                $response['message'] = trans('message.msg_employee_id_exist');
                return response()->json($response,400);
            }

            if($check_user_status['temp_id_exist']){
                $response['message'] = trans('message.msg_temp_citizen_id_exist');
                return response()->json($response,400);
            }

            $user = Employee::find($id);
            $user->emp_EmployeeName = $input['emp_EmployeeName'];
            $user->emp_EmployeeSurname = $input['emp_EmployeeSurname'];
            $user->emp_EmployeeID = $input['emp_EmployeeID'];
            $user->emp_TempCitizenId = $input['emp_TempCitizenId'];
            $user->emp_EmployeeGender = $input['emp_EmployeeGender'];
            $user->emp_DateOfBirth = UtilHelper::sqlDate($input['emp_DateOfBirth']);
            $user->emp_PlaceOfBirth = $input['emp_PlaceOfBirth'];
            $user->emp_Address = $input['emp_Address'];
            $user->emp_PhoneNumber = $input['emp_PhoneNumber'];
            $user->emp_Status = $input['emp_Status'];
            $user->emp_Notes = $input['emp_Notes'];
            $user->fkEmpMun = $input['fkEmpMun'];
            $user->fkEmpPof = $input['fkEmpPof'];
            $user->fkEmpCny = $input['fkEmpCny'];
            $user->fkEmpNat = $input['fkEmpNat'];
            $user->fkEmpRel = $input['fkEmpRel'];
            $user->fkEmpCtz = $input['fkEmpCtz'];

            $empType = EmployeeType::select('pkEpty')->where('epty_Name', '=', 'SchoolSubAdmin')
                ->where('epty_ParentId', '=', null)
                ->first();
            $engType = EngagementType::select('pkEty')->where('ety_EngagementTypeName_en', '=', 'Full Time')
                ->first(); //Full time
            $SchoolData = School::select('sch_SchoolName_en')->where('pkSch', $pkSch)->first();

            if (!empty($image)) {
                $input['emp_PicturePath'] = time() . '.' . $image->getClientOriginalExtension();
                $destinationPath = storage_path('app/public/images/users');
                $image->move($destinationPath, $input['emp_PicturePath']);
                if (!empty($user->emp_PicturePath)) {
                    $filepath = storage_path('app/public/images/users') . $user->emp_PicturePath;
                    if (file_exists($filepath)) {
                        unlink($filepath);
                    }
                }
                $user->emp_PicturePath = $input['emp_PicturePath'];
            }

            if ($user->email != $input['email']) {
                $current_time = date("Y-m-d H:i:s");
                $verification_key = md5(FrontHelper::generatePassword(20));

                $reset_pass_token = base64_encode($input['email'] . '&&SchoolSubAdmin&&' . $current_time);
                $data = ['email' => $input['email'], 'name' => $input['emp_EmployeeName'] . " " . $input['emp_EmployeeSurname'], 'school' => $SchoolData->sch_SchoolName_en, 'verify_key' => $verification_key, 'reset_pass_link' => $reset_pass_token, 'subject' => 'New School Sub Admin Credentials'];

                MailHelper::sendNewSchoolSubAdminCredentials($data);

                $user->email = $input['email'];
                $user->email_verified_at = null;
                $user->email_verification_key = $verification_key;
            }

            $engData = [];

            $engData['fkEenEpty'] = $input['fkEenEpty'];
            $engData['fkEenEty'] = $input['fkEenEty'];
            $engData['een_Notes'] = $input['een_Notes'];

            $engData['een_DateOfEngagement'] = UtilHelper::sqlDate($input['start_date']);
            if (!empty($input['end_date'])) {
                $engData['een_DateOfFinishEngagement'] = UtilHelper::sqlDate($input['end_date']);
            } else {
                $engData['een_DateOfFinishEngagement'] = null;
            }

            $endfg = EmployeesEngagement::where('pkEen', $input['emp_engagment_id'])->update($engData);

            EmployeesWeekHourRates::addHourlyRate($input['emp_engagment_id'], $input['ewh_WeeklyHoursRate'], $input['ewh_Notes'], UtilHelper::sqlDate($input['start_date']));

            if ($user->save()) {
                $response['status'] = true;
                $response['message'] = trans('message.msg_sub_admin_update_success');
            } else {
                $response['status'] = false;
                $response['message'] = trans('message.msg_something_wrong');
            }
        } else {
            $temp_pass = FrontHelper::generatePassword(10);
            $verification_key = md5(FrontHelper::generatePassword(20));

            // $emailExist = Employee::where('email', '=', $input['email'])->get();
            // $govIdExist = Employee::where('emp_EmployeeID', '=', $input['emp_EmployeeID'])->get();
            // $tempIdExist = Employee::where('emp_TempCitizenId', '=', $input['emp_TempCitizenId'])->where('emp_TempCitizenId', '!=', '')
            //     ->get();
            // $emailAdmExist = Admin::where('email', '=', $input['email'])->get();

            // $emailExistCount = $emailExist->count();
            // $emailAdmExistCount = $emailAdmExist->count();
            // $tempIdExistCount = $tempIdExist->count();
            // $govIdExistCount = $govIdExist->count();

            // if ($emailExistCount != 0 || $emailAdmExistCount != 0) {
            //     $response['status'] = false;
            //     $response['message'] = trans('message.msg_email_exist');
            //     return response()
            //         ->json($response);
            // }

            // if ($govIdExistCount != 0) {
            //     $response['status'] = false;
            //     $response['message'] = trans('message.msg_employee_id_exist');
            //     return response()
            //         ->json($response);
            // }

            // if ($tempIdExistCount != 0) {
            //     $response['status'] = false;
            //     $response['message'] = trans('message.msg_temp_citizen_id_exist');
            //     return response()
            //         ->json($response);
            // }

            $check_user_params = ['type'=>'Employee',
                                 'email'=>$input['email'],
                                 'gov_id'=>$input['emp_EmployeeID'],
                                 'temp_id'=>$input['emp_TempCitizenId']];
            $check_user_status = $this->checkUserEmailorUidExist($check_user_params);

            if($check_user_status['email_exist']){
                $response['status'] = false;
                $response['message'] = trans('message.msg_email_exist');
                return response()->json($response);
            }

            if($check_user_status['gov_id_exist']){
                $response['status'] = false;
                $response['message'] = trans('message.msg_employee_id_exist');
                return response()->json($response);
            }

            if($check_user_status['temp_id_exist']){
                $response['status'] = false;
                $response['message'] = trans('message.msg_temp_citizen_id_exist');
                return response()->json($response);
            }

            $user = new Employee;
            $user->email = $input['email'];
            $user->emp_EmployeeName = $input['emp_EmployeeName'];
            $user->emp_EmployeeSurname = $input['emp_EmployeeSurname'];
            $user->emp_EmployeeID = $input['emp_EmployeeID'];
            $user->emp_TempCitizenId = $input['emp_TempCitizenId'];
            $user->emp_EmployeeGender = $input['emp_EmployeeGender'];
            $user->emp_DateOfBirth = UtilHelper::sqlDate($input['emp_DateOfBirth']);
            $user->emp_PlaceOfBirth = $input['emp_PlaceOfBirth'];
            $user->emp_Address = $input['emp_Address'];
            $user->emp_PhoneNumber = $input['emp_PhoneNumber'];
            $user->emp_Notes = $input['emp_Notes'];
            $user->fkEmpMun = $input['fkEmpMun'];
            $user->fkEmpPof = $input['fkEmpPof'];
            $user->fkEmpCny = $input['fkEmpCny'];
            $user->fkEmpNat = $input['fkEmpNat'];
            $user->fkEmpRel = $input['fkEmpRel'];
            $user->fkEmpCtz = $input['fkEmpCtz'];
            $user->email_verification_key = $verification_key;

            if (!empty($image)) {
                $input['emp_PicturePath'] = time() . '.' . $image->getClientOriginalExtension();
                $destinationPath = storage_path('app/public/images/users');
                $image->move($destinationPath, $input['emp_PicturePath']);
                $user->emp_PicturePath = $input['emp_PicturePath'];
            }

            $empType = EmployeeType::select('pkEpty')->where('epty_Name', '=', 'SchoolSubAdmin')
                ->where('epty_ParentId', '=', null)
                ->first();
            $engType = EngagementType::select('pkEty')->where('ety_EngagementTypeName_en', '=', 'Full Time')
                ->first(); //Full time
            $SchoolData = School::select('sch_SchoolName_en')->where('pkSch', $pkSch)->first();

            if ($user->save()) {
                $ee = new EmployeesEngagement;
                $ee->fkEenSch = $pkSch;
                $ee->fkEenEmp = $user->id;
                $ee->fkEenEty = !empty($input['fkEenEty']) ? $input['fkEenEty'] : $engType->pkEty;
                $ee->fkEenEpty = !empty($input['fkEenEpty']) ? $input['fkEenEpty'] : $empType->pkEpty;
                $ee->een_DateOfEngagement = UtilHelper::sqlDate($input['start_date']);
                if (isset($input['end_date']) && !empty($input['end_date'])) {
                    $ee->een_DateOfFinishEngagement = UtilHelper::sqlDate($input['end_date']);
                }
                $ee->een_Notes = !empty($input['een_Notes']) ? $input['een_Notes'] : '';
                $ee->save();

                if ($ee->save()) {
                    EmployeesWeekHourRates::addHourlyRate($ee->pkEen, $input['ewh_WeeklyHoursRate'], $input['ewh_Notes'], UtilHelper::sqlDate($input['start_date']));
                }

                $current_time = date("Y-m-d H:i:s");
                $reset_pass_token = base64_encode($input['email'] . '&&SchoolSubAdmin&&' . $current_time);
                $data = ['email' => $input['email'], 'name' => $input['emp_EmployeeName'] . " " . $input['emp_EmployeeSurname'], 'school' => $SchoolData->sch_SchoolName_en, 'verify_key' => $verification_key, 'reset_pass_link' => $reset_pass_token, 'subject' => 'New School Sub Admin Credentials'];

                MailHelper::sendNewSchoolSubAdminCredentials($data);

                $response['status'] = true;
                $response['message'] = trans('message.msg_sub_admin_add_success');

            } else {
                $response['status'] = false;
                $response['message'] = trans('message.msg_something_wrong');
            }
        }

        return response()
            ->json($response);

    }

    /**
     * Edit Sub admin page
     * @param  int $id
     * @return view - employee.subAdmins.editSubAdmin
     */
    public function edit($id)
    {
        $response = [];

        $EngagementTypes = EngagementType::select('pkEty', 'ety_EngagementTypeName_' . $this->current_language . ' as ety_EngagementTypeName')
            ->get();
        $not_show_empType = ['Principal', 'Teacher', 'SchoolCoordinator', 'HomeroomTeacher'];
        $employeeType = EmployeeType::select('pkEpty', 'epty_Name', 'epty_subCatName', 'epty_ParentId')->whereNotIn('epty_Name', $not_show_empType)->get();

        $mdata = Employee::with([
            'EmployeesEngagement.employeeType',
            'EmployeesEngagement.getLatestHourRates',
        ])->whereHas('EmployeesEngagement', function ($q1) use ($id) {
            $q1->whereHas('employeeType', function ($q2) use ($id) {
                $q2->where('epty_Name', 'SchoolSubAdmin');
            })
                ->where('fkEenEmp', $id);
        })->first();

        $Municipalities = $this->getMuncipalityDropdownData($this->current_language);
        $PostalCodes = $this->getPostalCodeDropdownData($this->current_language);

        $Citizenships = Citizenship::select('pkCtz', 'ctz_CitizenshipName_' . $this->current_language . ' as ctz_CitizenshipName')
            ->get();
        $Nationalities = Nationality::select('pkNat', 'nat_NationalityName_' . $this->current_language . ' as nat_NationalityName')
            ->get();
        $Religions = Religion::select('pkRel', 'rel_ReligionName_' . $this->current_language . ' as rel_ReligionName')
            ->get();
        $Universities = University::select('pkUni', 'uni_UniversityName_' . $this->current_language . ' as uni_UniversityName')
            ->get();
        $EmployeeDesignations = EmployeeDesignation::select('pkEde', 'ede_EmployeeDesignationName_' . $this->current_language . ' as ede_EmployeeDesignationName')
            ->get();

        $mainSchool = $this->logged_user->sid;

        $datas = ['Nationalities' => $Nationalities,
            'Citizenships' => $Citizenships,
            'Religions' => $Religions,
            'Universities' => $Universities,
            'EmployeeDesignations' => $EmployeeDesignations,
            'mainSchool' => $mainSchool,
            'Municipalities' => $Municipalities,
            'PostalCodes' => $PostalCodes,
            'employeeType' => $employeeType,
            'EngagementTypes' => $EngagementTypes,
            'mdata' => $mdata];

        if (request()
            ->ajax()) {
            return \View::make('employee.subAdmins.editSubAdmin')
                ->with($datas)->renderSections();
        }
        return view('employee.subAdmins.editSubAdmin', $datas);
    }

    /**
     * View Sub admin page
     * @param  int $id
     * @return view - employee.subAdmins.viewSubAdmin
     */
    public function show($id)
    {
        $mdata = '';

        $mainSchool = $this
            ->logged_user->sid;

        $mdata = Employee::with(['country' => function ($query) {
            $query->select('pkCny', 'cny_CountryName_' . $this->current_language . ' as cny_CountryName');
        }
            , "employeesEngagement" => function ($q) use ($id, $mainSchool) {
                $q->where('fkEenEmp', $id)->where('fkEenSch', $mainSchool);
            }
            , "employeesEngagement.getLatestHourRates"
            , "employeesEngagement.getAllHourRates" => function ($q) {
                $q->orderBy('pkEwh', 'DESC');
            }
            , 'employeesEngagement.employeeType', 'employeesEngagement.engagementType' => function ($query) {
                $query->select('pkEty', 'ety_EngagementTypeName_' . $this->current_language . ' as ety_EngagementTypeName');
            }
        ])->where('id', $id)->first();

        if (request()
            ->ajax()) {
            return \View::make('employee.subAdmins.viewSubAdmin')
                ->with('mdata', $mdata)->renderSections();
        }
        return view('employee.subAdmins.viewSubAdmin', ['mdata' => $mdata]);
    }

    /**
     * Delete Sub admin
     * @param  Request $request
     * @return JSON $response
     */
    public function destroy($cid)
    {
        $response = [];

        if (empty($cid)) {
            $response['status'] = false;
        } else {
            Employee::where('id', $cid)->delete();
            EmployeesEngagement::where('fkEenEmp', $cid)->delete();
            $response['status'] = true;
            $response['message'] = trans('message.msg_admin_staff_delete_success');
        }

        return response()
            ->json($response);
    }

    /**
     * Fetch Subadmin Data
     * @param  Request $request
     * @return JSON $result
     */
    public function subadmins_list(Request $request)
    {
        $data = $request->all();

        $perpage = !empty($data['length']) ? (int) $data['length'] : 10;

        $filter = isset($data['search']) && is_string($data['search']) ? $data['search'] : '';

        $sort_type = isset($data['order'][0]['dir']) && is_string($data['order'][0]['dir']) ? $data['order'][0]['dir'] : '';
        $sort_col = $data['order'][0]['column'];

        $sort_field = $data['columns'][$sort_col]['data'];

        $mainSchool = $this
            ->logged_user->sid;

        $start_date = $data['start_date'];

        $end_date = $data['end_date'];

        $schoolAdmin = Employee::with('EmployeesEngagement.employeeType')->whereHas('EmployeesEngagement', function ($q1) use ($mainSchool, $start_date, $end_date) {
            $q1->whereHas('employeeType', function ($q2) use ($mainSchool) {
                $q2->where('epty_Name', 'SchoolSubAdmin');
            })
                ->where('fkEenSch', $mainSchool);

            if ($start_date && !$end_date) {
                $q1->whereDate('een_DateOfEngagement', '=', date('Y-m-d', strtotime($start_date)));
            }

            if ($end_date && !$start_date) {
                $q1->whereDate('een_DateOfFinishEngagement', '=', date('Y-m-d', strtotime($end_date)));
            }

            if ($start_date && $end_date) {
                $q1->whereDate('een_DateOfEngagement', '>=', date('Y-m-d', strtotime($start_date)))->whereDate('een_DateOfFinishEngagement', '<=', date('Y-m-d', strtotime($end_date)));
            }
        });

        if ($filter) {
            $schoolAdmin = $schoolAdmin->where(function ($query) use ($filter) {
                $query->where('emp_EmployeeID', 'LIKE', '%' . $filter . '%')->orWhere('email', 'LIKE', '%' . $filter . '%');
            });
        }

        $schoolAdminQuery = $schoolAdmin;

        if ($sort_col != 0) {
            $schoolAdminQuery = $schoolAdmin->orderBy($sort_field, $sort_type);
        }

        $total_admins = $schoolAdminQuery->count();

        $offset = $data['start'];

        $counter = $offset;
        $schoolAdmindata = [];
        $schoolAdmins = $schoolAdminQuery->offset($offset)->limit($perpage)->get()
            ->toArray();

        foreach ($schoolAdmins as $key => $value) {
            $value['index'] = $counter + 1;
            $value['end_date'] = '';
            $value['start_date'] = '';
            if (!empty($value['employees_engagement'][0]['een_DateOfEngagement'])) {
                $value['start_date'] = date('d/m/Y', strtotime($value['employees_engagement'][0]['een_DateOfEngagement']));
            }
            if (!empty($value['employees_engagement'][0]['een_DateOfFinishEngagement'])) {
                $value['end_date'] = date('d/m/Y', strtotime($value['employees_engagement'][0]['een_DateOfFinishEngagement']));
            }
            if ($value['emp_Status'] == 'Active') {
                $value['emp_Status'] = trans('general.gn_active');
            } else {
                $value['emp_Status'] = trans('general.gn_inactive');
            }

            $schoolAdmindata[$counter] = $value;
            $counter++;
        }

        $price = array_column($schoolAdmindata, 'index');

        if ($sort_col == 0) {
            if ($sort_type == 'desc') {
                array_multisort($price, SORT_DESC, $schoolAdmindata);
            } else {
                array_multisort($price, SORT_ASC, $schoolAdmindata);
            }
        }
        $schoolAdmindata = array_values($schoolAdmindata);
        $result = array(
            "draw" => $data['draw'],
            "recordsTotal" => $total_admins,
            "recordsFiltered" => $total_admins,
            "data" => $schoolAdmindata,
        );

        return response()->json($result);
    }
}
