<?php
/**
 * CourseGroupController
 *
 * @package    Laravel
 * @subpackage Controller
 * @since      1.0
 */

namespace App\Http\Controllers\Employee;

use App\Http\Controllers\Controller;
use App\Http\Services\CourseGroupService;
use App\Http\Traits\CommonTrait;
use App\Models\Classes;
use App\Models\ClassStudentsAllocation;
use App\Models\ClassTeachersCourseAllocation;
use App\Models\Course;
use App\Models\CourseGroupAllocation;
use App\Models\EducationPeriod;
use App\Models\EducationPlansForeignLanguage;
use App\Models\EducationPlansOptionalCourse;
use App\Models\Employee;
use App\Models\EmployeesEngagement;
use App\Models\EmployeeType;
use App\Models\EnrollStudent;
use App\Models\FacultativeCoursesGroup;
use App\Models\ForeignLanguageGroup;
use App\Models\GeneralPurposeGroup;
use App\Models\Grade;
use App\Models\OptionalCoursesGroup;
use App\Models\SchoolYear;
use App\Models\Student;
use Illuminate\Http\Request;

class CourseGroupController extends Controller
{
    use CommonTrait;

    /**
     * Course Group Listing Page
     * @param  Request $request
     * @return view - employee.courseGroup.courseGroups
     */
    public function index(Request $request)
    {
        /**
         * Used for Course Group listing page
         */
        $mainSchool = $this
            ->logged_user->sid;
        $searchSchYear = SchoolYear::select('pkSye', 'sye_NameNumeric', 'sye_DefaultYear', 'sye_NameCharacter_' . $this->current_language . ' AS sye_NameCharacter')->get()
            ->toArray();
        $educationPeriods = EducationPeriod::select('pkEdp', 'edp_EducationPeriodName_' . $this->current_language . ' as edp_EducationPeriodName')
            ->get();
        $currentYearRow = $this->getCurrentSemester($mainSchool);
        $currentSemesterId = $currentYearRow->fkSchEdp;
        $searchGrade = Grade::select('pkGra', 'gra_GradeNumeric')
            ->get()
            ->toArray();

        if (request()
            ->ajax()) {
            return \View::make('employee.courseGroup.courseGroups')
                ->with(['searchSchYear' => $searchSchYear,
                    'searchGrade' => $searchGrade,
                    'educationPeriods' => $educationPeriods,
                    'currentSemesterId' => $currentSemesterId])->renderSections();
        }
        return view('employee.courseGroup.courseGroups')
            ->with(['searchSchYear' => $searchSchYear,
                'searchGrade' => $searchGrade,
                'educationPeriods' => $educationPeriods,
                'currentSemesterId' => $currentSemesterId]);
    }

    /**
     * Course Group page
     * @param  Request $request
     * @return view - employee.courseGroup.courseGroup
     */
    public function create(Request $request)
    {
        $mainSchool = $this
            ->logged_user->sid;

        $schoolYears = SchoolYear::select('pkSye', 'sye_NameNumeric', 'sye_DefaultYear', 'sye_NameCharacter_' . $this->current_language . ' AS sye_NameCharacter')->get();
        $grades = Grade::select('pkGra', 'gra_GradeNumeric')
            ->get();
        $empType = EmployeeType::select('pkEpty')->where('epty_Name', '=', 'Teacher')
            ->get();
        $classes = Classes::select('pkCla', 'cla_ClassName_' . $this->current_language . ' as cla_ClassName')
            ->get();

        foreach ($empType as $key => $value) {
            $allowedTypes[] = $value->pkEpty;
        }

        $employees = EmployeesEngagement::with(['employee' => function ($q) { //Employee engagment id with employee
            $q->select('emp_EmployeeName', 'id', 'emp_EmployeeSurname');
        },
        ])
            ->where('fkEenSch', $mainSchool)->where('een_DateOfFinishEngagement', null)
            ->whereIn('fkEenEpty', $allowedTypes)->get();

        $semesters = EducationPeriod::select('pkEdp', 'edp_EducationPeriodName_' . $this->current_language . ' as edp_EducationPeriodName')
            ->get();

        $currentSemData = $this->getCurrentSemester($mainSchool);
        $currentSem = $currentSemData->fkSchEdp;

        if (request()
            ->ajax()) {
            return \View::make('employee.courseGroup.courseGroup')
                ->with(['mainSchool' => $mainSchool, 'schoolYears' => $schoolYears, 'classes' => $classes, 'grades' => $grades, 'employees' => $employees, 'semesters' => $semesters, 'currentSem' => $currentSem])->renderSections();
        }
        return view('employee.courseGroup.courseGroup')
            ->with(['mainSchool' => $mainSchool, 'schoolYears' => $schoolYears, 'classes' => $classes, 'grades' => $grades, 'employees' => $employees, 'semesters' => $semesters, 'currentSem' => $currentSem]);
    }

    /**
     * View Course Group
     * @param  Int $id
     * @return view - employee.courseGroup.viewCourseGroup
     */
    public function show($id)
    {
        $mdata = '';
        if (!empty($id)) {

            \DB::statement("SET SQL_MODE=''");
            $mdata = CourseGroupAllocation::with(['classStudents.classSemester.studentEnroll.grade' => function ($q) {
                $q->select('pkGra', 'gra_GradeNumeric');
            }
                , 'classStudents.classSemester.studentEnroll.student', 'classStudents.classSemester.studentEnroll.educationPlan' => function ($q) {
                    $q->select('pkEpl', 'epl_EducationPlanName_' . $this->current_language . ' as epl_EducationPlanName');
                }
                , 'classStudents.classSemester.studentEnroll.educationProgram' => function ($q) {
                    $q->select('pkEdp', 'edp_Name_' . $this->current_language . ' as edp_Name');
                }
                , 'classStudents.classCreationTeachers', 'GPG', 'OCG', 'FLG', 'FCG'])
                ->where('pkCga', $id)->first();

            $mainSchool = $this
                ->logged_user->sid;

            $schoolYears = SchoolYear::select('pkSye', 'sye_NameNumeric', 'sye_DefaultYear', 'sye_NameCharacter_' . $this->current_language . ' AS sye_NameCharacter')->get();
            $grades = Grade::select('pkGra', 'gra_GradeNumeric')
                ->get();
            $empType = EmployeeType::select('pkEpty')->where('epty_Name', '=', 'Teacher')
                ->get();
            $courses = Course::select('*', 'crs_CourseName_' . $this->current_language . ' as crs_CourseName')
                ->where('crs_CourseType', 'General')
                ->get();

            $OCG = OptionalCoursesGroup::select('pkOcg as id', 'ocg_Name_' . $this->current_language . ' as group_Name')
                ->get();

            $FCG = FacultativeCoursesGroup::select('pkFcg as id', 'fcg_Name_' . $this->current_language . ' as group_Name')
                ->get();

            $FLG = ForeignLanguageGroup::select('pkFon as id', 'fon_Name_' . $this->current_language . ' as group_Name')
                ->get();

            $GPG = GeneralPurposeGroup::select('pkGpg as id', 'gpg_Name_' . $this->current_language . ' as group_Name')
                ->get();

            foreach ($empType as $key => $value) {
                $allowedTypes[] = $value->pkEpty;
            }

            $employees = EmployeesEngagement::with(['employee' => function ($q) { //Employee engagment id with employee
                $q->select('emp_EmployeeName', 'id', 'emp_EmployeeSurname');
            },
            ])
                ->where('fkEenSch', $mainSchool)->where('een_DateOfFinishEngagement', null)
                ->whereIn('fkEenEpty', $allowedTypes)->get();

            return view('employee.courseGroup.viewCourseGroup')
                ->with(['mdata' => $mdata, 'schoolYears' => $schoolYears, 'grades' => $grades, 'employees' => $employees, 'courses' => $courses, 'mainSchool' => $mainSchool, 'OCG' => $OCG, 'FCG' => $FCG, 'FLG' => $FLG, 'GPG' => $GPG]);

        } else {
            return redirect($this
                    ->logged_user->utype . '/dashboard');
            }
        }

        /**
     * Save Course Group
     * @param Request $request
     * @return JSON $response
     */
        public function store(Request $request)
    {
        $input = $request->all();
        $response = [];

        $mainSchool = $this->logged_user->sid;

        $empType = EmployeeType::select('pkEpty')->where('epty_Name', '=', 'Teacher')->get();

        foreach ($empType as $key => $value) {
            $allowedTypes[] = $value->pkEpty;
        }

        $postData['fkCgaOcg'] = null;
        $postData['fkCgaFcg'] = null;
        $postData['fkCgaFlg'] = null;
        $postData['fkCgaGpg'] = null;

        $postData = [];

        $schoolYearData = $this->getCurrentYear();
        $school_year = $schoolYearData->pkSye;

        $group_order_exist_params = ['course_group_main' => $input['course_group_main'],
            'course_group_sub' => $input['course_group_sub'],
            'fkClrSye' => $school_year,
            'fkClrSch' => $mainSchool,
            'pkCga' => $input['pkCga'] ?? null,
            'semester_id' => $input['fkCgaEdu'],
        ];
        $exist_group_res = (new CourseGroupService())->checkCourseGroupExists($group_order_exist_params);

        if ($exist_group_res > 0) {
            $response['status'] = false;
            $response['message'] = trans('message.msg_course_group_already_exists');
            return response()->json($response);
        }

        switch ($input['course_group_main']) {
            case 'ocg':
                $postData['fkCgaOcg'] = $input['course_group_sub'];
                $col = 'fkCgaOcg';
                break;
            case 'fcg':
                $postData['fkCgaFcg'] = $input['course_group_sub'];
                $col = 'fkCgaFcg';
                break;
            case 'flg':
                $postData['fkCgaFlg'] = $input['course_group_sub'];
                $col = 'fkCgaFlg';
                break;
            case 'gpg':
                $postData['fkCgaGpg'] = $input['course_group_sub'];
                $col = 'fkCgaGpg';
                break;
            default:
                break;
        }

        $group_order_exist_params['student_ids'] = $input['sem_ids'];
        $group_order_exist_params['grade_ids'] = $input['fkClrGra'];
        $group_order_exist_params['course_order'] = $input['course_order'];
        $group_order_exist_params['column'] = $col;
        $group_order_exist_params['column_val'] = $input['course_group_sub'];
        $student_order_exists_res = (new CourseGroupService())->checkStudentOrderExists($group_order_exist_params);
        unset($group_order_exist_params);

        if ($student_order_exists_res) {
            $response['status'] = false;
            $response['message'] = trans('message.msg_course_order_number_exist_in_students');
            return response()->json($response);
        }

        if (isset($input['cg_hour']) && !empty($input['cg_hour'])) {
            $postData['cga_WeekHours'] = $input['cg_hour'];
        } else if (isset($input['clr_hrs'])) {
            $postData['cga_WeekHours'] = max($input['clr_hrs']);
        } else {
            $postData['cga_WeekHours'] = null;
        }

        $postData['cga_OrderNumber'] = $input['course_order'];
        $postData['fkCgaSch'] = $mainSchool;
        $postData['fkCgaSye'] = $input['fkCgaSye'];
        $postData['fkCgaViSch'] = $input['fkCgaViSch'];
        $postData['fkCgaEdu'] = $input['fkCgaEdu'];
        $postData['fkCgaCrs'] = $input['subject'];

        if (!isset($input['pkCga'])) {
            //Add Course Group
            $pkCga = CourseGroupAllocation::insertGetId($postData);
            CourseGroupAllocation::where('pkCga', $pkCga)->update(['cga_Uid' => strtoupper($input['course_group_main']) . $pkCga]);

            foreach ($input['stu_ids'] as $k => $v) {
                $stuId = ClassStudentsAllocation::insertGetId([
                    'fkCsaSem' => $input['sem_ids'][$k],
                    'fkCsaEmc' => $input['subject'],
                    'fkCsaCga' => $pkCga]);

                foreach ($input['teacher'] as $kt => $vt) {
                    $teacherId = ClassTeachersCourseAllocation::insertGetId(['fkCtcEeg' => $vt, 'fkCtcCcs' => $input['fkCtcCcs'][$k], 'Ctc_start' => now(), 'fkCtcCsa' => $stuId]);
                }
            }

            $response['status'] = true;
            $response['message'] = trans('message.msg_course_group_create_success');
        } else { //Edit Course Group
            $pkCga = $input['pkCga'];
            CourseGroupAllocation::where('pkCga', $pkCga)->update($postData);

            $oldStuAlloc = ClassStudentsAllocation::where('fkCsaCga', $pkCga)
                ->get()
                ->pluck('pkCsa')
                ->all();
            $teachAlloc = ClassTeachersCourseAllocation::whereIn('fkCtcCsa', $oldStuAlloc)
                ->get()
                ->pluck('Ctc_start', 'fkCtcEeg')
                ->all();

            ClassStudentsAllocation::where('fkCsaCga', $pkCga)->delete();

            ClassTeachersCourseAllocation::whereIn('fkCtcCsa', $oldStuAlloc)->update(['deleted_at' => now(), 'Ctc_end' => now()]);

            if (isset($input['stu_ids'])) {
                foreach ($input['stu_ids'] as $k => $v) {
                    $stuId = ClassStudentsAllocation::insertGetId([
                        'fkCsaSem' => $input['sem_ids'][$k],
                        'fkCsaEmc' => $input['subject'],
                        'fkCsaCga' => $pkCga]);
                    foreach ($input['teacher'] as $kt => $vt) {
                        if (isset($teachAlloc[$vt])) {
                            $Ctc_start = $teachAlloc[$vt];
                        } else {
                            $Ctc_start = now();
                        }
                        $teacherId = ClassTeachersCourseAllocation::insertGetId(['fkCtcEeg' => $vt, 'fkCtcCcs' => $input['fkCtcCcs'][$k], 'Ctc_start' => $Ctc_start, 'fkCtcCsa' => $stuId]);
                    }
                }
            }

            $response['status'] = true;
            $response['message'] = trans('message.msg_course_group_update_success');
        }

        return response()
            ->json($response);
    }

    /**
     * Edit Course Group
     * @param  Int $id
     * @return view - employee.courseGroup.courseGroup
     */
    public function edit($id)
    {
        $row = CourseGroupAllocation::find($id);
        if (!empty($row)) {
            $semId = $row->fkCgaEdu;
            $mdata = CourseGroupAllocation::with(['classStudents' => function ($q) use ($id) {
                $q->where('fkCsaCga', $id);
            }
                , 'classStudents.classSemester.studentEnroll.grade' => function ($q) {
                    $q->select('pkGra', 'gra_GradeNumeric');
                }
                , 'classStudents.classSemester.studentEnroll.student'
                , 'classStudents.classSemester.studentEnroll.educationPlan' => function ($q) {
                    $q->select('pkEpl', 'epl_EducationPlanName_' . $this->current_language . ' as epl_EducationPlanName');
                }
                , 'classStudents.classSemester.studentEnroll.educationProgram' => function ($q) {
                    $q->select('pkEdp', 'edp_Name_' . $this->current_language . ' as edp_Name');
                }
                , 'classStudents.classCreationTeachers', 'GPG', 'OCG', 'FLG', 'FCG'])
                ->where('pkCga', $id)->first();

            $mainSchool = $this
                ->logged_user->sid;

            $schoolYears = SchoolYear::select('pkSye', 'sye_NameNumeric', 'sye_DefaultYear', 'sye_NameCharacter_' . $this->current_language . ' AS sye_NameCharacter')->get();
            $grades = Grade::select('pkGra', 'gra_GradeNumeric')
                ->get();
            $classes = Classes::select('pkCla', 'cla_ClassName_' . $this->current_language . ' as cla_ClassName')
                ->get();
            $empType = EmployeeType::select('pkEpty')->where('epty_Name', '=', 'Teacher')
                ->get();
            $courses = Course::select('*', 'crs_CourseName_' . $this->current_language . ' as crs_CourseName')
                ->where('crs_CourseType', 'General')
                ->get();

            $OCG = OptionalCoursesGroup::select('pkOcg as id', 'ocg_Name_' . $this->current_language . ' as group_Name')
                ->get();

            $FCG = FacultativeCoursesGroup::select('pkFcg as id', 'fcg_Name_' . $this->current_language . ' as group_Name')
                ->get();

            $FLG = ForeignLanguageGroup::select('pkFon as id', 'fon_Name_' . $this->current_language . ' as group_Name')
                ->get();

            $GPG = GeneralPurposeGroup::select('pkGpg as id', 'gpg_Name_' . $this->current_language . ' as group_Name')
                ->get();

            foreach ($empType as $key => $value) {
                $allowedTypes[] = $value->pkEpty;
            }

            $employees = EmployeesEngagement::with(['employee' => function ($q) { //Employee engagment id with employee
                $q->select('emp_EmployeeName', 'id', 'emp_EmployeeSurname');
            },
            ])
                ->where('fkEenSch', $mainSchool)->where('een_DateOfFinishEngagement', null)
                ->whereIn('fkEenEpty', $allowedTypes)->get();
            $currentSem = 0;

            $semesters = EducationPeriod::select('pkEdp', 'edp_EducationPeriodName_' . $this->current_language . ' as edp_EducationPeriodName')
                ->get();

            return view('employee.courseGroup.courseGroup')
                ->with(['mdata' => $mdata, 'mainSchool' => $mainSchool, 'schoolYears' => $schoolYears, 'grades' => $grades, 'employees' => $employees, 'courses' => $courses, 'classes' => $classes, 'OCG' => $OCG, 'FCG' => $FCG, 'FLG' => $FLG, 'GPG' => $GPG, 'currentSem' => $currentSem, 'semesters' => $semesters]);
        } else {
            $response['status'] = false;
            $response['message'] = trans('message.msg_something_wrong');

            return response()
                ->json($response);
        }
    }

    /**
     * Delete Course Group
     * @param  Request $request
     * @return JSON $response
     */
    public function destroy($cid)
    {
        $response = [];

        if (empty($cid)) {
            $response['status'] = false;
        } else {
            $stusId = ClassStudentsAllocation::where('fkCsaCga', $cid)->get()
                ->pluck('pkCsa')
                ->all();

            $eed = CourseGroupAllocation::where('pkCga', $cid)->delete();

            ClassStudentsAllocation::where('fkCsaCga', $cid)->delete();

            ClassTeachersCourseAllocation::whereIn('fkCtcCsa', $stusId)->delete();

            if ($eed) {
                $response['status'] = true;
                $response['message'] = trans('message.msg_course_group_delete_success');
            } else {
                $response['status'] = false;
                $response['message'] = trans('message.msg_course_group_delete_prompt');
            }
        }

        return response()
            ->json($response);
    }

    /**
     * Fetch Education plan hours
     * @param  Request $request
     * @return JSON $response
     */
    public function fetchEducationPlanHours(Request $request)
    {
        $input = $request->all();
        $response = [];
        switch ($input['ctype']) {
            case 'ocg':
                $hoursDetail = EducationPlansOptionalCourse::select('eoc_hours as ep_hours')->where('fkEocEpl', $input['ep_plan'])->where('eoc_order', $input['ep_order'])->where('fkEocGra', $input['ep_gra'])->first();
                $message = trans('message.msg_no_ocg_order');

                break;

            case 'flg':
                $hoursDetail = EducationPlansForeignLanguage::select('efc_hours as ep_hours')->where('fkEflEpl', $input['ep_plan'])->where('efc_order', $input['ep_order'])->where('fkEflGra', $input['ep_gra'])->first();
                $message = trans('message.msg_no_flg_order');

                break;

            default:
                $hoursDetail = [];
        }

        if (empty($hoursDetail)) {
            $response['status'] = false;
            $response['message'] = $message;
        } else {
            $response['status'] = true;
            $response['data'] = $hoursDetail->ep_hours;
        }

        return response()
            ->json($response);
    }

    /**
     * Get students for group
     * @param  Request $request
     * @return JSON $result
     */
    public function coursegroup_students_list(Request $request)
    {
        $data = $request->all();

        $perpage = !empty($data['length']) ? (int) $data['length'] : 10;

        $filter = isset($data['search']) && is_string($data['search']) ? $data['search'] : '';

        $sort_type = isset($data['order'][0]['dir']) && is_string($data['order'][0]['dir']) ? $data['order'][0]['dir'] : '';

        $sort_col = $data['order'][0]['column'];

        $sort_field = $data['columns'][$sort_col]['data'];

        $schoolYear = $data['schoolYear'];

        $currentEdp = $data['currentEdp'];
        $mainSchool = $this
            ->logged_user->sid;

        if (isset($data['grades'])) {
            $classGrades = $data['grades'];
        } else {
            $classGrades = [];
        }

        if (isset($data['classes'])) {
            $classes = $data['classes'];
        } else {
            $classes = '';
        }

        $villageschool = $data['villageschool'] ? $data['villageschool'] : null;

        $classStudent = EnrollStudent::whereHas('classStudents.classCreationSemester', function ($q) use ($currentEdp) {
            $q->where('fkCcsEdp', $currentEdp);
        })
            ->whereHas('classStudents.classCreationSemester.classCreation', function ($q) use ($schoolYear, $classes, $villageschool) {
                $q->where('fkClrSye', $schoolYear)->where('fkClrVsc', $villageschool);
                if ($classes) {
                    $q->where('fkClrCla', $classes);
                }
            })
            ->whereHas(
                'student', function ($q) use ($filter) {
                    if (!empty($filter)) {
                        $q->where(function ($query) use ($filter) {
                            $query->where('stu_StudentName', 'LIKE', '%' . $filter . '%')->orWhere('stu_StudentSurname', 'LIKE', '%' . $filter . '%')->orWhere('stu_StudentID', 'LIKE', '%' . $filter . '%')->orWhere('stu_TempCitizenId', 'LIKE', '%' . $filter . '%');
                        });
                    }
                })
            ->with([
                'student',
                'classStudents',
                'classStudents.classCreationSemester' => function ($q) use ($currentEdp) {
                    $q->where('fkCcsEdp', $currentEdp);
                },
                'classStudents.classCreationSemester.classCreation',
                'classStudents.classCreationSemester.classCreation.classCreationClasses' => function ($q) use ($classes) {
                    $q->select('pkCla', 'cla_ClassName_' . $this->current_language . ' as cla_ClassName');
                }
                , 'grade' => function ($q) {
                    $q->select('pkGra', 'gra_GradeNameRoman', 'gra_GradeNumeric');
                }
                , 'educationProgram' => function ($q) {
                    $q->select('pkEdp', 'edp_Name_' . $this->current_language . ' as edp_Name');
                }
                , 'educationPlan' => function ($q) {
                    $q->select('pkEpl', 'epl_EducationPlanName_' . $this->current_language . ' as epl_EducationPlanName');
                },
            ])
            ->whereIn('fkSteGra', $classGrades)
            ->where('fkSteSch', $mainSchool)
            ->where('fkSteSye', $schoolYear)
            ->whereNull('ste_FinishingDate')
            ->whereNull('ste_EnrollmentFinishDate');

        if ($sort_col != 0) {
            $classStudent = $classStudent->orderBy($sort_field, $sort_type);
        }

        $total_mainBooks = $classStudent->count();
        $offset = $data['start'];
        $counter = $offset;
        $classStudentdata = [];
        $classStudents = $classStudent->offset($offset)->limit($perpage)->get()
            ->toArray();
        foreach ($classStudents as $key => $value) {
            if (isset($value['class_students'])) {
                foreach ($value['class_students'] as $k => $v) {
                    if (empty($v['class_creation_semester'])) {
                        unset($value['class_students'][$k]);
                        $value['class_students'] = array_values($value['class_students']);
                    }
                }
            }

            $value['index'] = $counter + 1;
            $value['stu_DateOfBirth'] = date('d/m/Y', strtotime($value['student']['stu_DateOfBirth']));
            $value['ste_EnrollmentDate'] = date('d/m/Y', strtotime($value['ste_EnrollmentDate']));
            $value['ste_status'] = trans('general.gn_active');
            $classStudentdata[$counter] = $value;
            $counter++;
        }

        $price = array_column($classStudentdata, 'index');

        if ($sort_col == 0) {
            if ($sort_type == 'desc') {
                array_multisort($price, SORT_DESC, $classStudentdata);
            } else {
                array_multisort($price, SORT_ASC, $classStudentdata);
            }
        }
        $result = array(
            "draw" => $data['draw'],
            "recordsTotal" => $total_mainBooks,
            "recordsFiltered" => $total_mainBooks,
            'data' => $classStudentdata,
        );

        return response()->json($result);
    }

    /**
     * Fetch Course Group Data
     * @param  Request $request
     * @return JSON $result
     */
    public function coursegroup_list(Request $request)
    {
        $data = $request->all();

        $perpage = !empty($data['length']) ? (int) $data['length'] : 10;

        $filter = isset($data['search']) && is_string($data['search']) ? $data['search'] : '';

        $filterGra = isset($data['grade']) ? $data['grade'] : '';

        $filterSye = isset($data['schoolYear']) ? $data['schoolYear'] : '';

        $filterEdp = isset($data['semester']) ? $data['semester'] : '';

        $sort_type = isset($data['order'][0]['dir']) && is_string($data['order'][0]['dir']) ? $data['order'][0]['dir'] : '';
        $sort_col = isset($data['order'][0]['column']) ? $data['order'][0]['column'] : '';

        $sort_field = isset($data['columns'][$sort_col]['data']) ? $data['columns'][$sort_col]['data'] : '';

        $mainSchool = $this
            ->logged_user->sid;

        $courseGroup = CourseGroupAllocation::with(['classStudents.classCreationTeachers.employeesEngagement.employee', 'classStudents.classCreationCourses' => function ($q) {
            $q->select('pkCrs', 'crs_CourseName_' . $this->current_language . ' as crs_CourseName');
        }
            , 'GPG', 'OCG', 'FLG', 'FCG', 'courseGroupSchoolYear' => function ($q) {
                $q->select('pkSye', 'sye_NameNumeric', 'sye_DefaultYear', 'sye_NameCharacter_' . $this->current_language . ' AS sye_NameCharacter');
            },
        ])
            ->where('fkCgaSch', $mainSchool);

        if ($filterEdp) {
            $courseGroup = $courseGroup->where('fkCgaEdu', $filterEdp);
        }

        if ($filter) {
            $courseGroup = $courseGroup->whereHas('classStudents.classCreationCourses', function ($q) use ($filter) {
                $q->where('crs_CourseName_' . $this->current_language, 'LIKE', '%' . $filter . '%');
            })
                ->orWhereHas('GPG', function ($qe) use ($filter) {
                    $qe->where('gpg_Name_' . $this->current_language, 'LIKE', '%' . $filter . '%');
                })
                ->orWhereHas('OCG', function ($qee) use ($filter) {
                    $qee->where('ocg_Name_' . $this->current_language, 'LIKE', '%' . $filter . '%');
                })
                ->orWhereHas('FLG', function ($qe) use ($filter) {
                    $qe->where('fon_Name_' . $this->current_language, 'LIKE', '%' . $filter . '%');
                })
                ->orWhereHas('FCG', function ($qee) use ($filter) {
                    $qee->where('fcg_Name_' . $this->current_language, 'LIKE', '%' . $filter . '%');
                })
                ->orWhere(function ($query) use ($filter) {
                    $query->where('cga_Uid', 'LIKE', '%' . $filter . '%')->orWhere('cga_WeekHours', 'LIKE', '%' . $filter . '%');
                });

        }

        if ($filterSye) {
            $courseGroup = $courseGroup->whereHas('courseGroupSchoolYear', function ($qsye) use ($filterSye) {
                $qsye->where('pkSye', $filterSye);
            });
        }

        $courseGroupQuery = $courseGroup;

        if ($sort_col != 0) {
            $courseGroupQuery = $courseGroupQuery->orderBy($sort_field, $sort_type);
        }

        $total_class_creation = $courseGroupQuery->count();

        $offset = isset($data['start']) ? $data['start'] : '';

        $counter = $offset;
        $courseGroupdata = [];
        $courseGroups = $courseGroupQuery->offset($offset)->limit($perpage)->get()
            ->toArray();
        $counter = 0;
        foreach ($courseGroups as $key => $value) {
            $aTeach = [];
            $aStud = [];

            if (isset($value['class_students'])) {
                $tmp = [];
                $value['course'] = '';
                foreach ($value['class_students'] as $k => $v) {
                    $tmp[] = $v['fkCsaSem'];
                    $value['course'] = ucfirst($v['class_creation_courses']['crs_CourseName']);
                    if (isset($v['class_creation_teachers'])) {
                        $tmpTeach = [];
                        foreach ($v['class_creation_teachers'] as $kt => $vt) {
                            $tmpTeach[] = ucfirst($vt['employees_engagement']['employee']['emp_EmployeeName'] . " " . $vt['employees_engagement']['employee']['emp_EmployeeSurname']);
                        }
                        $aTeach = array_unique($tmpTeach);
                    }
                }
                $aStud = array_unique($tmp);
            }

            if (!empty($value['f_c_g'])) {
                $value['course_group'] = $value['f_c_g']['fcg_Name_' . $this->current_language];
            } elseif (!empty($value['f_l_g'])) {
                $value['course_group'] = $value['f_l_g']['fon_Name_' . $this->current_language];
            } elseif (!empty($value['g_p_g'])) {
                $value['course_group'] = $value['g_p_g']['gpg_Name_' . $this->current_language];
            } elseif (!empty($value['o_c_g'])) {
                $value['course_group'] = $value['o_c_g']['ocg_Name_' . $this->current_language];
            } else {
                $value['course_group'] = '';
            }

            unset($value['f_c_g']);
            unset($value['f_l_g']);
            unset($value['g_p_g']);
            unset($value['o_c_g']);

            unset($value['fkCgaFcg']);
            unset($value['fkCgaFlg']);
            unset($value['fkCgaGpg']);
            unset($value['fkCgaOcg']);
            unset($value['deleted_at']);

            $value['teachers'] = implode(', ', $aTeach);
            $value['total_stu'] = count($aStud);
            $value['index'] = $counter + 1;
            $courseGroupdata[$counter] = $value;
            $counter++;
        }

        $price = array_column($courseGroupdata, 'index');
        if ($sort_col == 0) {
            if ($sort_type == 'desc') {
                array_multisort($price, SORT_DESC, $courseGroupdata);
            } else {
                array_multisort($price, SORT_ASC, $courseGroupdata);
            }
        }
        $result = array(
            "draw" => $data['draw'],
            "recordsTotal" => $total_class_creation,
            "recordsFiltered" => $total_class_creation,
            'data' => $courseGroupdata,
        );

        return response()->json($result);
    }

    /**
     * Fetch Course Group
     * @param  Request $request
     * @return JSON $response
     */
    public function fetchCoursesGroup(Request $request)
    {
        $input = $request->all();
        $response = [];

        $courses = Course::select('pkCrs', 'crs_CourseName_' . $this->current_language . ' as crs_CourseName')
            ->where('crs_CourseType', 'General')
            ->where('crs_IsForeignLanguage', 'No')
            ->get();

        switch ($input['ctype']) {
            case 'ocg':
                $coursesGroup = OptionalCoursesGroup::select('pkOcg as id', 'ocg_Name_' . $this->current_language . ' as group_Name')
                    ->get();
                break;
            case 'fcg':
                $coursesGroup = FacultativeCoursesGroup::select('pkFcg as id', 'fcg_Name_' . $this->current_language . ' as group_Name')
                    ->get();
                break;
            case 'flg':
                $coursesGroup = ForeignLanguageGroup::select('pkFon as id', 'fon_Name_' . $this->current_language . ' as group_Name')
                    ->get();
                $courses = Course::select('pkCrs', 'crs_CourseName_' . $this->current_language . ' as crs_CourseName')
                    ->where('crs_CourseType', 'General')
                    ->where('crs_IsForeignLanguage', 'Yes')
                    ->get();
                break;
            case 'gpg':
                $coursesGroup = GeneralPurposeGroup::select('pkGpg as id', 'gpg_Name_' . $this->current_language . ' as group_Name')
                    ->get();
                break;
            default:
                $coursesGroup = [];
                $courses = [];
        }

        $response['status'] = true;
        $response['data']['courseGroup'] = $coursesGroup;
        $response['data']['courses'] = $courses;

        return response()->json($response);
    }
}
