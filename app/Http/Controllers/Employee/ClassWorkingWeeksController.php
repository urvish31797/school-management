<?php

namespace App\Http\Controllers\Employee;

use Illuminate\Http\Request;
use App\Http\Services\ClassWorkingWeekService;
use App\Models\ClassCalendarWorkingWeeks;
use App\Models\WorkingWeeks;
use App\Models\ClassCreationSemester;
use App\Http\Controllers\Controller;
use App\Http\Services\ClassCreationSemesterService;
use DataTableService;
use Datatables;
use HtmlBuilder;
use Session;
use UtilHelper;

class ClassWorkingWeeksController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(HtmlBuilder $builder)
    {
        $builder_data['columns'] = [
            ['data' => 'DT_RowIndex', 'name' => 'DT_RowIndex', 'title' => trans('general.sr_no'), 'render' => null, 'orderable' => false, 'searchable' => false],
            ['data' => 'fkCcwWek', 'name' => 'fkCcwWek', 'title' => trans('sidebar.sidebar_nav_working_weeks'), 'orderable' => false, 'searchable' => false],
            ['data' => 'fkCcwShi', 'name' => 'fkCcwShi', 'title' => trans('general.gn_shift'), 'orderable' => false, 'searchable' => false],
            ['data' => 'fkCcwSteOne', 'name' => 'fkCcwSteOne', 'title' => trans('general.gn_first_student'), 'orderable' => false, 'searchable' => false],
            ['data' => 'fkCcwSteTwo', 'name' => 'fkCcwSteTwo', 'title' => trans('general.gn_second_student'), 'orderable' => false, 'searchable' => false],
            ['data' => 'ccw_startDate', 'name' => 'ccw_startDate', 'title' => trans('general.gn_start_date')],
            ['data' => 'ccw_endDate', 'name' => 'ccw_endDate', 'title' => trans('general.gn_end_date')],
            ['data' => 'action', 'name' => 'action', 'title' => trans('general.gn_actions'), 'orderable' => false, 'searchable' => false, 'width' => '10%'],
        ];

        $builder_data['ajax'] = [
            'url'=> route('class-working-weeks.list'),
            'data' => 'function(d) {
                d.search =  $("#search_working_weeks").val();
            }'
        ];

        $dt_html = new DataTableService($builder_data, $builder);
        $dt_html = $dt_html->builder;

        return view('employee.class_working_weeks.index',compact(['dt_html']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $class_sem_id = Session::get('curr_course_class');
        $classname = Session::get('classname');
        if(!empty($class_sem_id))
        {
            $working_weeks = WorkingWeeks::select('pkWek','wek_weekName_'.$this->current_language.' AS name')->pluck('name','pkWek')->all();
            $student_list = (new ClassWorkingWeekService())->getClassStudentList($class_sem_id);
            return view('employee.class_working_weeks.create',compact('class_sem_id','classname','working_weeks','student_list'));
        }
        else{
            return abort(404);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $school_year = $this->logged_user->syid;
        $engagement_id = $this->logged_user->eid;
        $shift = $request->fkCcwShi;
        $class_sem_id = $request->fkCcwCcs;

        if(!empty($request->pkCcw))
        {
            $check_data_ary = [
                'fkCcwSye'=>$school_year,
                'fkCcwShi'=>$shift,
                'fkCcwCcs'=>$class_sem_id,
                'fkCcwWek'=>$request->fkCcwWek,
            ];
            $start_date = UtilHelper::sqlDate($request->ccw_startDate,2);
            $end_date = UtilHelper::sqlDate($request->ccw_endDate,2);

            $count = ClassCalendarWorkingWeeks::where('pkCcw','!=',$request->pkCcw)->where($check_data_ary)->count();

            if(empty($count))
            {
                unset($check_data_ary['fkCcwWek']);
                $countfordate = ClassCalendarWorkingWeeks::where('pkCcw','!=',$request->pkCcw)
                ->where(function($q) use($start_date,$end_date){
                    $q->whereDate('ccw_startDate',$start_date)
                    ->orWhereDate('ccw_endDate',$end_date);
                })
                ->where($check_data_ary)
                ->count();

                if(!empty($countfordate))
                {
                    $response['status'] = false;
                    $response['message'] = trans('message.msg_class_calendar_weeks_date_already_exist');
                }
                else
                {
                    $update_data = [
                        'fkCcwShi'=>$shift,
                        'fkCcwWek'=>$request->fkCcwWek,
                        'fkCcwSteOne'=>$request->fkCcwSteOne,
                        'fkCcwSteTwo'=>$request->fkCcwSteTwo,
                        'ccw_startDate'=>$request->ccw_startDate,
                        'ccw_endDate'=>$request->ccw_endDate,
                        'ccw_notes'=>$request->ccw_notes,
                    ];

                    ClassCalendarWorkingWeeks::find($request->pkCcw)->update($update_data);

                    $response['status'] = true;
                    $response['message'] = trans('message.msg_class_calendar_weeks_updated');
                }
            }
            else
            {
                $response['status'] = false;
                $response['message'] = trans('message.msg_class_calendar_weeks_already_exist');
            }
        }
        else
        {
            foreach ($request->fkCcwWek as $key => $value) {
                $check_data_ary = [
                    'fkCcwSye'=>$school_year,
                    'fkCcwShi'=>$shift,
                    'fkCcwCcs'=>$class_sem_id,
                    'fkCcwWek'=>$request->fkCcwWek[$key],
                ];

                $check_data_copy_ary = [
                    'fkCcwSye'=>$school_year,
                    'fkCcwShi'=>$shift,
                    'fkCcwCcs'=>$class_sem_id,
                    'fkCcwWek'=>$request->fkCcwWek[$key],
                ];

                $count = ClassCalendarWorkingWeeks::where($check_data_ary)->count();
                if(empty($count))
                {
                    unset($check_data_copy_ary['fkCcwWek']);
                    $start_date = UtilHelper::sqlDate($request->ccw_startDate[$key],2);
                    $end_date = UtilHelper::sqlDate($request->ccw_endDate[$key],2);

                    $countfordate = ClassCalendarWorkingWeeks::where(function($q) use($start_date,$end_date){
                        $q->whereDate('ccw_startDate',$start_date)
                        ->orWhereDate('ccw_endDate',$end_date);
                    })
                    ->where($check_data_ary)
                    ->count();

                    if(empty($countfordate))
                    {
                        $insert_data = [
                            'fkCcwEen'=>$engagement_id,
                            'fkCcwSteOne'=>$request->fkCcwSteOne[$key],
                            'fkCcwSteTwo'=>$request->fkCcwSteTwo[$key],
                            'ccw_startDate'=>$request->ccw_startDate[$key],
                            'ccw_endDate'=>$request->ccw_endDate[$key],
                            'ccw_notes'=>$request->ccw_notes[$key],
                        ];

                        $insert_data = array_merge($check_data_ary,$insert_data);

                        ClassCalendarWorkingWeeks::create($insert_data);
                    }
                }
            }

            $response['status'] = true;
            $response['message'] = trans('message.msg_class_calendar_weeks_success');
        }

        return response()->json($response);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $ccw = ClassCalendarWorkingWeeks::with([
        'working_week'=>function($q){
            $q->select('pkWek','wek_weekName_' . $this->current_language . ' AS wek_weekName');
        },
        'first_student_enrollment.student'=>function($q){
            $q->GetStudentName();
        },
        'second_student_enrollment.student'=>function($q){
            $q->GetStudentName();
        },
        'school_year'=>function($q){
            $q->select('pkSye','sye_NameCharacter_'. $this->current_language . ' AS sye_NameCharacter');
        },
        'shift'=>function($q){
            $q->select('pkShi','shi_ShiftName_'.$this->current_language.' AS shi_ShiftName');
        },
        ])
        ->where('pkCcw',$id)
        ->get()
        ->map(function($q){
            return ['wek_weekName'=>$q->working_week->wek_weekName,
                    'first_student'=>$q->first_student_enrollment->student->studentname,
                    'second_student'=>$q->second_student_enrollment->student->studentname,
                    'shift_name'=>$q->shift->shi_ShiftName,
                    'school_year'=>$q->school_year->sye_NameCharacter,
                    'start_date'=>$q->csd,
                    'end_date'=>$q->ced,
                    'notes'=>$q->ccw_notes,
                    ];
        });

        $ccw = array_first($ccw);
        return view('employee.class_working_weeks.show',compact('ccw'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if(!empty($id))
        {
            $row = ClassCalendarWorkingWeeks::findorFail($id);
            $class_sem_id = $row->fkCcwCcs;
            $classId = $class_sem_id;
            $language = $this->current_language;
            $param_class = compact('language','classId');
            $class_detail = ClassCreationSemester::GetGradewithClass($param_class)
            ->pluck('classCreation');
            $classname = (new ClassCreationSemesterService())->getGradewithClass($class_detail);
            $working_weeks = WorkingWeeks::select('pkWek','wek_weekName_'.$this->current_language.' AS name')
            ->pluck('name','pkWek')
            ->all();
            $student_list = (new ClassWorkingWeekService())->getClassStudentList($class_sem_id);

            return view('employee.class_working_weeks.edit',compact('row','class_sem_id','classname','working_weeks','student_list'));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $response = [];

        if (empty($id)) {
            $response['status'] = false;
        } else {
            ClassCalendarWorkingWeeks::find($id)->delete();
            $response['status'] = true;
            $response['message'] = trans('message.msg_class_calendar_weeks_delete_success');
        }

        return response()
            ->json($response);
    }

    /**
     * Class Working weeks listing page
     *
     * @return void
     */
    public function class_working_weeks_list()
    {
        $current_school_year = $this->logged_user->syid;
        $current_emp_id = $this->logged_user->eid;

        $ccw = ClassCalendarWorkingWeeks::with([
            'working_week'=>function($q){
                $q->select('pkWek','wek_weekName_' . $this->current_language . ' AS wek_weekName');
            },
            'first_student_enrollment.student'=>function($q){
                $q->GetStudentName();
            },
            'second_student_enrollment.student'=>function($q){
                $q->GetStudentName();
            },
            'shift'=>function($q){
                $q->select('pkShi','shi_ShiftName_'.$this->current_language.' AS shi_ShiftName');
            }
            ])
        ->where('fkCcwEen',$current_emp_id)
        ->where('fkCcwSye',$current_school_year);

        $ccw->when(request('search'), function ($q){
            return $q->where('ccw_startDate', 'LIKE', '%' . request('search') . '%')
            ->orWhere('ccw_endDate', 'LIKE', '%' . request('search') . '%')
            ->orWhere('ccw_notes', 'LIKE', '%' . request('search') . '%');
        });

        return DataTables::of($ccw)
            ->editColumn('fkCcwWek', function($ccw){
                return $ccw->working_week->wek_weekName;
            })
            ->editColumn('fkCcwShi', function($ccw){
                return $ccw->shift->shi_ShiftName;
            })
            ->editColumn('fkCcwSteOne', function($ccw){
                return $ccw->first_student_enrollment->student->studentname;
            })
            ->editColumn('fkCcwSteTwo', function($ccw){
                return $ccw->second_student_enrollment->student->studentname;
            })
            ->editColumn('ccw_startDate',function($ccw){
                return $ccw->csd;
            })
            ->editColumn('ccw_endDate',function($ccw){
                return $ccw->ced;
            })
            ->addColumn('action', function ($ccw) {
                $str = '<a href='.url("employee/class-working-weeks/".$ccw->pkCcw).'><i class="fa fa-eye"></i></a>&nbsp;&nbsp';
                $str .= '<a href='.url("employee/class-working-weeks/".$ccw->pkCcw."/edit").'><i class="fa fa-pen"></i></a>&nbsp;&nbsp';
                $str .= '<a onclick="triggerDelete('.$ccw->pkCcw.')" href="javascript:void(0)"><i class="fa fa-trash"></i></a>';
                return $str;
            })
            ->addIndexColumn()
            ->escapeColumns()
            ->toJSON();
    }
}
