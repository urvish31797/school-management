<?php
/**
 * FinalMarkController
 *
 * @package    Laravel
 * @subpackage Controller
 * @since      1.0
 */

namespace App\Http\Controllers\Employee;

use App\Helpers\UtilHelper;
use App\Http\Controllers\Controller;
use App\Http\Services\FinalmarksService;
use App\Models\CertificateType;
use App\Models\ClassCreation;
use App\Models\ClassCreationGrades;
use App\Models\ClassCreationSemester;
use App\Models\ClassStudentsAllocation;
use App\Models\ClassStudentsSemester;
use App\Models\ClassTeachersCourseAllocation;
use App\Models\CourseOrder;
use App\Models\DescriptiveFinalMark;
use App\Models\EducationPlan;
use App\Models\Employee;
use App\Models\EmployeesEngagement;
use App\Models\EnrollStudent;
use App\Models\Grade;
use App\Models\GradeAttemptsNumber;
use App\Models\Student;
use App\Models\StudentBehaviour;
use App\Models\StudentCertificate;
use App\Models\StudentsBehaviour;
use Illuminate\Http\Request;

class FinalMarkController extends Controller
{
    /**
     * Save Final Mark
     * Store in 4 tables : StudentCertificate,ClassStudentsSemester,StudentsBehaviour,ClassStudentsAllocation
     * @param  Request $request
     * @return JSON $response
     */
    public function store(Request $request)
    {
        $input = $request->all();

        if (!isset($input['pkCsa'])) {
            $response['status'] = false;
            $response['message'] = trans('message.msg_final_mark_fail');
            return response()
                ->json($response);
        }

        if (isset($input['pkScr'])) {
            $oldCert = 0;
        } else {
            $oldCert = StudentCertificate::where('scr_CertificateNo', $input['scr_CertificateNo'])->first();
        }

        if (!empty($oldCert)) {
            $response['status'] = false;
            $response['message'] = trans('message.msg_certificate_exists');
        } else {

            if (!isset($input['pkScr'])) {
                StudentCertificate::insertGetId(['fkScrSem' => $input['pkSem'], 'fkScrCty' => $input['fkScrCty'], 'scr_CertificateNo' => $input['scr_CertificateNo'], 'scr_Date' => UtilHelper::sqlDate($input['scr_Date'])]);
            }

            $sem_generalSuccessDescriptiveFinalmarks = "";
            $fkCcaDfm = null;

            if (isset($input['csa_ManualDescriptMarks_checkbox'])) {
                $sem_generalSuccessDescriptiveFinalmarks = $input['sem_generalSuccessDescriptiveFinalmarks'];
            } else {
                if (!empty($input['fkCcaDfm'])) {
                    $fkCcaDfm = $input['fkCcaDfm'];
                }
            }

            ClassStudentsSemester::where('pkSem', $input['pkSem'])->update(['sem_UnjustifiedAbsenceHours' => $input['sem_UnjustifiedAbsenceHours'], 'sem_JustifiedAbsenceHours' => $input['sem_JustifiedAbsenceHours'], 'sem_GeneralSuccess' => $input['sem_GeneralSuccess'], 'sem_AvgGeneralSuccess' => $input['sem_AvgGeneralSuccess'], 'fkSemGan' => $input['gan'], 'fkCcaDfm' => $fkCcaDfm, 'sem_generalSuccessDescriptiveFinalmarks' => $sem_generalSuccessDescriptiveFinalmarks]);

            if (isset($input['pkStb'])) {
                StudentsBehaviour::where('pkStb', $input['pkStb'])->update(['fkSebSem' => $input['pkSem'], 'fkStbSbe' => $input['fkStbSbe'], 'stb_Explanation' => $input['stb_Explanation'], 'created_at' => now()]);
            } else {
                StudentsBehaviour::insertGetId(['fkSebSem' => $input['pkSem'], 'fkStbSbe' => $input['fkStbSbe'], 'stb_Explanation' => $input['stb_Explanation'], 'created_at' => now()]);
            }

            foreach ($input['pkCsa'] as $k => $v) {

                $csaData = [];
                if (isset($input['csa_FinalMarks_' . $v]) && !empty($input['csa_FinalMarks_' . $v])) {
                    $csaData['csa_FinalMarks'] = $input['csa_FinalMarks_' . $v];
                }

                if (isset($input['course_manual_dfm_check_checkbox_' . $v])) {
                    if (!empty($input['csa_ManualDescriptMarks_' . $v])) {
                        $csaData['csa_ManualDescriptMarks'] = $input['csa_ManualDescriptMarks_' . $v];
                        $csaData['fkCsaDfm'] = 0;
                    } else {
                        $csaData['csa_ManualDescriptMarks'] = "";
                        $csaData['fkCsaDfm'] = 0;
                    }
                } else {
                    if (!empty($input['fkCsaDfm_' . $v])) {
                        $csaData['fkCsaDfm'] = $input['fkCsaDfm_' . $v];
                        $csaData['csa_ManualDescriptMarks'] = "";
                    } else {
                        $csaData['fkCsaDfm'] = 0;
                        $csaData['csa_ManualDescriptMarks'] = "";
                    }
                }

                if (!empty($csaData)) {
                    ClassStudentsAllocation::where('pkCsa', $v)->update($csaData);
                }

            }

            $response['status'] = true;
            $response['message'] = trans('message.msg_final_mark_success');

            $stuCerti = StudentCertificate::where('fkScrSem', $input['pkSem'])->first();
            if (!empty($stuCerti)) {
                $res = (new FinalmarksService())->generatePuppeteerPDF($input['pkSem']);
                if (!empty($res) && $res['status']) {
                    $filename = $res['filename'];
                    if (!empty($stuCerti->scr_CertificatePdfName)) {
                        $existpdf = $stuCerti->scr_CertificatePdfName;
                        $filepath = storage_path('app/public/').config('assetpath.certificate_files')."/".$existpdf;
                        if (file_exists($filepath)) {
                            unlink($filepath);
                        }
                    }

                    $stuCerti->scr_CertificatePdfName = $filename;
                    $stuCerti->save();
                }
            }

        }

        return response()
            ->json($response);
    }

    /**
     * Display all finalmarks students semester wise page
     * @param  Request $request
     * @param  Int  $id
     * @return view - employee.finalMark.finalMarks
     */
    public function show(Request $request, $id)
    {
        $mdata = '';
        if (!empty($id)) {
            \DB::statement("SET SQL_MODE=''");
            $mdata = ClassCreation::whereHas('classCreationSemester', function ($q) use ($id) {
                $q->where('pkCcs', $id);
            })->with(['classCreationSchoolYear' => function ($q) {
                $q->select('pkSye', 'sye_NameNumeric', 'sye_NameCharacter_' . $this->current_language . ' AS sye_NameCharacter');
            }
                , 'classCreationGrades.grade' => function ($q) {
                    $q->select('pkGra', 'gra_GradeNameRoman', 'gra_GradeNumeric');
                }
                , 'classCreationSemester' => function ($q) use ($id) {
                    $q->where('pkCcs', $id);
                }
                , 'classCreationSemester.homeRoomTeacher.employeesEngagement.employee' => function ($q) {
                    $q->select('id', 'emp_EmployeeName', 'emp_EmployeeSurname');
                }
                , 'classCreationSemester.chiefStudent.student', 'classCreationSemester.treasureStudent.student', 'classCreationClasses' => function ($q) {
                    $q->select('pkCla', 'cla_ClassName_' . $this->current_language . ' as cla_ClassName');
                }
                , 'classCreationSemester.semester' => function ($q) {
                    $q->select('pkEdp', 'edp_EducationPeriodName_' . $this->current_language . ' as edp_EducationPeriodName');
                }
                , 'classCreationSemester.classStudentsSemester.studentEnroll.student', 'classCreationSemester.classStudentsSemester.classStudentCertificate', 'classCreationSemester.classStudentsSemester.studentEnroll.grade' => function ($q) {
                    $q->select('pkGra', 'gra_GradeNameRoman', 'gra_GradeNumeric');
                }
                , 'classCreationSemester.classStudentsSemester.studentEnroll.educationProgram' => function ($q) {
                    $q->select('pkEdp', 'edp_Name_' . $this->current_language . ' as edp_Name');
                }
                , 'classCreationSemester.classStudentsSemester.studentEnroll.educationPlan' => function ($q) {
                    $q->select('pkEpl', 'epl_EducationPlanName_' . $this->current_language . ' as epl_EducationPlanName');
                }
                ,

            ])
                ->first();

            if ($request->has('utest')) {
                dd($mdata);
            }

            $oldStuSemAllocId = ClassStudentsSemester::where('fkSemCcs', $id)->pluck('pkSem')
                ->all();
            $oldStuAllocId = ClassStudentsAllocation::whereIn('fkCsaSem', $oldStuSemAllocId)->get()
                ->pluck('pkCsa')
                ->all();
            $oldStuAlloc = ClassStudentsSemester::with(['studentEnroll', 'classStudentsAllocation.classCreationTeachers'])->where('fkSemCcs', '=', $id)->get();

            $oldTeachers = ClassTeachersCourseAllocation::select('fkCtcEeg', 'fkCtcCsa')->whereIn('fkCtcCsa', $oldStuAllocId)->get();

            $oldTeachersSel = [];
            foreach ($oldStuAlloc as $k => $v) {
                foreach ($v->classStudentsAllocation as $kc => $vc) {
                    foreach ($vc->classCreationTeachers as $kt => $vt) {
                        $oldTeachersSel[$v
                                ->studentEnroll
                                ->fkSteGra][$vc->fkCsaEmc][] = $vt->fkCtcEeg;
                    }
                }

            }

            $oldTeachersId = [];
            foreach ($oldTeachers as $key => $value) {
                $oldTeachersId[] = $value->fkCtcEeg;
            }

            $employees = EmployeesEngagement::with(['employee' => function ($q) {
                //Employee engagment id with employee
                $q->select('emp_EmployeeName', 'id', 'emp_EmployeeSurname');
            },
            ])
                ->where('een_DateOfFinishEngagement', null)
                ->whereIn('pkEen', $oldTeachersId)->get()
                ->pluck('employee.emp_EmployeeName', 'pkEen', 'employee.emp_EmployeeSurname');

            $ccdata = ClassCreationSemester::select("fkCcsClr")->where('pkCcs', $id)->first();
            $grades = ClassCreationGrades::where('fkCcgClr', $ccdata->fkCcsClr)
                ->get()
                ->pluck('fkCcgGra')
                ->toArray();
            asort($grades);

            $selectetTeachers = [];
            $mainSchool = $this
                ->logged_user->sid;

            //fetch school courses
            $stuEnrollsIds = ClassStudentsSemester::where('fkSemCcs', $id)->pluck('fkSemSen')
                ->all();
            $educationPlans = EnrollStudent::whereIn('pkSte', $stuEnrollsIds)->get()
                ->pluck("fkSteEpl")
                ->all();
            $courses = Grade::select('*', 'gra_GradeNumeric')->with(['educationPlansMandatoryCourse' => function ($query) use ($grades, $educationPlans) {
                $query->whereIn('fkEmcGra', $grades)->whereIn('fkEmcEpl', $educationPlans);
            }
                , 'educationPlansMandatoryCourse.mandatoryCourseGroup' => function ($query) use ($grades, $educationPlans) {
                    $query->select('pkCrs', 'crs_CourseName_' . $this->current_language . ' as crs_CourseName');
                }
                , 'educationPlansMandatoryCourse.educationPlan' => function ($query) use ($grades) {
                    $query->select('pkEpl', 'epl_EducationPlanName_' . $this->current_language . ' as epl_EducationPlanName');
                },
            ])
                ->whereIn('pkGra', $grades)->get();

        }

        if (request()
            ->ajax()) {
            return \View::make('employee.finalMark.finalMarks')
                ->with(['mdata' => $mdata, 'courses' => $courses, 'grades_ids' => $grades, 'employees' => $employees, 'existTeacher' => $oldTeachersSel])->renderSections();
        }
        return view('employee.finalMark.finalMarks', ['mdata' => $mdata, 'courses' => $courses, 'grades_ids' => $grades, 'employees' => $employees, 'existTeacher' => $oldTeachersSel]);
    }

    /**
     * Use for fetch finalmark for particular student
     * @param  Request $request
     * @param  Int  $id
     * @return view - employee.finalMark.addfinalMark
     * @return view - employee.finalMark.editfinalMark
     */
    public function edit(Request $request, $id)
    {
        $mdata = '';

        if (!empty($id)) {
            $cssData = classStudentsSemester::select('fkSemCcs')->where('pkSem', $id)->first();

            \DB::statement("SET SQL_MODE=''");
            $mdata = ClassCreation::whereHas('classCreationSemester', function ($q) use ($cssData) {
                $q->where('pkCcs', $cssData->fkSemCcs);
            })->with(['classCreationSchoolYear' => function ($q) {
                $q->select('pkSye', 'sye_NameNumeric', 'sye_NameCharacter_' . $this->current_language . ' AS sye_NameCharacter');
            }
                , 'classCreationGrades.grade' => function ($q) {
                    $q->select('pkGra', 'gra_GradeNameRoman', 'gra_GradeNumeric');
                }
                , 'classCreationSemester' => function ($q) use ($cssData) {
                    $q->where('pkCcs', $cssData->fkSemCcs);
                }
                , 'classCreationSemester.classStudentsSemester' => function ($q) use ($id) {
                    $q->where('pkSem', $id);
                }
                , 'classCreationSemester.homeRoomTeacher.employeesEngagement.employee' => function ($q) {
                    $q->select('id', 'emp_EmployeeName', 'emp_EmployeeSurname');
                }
                , 'classCreationSemester.chiefStudent.student', 'classCreationSemester.treasureStudent.student', 'classCreationClasses' => function ($q) {
                    $q->select('pkCla', 'cla_ClassName_' . $this->current_language . ' as cla_ClassName');
                }
                , 'classCreationSemester.semester' => function ($q) {
                    $q->select('pkEdp', 'edp_EducationPeriodName_' . $this->current_language . ' as edp_EducationPeriodName');
                }
                , 'classCreationSemester.classStudentsSemester.studentEnroll.student', 'classCreationSemester.classStudentsSemester.classStudentsBehaviour', 'classCreationSemester.classStudentsSemester.classStudentCertificate', 'classCreationSemester.classStudentsSemester.studentEnroll.grade' => function ($q) {
                    $q->select('pkGra', 'gra_GradeNameRoman', 'gra_GradeNumeric');
                }
                , 'classCreationSemester.classStudentsSemester.studentEnroll.educationProgram' => function ($q) {
                    $q->select('pkEdp', 'edp_Name_en', 'edp_Name_' . $this->current_language . ' as edp_Name');
                }
                , 'classCreationSemester.classStudentsSemester.classStudentsAllocation.classCreationCourses' => function ($q) {
                    $q->select('pkCrs', 'crs_CourseName_' . $this->current_language . ' as crs_CourseName', 'crs_CourseAlternativeName');
                }
                , 'classCreationSemester.classStudentsSemester.classStudentsAllocation.courseGroup', 'classCreationSemester.classStudentsSemester.studentEnroll.educationPlan' => function ($q) {
                    $q->select('pkEpl', 'epl_EducationPlanName_' . $this->current_language . ' as epl_EducationPlanName');
                }
                ,

            ])->first();

            $GAN = GradeAttemptsNumber::select('pkGan', 'gan_Name_' . $this->current_language . ' as gan_Name')
                ->get();
            $DFM = DescriptiveFinalMark::select('pkDfm', 'dfm_Text_en', 'dfm_Text_' . $this->current_language . ' as dfm_Text')
                ->get();
            $SDT = StudentBehaviour::select('pkSbe', 'sbe_BehaviourName_' . $this->current_language . ' as sbe_BehaviourName')
                ->get();
            $certificateType = CertificateType::select('pkCty', 'cty_Name_' . $this->current_language . ' as cty_Name')
                ->get();

            $ccdata = ClassCreationSemester::select("fkCcsClr")->where('pkCcs', $cssData->fkSemCcs)
                ->first();
            $grades = ClassCreationGrades::where('fkCcgClr', $ccdata->fkCcsClr)
                ->get()
                ->pluck('fkCcgGra')
                ->toArray();
            asort($grades);

            $selectetTeachers = [];
            $mainSchool = $this
                ->logged_user->sid;
            //fetch school courses
            $stuEnrollsIds = ClassStudentsSemester::where('fkSemCcs', $cssData->fkSemCcs)
                ->pluck('fkSemSen')
                ->all();
            $educationPlans = EnrollStudent::whereIn('pkSte', $stuEnrollsIds)->get()
                ->pluck("fkSteEpl")
                ->all();

            $courseOrderData = CourseOrder::with(['courseOrderAllocation' => function ($q) use ($mdata) {
                $q->where('fkCoaGra', $mdata->classCreationSemester[0]->classStudentsSemester[0]->studentEnroll->grade->pkGra)
                    ->with(['course']);
            },
            ])->where('fkCorEpl', $mdata->classCreationSemester[0]->classStudentsSemester[0]->studentEnroll->educationPlan->pkEpl)
                ->where('fkCorSch', $mainSchool)
                ->first();

            if (is_null($courseOrderData)) {
                $response['status'] = true;
                $response['message'] = trans('message.msg_course_order_create_warning');
                return response()
                    ->json($response);
            }

            $coursesOrderData = (new FinalmarksService())->createCourseOrderforCourse($mdata, $courseOrderData);
            extract($coursesOrderData);

            $showDFM = (new FinalmarksService())->checkStatusforDescriptiveFinalMark($mdata);
            $finalMarksScore = [1, 2, 3, 4, 5];

        }

        $studentCertificates = StudentCertificate::where('fkScrSem', $id)->first();
        if (!empty($studentCertificates)) {
            $view = 'employee.finalMark.editfinalMark';
        } else {
            $view = 'employee.finalMark.addfinalMark';
        }

        $studentBehaviour = StudentsBehaviour::where('fkSebSem', $id)->orderBy('pkStb', 'DESC')
            ->first();

        $data = compact('mdata', 'courses', 'GAN', 'DFM', 'SDT', 'certificateType', 'OCGCrs', 'FCGCrs', 'showDFM', 'finalMarksScore', 'studentCertificates', 'studentBehaviour');

        return view($view, $data);
    }

    /**
     * Print Certificate
     * @param  Request $request
     * @return JSON $res
     */
    public function generateCertificate(Request $request)
    {
        $input = $request->all();
        $id = $input['pkSem'];

        $res['status'] = false;
        $res['message'] = trans('message.msg_something_wrong');
        if (!empty($id)) {
            $certi = StudentCertificate::where('fkScrSem', $id)->first();
            if (!empty($certi)) {
                unset($res);
                $res['status'] = true;
                $res['filepath'] = asset('files/studentcertificates') . "/" . $certi->scr_CertificatePdfName;
            }
        }

        return response()
            ->json($res);
    }
}
