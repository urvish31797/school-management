<?php
/**
 * CourseOrderController
 *
 * @package    Laravel
 * @subpackage Controller
 * @since      1.0
 */

namespace App\Http\Controllers\Employee;

use App\Http\Controllers\Controller;
use App\Models\Course;
use App\Models\CourseOrder;
use App\Models\CourseOrderAllocation;
use App\Models\EducationPlan;
use App\Models\Employee;
use App\Models\SchoolEducationPlanAssignment;
use Illuminate\Http\Request;

class CourseOrderController extends Controller
{
    /**
     * Used for Course Order listing page
     * @param  Request $request
     * @return view - employee.courseOrder.courseOrders
     */
    public function index(Request $request)
    {
        return view('employee.courseOrder.courseOrders');
    }

    /**
     * Course Order Page
     * @param  Request $request
     * @return view - employee.courseOrder.courseOrder
     */
    public function create(Request $request)
    {
        $mainSchool = $this
            ->logged_user->sid;

        $planDetail = SchoolEducationPlanAssignment::
            // with('educationPlan','schoolEducationPlanAssignment.educationProgram.parent')
            with(['educationPlan' => function ($query) {
            $query->select('pkEpl', 'epl_EducationPlanName_' . $this->current_language . ' as epl_EducationPlanName');
        }
            , 'educationProgram' => function ($query) {
                $query->select('pkEdp', 'edp_Name_' . $this->current_language . ' as edp_Name');
            },
        ])
            ->where('sep_Status', 'Active')
            ->where('fkSepSch', $mainSchool)->get();

        if (request()
            ->ajax()) {
            return \View::make('employee.courseOrder.courseOrder')
                ->with(['planDetail' => $planDetail])->renderSections();
        }
        return view('employee.courseOrder.courseOrder')
            ->with(['planDetail' => $planDetail]);
    }

    /**
     * View Course Order Page
     * @param  Int $id
     * @return view - employee.courseOrder.viewCourseOrder
     */
    public function show($id)
    {
        $mdata = '';
        if (!empty($id)) {

            $mdata = CourseOrder::with(['educationPlan' => function ($q) {
                $q->select('pkEpl', 'epl_EducationPlanName_' . $this->current_language . ' as epl_EducationPlanName');
            }
                , 'educationProgram' => function ($q) {
                    $q->select('pkEdp', 'edp_Name_' . $this->current_language . ' as edp_Name');
                }
                , 'courseOrderAllocation', 'educationPlan.mandatoryCourse.mandatoryCourseGroup' => function ($query) {
                    $query->select('pkCrs', 'crs_CourseName_' . $this->current_language . ' as crs_CourseName')
                        ->where('crs_CourseType', '!=', 'DummyOptionalCourse')
                        ->where('crs_CourseType', '!=', 'DummyForeignCourse');
                }
                , 'educationPlan.optionalCourse.optionalCoursesGroup' => function ($query) {
                    $query->select('pkCrs', 'crs_CourseName_' . $this->current_language . ' as crs_CourseName')
                        ->where('crs_CourseType', '=', 'DummyOptionalCourse');
                }
                , 'educationPlan.foreignLanguageCourse.foreignLanguageGroup' => function ($query) {
                    $query->select('pkCrs', 'crs_CourseName_' . $this->current_language . ' as crs_CourseName')
                        ->where('crs_CourseType', '=', 'DummyForeignCourse');
                }
                , 'educationPlan.mandatoryCourse.grade' => function ($query) {
                    $query->select('pkGra', 'gra_GradeNameRoman', 'gra_GradeNumeric');
                }
                , 'educationPlan.optionalCourse.grade' => function ($query) {
                    $query->select('pkGra', 'gra_GradeNameRoman', 'gra_GradeNumeric');
                }
                , 'educationPlan.foreignLanguageCourse.grade' => function ($query) {
                    $query->select('pkGra', 'gra_GradeNameRoman', 'gra_GradeNumeric');
                },
            ])
                ->where('pkCor', $id)->first();

            $mainSchool = $this
                ->logged_user->sid;

            $planDetail = SchoolEducationPlanAssignment::with(['educationPlan' => function ($query) {
                $query->select('pkEpl', 'epl_EducationPlanName_' . $this->current_language . ' as epl_EducationPlanName');
            }
                , 'educationProgram' => function ($query) {
                    $query->select('pkEdp', 'edp_Name_' . $this->current_language . ' as edp_Name');
                },
            ])
                ->where('sep_Status', 'Active')
                ->where('fkSepSch', $mainSchool)->get();

            return view('employee.courseOrder.viewCourseOrder')
                ->with(['planDetail' => $planDetail, 'mdata' => $mdata]);

        } else {
            return redirect($this->logged_user->utype . '/dashboard');
            }
        }

        /**
     * Save Course Order
     * @param Request $request
     * @return JSON $response
     */
        public function store(Request $request)
    {
        $input = $request->all();
        $response = [];
        $mainSchool = $this
            ->logged_user->sid;

        if (isset($input['pkCor'])) {
            $pkCor = $input['pkCor'];
            unset($input['pkCor']);
        }
        $epl = $input['epl'];
        $crs_ids = $input['crs_ids'];
        $epr_ids = $input['epr_ids'];
        $gra_ids = $input['gra_ids'];

        unset($input['epl']);
        unset($input['crs_ids']);
        unset($input['epr_ids']);
        unset($input['gra_ids']);
        unset($input['epl_ids']);

        $crs_orders = array_values($input);

        if (!isset($pkCor)) { //Add Course Order
            $existsCor = CourseOrder::where('fkCorSch', $mainSchool)->where('fkCorEdp', $epr_ids[0])->where('fkCorEpl', $epl)->first();

            if (empty($existsCor)) {
                $pkCor = CourseOrder::insertGetId(['fkCorSch' => $mainSchool, 'fkCorEdp' => $epr_ids[0], 'fkCorEpl' => $epl]);

                CourseOrder::where('pkCor', $pkCor)->update(['cor_Uid' => 'COR' . $pkCor]);

                foreach ($crs_ids as $k => $v) {
                    $coaId = CourseOrderAllocation::insertGetId(['fkCoaCor' => $pkCor, 'fkCoaCrs' => $v, 'fkCoaGra' => $gra_ids[$k], 'coa_OrderNo' => $crs_orders[$k]]);
                }

                $response['status'] = true;
                $response['message'] = trans('message.msg_course_order_create_success');

            } else {

                $response['status'] = false;
                $response['message'] = trans('message.msg_course_order_exists');
            }

        } else { //Edit Course Order
            $existsCoaAll = CourseOrderAllocation::where('fkCoaCor', $pkCor)->get()
                ->pluck('pkCoa')
                ->all();
            $newExistsCoaAll = [];

            foreach ($crs_ids as $k => $v) {
                $existsCoa = CourseOrderAllocation::where('fkCoaCor', $pkCor)->where('fkCoaCrs', $v)->where('fkCoaGra', $gra_ids[$k])->first();
                if (empty($existsCoa)) {
                    $coaId = CourseOrderAllocation::insertGetId(['fkCoaCor' => $pkCor, 'fkCoaCrs' => $v, 'fkCoaGra' => $gra_ids[$k], 'coa_OrderNo' => $crs_orders[$k]]);
                } else {
                    $newExistsCoaAll[] = $existsCoa->pkCoa;
                    CourseOrderAllocation::where('pkCoa', $existsCoa->pkCoa)
                        ->update(['coa_OrderNo' => $crs_orders[$k]]);
                }
            }

            $delCoa = array_diff($existsCoaAll, $newExistsCoaAll);

            CourseOrderAllocation::whereIn('pkCoa', $delCoa)->delete();

            $response['status'] = true;
            $response['message'] = trans('message.msg_course_order_update_success');
        }

        return response()
            ->json($response);
    }

    /**
     * Edit Course Order Page
     * @param  Int $id
     * @return view - employee.courseOrder.courseOrder
     */
    public function edit($id)
    {
        $mdata = CourseOrder::with(['educationPlan' => function ($q) {
            $q->select('pkEpl', 'epl_EducationPlanName_' . $this->current_language . ' as epl_EducationPlanName');
        }
            , 'educationProgram' => function ($q) {
                $q->select('pkEdp', 'edp_Name_' . $this->current_language . ' as edp_Name');
            }
            , 'courseOrderAllocation', 'educationPlan.mandatoryCourse.mandatoryCourseGroup' => function ($query) {
                $query->select('pkCrs', 'crs_CourseName_' . $this->current_language . ' as crs_CourseName')
                    ->where('crs_CourseType', '!=', 'DummyOptionalCourse')
                    ->where('crs_CourseType', '!=', 'DummyForeignCourse');
            }
            , 'educationPlan.optionalCourse.optionalCoursesGroup' => function ($query) {
                $query->select('pkCrs', 'crs_CourseName_' . $this->current_language . ' as crs_CourseName')
                    ->where('crs_CourseType', '=', 'DummyOptionalCourse');
            }
            , 'educationPlan.foreignLanguageCourse.foreignLanguageGroup' => function ($query) {
                $query->select('pkCrs', 'crs_CourseName_' . $this->current_language . ' as crs_CourseName')
                    ->where('crs_CourseType', '=', 'DummyForeignCourse');
            }
            , 'educationPlan.mandatoryCourse.grade' => function ($query) {
                $query->select('pkGra', 'gra_GradeNameRoman', 'gra_GradeNumeric');
            }
            , 'educationPlan.optionalCourse.grade' => function ($query) {
                $query->select('pkGra', 'gra_GradeNameRoman', 'gra_GradeNumeric');
            }
            , 'educationPlan.foreignLanguageCourse.grade' => function ($query) {
                $query->select('pkGra', 'gra_GradeNameRoman', 'gra_GradeNumeric');
            },
        ])
            ->where('pkCor', $id)->first();

        $mainSchool = $this
            ->logged_user->sid;
        $planDetail = SchoolEducationPlanAssignment::with(['educationPlan' => function ($query) {
            $query->select('pkEpl', 'epl_EducationPlanName_' . $this->current_language . ' as epl_EducationPlanName');
        }
            , 'educationProgram' => function ($query) {
                $query->select('pkEdp', 'edp_Name_' . $this->current_language . ' as edp_Name');
            },
        ])
            ->where('sep_Status', 'Active')
            ->where('fkSepSch', $mainSchool)->get();

        if (request()
            ->ajax()) {
            return \View::make('employee.courseOrder.courseOrder')
                ->with(['planDetail' => $planDetail, 'mdata' => $mdata])->renderSections();
        }
        return view('employee.courseOrder.courseOrder')
            ->with(['planDetail' => $planDetail, 'mdata' => $mdata]);
    }

    /**
     * Delete Course Order
     * @param  Request $request
     * @return JSON $response
     */
    public function destroy(Request $request, $cid)
    {
        $response = [];

        if (empty($cid)) {
            $response['status'] = false;
        } else {
            $eed = CourseOrder::where('pkCor', $cid)->delete();

            CourseOrderAllocation::where('fkCoaCor', $cid)->delete();

            if ($eed) {
                $response['status'] = true;
                $response['message'] = trans('message.msg_course_order_delete_success');
            } else {
                $response['status'] = false;
                $response['message'] = trans('message.msg_course_order_delete_prompt');
            }
        }

        return response()
            ->json($response);
    }

    /**
     * Fetch Course Order Data
     * @param  Request $request
     * @return JSON $result
     */
    public function courseorders_list(Request $request)
    {
        $data = $request->all();

        $perpage = !empty($data['length']) ? (int) $data['length'] : 10;

        $filter = isset($data['search']) && is_string($data['search']) ? $data['search'] : '';

        $sort_type = isset($data['order'][0]['dir']) && is_string($data['order'][0]['dir']) ? $data['order'][0]['dir'] : '';

        $sort_col = isset($data['order'][0]['column']) ? $data['order'][0]['column'] : '';

        $sort_field = isset($data['columns'][$sort_col]['data']) ? $data['columns'][$sort_col]['data'] : '';
        $mainSchool = $this
            ->logged_user->sid;
        $courseOrder = CourseOrder::with(['educationPlan' => function ($q) {
            $q->select('pkEpl', 'epl_EducationPlanName_' . $this->current_language . ' as epl_EducationPlanName');
        }
            , 'educationProgram' => function ($q) {
                $q->select('pkEdp', 'edp_Name_' . $this->current_language . ' as edp_Name');
            }
        ])
            ->where('fkCorSch', $mainSchool);

        if ($filter) {
            $courseOrder = $courseOrder->whereHas('educationPlan', function ($q) use ($filter) {
                $q->where('epl_EducationPlanName_' . $this->current_language, 'LIKE', '%' . $filter . '%');
            })
                ->orWhereHas('educationProgram', function ($qe) use ($filter) {
                    $qe->where('edp_Name_' . $this->current_language, 'LIKE', '%' . $filter . '%');
                })
                ->orWhere(function ($query) use ($filter) {
                    $query->where('cor_Uid', 'LIKE', '%' . $filter . '%');
                });

        }

        $courseOrderQuery = $courseOrder;

        if ($sort_col != 0) {
            $courseOrderQuery = $courseOrderQuery->orderBy($sort_field, $sort_type);
        }

        $total_class_creation = $courseOrderQuery->count();

        $offset = isset($data['start']) ? $data['start'] : '';

        $counter = $offset;
        $courseOrderdata = [];
        $courseOrders = $courseOrderQuery->offset($offset)->limit($perpage)->get()
            ->toArray();
        $counter = 0;
        foreach ($courseOrders as $key => $value) {
            unset($value['fkCorEdp']);
            unset($value['fkCorEpl']);
            unset($value['fkCorSch']);
            unset($value['deleted_at']);

            $value['index'] = $counter + 1;
            $courseOrderdata[$counter] = $value;
            $counter++;
        }

        $price = array_column($courseOrderdata, 'index');
        if ($sort_col == 0) {
            if ($sort_type == 'desc') {
                array_multisort($price, SORT_DESC, $courseOrderdata);
            } else {
                array_multisort($price, SORT_ASC, $courseOrderdata);
            }
        }
        $result = array(
            "draw" => $data['draw'],
            "recordsTotal" => $total_class_creation,
            "recordsFiltered" => $total_class_creation,
            'data' => $courseOrderdata,
        );

        return response()->json($result);
    }

    /**
     * Fetch Course Page
     * @param  Request $request
     * @return JSON - $response
     */
    public function fetchCourses(Request $request)
    {
        $input = $request->all();
        $mainSchool = $this
            ->logged_user->sid;
        $checkCourseOrder = CourseOrder::where('fkCorSch', $mainSchool)->where('fkCorEpl', $input['epl'])->count();
        $response = [];
        if ($checkCourseOrder) {
            $response['status'] = false;
            $response['message'] = trans('message.msg_course_order_exists');
            return response()
                ->json($response);
        }

        $mdata = EducationPlan::with(['mandatoryCourse.mandatoryCourseGroup' => function ($query) {
            $query->select('pkCrs', 'crs_CourseName_' . $this->current_language . ' as crs_CourseName')
                ->where('crs_CourseType', '!=', 'DummyOptionalCourse')
                ->where('crs_CourseType', '!=', 'DummyForeignCourse');
        }
            , 'optionalCourse.optionalCoursesGroup' => function ($query) {
                $query->select('pkCrs', 'crs_CourseName_' . $this->current_language . ' as crs_CourseName')
                    ->where('crs_CourseType', '=', 'DummyOptionalCourse');
            }
            , 'foreignLanguageCourse.foreignLanguageGroup' => function ($query) {
                $query->select('pkCrs', 'crs_CourseName_' . $this->current_language . ' as crs_CourseName')
                    ->where('crs_CourseType', '=', 'DummyForeignCourse');
            }
            , 'mandatoryCourse.grade' => function ($query) {
                $query->select('pkGra', 'gra_GradeNameRoman', 'gra_GradeNumeric');
            }
            , 'optionalCourse.grade' => function ($query) {
                $query->select('pkGra', 'gra_GradeNameRoman', 'gra_GradeNumeric');
            }
            , 'foreignLanguageCourse.grade' => function ($query) {
                $query->select('pkGra', 'gra_GradeNameRoman', 'gra_GradeNumeric');
            },
        ]);
        $mdata = $mdata->where('pkEpl', '=', $input['epl'])->first();

        if (!empty($mdata)) {
            $response['status'] = true;
            $response['data'] = $mdata;
        } else {
            $response['status'] = false;
            $response['message'] = trans('message.msg_something_wrong');
        }

        return response()
            ->json($response);
    }
}
