<?php
namespace App\Http\Controllers\Employee;

use App\Http\Services\AttendanceAccomplishmentService;
use App\Http\Services\ClassWorkingWeekService;
use App\Models\DailyStudentAttendance;
use Illuminate\Http\Request;
use UtilHelper;

class AttendanceStudentBehaviourController extends AttendanceAccomplishmentController{

    public function edit($id)
    {
        if (!empty($id)) {
            $language = $this->current_language;
            $dailyBookLectureData = (new AttendanceAccomplishmentService())->getTeacherClassDetails($id, $language);
            extract($dailyBookLectureData);

            $lecture_details = $class_details->pluck('lecture_details')->toArray();
            $attendance_details = $class_details->pluck('attendance_details')->toArray();

            $date = UtilHelper::sqlDate($basic_details['date'],2);
            $week_params = ['language'=>$language,
                            'shift_id'=>$basic_details['shift_id'],
                            'class_sem_id'=>$basic_details['class_sem_id'],
                            'date'=>$date];
            $working_weeks_details = (new ClassWorkingWeekService())->getOnDutyStudents($week_params);

            return view('employee.attendanceAccomplishment.studentBehaviour.create_behaviour_notes')->with(compact('lecture_details','attendance_details','basic_details','working_weeks_details'));
        }
    }

    public function store(Request $request)
    {
        foreach ($request->pkSae as $value) {
            DailyStudentAttendance::find($value)->update(['sae_StudentTDesc'=>$request->sae_StudentTDesc[$value]]);
        }

        $response['message'] = trans('message.msg_student_behaviour_success');
        return response()->json($response,200);
    }
}