<?php
/**
 * VillageSchoolController
 *
 * @package    Laravel
 * @subpackage Controller
 * @since      1.0
 */

namespace App\Http\Controllers\Employee;

use App\Http\Controllers\Controller;
use App\Models\Employee;
use App\Models\PostalCode;
use App\Models\VillageSchool;
use Illuminate\Http\Request;
use App\Http\Requests\VillageSchoolRequest;
use DataTableService;
use Datatables;
use HtmlBuilder;
use HttpResponse;

class VillageSchoolController extends Controller
{
    /**
     * VillageSchool Listing Page
     * @param  Request $request
     * @return view - employee.villageSchools.villageSchools
     */
    public function index(HtmlBuilder $builder)
    {
        $PostalCodes = PostalCode::select('pkPof', 'pof_PostOfficeNumber', 'pof_PostOfficeName_' . $this->current_language . ' as pof_PostOfficeName')
            ->get();
        $mainSchool = $this
            ->logged_user->sid;

            $builder_data['columns'] = [
                ['data' => 'DT_RowIndex', 'name' => 'DT_RowIndex', 'title' => trans('general.sr_no'), 'render' => null, 'orderable' => false, 'searchable' => false],
                ['data' => 'vsc_Uid', 'name' => 'vsc_Uid', 'title' => trans('general.gn_uid')],
                ['data' => 'vsc_VillageSchoolName', 'name' => 'vsc_VillageSchoolName', 'title' => trans('general.gn_name')],
                ['data' => 'sch_SchoolName', 'name' => 'fkVscSch', 'title' => trans('general.gn_main_school'), 'orderable' => false, 'searchable' => false],
                ['data' => 'vsc_Residence', 'name' => 'vsc_Residence', 'title' => trans('general.gn_residence')],
                ['data' => 'vsc_Notes', 'name' => 'vsc_Notes', 'title' => trans('general.gn_note')],
                ['data' => 'action', 'name' => 'action', 'title' => trans('general.gn_actions'), 'orderable' => false, 'searchable' => false, 'width' => '10%'],
            ];
    
            $builder_data['ajax'] = [
                'url'=> route('fetch-villageschool-lists'),
                'data' => 'function(d) {
                    d.search =  $("#search_village_school").val();
                }'
            ];
    
            $dt_html = new DataTableService($builder_data, $builder);
            $dt_html = $dt_html->builder;

        return view('employee.villageSchools.villageSchools')->with(compact('PostalCodes','mainSchool','dt_html'));
    }

    public function store(VillageSchoolRequest $request)
    {
        $column = 'vsc_VillageSchoolName_'.$this->current_language;
        $value = $request[$column];
        $response = [];

        if (!empty($request->pkVsc)) {
            $checkPrev = VillageSchool::where($column,$value)
                    ->where('pkVsc', '!=', $request->pkVsc)->first();
            if (!empty($checkPrev)) {
                $status_code = HttpResponse::HTTP_BAD_REQUEST;
                $response['message'] = trans('message.msg_village_school_exist');
            } else {
                $id = VillageSchool::where('pkVsc', $request->pkVsc)->update($request->validated());
                
                $response['message'] = trans('message.msg_village_school_update_success');
            }
        } else {
            $checkPrev = VillageSchool::where($column,$value)->first();
            if (!empty($checkPrev)) {
                $status_code = HttpResponse::HTTP_BAD_REQUEST;
                $response['message'] = trans('message.msg_village_school_exist');
            } else {
                $insertadata = $request->validated();
                $insertadata['fkVscSch'] = $this->logged_user->sid;
                $id = VillageSchool::insertGetId($insertadata);
                if (!empty($id)) {
                    $id = VillageSchool::where('pkVsc', $id)->update(['vsc_Uid' => "VSC" . $id]);
                    
                    $response['message'] = trans('message.msg_village_school_add_success');

                } else {
                    $status_code = HttpResponse::HTTP_BAD_REQUEST;
                    $response['message'] = trans('message.msg_something_wrong');
                }
            }
        }

        return response()->json($response,$status_code ?? HttpResponse::HTTP_OK);
    }

    public function edit(Request $request, $cid)
    {
        $response = [];
        $cdata = VillageSchool::where('pkVsc', '=', $cid)->first();

        if (empty($cid) || empty($cdata)) {
            $status_code = HttpResponse::HTTP_BAD_REQUEST;
        } else {
            $response['data'] = $cdata;
        }

        return response()->json($response,$status_code ?? HttpResponse::HTTP_OK);
    }

    public function destroy($cid)
    {
        $response = [];

        if (empty($cid)) {
            $status_code = HttpResponse::HTTP_BAD_REQUEST;
        } else {
            VillageSchool::where('pkVsc', $cid)->delete();
            $response['message'] = trans('message.msg_village_school_delete_success');
        }

        return response()->json($response,$status_code ?? HttpResponse::HTTP_OK);
    }

    public function villageschool_lsdist(Request $request)
    {
        $data = $request->all();

        $perpage = !empty($data['length']) ? (int) $data['length'] : 10;

        $filter = isset($data['search']) && is_string($data['search']) ? $data['search'] : '';

        $sort_type = isset($data['order'][0]['dir']) && is_string($data['order'][0]['dir']) ? $data['order'][0]['dir'] : '';

        $sort_col = $data['order'][0]['column'];

        $sort_field = $data['columns'][$sort_col]['data'];

        $mainSchool = $this
            ->logged_user->sid;

        $VillageSchool = VillageSchool::with('school')->whereHas('school', function ($query) use ($mainSchool, $filter) {
            if ($filter) {
                $query->where('sch_SchoolName_' . $this->current_language, 'LIKE', '%' . $filter . '%')->where('pkSch', $mainSchool);
            }
        });

        if ($filter) {
            $VillageSchool = $VillageSchool->orWhere(function ($query) use ($filter) {
                $query->where('vsc_Uid', 'LIKE', '%' . $filter . '%')->orWhere('vsc_VillageSchoolName_' . $this->current_language, 'LIKE', '%' . $filter . '%');
            });
        }

        $VSQuery = $VillageSchool->where('fkVscSch', $mainSchool);

        if ($sort_col != 0) {
            $VSQuery = $VSQuery->orderBy($sort_field, $sort_type);
        }

        $total_vs = $VSQuery->count();

        $offset = $data['start'];

        $counter = $offset;
        $VSdata = [];
        $countries = $VSQuery->select('*', 'vsc_VillageSchoolName_' . $this->current_language . ' as vsc_VillageSchoolName')
            ->offset($offset)->limit($perpage)->get()
            ->toArray();

        foreach ($countries as $key => $value) {
            $value['index'] = $counter + 1;
            $VSdata[$counter] = $value;
            $counter++;
        }

        $price = array_column($VSdata, 'index');

        if ($sort_col == 0) {
            if ($sort_type == 'desc') {
                array_multisort($price, SORT_DESC, $VSdata);
            } else {
                array_multisort($price, SORT_ASC, $VSdata);
            }
        }

        $VSdata = array_values($VSdata);
        $result = array(
            "draw" => $data['draw'],
            "recordsTotal" => $total_vs,
            "recordsFiltered" => $total_vs,
            'data' => $VSdata,
        );

        return response()->json($result);
    }

    public function villageschool_list()
    {        
        $mainSchool = $this->logged_user->sid;

        $VillageSchool = VillageSchool::with(['school'=>function($q){
            $q->select('pkSch','sch_SchoolName_' . $this->current_language . ' as sch_SchoolName');
        }])
        ->select('*', 'vsc_VillageSchoolName_' . $this->current_language . ' as vsc_VillageSchoolName')
        ->where('fkVscSch', $mainSchool);

        $VillageSchool->when(request('search'), function ($q){
            return $q->where('vsc_Uid', 'LIKE', '%' . request('search') . '%')
            ->orWhere('vsc_VillageSchoolName_' . $this->current_language, 'LIKE', '%' . request('search') . '%');
        });

        $VillageSchool->when(empty(array_first(request('order'))['column']), function($q){
            return $q->orderBy('pkVsc','ASC');
        });

        return DataTables::of($VillageSchool)
            ->editColumn('vsc_Address', function($VillageSchool){
                return substr($VillageSchool->vsc_Address,0,45);
            })
            ->editColumn('sch_SchoolName', function($VillageSchool){
                return $VillageSchool->school->sch_SchoolName ?? '';
            })
            ->addColumn('action', function ($VillageSchool) {
                $str = '<a cid="'.$VillageSchool->pkVsc.'" onclick="triggerEdit('.$VillageSchool->pkVsc.')" href="javascript:void(0)"><i class="fa fa-pen"></i></a>&nbsp;&nbsp';
                $str .= '<a onclick="triggerDelete('.$VillageSchool->pkVsc.')" href="javascript:void(0)"><i class="fa fa-trash"></i></a>';
                return $str;
            })
            ->addIndexColumn()
            ->escapeColumns()
            ->toJSON();
    }
}
