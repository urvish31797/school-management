<?php

namespace App\Http\Controllers\Employee;

use App\Models\ClassCreationSemester;
use App\Models\ClassStudentsAllocation;
use App\Models\DailyBookLecture;
use App\Models\DailyStudentAttendance;
use App\Http\Services\AttendanceAccomplishmentService;
use Illuminate\Http\Request;
use Session;
use UtilHelper;

class AttendanceAccomplishmentSubController extends AttendanceAccomplishmentController
{
    /**
     * Get Student Listing for attendance page
     *
     * @param \Illuminate\Http\Request $request
     * @return void
     */
    public function classstudents_list(Request $request)
    {
        $data = $request->all();

        $perpage = !empty($data['length']) ? (int) $data['length'] : 10;

        $filter = isset($data['search']) && is_string($data['search']) ? $data['search'] : '';

        $sort_type = isset($data['order'][0]['dir']) && is_string($data['order'][0]['dir']) ? $data['order'][0]['dir'] : '';

        $sort_col = $data['order'][0]['column'];

        $sort_field = $data['columns'][$sort_col]['data'];

        $mainSchool = $this->logged_user->sid;
        $lecture_number = ($request->lecture_number > 0 ? $request->lecture_number : '');

        if (isset($data['grades']) && !empty($data['grades'])) {
            $grades = $data['grades'];
        } else {
            $grades = [];
        }

        $classId = 0;
        $ccsGroups = '';
        $subclassId = 0;
        $courseId = 0;
        $coursegroupId = 0;
        $selectedCourseData = $this->selectedClassData();
        extract($selectedCourseData['current_lecture_data']);

        $classStudent = ClassStudentsAllocation::select('pkCsa', 'fkCsaSem', 'fkCsaEmc', 'fkCsaScg', 'fkCsaCga')
            ->whereHas('classSemester', function ($q) use ($ccsGroups) {
                $q->whereIn('fkSemCcs', explode(",",$ccsGroups));
            })
            ->whereHas('classSemester.studentEnroll.grade', function ($q) use ($grades) {
                if (!empty($grades)) {
                    $q->whereIn('pkGra', $grades);
                }
            })
            ->with(['classSemester' => function ($q) {
                    $q->select('pkSem', 'fkSemCcs', 'fkSemSen');
                }
                , 'classSemester.studentEnroll' => function ($q) {
                    $q->select('pkSte', 'fkSteStu', 'fkSteGra');
                }
                , 'classSemester.studentEnroll.grade' => function ($q) {
                    $q->select('pkGra', 'gra_GradeNumeric');
                }
                , 'classSemester.studentEnroll.student' => function ($q) {
                    $q->select('id', 'stu_StudentID', 'stu_TempCitizenId', \DB::raw("CONCAT(COALESCE(stu_StudentName,''), ' ', COALESCE(stu_StudentSurname,''), ' ',COALESCE(stu_FatherName,'')) AS stuName"));
                }])->where('fkCsaEmc', $courseId);

        if (!empty($subclassId)) {
            $classStudent = $classStudent->where('fkCsaScg', $subclassId);
        } else {
            $classStudent = $classStudent->whereNull('fkCsaScg');
        }

        if (!empty($coursegroupId)) {
            $classStudent = $classStudent->where('fkCsaCga', $coursegroupId);
        } else {
            $classStudent = $classStudent->whereNull('fkCsaCga');
        }

        $total_students = $classStudent->count();

        $offset = $data['start'];

        $counter = $offset;
        $classStudentdata = [];
        $classStudents = $classStudent->offset($offset)->limit($perpage)->get()
            ->toArray();

        foreach ($classStudents as $key => $value) {
            $value['index'] = $counter + 1;

            if (empty($value['class_semester']['student_enroll']['student']['stu_StudentID'])) {
                $value['studentId'] = $value['class_semester']['student_enroll']['student']['stu_TempCitizenId'];
            } else {
                $value['studentId'] = $value['class_semester']['student_enroll']['student']['stu_StudentID'];
            }

            $value['enrollId'] = $value['class_semester']['student_enroll']['pkSte'];
            $value['gra_GradeNumeric'] = $value['class_semester']['student_enroll']['grade']['gra_GradeNumeric'];
            $value['pkGra'] = $value['class_semester']['student_enroll']['grade']['pkGra'];
            $value['pkSem'] = $value['class_semester']['pkSem'];
            $value['stu_StudentName'] = $value['class_semester']['student_enroll']['student']['stuName'] ?? '';

            $check_box_value = $value['pkSem'] . '-' . $value['pkCsa'] . '-' . $value['pkGra'];

            $value['hour1'] = '<div class="form-group form-check"><input ' . ($lecture_number == 1 ? 'checked' : 'disabled="disabled"') . ' type="checkbox" class="form-check-input check_all_1 stu_checkboxes" name="hour1[]" data-enrollid=' . $value['enrollId'] . ' data-pksem=' . $value['pkSem'] . ' data-pkCsa=' . $value['pkCsa'] . ' data-pkGra=' . $value['pkGra'] . ' value=' . $check_box_value . '><label class="custom_checkbox"></label></div>';
            $value['hour2'] = '<div class="form-group form-check"><input ' . ($lecture_number == 2 ? 'checked' : 'disabled="disabled"') . ' type="checkbox" class="form-check-input check_all_2 stu_checkboxes" name="hour2[]" data-enrollid=' . $value['enrollId'] . ' data-pksem=' . $value['pkSem'] . ' data-pkCsa=' . $value['pkCsa'] . ' data-pkGra=' . $value['pkGra'] . ' value=' . $check_box_value . '><label class="custom_checkbox"></label></div>';
            $value['hour3'] = '<div class="form-group form-check"><input ' . ($lecture_number == 3 ? 'checked' : 'disabled="disabled"') . ' type="checkbox" class="form-check-input check_all_3 stu_checkboxes" name="hour3[]" data-enrollid=' . $value['enrollId'] . ' data-pksem=' . $value['pkSem'] . ' data-pkCsa=' . $value['pkCsa'] . ' data-pkGra=' . $value['pkGra'] . ' value=' . $check_box_value . '><label class="custom_checkbox"></label></div>';
            $value['hour4'] = '<div class="form-group form-check"><input ' . ($lecture_number == 4 ? 'checked' : 'disabled="disabled"') . ' type="checkbox" class="form-check-input check_all_4 stu_checkboxes" name="hour4[]" data-enrollid=' . $value['enrollId'] . ' data-pksem=' . $value['pkSem'] . ' data-pkCsa=' . $value['pkCsa'] . ' data-pkGra=' . $value['pkGra'] . ' value=' . $check_box_value . '><label class="custom_checkbox"></label></div>';
            $value['hour5'] = '<div class="form-group form-check"><input ' . ($lecture_number == 5 ? 'checked' : 'disabled="disabled"') . ' type="checkbox" class="form-check-input check_all_5 stu_checkboxes" name="hour5[]" data-enrollid=' . $value['enrollId'] . ' data-pksem=' . $value['pkSem'] . ' data-pkCsa=' . $value['pkCsa'] . ' data-pkGra=' . $value['pkGra'] . ' value=' . $check_box_value . '><label class="custom_checkbox"></label></div>';
            $value['hour6'] = '<div class="form-group form-check"><input ' . ($lecture_number == 6 ? 'checked' : 'disabled="disabled"') . ' type="checkbox" class="form-check-input check_all_6 stu_checkboxes" name="hour6[]" data-enrollid=' . $value['enrollId'] . ' data-pksem=' . $value['pkSem'] . ' data-pkCsa=' . $value['pkCsa'] . ' data-pkGra=' . $value['pkGra'] . ' value=' . $check_box_value . '><label class="custom_checkbox"></label></div>';
            $value['hour7'] = '<div class="form-group form-check"><input ' . ($lecture_number == 7 ? 'checked' : 'disabled="disabled"') . ' type="checkbox" class="form-check-input check_all_7 stu_checkboxes" name="hour7[]" data-enrollid=' . $value['enrollId'] . ' data-pksem=' . $value['pkSem'] . ' data-pkCsa=' . $value['pkCsa'] . ' data-pkGra=' . $value['pkGra'] . ' value=' . $check_box_value . '><label class="custom_checkbox"></label></div>';

            $classStudentdata[$counter] = $value;
            $counter++;
        }

        if ($sort_col != 0) {
            $price = array_column($classStudentdata, $sort_field);
        } else {
            $price = array_column($classStudentdata, 'index');
        }

        if ($sort_col != 0) {
            if ($sort_type == 'desc') {
                array_multisort($price, SORT_DESC, SORT_STRING, $classStudentdata);
            } else {
                array_multisort($price, SORT_ASC, SORT_STRING, $classStudentdata);
            }
        } else {
            if ($sort_type == 'desc') {
                array_multisort($price, SORT_DESC, $classStudentdata);
            } else {
                array_multisort($price, SORT_ASC, $classStudentdata);
            }
        }

        $classStudentdata = array_values($classStudentdata);

        $results = array(
            "draw" => $data['draw'],
            "recordsTotal" => $total_students,
            "recordsFiltered" => $total_students,
            'data' => $classStudentdata,
        );

        return response()->json($results);
    }

    /**
     * Student Attendance Listing
     *
     * @param \Illuminate\Http\Request $request
     * @return JSON
     */
    public function teacher_attendance_listing(Request $request)
    {
        $data = $request->all();
        $perpage = !empty($data['length']) ? (int) $data['length'] : 10;
        $filter = isset($data['search']) && is_string($data['search']) ? $data['search'] : '';
        $sort_type = isset($data['order'][0]['dir']) && is_string($data['order'][0]['dir']) ? $data['order'][0]['dir'] : '';
        $sort_col = $data['order'][0]['column'];
        $sort_field = $data['columns'][$sort_col]['data'];

        $employeeEeg = Session::get('curr_emp_eid');
        $shift = Session::get('curr_shift');
        $schoolYear = Session::get('curr_school_year');
        $currentSem = Session::get('curr_school_sem');

        $dailyBookData = [];
        $total_DailyBook = 0;
        $classSems = ClassCreationSemester::whereHas('classCreation', function ($q) use ($schoolYear, $shift) {
            $q->where('fkClrSye', $schoolYear);
        })
            ->whereHas('classTeachers', function ($q) use ($employeeEeg) {
                $q->where('fkCtcEeg', $employeeEeg)->whereNull('Ctc_end');
            })
            ->where('fkCcsEdp', $currentSem)
            ->get()
            ->pluck('pkCcs')
            ->all();

        if (!empty($classSems)) {

            $Lectures = DailyBookLecture::whereHas('dailyBook', function ($q) use ($classSems, $schoolYear, $shift) {
                $q->whereIn('fkDbkCcs', $classSems)
                    ->where('fkDbkSye', $schoolYear)
                    ->where('fkDbkShi', $shift);
                })
                ->whereHas('syllabusAccomplishment', function ($q) use ($employeeEeg) {
                    $q->where('fkSyaEen', $employeeEeg);
                })
                ->with([
                    'syllabusAccomplishment.course' => function ($q) {
                        $q->select('pkCrs', 'crs_CourseName_' . $this->current_language . ' AS courseName');
                    }
                    , 'dailyBook.shift' => function ($q) {
                        $q->select('pkShi', 'shi_ShiftName_' . $this->current_language . ' AS shiftName');
                    }
                    , 'syllabusAccomplishment.grade' => function ($q) {
                        $q->select('pkGra', 'gra_GradeNumeric');
                    }
                    , 'syllabusAccomplishment.class' => function ($q) {
                        $q->select('pkCla', 'cla_ClassName_' . $this->current_language . ' AS cla_ClassName');
                    }
                    , 'dailyBook.villageSchool' => function ($q) {
                        $q->select('pkVsc', 'vsc_VillageSchoolName_' . $this->current_language . ' as vsc_VillageSchoolName');
                    },
                ]);

            if ($sort_col != 0) {
                $Lectures = $Lectures->orderBy($sort_field, $sort_type);
            }

            $total_DailyBook = $Lectures->count();
            $offset = $data['start'];
            $counter = $offset;
            $Lectures = $Lectures->offset($offset)
                ->limit($perpage)
                ->get();
            // dd($Lectures);
            foreach ($Lectures as $key => $value) {
                // $courses = [];
                // $grades[] = collect($value['syllabus_accomplishment'])
                //     ->pluck('grade')
                //     ->pluck('gra_GradeNumeric')
                //     ->all();

                // $classes[] = collect($value['syllabus_accomplishment'])
                //     ->pluck('class')
                //     ->pluck('cla_ClassName')
                //     ->all();

                // $courses[] = collect($value['syllabus_accomplishment'])
                //     ->pluck('course')
                //     ->pluck('courseName')
                //     ->all();
                // $courseName = implode(",", $courses[0]);
                // $fullClassName = UtilHelper::createClassSyntaxLine($grades[0], $classes[0]);
                $fullClassName = "";
                $courseName = "";
                $villageSchoolName = $value->dailyBook->villageSchool->vsc_VillageSchoolName ?? '-';

                $dta['index'] = $counter + 1;
                $dta['gradeClass'] = $fullClassName;
                $dta['pkDbk'] = $value->dailyBook->pkDbk;
                $dta['pkDbl'] = $value->pkDbl;
                $dta['shiftName'] = $value->dailyBook->shift->shiftName;
                $dta['dbk_Date'] = $value->dailyBook->dbk_Date;
                $dta['courseName'] = $courseName;
                $dta['lectureNumber'] = $value->dbl_LectureNumber;
                $dta['villageSchoolName'] = $villageSchoolName;

                $dailyBookData[$counter] = $dta;
                $counter++;
            }
        }

        $colmns = array_column($dailyBookData, 'index');

        if ($sort_col == 0) {
            if ($sort_type == 'desc') {
                array_multisort($colmns, SORT_DESC, $dailyBookData);
            } else {
                array_multisort($colmns, SORT_ASC, $dailyBookData);
            }
        }

        $dailyBookData = array_values($dailyBookData);

        $results = array(
            "draw" => $data['draw'],
            "recordsTotal" => $total_DailyBook,
            "recordsFiltered" => $total_DailyBook,
            'data' => $dailyBookData,
        );

        return response()->json($results);
    }

    /**
     * Get Class Attendance List
     *
     * @param \Illuminate\Http\Request $request
     * @return JSON
     */
    public function lecture_detail_listing(Request $request)
    {
        $data = $request->all();

        $perpage = !empty($data['length']) ? (int) $data['length'] : 10;

        $filter = isset($data['search']) && is_string($data['search']) ? $data['search'] : '';

        $sort_type = isset($data['order'][0]['dir']) && is_string($data['order'][0]['dir']) ? $data['order'][0]['dir'] : '';

        $sort_col = $data['order'][0]['column'];

        $sort_field = $data['columns'][$sort_col]['data'];

        $classStudent = DailyStudentAttendance::with([
            'classStudentsSemester' => function ($q) {
                $q->select('pkSem', 'fkSemCcs', 'fkSemSen');
            },
            'classStudentsSemester.studentEnroll.student' => function ($q) {
                $q->select('id', 'stu_StudentID', 'stu_TempCitizenId', \DB::raw("CONCAT(stu_StudentSurname, ' ',stu_StudentName) AS stuName"));
            }])
            ->select('pkSae', 'fkSaedbl', 'fkSaeSem', 'sae_LectureAttendance', 'sae_LecutreStudentStatus', 'sae_StudentTDesc', 'sae_StudentHTDesc')
            ->where('fkSaedbl', $data['pkDbl']);

        $total_students = $classStudent->count();

        $offset = $data['start'];

        $counter = $offset;
        $classStudentdata = [];
        $classStudents = $classStudent->offset($offset)->limit($perpage)->get()
            ->toArray();

        foreach ($classStudents as $key => $value) {
            $value['index'] = $counter + 1;

            if (empty($value['class_students_semester']['student_enroll']['student']['stu_StudentID'])) {
                $value['studentId'] = $value['class_students_semester']['student_enroll']['student']['stu_TempCitizenId'];
            } else {
                $value['studentId'] = $value['class_students_semester']['student_enroll']['student']['stu_StudentID'];
            }

            $value['stu_StudentName'] = $value['class_students_semester']['student_enroll']['student']['stuName'];

            if ($value['sae_LectureAttendance'] == 'A') {
                $checkedString = '';
            } else {
                $checkedString = 'checked';
            }
            $value['hour'] = '<div class="form-group form-check"><input type="checkbox" ' . $checkedString . ' disabled="disabled" class="form-check-input"><label class="custom_checkbox"></label></div>';

            $classStudentdata[$counter] = $value;
            $counter++;
        }

        if ($sort_col != 0) {
            $price = array_column($classStudentdata, $sort_field);
        } else {
            $price = array_column($classStudentdata, 'index');
        }

        if ($sort_col != 0) {
            if ($sort_type == 'desc') {
                array_multisort($price, SORT_DESC, SORT_STRING, $classStudentdata);
            } else {
                array_multisort($price, SORT_ASC, SORT_STRING, $classStudentdata);
            }
        } else {
            if ($sort_type == 'desc') {
                array_multisort($price, SORT_DESC, $classStudentdata);
            } else {
                array_multisort($price, SORT_ASC, $classStudentdata);
            }
        }

        $classStudentdata = array_values($classStudentdata);

        $results = array(
            "draw" => $data['draw'],
            "recordsTotal" => $total_students,
            "recordsFiltered" => $total_students,
            'data' => $classStudentdata,
        );

        return response()->json($results);
    }

    public function fetch_course_hours(Request $request)
    {
        $hours_details = (new AttendanceAccomplishmentService())->getCourseHoursforVillageSchoolClass($request->course_id,$request->grade_id,$request->classDetail);
        return response()->json($hours_details);
    }
}
