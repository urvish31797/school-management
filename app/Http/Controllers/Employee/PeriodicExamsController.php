<?php

namespace App\Http\Controllers\Employee;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\PeriodicExams;
use App\Models\{ClassStudentsAllocation,PeriodicExamWay,DescriptivePeriodicExamMark};
use App\Http\Services\AttendanceAccomplishmentService;
use HttpResponse;
use HtmlBuilder;
use DataTableService;
use Datatables;
use Session;
use UtilHelper;

class PeriodicExamsController extends Controller
{
    public function index(HtmlBuilder $builder)
    {
        $builder_data['columns'] = [
            ['data' => 'DT_RowIndex', 'name' => 'DT_RowIndex', 'title' => trans('general.sr_no'), 'render' => null, 'orderable' => false, 'searchable' => false],
            ['data' => 'pex_Date', 'name' => 'pex_Date', 'title' => trans('general.gn_date')],
            ['data' => 'fkPexCsa', 'name' => 'fkPexCsa', 'title' => trans('general.gn_student_name'),'render' => null, 'orderable' => false, 'searchable' => false],
            ['data' => 'fkPexPet', 'name' => 'fkPexPet', 'title' => trans('general.gn_periodic_exam_type'),'render' => null, 'orderable' => false, 'searchable' => false],
            ['data' => 'pex_ExamContent', 'name' => 'pex_ExamContent', 'title' => trans('general.gn_exam_content')],
            ['data' => 'pex_ExamNumericMark', 'name' => 'pex_ExamNumericMark', 'title' => trans('general.gn_numeric_mark')],
            ['data' => 'action', 'name' => 'action', 'title' => trans('general.gn_actions'), 'orderable' => false, 'searchable' => false, 'width' => '10%'],
        ];

        $builder_data['ajax'] = [
            'url'=> route('periodic-exam.list'),
            'data' => 'function(d) {
                d.search =  $("#search_periodic_exam").val();
            }'
        ];

        $dt_html = new DataTableService($builder_data, $builder);
        $dt_html = $dt_html->builder;

        return view('employee.periodicExams.index',compact('dt_html'));
    }

    public function create()
    {
        $examway = PeriodicExamWay::CurrentPeriodicWay()->first();
        $default_exam_way = $examway->pkPew;

        $classData = self::selectedClassData();

        $classDetail = $classData['current_lecture_data'];
        $language = $this->current_language;

        if (!empty($classDetail['classId']) && !empty($classData)) {
            $components = (new AttendanceAccomplishmentService())->createClassComponents($classData, $language);
            return view('employee.periodicExams.create',compact('components','default_exam_way'));
        } else {
            return redirect('employee/dashboard');
        }
    }

    public function store(Request $request)
    {
        if(count($request->student_data) > 0)
        {
            foreach($request->student_data as $key => $value){
                $student_data = explode("-",$value);
                $row['fkPexEdp'] = $student_data[2];
                $row['fkPexPew'] = $request->fkPexPew;
                $row['fkPexPet'] = $request->fkPexPet;
                $row['pex_Date'] = UtilHelper::sqlDate($request->pex_Date);
                $row['fkPexSem'] = $student_data[0];
                $row['pex_ExamContent'] = $request->pex_ExamContent;
                $row['fkPexEen'] = $this->logged_user->eid;
                $row['fkPexCsa'] = $key;
                $row['fkPexDpem'] = $request->fkPexDpem[$key];
                $row['pex_ExamNumericMark'] = $request->pex_ExamNumericMark[$key];
                $row['pex_ExamPointsNumber'] = $request->pex_ExamPointsNumber[$key];
                
                PeriodicExams::insert($row);
            }

            $response['message'] = trans('message.msg_periodic_exam_saved_sucessfully');
        }
        else
        {
            $status_code = HttpResponse::HTTP_BAD_REQUEST;
            $response['message'] = trans('message.msg_something_wrong');
        }

        return response()->json($response,$status_code ?? HttpResponse::HTTP_OK);
    }

    public function show($id)
    {
        
    }

    public function edit($id)
    {
        return view('employee.periodicExams.edit');
    }

    public function destroy($id)
    {
        $response = [];
        if (empty($id)) {
            $status_code = HttpResponse::HTTP_BAD_REQUEST;
        } else {
            PeriodicExams::where('pkPex', $id)->delete();
            $response['message'] = trans('message.msg_periodic_exam_delete_success');
        }

        return response()->json($response,$status_code ?? HttpResponse::HTTP_OK);
    }

    public function exam_listing()
    {
        $current_engagement_id = $this->logged_user->eid;
        $classData = self::selectedClassData();
        $classDetail = $classData['current_lecture_data'];
        extract($classDetail);

        $classStudent = ClassStudentsAllocation::whereHas('classSemester.classCreationSemester',function($q) use($classId){
            $q->where('pkCcs',$classId);
        })->where('fkCsaEmc', $courseId);

        if (!empty($subclassId)) {
            $classStudent = $classStudent->where('fkCsaScg', $subclassId);
        } else {
            $classStudent = $classStudent->whereNull('fkCsaScg');
        }

        if (!empty($coursegroupId)) {
            $classStudent = $classStudent->where('fkCsaCga', $coursegroupId);
        } else {
            $classStudent = $classStudent->whereNull('fkCsaCga');
        }

        $classStudents = $classStudent->pluck('pkCsa');

        $periodic_exam = PeriodicExams::with([
            'classstudentsemester.studentEnroll.student' => function ($q){
                $q->select('id', 'stu_StudentID', 'stu_TempCitizenId', \DB::raw("CONCAT(COALESCE(stu_StudentName,''), ' ', COALESCE(stu_StudentSurname,''), ' ',COALESCE(stu_FatherName,'')) AS stuName"));
            },
            'periodicexamtype' => function($q){
                $q->select('pkPet','pet_Name_'.$this->current_language.' AS pet_Name');
            }
        ])->where('fkPexEen',$current_engagement_id)->whereIn('fkPexCsa',$classStudents);

        $periodic_exam->when(empty(array_first(request('order'))['column']), function($q){
            return $q->orderBy('pkPex','DESC');
        });

        // $periodic_exam->when(request('search'), function ($q){
            // return $q->orWhere('dbk_Verified', 'LIKE', '%' . request('search') . '%')
            // ->orWhere('dbk_Date','LIKE', '%'. request('search') .'%');
        // });

        return DataTables::of($periodic_exam)
            ->editColumn('fkPexCsa', function($periodic_exam){
                return $periodic_exam->classstudentsemester->studentEnroll->student->stuName ?? '-';
            })
            ->editColumn('fkPexPet', function($periodic_exam){
                return $periodic_exam->periodicexamtype->pet_Name ?? '-';
            })
            ->addColumn('action', function ($periodic_exam) {
                $str = '<a href='.url('/employee/periodic-exam/'.$periodic_exam->pkPex.'/edit').'><i class="fa fa-pen"></i></a>&nbsp;&nbsp';
                $str .= '<a onclick="triggerDelete('.$periodic_exam->pkPex.')" href="javascript:void(0)"><i class="fa fa-trash"></i></a>';
                return $str;
            })
            ->addIndexColumn()
            ->escapeColumns()
            ->toJSON();
    }

    public function exam_student_listing(Request $request)
    {
        $data = $request->all();

        $perpage = !empty($data['length']) ? (int) $data['length'] : 10;

        $filter = isset($data['search']) && is_string($data['search']) ? $data['search'] : '';

        $sort_type = isset($data['order'][0]['dir']) && is_string($data['order'][0]['dir']) ? $data['order'][0]['dir'] : '';

        $sort_col = $data['order'][0]['column'];

        $sort_field = $data['columns'][$sort_col]['data'];

        $mainSchool = $this->logged_user->sid;
        $lecture_number = ($request->lecture_number > 0 ? $request->lecture_number : '');

        if (isset($data['grades']) && !empty($data['grades'])) {
            $grades = $data['grades'];
        } else {
            $grades = [];
        }

        $classId = 0;
        $ccsGroups = '';
        $subclassId = 0;
        $courseId = 0;
        $coursegroupId = 0;
        $selectedCourseData = $this->selectedClassData();
        extract($selectedCourseData['current_lecture_data']);

        $classStudent = ClassStudentsAllocation::select('pkCsa', 'fkCsaSem', 'fkCsaEmc', 'fkCsaScg', 'fkCsaCga')
            ->whereHas('classSemester', function ($q) use ($ccsGroups) {
                $q->whereIn('fkSemCcs', explode(",",$ccsGroups));
            })
            ->whereHas('classSemester.studentEnroll.grade', function ($q) use ($grades) {
                if (!empty($grades)) {
                    $q->whereIn('pkGra', $grades);
                }
            })
            ->with(['classSemester' => function ($q) {
                    $q->select('pkSem', 'fkSemCcs', 'fkSemSen');
                }
                , 'classSemester.classCreationSemester' => function ($q){
                    $q->select('pkCcs','fkCcsEdp');
                }
                , 'classSemester.studentEnroll' => function ($q) {
                    $q->select('pkSte', 'fkSteStu', 'fkSteGra');
                }
                , 'classSemester.studentEnroll.grade' => function ($q) {
                    $q->select('pkGra', 'gra_GradeNumeric');
                }
                , 'classSemester.studentEnroll.student' => function ($q) {
                    $q->select('id', 'stu_StudentID', 'stu_TempCitizenId', \DB::raw("CONCAT(COALESCE(stu_StudentName,''), ' ', COALESCE(stu_StudentSurname,''), ' ',COALESCE(stu_FatherName,'')) AS stuName"));
                }])
            ->where('fkCsaEmc', $courseId);

        if (!empty($subclassId)) {
            $classStudent = $classStudent->where('fkCsaScg', $subclassId);
        } else {
            $classStudent = $classStudent->whereNull('fkCsaScg');
        }

        if (!empty($coursegroupId)) {
            $classStudent = $classStudent->where('fkCsaCga', $coursegroupId);
        } else {
            $classStudent = $classStudent->whereNull('fkCsaCga');
        }

        $total_students = $classStudent->count();

        $offset = $data['start'];

        $counter = $offset;
        $classStudentdata = [];
        $classStudents = $classStudent->offset($offset)->limit($perpage)->get()
            ->toArray();
        // dd($classStudents);
        $descriptive_final_marks = DescriptivePeriodicExamMark::select('pkDpem','dm_Name_'.$this->current_language.' AS dm_Name')->get();
        
        $option = "";
        foreach($descriptive_final_marks as $k => $v){
            $option .= '<option value='.$v['pkDpem'].'>'.$v['dm_Name'].'</option>';
        }
        $select_final_mark_end = '</select>';

        foreach ($classStudents as $key => $value) {
            $value['index'] = $counter + 1;

            if (empty($value['class_semester']['student_enroll']['student']['stu_StudentID'])) {
                $value['studentId'] = $value['class_semester']['student_enroll']['student']['stu_TempCitizenId'];
            } else {
                $value['studentId'] = $value['class_semester']['student_enroll']['student']['stu_StudentID'];
            }

            $value['enrollId'] = $value['class_semester']['student_enroll']['pkSte'];
            $value['gra_GradeNumeric'] = $value['class_semester']['student_enroll']['grade']['gra_GradeNumeric'];
            $value['pkGra'] = $value['class_semester']['student_enroll']['grade']['pkGra'];
            $value['pkSem'] = $value['class_semester']['pkSem'];
            $value['education_period'] = $value['class_semester']['class_creation_semester']['fkCcsEdp'];
            $value['stu_StudentName'] = $value['class_semester']['student_enroll']['student']['stuName'] ?? '';

            $select_final_mark_start = '<select name="fkPexDpem['.$value['pkCsa'].']" class="form-control icon_control dropdown_control"><option value="">'.trans('general.gn_select').'</option>';

            $descriptive_final_mark = $select_final_mark_start.$option.$select_final_mark_end;
            
            $value['descriptive_final_mark'] = $descriptive_final_mark;
            
            $numeric_mark = '<select name="pex_ExamNumericMark['.$value['pkCsa'].']" class="form-control icon_control dropdown_control"><option value="">'.trans('general.gn_select').'</option><option value="1">1</option><option value="2">2</option><option value="3">3</option><option value="4">4</option><option value="5">5</option></select>';

            $value['numeric_mark'] = $numeric_mark;
            $value['exam_points_number'] = '<input type="text" class="form-control" name="pex_ExamPointsNumber['.$value['pkCsa'].']" ">';

            $value['checkboxvalue'] = $value['pkSem'] . '-' . $value['pkCsa'] . '-' . $value['education_period'];
            $value['hidden_checkbox'] = '<input type="hidden" name="student_data['.$value['pkCsa'].']" value='.$value['checkboxvalue'].'>';

            $classStudentdata[$counter] = $value;
            $counter++;
        }

        if ($sort_col != 0) {
            $price = array_column($classStudentdata, $sort_field);
        } else {
            $price = array_column($classStudentdata, 'index');
        }

        if ($sort_col != 0) {
            if ($sort_type == 'desc') {
                array_multisort($price, SORT_DESC, SORT_STRING, $classStudentdata);
            } else {
                array_multisort($price, SORT_ASC, SORT_STRING, $classStudentdata);
            }
        } else {
            if ($sort_type == 'desc') {
                array_multisort($price, SORT_DESC, $classStudentdata);
            } else {
                array_multisort($price, SORT_ASC, $classStudentdata);
            }
        }

        $classStudentdata = array_values($classStudentdata);

        $results = array(
            "draw" => $data['draw'],
            "recordsTotal" => $total_students,
            "recordsFiltered" => $total_students,
            'data' => $classStudentdata,
        );

        return response()->json($results);
    }

    public function selectedClassData()
    {
        $data['current_lecture_data'] = Session::get('curr_course_class');
        $data['current_employee'] = Session::get('curr_emp_eid');
        $data['current_school_year'] = Session::get('curr_school_year');
        $data['current_shift'] = Session::get('curr_shift');
        $data['current_sem'] = Session::get('curr_school_sem');
        $data['current_school'] = Session::get('curr_emp_sid');

        return $data;
    }
}
