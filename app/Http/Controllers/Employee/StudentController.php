<?php
namespace App\Http\Controllers\Employee;

use App\Helpers\FrontHelper;
use App\Helpers\MailHelper;
use App\Helpers\UtilHelper;
use App\Http\Controllers\Controller;
use App\Models\Admin;
use App\Models\ClassStudentsSemester;
use App\Models\Country;
use App\Models\EducationPlan;
use App\Models\EducationProgram;
use App\Models\Employee;
use App\Models\EnrollStudent;
use App\Models\Grade;
use App\Models\JobAndWork;
use App\Models\MainBook;
use App\Models\Municipality;
use App\Models\Nationality;
use App\Models\PostalCode;
use App\Models\Religion;
use App\Models\SchoolEducationPlanAssignment;
use App\Models\SchoolYear;
use App\Models\Student;
use App\Models\VillageSchool;
use Illuminate\Http\Request;

class StudentController extends Controller
{
    /**
     * Students Listing Page
     * @param  Request $request
     * @return view - employee.student.student
     */
    public function index(Request $request)
    {
        if (request()
            ->ajax()) {
            return \View::make('employee.student.student')->renderSections();
        }
        return view('employee.student.student');
    }

    /**
     * Add Student Page
     * @param Request $request
     * @return view - employee.student.addStudent
     */
    public function create(Request $request)
    {
        $municipality = Municipality::with([
            'canton.state.country' => function ($q) {
                $q->select('pkCny', 'cny_CountryName_' . $this->current_language . ' as cny_CountryName');
            },
        ])->select('pkMun', 'fkMunCan', 'mun_MunicipalityName_' . $this->current_language . ' as mun_MunicipalityName')->get();

        $nationality = Nationality::select('pkNat', 'nat_NationalityName_' . $this->current_language . ' as nat_NationalityName')
            ->get();
        $country = Country::select('pkCny', 'cny_CountryName_' . $this->current_language . ' as cny_CountryName')
            ->get();
        $religion = Religion::select('pkRel', 'rel_ReligionName_' . $this->current_language . ' as rel_ReligionName')
            ->get();
        $postalCode = PostalCode::with([
            'municipality' => function ($q) {
                $q->select('pkMun', 'fkMunCan', 'mun_MunicipalityName_' . $this->current_language . ' as mun_MunicipalityName');
            },
            'municipality.canton.state.country' => function ($q) {
                $q->select('pkCny', 'cny_CountryName_' . $this->current_language . ' as cny_CountryName');
            },
        ])->select('pkPof', 'fkPofMun', 'pof_PostOfficeNumber', 'pof_PostOfficeName_' . $this->current_language . ' as pof_PostOfficeName')
            ->get();

        $jawWork = JobAndWork::select('pkJaw', 'jaw_Name_' . $this->current_language . ' as jaw_Name')
            ->get();

        if (request()
            ->ajax()) {
            return \View::make('employee.student.addStudent')
                ->with(['jawWork' => $jawWork, 'postalCode' => $postalCode, 'municipality' => $municipality, 'nationality' => $nationality, 'country' => $country, 'religion' => $religion])->renderSections();
        }
        return view('employee.student.addStudent', ['jawWork' => $jawWork, 'postalCode' => $postalCode, 'municipality' => $municipality, 'nationality' => $nationality, 'country' => $country, 'religion' => $religion]);
    }

    /**
     * Save Student
     * @param Request $request
     * @param JSON $response
     */
    public function store(Request $request)
    {
        $response = [];
        $input = $request->all();
        $image = $request->file('stu_PicturePath');

        if (isset($input['id']) && !empty($input['id'])) {
            if (!empty($image)) {
                $input['stu_PicturePath'] = time() . '.' . $image->getClientOriginalExtension();
                $destinationPath = storage_path('app/public/images/students');
                $image->move($destinationPath, $input['stu_PicturePath']);
                $imgData = Student::select('stu_PicturePath')->where('id', $input['id'])->first();
                if (!empty($imgData->stu_PicturePath)) {
                    $filepath = storage_path('app/public/images/students/') . "/" . $imgData->stu_PicturePath;
                    if (file_exists($filepath)) {
                        unlink($filepath);
                    }
                }
            }

            if (isset($input['stu_StudentID']) && !empty($input['stu_StudentID'])) {
                $govIdExist = Student::where('stu_StudentID', $input['stu_StudentID'])->where('id', '!=', $input['id'])->get();
                $govIdExistCount = $govIdExist->count();
            } else {
                $govIdExistCount = 0;
            }

            $emailExistCount = 0;
            $emailEmpExistCount = 0;
            $tempIdExistCount = 0;
            $emailAdmExistCount = 0;
            if (!empty($input['email'])) {
                $emailExistCount = Student::where('email', '=', $input['email'])
                    ->where('id', '!=', $input['id'])
                    ->count();
                $emailEmpExistCount = Employee::where('email', '=', $input['email'])->count();
                $emailAdmExistCount = Admin::where('email', '=', $input['email'])->count();
            }

            if (isset($input['stu_TempCitizenId']) && !empty($input['stu_TempCitizenId'])) {
                $tempIdExistCount = Student::where('stu_TempCitizenId', '=', $input['stu_TempCitizenId'])->where('stu_TempCitizenId', '!=', null)
                    ->where('id', '!=', $input['id'])->count();
            }

            if ($emailExistCount != 0 || $emailAdmExistCount != 0 || $emailEmpExistCount != 0) {
                $response['status'] = false;
                $response['message'] = trans('message.msg_email_exist');
                return response()
                    ->json($response);
            }

            if ($govIdExistCount != 0) {
                $response['status'] = false;
                $response['message'] = trans('message.msg_student_id_exist');
                return response()
                    ->json($response);
            }

            if ($tempIdExistCount != 0) {
                $response['status'] = false;
                $response['message'] = trans('message.msg_temp_citizen_id_exist');
                return response()
                    ->json($response);
            }

            if (!empty($input['id'])) {
                $input['stu_DateOfBirth'] = UtilHelper::sqlDate($input['stu_DateOfBirth']);

                $sdata = Student::where('id', $input['id'])->first();

                if (!empty($input['email']) && $sdata->email != $input['email']) {
                    $current_time = date("Y-m-d H:i:s");
                    $verification_key = md5(FrontHelper::generatePassword(20));
                    $reset_pass_token = base64_encode($input['email'] . '&&Student&&' . $current_time);

                    $data = ['email' => $input['email'], 'name' => $input['stu_StudentName'] . " " . $input['stu_StudentSurname'], 'verify_key' => $verification_key, 'reset_pass_link' => $reset_pass_token, 'subject' => 'New Student Credentials'];

                    MailHelper::sendNewCredentials($data);
                    $input['email_verified_at'] = null;
                    $input['email_verification_key'] = $verification_key;
                } else {
                    $verification_key = md5(FrontHelper::generatePassword(20));
                    $input['email_verification_key'] = $verification_key;
                    $input['email_verified_at'] = null;
                }

                if (!empty($input['stu_TempCitizenId'])) {
                    $input['stu_StudentID'] = '';
                }

                if (!empty($input['stu_StudentID'])) {
                    $input['stu_TempCitizenId'] = '';
                }

                $id = Student::where('id', $input['id'])->update($input);

            }

            if ($id) {
                $response['status'] = true;
                $response['message'] = trans('message.msg_student_update_success');
                $response['redirect'] = url('/employee/students');
            } else {
                $response['status'] = false;
                $response['message'] = trans('message.msg_something_wrong');
            }
        } else {
            $picturePath = '';
            $emailExistCount = 0;
            $emailEmpExistCount = 0;
            $govIdExistCount = 0;
            $tempIdExistCount = 0;
            $emailAdmExistCount = 0;

            if (!empty($input['email'])) {
                $emailExistCount = Student::where('email', '=', $input['email'])->count();
                $emailEmpExistCount = Employee::where('email', '=', $input['email'])->count();
                $emailAdmExistCount = Admin::where('email', '=', $input['email'])->count();
            }

            if (isset($input['stu_StudentID']) && !empty($input['stu_StudentID'])) {
                $govIdExistCount = Student::where('stu_StudentID', $input['stu_StudentID'])->count();
            }

            if (isset($input['stu_TempCitizenId']) && !empty($input['stu_TempCitizenId'])) {
                $tempIdExistCount = Student::where('stu_TempCitizenId', '=', $input['stu_TempCitizenId'])
                    ->where('stu_TempCitizenId', '!=', null)
                    ->count();
            }

            if ($emailExistCount != 0 || $emailAdmExistCount != 0 || $emailEmpExistCount != 0) {
                $response['status'] = false;
                $response['message'] = trans('message.msg_email_exist');
                return response()
                    ->json($response);
            }

            if ($govIdExistCount != 0) {
                $response['status'] = false;
                $response['message'] = trans('message.msg_student_id_exist');
                return response()
                    ->json($response);
            }

            if ($tempIdExistCount != 0) {
                $response['status'] = false;
                $response['message'] = trans('message.msg_temp_citizen_id_exist');
                return response()
                    ->json($response);
            }

            $student = new Student;
            if (!empty($image)) {
                $input['imagename'] = time() . '.' . $image->getClientOriginalExtension();
                $destinationPath = storage_path('app/public/images/students');
                $image->move($destinationPath, $input['imagename']);
                $picturePath = $input['imagename'];
            }

            $current_time = date("Y-m-d H:i:s");
            $verification_key = md5(FrontHelper::generatePassword(20));
            $reset_pass_token = base64_encode($input['email'] . '&&Student&&' . $current_time);

            $input['email_verification_key'] = $verification_key;
            $input['stu_PicturePath'] = $picturePath;
            $input['stu_DateOfBirth'] = UtilHelper::sqlDate($input['stu_DateOfBirth']);
            $student->create($input);

            if ($student) {
                if (!empty($input['email'])) {
                    $data = ['email' => $input['email'], 'name' => $input['stu_StudentName'] . " " . $input['stu_StudentSurname'], 'verify_key' => $verification_key, 'reset_pass_link' => $reset_pass_token, 'subject' => 'New Student Credentials'];

                    MailHelper::sendNewCredentials($data);
                }

                $response['status'] = true;
                $response['message'] = trans('message.msg_student_add_success');
                $response['redirect'] = url('/employee/students');
            } else {
                $response['status'] = false;
                $response['message'] = trans('message.msg_something_wrong');
            }
        }

        return response()
            ->json($response);
    }

    /**
     * Edit Student Page
     * @param  Request $request
     * @param  int $id
     * @return view - employee.student.editStudent
     */
    public function edit(Request $request, $id)
    {
        $input = $request->all();
        $mainSchool = $this->logged_user->sid;

        $municipality = Municipality::with([
            'canton.state.country' => function ($q) {
                $q->select('pkCny', 'cny_CountryName_' . $this->current_language . ' as cny_CountryName');
            },
        ])->select('pkMun', 'fkMunCan', 'mun_MunicipalityName_' . $this->current_language . ' as mun_MunicipalityName')->get();

        $nationality = Nationality::select('pkNat', 'nat_NationalityName_' . $this->current_language . ' as nat_NationalityName')
            ->get();
        $country = Country::select('pkCny', 'cny_CountryName_' . $this->current_language . ' as cny_CountryName')
            ->get();
        $religion = Religion::select('pkRel', 'rel_ReligionName_' . $this->current_language . ' as rel_ReligionName')
            ->get();

        $params = ['language'=>$this->current_language,'fkVscSch'=>$mainSchool];
        $village_schools = VillageSchool::GetVillageSchoolList($params)
        ->pluck('vsc_VillageSchoolName','pkVsc')
        ->prepend(trans('general.gn_select'), '');

        $postalCode = PostalCode::with([
            'municipality' => function ($q) {
                $q->select('pkMun', 'fkMunCan', 'mun_MunicipalityName_' . $this->current_language . ' as mun_MunicipalityName');
            },
            'municipality.canton.state.country' => function ($q) {
                $q->select('pkCny', 'cny_CountryName_' . $this->current_language . ' as cny_CountryName');
            },
        ])->select('pkPof', 'fkPofMun', 'pof_PostOfficeNumber', 'pof_PostOfficeName_' . $this->current_language . ' as pof_PostOfficeName')
            ->get();

        $jawWork = JobAndWork::select('pkJaw', 'jaw_Name_' . $this->current_language . ' as jaw_Name')
            ->get();
        $mainBooks = MainBook::select('pkMbo', 'mbo_Uid', 'mbo_MainBookNameRoman')->where('fkMboSch', $mainSchool)->get();
        $schoolYear = SchoolYear::select('pkSye', 'sye_NameNumeric', 'sye_NameCharacter_' . $this->current_language . ' AS sye_NameCharacter')->get();

        $sclEduPlnAssinment = SchoolEducationPlanAssignment::where('fkSepSch', $mainSchool)->where('sep_Status', 'Active')
            ->get()
            ->toArray();
        foreach ($sclEduPlnAssinment as $key => $value) {
            $eduProg[] = $value['fkSepEdp'];
            $eduPlan[] = $value['fkSepEpl'];
        }
        $educationProg = EducationProgram::select('pkEdp', 'edp_Name_' . $this->current_language . ' as edp_Name')
            ->whereIn('pkEdp', $eduProg)->get();

        //Get Education Education Program from  SchoolEducationPlanAssignment
        if (isset($input['fkSteEdp']) && $input['fkSteEdp']) {
            $sclEduPlnAssinmen = SchoolEducationPlanAssignment::where('fkSepSch', $mainSchool)->where('fkSepEdp', $input['fkSteEdp'])->where('sep_Status', 'Active')
                ->get()
                ->toArray();
            foreach ($sclEduPlnAssinmen as $key => $value) {
                $eduPlan1[] = $value['fkSepEpl'];
            }

            $educationPlans = EducationPlan::select('pkEpl', 'epl_EducationPlanName_' . $this->current_language . ' as epl_EducationPlanName')
                ->whereIn('pkEpl', $eduPlan1)->get()
                ->toArray();

            return response()
                ->json(['status' => true, 'educationPlans' => $educationPlans]);
        }

        \DB::statement("SET SQL_MODE=''");
        $aStudentData = Student::with(['enrollStudent' => function ($q) use ($mainSchool) {
                $q->where('fkSteSch', $mainSchool);
            }
            , 'enrollStudent.school' => function ($q){
                $q->select('pkSch','sch_SchoolName_'.$this->current_language.' AS sch_SchoolName');
            }
            , 'enrollStudent.villageschool' => function ($q){
                $q->select('pkVsc','vsc_VillageSchoolName_'.$this->current_language.' AS vsc_VillageSchoolName');
            }
            , 'enrollStudent.grade' => function ($q) {
                $q->select('pkGra', 'gra_GradeNameRoman', 'gra_GradeNumeric');
            }
            , 'enrollStudent.schoolYear' => function ($q) {
                $q->select('pkSye', 'sye_NameNumeric', 'sye_NameCharacter_' . $this->current_language . ' AS sye_NameCharacter');
            }
            , 'enrollStudent.educationProgram' => function ($q) {
                $q->select('pkEdp', 'edp_Name_' . $this->current_language . ' as edp_Name');
            }
            , 'enrollStudent.educationProgram.educationPlan' => function ($q) {
                $q->select('*', 'pkEpl', 'epl_EducationPlanName_' . $this->current_language . ' as epl_EducationPlanName');
            }
            , 'enrollStudent.educationProgram.schoolEducationPlanAssignment' => function ($q) use ($mainSchool) {
                $q->where('fkSepSch', $mainSchool);
            }
            , 'enrollStudent.educationProgram.schoolEducationPlanAssignment.educationPlan' => function ($q) {
                $q->select('*', 'pkEpl', 'epl_EducationPlanName_' . $this->current_language . ' as epl_EducationPlanName');
            }
            , 'enrollStudent.educationPlan' => function ($q) {
                $q->select('pkEpl', 'fkEplEdp', 'epl_EducationPlanName_' . $this->current_language . ' as epl_EducationPlanName');
            }
        ])->where('id', $id)->first();

        $allGrades = Grade::all();

        return view('employee.student.editStudent', ['aStudentData' => $aStudentData,
         'jawWork' => $jawWork,
         'postalCode' => $postalCode,
         'municipality' => $municipality,
         'nationality' => $nationality,
         'country' => $country,
         'religion' => $religion,
         'mainBooks' => $mainBooks,
         'schoolYear' => $schoolYear,
         'educationProg' => $educationProg,
         'allGrades' => $allGrades,
         'village_schools' => $village_schools,
         'school_id' => $mainSchool]);
    }

    /**
     * View Student Page
     * @param  Request $request
     * @param  int  $id
     * @return view - employee.student.viewStudent
     */
    public function show(Request $request, $id)
    {
        $mainSchool = $this
            ->logged_user->sid;

        $aStudentData = Student::with(['country' => function ($q) {
            $q->select('pkCny', 'cny_CountryName_' . $this->current_language . ' as cny_CountryName');
        }
            , 'postalCode' => function ($q) {
                $q->select('pkPof', 'fkPofMun', 'pof_PostOfficeNumber', 'pof_PostOfficeName_' . $this->current_language . ' as pof_PostOfficeName');
            }
            , 'municipality' => function ($q) {
                $q->select('pkMun', 'mun_MunicipalityName_' . $this->current_language . ' as mun_MunicipalityName');
            }
            , 'nationality' => function ($q) {
                $q->select('pkNat', 'nat_NationalityName_' . $this->current_language . ' as nat_NationalityName');
            }
            , 'riligeion' => function ($q) {
                $q->select('pkRel', 'rel_ReligionName_' . $this->current_language . ' as rel_ReligionName');
            }
            , 'enrollStudent' => function ($q) use ($mainSchool) {
                $q->where('fkSteSch', $mainSchool);
            }
            , 'enrollStudent.grade' => function ($q) {
                $q->select('pkGra', 'gra_GradeNameRoman', 'gra_GradeNumeric');
            }
            , 'enrollStudent.school' => function ($q){
                $q->select('pkSch','sch_SchoolName_'.$this->current_language.' AS sch_SchoolName');
            }
            , 'enrollStudent.villageschool' => function ($q){
                $q->select('pkVsc','vsc_VillageSchoolName_'.$this->current_language.' AS vsc_VillageSchoolName');
            }
            , 'enrollStudent.schoolYear' => function ($q) {
                $q->select('pkSye', 'sye_NameNumeric', 'sye_NameCharacter_' . $this->current_language . ' AS sye_NameCharacter');
            }
            , 'enrollStudent.educationProgram' => function ($q) {
                $q->select('pkEdp', 'edp_Name_' . $this->current_language . ' as edp_Name');
            }
            , 'enrollStudent.educationPlan' => function ($q) {
                $q->select('pkEpl', 'epl_EducationPlanName_' . $this->current_language . ' as epl_EducationPlanName');
            }
            ,
        ])->where('id', $id)->first();

        if (request()
            ->ajax()) {
            return \View::make('employee.student.viewStudent')
                ->with(['mdata' => $aStudentData])->renderSections();
        }

        return view('employee.student.viewStudent', ['mdata' => $aStudentData]);
    }

    /**
     * Edit/Add Student Enrollment
     * @param  Request $request
     * @return JSON $response
     */
    public function editStudentEnroll(Request $request)
    {
        $input = $request->all();
        $response = [];

        $tdata = $input['total_details'];
        // $pkSte = explode(',', $input['pkSte']);
        $oldEng = explode(',', $input['oldEng']);

        $mainSchool = $this
            ->logged_user->sid;

        $classStudents = ClassStudentsSemester::whereIn('fkSemSen', $oldEng)->get()
            ->count();

        if ($classStudents != 0) {
            EnrollStudent::whereIn('pkSte', $oldEng)->delete();
        }

        for ($i = 1; $i <= $tdata; $i++) {
            $details = [];
            $finishDate = null;
            $expellDate = null;
            $breakDate = null;
            if (!empty($input['ste_FinishingDate_' . $i])) {
                $finishDate = UtilHelper::sqlDate($input['ste_FinishingDate_' . $i]);
            }
            if (!empty($input['ste_ExpellingDate' . $i])) {
                $expellDate = UtilHelper::sqlDate($input['ste_ExpellingDate' . $i]);
            }
            if (!empty($input['ste_BreakingDate' . $i])) {
                $breakDate = UtilHelper::sqlDate($input['ste_BreakingDate' . $i]);
            }
            $details = ['fkSteStu' => $input['fkSteStu'],
             'fkSteSch' => $mainSchool,
             'ste_EnrollmentDate' => UtilHelper::sqlDate($input['start_date_' . $i]),
             'fkSteMbo' => $input['fkSteMbo_' . $i],
             'fkSteGra' => $input['fkSteGra_' . $i],
             'fkSteViSch' => $input['fkSteViSch_' . $i],
             'fkSteEdp' => $input['fkSteEdp_' . $i],
             'fkSteEpl' => $input['fkSteEpl_' . $i],
             'fkSteSye' => $input['fkSteSye_' . $i],
             'ste_EnrollBasedOn' => $input['ste_EnrollBasedOn_' . $i],
             'ste_MainBookOrderNumber' => $input['ste_MainBook_' . $i],
             'ste_Reason' => $input['ste_Reason_' . $i],
             'ste_FinishingDate' => $finishDate,
             'ste_BreakingDate' => $breakDate,
             'ste_ExpellingDate' => $expellDate];

            if ($input['pkSte_'.$i] == 'new') {

                $result = EnrollStudent::insertGetId($details);
                if (!empty($result)) {
                    $previousGrade = EnrollStudent::select('pkSte')->where('fkSteStu', $input['fkSteStu'])->where('fkSteSch', $mainSchool)->where('pkSte', '!=', $result)->orderBy('pkSte', 'DESC')
                        ->first();

                    if (!empty($previousGrade)) {
                        $previousGrade->ste_EnrollmentFinishDate = date('Y-m-d h:i:s');
                        $previousGrade->save();
                    }
                }
            } else {
                EnrollStudent::where('pkSte', $input['pkSte_'.$i])->update($details);
            }
        }

        $response['status'] = true;
        $response['message'] = trans('message.msg_enroll_update_success');

        return response()
            ->json($response);
    }

    /**
     * Fetch Students data
     * @param  Request $request
     * @return JSON $result
     */
    public function student_list(Request $request)
    {
        $data = $request->all();

        $perpage = !empty($data['length']) ? (int) $data['length'] : 10;

        $filter = isset($data['search']) && is_string($data['search']) ? $data['search'] : '';

        $sort_type = isset($data['order'][0]['dir']) && is_string($data['order'][0]['dir']) ? $data['order'][0]['dir'] : '';
        $sort_col = isset($data['order'][0]['column']) ? $data['order'][0]['column'] : '';

        $sort_field = isset($data['columns'][$sort_col]['data']) ? $data['columns'][$sort_col]['data'] : '';

        $mainSchool = $this
            ->logged_user->sid;

        $student = Student::whereHas('EnrollStudent', function ($q) use ($filter, $mainSchool) {
            $q->where('fkSteSch', $mainSchool);
        });

        if ($filter) {
            $student = $student->where(function ($query) use ($filter) {
                $query->where('stu_StudentID', 'LIKE', '%' . $filter . '%')->orWhere('stu_StudentName', 'LIKE', '%' . $filter . '%')->orWhere('stu_StudentSurname', 'LIKE', '%' . $filter . '%')->orWhere('stu_TempCitizenId', 'LIKE', '%' . $filter . '%');
            });
        }
        $studentQuery = $student;

        if ($sort_col != 0) {
            $studentQuery = $studentQuery->orderBy($sort_field, $sort_type);
        }

        $total_students = $studentQuery->count();

        $offset = $data['start'];

        $counter = $offset;
        $studentdata = [];
        $students = $studentQuery->offset($offset)->limit($perpage)->get()
            ->toArray();

        foreach ($students as $key => $value) {
            $value['index'] = $counter + 1;
            if ($value['stu_StudentGender'] == 'Male') {
                $value['stu_StudentGender'] = trans('general.gn_male');
            } else {
                $value['stu_StudentGender'] = trans('general.gn_female');
            }
            $studentdata[$counter] = $value;
            $counter++;
        }

        $price = array_column($studentdata, 'index');

        if ($sort_col == 0) {
            if ($sort_type == 'desc') {
                array_multisort($price, SORT_DESC, $studentdata);
            } else {
                array_multisort($price, SORT_ASC, $studentdata);
            }
        }
        $studentdata = array_values($studentdata);
        $result = array(
            "draw" => $data['draw'],
            "recordsTotal" => $total_students,
            "recordsFiltered" => $total_students,
            "data" => $studentdata,
        );

        return response()->json($result);
    }

    /**
     * Enroll Student Page
     * @param  Request $request
     * @return JSON
     */
    public function enrollStudents(Request $request)
    {
        $educationPro = [];
        $eduPlan = [];
        $educationPrograms = '';
        $grades = [];
        $input = $request->all();
        //Get School ID
        $schoolDetail = Employee::with('EmployeesEngagement', 'EmployeesEngagement.employeeType')->whereHas('EmployeesEngagement', function ($query) {
            $query->whereHas('employeeType', function ($query) {
                $query->where('epty_Name', 'SchoolCoordinator')
                    ->orWhere('epty_Name', 'SchoolSubAdmin');
            })
                ->where('fkEenEmp', $this
                        ->logged_user
                        ->id)->where(function ($query) {
                $query->where('een_DateOfFinishEngagement', '=', null)
                    ->orWhere('een_DateOfFinishEngagement', '>=', now());
            });
        })
            ->first();
        $mainSchool = $schoolDetail->EmployeesEngagement[0]->fkEenSch;

        //Get Education Data
        $sclEduPlnAssinment = SchoolEducationPlanAssignment::where('fkSepSch', $mainSchool)->where('sep_Status', 'Active')
            ->get()
            ->toArray();
        foreach ($sclEduPlnAssinment as $key => $value) {
            $eduProg[] = $value['fkSepEdp'];
            $eduPlan[] = $value['fkSepEpl'];
        }
        $educationProg = EducationProgram::select('pkEdp', 'edp_Uid', 'edp_Name_' . $this->current_language . ' as edp_Name')
            ->whereIn('pkEdp', $eduProg)->get();

        //Get Education Education Program from  SchoolEducationPlanAssignment
        if (isset($input['fkSteEdp']) && $input['fkSteEdp']) {
            $sclEduPlnAssinmen = SchoolEducationPlanAssignment::where('fkSepSch', $mainSchool)->where('fkSepEdp', $input['fkSteEdp'])->where('sep_Status', 'Active')
                ->get()
                ->toArray();
            foreach ($sclEduPlnAssinmen as $key => $value) {
                $eduPlan1[] = $value['fkSepEpl'];
            }

            $educationPlans = EducationPlan::select('pkEpl', 'epl_EducationPlanName_' . $this->current_language . ' as epl_EducationPlanName')
                ->whereIn('pkEpl', $eduPlan1)->get()
                ->toArray();

            return response()
                ->json(['status' => true, 'educationPlans' => $educationPlans]);
        }
        //Get Grade List From Education plan
        if (isset($input['fkSteEpl']) && $input['fkSteEpl']) {

            $gradIdeduPlan = EducationPlan::with(['mandatoryCourse'])->where('pkEpl', $input['fkSteEpl'])->first();

            $gradesArr = [];
            foreach ($gradIdeduPlan->mandatoryCourse as $k => $v) {
                $tmpGrade[] = $v->fkEmcGra;
            }
            $gradesArr = array_unique($tmpGrade);

            $grades = Grade::select('pkGra', 'gra_Uid', 'gra_GradeNumeric')
                ->whereIn('pkGra', $gradesArr)->get()
                ->toArray();
            return response()
                ->json(['status' => true, 'grades' => $grades]);
        }
    }

    /**
     * Used for checking student Govt or temp Id
     * @param  Request $request
     * @return JSON $response
     */
    public function checkStuId(Request $request)
    {
        $response = [];
        $input = $request->all();

        if (!empty($input['id'])) {
            $student = Student::where(function ($query) use ($input) {
                $query->where('stu_StudentID', $input['stuId'])->orWhere('stu_TempCitizenId', $input['stuId']);
            })->where('id', '!=', $input['id'])->count();
        } else {
            $student = Student::where(function ($query) use ($input) {
                $query->where('stu_StudentID', $input['stuId'])->orWhere('stu_TempCitizenId', $input['stuId']);
            })->count();
        }

        if ($student != 0) {
            $response['status'] = false;
            $response['message'] = trans('message.msg_student_id_exist');
        } else {
            $response['status'] = true;
        }

        return response()->json($response);
    }

}
