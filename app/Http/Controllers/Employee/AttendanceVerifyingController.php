<?php

namespace App\Http\Controllers\Employee;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\{DailyBook,DailyBookLecture,DailyStudentAttendance};
use App\Http\Services\AttendanceAccomplishmentService;
use App\Http\Services\ClassWorkingWeekService;
use DataTableService;
use Datatables;
use HtmlBuilder;
use UtilHelper;
use Session;
use HttpResponse;

class AttendanceVerifyingController extends Controller
{
    public function index(HtmlBuilder $builder)
    {
        $builder_data['columns'] = [
            ['data' => 'DT_RowIndex', 'name' => 'DT_RowIndex', 'title' => trans('general.sr_no'), 'render' => null, 'orderable' => false, 'searchable' => false],
            ['data' => 'shi_ShiftName', 'name' => 'shi_ShiftName', 'title' => trans('general.gn_shift'),'orderable' => false, 'searchable' => false],
            ['data' => 'dbk_Date', 'name' => 'dbk_Date', 'title' => trans('general.gn_date')],
            ['data' => 'vsc_VillageSchoolName', 'name' => 'vsc_VillageSchoolName', 'title' => trans('sidebar.sidebar_nav_village_schools'),'orderable' => false, 'searchable' => false],
            ['data' => 'dbk_Verified', 'name' => 'dbk_Verified', 'title' => trans('general.gn_verified')." ".trans('general.gn_status')],
            ['data' => 'action', 'name' => 'action', 'title' => trans('general.gn_actions'), 'orderable' => false, 'searchable' => false, 'width' => '10%'],
        ];

        $builder_data['ajax'] = [
            'url'=> route('attendance-verifying.list'),
            'data' => 'function(d) {
                d.search =  $("#search_state").val();
                d.country_filter = $("#country_filter").val();
            }'
        ];

        $dt_html = new DataTableService($builder_data, $builder);
        $dt_html = $dt_html->builder;

        return view('employee.attendanceVerifying.index',compact('dt_html'));
    }

    public function store(Request $request)
    {
        if(count($request->sae_StudentHTDesc) > 0)
        {
            foreach($request->sae_StudentHTDesc as $key => $value){
                if(isset($request->attendanceLecturestatus[$key]))
                {
                    $attendance = DailyStudentAttendance::find($key);
                    $attendance->sae_LecutreStudentStatus = $request->attendanceLecturestatus[$key];
                    $attendance->sae_StudentHTDesc = $value;
                    $attendance->save();
                }

                $response['message'] = trans('message.msg_attendance_verified_sucessfully');
            }
            
            if($request->verified_status != 'false'){
                $dbk = DailyBook::find($request->dailybook_id);
                $dbk->dbk_Verified = 'YES';
                $dbk->save();
            }
        }
        else
        {
            $status_code = HttpResponse::HTTP_BAD_REQUEST;
            $response['message'] = trans('message.msg_something_wrong');
        }

        return response()->json($response,$status_code ?? HttpResponse::HTTP_OK);
    }

    public function edit($id)
    {
        $dailyBookLecture = DailyBookLecture::where('fkDblDbk',$id)->orderBy('dbl_LectureNumber','ASC')->get();
        $count = $dailyBookLecture->count();
        return view('employee.attendanceVerifying.edit',compact('dailyBookLecture','count','id'));
    }

    public function attendance_class_detail()
    {
        $id = request('pkDbl');
        $language = $this->current_language;
        $dailyBookLectureData = (new AttendanceAccomplishmentService())->getTeacherClassDetails($id, $language);
        extract($dailyBookLectureData);

        $lecture_details = $class_details->pluck('lecture_details')->toArray();
        $attendance_details = $class_details->pluck('attendance_details')->toArray();
        
        $date = UtilHelper::sqlDate($basic_details['date'],2);
        $week_params = ['language'=>$language,
		            'shift_id'=>$basic_details['shift_id'],
		            'class_sem_id'=>$basic_details['class_sem_id'],
		            'date'=>$date];
	    $working_weeks_details = (new ClassWorkingWeekService())->getOnDutyStudents($week_params);

        $response['status'] = true;
        $html = \View::make('employee.attendanceVerifying.attendance_verifying_template')->with(compact('id','lecture_details','basic_details','attendance_details','working_weeks_details'))->renderSections();
        $response['data'] = $html['attendance_verify'];
        
        return response()->json($response);
    }

    public function destroy($id)
    {
        $response = [];
        if (empty($id)) {
            $status_code = HttpResponse::HTTP_BAD_REQUEST;
        } else {
            DailyBook::where('pkDbk', $id)->delete();
            $response['message'] = trans('message.msg_daily_book_delete_success');
        }

        return response()->json($response,$status_code ?? HttpResponse::HTTP_OK);
    }

    public function attendance_listing()
    {
        $current_homeroom_teacher_class = Session::get('curr_course_class');
        $dailybook = DailyBook::with([
            'villageSchool' => function ($query){
                $query->select('pkVsc', 'vsc_VillageSchoolName_' . $this->current_language . ' as vsc_VillageSchoolName');
            },
            'shift' => function ($query){
                $query->select('pkShi', 'shi_ShiftName_' . $this->current_language . ' as shi_ShiftName');
            },
        ])->where('fkDbkCcs',$current_homeroom_teacher_class);

        $dailybook->when(empty(array_first(request('order'))['column']), function($q){
            return $q->orderBy('pkDbk','DESC');
        });

        $dailybook->when(request('search'), function ($q){
            return $q->orWhere('dbk_Verified', 'LIKE', '%' . request('search') . '%')
            ->orWhere('dbk_Date','LIKE', '%'. request('search') .'%');
        });

        return DataTables::of($dailybook)
            ->editColumn('vsc_VillageSchoolName', function($dailybook){
                return $dailybook->villageSchool->vsc_VillageSchoolName ?? '-';
            })
            ->editColumn('shi_ShiftName', function($dailybook){
                return $dailybook->shift->shi_ShiftName;
            })
            ->editColumn('dbk_Verified', function($dailybook){
                if ($dailybook->dbk_Verified == 'YES') {
                    return trans('general.gn_yes');
                } else {
                    return trans('general.gn_no');
                }
            })
            ->addColumn('action', function ($dailybook) {
                $str = '<a href='.url('/employee/attendance-verifying/'.$dailybook->pkDbk.'/edit').'><i class="fa fa-pen"></i></a>&nbsp;&nbsp';
                $str .= '<a onclick="triggerDelete('.$dailybook->pkDbk.')" href="javascript:void(0)"><i class="fa fa-trash"></i></a>';
                return $str;
            })
            ->addIndexColumn()
            ->escapeColumns()
            ->toJSON();
    }

    public function delete_attendance_lecture()
    {
        $id = request('pkDbl');
        $response = [];
        if (empty($id)) {
            $status_code = HttpResponse::HTTP_BAD_REQUEST;
        } else {
            DailyBookLecture::where('pkDbl', $id)->delete();
            $response['message'] = trans('message.msg_daily_book_lecture_delete_success');
        }

        return response()->json($response,$status_code ?? HttpResponse::HTTP_OK);
    }
}
