<?php
/**
 * CloneSemesterController
 *
 * @package    Laravel
 * @subpackage Controller
 * @since      1.0
 */

namespace App\Http\Controllers\Employee;

use App\Http\Controllers\Controller;
use App\Http\Traits\CommonTrait;
use App\Models\ClassCreation;
use App\Models\ClassCreationSemester;
use App\Models\ClassStudentsAllocation;
use App\Models\CourseGroupAllocation;
use App\Models\HomeRoomTeacher;
use Illuminate\Http\Request;

class CloneSemesterController extends Controller
{
    use CommonTrait;

    /**
     * Clone Semester for copy all data from one semester to other semester of all students for particular school.
     * Clone in this table: classCreationSemester,classStudentsSemester,classStudentsAllocation,classCreationTeachers,Homeroomteacher,CourseGroupAllocation
     * @param \Illuminate\Http\Request $request
     * @return JSON $response
     */
    public function store(Request $request)
    {
        $input = $request->all();
        $mainSchool = $this
            ->logged_user->sid;
        $currentSchoolYear = $this->getCurrentYear();
        $schoolYear = $currentSchoolYear->pkSye;
        $currentSem = $input['currentSemesterId'];
        $nextSem = $input['nextSemesterId'];
        if (!empty($currentSem) && !empty($nextSem) && !empty($schoolYear) && !empty($mainSchool)) {
            $currentSchoolClasses = ClassCreation::with([
            'classCreationSemester' => function ($q) use ($currentSem) {
                $q->where('fkCcsEdp', $currentSem);
            },
            'classCreationSemester.classStudentsSemester',
            'classCreationSemester.classStudentsSemester.classStudentsAllocation',
            'classCreationSemester.classStudentsSemester.classStudentsAllocation.classCreationTeachers' => function ($q) {
                $q->whereNull('Ctc_end');
            },
            ])
            ->where('fkClrSch', $mainSchool)
            ->where('fkClrSye', $schoolYear)
            ->where('clr_Status', 'Publish')
            ->get();

            $pkCsas = array();
            $pkCcss = array();
            if (!empty($currentSchoolClasses)) {
                foreach ($currentSchoolClasses as $key => $value) {
                    $status = $this->checkAlreadyExistSemester($nextSem, $value->pkClr);
                    if ($status && !empty($value->classCreationSemester)) {
                        foreach ($value->classCreationSemester as $kccs => $vccs) {
                            //Clone for Class Semester entries
                            $oldpkCcs = $vccs->pkCcs;
                            $classCreationSemester = $vccs->replicate();
                            $classCreationSemester->fkCcsEdp = $nextSem;
                            $classCreationSemester->save();

                            if ($classCreationSemester->save()) {
                                $newfkSemCcs = $classCreationSemester->pkCcs;
                                $pkCcss[$oldpkCcs] = $newfkSemCcs;
                                if (!empty($vccs->ClassStudentsSemester)) {
                                    foreach ($vccs->ClassStudentsSemester as $kcss => $vcss) {
                                        //Clone for Class Students Semester entries
                                        $ClassStudentsSemester = $vcss->replicate();
                                        $ClassStudentsSemester->fkSemCcs = $newfkSemCcs;
                                        $ClassStudentsSemester->sem_GeneralSuccess = null;
                                        $ClassStudentsSemester->sem_AvgGeneralSuccess = null;
                                        $ClassStudentsSemester->sem_UnjustifiedAbsenceHours = null;
                                        $ClassStudentsSemester->sem_JustifiedAbsenceHours = null;
                                        $ClassStudentsSemester->fkSemGan = null;
                                        $ClassStudentsSemester->fkCcaDfm = null;
                                        $ClassStudentsSemester->sem_generalSuccessDescriptiveFinalmarks = null;
                                        $ClassStudentsSemester->save();

                                        if ($ClassStudentsSemester->save()) {
                                            $newfkCsaSem = $ClassStudentsSemester->pkSem;
                                            if (!empty($vcss->ClassStudentsAllocation)) {
                                                foreach ($vcss->ClassStudentsAllocation as $kcsca => $vcsca) {
                                                    //Clone for Class Students Course Allocation entries
                                                    $ClassStudentsAllocation = $vcsca->replicate();
                                                    $ClassStudentsAllocation->fkCsaSem = $newfkCsaSem;
                                                    $ClassStudentsAllocation->csa_FinalMarks = null;
                                                    $ClassStudentsAllocation->csa_AveragePeriodic = null;
                                                    $ClassStudentsAllocation->fkCsaDfm = null;
                                                    $ClassStudentsAllocation->csa_ManualDescriptMarks = null;
                                                    $ClassStudentsAllocation->save();

                                                    if ($ClassStudentsAllocation->save()) {
                                                        $newfkCtcCsa = $ClassStudentsAllocation->pkCsa;
                                                        $pkCsas[] = $newfkCtcCsa;

                                                        if (!empty($vcsca->classCreationTeachers)) {
                                                            foreach ($vcsca->classCreationTeachers as $kctca => $vctca) {
                                                                //Clone for Class Teachers Course Allocation entries
                                                                $classCreationTeachers = $vctca->replicate();
                                                                $classCreationTeachers->fkCtcCsa = $newfkCtcCsa;
                                                                $classCreationTeachers->fkCtcCcs = $newfkSemCcs;
                                                                $classCreationTeachers->Ctc_start = date("Y-m-d h:i:s");
                                                                $classCreationTeachers->save();
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }

                                    }
                                }
                            }
                        }
                    }

                }

                if (!empty($pkCcss)) {
                    $this->createHomeroomTeacherClone($pkCcss);
                }

                if (!empty($pkCsas)) {
                    $this->createOptionalCourseGroupClone($pkCsas, $nextSem);
                }

                $response['status'] = true;
                $response['message'] = "Semester cloned successfully.";
            } else {
                $response['status'] = false;
                $response['message'] = "Class not found!";
            }
        } else {
            $response['status'] = false;
            $response['message'] = trans('message.msg_something_wrong');
        }
        return response()
            ->json($response);
    }

    /**
     * If already exist clone semester entry then return false. It will help to prevent duplicate entries.
     *
     * @param [int] $nextSem
     * @param [int] $pkClr
     * @return [boolean] $status
     */
    public function checkAlreadyExistSemester($nextSem, $pkClr)
    {
        $status = true;
        $count = ClassCreationSemester::where('fkCcsEdp', $nextSem)->where('fkCcsClr', $pkClr)->count();
        if ($count > 0) {
            $status = false;
        }

        return $status;
    }

    /**
     * Create homeroomteacher for new cloned semester
     *
     * @param [array] $pkCcss
     * @return void
     */
    public function createHomeroomTeacherClone($pkCcss)
    {
        foreach ($pkCcss as $key => $val) {
            $hrt = HomeRoomTeacher::where('fkHrtCcs', $key)->orderBy('pkHrt', 'DESC')
                ->first();
            if (!empty($hrt)) {
                $hrt = $hrt->replicate();
                $hrt->fkHrtCcs = $val;
                $hrt->save();
            }
        }
    }

    /**
     * Create/Clone Optional Course group and update new id in existing studentcourseallocation table
     *
     * @param [array] $pkCsas
     * @param [int] $nextSem
     * @return void
     */
    public function createOptionalCourseGroupClone($pkCsas, $nextSem)
    {
        if (!empty($pkCsas)) {
            $latestPks = array();
            $classStudentsCourses = ClassStudentsAllocation::select('fkCsaCga')->distinct()
                ->whereIn('pkCsa', $pkCsas)->whereNotNull('fkCsaCga')
                ->pluck('fkCsaCga');
            if (!empty($classStudentsCourses)) {
                for ($i = 0; $i < count($classStudentsCourses); $i++) {
                    //Create clone course group from existing groups
                    $courseGroups = CourseGroupAllocation::find($classStudentsCourses[$i]);
                    $courseGroups = $courseGroups->replicate();
                    $courseGroups->fkCgaEdu = $nextSem;
                    $courseGroups->save();

                    if ($courseGroups->save()) {
                        $newpkCga = $courseGroups->pkCga;
                        $latestPks[$classStudentsCourses[$i]] = $newpkCga;
                    }
                }

                if (!empty($latestPks)) {
                    foreach ($latestPks as $key => $val) {
                        $classStudentCourse = ClassStudentsAllocation::where('fkCsaCga', $key)->whereIn('pkCsa', $pkCsas)->get();
                        if (!empty($classStudentCourse)) {
                            //update new coursegroup id to cloned/new studentcourse entries
                            foreach ($classStudentCourse as $kcs => $vcs) {
                                $vcs->fkCsaCga = $val;
                                $vcs->save();
                            }
                        }
                    }
                }
            }
        }
    }

}
