<?php
/**
 * UserController
 *
 * This file is used for Login, Fogot Password and profile
 *
 * @package    Laravel
 * @subpackage Controller
 * @since      1.0
 */

namespace App\Http\Controllers;

use App\Helpers\FrontHelper;
use App\Helpers\MailHelper;
use App\Http\Controllers\Controller;
use App\Jobs\SendEmail;
use App\Mail\ForgotPassword;
use App\Models\Admin;
use App\Models\Employee;
use App\Models\Student;
use App\User;
use Auth;
use Hash;
use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Session;
use Validator;

class UserController extends Controller
{
    /**
     * Used for Admin Login
     * @return redirect to Login
     */
    public function index(Request $request)
    {
        return view('login.login');
    }

    /**
     * Used for Login Checking
     */
    public function loginPost(Request $request)
    {
        $input = $request->all();
        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
            'password' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect('/login')
                ->withErrors($validator)
                ->withInput();
        }
        $emailCount = Admin::where('email', '=', $input['email'])->count();

        $EmpEmailCount = Employee::where('email', '=', $input['email'])->count();

        $StuEmailCount = Student::where('email', '=', $input['email'])->count();

        $noEmailFound = true;

        if ($emailCount != 0) {
            $userData = Admin::where('email', '=', $input['email'])->first();
            $Name = $userData->adm_Name;
            $noEmailFound = false;
        } elseif ($EmpEmailCount != 0) {
            $noEmailFound = false;
            $userData = Employee::where('email', '=', $input['email'])->first();
            $Name = $userData->emp_EmployeeName . " " . $userData->emp_EmployeeSurname;
        } elseif ($StuEmailCount != 0) {
            $noEmailFound = false;
            $userData = Student::where('email', '=', $input['email'])->first();
            $Name = $userData->stu_StudentName . " " . $userData->stu_StudentSurname;
        }

        if ($noEmailFound) {
            $message = trans('message.msg_no_email_found');
            return redirect('login')->withErrors([$message]);
        }

        if ($userData->email_verified_at == null) {
            $verification_key = md5(FrontHelper::generatePassword(20));
            $userData->email_verification_key = $verification_key;
            $userData->save();
            $data = ['email' => $userData->email, 'name' => $Name, 'verify_key' => $verification_key, 'subject' => 'Hertronic Email Verification'];

            MailHelper::sendEmailVerification($data);
            $message = trans('message.msg_email_not_verified');
            return redirect('login')
                ->withErrors([$message]);
        }

        if ($userData->adm_Status == 'Inactive' || $userData->emp_Status == 'Inactive') {
            $message = trans('message.msg_account_inactive');
            return redirect('login')
                ->withErrors([$message]);
        }

        $remember = (isset($input['remember_me']) && isset($input['remember_me']) != '') ? 1 : 0;

        if ($userData->type == 'HertronicAdmin' || $userData->type == 'MinistryAdmin' || $userData->type == 'MinistrySubAdmin') {
            $guard = 'admin';
        } else {
            if ($EmpEmailCount != 0) {
                $guard = 'employee';
            } elseif ($StuEmailCount != 0) {
                $guard = 'student';
            }
        }

        if (!empty($guard)) {
            if (Auth::guard($guard)->attempt(['email' => $input['email'], 'password' => $input['password']], $remember)) {

                if ($EmpEmailCount != 0) {

                    $employeeRoles = Employee::with(['EmployeesEngagement.school',
                        'EmployeesEngagement' => function ($query) {
                            $query->where('een_DateOfFinishEngagement', '=', null);
                        },
                    ]);
                    $employeeRoles = $employeeRoles->where('id', '=', $userData->id)->first();

                    Session::put('curr_emp_eid', $employeeRoles->EmployeesEngagement[0]->pkEen);
                    Session::put('curr_emp_sid', $employeeRoles->EmployeesEngagement[0]->school->pkSch);
                }

                return redirect($guard . "/dashboard");
            } else {
                $message = trans('validation.msg_invalid_password');
                return redirect('login')
                    ->withErrors([$message]);
            }
        }
    }

    /**
     * Used for Forgot Password Page
     * @return redirect to Admin->Forgot Password page
     */
    public function forgotPass(Request $request)
    {
        return view('login.forgot');
    }

    /**
     * Used for Forgot Password Check
     * @return redirect to Admin->Forgot Password Check
     */
    public function forgotPasswordPost(Request $request)
    {
        $input = $request->all();
        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
        ]);

        if ($validator->fails()) {
            return redirect('/admin/forgot-pass')
                ->withErrors($validator)
                ->withInput();
        }

        $user = Admin::where('email', '=', $input['email'])->first();
        $uType = 'Admin';
        if (empty($user)) {
            $user = Employee::where('email', '=', $input['email'])->first();
            $uType = 'Employee';
        }
        if (empty($user)) {
            $user = Student::where('email', '=', $input['email'])->first();
            $uType = 'Student';
        }

        if (empty($user)) {
            $message = trans('message.msg_forgot_pass_fail');
            return redirect('/forgot-pass')
                ->withErrors([$message])
                ->withInput();
        } else {
            if ($uType == 'Admin') {
                $name = $user->adm_Name;
            }
            if ($uType == 'Employee') {
                $name = $user->emp_EmployeeName . " " . $user->emp_EmployeeSurname;
            }
            if ($uType == 'Student') {
                $name = $user->stu_StudentName . " " . $user->stu_StudentSurname;
            }

            $pass = FrontHelper::generatePassword(10);
            $new_pass = Hash::make($pass);

            $current_time = date("Y-m-d H:i:s");
            $reset_pass_token = base64_encode($input['email'] . '&&' . $uType . "&&" . $current_time);

            $data = ['firstname' => $name, 'pass' => $pass, 'email' => $user->email, 'reset_pass_link' => $reset_pass_token];
            // SendEmail::dispatch($user->email, new ForgotPassword($data))
            //     ->delay(now()->addSeconds(5));
            MailHelper::sendForgotPassAdmin($data);
            $message = trans('message.msg_forgot_pass_success');
            return redirect('forgot-pass')->with('success', $message);

        }
    }

    public function resetPassword($token)
    {
        $response = [];

        $decoded = base64_decode($token);
        $tmp_dec = explode('&&', $decoded);

        if (empty($tmp_dec[0]) || empty($tmp_dec[1]) || empty($tmp_dec[2])) {
            $response['status'] = false;
            $response['message'] = 'Invalid reset password token';
            return response()->json($response);
        }

        $current_time = date("Y-m-d H:i:s");

        $minuteDiff = round((strtotime($current_time) - strtotime($tmp_dec[2])) / 60, 1);

        if ($minuteDiff > 30) { //check if link is generated more than 30 mins ago
            $message = trans('message.msg_reset_pass_link_expire');
            return redirect('/forgot-pass')
                ->withErrors([$message])
                ->withInput();
        }

        return view('login.reset_password', ['token' => $token]);
    }

    /**
     * Used for Admin Logout
     * @return redirect to Admin->Logout
     */
    public function logout(Request $request)
    {
        $prevUser = Session::get('previous_user');

        if (!empty($prevUser)) {
            Auth::guard($prevUser->utype)->login($prevUser);
            $uimg = asset('images/user.png');
            $type = '';
            if (!empty($prevUser->adm_Photo)) {
                $uimg = asset('images/users') . '/' . $prevUser->adm_Photo;
            }
            if ($prevUser->type == 'HertronicAdmin' || $prevUser->type == 'MinistrySubAdmin') {
                $type = 'Hertronic Super Admin';
                $url = url('/admin/dashboard');
            } else if ($prevUser->type == 'MinistryAdmin') {
                $type = trans('general.gn_ministry_super_admin');
                $url = url('/admin/dashboard');
            } else if ($prevUser->type == 'MinistrySubAdmin') {
                $type = 'Ministry Sub Admin';
                $url = url('/admin/dashboard');
            }
            $msg = trans('general.gn_logged_in_as');
            $msg = $msg . " " . $prevUser->adm_Name . " - " . $type;
            $data = '<div class="alert alert-warning alert-dismissible fade show loginas-alert" role="alert">
             <div class="profile-cover">
              <img src="' . $uimg . '">
            </div>
             ' . $msg . '
            <button data-redir="' . $url . '" type="button" class="remove_scroll theme_btn min_btn ml-md-5" data-dismiss="alert" aria-label="Close">
              Exit
            </button>
          </div><div class="alert-bg"></div>';
            Session::forget('previous_user');
            Session::forget('curr_emp_type');
            Session::forget('curr_emp_eid');
            Session::forget('curr_emp_sid');
            Session::forget('curr_school_sem');
            Session::forget('curr_school_year');
            Session::forget('curr_course_class');
            Session::forget('curr_shift');
            Session::forget('employeeRoles');
            Session::forget('schoolYear');

            Session::flash('previous_login', $data);
            Auth::guard('employee')->logout();
            return redirect($prevUser->utype . '/dashboard');
        } else {
            Session::forget('curr_emp_type');
            Session::forget('curr_emp_eid');
            Session::forget('curr_emp_sid');
            Session::forget('curr_school_sem');
            Session::forget('curr_school_year');
            Session::forget('curr_course_class');
            Session::forget('curr_shift');
            Session::forget('employeeRoles');
            Session::forget('schoolYear');

            Auth::guard('admin')->logout();
            Auth::guard('employee')->logout();
            Auth::guard('student')->logout();
            Auth::logout();

            return redirect('login');
        }
    }

    /**
     * Used for Profile Change Password when forgot save
     * @return redirect to Admin->Profile
     */
    public function changePasswordPost(Request $request)
    {
        $response = [];
        $input = $request->all();

        if (isset($input['old_password']) && $input['old_password'] != null && !empty($input['old_password'])) {

            if (Hash::check($input['old_password'], $this->logged_user->password)) {
                // The passwords match...
                if (in_array($this->logged_user->type, $this->admins)) {
                    $user = Admin::findorfail($this->logged_user->id);
                } elseif (in_array($this->logged_user->type, $this->employees)) {
                    $user = Employee::findorfail($this->logged_user->id);
                } elseif ($this->logged_user->type == 'student') {
                    $user = Student::findorfail($this->logged_user->id);
                }

                if (isset($input['new_password']) && $input['new_password'] != null && !empty($input['new_password'])) {
                    $user->password = Hash::make($input['new_password']);
                }
                if ($user->save()) {
                    $response['status'] = true;
                    $response['message'] = trans('message.msg_pass_update_success');
                    $response['redirect'] = url('/logout');

                } else {
                    $response['status'] = false;
                    $response['message'] = trans('message.msg_something_wrong');
                }

            } else {
                $response['status'] = false;
                $response['message'] = trans('message.msg_pass_match_fail');
            }
        }

        return response()->json($response);
    }

    public function resetPasswordPost(Request $request)
    {
        $input = $request->all();
        $response = [];
        $decoded = base64_decode($input['token']);
        $tmp_dec = explode('&&', $decoded);

        if (empty($tmp_dec[0]) || empty($tmp_dec[1])) {
            $response['status'] = false;
            $response['message'] = trans('message.msg_invalid_pass_token');
            return response()->json($response);
        }

        $new_pass = Hash::make($input['new_password']);

        $user = Admin::where('email', $tmp_dec[0])->first();

        if (empty($user)) {
            $user = Employee::where('email', $tmp_dec[0])->first();
        }

        if (empty($user)) {
            $user = Student::where('email', $tmp_dec[0])->first();
        }

        if (!empty($user)) {
            $user->password = $new_pass;
            $user->save();
            $response['status'] = true;
            $response['message'] = trans('message.msg_reset_pass_success');
        } else {
            $response['status'] = false;
            $response['message'] = trans('message.msg_account_not_exist');
        }
        $response['redirect'] = url('login');
        return response()->json($response);
    }

    public function verifyEmail()
    {
        $key = Input::get('verification-key');
        $user = Admin::where('email_verification_key', $key)->get()->first();
        if (empty($user)) {
            $user = Employee::where('email_verification_key', $key)->get()->first();
        }
        if (empty($user)) {
            $user = Student::where('email_verification_key', $key)->get()->first();
        }
        $message = '';
        $status = '';
        if (!empty($user)) {
            if ($user->email_verified_at) {
                $message = trans('message.msg_email_already_verify');
                $status = true;
            } else {
                $user->email_verification_key = null;
                $user->email_verified_at = now();
                $user->save();
                $message = trans('message.msg_email_verify_success');
                $status = true;
            }
        } else {
            $message = trans('message.msg_invalid_verify_key');
            $status = false;
        }

        return view('email.email_verify', array('message' => $message, 'status' => $status));
    }

    public function switchLanguage(Request $request)
    {
        $response = [];

        $data = $request->all();

        Session::put('current_language', $data['lang']);

        $response['message'] = "Language changed successfully!";
        $response['status'] = true;

        return response()->json($response);
    }
}
