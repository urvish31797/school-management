<?php
/**
 * CustomAPIController
 *
 * @package    Laravel
 * @subpackage Controller
 * @since      1.0
 */

namespace App\Http\Controllers\CustomAPI;

use App\Http\Controllers\Controller;
use App\Http\Services\FinalmarksService;
use App\Models\CertificateBluePrints;
use App\Models\ClassCreation;
use App\Models\ClassStudentsSemester;
use App\Models\CourseOrder;
use App\Models\DescriptiveFinalMark;
use App\Models\EducationPlan;
use App\Models\EmployeeType;
use App\Models\EnrollStudent;
use App\Models\Language;
use App\Models\MarksExplanation;
use App\Models\NationalEducationPlan;
use App\Models\StudentBehaviour;
use App\Models\StudentCertificate;
use App\Models\StudentsBehaviour;
use App\Models\Translation;
use UtilHelper;

class CustomAPIController extends Controller
{
    public $div_left_end = "<div class='cource_left'>";
    public $div_end = "</div>";

    /**
     * Render Certificate API
     * @param  Int  $id
     * @return html $layout
     */
    public function renderCertificate($id)
    {
        try
        {
            if (!empty($id)) {
                $principalTypes = EmployeeType::where('epty_Name', 'Principal')->first();
                $principalTypeId = $principalTypes->pkEpty;
                $cssData = classStudentsSemester::select('fkSemCcs', 'fkSemSen')->where('pkSem', $id)->first();
                //fetch blueprint of certificate & fetch language
                $enrollId = $cssData->fkSemSen;
                $enrollStudent = EnrollStudent::select('fkSteEpl')->find($enrollId);
                $educationPlanId = $enrollStudent->fkSteEpl;
                $educationPlan = EducationPlan::select('fkEplNep', 'fkCbp')->where('pkEpl', $educationPlanId)->first();
                $nationalEducationPlan = NationalEducationPlan::select('fkLid')->where('pkNep', $educationPlan->fkEplNep)->first();
                $language = Language::select('language_key')->where('id', $nationalEducationPlan->fkLid)->first();
                $current_language = $language->language_key;
                $blueprint = "cbp_htmlContent_" . $current_language;
                $blueprintName = "cbp_BluePrintName_" . $current_language;
                $blueprintId = $educationPlan->fkCbp;
                $bluePrint = CertificateBluePrints::select($blueprintName, $blueprint, 'cbp_keywords')->where('pkCbp', $blueprintId)->first();
                $mdata = ClassCreation::whereHas('classCreationSemester', function ($q) use ($cssData) {
                        $q->where('pkCcs', $cssData->fkSemCcs);
                    })
                    ->with([
                    'classCreationSchoolYear' => function ($q) {
                        $q->select('pkSye', 'sye_NameNumeric', 'sye_NameCharacter_' . $this->current_language . ' AS sye_NameCharacter', 'sye_ShortName');
                    },
                    'classCreationGrades.grade' => function ($q) use ($current_language) {
                        $q->select('pkGra', 'gra_GradeNameRoman', 'gra_GradeNumeric');
                    },
                    'classCreationSemester' => function ($q) use ($cssData) {
                        $q->where('pkCcs', $cssData->fkSemCcs);
                    },
                    'classCreationSemester.classStudentsSemester' => function ($q) use ($id) {
                        $q->where('pkSem', $id);
                    },
                    'classCreationSemester.classStudentsSemester.gradeAttemptNumber' => function ($q) use ($current_language) {
                        $q->select('pkGan', 'gan_Name_' . $current_language . ' as gan_Name');
                    },
                    'classCreationSemester.homeRoomTeacher.employeesEngagement.employee',
                    'classCreationSemester.semester' => function ($q) use ($current_language) {
                        $q->select('pkEdp', 'edp_EducationPeriodName_' . $current_language . ' as edp_EducationPeriodName');
                    },
                    'classCreationSemester.classStudentsSemester.studentEnroll.student',
                    'classCreationSemester.classStudentsSemester.studentEnroll.student.nationality',
                    'classCreationSemester.classStudentsSemester.studentEnroll.student.municipality' => function ($q) use ($current_language) {
                        $q->select('pkMun', 'mun_MunicipalityName_' . $current_language . ' as mun_MunicipalityName');
                    },
                    'classCreationSemester.classStudentsSemester.studentEnroll.student.country.citizenship',
                    'classCreationSemester.classStudentsSemester.studentEnroll.school' => function ($q) use ($current_language, $principalTypeId) {
                        $q->select('pkSch', 'fkSchPof', 'sch_SchoolName_' . $current_language . ' as sch_SchoolName', 'sch_Address');
                    },
                    'classCreationSemester.classStudentsSemester.studentEnroll.school.employeesEngagement' => function ($q) use ($current_language, $principalTypeId) {
                        $q->where('fkEenEpty', $principalTypeId)->with('employee');
                    },
                    'classCreationSemester.classStudentsSemester.studentEnroll.school.postalCode.municipality.canton.state.country' => function ($q) use ($current_language) {
                        $q->select('pkCny', 'cny_CountryName_' . $current_language . ' as cny_CountryName');
                    },
                    'classCreationSemester.classStudentsSemester.studentEnroll.mainBook' => function ($q) use ($current_language) {
                        $q->select('pkMbo', 'mbo_MainBookNameRoman');
                    },
                    'classCreationSemester.classStudentsSemester.studentEnroll.grade' => function ($q) use ($current_language) {
                        $q->select('pkGra', 'gra_GradeNameRoman', 'gra_GradeNumeric');
                    },
                    'classCreationSemester.classStudentsSemester.studentEnroll.educationProgram' => function ($q) use ($current_language) {
                        $q->select('pkEdp', 'edp_Name_' . $current_language . ' as edp_Name');
                    },
                    'classCreationSemester.classStudentsSemester.classStudentsAllocation.classCreationCourses' => function ($q) use ($current_language) {
                        $q->select('pkCrs', 'crs_CourseName_' . $current_language . ' as crs_CourseName');
                    },
                    'classCreationSemester.classStudentsSemester.classStudentsAllocation.courseGroup',
                    'classCreationSemester.classStudentsSemester.studentEnroll.educationPlan' => function ($q) use ($current_language) {
                        $q->select('pkEpl', 'epl_EducationPlanName_' . $current_language . ' as epl_EducationPlanName');
                    },
                ]);
                $mdata = $mdata->first();

                if (!empty($mdata)) {
                    $courseOrderData = CourseOrder::with(['courseOrderAllocation' => function ($q) use ($mdata) {
                        $q->where('fkCoaGra', $mdata->classCreationSemester[0]->classStudentsSemester[0]->studentEnroll->grade->pkGra)->with(['course']);
                    }])
                        ->where('fkCorEpl', $mdata->classCreationSemester[0]->classStudentsSemester[0]->studentEnroll->educationPlan->pkEpl)
                        ->where('fkCorSch', $mdata->classCreationSemester[0]->classStudentsSemester[0]->studentEnroll->school->pkSch)
                        ->first();

                    $coursesOrderData = (new FinalmarksService())->createCourseOrderforCourse($mdata, $courseOrderData);
                    extract($coursesOrderData);

                    $explantionColumn = 'me_Explanation_' . $current_language;
                    $descriptiveColumn = 'dfm_Text_' . $current_language;
                    $markExplanation = MarksExplanation::select('pkMe', 'me_NumericExplanationMark', $explantionColumn)->get()->toArray();

                    $generalSuccessNumeric = $mdata->classCreationSemester[0]->classStudentsSemester[0]->sem_GeneralSuccess ?? 0;
                    $generalSuccessExplanation = '';
                    foreach ($markExplanation as $key => $value) {
                        if ($value['me_NumericExplanationMark'] == $generalSuccessNumeric) {
                            $generalSuccessExplanation = $value[$explantionColumn];
                        }
                    }
                    $descriptiveFinalMark = DescriptiveFinalMark::select('pkDfm', $descriptiveColumn)->get()->toArray();
                    $studentCertificates = StudentCertificate::where('fkScrSem', $id)->first();
                    $studentBehaviour = StudentsBehaviour::where('fkSebSem', $id)->orderBy('pkStb', 'DESC')->first();

                    $layout = $bluePrint->$blueprint;
                    $certiName = $bluePrint->$blueprintName;

                    $monthKeys = array('gn_jan', 'gn_feb', 'gn_mar', 'gn_apr', 'gn_may', 'gn_jun', 'gn_jul', 'gn_aug', 'gn_sep', 'gn_oct', 'gn_nov', 'gn_dec', 'gn_year');
                    $monthTrans = Translation::GetSelectedKeyword(['keys' => array_values($monthKeys)])
                        ->get()
                        ->toArray();


                    $months = array();
                    foreach ($monthTrans as $k => $v) {
                        if ($v['key'] == 'gn_year') {
                            $months['year']['gn_year'] = $v['text'][$current_language];
                        } else {
                            $months['month'][$v['text']['en']] = $v['text'][$current_language];
                        }
                    }

                    $COUNTRY = $mdata->classCreationSemester[0]->classStudentsSemester[0]->studentEnroll->school->postalCode->municipality->canton->state->country->cny_CountryName ?? '~';
                    $STATE = $mdata->classCreationSemester[0]->classStudentsSemester[0]->studentEnroll->school->postalCode->municipality->canton->state['sta_StateName_' . $current_language] ?? '~';
                    $CANTON = $mdata->classCreationSemester[0]->classStudentsSemester[0]->studentEnroll->school->postalCode->municipality->canton['can_CantonName_' . $current_language] ?? '~';
                    $SCHOOL = $mdata->classCreationSemester[0]->classStudentsSemester[0]->studentEnroll->school->sch_SchoolName ?? '~';
                    $CERTINO = $studentCertificates->scr_CertificateNo ?? '~';
                    $BOOKNO = $mdata->classCreationSemester[0]->classStudentsSemester[0]->studentEnroll->mainBook->mbo_MainBookNameRoman ?? '~';
                    $NUMBER = $mdata->classCreationSemester[0]->classStudentsSemester[0]->studentEnroll->ste_MainBookOrderNumber ?? '~';
                    $MUNICIPALITY = $mdata->classCreationSemester[0]->classStudentsSemester[0]->studentEnroll->student->municipality->mun_MunicipalityName ?? '~';
                    $STUDENTNAME = $mdata->classCreationSemester[0]->classStudentsSemester[0]->studentEnroll->student->full_name ?? '~';
                    $EDUCATIONPROGRAM = $mdata->classCreationSemester[0]->classStudentsSemester[0]->studentEnroll->educationProgram->edp_Name ?? '~';
                    $PARENTNAME = $mdata->classCreationSemester[0]->classStudentsSemester[0]->studentEnroll->student->parent_name ?? '~';
                    $BIRTHDATE = UtilHelper::certiDate($mdata->classCreationSemester[0]->classStudentsSemester[0]->studentEnroll->student->stu_DateOfBirth, $months) ?? '~';
                    $BIRTHPLACE = $mdata->classCreationSemester[0]->classStudentsSemester[0]->studentEnroll->student->stu_PlaceOfBirth ?? '~';
                    $CITIZENSHIP = $mdata->classCreationSemester[0]->classStudentsSemester[0]->studentEnroll->student->country->citizenship[0]['ctz_CitizenshipName_' . $current_language] ?? '~';
                    $NATIONALITY = $mdata->classCreationSemester[0]->classStudentsSemester[0]->studentEnroll->student->nationality['nat_NationalityName_' . $current_language] ?? '~';
                    $SCHOOLYEAR = $mdata->classCreationSchoolYear->sye_NameNumeric ?? '~';
                    $GRADEATTEMPT = $mdata->classCreationSemester[0]->classStudentsSemester[0]->gradeAttemptNumber->gan_Name ?? '~';
                    $gradeRoman = " (" . $mdata->classCreationSemester[0]->classStudentsSemester[0]->studentEnroll->grade->gra_GradeNameRoman . ")" ?? '~';
                    $GRADEEXPLANATION = $mdata->classCreationSemester[0]->classStudentsSemester[0]->studentEnroll->grade->gra_GradeName . $gradeRoman ?? '~';
                    $COURSESECTION = $this->createCourseTable(compact('courses', 'OCGCrs', 'FCGCrs', 'current_language', 'markExplanation', 'descriptiveFinalMark', 'id'));
                    $JUSTIFIEDHOURS = $mdata->classCreationSemester[0]->classStudentsSemester[0]->sem_JustifiedAbsenceHours ?? '~';
                    $UNJUSTIFIEDHOURS = $mdata->classCreationSemester[0]->classStudentsSemester[0]->sem_UnjustifiedAbsenceHours ?? '~';
                    $BEHAVIOR = $studentBehaviour->studentBehaviour['sbe_BehaviourName_' . $current_language] ?? '~';
                    $GRADE = $GRADEEXPLANATION;
                    $SCHOOLADDRESS = $mdata->classCreationSemester[0]->classStudentsSemester[0]->studentEnroll->school->sch_Address ?? '~';
                    if (!empty($SCHOOLADDRESS)) {
                        $CERTIDATE = ' , ' . UtilHelper::certiDate($studentCertificates->scr_Date, $months) ?? '~';
                    } else {
                        $CERTIDATE = UtilHelper::certiDate($studentCertificates->scr_Date, $months) ?? '~';
                    }

                    $GENERALSUCCESSMARKEXPLANATION = $generalSuccessExplanation;
                    $GENERALSUCCESSNUMERIC = $generalSuccessNumeric;
                    $HOMEROOMTEACHER = $mdata->classCreationSemester[0]->homeRoomTeacher[0]->employeesEngagement->employee->full_name ?? '~';
                    $homeRoomTeacherGender = $mdata->classCreationSemester[0]->homeRoomTeacher[0]->employeesEngagement->employee->emp_EmployeeGender ?? '~';
                    $PRINCIPAL = $mdata->classCreationSemester[0]->classStudentsSemester[0]->studentEnroll->school->employeesEngagement[0]->employee->full_name ?? '~';
                    $principalGender = $mdata->classCreationSemester[0]->classStudentsSemester[0]->studentEnroll->school->employeesEngagement[0]->employee->emp_EmployeeGender ?? '~';
                    $BLUEPRINTNAME = $certiName ?? '~';

                    $StudentGender = $mdata->classCreationSemester[0]->classStudentsSemester[0]->studentEnroll->student->stu_StudentGender ?? '~';

                    $GENDER = '';
                    $BORNLABEL = '';
                    $CITIZENLABEL = '';
                    $ACHIEVELABEL = '';
                    $PRINCIPALLABEL = '';
                    $FINISHLABEL = '';
                    $HOMEROOMTEACHERLABEL = '';
                    $STUDENTLABEL = '';
                    $ATTENDANTLABEL = '';

                    $certikeys = array();
                    if ($StudentGender == "Male") {
                        $certikeys['GENDER'] = 'certificate_gender_son';
                        $certikeys['BORNLABEL'] = 'certificate_male_born';
                        $certikeys['CITIZENLABEL'] = 'certificate_male_citizen';
                        $certikeys['ACHIEVELABEL'] = 'certificate_male_achieved';
                        $certikeys['FINISHLABEL'] = 'certificate_male_finished';
                        $certikeys['STUDENTLABEL'] = 'certificate_male_student';
                        $certikeys['ATTENDANTLABEL'] = 'certificate_male_attend';
                    } else {
                        $certikeys['GENDER'] = 'certificate_gender_daughter';
                        $certikeys['BORNLABEL'] = 'certificate_female_born';
                        $certikeys['CITIZENLABEL'] = 'certificate_female_citizen';
                        $certikeys['ACHIEVELABEL'] = 'certificate_female_achieved';
                        $certikeys['FINISHLABEL'] = 'certificate_female_finished';
                        $certikeys['STUDENTLABEL'] = 'certificate_female_student';
                        $certikeys['ATTENDANTLABEL'] = 'certificate_female_attend';
                    }

                    if ($homeRoomTeacherGender == "Male") {
                        $certikeys['HOMEROOMTEACHERLABEL'] = 'certificate_male_homeroomteacher';
                    } else {
                        $certikeys['HOMEROOMTEACHERLABEL'] = 'certificate_female_homeroomteacher';
                    }

                    if ($principalGender = "Male") {
                        $certikeys['PRINCIPALLABEL'] = 'certificate_male_principal';
                    } else {
                        $certikeys['PRINCIPALLABEL'] = 'certificate_female_principal';
                    }

                    $trans = Translation::GetSelectedKeyword(['keys' => array_values($certikeys)])
                        ->get()
                        ->toArray();

                    $transValue = array();
                    foreach ($certikeys as $key => $value) {
                        foreach ($trans as $k => $v) {
                            if ($value == $v['key']) {
                                $transValue[$key] = $v['text'][$current_language];
                            }
                        }
                    }

                    extract($transValue);
                    $data = compact('COUNTRY', 'STATE', 'CANTON', 'SCHOOL', 'CERTINO', 'BOOKNO', 'NUMBER', 'MUNICIPALITY', 'STUDENTNAME', 'EDUCATIONPROGRAM', 'GENDER', 'PARENTNAME', 'BORNLABEL', 'BIRTHDATE', 'BIRTHPLACE', 'CITIZENLABEL', 'CITIZENSHIP', 'NATIONALITY', 'SCHOOLYEAR', 'GRADEATTEMPT', 'GRADEEXPLANATION', 'ACHIEVELABEL', 'COURSESECTION', 'JUSTIFIEDHOURS', 'UNJUSTIFIEDHOURS', 'BEHAVIOR', 'STUDENTLABEL', 'ATTENDANTLABEL', 'GENERALSUCCESSMARKEXPLANATION', 'GENERALSUCCESSNUMERIC', 'FINISHLABEL', 'GRADE', 'SCHOOLADDRESS', 'CERTIDATE', 'HOMEROOMTEACHERLABEL', 'HOMEROOMTEACHER', 'PRINCIPALLABEL', 'PRINCIPAL', 'BLUEPRINTNAME');
                    foreach ($data as $key => $value) {
                        $search = "{" . $key . "}";
                        $layout = str_replace($search, $value, $layout);
                    }
                    echo $layout;
                    exit;
                } else {
                    echo 'Data not found';
                }
            } else {
                die('ID not found');
            }
        } catch (\Exception $e) {
            report($e);
        }
    }

    /**
     * Create course table for certificate generation
     * @param  array $data
     * @return html $COURSESECTION
     */
    public function createCourseTable($data)
    {
        extract($data);
        $start_div = "<fieldset class='first_fieldset'><legend><b>Cources</b></legend>";
        $end_div = "</fieldset>";
        $left_div_start = "<div class='col_6 border_right'>";

        $right_div_start = "<div class='col_6'>";
        $left_div_data = "";
        $right_div_data = "";
        $left_div_datas = "";
        $right_div_datas = "";
        $left_side_courses = [];
        $right_side_courses = [];

        $mandatoryCourses = array();
        foreach ($courses as $key => $value) {
            $left_div_data .= "<div class='cource_detail'>";
            $left_div_data .= "<div class='cource_title'>" . $value['crs_CourseName'] . "</div>";
            $left_div_data .= $this->courseMarks($value, $current_language, $data['markExplanation'], $data['descriptiveFinalMark']);
            $left_div_data .= "</div>";
            $mandatoryCourses[] = $value['pkCsa'];
            $left_side_courses[] = $left_div_data;
            $left_div_data = '';
        }

        $totalLeftSideCourse = count($mandatoryCourses);
        if ($totalLeftSideCourse < 12) {
            for ($i = 0; $i < $totalLeftSideCourse; $i++) {
                $left_div_data .= $left_side_courses[$i];
            }

            $remain_rows = 12 - $totalLeftSideCourse;
            for ($i = 1; $i <= $remain_rows; $i++) {
                $left_div_data .= "<div class='cource_detail'>";
                $left_div_data .= "<div class='cource_title'>~~~~~~~~~~~~~~~~~~~~~~~~~~</div>";
                $left_div_data .= "<div class='cource_left'>~~~~~~~~~~~~</div>";
                $left_div_data .= "</div>";
            }

            for ($i = 1; $i <= 4; $i++) {
                $right_div_data .= "<div class='cource_detail'>";
                $right_div_data .= "<div class='cource_title'>~~~~~~~~~~~~~~~~~~~~~~~~~~</div>";
                $right_div_data .= "<div class='cource_left'>~~~~~~~~~~~~</div>";
                $right_div_data .= "</div>";
            }
        } elseif ($totalLeftSideCourse > 12) {
            for ($i = 0; $i < 12; $i++) {
                $left_div_data .= $left_side_courses[$i];
            }

            for ($i = 12; $i < 16; $i++) {
                $right_div_data .= $left_side_courses[$i];
            }
        } elseif ($totalLeftSideCourse == 12) {
            //mandatory + foriegn show as well
            for ($i = 0; $i < 12; $i++) {
                $left_div_data .= $left_side_courses[$i];
            }

            for ($i = 1; $i <= 4; $i++) {
                $right_div_data .= "<div class='cource_detail'>";
                $right_div_data .= "<div class='cource_title'>~~~~~~~~~~~~~~~~~~~~~~~~~~</div>";
                $right_div_data .= "<div class='cource_left'>~~~~~~~~~~~~</div>";
                $right_div_data .= "</div>";
            }
        }

        $translations = Translation::whereIn('key', ['gn_optional_courses', '
            gn_facultative_courses', ])->get()->pluck('value_' . $current_language, 'key');
        $optionalTitle = $translations['gn_optional_courses'] ?? "Optional Cources";
        $facultativeTitle = $translations['gn_facultative_courses'] ?? "Facultative Courses";

        $right_div_data .= "<div class='cource_detail'>";
        $right_div_data .= "<div><b>" . $optionalTitle . "</b></div>";
        $right_div_data .= "<div></div>";
        $right_div_data .= "</div>";

        $optionalCourse = array();
        foreach ($OCGCrs as $key => $value) {
            $right_div_data .= "<div class='cource_detail'>";
            $right_div_data .= "<div class='cource_title'>" . $value['crs_CourseName'] . "</div>";
            $right_div_data .= $this->courseMarks($value, $current_language, $data['markExplanation'], $data['descriptiveFinalMark']);
            $right_div_data .= "</div>";
            $right_side_courses[] = $value['pkCsa'];
            $optionalCourse[] = $value['pkCsa'];
        }

        if (count($optionalCourse) < 4) {
            $remain_rows = 4 - count($optionalCourse);
            for ($i = 1; $i <= $remain_rows; $i++) {
                $right_div_data .= "<div class='cource_detail'>";
                $right_div_data .= "<div class='cource_title'>~~~~~~~~~~~~~~~~~~~~~~~~~~</div>";
                $right_div_data .= "<div class='cource_left'>~~~~~~~~~~~~</div>";
                $right_div_data .= "</div>";
            }
        }

        $right_div_data .= "<div class='cource_detail'>";
        $right_div_data .= "<div><b>" . $facultativeTitle . "</b></div>";
        $right_div_data .= "<div></div>";
        $right_div_data .= "</div>";

        $facultativeCourse = array();
        foreach ($FCGCrs as $key => $value) {
            $right_div_data .= "<div class='cource_detail'>";
            $right_div_data .= "<div class='cource_title'>" . $value['crs_CourseName'] . "</div>";
            $right_div_data .= $this->courseMarks($value, $current_language, $data['markExplanation'], $data['descriptiveFinalMark']);
            $right_div_data .= "</div>";
            $right_side_courses[] = $value['pkCsa'];
            $facultativeCourse[] = $value['pkCsa'];
        }

        if (count($facultativeCourse) < 2) {
            $remain_rows = 2 - count($facultativeCourse);
            for ($i = 1; $i <= $remain_rows; $i++) {
                $right_div_data .= "<div class='cource_detail'>";
                $right_div_data .= "<div class='cource_title'>~~~~~~~~~~~~~~~~~~~~~~~~~~</div>";
                $right_div_data .= "<div class='cource_left'>~~~~~~~~~~~~</div>";
                $right_div_data .= "</div>";
            }
        }

        $left_div_datas = $left_div_start . $left_div_data . $this->div_end;
        $right_div_datas = $right_div_start . $right_div_data . $this->div_end;

        return $start_div . $left_div_datas . $right_div_datas . $end_div;
    }

    /**
     * Generate row for coursemarks
     * @param  array $index
     * @param  string $current_language
     * @param  array $markExplanation
     * @param  array $descriptiveFinalMark
     * @return html $row
     */
    public function courseMarks($index, $current_language, $markExplanation, $descriptiveFinalMark)
    {
        $row = "<div class='cource_left'><span></span></div>";
        if (!empty($index['fkCsaDfm'])) {
            foreach ($descriptiveFinalMark as $key => $value) {
                if ($value['pkDfm'] == $index['fkCsaDfm']) {
                    $row = $this->div_left_end . $value['dfm_Text_' . $current_language] . "<span></span></div>";
                }
            }
        } elseif (!empty($index['dfm_manual'])) {
            $row = $this->div_left_end . $index['dfm_manual'] . "<span></span></div>";
        } elseif (!empty($index['final_mark'])) {
            foreach ($markExplanation as $key => $value) {
                if ($value['me_NumericExplanationMark'] == $index['final_mark']) {
                    $row = $this->div_left_end . $value['me_Explanation_' . $current_language] . "<span>(" . $index['final_mark'] . ")</span></div>";
                }
            }
        }

        return $row;
    }
}
