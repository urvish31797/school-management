<?php
/**
 * TestController
 *
 * @package    Laravel
 * @subpackage Controller
 * @since      1.0
 */

namespace App\Http\Controllers\CustomAPI;

use App\Http\Controllers\Controller;
use App\Http\Services\FinalmarksService;
use App\Models\ClassCreation;
use App\Models\ClassCreationGrades;
use App\Models\ClassCreationSemester;
use App\Models\ClassStudentsAllocation;
use App\Models\ClassStudentsSemester;
use App\Models\ClassTeachersCourseAllocation;
use App\Models\DailyBook;
use App\Models\DailyBookLecture;
use App\Models\DailyStudentAttendance;
use App\Models\DailySyllabusAccomplishment;
use App\Models\HomeRoomTeacher;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class TestController extends Controller
{
    public function tests(Request $request)
    {   
        try{
            $validator = Validator::make($request->all(), [
                'title' => 'max:255|string|required',
                'header_line' => 'max:1024|required',
                'stars' => 'nullable|integer|digits_between: 1,5'
            ]);
            
            if ($validator->fails()) {
                echo "detected errors";
                // return redirect('form')
                //     ->withErrors($validator)
                //     ->withInput();
            } else{
                echo "working";
                // return redirect('form')->with('message', "Succesfully validated!");
            }
            dd($validator->messages());
           
        }catch(\Exception $e){
            echo $e->getMessage();
        }
    }

    public function deleteSchoolClasses(Request $request, $school_id)
    {
        if (!empty($school_id)) {
            $classes = ClassCreation::with([
                'classCreationGrades',
                'classCreationSemester',
                'classCreationSemester.classStudentsSemester',
                'classCreationSemester.classStudentsSemester.ClassStudentsAllocation',
                'classCreationSemester.classTeachers',
                'classCreationSemester.HomeRoomTeacher',
            ])->where('fkClrSch', $school_id)->get();

            if (count($classes)) {
                foreach ($classes as $ck => $cv) {
                    if (isset($cv->classCreationSemester)) {
                        foreach ($cv->classCreationSemester as $cck => $ccv) {
                            if (isset($ccv->classStudentsSemester)) {
                                foreach ($ccv->classStudentsSemester as $csck => $cscv) {
                                    if (isset($cscv->ClassStudentsAllocation)) {
                                        foreach ($cscv->ClassStudentsAllocation as $csak => $csav) {
                                            echo "pkCsa: " . $csav->pkCsa . "\n";
                                            ClassTeachersCourseAllocation::find($csav->pkCsa)->delete();
                                        }
                                    }
                                    echo "pkSem: " . $cscv->pkSem . "\n";
                                    ClassStudentsSemester::find($cscv->pkSem)->delete();
                                }
                            }

                            if (isset($ccv->classTeachers)) {
                                foreach ($ccv->classTeachers as $ctk => $ctv) {
                                    echo "pkCtc: " . $ctv->pkCtc . "\n";
                                    ClassTeachersCourseAllocation::find($ctv->pkCtc)->delete();
                                }
                            }

                            if (isset($ccv->HomeRoomTeacher)) {
                                foreach ($ccv->HomeRoomTeacher as $chtv => $chtk) {
                                    echo "pkHrt: " . $chtk->pkHrt . "\n";
                                    HomeRoomTeacher::find($chtk->pkHrt)->delete();
                                }
                            }

                            echo "pkCcs: " . $ccv->pkCcs . "\n";
                            ClassCreationSemester::find($ccv->pkCcs)->delete();
                        }
                    }

                    if (isset($cv->classCreationGrades)) {
                        foreach ($cv->classCreationGrades as $ccgk => $ccgv) {
                            echo "pkCcg: " . $ccgv->pkCcg . "\n";
                            ClassCreationGrades::where('pkCcg', $ccgv->pkCcg)->delete();
                        }
                    }
                }
                echo "pkClr: " . $cv->pkClr . "\n";
                ClassCreation::where('pkClr', $cv->pkClr)->delete();
            }
        }
    }

    public function createPdf($id)
    {
        $res = (new FinalmarksService())->generatePuppeteerPDF($id);
        dd($res);
    }

    public function emptytables()
    {
        DailyBook::truncate();
        DailyBookLecture::truncate();
        DailySyllabusAccomplishment::truncate();
        DailyStudentAttendance::truncate();
    }

    public function getTablesWithColumns()
    {
        $tables = \DB::select('SHOW TABLES');

        foreach ($tables as $table) {
            $tableName = $table->Tables_in_gaud_gaudeamus;
            $result = \DB::select(
                "SELECT COUNT(*) AS Columns FROM INFORMATION_SCHEMA.COLUMNS
                WHERE table_schema = 'gaud_gaudeamus' AND table_name = '" . $tableName . "'");
            echo $tableName . " - " . $result[0]->Columns . "<br>";
        }
    }
}
