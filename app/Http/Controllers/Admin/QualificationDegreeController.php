<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\EducationPlan;
use App\Models\EmployeesEducationDetail;
use App\Models\QualificationDegree;
use App\Http\Requests\QualificationDegreeRequest;
use DataTableService;
use Datatables;
use HtmlBuilder;
use HttpResponse;

class QualificationDegreeController extends Controller
{
    public function index(HtmlBuilder $builder)
    {
        $builder_data['columns'] = [
            ['data' => 'DT_RowIndex', 'name' => 'DT_RowIndex', 'title' => trans('general.sr_no'), 'render' => null, 'orderable' => false, 'searchable' => false],
            ['data' => 'qde_Uid', 'name' => 'qde_Uid', 'title' => trans('general.gn_uid')],
            ['data' => 'qde_QualificationDegreeName', 'name' => 'qde_QualificationDegreeName', 'title' => trans('general.gn_name')],
            ['data' => 'qde_Notes', 'name' => 'qde_Notes', 'title' => trans('general.gn_notes')],
            ['data' => 'qde_Status', 'name' => 'qde_Status', 'title' => trans('general.gn_status')],
            ['data' => 'action', 'name' => 'action', 'title' => trans('general.gn_actions'), 'orderable' => false, 'searchable' => false, 'width' => '10%'],
        ];

        $builder_data['ajax'] = [
            'url'=> route('qualificationdegrees.list'),
            'data' => 'function(d) {
                d.search =  $("#search_qualification_degree").val();
            }'
        ];

        $dt_html = new DataTableService($builder_data, $builder);
        $dt_html = $dt_html->builder;

        return view('admin.qualificationDegrees.qualificationDegrees',compact('dt_html'));
    }

    public function store(QualificationDegreeRequest $request)
    {
        $column = 'qde_QualificationDegreeName_'.$this->current_language;
        $value = $request[$column];

        $response = [];
        if (!empty($request->pkQde)) {
            $checkPrev = QualificationDegree::where($column,$value)
                    ->where('pkQde', '!=', $request->pkQde)->count();
            if ($checkPrev) {
                $status_code = HttpResponse::HTTP_BAD_REQUEST;
                $response['message'] = trans('message.msg_qd_exists');
            } else {
                QualificationDegree::where('pkQde', $request->pkQde)->update($request->validated());

                $response['message'] = trans('message.msg_qd_update_success');
            }
        } else {
            $checkPrev = QualificationDegree::where($column,$value)
                    ->count();
            if ($checkPrev) {
                $status_code = HttpResponse::HTTP_BAD_REQUEST;
                $response['message'] = trans('message.msg_qd_exists');
            } else {
                $id = QualificationDegree::insertGetId($request->validated());
                if (!empty($id)) {
                    QualificationDegree::where('pkQde', $id)->update(['qde_Uid' => "QUD" . $id]);

                    $response['message'] = trans('message.msg_qd_add_success');

                } else {
                    $status_code = HttpResponse::HTTP_BAD_REQUEST;
                    $response['message'] = trans('message.msg_something_wrong');
                }
            }
        }

        return response()->json($response,$status_code ?? HttpResponse::HTTP_OK);
    }

    public function edit($cid)
    {
        $response = [];
        $cdata = QualificationDegree::where('pkQde', '=', $cid)->first();

        if (empty($cid) || empty($cdata)) {
            $status_code = HttpResponse::HTTP_BAD_REQUEST;
        } else {

            $response['data'] = $cdata;
        }

        return response()->json($response,$status_code ?? HttpResponse::HTTP_OK);

    }

    public function destroy($cid)
    {
        $response = [];

        if (empty($cid)) {
            $status_code = HttpResponse::HTTP_BAD_REQUEST;
        } else {

            $EducationPlan = EducationPlan::where('fkEplQde', $cid)
                ->count();
            $eed = EmployeesEducationDetail::where('fkEedQde', $cid)
                ->count();

            if ($EducationPlan != 0 || $eed != 0) {
                $status_code = HttpResponse::HTTP_BAD_REQUEST;
                $response['message'] = trans('message.msg_qualification_degree_delete_prompt');
            } else {
                QualificationDegree::where('pkQde', $cid)->delete();

                $response['message'] = trans('message.msg_qualification_degree_delete_success');
            }
        }

        return response()->json($response,$status_code ?? HttpResponse::HTTP_OK);
    }

    /**
     * Get Qualification degrees list
     *
     * @return JSON
     */
    public function qualificationdegrees_list()
    {
        $qualification_degree = QualificationDegree::select('*', 'qde_QualificationDegreeName_' . $this->current_language . ' as qde_QualificationDegreeName');

        $qualification_degree->when(request('search'),function($q){
            return $q->where('qde_Uid', 'LIKE', '%' . request('search') . '%')
            ->orWhere('qde_Notes', 'LIKE', '%' . request('search') . '%')
            ->orWhere('qde_QualificationDegreeName_' . $this->current_language, 'LIKE', '%' . request('search') . '%');
        });

        return DataTables::of($qualification_degree)
            ->editColumn('qde_Notes', function($qualification_degree){
                return substr($qualification_degree->qde_Notes,0,45);
            })
            ->editColumn('qde_Status', function($qualification_degree){
                if ($qualification_degree->qde_Status == 'Active') {
                    return trans('general.gn_active');
                } else {
                    return trans('general.gn_inactive');
                }
            })
            ->addColumn('action', function ($qualification_degree) {
                $str = '<a cid="'.$qualification_degree->pkQde.'" onclick="triggerEdit('.$qualification_degree->pkQde.')" href="javascript:void(0)"><i class="fa fa-pen"></i></a>&nbsp;&nbsp';
                $str .= '<a onclick="triggerDelete('.$qualification_degree->pkQde.')" href="javascript:void(0)"><i class="fa fa-trash"></i></a>';
                return $str;
            })
            ->addIndexColumn()
            ->escapeColumns()
            ->toJSON();
    }

}
