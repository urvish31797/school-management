<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\OptionalCoursesGroup;
use App\Http\Requests\OptionalCoursesGroupRequest;
use DataTableService;
use Datatables;
use HtmlBuilder;
use HttpResponse;

class OptionalCoursesGroupController extends Controller
{
    public function index(HtmlBuilder $builder)
    {
        $builder_data['columns'] = [
            ['data' => 'DT_RowIndex', 'name' => 'DT_RowIndex', 'title' => trans('general.sr_no'), 'render' => null, 'orderable' => false, 'searchable' => false],
            ['data' => 'ocg_Uid', 'name' => 'ocg_Uid', 'title' => trans('general.gn_uid')],
            ['data' => 'ocg_Name', 'name' => 'ocg_Name', 'title' => trans('general.gn_name')],
            ['data' => 'ocg_Notes', 'name' => 'ocg_Notes', 'title' => trans('general.gn_notes')],
            ['data' => 'action', 'name' => 'action', 'title' => trans('general.gn_actions'), 'orderable' => false, 'searchable' => false, 'width' => '10%'],
        ];

        $builder_data['ajax'] = [
            'url'=> route('optionalcoursesgroup.list'),
            'data' => 'function(d) {
                d.search =  $("#search_optionalCoursesGroup").val();
            }'
        ];

        $dt_html = new DataTableService($builder_data, $builder);
        $dt_html = $dt_html->builder;

        return view('admin.optionalCoursesGroup.optionalCoursesGroup',compact('dt_html'));
    }

    public function store(OptionalCoursesGroupRequest $request)
    {
        $column = 'ocg_Name_'.$this->current_language;
        $value = $request[$column];

        $response = [];
        if (!empty($request->pkOcg)) {
            $checkPrev = OptionalCoursesGroup::where($column,$value)
                    ->where('pkOcg', '!=', $request->pkOcg)->count();
            if ($checkPrev) {
                $status_code = HttpResponse::HTTP_BAD_REQUEST;
                $response['message'] = trans('message.msg_ocg_exist');
            } else {
                OptionalCoursesGroup::where('pkOcg', $request->pkOcg)->update($request->validated());
                
                $response['message'] = trans('message.msg_ocg_update_success');
            }
        } else {
            $checkPrev = OptionalCoursesGroup::where($column,$value)
                    ->count();
            if ($checkPrev) {
                $status_code = HttpResponse::HTTP_BAD_REQUEST;
                $response['message'] = trans('message.msg_ocg_exist');
            } else {
                $id = OptionalCoursesGroup::insertGetId($request->validated());
                if (!empty($id)) {
                    OptionalCoursesGroup::where('pkOcg', $id)->update(['ocg_Uid' => "OCG" . $id]);
                    
                    $response['message'] = trans('message.msg_ocg_add_success');

                } else {
                    $status_code = HttpResponse::HTTP_BAD_REQUEST;
                    $response['message'] = trans('message.msg_something_wrong');
                }
            }
        }

        return response()->json($response,$status_code ?? HttpResponse::HTTP_OK);
    }

    public function edit($cid)
    {
        $response = [];
        $cdata = OptionalCoursesGroup::where('pkOcg', '=', $cid)->first();

        if (empty($cid) || empty($cdata)) {
            $status_code = HttpResponse::HTTP_BAD_REQUEST;
        } else {
            
            $response['data'] = $cdata;
        }

        return response()->json($response,$status_code ?? HttpResponse::HTTP_OK);

    }

    public function destroy($cid)
    {
        $response = [];

        if (empty($cid)) {
            $status_code = HttpResponse::HTTP_BAD_REQUEST;
        } else {
            OptionalCoursesGroup::where('pkOcg', $cid)->delete();
            
            $response['message'] = trans('message.msg_ocg_delete_success');
        }

        return response()->json($response,$status_code ?? HttpResponse::HTTP_OK);
    }

    public function optionalcoursesgroup_list()
    {
        $optional_courses = OptionalCoursesGroup::select('*', 'ocg_Name_' . $this->current_language . ' as ocg_Name');
        $optional_courses->when(request('search'), function ($q){
            return $q->where('ocg_Uid', 'LIKE', '%' . request('search') . '%')
            ->orWhere('ocg_Name_' . $this->current_language, 'LIKE', '%' . request('search') . '%')
            ->orWhere('ocg_Notes', 'LIKE', '%' . request('search') . '%');
        });

        return DataTables::of($optional_courses)
            ->editColumn('ocg_Notes', function($optional_courses){
                return substr($optional_courses->ocg_Notes,0,45);
            })
            ->addColumn('action', function ($optional_courses) {
                $str = '<a cid="'.$optional_courses->pkOcg.'" onclick="triggerEdit('.$optional_courses->pkOcg.')" href="javascript:void(0)"><i class="fa fa-pen"></i></a>&nbsp;&nbsp';
                $str .= '<a onclick="triggerDelete('.$optional_courses->pkOcg.')" href="javascript:void(0)"><i class="fa fa-trash"></i></a>';
                return $str;
            })
            ->addIndexColumn()
            ->escapeColumns()
            ->toJSON();
    }

}
