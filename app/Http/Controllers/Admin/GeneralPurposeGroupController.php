<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\GeneralPurposeGroup;
use App\Http\Requests\GeneralPurposeGroupRequest;
use DataTableService;
use Datatables;
use HttpResponse;
use HtmlBuilder;

class GeneralPurposeGroupController extends Controller
{
    public function index(HtmlBuilder $builder)
    {
        $builder_data['columns'] = [
            ['data' => 'DT_RowIndex', 'name' => 'DT_RowIndex', 'title' => trans('general.sr_no'), 'render' => null, 'orderable' => false, 'searchable' => false],
            ['data' => 'gpg_Uid', 'name' => 'gpg_Uid', 'title' => trans('general.gn_uid')],
            ['data' => 'gpg_Name', 'name' => 'gpg_Name', 'title' => trans('general.gn_name')],
            ['data' => 'gpg_Notes', 'name' => 'gpg_Notes', 'title' => trans('general.gn_notes')],
            ['data' => 'action', 'name' => 'action', 'title' => trans('general.gn_actions'), 'orderable' => false, 'searchable' => false, 'width' => '10%'],
        ];

        $builder_data['ajax'] = [
            'url'=> route('generalpurposegroup.list'),
            'data' => 'function(d) {
                d.search =  $("#search_facultativeCoursesGroup").val();
            }'
        ];

        $dt_html = new DataTableService($builder_data, $builder);
        $dt_html = $dt_html->builder;

        return view('admin.generalPurposeGroup.generalPurposeGroup',compact('dt_html'));
    }

    public function store(GeneralPurposeGroupRequest $request)
    {
        $column = 'gpg_Name_'.$this->current_language;
        $value = $request[$column];

        $response = [];
        if (!empty($request->pkGpg)) {
            $checkPrev = GeneralPurposeGroup::where($column,$value)
                    ->where('pkGpg', '!=', $request->pkGpg)->count();
            if ($checkPrev) {
                $status_code = HttpResponse::HTTP_BAD_REQUEST;
                $response['message'] = trans('message.msg_gpg_exist');
            } else {
                GeneralPurposeGroup::where('pkGpg', $request->pkGpg)->update($request->validated());

                $response['message'] = trans('message.msg_gpg_update_success');
            }
        } else {
            $checkPrev = GeneralPurposeGroup::where($column,$value)
                    ->count();
            if ($checkPrev) {
                $status_code = HttpResponse::HTTP_BAD_REQUEST;
                $response['message'] = trans('message.msg_gpg_exist');
            } else {
                $id = GeneralPurposeGroup::insertGetId($request->validated());
                if (!empty($id)) {
                    GeneralPurposeGroup::where('pkGpg', $id)->update(['gpg_Uid' => "GPG" . $id]);
                    $response['message'] = trans('message.msg_gpg_add_success');
                } else {
                    $status_code = HttpResponse::HTTP_BAD_REQUEST;
                    $response['message'] = trans('message.msg_something_wrong');
                }
            }
        }

        return response()->json($response,$status_code ?? HttpResponse::HTTP_OK);
    }

    public function edit($cid)
    {
        $response = [];
        $cdata = GeneralPurposeGroup::where('pkGpg', '=', $cid)->first();

        if (empty($cid) || empty($cdata)) {
            $status_code = HttpResponse::HTTP_BAD_REQUEST;
        } else {

            $response['data'] = $cdata;
        }

        return response()->json($response,$status_code ?? HttpResponse::HTTP_OK);

    }

    public function destroy($cid)
    {
        $response = [];

        if (empty($cid)) {
            $status_code = HttpResponse::HTTP_BAD_REQUEST;
        } else {
            GeneralPurposeGroup::where('pkGpg', $cid)->delete();

            $response['message'] = trans('message.msg_gpg_delete_success');
        }

        return response()->json($response,$status_code ?? HttpResponse::HTTP_OK);
    }

    /**
     * General Purpose Groups List
     *
     * @param \Illuminate\Http\Request $request
     * @return JSON
     */
    public function generalpurposegroup_list()
    {
        $general_groups = GeneralPurposeGroup::select('*', 'gpg_Name_' . $this->current_language . ' as gpg_Name');
        $general_groups->when(request('search'), function ($q){
            return $q->where('gpg_Uid', 'LIKE', '%' . request('search') . '%')
            ->orWhere('gpg_Name_' . $this->current_language, 'LIKE', '%' . request('search') . '%')
            ->orWhere('gpg_Notes', 'LIKE', '%' . request('search') . '%');
        });

        return DataTables::of($general_groups)
            ->editColumn('gpg_Notes', function($general_groups){
                return substr($general_groups->gpg_Notes,0,45);
            })
            ->addColumn('action', function ($general_groups) {
                $str = '<a cid="'.$general_groups->pkGpg.'" onclick="triggerEdit('.$general_groups->pkGpg.')" href="javascript:void(0)"><i class="fa fa-pen"></i></a>&nbsp;&nbsp';
                $str .= '<a onclick="triggerDelete('.$general_groups->pkGpg.')" href="javascript:void(0)"><i class="fa fa-trash"></i></a>';
                return $str;
            })
            ->addIndexColumn()
            ->escapeColumns()
            ->toJSON();
    }

}
