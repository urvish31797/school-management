<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Traits\CommonTrait;
use App\Models\SchoolYear;
use App\Models\SchoolYearWeekNumber;
use Illuminate\Http\Request;
use App\Http\Requests\SchoolYearRequest;
use DataTableService;
use Datatables;
use HtmlBuilder;
use HttpResponse;

class SchoolYearController extends Controller
{
    use CommonTrait;

    public function index(HtmlBuilder $builder)
    {
        $builder_data['columns'] = [
            ['data' => 'DT_RowIndex', 'name' => 'DT_RowIndex', 'title' => trans('general.sr_no'), 'render' => null, 'orderable' => false, 'searchable' => false],
            ['data' => 'sye_Uid', 'name' => 'sye_Uid', 'title' => trans('general.gn_uid')],
            ['data' => 'sye_NameCharacter', 'name' => 'sye_NameCharacter', 'title' => trans('general.gn_name_character')],
            ['data' => 'sye_NameNumeric', 'name' => 'sye_NameNumeric', 'title' => trans('general.gn_name_numeric')],
            ['data' => 'sye_DefaultYear', 'name' => 'sye_DefaultYear', 'title' => trans('general.current_year') , 'orderable' => false],
            ['data' => 'action', 'name' => 'action', 'title' => trans('general.gn_actions'), 'orderable' => false, 'searchable' => false, 'width' => '10%'],
        ];

        $builder_data['ajax'] = [
            'url'=> route('schoolyear.list'),
            'data' => 'function(d) {
                d.search =  $("#search_schoolYear").val();
            }'
        ];

        $dt_html = new DataTableService($builder_data, $builder);
        $dt_html = $dt_html->builder;

        return view('admin.schoolYear.schoolYear',compact('dt_html'));
    }

    public function create()
    {
        $cantons = $this->getCantonList($this->current_language);
        $pagedata = compact('cantons');

        return view('admin.schoolYear.addschoolYear')->with($pagedata);
    }

    public function store(SchoolYearRequest $request)
    {
        $response = [];

        $input = $request->validated();
        if (!empty($input['pkSye'])) {
            $checkPrev = SchoolYear::where(function ($query) use ($input) {
                $query->where('sye_NameCharacter_' . $this->current_language, $input['sye_NameCharacter_' . $this->current_language])
                    ->orWhere('sye_NameNumeric', $input['sye_NameNumeric']);
            })->where(function ($query) use ($input) {
                $query->where('pkSye', '!=', $input['pkSye']);
            })->first();

            if (!empty($checkPrev)) {
                $status_code = HttpResponse::HTTP_BAD_REQUEST;
                $response['message'] = trans('message.msg_school_year_exists');
            } else {
                $fkSywCan = $input['fkSywCan'];
                unset($input['fkSywCan']);
                $sye_NumberofWeek = $input['sye_NumberofWeek'];
                unset($input['sye_NumberofWeek']);

                SchoolYear::where('pkSye', $input['pkSye'])->update($input);
                SchoolYearWeekNumber::where('fkSywSye', $input['pkSye'])->delete();
                foreach ($fkSywCan as $k => $v) {
                    if (!empty($sye_NumberofWeek[$k])) {
                        $data = ['fkSywSye' => $input['pkSye'], 'fkSywCan' => $v, 'sye_NumberofWeek' => $sye_NumberofWeek[$k]];
                        SchoolYearWeekNumber::insert($data);
                    }
                }

                $response['message'] = trans('message.msg_school_year_update_success');
            }
        } else {
            $checkPrev = SchoolYear::where('sye_NameCharacter_' . $this->current_language, $input['sye_NameCharacter_' . $this
                    ->current_language])
                    ->orWhere('sye_NameNumeric', $input['sye_NameNumeric'])->first();

            if (!empty($checkPrev)) {
                $status_code = HttpResponse::HTTP_BAD_REQUEST;
                $response['message'] = trans('message.msg_school_year_exists');
            } else {
                $fkSywCan = $input['fkSywCan'];
                unset($input['fkSywCan']);
                $sye_NumberofWeek = $input['sye_NumberofWeek'];
                unset($input['sye_NumberofWeek']);
                unset($input['pkSye']);
                $id = SchoolYear::insertGetId($input);
                if (!empty($id)) {
                    SchoolYear::where('pkSye', $id)->update(['sye_Uid' => "SYE" . $id]);
                    foreach ($fkSywCan as $k => $v) {
                        if (!empty($sye_NumberofWeek[$k])) {
                            $data = ['fkSywSye' => $id, 'fkSywCan' => $v, 'sye_NumberofWeek' => $sye_NumberofWeek[$k]];
                            SchoolYearWeekNumber::insert($data);
                        }
                    }

                    $response['message'] = trans('message.msg_school_year_add_success');

                } else {
                    $status_code = HttpResponse::HTTP_BAD_REQUEST;
                    $response['message'] = trans('message.msg_something_wrong');
                }
            }
        }

        return response()->json($response,$status_code ?? HttpResponse::HTTP_OK);
    }

    /**
     * View Detail Page
     *
     * @param [int] $cid
     * @return view('admin.schoolYear.viewschoolYear')
     */
    public function show($cid)
    {
        $cdata = SchoolYear::with(['cantonweekNumbers',
            'cantonweekNumbers.cantons' => function ($q) {
                $q->select('pkCan', 'fkCanSta', 'can_CantonName_' . $this->current_language . ' AS can_CantonName');
            },
            'cantonweekNumbers.cantons.state' => function ($q) {
                $q->select('pkSta', 'sta_StateName_' . $this->current_language . ' AS sta_StateName');
            }])->where('pkSye', '=', $cid)->first();
        $pagedata = compact('cdata');

        return view('admin.schoolYear.viewschoolYear')->with($pagedata);
    }

    /**
     * Edit Page
     *
     * @param [int] $cid
     * @return view('admin.schoolYear.editschoolYear')
     */
    public function edit($cid)
    {
        $cantons = $this->getCantonList($this->current_language);

        $cdata = SchoolYear::with(['cantonweekNumbers',
            'cantonweekNumbers.cantons' => function ($q) {
                $q->select('pkCan', 'fkCanSta', 'can_CantonName_' . $this->current_language . ' AS can_CantonName');
            },
            'cantonweekNumbers.cantons.state' => function ($q) {
                $q->select('pkSta', 'sta_StateName_' . $this->current_language . ' AS sta_StateName');
            }])->where('pkSye', '=', $cid)->first();
        $pagedata = compact('cdata', 'cantons', 'cid');

        return view('admin.schoolYear.editschoolYear')->with($pagedata);
    }

    /**
     * Delete School Year
     *
     * @param \Illuminate\Http\Request $request
     * @param [int] $cid
     * @return JSON
     */
    public function destroy($cid)
    {
        $response = [];

        if (empty($cid)) {
            $status_code = HttpResponse::HTTP_BAD_REQUEST;
        } else {
            $StudentEnrollment = 0;
            if ($StudentEnrollment != 0) {
                $status_code = HttpResponse::HTTP_BAD_REQUEST;
                $response['message'] = trans('message.msg_school_year_delete_prompt');
            } else {
                $schoolYear = SchoolYear::where('pkSye', $cid)->first();
                if (!empty($schoolYear)) {
                    if ($schoolYear->sye_DefaultYear == 1) {
                        $status_code = HttpResponse::HTTP_BAD_REQUEST;
                        $response['message'] = trans('message.msg_school_year_delete_error');
                    } else {
                        $schoolYear->deleted_at = now();
                        $schoolYear->save();

                        $response['message'] = trans('message.msg_school_year_delete_success');
                    }
                }
            }
        }

        return response()->json($response,$status_code ?? HttpResponse::HTTP_OK);
    }
    /**
     * Change Shool Year
     *
     * @param \Illuminate\Http\Request $request
     * @return JSON
     */
    public function change_schoolyear()
    {
        $cyid = request('cyid');
        $response = [];

        if (empty($cyid)) {
            $status_code = HttpResponse::HTTP_BAD_REQUEST;
            $response['message'] = trans('message.msg_something_wrong');
        } else {
            $existActiveYear = SchoolYear::where('sye_DefaultYear', 1)->first();
            if (!empty($existActiveYear)) {
                $existActiveYear->sye_DefaultYear = 0;
                $existActiveYear->save();
            }

            SchoolYear::where('pkSye', $cyid)->update(['sye_DefaultYear' => 1]);
            $response['message'] = trans('message.msg_school_year_active_success');
        }

        return response()->json($response,$status_code ?? HttpResponse::HTTP_OK);
    }

    /**
     * Get School Years List
     *
     * @param \Illuminate\Http\Request $request
     * @return JSON
     */
    public function schoolyear_list()
    {
        $school_year = SchoolYear::select('*', 'sye_NameCharacter_' . $this->current_language . ' as sye_NameCharacter');
        $school_year->when(request('search'),function($q){
            return $q->where('sye_Uid', 'LIKE', '%' . request('search') . '%')
            ->orWhere('sye_NameNumeric', 'LIKE', '%' . request('search') . '%')
            ->orWhere('sye_NameCharacter_' . $this->current_language, 'LIKE', '%' . request('search') . '%');
         });

        return DataTables::of($school_year)
        ->editColumn('sye_DefaultYear',function($school_year){
            if($school_year->sye_DefaultYear == 1){
                return '<a class="btn btn-success btn-sm" href="javascript:void(0)">'.trans('general.gn_default').'</a>';
            } else {
                return '<a class="btn btn-danger btn-sm" href="javascript:void(0)" onclick="changeYearModal('.$school_year->pkSye.')">'.trans('general.gn_not_default').'</a>';
            }
        })
        ->addColumn('action', function ($school_year) {
            $str = '<a href='.url("admin/schoolyear/".$school_year->pkSye).'><i class="fa fa-eye"></i></a>&nbsp;&nbsp';
            $str .= '<a href='.url("admin/schoolyear/".$school_year->pkSye."/edit").'><i class="fa fa-pen"></i></a>&nbsp;&nbsp';
            $str .= '<a onclick="triggerDelete('.$school_year->pkSye.')" href="javascript:void(0)"><i class="fa fa-trash"></i></a>';
            return $str;
        })
        ->rawColumns(['sye_DefaultYear','action'])
        ->addIndexColumn()
        ->escapeColumns()
        ->toJSON();

    }
}
