<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Citizenship;
use App\Models\Country;
use App\Models\Employee;
use App\Http\Requests\CitizenshipRequest;
use DataTableService;
use Datatables;
use HtmlBuilder;
use HttpResponse;

class CitizenshipController extends Controller
{
    public function index(HtmlBuilder $builder)
    {
        $builder_data['columns'] = [
            ['data' => 'DT_RowIndex', 'name' => 'DT_RowIndex', 'title' => trans('general.sr_no'), 'render' => null, 'orderable' => false, 'searchable' => false],
            ['data' => 'ctz_Uid', 'name' => 'ctz_Uid', 'title' => trans('general.gn_uid')],
            ['data' => 'ctz_CitizenshipName', 'name' => 'ctz_CitizenshipName', 'title' => trans('general.gn_name')],
            ['data' => 'cny_CountryName', 'name' => 'cny_CountryName', 'title' => trans('general.gn_country'), 'orderable' => false],
            ['data' => 'ctz_Notes', 'name' => 'ctz_Notes', 'title' => trans('general.gn_notes')],
            ['data' => 'action', 'name' => 'action', 'title' => trans('general.gn_actions'), 'orderable' => false, 'searchable' => false, 'width' => '10%'],
        ];

        $builder_data['ajax'] = [
            'url'=> route('citizenships.list'),
            'data' => 'function(d) {
                d.search =  $("#search_citizenship").val();
                d.country =  $("#country_filter").val();
            }'
        ];

        $dt_html = new DataTableService($builder_data, $builder);
        $dt_html = $dt_html->builder;

        return View('admin.citizenships.citizenships', compact('dt_html'));
    }

    public function store(CitizenshipRequest $request)
    {
        $column = 'ctz_CitizenshipName_'.$this->current_language;
        $value = $request[$column];

        $response = [];
        if (!empty($request->pkCtz)) {
            $checkPrev = Citizenship::where($column,$value)
                    ->where('pkCtz', '!=', $request->pkCtz)->count();
            if ($checkPrev) {
                $status_code = HttpResponse::HTTP_BAD_REQUEST;
                $response['message'] = trans('message.msg_citizenship_exist');
            } else {
                Citizenship::where('pkCtz', $request->pkCtz)->update($request->validated());
                $response['message'] = trans('message.msg_citizenship_update_success');
            }
        } else {

            $checkPrev = Citizenship::where($column,$value)
                    ->count();
            if ($checkPrev) {
                $status_code = HttpResponse::HTTP_BAD_REQUEST;
                $response['message'] = trans('message.msg_citizenship_exist');
            } else {
                $id = Citizenship::insertGetId($request->validated());
                if (!empty($id)) {
                    Citizenship::where('pkCtz', $id)->update(['ctz_Uid' => "CIZ" . $id]);
                    $response['message'] = trans('message.msg_citizenship_add_success');

                } else {
                    $status_code = HttpResponse::HTTP_BAD_REQUEST;
                    $response['message'] = trans('message.msg_something_wrong');
                }
            }
        }

        return response()->json($response,$status_code ?? HttpResponse::HTTP_OK);
    }

    public function edit($id)
    {
        $response = [];
        $cdata = Citizenship::where('pkCtz', '=', $id)->first();

        if (empty($id) || empty($cdata)) {
            $status_code = HttpResponse::HTTP_BAD_REQUEST;
        } else {

            $response['data'] = $cdata;
        }

        return response()->json($response,$status_code ?? HttpResponse::HTTP_OK);
    }

    public function destroy($id)
    {
        $response = [];

        if (empty($id)) {
            $status_code = HttpResponse::HTTP_BAD_REQUEST;
        } else {
            $Employee = Employee::where('fkEmpCtz', $id)
                ->count();
            $Student = 0;
            if ($Employee != 0 || $Student != 0) {
                $status_code = HttpResponse::HTTP_BAD_REQUEST;
                $response['message'] = trans('message.msg_citizenship_delete_prompt');
            } else {
                Citizenship::where('pkCtz', $id)->delete();

                $response['message'] = trans('message.msg_citizenship_delete_success');
            }
        }

        return response()->json($response,$status_code ?? HttpResponse::HTTP_OK);
    }

    /**
     * Citizenship List
     *
     * @param \Illuminate\Http\Request $request
     * @return void
     */
    public function citizenships_list()
    {
        $citizenship = Citizenship::select('*', 'ctz_CitizenshipName_' . $this->current_language . ' as ctz_CitizenshipName')
        ->with([
            'country' => function ($query) {
                $query->select('pkCny', 'cny_CountryName_' . $this->current_language . ' as cny_CountryName');
            },
        ]);

        $citizenship->when(request('search'), function ($q){
            return $q->where('ctz_Uid', 'LIKE', '%' . request('search') . '%')
            ->orWhere('ctz_Notes', 'LIKE', '%' . request('search') . '%')
            ->orWhere('ctz_CitizenshipName_' . $this->current_language, 'LIKE', '%' . request('search') . '%');
        });

        $citizenship->when(request('country'), function ($q){
            return $q->where('fkCtzCny','=',request('country'));
        });

        return DataTables::of($citizenship)
            ->editColumn('cny_CountryName', function($citizenship){
                return $citizenship->country->cny_CountryName;
            })
            ->editColumn('ctz_Notes', function($citizenship){
                return substr($citizenship->ctz_Notes,0,45);
            })
            ->addColumn('action', function ($citizenship) {
                $str = '<a cid="'.$citizenship->pkCtz.'" onclick="triggerEdit('.$citizenship->pkCtz.')" href="javascript:void(0)"><i class="fa fa-pen"></i></a>&nbsp;&nbsp';
                $str .= '<a onclick="triggerDelete('.$citizenship->pkCtz.')" href="javascript:void(0)"><i class="fa fa-trash"></i></a>';
                return $str;
            })
            ->addIndexColumn()
            ->escapeColumns()
            ->toJSON();
    }

}
