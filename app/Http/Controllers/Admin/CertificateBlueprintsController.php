<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\CertificateBluePrints;
use App\Http\Requests\CertificateBluePrintsRequest;
use UtilHelper;
use DataTableService;
use Datatables;
use HtmlBuilder;
use HttpResponse;

class CertificateBlueprintsController extends Controller
{
    public function index(HtmlBuilder $builder)
    {
        $builder_data['columns'] = [
            ['data' => 'DT_RowIndex', 'name' => 'DT_RowIndex', 'title' => trans('general.sr_no'), 'render' => null, 'orderable' => false, 'searchable' => false],
            ['data' => 'cbp_BluePrintName', 'name' => 'cbp_BluePrintName', 'title' => trans('general.gn_name')],
            ['data' => 'cbp_startdate', 'name' => 'cbp_startdate', 'title' => trans('general.gn_start_date'), 'orderable' => false],
            ['data' => 'cbp_enddate', 'name' => 'cbp_enddate', 'title' => trans('general.gn_end_date'), 'orderable' => false],
            ['data' => 'cbp_Cbpstatus', 'name' => 'cbp_Cbpstatus', 'title' => trans('general.gn_status')],
            ['data' => 'action', 'name' => 'action', 'title' => trans('general.gn_actions'), 'orderable' => false, 'searchable' => false, 'width' => '10%'],
        ];

        $builder_data['ajax'] = [
            'url'=> route('certificateblueprints.list'),
            'data' => 'function(d) {
                d.search =  $("#search_certificate_blueprints").val();
            }'
        ];

        $dt_html = new DataTableService($builder_data, $builder);
        $dt_html = $dt_html->builder;

        return view('admin.certificateBlueprints.certificateBlueprints',compact('dt_html'));
    }

    public function create()
    {
        return view('admin.certificateBlueprints.addcertificateBlueprints');
    }

    public function edit($id)
    {
        $data = CertificateBluePrints::findorFail($id);
        return view('admin.certificateBlueprints.editcertificateBlueprints', ['data' => $data]);
    }

    public function store(CertificateBluePrintsRequest $request)
    {
        $column = 'cbp_BluePrintName_'.$this->current_language;
        $value = $request[$column];

        if (!empty($request->pkCbp)) {

            $checkPrev = CertificateBluePrints::where($column,$value)
                    ->where('pkCbp', '!=', $request->pkCbp)->count();
            if ($checkPrev) {
                $status_code = HttpResponse::HTTP_BAD_REQUEST;
                $response['message'] = trans('message.msg_cb_exists');
            } else {
                CertificateBluePrints::find($request->pkCbp)->update($request->validated());
                $response['message'] = trans('message.msg_cb_update_success');
            }
        } else {
            $checkPrev = CertificateBluePrints::where($column,$value)
                    ->count();
            if ($checkPrev) {
                $status_code = HttpResponse::HTTP_BAD_REQUEST;
                $response['message'] = trans('message.msg_cb_exists');
            } else {
                $result = CertificateBluePrints::create($request->validated());
                if (!empty($result)) {
                    $response['message'] = trans('message.msg_cb_add_success');
                } else {
                    $status_code = HttpResponse::HTTP_BAD_REQUEST;
                    $response['message'] = trans('message.msg_something_wrong');
                }
            }
        }

        return response()->json($response,$status_code ?? HttpResponse::HTTP_OK);
    }

    public function destroy($cid)
    {
        $response = [];

        if (empty($cid)) {
            $status_code = HttpResponse::HTTP_BAD_REQUEST;
        } else {
            CertificateBluePrints::where('pkCbp', $cid)->delete();

            $response['message'] = trans('message.msg_cb_delete_success');
        }

        return response()->json($response,$status_code ?? HttpResponse::HTTP_OK);
    }

    /**
     * Get Certificate Blueprints List
     *
     * @return JSON
     */
    public function certificateblueprints_list()
    {
        $certificate_blueprints = CertificateBluePrints::select('*', 'cbp_BluePrintName_' . $this->current_language . ' as cbp_BluePrintName');
        $certificate_blueprints->when(request('search'),function($q){
           return $q->where('cbp_keywords', 'LIKE', '%' . request('search') . '%')
           ->orWhere('cbp_startdate', 'LIKE', '%' . request('search') . '%')
           ->orWhere('cbp_enddate', 'LIKE', '%' . request('search') . '%')
           ->orWhere('cbp_Cbpstatus', 'LIKE', '%' . request('search') . '%')
           ->orWhere('cbp_BluePrintName_' . $this->current_language, 'LIKE', '%' . request('search') . '%');
        });

        return DataTables::of($certificate_blueprints)
            ->editColumn('cbp_enddate', function($certificate_blueprints){
                if(!empty($certificate_blueprints->cbp_enddate)) {
                    return $certificate_blueprints->cbp_enddate;
                } else {
                    return "-";
                }
            })
            ->addColumn('action', function ($certificate_blueprints) {
                $str = '<a href='.url("admin/certificateblueprints/".$certificate_blueprints->pkCbp."/edit").'><i class="fa fa-pen"></i></a>&nbsp;&nbsp';
                $str .= '<a onclick="triggerDelete('.$certificate_blueprints->pkCbp.')" href="javascript:void(0)"><i class="fa fa-trash"></i></a>';
                return $str;
            })
            ->addIndexColumn()
            ->escapeColumns()
            ->toJSON();
    }
}
