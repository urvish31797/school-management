<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\DescriptiveFinalMark;
use App\Http\Requests\DFMRequest;
use DataTableService;
use Datatables;
use HtmlBuilder;
use HttpResponse;

class DFMController extends Controller
{
    public function index(HtmlBuilder $builder)
    {
        $builder_data['columns'] = [
            ['data' => 'DT_RowIndex', 'name' => 'DT_RowIndex', 'title' => trans('general.sr_no'), 'render' => null, 'orderable' => false, 'searchable' => false],
            ['data' => 'dfm_Uid', 'name' => 'dfm_Uid', 'title' => trans('general.gn_uid')],
            ['data' => 'dfm_Text', 'name' => 'dfm_Text', 'title' => trans('general.gn_name')],
            ['data' => 'action', 'name' => 'action', 'title' => trans('general.gn_actions'), 'orderable' => false, 'searchable' => false, 'width' => '10%'],
        ];

        $builder_data['ajax'] = [
            'url'=> route('descriptivefinalmarks.list'),
            'data' => 'function(d) {
                d.search =  $("#search_dfm").val();
            }'
        ];

        $dt_html = new DataTableService($builder_data, $builder);
        $dt_html = $dt_html->builder;

        return view('admin.descriptiveFinalMarks.descriptiveFinalMarks', compact('dt_html'));
    }

    public function store(DFMRequest $request)
    {
        $column = 'dfm_Text_'.$this->current_language;
        $value = $request[$column];

        $response = [];
        if (!empty($request->pkDfm)) {
            $checkPrev = DescriptiveFinalMark::where($column,$value)
                    ->where('pkDfm', '!=', $request->pkDfm)->count();
            if ($checkPrev) {
                $status_code = HttpResponse::HTTP_BAD_REQUEST;
                $response['message'] = trans('message.msg_dfm_exist');
            } else {
                DescriptiveFinalMark::where('pkDfm', $request->pkDfm)->update($request->validated());
                $response['message'] = trans('message.msg_dfm_update_success');
            }
        } else {
            $checkPrev = DescriptiveFinalMark::where($column,$value)
                    ->count();
            if ($checkPrev) {
                $status_code = HttpResponse::HTTP_BAD_REQUEST;
                $response['message'] = trans('message.msg_dfm_exist');
            } else {
                $id = DescriptiveFinalMark::insertGetId($request->validated());
                if (!empty($id)) {
                    DescriptiveFinalMark::where('pkDfm', $id)->update(['dfm_Uid' => "DFM" . $id]);
                    $response['message'] = trans('message.msg_dfm_add_success');

                } else {
                    $status_code = HttpResponse::HTTP_BAD_REQUEST;
                    $response['message'] = trans('message.msg_something_wrong');
                }
            }
        }

        return response()->json($response,$status_code ?? HttpResponse::HTTP_OK);
    }

    public function edit($cid)
    {
        $response = [];
        $cdata = DescriptiveFinalMark::where('pkDfm', '=', $cid)->first();

        if (empty($cid) || empty($cdata)) {
            $status_code = HttpResponse::HTTP_BAD_REQUEST;
        } else {

            $response['data'] = $cdata;
        }

        return response()->json($response,$status_code ?? HttpResponse::HTTP_OK);
    }

    public function destroy($cid)
    {
        $response = [];

        if (empty($cid)) {
            $status_code = HttpResponse::HTTP_BAD_REQUEST;
        } else {
            $Admin = 0;
            $State = 0;

            if ($Admin != 0 || $State != 0) {
                $status_code = HttpResponse::HTTP_BAD_REQUEST;
                $response['message'] = trans('message.msg_dfm_delete_prompt');
            } else {
                DescriptiveFinalMark::where('pkDfm', $cid)->delete();

                $response['message'] = trans('message.msg_dfm_delete_success');
            }
        }

        return response()->json($response,$status_code ?? HttpResponse::HTTP_OK);
    }

    /**
     * Desciptive Final Mark Listings
     *
     * @param \Illuminate\Http\Request $request
     * @return JSON
     */
    public function descriptivefinalmarks_list()
    {
        $descriptive = DescriptiveFinalMark::select('*', 'dfm_Text_' . $this->current_language . ' as dfm_Text');

        $descriptive->when(request('search'),function($q){
            return $q->where('dfm_Uid', 'LIKE', '%' . request('search') . '%')
            ->orWhere('dfm_Text_' . $this->current_language, 'LIKE', '%' . request('search') . '%');
        });

        return DataTables::of($descriptive)
            ->editColumn('dfm_Text',function($descriptive){
                return substr($descriptive->dfm_Text,0,45);
            })
            ->addColumn('action', function ($descriptive) {
                $str = '<a cid="'.$descriptive->pkDfm.'" onclick="triggerEdit('.$descriptive->pkDfm.')" href="javascript:void(0)"><i class="fa fa-pen"></i></a>&nbsp;&nbsp';
                $str .= '<a onclick="triggerDelete('.$descriptive->pkDfm.')" href="javascript:void(0)"><i class="fa fa-trash"></i></a>';
                return $str;
            })
            ->addIndexColumn()
            ->escapeColumns()
            ->toJSON();

    }

}
