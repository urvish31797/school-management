<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Admin;
use App\Models\Canton;
use App\Models\Municipality;
use App\Models\State;
use App\Http\Requests\CantonRequest;
use DataTableService;
use Datatables;
use HtmlBuilder;
use HttpResponse;

class CantonController extends Controller
{
    public function index(HtmlBuilder $builder)
    {
        $builder_data['columns'] = [
            ['data' => 'DT_RowIndex', 'name' => 'DT_RowIndex', 'title' => trans('general.sr_no'), 'render' => null, 'orderable' => false, 'searchable' => false],
            ['data' => 'can_Uid', 'name' => 'can_Uid', 'title' => trans('general.gn_uid')],
            ['data' => 'can_CantonName', 'name' => 'can_CantonName', 'title' => trans('general.gn_name')],
            ['data' => 'sta_StateName', 'name' => 'sta_StateName', 'title' => trans('general.gn_state'), 'orderable' => false],
            ['data' => 'cny_CountryName', 'name' => 'cny_CountryName', 'title' => trans('general.gn_country'), 'orderable' => false],
            ['data' => 'can_Note', 'name' => 'can_Note', 'title' => trans('general.gn_notes')],
            ['data' => 'can_Status', 'name' => 'can_Status', 'title' => trans('general.gn_status')],
            ['data' => 'action', 'name' => 'action', 'title' => trans('general.gn_actions'), 'orderable' => false, 'searchable' => false, 'width' => '10%'],
        ];

        $builder_data['ajax'] = [
            'url'=> route('canton.list'),
            'data' => 'function(d) {
                d.search =  $("#search_canton").val();
                d.state = $("#state_filter").val();
                d.country = $("#country_filter").val();
            }'
        ];

        $dt_html = new DataTableService($builder_data, $builder);
        $dt_html = $dt_html->builder;

        return view('admin.cantons.cantons', compact('dt_html'));
    }

    public function store(CantonRequest $request)
    {
        $response = [];
        $column = 'can_CantonName_'.$this->current_language;
        $value = $request[$column];
        if (!empty($request->pkCan)) {
            $checkPrev = Canton::where($column,$value)
                    ->where('pkCan', '!=', $request->pkCan)
                    ->count();
            if ($checkPrev) {
                $status_code = HttpResponse::HTTP_BAD_REQUEST;
                $response['message'] = trans('message.msg_canton_exist');
            } else {
                Canton::where('pkCan', $request->pkCan)->update($request->validated());
                $response['message'] = trans('message.msg_canton_update_success');
            }
        } else {
            $checkPrev = Canton::where($column,$value)->count();
            if ($checkPrev) {
                $status_code = HttpResponse::HTTP_BAD_REQUEST;
                $response['message'] = trans('message.msg_canton_exist');
            } else {
                $id = Canton::insertGetId($request->validated());
                if (!empty($id)) {
                    Canton::where('pkCan', $id)->update(['can_Uid' => "CAN" . $id]);
                    $response['message'] = trans('message.msg_canton_add_success');

                } else {
                    $status_code = HttpResponse::HTTP_BAD_REQUEST;
                    $response['message'] = trans('message.msg_something_wrong');
                }
            }
        }

        return response()->json($response,$status_code ?? HttpResponse::HTTP_OK);
    }

    public function edit($id)
    {
        $response = [];
        $cdata = Canton::where('pkCan', '=', $id)->first();
        $sdata = State::select('fkStaCny')->where('pkSta', '=', $cdata->fkCanSta)->first();
        $cdata['selCountry'] = $sdata['fkStaCny'];
        if (empty($id) || empty($cdata)) {
            $status_code = HttpResponse::HTTP_BAD_REQUEST;
        } else {
            $response['data'] = $cdata;
        }

        return response()->json($response,$status_code ?? HttpResponse::HTTP_OK);

    }

    public function destroy($id)
    {
        $response = [];

        if (empty($id)) {
            $status_code = HttpResponse::HTTP_BAD_REQUEST;
        } else {
            $Admin = Admin::where('fkAdmCan', $id)
                ->count();
            $Municipality = Municipality::where('fkMunCan', $id)
                ->count();
            if ($Admin != 0 || $Municipality != 0) {
                $status_code = HttpResponse::HTTP_BAD_REQUEST;
                $response['message'] = trans('message.msg_canton_delete_prompt');
            } else {
                Canton::where('pkCan', $id)->delete();
                $response['message'] = trans('message.msg_canton_delete_success');
            }
        }

        return response()->json($response,$status_code ?? HttpResponse::HTTP_OK);
    }

    /**
     * Get Canton List
     *
     * @param \Illuminate\Http\Request $request
     * @return JSON
     */
    public function canton_list()
    {
        $canton = Canton::select('*','can_CantonName_'.$this->current_language.' AS can_CantonName')
        ->with(['state'=>function($q){
            $q->select('pkSta','fkStaCny','sta_StateName_'.$this->current_language.' AS sta_StateName');
        }
        , 'state.country'=>function($q){
            $q->select('pkCny','cny_CountryName_' . $this->current_language . ' as cny_CountryName');
        }]);

        $canton->when(empty(array_first(request('order'))['column']), function($q){
            return $q->orderBy('pkCan','ASC');
        });

        $canton->when(request('country'), function ($q){
            $q->whereHas('state.country', function ($q) {
                $q->where('pkCny', '=', request('country'));
            });
        });

        $canton->when(request('state'), function ($q){
            $q->whereHas('state', function ($q) {
                $q->where('fkCanSta', '=', request('state'));
            });
        });

        $canton->when(request('search'), function ($q){
            return $q->where('can_Uid', 'LIKE', '%' . request('search') . '%')
            ->orWhere('can_Note', 'LIKE', '%' . request('search') . '%')
            ->orWhere('can_CantonName_' . $this->current_language, 'LIKE', '%' . request('search') . '%');
        });

        return DataTables::of($canton)
            ->editColumn('cny_CountryName', function($canton){
                return $canton->state->country->cny_CountryName;
            })
            ->editColumn('sta_StateName', function($canton){
                return $canton->state->sta_StateName;
            })
            ->editColumn('can_Note', function($canton){
                return substr($canton->can_Note,0,45);
            })
            ->editColumn('can_Status', function($canton){
                if ($canton->can_Status == 'Active') {
                    return trans('general.gn_active');
                } else {
                    return trans('general.gn_inactive');
                }
            })
            ->addColumn('action', function ($canton) {
                $str = '<a cid="'.$canton->pkCan.'" onclick="triggerEdit('.$canton->pkCan.')" href="javascript:void(0)"><i class="fa fa-pen"></i></a>&nbsp;&nbsp';
                $str .= '<a onclick="triggerDelete('.$canton->pkCan.')" href="javascript:void(0)"><i class="fa fa-trash"></i></a>';
                return $str;
            })
            ->addIndexColumn()
            ->escapeColumns()
            ->toJSON();
    }

    /**
     * Get canton By Country
     *
     * @param \Illuminate\Http\Request $request
     * @return JSON
     */
    public function statebycountry_list()
    {
        $response = [];
        if (!empty(request('cid'))) {
            $sdata = State::select('pkSta', 'sta_StateName_' . $this->current_language . ' AS sta_StateName')
                ->where('fkStaCny',request('cid'))->get();
            if (!empty($sdata)) {
                $response['data'] = $sdata;
            } else {
                $status_code = HttpResponse::HTTP_BAD_REQUEST;
            }
        } else {
            $status_code = HttpResponse::HTTP_BAD_REQUEST;
            $response['message'] = trans('message.msg_please_select_country');
        }

        return response()->json($response,$status_code ?? HttpResponse::HTTP_OK);
    }
}
