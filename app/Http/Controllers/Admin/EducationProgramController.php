<?php
namespace App\Http\Controllers\Admin;

use App\Helpers\FrontHelper;
use App\Http\Controllers\Controller;
use App\Models\EducationPlan;
use App\Models\EducationProgram;
use App\Models\SchoolEducationPlanAssignment;
use Illuminate\Http\Request;

class EducationProgramController extends Controller
{
    public function index(Request $request)
    {
        return view('admin.educationPrograms.educationPrograms');
    }

    public function create(Request $request)
    {
        $streams = EducationProgram::select('pkEdp', 'edp_Name_' . $this->current_language . ' as edp_Name', 'edp_ParentId')
            ->get()
            ->toArray();
        $tree = FrontHelper::buildtree($streams);

        return view('admin.educationPrograms.addEducationProgram', ['educationProgram' => $tree]);
    }

    public function edit($id)
    {
        $streams = EducationProgram::select('edp_Name_' . $this->current_language . ' as edp_Name', 'pkEdp', 'edp_ParentId', 'edp_Uid')
            ->get()
            ->toArray();
        $tree = FrontHelper::buildtree($streams);

        $data = EducationProgram::where('pkEdp', $id)->first();

        if (request()
            ->ajax()) {
            return \View::make('admin.educationPrograms.editEducationProgram')
                ->with(['educationProgram' => $tree, 'data' => $data])->renderSections();
        }
        return view('admin.educationPrograms.editEducationProgram', ['educationProgram' => $tree, 'data' => $data]);
    }

    public function store(Request $request)
    {
        $input = $request->all();
        $response = [];
        if (!empty($input['pkEdp'])) {
            $checkPrev = EducationProgram::where('edp_Name_' . $this->current_language, $input['edp_Name_' . $this
                    ->current_language])
                    ->where('pkEdp', '!=', $input['pkEdp'])->first();
            if (!empty($checkPrev)) {
                $response['status'] = false;
                $response['message'] = trans('message.msg_education_program_exist');
            } else {
                EducationProgram::where('pkEdp', $input['pkEdp'])->update($input);
                $response['status'] = true;
                $response['message'] = trans('message.msg_education_program_update_success');
            }
        } else {
            $checkPrev = EducationProgram::where('edp_Name_' . $this->current_language, $input['edp_Name_' . $this
                    ->current_language])
                    ->first();
            if (!empty($checkPrev)) {
                $response['status'] = false;
                $response['message'] = trans('message.msg_education_program_exist');
            } else {
                $id = EducationProgram::insertGetId($input);
                if (!empty($id)) {
                    EducationProgram::where('pkEdp', $id)->update(['edp_Uid' => "EPR" . $id]);
                    $response['status'] = true;
                    $response['message'] = trans('message.msg_education_program_add_success');

                } else {
                    $response['status'] = false;
                    $response['message'] = trans('message.msg_something_wrong');
                }
            }
        }

        return response()
            ->json($response);
    }

    public function destroy(Request $request, $cid)
    {
        $response = [];

        if (empty($cid)) {
            $response['status'] = false;
        } else {
            $EducationPlan = EducationPlan::where('fkEplEdp', $cid)
                ->count();
            $SchoolEducationPlanAssignment = SchoolEducationPlanAssignment::where('fkSepEdp', $cid)
                ->count();
            if ($EducationPlan != 0 || $SchoolEducationPlanAssignment != 0) {
                $response['status'] = false;
                $response['message'] = trans('message.msg_education_program_delete_prompt');
            } else {
                EducationProgram::where('pkEdp', $cid)->orWhere('edp_ParentId', $cid)->delete();
                $response['status'] = true;
                $response['message'] = trans('message.msg_education_program_delete_success');
            }
        }

        return response()
            ->json($response);
    }

    /**
     * Get Education Program List
     *
     * @param \Illuminate\Http\Request $request
     * @return JSON
     */
    public function educationprogram_list(Request $request)
    {
        $data = $request->all();

        $perpage = !empty($data['length']) ? (int) $data['length'] : 10;

        $filter = isset($data['search']) && is_string($data['search']) ? $data['search'] : '';

        $sort_type = isset($data['order'][0]['dir']) && is_string($data['order'][0]['dir']) ? $data['order'][0]['dir'] : '';

        $sort_col = $data['order'][0]['column'];

        $sort_field = $data['columns'][$sort_col]['data'];

        $EducationProgram = new EducationProgram;

        if ($filter) {
            $EducationProgram = $EducationProgram->where('edp_Uid', 'LIKE', '%' . $filter . '%')->orWhere('edp_Name_' . $this->current_language, 'LIKE', '%' . $filter . '%')->orWhere('edp_Notes', 'LIKE', '%' . $filter . '%');
        }
        $EducationProgramQuery = $EducationProgram;

        if ($sort_col != 0) {
            $EducationProgramQuery = $EducationProgramQuery->orderBy($sort_field, $sort_type);
        }

        $total_EducationProgram = $EducationProgramQuery->count();

        $offset = $data['start'];

        $counter = $offset;
        $EducationProgramData = [];
        $EducationPrograms = $EducationProgramQuery->select('edp_Name_' . $this->current_language . ' as edp_Name', 'pkEdp', 'edp_ParentId', 'edp_Uid', 'edp_Notes')
            ->offset($offset)->limit($perpage)->with('parent')
            ->get()
            ->toArray();

        foreach ($EducationPrograms as $key => $value) {
            $value['index'] = $counter + 1;
            $EducationProgramData[$counter] = $value;
            $counter++;
        }

        $price = array_column($EducationProgramData, 'index');

        if ($sort_col == 0) {
            if ($sort_type == 'desc') {
                array_multisort($price, SORT_DESC, $EducationProgramData);
            } else {
                array_multisort($price, SORT_ASC, $EducationProgramData);
            }
        }
        $EducationProgramData = array_values($EducationProgramData);
        $result = array(
            "draw" => $data['draw'],
            "recordsTotal" => $total_EducationProgram,
            "recordsFiltered" => $total_EducationProgram,
            'data' => $EducationProgramData,
        );

        return response()->json($result);
    }

}
