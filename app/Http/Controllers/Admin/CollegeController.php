<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\College;
use App\Models\EmployeesEducationDetail;
use App\Http\Traits\FileTrait;
use App\Http\Requests\CollegeRequest;
use DataTableService;
use Datatables;
use HtmlBuilder;
use HttpResponse;

class CollegeController extends Controller
{
    use FileTrait;
    public function index(HtmlBuilder $builder)
    {
        $builder_data['columns'] = [
            ['data' => 'DT_RowIndex', 'name' => 'DT_RowIndex', 'title' => trans('general.sr_no'), 'render' => null, 'orderable' => false, 'searchable' => false],
            ['data' => 'col_Uid', 'name' => 'col_Uid', 'title' => trans('general.gn_uid')],
            ['data' => 'col_CollegeName', 'name' => 'col_CollegeName', 'title' => trans('general.gn_name')],
            ['data' => 'uni_UniversityName', 'name' => 'uni_UniversityName', 'title' => trans('general.gn_university'), 'orderable' => false],
            ['data' => 'cny_CountryName', 'name' => 'cny_CountryName', 'title' => trans('general.gn_country'), 'orderable' => false],
            ['data' => 'col_YearStartedFounded', 'name' => 'col_YearStartedFounded', 'title' => trans('general.gn_started_year')],
            ['data' => 'oty_OwnershipTypeName', 'name' => 'oty_OwnershipTypeName', 'title' => trans('general.gn_ownership_type'), 'orderable' => false],
            ['data' => 'col_Notes', 'name' => 'col_Notes', 'title' => trans('general.gn_notes')],
            ['data' => 'action', 'name' => 'action', 'title' => trans('general.gn_actions'), 'orderable' => false, 'searchable' => false, 'width' => '10%'],
        ];

        $builder_data['ajax'] = [
            'url' => route('colleges.list'),
            'data' => 'function(d) {
                d.search = $("#search_college").val();
                d.year = $("#year_filter").val();
                d.country = $("#country_filter").val();
                d.ownership = $("#ownership_filter").val();
                d.university = $("#university_filter").val();
            }'
        ];

        $dt_html = new DataTableService($builder_data, $builder);
        $dt_html = $dt_html->builder;

        return view('admin.colleges.colleges', compact('dt_html'));
    }

    public function store(CollegeRequest $request)
    {
        $response = [];
        $column = 'col_CollegeName_'.$this->current_language;
        $value = $request[$column];
        if (!empty($request->pkCol)) {
            $college_id = $request->pkCol;
            $checkPrev = College::where($column,$value)->where('pkCol', '!=', $request->pkCol)->count();
            if ($checkPrev) {
                $status_code = HttpResponse::HTTP_BAD_REQUEST;
                $response['message'] = trans('message.msg_college_exist');
            } else {
                College::where('pkCol', $request->pkCol)->update($request->validated());
                $response['message'] = trans('message.msg_college_update_success');
            }
        } else {
            $checkPrev = College::where($column,$value)->count();
            if ($checkPrev) {
                $status_code = HttpResponse::HTTP_BAD_REQUEST;
                $response['message'] = trans('message.msg_college_exist');
            } else {
                $id = College::insertGetId($request->validated());
                if (!empty($id)) {
                    $college_id = $id;
                    College::where('pkCol', $id)->update(['col_Uid' => "COL" . $id]);
                    $response['message'] = trans('message.msg_college_add_success');
                } else {
                    $status_code = HttpResponse::HTTP_BAD_REQUEST;
                    $response['message'] = trans('message.msg_something_wrong');
                }
            }
        }

        if($request->file('upload_profile') && !empty($college_id)){

            $college = College::where('pkCol',$college_id)->first();
            if (!empty($college->col_PicturePath)) {
                $unlink_file = $college->col_PicturePath;
            }

            $upload_params = [
                'folder_path'=>config('assetpath.colleges_images'),
                'unlink_file'=>$unlink_file ?? ''
            ];
            $image = $request->file('upload_profile');
            $filename = $this->uploadFiletoStorage($image,$upload_params);

            if(!empty($filename)){
                College::where('pkCol',$college_id)->update(['col_PicturePath'=>$filename]);
            }
        }

        return response()->json($response,$status_code ?? HttpResponse::HTTP_OK);
    }

    public function edit($cid)
    {
        $response = [];
        $cdata = College::where('pkCol', '=', $cid)->first();
        if (empty($cid) || empty($cdata)) {
            $status_code = HttpResponse::HTTP_BAD_REQUEST;
        } else {
            if (!empty($cdata->col_PicturePath)) {
                $cdata->col_PicturePath = asset(config('assetpath.colleges_images')) . '/' . $cdata->col_PicturePath;
            }
            $response['data'] = $cdata;
        }

        return response()->json($response,$status_code ?? HttpResponse::HTTP_OK);

    }

    public function destroy($cid)
    {
        $response = [];

        if (empty($cid)) {
            $status_code = HttpResponse::HTTP_BAD_REQUEST;
        } else {

            $eed = EmployeesEducationDetail::where('fkEedCol', $cid)
                ->count();

            if ($eed != 0) {
                $status_code = HttpResponse::HTTP_BAD_REQUEST;
                $response['message'] = trans('message.msg_college_delete_prompt');
            } else {
                College::where('pkCol', $cid)->delete();

                $response['message'] = trans('message.msg_college_delete_success');
            }
        }

        return response()->json($response,$status_code ?? HttpResponse::HTTP_OK);
    }

    /**
     * Get Colleges List
     *
     * @param \Illuminate\Http\Request $request
     * @return JSON
     */
    public function colleges_list()
    {
        $college = College::select('*', 'col_CollegeName_' . $this->current_language . ' as col_CollegeName')
        ->with(['ownershipType' => function ($query) {
                $query->select('pkOty', 'oty_OwnershipTypeName_' . $this->current_language . ' as oty_OwnershipTypeName');
            }
            , 'country' => function ($query) {
                $query->select('pkCny', 'cny_CountryName_' . $this->current_language . ' as cny_CountryName');
            }
            , 'university' => function ($query) {
                $query->select('pkUni', 'uni_UniversityName_' . $this->current_language . ' as uni_UniversityName');
            },
        ]);

        $college->when(request('search'), function ($q){
            return $q->where('col_Uid', 'LIKE', '%' . request('search') . '%')
            ->orWhere('col_Notes', 'LIKE', '%' . request('search') . '%')
            ->orWhere('col_CollegeName_' . $this->current_language, 'LIKE', '%' . request('search') . '%');
        });

        $college->when(request('country'), function ($q){
            return $q->where('fkColCny', '=', request('country'));
        });

        $college->when(request('ownership'), function ($q){
            return $q->where('fkColOty', '=', request('ownership'));
        });

        $college->when(request('university'), function ($q){
            return $q->where('fkColUni', '=', request('university'));
        });

        $college->when(request('year'), function ($q){
            return $q->where('col_YearStartedFounded', '=', request('year'));
        });

        return DataTables::of($college)
            ->editColumn('oty_OwnershipTypeName', function($college){
                return $college->ownershipType->oty_OwnershipTypeName;
            })
            ->editColumn('cny_CountryName', function($college){
                return $college->country->cny_CountryName;
            })
            ->editColumn('uni_UniversityName', function($college){
                return $college->university->uni_UniversityName ?? '-';
            })
            ->editColumn('col_Notes', function($college){
                return substr($college->col_Notes,0,45);
            })
            ->addColumn('action', function ($college) {
                $str = '<a cid="'.$college->pkCol.'" onclick="triggerEdit('.$college->pkCol.')" href="javascript:void(0)"><i class="fa fa-pen"></i></a>&nbsp;&nbsp';
                $str .= '<a onclick="triggerDelete('.$college->pkCol.')" href="javascript:void(0)"><i class="fa fa-trash"></i></a>';
                return $str;
            })
            ->addIndexColumn()
            ->escapeColumns()
            ->toJSON();
    }
}
