<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\AttendanceWay;
use App\Http\Requests\AttendanceWayRequest;
use DataTableService;
use Datatables;
use HtmlBuilder;
use HttpResponse;

class AttendanceWayController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(HtmlBuilder $builder)
    {
        $builder_data['columns'] = [
            ['data' => 'DT_RowIndex', 'name' => 'DT_RowIndex', 'title' => trans('general.sr_no'), 'render' => null, 'orderable' => false, 'searchable' => false],
            ['data' => 'aw_Uid', 'name' => 'aw_Uid', 'title' => trans('general.gn_uid')],
            ['data' => 'aw_Name', 'name' => 'aw_Name', 'title' => trans('general.gn_name')],
            ['data' => 'aw_Note', 'name' => 'aw_Note', 'title' => trans('general.gn_notes')],
            ['data' => 'aw_Default', 'name' => 'aw_Default', 'title' => trans('general.gn_default')],
            ['data' => 'action', 'name' => 'action', 'title' => trans('general.gn_actions'), 'orderable' => false, 'searchable' => false, 'width' => '10%'],
        ];

        $builder_data['ajax'] = [
            'url'=> route('attendanceway.list'),
            'data' => 'function(d) {
                d.search =  $("#search_attendance_way").val();
            }'
        ];

        $dt_html = new DataTableService($builder_data, $builder);
        $dt_html = $dt_html->builder;

        return view('admin.attendanceWay.attendanceWay',compact('dt_html'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function store(AttendanceWayRequest $request)
    {
        $column = 'aw_Name_'.$this->current_language;
        $value = $request[$column];

        $response = [];
        if (!empty($request->pkAw)) {
            $checkPrev = AttendanceWay::where($column,$value)
                    ->where('pkAw', '!=', $request->pkAw)->count();
            if ($checkPrev) {
                $status_code = HttpResponse::HTTP_BAD_REQUEST;
                $response['message'] = trans('message.msg_aw_exists');
            } else {
                AttendanceWay::where('pkAw', $request->pkAw)->update($request->validated());

                $response['message'] = trans('message.msg_aw_update_success');
            }
        } else {
            $checkPrev = AttendanceWay::where($column,$value)
                    ->count();
            if ($checkPrev) {
                $status_code = HttpResponse::HTTP_BAD_REQUEST;
                $response['message'] = trans('message.msg_aw_exists');
            } else {
                $id = AttendanceWay::insertGetId($request->validated());
                if (!empty($id)) {
                    AttendanceWay::where('pkAw', $id)->update(['aw_Uid' => "AW" . $id]);
                    $response['message'] = trans('message.msg_aw_add_success');
                } else {
                    $status_code = HttpResponse::HTTP_BAD_REQUEST;
                    $response['message'] = trans('message.msg_something_wrong');
                }
            }
        }

        return response()->json($response,$status_code ?? HttpResponse::HTTP_OK);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($cid)
    {
        $response = [];
        $cdata = AttendanceWay::where('pkAw', '=', $cid)->first();

        if (empty($cid) || empty($cdata)) {
            $status_code = HttpResponse::HTTP_BAD_REQUEST;
        } else {
            $response['data'] = $cdata;
        }

        return response()->json($response,$status_code ?? HttpResponse::HTTP_OK);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($cid)
    {
        $response = [];
        if (empty($cid)) {
            $status_code = HttpResponse::HTTP_BAD_REQUEST;
        } else {
            AttendanceWay::where('pkAw', $cid)->delete();
            $response['message'] = trans('message.msg_attendanceway_delete_success');
        }

        return response()->json($response,$status_code ?? HttpResponse::HTTP_OK);
    }

    /**
     * Get Attendance ways list
     *
     * @param \Illuminate\Http\Request $request
     * @return JSON
     */
    public function attendanceway_list()
    {
        $attendance_way = AttendanceWay::select('*', 'aw_Name_' . $this->current_language . ' as aw_Name');
        $attendance_way->when(request('search'),function($q){
            return $q->where('aw_Uid', 'LIKE', '%' . request('search') . '%')
            ->orWhere('aw_Note', 'LIKE', '%' . request('search') . '%')
            ->orWhere('aw_Name_' . $this->current_language, 'LIKE', '%' . request('search') . '%');
         });

        return DataTables::of($attendance_way)
        ->editColumn('aw_Default',function($attendance_way){
            if($attendance_way->aw_Default == 1){
                return '<a class="btn btn-success btn-sm" href="javascript:void(0)">'.trans('general.gn_default').'</a>';
            } else {
                return '<a class="btn btn-danger btn-sm" href="javascript:void(0)" onclick="changeDefaultModal('.$attendance_way->pkAw.')">'.trans('general.gn_not_default').'</a>';
            }
        })
        ->addColumn('action', function ($attendance_way) {
            $str = '<a onclick="triggerEdit(' . $attendance_way->pkAw . ')" href="javascript:void(0)"><i class="fa fa-pen"></i></a>&nbsp;&nbsp';
            $str .= '<a onclick="triggerDelete('.$attendance_way->pkAw.')" href="javascript:void(0)"><i class="fa fa-trash"></i></a>';
            return $str;
        })
        ->rawColumns(['aw_Default','action'])
        ->addIndexColumn()
        ->escapeColumns()
        ->toJSON();
    }

    /**
     * Change Default Attendance Way
     *
     * @param \Illuminate\Http\Request $request
     * @return JSON
     */
    public function change_attendanceway()
    {
        $id = request('id');
        $response = [];

        if (empty($id)) {
            $status_code = HttpResponse::HTTP_BAD_REQUEST;
            $response['message'] = trans('message.msg_something_wrong');
        } else {
            $existActive = AttendanceWay::where('aw_Default', 1)->first();
            if (!empty($existActive)) {
                $existActive->aw_Default = 0;
                $existActive->save();
            }

            AttendanceWay::where('pkAw', $id)->update(['aw_Default' => 1]);
            $response['message'] = trans('message.msg_attendance_way_active_success');
        }

        return response()->json($response,$status_code ?? HttpResponse::HTTP_OK);
    }
}
