<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Employee;
use App\Models\PostalCode;
use App\Models\School;
use App\Models\VillageSchool;
use App\Http\Requests\PostalCodeRequest;
use DataTableService;
use Datatables;
use HtmlBuilder;
use HttpResponse;

class PostalCodeController extends Controller
{
    public function index(HtmlBuilder $builder)
    {
        $builder_data['columns'] = [
            ['data' => 'DT_RowIndex', 'name' => 'DT_RowIndex', 'title' => trans('general.sr_no'), 'render' => null, 'orderable' => false, 'searchable' => false],
            ['data' => 'pof_Uid', 'name' => 'pof_Uid', 'title' => trans('general.gn_uid')],
            ['data' => 'pof_PostOfficeName', 'name' => 'pof_PostOfficeName', 'title' => trans('general.gn_name')],
            ['data' => 'pof_PostOfficeNumber', 'name' => 'pof_PostOfficeNumber', 'title' => trans('sidebar.sidebar_nav_postal_code')],
            ['data' => 'mun_MunicipalityName', 'name' => 'mun_MunicipalityName', 'title' => trans('general.gn_town'), 'orderable' => false, 'searchable' => false],
            ['data' => 'pof_Notes', 'name' => 'pof_Notes', 'title' => trans('general.gn_notes')],
            ['data' => 'action', 'name' => 'action', 'title' => trans('general.gn_actions'), 'orderable' => false, 'searchable' => false, 'width' => '10%'],
        ];

        $builder_data['ajax'] = [
            'url'=> route('postalcodes.list'),
            'data' => 'function(d) {
                d.search = $("#search_postalCode").val();
                d.town = $("#town_filter").val();
            }'
        ];

        $dt_html = new DataTableService($builder_data, $builder);
        $dt_html = $dt_html->builder;

        return view('admin.postalCodes.postalCodes', compact('dt_html'));
    }

    public function store(PostalCodeRequest $request)
    {
        $response = [];
        $column = 'pof_PostOfficeName_'.$this->current_language;
        $value = $request[$column];
        if (!empty($request->pkPof)) {
            $checkPrev = PostalCode::where($column, $value)
                    ->where('pkPof', '!=', $request->pkPof)->count();
            $checkPrevCode = PostalCode::where('pof_PostOfficeNumber', $request->pof_PostOfficeNumber)
            ->where('pkPof', '!=', $request->pkPof)->count();
            if ($checkPrev) {
                $status_code = HttpResponse::HTTP_BAD_REQUEST;
                $response['message'] = trans('message.msg_postal_name_exist');
            } else if ($checkPrevCode) {
                $status_code = HttpResponse::HTTP_BAD_REQUEST;
                $response['message'] = trans('message.msg_postal_code_exist');
            } else {
                PostalCode::where('pkPof', $request->pkPof)->update($request->validated());
                $response['message'] = trans('message.msg_postal_code_update_success');
            }
        } else {
            $checkPrev = PostalCode::where($column,$value)
                    ->count();
            $checkPrevCode = PostalCode::where('pof_PostOfficeNumber', $request->pof_PostOfficeNumber)
                    ->count();
            if ($checkPrev) {
                $status_code = HttpResponse::HTTP_BAD_REQUEST;
                $response['message'] = trans('message.msg_postal_name_exist');
            } else if ($checkPrevCode) {
                $status_code = HttpResponse::HTTP_BAD_REQUEST;
                $response['message'] = trans('message.msg_postal_code_exist');
            } else {
                $id = PostalCode::insertGetId($request->validated());
                if (!empty($id)) {
                    PostalCode::where('pkPof', $id)->update(['pof_Uid' => "ZIP" . $id]);
                    $response['message'] = trans('message.msg_postal_code_add_success');
                } else {
                    $status_code = HttpResponse::HTTP_BAD_REQUEST;
                    $response['message'] = trans('message.msg_something_wrong');
                }
            }
        }

        return response()->json($response,$status_code ?? HttpResponse::HTTP_OK);
    }

    public function edit($id)
    {
        $response = [];
        $cdata = PostalCode::where('pkPof', '=', $id)->first();

        if (empty($id) || empty($cdata)) {
            $status_code = HttpResponse::HTTP_BAD_REQUEST;
        } else {
            $response['data'] = $cdata;
        }

        return response()->json($response,$status_code ?? HttpResponse::HTTP_OK);

    }

    public function destroy($id)
    {
        $response = [];
        if (empty($id)) {
            $status_code = HttpResponse::HTTP_BAD_REQUEST;
        } else {
            $school = School::where('fkSchPof', $id)
                ->count();
            $employee = Employee::where('fkEmpPof', $id)
                ->count();
            $villageSchool = VillageSchool::where('fkVscPof', $id)
                ->count();
            if ($school != 0 || $employee != 0 || $villageSchool != 0) {
                $status_code = HttpResponse::HTTP_BAD_REQUEST;
                $response['message'] = trans('message.msg_postal_code_delete_prompt');
            } else {
                PostalCode::where('pkPof', $id)->delete();

                $response['message'] = trans('message.msg_postal_code_delete_success');
            }
        }

        return response()->json($response,$status_code ?? HttpResponse::HTTP_OK);
    }

    /**
     * Get Postal Codes Listings
     *
     * @param \Illuminate\Http\Request $request
     * @return JSON
     */
    public function postalcodes_list()
    {
        $postal_code = PostalCode::select('pkPof', 'pof_Uid', 'pof_PostOfficeName_' . $this->current_language . ' as pof_PostOfficeName', 'fkPofMun', 'pof_PostOfficeNumber', 'pof_Notes')
        ->with([
            'municipality' => function ($query) {
                $query->select('pkMun', 'mun_MunicipalityName_' . $this->current_language . ' as mun_MunicipalityName');
            },
        ]);

        $postal_code->when(request('search'), function ($q){
            return $q->where('pof_Uid', 'LIKE', '%' . request('search') . '%')
            ->orWhere('pof_PostOfficeName_' . $this->current_language, 'LIKE', '%' . request('search') . '%')
            ->orWhere('pof_Notes', 'LIKE', '%' . request('search') . '%')
            ->orWhere('pof_PostOfficeNumber', 'LIKE', '%' . request('search') . '%');
        });

        $postal_code->when(request('town'), function ($q){
            return $q->where('fkPofMun', '=', request('town'));
        });

        return DataTables::of($postal_code)
            ->editColumn('mun_MunicipalityName', function($postal_code){
                return $postal_code->municipality->mun_MunicipalityName;
            })
            ->editColumn('sta_Note', function($postal_code){
                return substr($postal_code->sta_Note,0,45);
            })
            ->editColumn('sta_Status', function($postal_code){
                if ($postal_code->sta_Status == 'Active') {
                    return trans('general.gn_active');
                } else {
                    return trans('general.gn_inactive');
                }
            })
            ->addColumn('action', function ($postal_code) {
                $str = '<a cid="'.$postal_code->pkPof.'" onclick="triggerEdit('.$postal_code->pkPof.')" href="javascript:void(0)"><i class="fa fa-pen"></i></a>&nbsp;&nbsp';
                $str .= '<a onclick="triggerDelete('.$postal_code->pkPof.')" href="javascript:void(0)"><i class="fa fa-trash"></i></a>';
                return $str;
            })
            ->addIndexColumn()
            ->escapeColumns()
            ->toJSON();

    }

}
