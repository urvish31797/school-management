<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Classes;
use App\Http\Requests\ClassRequest;
use DataTableService;
use Datatables;
use HtmlBuilder;
use HttpResponse;

class ClassController extends Controller
{
    public function index(HtmlBuilder $builder)
    {
        $builder_data['columns'] = [
            ['data' => 'DT_RowIndex', 'name' => 'DT_RowIndex', 'title' => trans('general.sr_no'), 'render' => null, 'orderable' => false, 'searchable' => false],
            ['data' => 'cla_Uid', 'name' => 'cla_Uid', 'title' => trans('general.gn_uid')],
            ['data' => 'cla_ClassName', 'name' => 'cla_ClassName', 'title' => trans('general.gn_name')],
            ['data' => 'cla_Notes', 'name' => 'cla_Notes', 'title' => trans('general.gn_notes')],
            ['data' => 'action', 'name' => 'action', 'title' => trans('general.gn_actions'), 'orderable' => false, 'searchable' => false, 'width' => '10%'],
        ];

        $builder_data['ajax'] = [
            'url' => route('classes.list'),
            'data' => 'function(d) {
                d.search = $("#search_class").val();
            }'
        ];

        $dt_html = new DataTableService($builder_data, $builder);
        $dt_html = $dt_html->builder;

        return view('admin.classes.classes', compact('dt_html'));
    }

    public function store(ClassRequest $request)
    {
        $response = [];
        $column = 'cla_ClassName_'.$this->current_language;
        $value = $request[$column];

        if (!empty($request->pkCla)) {
            $checkPrev = Classes::where($column,$value)->where('pkCla', '!=', $request->pkCla)->count();
            if ($checkPrev) {
                $status_code = HttpResponse::HTTP_BAD_REQUEST;
                $response['message'] = trans('message.msg_class_exist');
            } else {
                Classes::where('pkCla', $request->pkCla)->update($request->validated());
                $response['message'] = trans('message.msg_class_update_success');
            }
        } else {
            $checkPrev = Classes::where($column,$value)
                    ->count();
            if ($checkPrev) {
                $status_code = HttpResponse::HTTP_BAD_REQUEST;
                $response['message'] = trans('message.msg_class_exist');
            } else {
                $id = Classes::insertGetId($request->validated());
                if (!empty($id)) {
                    Classes::where('pkCla', $id)->update(['cla_Uid' => "CLA" . $id]);
                    $response['message'] = trans('message.msg_class_add_success');

                } else {
                    $status_code = HttpResponse::HTTP_BAD_REQUEST;
                    $response['message'] = trans('message.msg_something_wrong');
                }
            }
        }

        return response()->json($response,$status_code ?? HttpResponse::HTTP_OK);
    }

    public function edit($id)
    {
        $response = [];
        $cdata = Classes::where('pkCla', '=', $id)->first();

        if (empty($id) || empty($cdata)) {
            $status_code = HttpResponse::HTTP_BAD_REQUEST;
        } else {
            $response['data'] = $cdata;
        }

        return response()->json($response,$status_code ?? HttpResponse::HTTP_OK);
    }

    public function destroy($id)
    {
        $response = [];

        if (empty($id)) {
            $status_code = HttpResponse::HTTP_BAD_REQUEST;
        } else {
            $eed = 0;
            if ($eed != 0) {
                $status_code = HttpResponse::HTTP_BAD_REQUEST;
                $response['message'] = trans('message.msg_class_delete_prompt');
            } else {
                Classes::where('pkCla', $id)->delete();
                $response['message'] = trans('message.msg_class_delete_success');
            }
        }

        return response()->json($response,$status_code ?? HttpResponse::HTTP_OK);
    }

    public function classes_list()
    {
        $class = Classes::select('*', 'cla_ClassName_' . $this->current_language . ' as cla_ClassName');
        $class->when(request('search'), function ($q){
            return $q->where('cla_Uid', 'LIKE', '%' . request('search') . '%')
            ->orWhere('cla_Notes', 'LIKE', '%' . request('search') . '%')
            ->orWhere('cla_ClassName_' . $this->current_language, 'LIKE', '%' . request('search') . '%');
        });

        return DataTables::of($class)
            ->editColumn('cla_Notes', function($class){
                return substr($class->cla_Notes,0,45);
            })
            ->addColumn('action', function ($class) {
                $str = '<a cid="'.$class->pkCla.'" onclick="triggerEdit('.$class->pkCla.')" href="javascript:void(0)"><i class="fa fa-pen"></i></a>&nbsp;&nbsp';
                $str .= '<a onclick="triggerDelete('.$class->pkCla.')" href="javascript:void(0)"><i class="fa fa-trash"></i></a>';
                return $str;
            })
            ->addIndexColumn()
            ->escapeColumns()
            ->toJSON();
    }

}
