<?php
/**
 * EducationPlanController
 *
 * @package    Laravel
 * @subpackage Controller
 * @since      1.0
 */

namespace App\Http\Controllers\Admin;

use App\Helpers\FrontHelper;
use App\Http\Controllers\Controller;
use App\Http\Traits\CommonTrait;
use App\Models\CertificateBluePrints;
use App\Models\Course;
use App\Models\EducationPlan;
use App\Models\EducationPlansForeignLanguage;
use App\Models\EducationPlansMandatoryCourse;
use App\Models\EducationPlansOptionalCourse;
use App\Models\EducationProfile;
use App\Models\EducationProgram;
use App\Models\EnrollStudent;
use App\Models\Grade;
use App\Models\NationalEducationPlan;
use App\Models\QualificationDegree;
use App\Models\SchoolEducationPlanAssignment;
use Illuminate\Http\Request;

class EducationPlanController extends Controller
{
    use CommonTrait;

    public function index(Request $request)
    {
        return view('admin.educationPlans.educationPlans');
    }

    public function create(Request $request)
    {
        $cantons = $this->getCantonList($this->current_language, $this->logged_user->fkAdmCan);
        $grade = Grade::select('pkGra', 'gra_GradeNumeric', 'gra_GradeNameRoman')
            ->get();
        $course = Course::select('pkCrs', 'crs_CourseName_' . $this->current_language . ' as crs_CourseName', 'crs_CourseAlternativeName')
            ->where('crs_CourseType', '!=', 'DummyOptionalCourse')
            ->where('crs_CourseType', '!=', 'DummyForeignCourse')
            ->get();
        $optionalCoursesGroup = Course::select('pkCrs', 'crs_CourseName_' . $this->current_language . ' as crs_CourseName')
            ->where('crs_CourseType', '=', 'DummyOptionalCourse')
            ->get();
        $foreignLanguageGroup = Course::select('pkCrs', 'crs_CourseName_' . $this->current_language . ' as crs_CourseName')
            ->where('crs_CourseType', '=', 'DummyForeignCourse')
            ->get();
        $streams = EducationProgram::select('pkEdp', 'edp_ParentId', 'edp_Name_' . $this->current_language . ' as edp_Name')
            ->get()
            ->toArray();
        $educationProgram = FrontHelper::buildtree($streams);
        $educationProfile = EducationProfile::select('pkEpr', 'epr_EducationProfileName_' . $this->current_language . ' as epr_EducationProfileName')
            ->get();
        $qualificationDegree = QualificationDegree::select('pkQde', 'qde_QualificationDegreeName_' . $this->current_language . ' as qde_QualificationDegreeName')
            ->get();
        $nationalEducationPlan = NationalEducationPlan::select('pkNep', 'nep_NationalEducationPlanName_' . $this->current_language . ' as nep_NationalEducationPlanName')
            ->get();
        $certificateBluePrints = CertificateBluePrints::select('pkCbp', 'cbp_BluePrintName_' . $this->current_language . ' as cbp_BluePrintName')
            ->get();
        $pagedata = compact('cantons', 'grade', 'course', 'optionalCoursesGroup', 'foreignLanguageGroup', 'educationProgram', 'educationProfile', 'qualificationDegree', 'nationalEducationPlan', 'certificateBluePrints');
        if (request()
            ->ajax()) {
            return \View::make('admin.educationPlans.addEducationPlan')
                ->with($pagedata)->renderSections();
        }
        return view('admin.educationPlans.addEducationPlan', $pagedata);
    }

    public function store(Request $request)
    {
        $input = $request->all();

        if (isset($input['pkEpl']) && !empty($input['pkEpl'])) {
            $response = [];
            $MCGdata = [];
            $OCGdata = [];
            $FCGdata = [];

            unset($input['_1']);
            unset($input['_2']);
            unset($input['_3']);
            unset($input['_4']);
            unset($input['_5']);
            unset($input['_6']);
            unset($input['_7']);
            unset($input['_8']);
            unset($input['_9']);
            unset($input['_10']);
            unset($input['_11']);
            unset($input['_12']);

            $MCG = $input['MCG'];
            $OCG = $input['OCG'] ?? [];
            $FCG = $input['FCG'] ?? [];

            unset($input['MCG']);
            unset($input['OCG']);
            unset($input['FCG']);
            unset($input['gra_sel']);

            $mcg_gra = $input['mcg_gra'];
            $ocg_gra = $input['ocg_gra'] ?? [];
            $fcg_gra = $input['fcg_gra'] ?? [];

            unset($input['mcg_gra']);
            unset($input['ocg_gra']);
            unset($input['fcg_gra']);

            $OCG_order = $input['OCG_order'] ?? [];
            $FCG_order = $input['FCG_order'] ?? [];

            unset($input['OCG_order']);
            unset($input['FCG_order']);

            $mcg_hrs = $input['mcg_hrs'];
            $mcg_weekHourRateTeacher = $input['mcg_weekHourRateTeacher'];
            $mcg_summerHolidayHourRate = $input['mcg_summerHolidayHourRate'];
            $ocg_hrs = $input['ocg_hrs'] ?? [];
            $fcg_hrs = $input['fcg_hrs'] ?? [];
            unset($input['mcg_hrs']);
            unset($input['mcg_weekHourRateTeacher']);
            unset($input['mcg_summerHolidayHourRate']);
            unset($input['ocg_hrs']);
            unset($input['fcg_hrs']);
            unset($input['eoc_order']);
            unset($input['efc_order']);

            $def_mcg = [];
            if (isset($input['default_mcg'])) {
                $def_mcg = $input['default_mcg'];
            }

            $def_ocg = [];
            if (isset($input['default_ocg'])) {
                $def_ocg = $input['default_ocg'];
            }

            $def_fcg = [];
            if (isset($input['default_fcg'])) {
                $def_fcg = $input['default_fcg'];
            }

            unset($input['default_mcg']);
            unset($input['default_ocg']);
            unset($input['default_fcg']);

            unset($input['mcg_select_gra']);
            unset($input['ocg_select_gra']);
            unset($input['fcg_select_gra']);

            $checkPrev = EducationPlan::where('epl_EducationPlanName_' . $this->current_language, $input['epl_EducationPlanName_' . $this
                    ->current_language])
                    ->where('pkEpl', '!=', $input['pkEpl'])->first();
            if (!empty($checkPrev)) {
                $response['status'] = false;
                $response['message'] = trans("sidebar.sidebar_nav_education_plan") . " " . trans("message.msg_name_already_exist");
            } else {
                EducationPlan::where('pkEpl', $input['pkEpl'])->update($input);
                EducationPlansMandatoryCourse::where('fkEmcEpl', $input['pkEpl'])->forceDelete();
                EducationPlansOptionalCourse::where('fkEocEpl', $input['pkEpl'])->forceDelete();
                EducationPlansForeignLanguage::where('fkEflEpl', $input['pkEpl'])->forceDelete();

                foreach ($MCG as $k => $v) {
                    $mcg_def = 'No';
                    if (in_array($v . '_' . $mcg_gra[$k], $def_mcg)) {
                        $mcg_def = 'Yes';
                    }
                    if (!empty($mcg_hrs[$k]) || !empty($mcg_weekHourRateTeacher[$k]) || !empty($mcg_summerHolidayHourRate[$k])) {
                        $MCGdata[] = [
                            'fkEmcEpl' => $input['pkEpl'],
                            'fkEplCrs' => $v,
                            'fkEmcGra' => $mcg_gra[$k],
                            'emc_default' => $mcg_def,
                            'emc_hours' => $mcg_hrs[$k],
                            'emc_weekHourRateTeacher' => $mcg_weekHourRateTeacher[$k],
                            'emc_summerHolidayHourRate' => $mcg_summerHolidayHourRate[$k],
                        ];
                    }
                }

                EducationPlansMandatoryCourse::insert(array_reverse($MCGdata));

                if (count($OCG) > 0) {
                    foreach ($OCG as $k => $v) {
                        $ocg_def = 'No';
                        if (in_array($v . '_' . $ocg_gra[$k], $def_ocg)) {
                            $ocg_def = 'Yes';
                        }
                        $OCGdata[] = ['fkEocEpl' => $input['pkEpl'], 'fkEocCrs' => $v, 'fkEocGra' => $ocg_gra[$k], 'eoc_order' => $OCG_order[$k], 'eoc_default' => $ocg_def, 'eoc_hours' => $ocg_hrs[$k]];
                    }
                    EducationPlansOptionalCourse::insert(array_reverse($OCGdata));
                }

                if (count($FCG) > 0) {
                    foreach ($FCG as $k => $v) {
                        $fcg_def = 'No';
                        if (in_array($v . '_' . $fcg_gra[$k], $def_fcg)) {
                            $fcg_def = 'Yes';
                        }
                        $FCGdata[] = ['fkEflEpl' => $input['pkEpl'], 'fkEflCrs' => $v, 'fkEflGra' => $fcg_gra[$k], 'efc_order' => $FCG_order[$k], 'efc_default' => $fcg_def, 'efc_hours' => $fcg_hrs[$k]];
                    }
                    EducationPlansForeignLanguage::insert(array_reverse($FCGdata));
                }

                $response['status'] = true;
                $response['message'] = trans('message.msg_education_plan_update_success');

            }
        } else {
            $response = [];

            $MCGdata = [];
            $OCGdata = [];
            $FCGdata = [];
            unset($input['_1']);
            unset($input['_2']);
            unset($input['_3']);
            unset($input['_4']);
            unset($input['_5']);
            unset($input['_6']);
            unset($input['_7']);
            unset($input['_8']);
            unset($input['_9']);
            unset($input['_10']);
            unset($input['_11']);
            unset($input['_12']);

            $MCG = $input['MCG'];
            $OCG = $input['OCG'] ?? [];
            $FCG = $input['FCG'] ?? [];

            unset($input['MCG']);
            unset($input['OCG']);
            unset($input['FCG']);

            $mcg_gra = $input['mcg_gra'];
            $ocg_gra = $input['ocg_gra'] ?? [];
            $fcg_gra = $input['fcg_gra'] ?? [];

            unset($input['mcg_gra']);
            unset($input['ocg_gra']);
            unset($input['fcg_gra']);

            $OCG_order = $input['OCG_order'] ?? [];
            $FCG_order = $input['FCG_order'] ?? [];

            unset($input['OCG_order']);
            unset($input['FCG_order']);

            $mcg_hrs = $input['mcg_hrs'];
            $mcg_weekHourRateTeacher = $input['mcg_weekHourRateTeacher'];
            $mcg_summerHolidayHourRate = $input['mcg_summerHolidayHourRate'];
            $ocg_hrs = $input['ocg_hrs'] ?? [];
            $fcg_hrs = $input['fcg_hrs'] ?? [];
            unset($input['mcg_hrs']);
            unset($input['mcg_weekHourRateTeacher']);
            unset($input['mcg_summerHolidayHourRate']);
            unset($input['ocg_hrs']);
            unset($input['fcg_hrs']);
            unset($input['pkEpl']);
            unset($input['eoc_order']);
            unset($input['efc_order']);

            $def_mcg = [];
            if (isset($input['default_mcg'])) {
                $def_mcg = $input['default_mcg'];
            }

            $def_ocg = [];
            if (isset($input['default_ocg'])) {
                $def_ocg = $input['default_ocg'];
            }

            $def_fcg = [];
            if (isset($input['default_fcg'])) {
                $def_fcg = $input['default_fcg'];
            }
            unset($input['default_mcg']);
            unset($input['default_ocg']);
            unset($input['default_fcg']);
            unset($input['gra_sel']);

            unset($input['mcg_select_gra']);
            unset($input['ocg_select_gra']);
            unset($input['fcg_select_gra']);

            $checkPrev = EducationPlan::where('epl_EducationPlanName_' . $this->current_language, $input['epl_EducationPlanName_' . $this
                    ->current_language])
                    ->first();
            if (!empty($checkPrev)) {
                $response['status'] = false;
                $response['message'] = trans("sidebar.sidebar_nav_education_plan") . " " . trans("message.msg_name_already_exist");
            } else {
                $id = EducationPlan::insertGetId($input);
                if (!empty($id)) {
                    EducationPlan::where('pkEpl', $id)->update(['epl_Uid' => "EPL" . $id]);

                    foreach ($MCG as $k => $v) {
                        $mcg_def = 'No';
                        if (in_array($v . '_' . $mcg_gra[$k], $def_mcg)) {
                            $mcg_def = 'Yes';
                        }
                        if (!empty($mcg_hrs[$k]) || !empty($mcg_weekHourRateTeacher[$k]) || !empty($mcg_summerHolidayHourRate[$k])) {
                            $MCGdata[] = [
                                'fkEmcEpl' => $id,
                                'fkEplCrs' => $v,
                                'fkEmcGra' => $mcg_gra[$k],
                                'emc_default' => $mcg_def,
                                'emc_hours' => $mcg_hrs[$k],
                                'emc_weekHourRateTeacher' => $mcg_weekHourRateTeacher[$k],
                                'emc_summerHolidayHourRate' => $mcg_summerHolidayHourRate[$k],
                            ];
                        }
                    }

                    EducationPlansMandatoryCourse::insert($MCGdata);
                    if (count($OCG) > 0) {
                        foreach ($OCG as $k => $v) {
                            $ocg_def = 'No';
                            if (in_array($v . '_' . $ocg_gra[$k], $def_ocg)) {
                                $ocg_def = 'Yes';
                            }
                            $OCGdata[] = ['fkEocEpl' => $id, 'fkEocCrs' => $v, 'fkEocGra' => $ocg_gra[$k], 'eoc_order' => $OCG_order[$k], 'eoc_default' => $ocg_def, 'eoc_hours' => $ocg_hrs[$k]];
                        }
                        EducationPlansOptionalCourse::insert($OCGdata);
                    }

                    if (count($FCG) > 0) {
                        foreach ($FCG as $k => $v) {
                            $fcg_def = 'No';
                            if (in_array($v . '_' . $fcg_gra[$k], $def_fcg)) {
                                $fcg_def = 'Yes';
                            }
                            $FCGdata[] = ['fkEflEpl' => $id, 'fkEflCrs' => $v, 'fkEflGra' => $fcg_gra[$k], 'efc_order' => $FCG_order[$k], 'efc_default' => $fcg_def, 'efc_hours' => $fcg_hrs[$k]];
                        }
                        EducationPlansForeignLanguage::insert($FCGdata);
                    }

                    $response['status'] = true;
                    $response['message'] = trans('message.msg_education_plan_add_success');

                } else {
                    $response['status'] = false;
                    $response['message'] = trans('message.msg_something_wrong');
                }
            }
        }

        return response()
            ->json($response);
    }

    public function edit($id)
    {
        if (!empty($id)) {
            $mdata = EducationPlan::with(['grades' => function ($query) {
                $query->select('pkGra', 'gra_GradeNameRoman', 'gra_GradeNumeric');
            }
                , 'educationProfile' => function ($query) {
                    $query->select('pkEpr', 'epr_EducationProfileName_' . $this->current_language . ' as epr_EducationProfileName');
                }
                , 'educationProgram' => function ($query) {
                    $query->select('pkEdp', 'edp_Name_' . $this->current_language . ' as edp_Name');
                }
                , 'nationalEducationPlan' => function ($query) {
                    $query->select('pkNep', 'nep_NationalEducationPlanName_' . $this->current_language . ' as nep_NationalEducationPlanName');
                }
                , 'QualificationDegree' => function ($query) {
                    $query->select('pkQde', 'qde_QualificationDegreeName_' . $this->current_language . ' as qde_QualificationDegreeName');
                }
                , 'mandatoryCourse.mandatoryCourseGroup' => function ($query) {
                    $query->select('pkCrs', 'crs_CourseName_' . $this->current_language . ' as crs_CourseName', 'crs_CourseAlternativeName')
                        ->where('crs_CourseType', '!=', 'DummyOptionalCourse')
                        ->where('crs_CourseType', '!=', 'DummyForeignCourse');
                }
                , 'optionalCourse.optionalCoursesGroup' => function ($query) {
                    $query->select('pkCrs', 'crs_CourseName_' . $this->current_language . ' as crs_CourseName')
                        ->where('crs_CourseType', '=', 'DummyOptionalCourse');
                }
                , 'foreignLanguageCourse.foreignLanguageGroup' => function ($query) {
                    $query->select('pkCrs', 'crs_CourseName_' . $this->current_language . ' as crs_CourseName')
                        ->where('crs_CourseType', '=', 'DummyForeignCourse');
                }
                , 'mandatoryCourse.grade' => function ($query) {
                    $query->select('pkGra', 'gra_GradeNameRoman', 'gra_GradeNumeric');
                }
                , 'optionalCourse.grade' => function ($query) {
                    $query->select('pkGra', 'gra_GradeNameRoman', 'gra_GradeNumeric');
                }
                , 'foreignLanguageCourse.grade' => function ($query) {
                    $query->select('pkGra', 'gra_GradeNameRoman', 'gra_GradeNumeric');
                },
            ]);
            $mdata = $mdata->where('pkEpl', '=', $id)->where('deleted_at', '=', null)
                ->first();
        }

        $cantons = $this->getCantonList($this->current_language, $this->logged_user->fkAdmCan);
        $grade = Grade::select('pkGra', 'gra_GradeNameRoman', 'gra_GradeNumeric')
            ->get();
        $course = Course::select('pkCrs', 'crs_CourseName_' . $this->current_language . ' as crs_CourseName', 'crs_CourseAlternativeName')
            ->where('crs_CourseType', '!=', 'DummyOptionalCourse')
            ->where('crs_CourseType', '!=', 'DummyForeignCourse')
            ->get();
        $optionalCoursesGroup = Course::select('pkCrs', 'crs_CourseName_' . $this->current_language . ' as crs_CourseName')
            ->where('crs_CourseType', '=', 'DummyOptionalCourse')
            ->get();
        $foreignLanguageGroup = Course::select('pkCrs', 'crs_CourseName_' . $this->current_language . ' as crs_CourseName')
            ->where('crs_CourseType', '=', 'DummyForeignCourse')
            ->get();
        $streams = EducationProgram::select('pkEdp', 'edp_ParentId', 'edp_Name_' . $this->current_language . ' as edp_Name')
            ->get()
            ->toArray();
        $educationProgram = FrontHelper::buildtree($streams);
        $educationProfile = EducationProfile::select('pkEpr', 'epr_EducationProfileName_' . $this->current_language . ' as epr_EducationProfileName')
            ->get();
        $qualificationDegree = QualificationDegree::select('pkQde', 'qde_QualificationDegreeName_' . $this->current_language . ' as qde_QualificationDegreeName')
            ->get();
        $nationalEducationPlan = NationalEducationPlan::select('pkNep', 'nep_NationalEducationPlanName_' . $this->current_language . ' as nep_NationalEducationPlanName')
            ->get();
        $certificateBluePrints = CertificateBluePrints::select('pkCbp', 'cbp_BluePrintName_' . $this->current_language . ' as cbp_BluePrintName')
            ->get();
        $pagedata = compact('cantons', 'grade', 'course', 'optionalCoursesGroup', 'foreignLanguageGroup', 'educationProgram', 'educationProfile', 'qualificationDegree', 'nationalEducationPlan', 'mdata', 'certificateBluePrints');

        if (request()
            ->ajax()) {
            return \View::make('admin.educationPlans.editEducationPlan')
                ->with($pagedata)->renderSections();
        }
        return view('admin.educationPlans.editEducationPlan', $pagedata);

    }

    public function show($id)
    {
        $mdata = '';
        if (!empty($id)) {
            $mdata = EducationPlan::with(['canton' => function ($q) {
                $q->select('pkCan', 'fkCanSta', 'can_CantonName_' . $this->current_language . ' AS can_CantonName');
            },
                'canton.state' => function ($q) {
                    $q->select('pkSta', 'sta_StateName_' . $this->current_language . ' AS sta_StateName');
                },
                'grades' => function ($query) {
                    $query->select('pkGra', 'gra_GradeNameRoman', 'gra_GradeNumeric');
                }
                , 'educationProfile' => function ($query) {
                    $query->select('pkEpr', 'epr_EducationProfileName_' . $this->current_language . ' as epr_EducationProfileName');
                }
                , 'educationProgram' => function ($query) {
                    $query->select('pkEdp', 'edp_Name_' . $this->current_language . ' as edp_Name');
                }
                , 'nationalEducationPlan' => function ($query) {
                    $query->select('pkNep', 'nep_NationalEducationPlanName_' . $this->current_language . ' as nep_NationalEducationPlanName');
                }
                , 'QualificationDegree' => function ($query) {
                    $query->select('pkQde', 'qde_QualificationDegreeName_' . $this->current_language . ' as qde_QualificationDegreeName');
                }
                , 'CertificateBluePrints' => function ($query) {
                    $query->select('pkCbp', 'cbp_BluePrintName_' . $this->current_language . ' as cbp_BluePrintName');
                }
                , 'mandatoryCourse.mandatoryCourseGroup' => function ($query) {
                    $query->select('pkCrs', 'crs_CourseName_' . $this->current_language . ' as crs_CourseName')
                        ->where('crs_CourseType', '!=', 'DummyOptionalCourse')
                        ->where('crs_CourseType', '!=', 'DummyForeignCourse');
                }
                , 'optionalCourse.optionalCoursesGroup' => function ($query) {
                    $query->select('pkCrs', 'crs_CourseName_' . $this->current_language . ' as crs_CourseName')
                        ->where('crs_CourseType', '=', 'DummyOptionalCourse');
                }
                , 'foreignLanguageCourse.foreignLanguageGroup' => function ($query) {
                    $query->select('pkCrs', 'crs_CourseName_' . $this->current_language . ' as crs_CourseName')
                        ->where('crs_CourseType', '=', 'DummyForeignCourse');
                }
                , 'mandatoryCourse.grade' => function ($query) {
                    $query->select('pkGra', 'gra_GradeNameRoman', 'gra_GradeNumeric');
                }
                , 'optionalCourse.grade' => function ($query) {
                    $query->select('pkGra', 'gra_GradeNameRoman', 'gra_GradeNumeric');
                }
                , 'foreignLanguageCourse.grade' => function ($query) {
                    $query->select('pkGra', 'gra_GradeNameRoman', 'gra_GradeNumeric');
                },
            ]);
            $mdata = $mdata->where('pkEpl', '=', $id)
                ->first();
        }

        if (!empty($mdata)) {
            return view('admin.educationPlans.viewEducationPlan', ['mdata' => $mdata]);
        }

    }

    public function destroy(Request $request, $cid)
    {
        $response = [];

        if (empty($cid)) {
            $response['status'] = false;
        } else {
            $eed = SchoolEducationPlanAssignment::where('fkSepEpl', $cid)
                ->count();
            $sec = EnrollStudent::where('fkSteEpl', $cid)
                ->count();

            if ($eed > 0 || $sec > 0) {
                $response['status'] = false;
                $response['message'] = trans('message.msg_education_plan_delete_prompt');
            } else {
                EducationPlan::where('pkEpl', $cid)->delete();
                $response['status'] = true;
                $response['message'] = trans('message.msg_education_plan_delete_success');
            }
        }

        return response()
            ->json($response);
    }

    public function educationplan_list(Request $request)
    {
        $data = $request->all();

        $perpage = !empty($data['length']) ? (int) $data['length'] : 10;

        $filter = isset($data['search']) && is_string($data['search']) ? $data['search'] : '';

        $sort_type = isset($data['order'][0]['dir']) && is_string($data['order'][0]['dir']) ? $data['order'][0]['dir'] : '';

        $sort_col = $data['order'][0]['column'];

        $sort_field = $data['columns'][$sort_col]['data'];

        $EducationPlan = EducationPlan::with([
            // 'grades'=> function($query){
            //     $query->select('pkGra', 'gra_GradeNameRoman' ,'gra_GradeName_'.$this->current_language.' as gra_GradeName');
            // },
            'mandatoryCourse.grade' => function ($query) {
                $query->select('pkGra', 'gra_GradeNameRoman', 'gra_GradeNumeric');
            }
            , 'optionalCourse.grade' => function ($query) {
                $query->select('pkGra', 'gra_GradeNameRoman', 'gra_GradeNumeric');
            }
            , 'foreignLanguageCourse.grade' => function ($query) {
                $query->select('pkGra', 'gra_GradeNameRoman', 'gra_GradeNumeric');
            },
            // ,'country'=> function($query){
            //     // selecting fields from country table
            //     $query->select('pkCny','cny_CountryName');
            // }
        ]);

        if ($filter) {
            $EducationPlan = $EducationPlan->where('epl_Uid', 'LIKE', '%' . $filter . '%')->orWhere('epl_EducationPlanName_' . $this->current_language, 'LIKE', '%' . $filter . '%');
        }
        if ($this->logged_user->type == "MinistryAdmin") {
            $EducationPlanQuery = $EducationPlan->where('fkEplCan', $this->logged_user->fkAdmCan);
        } else {
            $EducationPlanQuery = $EducationPlan;
        }

        if ($sort_col != 0) {
            $EducationPlanQuery = $EducationPlanQuery->orderBy($sort_field, $sort_type);
        }

        $total_EducationPlan = $EducationPlanQuery->count();

        $offset = $data['start'];

        $counter = $offset;
        $EducationPlanData = [];
        $EducationPlans = $EducationPlanQuery->select('*', 'epl_EducationPlanName_' . $this->current_language . ' as epl_EducationPlanName')
            ->offset($offset)->limit($perpage)->get()
            ->toArray();

        foreach ($EducationPlans as $key => $value) {
            $value['index'] = $counter + 1;
            $aGrade = [];
            $tmp = [];
            foreach ($value['mandatory_course'] as $k => $v) {
                $tmp[] = $v['grade']['gra_GradeNumeric'];
            }
            foreach ($value['optional_course'] as $k => $v) {
                $tmp[] = $v['grade']['gra_GradeNumeric'];
            }
            foreach ($value['foreign_language_course'] as $k => $v) {
                $tmp[] = $v['grade']['gra_GradeNumeric'];
            }
            $aGrade = array_unique($tmp);
            $value['grades'] = implode(',', $aGrade);
            $EducationPlanData[$counter] = $value;
            $counter++;
        }

        $price = array_column($EducationPlanData, 'index');

        if ($sort_col == 0) {
            if ($sort_type == 'desc') {
                array_multisort($price, SORT_DESC, $EducationPlanData);
            } else {
                array_multisort($price, SORT_ASC, $EducationPlanData);
            }
        }
        $EducationPlanData = array_values($EducationPlanData);
        $result = array(
            "draw" => $data['draw'],
            "recordsTotal" => $total_EducationPlan,
            "recordsFiltered" => $total_EducationPlan,
            'data' => $EducationPlanData,
        );

        return response()->json($result);
    }

}
