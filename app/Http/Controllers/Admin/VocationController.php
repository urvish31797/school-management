<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Vocation;
use App\Http\Requests\VocationRequest;
use DataTableService;
use Datatables;
use HtmlBuilder;
use HttpResponse;

class VocationController extends Controller
{
    public function index(HtmlBuilder $builder)
    {
        $builder_data['columns'] = [
            ['data' => 'DT_RowIndex', 'name' => 'DT_RowIndex', 'title' => trans('general.sr_no'), 'render' => null, 'orderable' => false, 'searchable' => false],
            ['data' => 'vct_Uid', 'name' => 'vct_Uid', 'title' => trans('general.gn_uid')],
            ['data' => 'vct_VocationName', 'name' => 'vct_VocationName', 'title' => trans('general.gn_name')],
            ['data' => 'vct_Notes', 'name' => 'vct_Notes', 'title' => trans('general.gn_notes')],
            ['data' => 'action', 'name' => 'action', 'title' => trans('general.gn_actions'), 'orderable' => false, 'searchable' => false, 'width' => '10%'],
        ];

        $builder_data['ajax'] = [
            'url'=> route('vocations.list'),
            'data' => 'function(d) {
                d.search =  $("#search_vocation").val();
            }'
        ];

        $dt_html = new DataTableService($builder_data, $builder);
        $dt_html = $dt_html->builder;

        return view('admin.vocations.vocations',compact('dt_html'));
    }

    public function store(VocationRequest $request)
    {
        $column = 'vct_VocationName_'.$this->current_language;
        $value = $request[$column];

        $response = [];
        if (!empty($request->pkVct)) {
            $checkPrev = Vocation::where($column,$value)
                    ->where('pkVct', '!=', $request->pkVct)->count();
            if ($checkPrev) {
                $status_code = HttpResponse::HTTP_BAD_REQUEST;
                $response['message'] = trans('message.msg_vocation_exist');
            } else {
                Vocation::where('pkVct', $request->pkVct)->update($request->validated());
                $response['message'] = trans('message.msg_vocation_update_success');
            }
        } else {
            $checkPrev = Vocation::where($column,$value)->count();
            if ($checkPrev) {
                $status_code = HttpResponse::HTTP_BAD_REQUEST;
                $response['message'] = trans('message.msg_vocation_exist');
            } else {
                $id = Vocation::insertGetId($request->validated());
                if (!empty($id)) {
                    Vocation::where('pkVct', $id)->update(['vct_Uid' => "VOC" . $id]);
                    $response['message'] = trans('message.msg_vocation_add_success');

                } else {
                    $status_code = HttpResponse::HTTP_BAD_REQUEST;
                    $response['message'] = trans('message.msg_something_wrong');
                }
            }
        }

        return response()->json($response,$status_code ?? HttpResponse::HTTP_OK);
    }

    public function edit($cid)
    {
        $response = [];
        $cdata = Vocation::where('pkVct', '=', $cid)->first();

        if (empty($cid) || empty($cdata)) {
            $status_code = HttpResponse::HTTP_BAD_REQUEST;
        } else {

            $response['data'] = $cdata;
        }

        return response()->json($response,$status_code ?? HttpResponse::HTTP_OK);
    }

    public function destroy($cid)
    {
        $response = [];

        if (empty($cid)) {
            $status_code = HttpResponse::HTTP_BAD_REQUEST;
        } else {
            Vocation::where('pkVct', $cid)->delete();

            $response['message'] = trans('message.msg_vocation_delete_success');
        }

        return response()->json($response,$status_code ?? HttpResponse::HTTP_OK);
    }

    public function vocations_list()
    {
        $vocation = Vocation::select('*', 'vct_VocationName_' . $this->current_language . ' as vct_VocationName');
        $vocation->when(request('search'), function ($q){
            return $q->where('vct_Uid', 'LIKE', '%' . request('search') . '%')
            ->orWhere('vct_Notes', 'LIKE', '%' . request('search') . '%')
            ->orWhere('vct_VocationName_' . $this->current_language, 'LIKE', '%' . request('search') . '%');
        });

        return DataTables::of($vocation)
            ->editColumn('vct_Notes', function($vocation){
                return substr($vocation->vct_Notes,0,45);
            })
            ->addColumn('action', function ($vocation) {
                $str = '<a cid="'.$vocation->pkVct.'" onclick="triggerEdit('.$vocation->pkVct.')" href="javascript:void(0)"><i class="fa fa-pen"></i></a>&nbsp;&nbsp';
                $str .= '<a onclick="triggerDelete('.$vocation->pkVct.')" href="javascript:void(0)"><i class="fa fa-trash"></i></a>';
                return $str;
            })
            ->addIndexColumn()
            ->escapeColumns()
            ->toJSON();
    }
}
