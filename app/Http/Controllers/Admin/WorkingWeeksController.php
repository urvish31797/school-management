<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\WorkingWeeksRequest;
use App\Http\Controllers\Controller;
use App\Models\WorkingWeeks;
use App\Models\ClassCalendarWorkingWeeks;
use DataTableService;
use Datatables;
use HtmlBuilder;
use HttpResponse;

class WorkingWeeksController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(HtmlBuilder $builder)
    {
        $builder_data['columns'] = [
            ['data' => 'DT_RowIndex', 'name' => 'DT_RowIndex', 'title' => trans('general.sr_no'), 'render' => null, 'orderable' => false, 'searchable' => false],
            ['data' => 'wek_weekName', 'name' => 'wek_weekName', 'title' => trans('general.gn_name')],
            ['data' => 'wek_notes', 'name' => 'wek_notes', 'title' => trans('general.gn_notes')],
            ['data' => 'action', 'name' => 'action', 'title' => trans('general.gn_actions'), 'orderable' => false, 'searchable' => false, 'width' => '10%'],
        ];

        $builder_data['ajax'] = [
            'url'=> route('working-weeks.list'),
            'data' => 'function(d) {
                d.search =  $("#search_working_weeks").val();
            }'
        ];

        $dt_html = new DataTableService($builder_data, $builder);
        $dt_html = $dt_html->builder;

        return view('admin.workingweeks.index',compact(['dt_html']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(WorkingWeeksRequest $request)
    {
        $response = [];

        $column = 'wek_weekName_'.$this->current_language;
        $value = $request[$column];

        if (!empty($request->pkWek)) {
            $checkPrev = WorkingWeeks::where($column,$value)
                    ->where('pkWek', '!=', $request->pkWek)
                    ->count();
            if ($checkPrev) {
                $status_code = HttpResponse::HTTP_BAD_REQUEST;
                $response['message'] = trans('message.msg_working_weeks_exist');
            } else {
                WorkingWeeks::where('pkWek', $request->pkWek)->update($request->validated());

                $response['message'] = trans('message.msg_working_weeks_update_success');
            }
        } else {
            $checkPrev = WorkingWeeks::where($column,$value)
                    ->count();
            if ($checkPrev) {
                $status_code = HttpResponse::HTTP_BAD_REQUEST;
                $response['message'] = trans('message.msg_working_weeks_exist');
            } else {
                $id = WorkingWeeks::insert($request->validated());
                if (!empty($id)) {

                    $response['message'] = trans('message.msg_working_weeks_add_success');
                } else {
                    $status_code = HttpResponse::HTTP_BAD_REQUEST;
                    $response['message'] = trans('message.msg_something_wrong');
                }
            }

        }

        return response()->json($response,$status_code ?? HttpResponse::HTTP_OK);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $response = [];
        $cdata = WorkingWeeks::where('pkWek', '=', $id)->first();

        if (empty($id) || empty($cdata)) {
            $status_code = HttpResponse::HTTP_BAD_REQUEST;
        } else {

            $response['data'] = $cdata;
        }

        return response()->json($response,$status_code ?? HttpResponse::HTTP_OK);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $response = [];

        if (empty($id)) {
            $status_code = HttpResponse::HTTP_BAD_REQUEST;
        } else {
            $ccw = ClassCalendarWorkingWeeks::where('fkCcwWek', $id)
                ->count();
            if ($ccw != 0) {
                $status_code = HttpResponse::HTTP_BAD_REQUEST;
                $response['message'] = trans('message.msg_working_week_delete_prompt');
            } else {
                WorkingWeeks::where('pkWek', $id)->delete();

                $response['message'] = trans('message.msg_working_week_delete_success');

            }
        }

        return response()->json($response,$status_code ?? HttpResponse::HTTP_OK);
    }

    /**
     * Working weeks listings
     *
     * @return JSON
     */
    public function working_weeks_list()
    {
        $working_weeks = WorkingWeeks::select('*','wek_weekName_' . $this->current_language . ' AS wek_weekName');

        $working_weeks->when(request('search'), function ($q){
            return $q->where('wek_weekName_' . $this->current_language, 'LIKE', '%' . request('search') . '%')
            ->orWhere('wek_notes', 'LIKE', '%' . request('search') . '%');
        });

        return DataTables::of($working_weeks)
            ->editColumn('wek_notes', function($working_weeks){
                return substr($working_weeks->wek_notes,0,45);
            })
            ->addColumn('action', function ($working_weeks) {
                $str = '<a cid="'.$working_weeks->pkWek.'" onclick="triggerEdit('.$working_weeks->pkWek.')" href="javascript:void(0)"><i class="fa fa-pen"></i></a>&nbsp;&nbsp';
                $str .= '<a onclick="triggerDelete('.$working_weeks->pkWek.')" href="javascript:void(0)"><i class="fa fa-trash"></i></a>';
                return $str;
            })
            ->addIndexColumn()
            ->escapeColumns()
            ->toJSON();
    }
}
