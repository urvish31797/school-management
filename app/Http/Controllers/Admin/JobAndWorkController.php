<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\JobAndWorkRequest;
use App\Models\{JobAndWork,Student};
use DataTableService;
use Datatables;
use HtmlBuilder;
use HttpResponse;

class JobAndWorkController extends Controller
{
    public function index(HtmlBuilder $builder)
    {
        $builder_data['columns'] = [
            ['data' => 'DT_RowIndex', 'name' => 'DT_RowIndex', 'title' => trans('general.sr_no'), 'render' => null, 'orderable' => false, 'searchable' => false],
            ['data' => 'jaw_Uid', 'name' => 'jaw_Uid', 'title' => trans('general.gn_uid')],
            ['data' => 'jaw_Name', 'name' => 'jaw_Name', 'title' => trans('general.gn_name')],
            ['data' => 'jaw_Notes', 'name' => 'jaw_Notes', 'title' => trans('general.gn_notes')],
            ['data' => 'jaw_Status', 'name' => 'jaw_Status', 'title' => trans('general.gn_status')],
            ['data' => 'action', 'name' => 'action', 'title' => trans('general.gn_actions'), 'orderable' => false, 'searchable' => false, 'width' => '10%'],
        ];

        $builder_data['ajax'] = [
            'url'=> route('jobandwork.list'),
            'data' => 'function(d) {
                d.search =  $("#search_job_work").val();
            }'
        ];

        $dt_html = new DataTableService($builder_data, $builder);
        $dt_html = $dt_html->builder;

        return view('admin.jobAndWorks.jobAndWorks',compact(['dt_html']));
    }

    public function store(JobAndWorkRequest $request)
    {
        $response = [];
        $column = 'jaw_Name_'.$this->current_language;
        $value = $request[$column];
        if (!empty($request->pkJaw)) {
            $checkPrev = JobAndWork::where($column, $value)
                    ->where('pkJaw', '!=', $request->pkJaw)
                    ->count();
            if ($checkPrev) {
                $status_code = HttpResponse::HTTP_BAD_REQUEST;
                $response['message'] = trans('message.msg_job_work_exist');
            } else {
                JobAndWork::where('pkJaw', $request->pkJaw)->update($request->validated());
                $response['message'] = trans('message.msg_job_work_update_success');
            }
        } else {
            $checkPrev = JobAndWork::where($column, $value)->count();
            if ($checkPrev) {
                $status_code = HttpResponse::HTTP_BAD_REQUEST;
                $response['message'] = trans('message.msg_job_work_exist');
            } else {
                $id = JobAndWork::create($request->validated());
                if (!empty($id)) {
                    $response['message'] = trans('message.msg_job_work_add_success');

                } else {
                    $status_code = HttpResponse::HTTP_BAD_REQUEST;
                    $response['message'] = trans('message.msg_something_wrong');
                }
            }
        }

        return response()->json($response,$status_code ?? HttpResponse::HTTP_OK);
    }

    public function edit($cid)
    {
        $response = [];
        $cdata = JobAndWork::where('pkJaw', $cid)->first();

        if (empty($cid) || empty($cdata)) {
            $status_code = HttpResponse::HTTP_BAD_REQUEST;
        } else {
            $response['data'] = $cdata;
        }

        return response()->json($response,$status_code ?? HttpResponse::HTTP_OK);

    }

    public function destroy($cid)
    {
        $response = [];
        if (empty($cid)) {
            $status_code = HttpResponse::HTTP_BAD_REQUEST;
        } else {
            $student_count = Student::where('fkStuFatherJaw', $cid)->orWhere('fkStuMotherJaw',$cid)->count();
            if($student_count != 0)
            {
                $status_code = HttpResponse::HTTP_BAD_REQUEST;
                $response['message'] = trans('message.msg_job_work_delete_prompt');
            }
            else
            {
                JobAndWork::where('pkJaw', $cid)->delete();
                $response['message'] = trans('message.msg_job_work_delete_success');
            }
        }

        return response()->json($response,$status_code ?? HttpResponse::HTTP_OK);
    }

    /**
     * Get Job and Works List
     *
     * @param \Illuminate\Http\Request $request
     * @return JSON
     */
    public function jobandwork_list()
    {
        $jobwork = JobAndWork::select('pkJaw', 'jaw_Uid', 'jaw_Name_' . $this->current_language . ' as jaw_Name', 'jaw_Notes', 'jaw_Status');
        $jobwork->when(request('search'), function ($q){
            return $q->where('jaw_Uid', 'LIKE', '%' . request('search') . '%')
            ->orWhere('jaw_Name_' . $this->current_language, 'LIKE', '%' . request('search') . '%');
        });

        $jobwork->when(empty(array_first(request('order'))['column']), function($q){
            return $q->orderBy('pkJaw','ASC');
        });

        return DataTables::of($jobwork)
            ->editColumn('jaw_Status', function($jobwork){
                if ($jobwork->jaw_Status == 'Active') {
                    return trans('general.gn_active');
                } else {
                    return trans('general.gn_inactive');
                }
            })
            ->editColumn('jaw_Notes', function($jobwork){
                return substr($jobwork->jaw_Notes,0,45);
            })
            ->addColumn('action', function ($jobwork) {
                $str = '<a cid="'.$jobwork->pkJaw.'" onclick="triggerEdit('.$jobwork->pkJaw.')" href="javascript:void(0)"><i class="fa fa-pen"></i></a>&nbsp;&nbsp';
                $str .= '<a onclick="triggerDelete('.$jobwork->pkJaw.')" href="javascript:void(0)"><i class="fa fa-trash"></i></a>';
                return $str;
            })
            ->addIndexColumn()
            ->escapeColumns()
            ->toJSON();
    }

}
