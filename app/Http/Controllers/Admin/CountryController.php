<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\CountryRequest;
use App\Models\Admin;
use App\Models\Citizenship;
use App\Models\College;
use App\Models\Country;
use App\Models\Employee;
use App\Models\State;
use App\Models\University;
use DataTableService;
use Datatables;
use HtmlBuilder;
use HttpResponse;

class CountryController extends Controller
{
    public function index(HtmlBuilder $builder)
    {
        $builder_data['columns'] = [
            ['data' => 'DT_RowIndex', 'name' => 'DT_RowIndex', 'title' => trans('general.sr_no'), 'render' => null, 'orderable' => false, 'searchable' => false],
            ['data' => 'cny_Uid', 'name' => 'cny_Uid', 'title' => trans('general.gn_uid')],
            ['data' => 'cny_CountryName', 'name' => 'cny_CountryName', 'title' => trans('general.gn_name')],
            ['data' => 'cny_Note', 'name' => 'cny_Note', 'title' => trans('general.gn_notes')],
            ['data' => 'cny_Status', 'name' => 'cny_Status', 'title' => trans('general.gn_status')],
            ['data' => 'action', 'name' => 'action', 'title' => trans('general.gn_actions'), 'orderable' => false, 'searchable' => false, 'width' => '10%'],
        ];

        $builder_data['ajax'] = [
            'url' => route('country.list'),
            'data' => 'function(d) {
                d.search =  $("#search_country").val();
            }',
        ];

        $dt_html = new DataTableService($builder_data, $builder);
        $dt_html = $dt_html->builder;

        return view('admin.countries.countries', compact('dt_html'));
    }

    public function store(CountryRequest $request)
    {
        $response = [];
        $column = 'cny_CountryName_'.$this->current_language;
        $value = $request[$column];
        if (!empty($request->pkCny)) {
            $checkPrev = Country::where($column, $value)
                    ->where('pkCny', '!=', $request->pkCny)->count();
            if ($checkPrev) {
                $status_code = HttpResponse::HTTP_BAD_REQUEST;
                $response['message'] = trans('message.msg_country_exist');
            } else {
                Country::where('pkCny', $request->pkCny)->update($request->validated());
                $response['message'] = trans('message.msg_country_update_success');
            }
        } else {
            $checkPrev = Country::where($column, $value)->count();
            if ($checkPrev) {
                $status_code = HttpResponse::HTTP_BAD_REQUEST;
                $response['message'] = trans('message.msg_country_exist');
            } else {
                $id = Country::insertGetId($request->validated());
                if (!empty($id)) {
                    Country::where('pkCny', $id)->update(['cny_Uid' => "CON" . $id]);
                    $response['message'] = trans('message.msg_country_add_success');

                } else {
                    $status_code = HttpResponse::HTTP_BAD_REQUEST;
                    $response['message'] = trans('message.msg_something_wrong');
                }
            }
        }

        return response()->json($response,$status_code ?? HttpResponse::HTTP_OK);
    }

    public function edit($cid)
    {
        $response = [];
        $cdata = Country::where('pkCny', '=', $cid)->first();

        if (empty($cid) || empty($cdata)) {
            $status_code = HttpResponse::HTTP_BAD_REQUEST;
        } else {
            $response['data'] = $cdata;
        }

        return response()->json($response,$status_code ?? HttpResponse::HTTP_OK);

    }

    public function destroy($cid)
    {
        $response = [];

        if (empty($cid)) {
            $status_code = HttpResponse::HTTP_BAD_REQUEST;
        } else {
            $State = State::where('fkStaCny', $cid)
                ->count();
            $University = University::where('fkUniCny', $cid)
            ->count();
            $College = College::where('fkColCny', $cid)
            ->count();
            $Citizenship = Citizenship::where('fkCtzCny', $cid)
            ->count();
            $Employee = Employee::where('fkEmpCny', $cid)
            ->count();

            if ($State != 0 || $Employee != 0 || $University != 0 || $College != 0 || $Citizenship != 0) {
                $status_code = HttpResponse::HTTP_BAD_REQUEST;
                $response['message'] = trans('message.msg_country_delete_prompt');
            } else {
                Country::where('pkCny', $cid)->delete();
                $response['message'] = trans('message.msg_country_delete_success');
            }
        }

        return response()->json($response,$status_code ?? HttpResponse::HTTP_OK);
    }

    /**
     * Get Countries
     *
     * @param \Illuminate\Http\Request $request
     * @return JSON
     */
    public function country_list()
    {
        $country = Country::select('pkCny', 'cny_Uid', 'cny_CountryName_' . $this->current_language . ' as cny_CountryName', 'cny_Note', 'cny_Status');
        $country->when(request('search'), function ($q) {
            return $q->where('cny_Uid', 'LIKE', '%' . request('search') . '%')
                ->orWhere('cny_Note', 'LIKE', '%' . request('search') . '%')
                ->orWhere('cny_CountryName_' . $this->current_language, 'LIKE', '%' . request('search') . '%');
        });

        $country->when(empty(array_first(request('order'))['column']), function($q){
            return $q->orderBy('pkCny','ASC');
        });

        return DataTables::of($country)
            ->editColumn('cny_Status', function ($country) {
                if ($country->cny_Status == 'Active') {
                    return trans('general.gn_active');
                } else {
                    return trans('general.gn_inactive');
                }
            })
            ->addColumn('action', function ($country) {
                $str = '<a cid="' . $country->pkCny . '" onclick="triggerEdit(' . $country->pkCny . ')" href="javascript:void(0)"><i class="fa fa-pen"></i></a>&nbsp;&nbsp';
                $str .= '<a onclick="triggerDelete(' . $country->pkCny . ')" href="javascript:void(0)"><i class="fa fa-trash"></i></a>';
                return $str;
            })
            ->addIndexColumn()
            ->escapeColumns()
            ->toJSON();
    }

}
