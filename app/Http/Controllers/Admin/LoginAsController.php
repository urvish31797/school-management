<?php
/**
 * LoginAsController
 *
 * @package    Laravel
 * @subpackage Controller
 * @since      1.0
 */

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Admin;
use App\Models\Employee;
use App\Models\EmployeeType;
use Auth;
use Illuminate\Http\Request;
use Session;

class LoginAsController extends Controller
{
    public function index(Request $request)
    {
        return view('admin.loginAs.loginAs');
    }

    /**
     * Login as listing
     *
     * @param \Illuminate\Http\Request $request
     * @return JSON
     */
    public function loginas_list(Request $request)
    {
        $data = $request->all();
        $perpage = !empty($data['length']) ? (int) $data['length'] : 10;
        $filter = isset($data['search']) && is_string($data['search']) ? $data['search'] : '';
        $sort_type = isset($data['order'][0]['dir']) && is_string($data['order'][0]['dir']) ? $data['order'][0]['dir'] : '';
        $sort_col = $data['order'][0]['column'];
        $sort_field = $data['columns'][$sort_col]['data'];
        $role_filter = $data['role'];

        $admin = new Admin;

        if ($filter) {
            $admin = $admin->Where(function ($query) use ($filter) {
                $query->where('adm_Uid', 'LIKE', '%' . $filter . '%')->orWhere('email', 'LIKE', '%' . $filter . '%')->orWhere('adm_Name', 'LIKE', '%' . $filter . '%')->orWhere('adm_GovId', 'LIKE', '%' . $filter . '%');
            });
        }
        $admin = $admin->where('type', '!=', 'HertronicAdmin')
            ->where('type', '!=', 'MinistrySubAdmin');

        if ($role_filter) {
            $admin = $admin->where('type', '=', $role_filter);
        }

        if ($sort_col != 0) {
            $admin = $admin->orderBy($sort_field, $sort_type);
        }

        $offset = $data['start'];

        $counter = $offset;
        $admindata = [];
        $admins = $admin->get()
            ->toArray();

        foreach ($admins as $key => $value) {
            $value['index'] = $counter + 1;
            $admindata[$counter] = $value;
            $counter++;
        }

        $empType = EmployeeType::select('pkEpty')->where('epty_Name', '=', 'Principal')
            ->orWhere('epty_Name', '=', 'SchoolSubAdmin')
            ->orWhere('epty_Name', '=', 'HomeroomTeacher')
            ->where('epty_ParentId', '=', null)
            ->get();

        foreach ($empType as $key => $value) {
            $notAllowedTypes[] = $value->pkEpty;
        }

        $mdata = Employee::with(["employeesEngagement" => function ($q) use ($role_filter, $filter, $notAllowedTypes) {
            $q->whereHas('employeeType', function ($query) use ($role_filter, $filter, $notAllowedTypes) {
                if ($role_filter) {
                    $query->where('epty_Name', '=', $role_filter);
                }
            });
            $q->where('een_DateOfFinishEngagement', '=', null)
                ->whereNotIn('fkEenEpty', $notAllowedTypes);
        }
            , 'employeesEngagement.employeeType']);

        if ($filter) {
            $mdata = $mdata->where(function ($query) use ($filter) {
                $explodestring = explode(" ", $filter);
                $firstname = $explodestring[0] ?? $filter;
                $lastname = $explodestring[1] ?? $filter;

                $query->where('emp_EmployeeName', 'LIKE', '%' . $firstname . '%')
                    ->orWhere('emp_EmployeeSurname', 'LIKE', $lastname . '%')
                    ->orWhere('emp_EmployeeID', 'LIKE', '%' . $filter . '%')
                    ->orWhere('email', 'LIKE', '%' . $filter . '%');
            });
        }

        $mdata = $mdata->get()
            ->toArray();

        $counter1 = count($admins);
        $employeesData = [];

        foreach ($mdata as $key => $value) {
            if (isset($value['employees_engagement'])) {
                foreach ($value['employees_engagement'] as $k => $v) {
                    if (!empty($value['emp_EmployeeID'])) {
                        $adm_Uid = $value['emp_EmployeeID'];
                    } else {
                        $adm_Uid = $value['emp_TempCitizenId'];
                    }

                    $employeesData[] = ['id' => $value['id'], 'adm_Uid' => $adm_Uid, 'type' => $v['employee_type']['epty_Name'], 'email' => $value['email'], 'adm_Name' => $value['emp_EmployeeName'] . " " . $value['emp_EmployeeSurname'], 'adm_Status' => 'Active', 'pkEen' => $v['pkEen'], 'fkEenSch' => $v['fkEenSch']];
                }
            }
        }

        foreach ($employeesData as $key => $value) {
            $value['index'] = $counter1 + 1;
            $admindata[] = $value;
            $counter1++;
        }

        $postMain = array_slice($admindata, $offset, $perpage);

        $total_admins = sizeof($admindata);

        $totalPage = ceil($total_admins / $perpage);

        $price = array_column($postMain, 'index');

        if ($sort_col == 0) {
            if ($sort_type == 'desc') {
                array_multisort($price, SORT_DESC, $postMain);
            } else {
                array_multisort($price, SORT_ASC, $postMain);
            }
        }

        $postMain = array_values($postMain);

        $result = array(
            "draw" => $data['draw'],
            "recordsTotal" => $total_admins,
            "recordsFiltered" => $total_admins,
            "data" => $postMain,
        );

        return response()->json($result);
    }

    /**
     * Login as auth method
     *
     * @param \Illuminate\Http\Request $request
     * @return JSON
     */
    public function auth_loginas(Request $request)
    {
        $data = $request->all();
        $response = [];
        $userData = '';
        $uimg = asset('images/user.png');
        if ($data['type'] == 'HertronicAdmin' || $data['type'] == 'MinistryAdmin' || $data['type'] == 'MinistrySubAdmin') {
            $userData = Admin::where('id', '=', $data['id'])->first();
            $guard = 'admin';
            $type = trans('general.gn_ministry_super_admin');
            $url = url('/admin/dashboard');
            if (!empty($userData->adm_Photo)) {
                $uimg = asset('images/users') . '/' . $userData->adm_Photo;
            }
            $name = $userData->adm_Name;
        } else {
            $userData = Employee::where('id', '=', $data['id'])->first();
            $guard = 'employee';
            if ($data['type'] == 'SchoolCoordinator') {
                $type = trans('general.gn_school_coordinator');
            } elseif ($data['type'] == 'Teacher') {
                $type = trans('general.gn_teacher');
            }
            $url = url('/employee/dashboard');
            if (!empty($userData->emp_PicturePath)) {
                $uimg = asset('images/users') . '/' . $userData->emp_PicturePath;
            }
            $name = $userData->emp_EmployeeName . " " . $userData->emp_EmployeeSurname;

            Session::put('curr_emp_type', $data['type']);
        }

        $msg = trans('general.gn_logged_in_as');
        $msg = $msg . " " . $name . " - " . $type;

        Session::put('previous_user', $this->logged_user);
        Session::put('curr_emp_eid', $data['eid']);
        Session::put('curr_emp_sid', $data['sid']);
        $uauth = Auth::guard($guard)->login($userData);

        $data = '<div class="alert alert-warning alert-dismissible fade show loginas-alert" role="alert">
           <div class="profile-cover">
            <img src="' . $uimg . '">
          </div>
           ' . $msg . '
          <button data-redir="' . $url . '" type="button" class="login_as_redir theme_btn min_btn ml-md-5" data-dismiss="alert" aria-label="Close">
            Exit
          </button>
        </div><div class="alert-bg"></div>';
        $response['status'] = true;
        $response['message'] = $msg;
        $response['data'] = $data;

        return response()->json($response);
    }
}
