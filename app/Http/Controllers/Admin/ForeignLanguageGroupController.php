<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\ForeignLanguageGroup;
use App\Http\Requests\ForeignLanguageGroupRequest;
use DataTableService;
use Datatables;
use HtmlBuilder;
use HttpResponse;

class ForeignLanguageGroupController extends Controller
{
    public function index(HtmlBuilder $builder)
    {
        $builder_data['columns'] = [
            ['data' => 'DT_RowIndex', 'name' => 'DT_RowIndex', 'title' => trans('general.sr_no'), 'render' => null, 'orderable' => false, 'searchable' => false],
            ['data' => 'fon_Uid', 'name' => 'fon_Uid', 'title' => trans('general.gn_uid')],
            ['data' => 'fon_Name', 'name' => 'fon_Name', 'title' => trans('general.gn_name')],
            ['data' => 'fon_Notes', 'name' => 'fon_Notes', 'title' => trans('general.gn_notes')],
            ['data' => 'action', 'name' => 'action', 'title' => trans('general.gn_actions'), 'orderable' => false, 'searchable' => false, 'width' => '10%'],
        ];

        $builder_data['ajax'] = [
            'url'=> route('foreignlanguagegroup.list'),
            'data' => 'function(d) {
                d.search =  $("#search_foreignLanguageGroup").val();
            }'
        ];

        $dt_html = new DataTableService($builder_data, $builder);
        $dt_html = $dt_html->builder;

        return view('admin.foreignLanguageGroup.foreignLanguageGroup',compact('dt_html'));
    }

    public function store(ForeignLanguageGroupRequest $request)
    {
        $column = 'fon_Name_'.$this->current_language;
        $value = $request[$column];

        $response = [];
        if (!empty($request->pkFon)) {
            $checkPrev = ForeignLanguageGroup::where($column,$value)
                    ->where('pkFon', '!=', $request->pkFon)->count();
            if ($checkPrev) {
                $status_code = HttpResponse::HTTP_BAD_REQUEST;
                $response['message'] = trans('message.msg_flg_exist');
            } else {
                ForeignLanguageGroup::where('pkFon', $request->pkFon)->update($request->validated());
                $response['message'] = trans('message.msg_flg_update_success');
            }
        } else {
            $checkPrev = ForeignLanguageGroup::where($column,$value)->count();
            if ($checkPrev) {
                $status_code = HttpResponse::HTTP_BAD_REQUEST;
                $response['message'] = trans('message.msg_flg_exist');
            } else {
                $id = ForeignLanguageGroup::insertGetId($request->validated());
                if (!empty($id)) {
                    ForeignLanguageGroup::where('pkFon', $id)->update(['fon_Uid' => "FLG" . $id]);
                    $response['message'] = trans('message.msg_flg_add_success');

                } else {
                    $status_code = HttpResponse::HTTP_BAD_REQUEST;
                    $response['message'] = trans('message.msg_something_wrong');
                }
            }
        }

        return response()->json($response,$status_code ?? HttpResponse::HTTP_OK);
    }

    public function edit($cid)
    {
        $response = [];
        $cdata = ForeignLanguageGroup::where('pkFon', '=', $cid)->first();

        if (empty($cid) || empty($cdata)) {
            $status_code = HttpResponse::HTTP_BAD_REQUEST;
        } else {
            $response['data'] = $cdata;
        }

        return response()->json($response,$status_code ?? HttpResponse::HTTP_OK);

    }

    public function destroy($cid)
    {
        $response = [];

        if (empty($cid)) {
            $status_code = HttpResponse::HTTP_BAD_REQUEST;
        } else {
            ForeignLanguageGroup::where('pkFon', $cid)->delete();
            $response['message'] = trans('message.msg_flg_delete_success');
        }

        return response()->json($response,$status_code ?? HttpResponse::HTTP_OK);
    }

    /**
     * Get Foriegn Language Groups List
     *
     * @return JSON
     */
    public function foreignlanguagegroup_list()
    {
        $foriegn_groups = ForeignLanguageGroup::select('*', 'fon_Name_' . $this->current_language . ' as fon_Name');
        $foriegn_groups->when(request('search'), function ($q){
            return $q->where('fon_Uid', 'LIKE', '%' . request('search') . '%')
            ->orWhere('fon_Name_' . $this->current_language, 'LIKE', '%' . request('search') . '%')
            ->orWhere('fon_Notes', 'LIKE', '%' . request('search') . '%');
        });

        return DataTables::of($foriegn_groups)
            ->addColumn('action', function ($foriegn_groups) {
                $str = '<a cid="'.$foriegn_groups->pkFon.'" onclick="triggerEdit('.$foriegn_groups->pkFon.')" href="javascript:void(0)"><i class="fa fa-pen"></i></a>&nbsp;&nbsp';
                $str .= '<a onclick="triggerDelete('.$foriegn_groups->pkFon.')" href="javascript:void(0)"><i class="fa fa-trash"></i></a>';
                return $str;
            })
            ->addIndexColumn()
            ->escapeColumns()
            ->toJSON();
    }

}
