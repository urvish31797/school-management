<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\College;
use App\Models\OwnershipType;
use App\Models\School;
use App\Models\University;
use App\Http\Requests\OwnershipTypeRequest;
use DataTableService;
use Datatables;
use HtmlBuilder;
use HttpResponse;

class OwnershipTypeController extends Controller
{
    public function index(HtmlBuilder $builder)
    {
        $builder_data['columns'] = [
            ['data' => 'DT_RowIndex', 'name' => 'DT_RowIndex', 'title' => trans('general.sr_no'), 'render' => null, 'orderable' => false, 'searchable' => false],
            ['data' => 'oty_Uid', 'name' => 'oty_Uid', 'title' => trans('general.gn_uid')],
            ['data' => 'oty_OwnershipTypeName', 'name' => 'oty_OwnershipTypeName', 'title' => trans('general.gn_name')],
            ['data' => 'oty_Notes', 'name' => 'oty_Notes', 'title' => trans('general.gn_notes')],
            ['data' => 'oty_Status', 'name' => 'oty_Status', 'title' => trans('general.gn_status')],
            ['data' => 'action', 'name' => 'action', 'title' => trans('general.gn_actions'), 'orderable' => false, 'searchable' => false, 'width' => '10%'],
        ];

        $builder_data['ajax'] = [
            'url'=> route('ownershiptypes.list'),
            'data' => 'function(d) {
                d.search = $("#search_ownership_type").val();
            }'
        ];

        $dt_html = new DataTableService($builder_data, $builder);
        $dt_html = $dt_html->builder;

        return view('admin.ownershipTypes.ownershipTypes',compact('dt_html'));
    }

    public function store(OwnershipTypeRequest $request)
    {
        $response = [];
        $column = 'oty_OwnershipTypeName_'.$this->current_language;
        $value = $request[$column];

        if (!empty($request->pkOty)) {
            $checkPrev = OwnershipType::where($column,$value)
                    ->where('pkOty', '!=', $request->pkOty)->count();
            if ($checkPrev) {
                $status_code = HttpResponse::HTTP_BAD_REQUEST;
                $response['message'] = trans('message.msg_ownership_type_exist');
            } else {
                OwnershipType::where('pkOty', $request->pkOty)->update($request->validated());
                $response['message'] = trans('message.msg_ownership_type_update_success');
            }
        } else {
            $checkPrev = OwnershipType::where($column,$value)->count();
            if ($checkPrev) {
                $status_code = HttpResponse::HTTP_BAD_REQUEST;
                $response['message'] = trans('message.msg_ownership_type_exist');
            } else {
                $id = OwnershipType::insertGetId($request->validated());
                if (!empty($id)) {
                    OwnershipType::where('pkOty', $id)->update(['oty_Uid' => "OWN" . $id]);
                    $response['message'] = trans('message.msg_ownership_type_add_success');

                } else {
                    $status_code = HttpResponse::HTTP_BAD_REQUEST;
                    $response['message'] = trans('message.msg_something_wrong');
                }
            }
        }

        return response()->json($response,$status_code ?? HttpResponse::HTTP_OK);
    }

    public function edit($id)
    {
        $response = [];
        $cdata = OwnershipType::where('pkOty', $id)->first();

        if (empty($id) || empty($cdata)) {
            $status_code = HttpResponse::HTTP_BAD_REQUEST;
        } else {
            $response['data'] = $cdata;
        }

        return response()->json($response,$status_code ?? HttpResponse::HTTP_OK);

    }

    public function destroy($id)
    {
        $response = [];

        if (empty($id)) {
            $status_code = HttpResponse::HTTP_BAD_REQUEST;
        } else {
            $College = College::where('fkColOty', $id)->count();
            $University = University::where('fkUniOty', $id)->count();
            $School = School::where('fkSchOty', $id)->count();

            if ($College != 0 || $School != 0 || $University != 0) {
                $status_code = HttpResponse::HTTP_BAD_REQUEST;
                $response['message'] = trans('message.msg_ownershiptype_delete_prompt');
            } else {
                OwnershipType::where('pkOty', $id)->delete();
                $response['message'] = trans('message.msg_ownership_delete_success');
            }
        }

        return response()->json($response,$status_code ?? HttpResponse::HTTP_OK);
    }

    /**
     * Get Ownership list
     *
     * @param \Illuminate\Http\Request $request
     * @return JSON
     */
    public function ownershiptypes_list()
    {
        $ownership = OwnershipType::select('*', 'oty_OwnershipTypeName_' . $this->current_language . ' as oty_OwnershipTypeName');

        $ownership->when(request('search'),function($q){
            $q->where('oty_Uid', 'LIKE', '%' . request('search') . '%')
            ->orWhere('oty_OwnershipTypeName_' . $this->current_language, 'LIKE', '%' . request('search') . '%');
        });

        return DataTables::of($ownership)
            ->editColumn('oty_Notes', function($ownership){
                return substr($ownership->oty_Notes,0,45);
            })
            ->editColumn('oty_Status', function($ownership){
                if ($ownership->oty_Status == 'Active') {
                    return trans('general.gn_active');
                } else {
                    return trans('general.gn_inactive');
                }
            })
            ->addColumn('action', function ($ownership) {
                $str = '<a cid="'.$ownership->pkOty.'" onclick="triggerEdit('.$ownership->pkOty.')" href="javascript:void(0)"><i class="fa fa-pen"></i></a>&nbsp;&nbsp';
                $str .= '<a onclick="triggerDelete('.$ownership->pkOty.')" href="javascript:void(0)"><i class="fa fa-trash"></i></a>';
                return $str;
            })
            ->addIndexColumn()
            ->escapeColumns()
            ->toJSON();
    }

}
