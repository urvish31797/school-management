<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\StudentDisciplineMeasureType;
use App\Http\Requests\StudentDisciplineMeasureTypeRequest;
use DataTableService;
use Datatables;
use HtmlBuilder;
use HttpResponse;

class StudentDisciplineMeasureTypeController extends Controller
{
    public function index(HtmlBuilder $builder)
    {
        $builder_data['columns'] = [
            ['data' => 'DT_RowIndex', 'name' => 'DT_RowIndex', 'title' => trans('general.sr_no'), 'render' => null, 'orderable' => false, 'searchable' => false],
            ['data' => 'smt_Uid', 'name' => 'smt_Uid', 'title' => trans('general.gn_uid')],
            ['data' => 'smt_DisciplineMeasureName', 'name' => 'smt_DisciplineMeasureName', 'title' => trans('general.gn_name')],
            ['data' => 'smt_Notes', 'name' => 'smt_Notes', 'title' => trans('general.gn_notes')],
            ['data' => 'action', 'name' => 'action', 'title' => trans('general.gn_actions'), 'orderable' => false, 'searchable' => false, 'width' => '10%'],
        ];

        $builder_data['ajax'] = [
            'url'=> route('disciplinemeasuretype.list'),
            'data' => 'function(d) {
                d.search =  $("#search_disciplineMeasureType").val();
            }'
        ];

        $dt_html = new DataTableService($builder_data, $builder);
        $dt_html = $dt_html->builder;

        return view('admin.disciplineMeasureType.disciplineMeasureType',compact('dt_html'));
    }

    public function store(StudentDisciplineMeasureTypeRequest $request)
    {
        $column = 'smt_DisciplineMeasureName_'.$this->current_language;
        $value = $request[$column];

        $response = [];
        if (!empty($request->pkSmt)) {
            $checkPrev = StudentDisciplineMeasureType::where($column,$value)
                    ->where('pkSmt', '!=', $request->pkSmt)->count();
            if ($checkPrev) {
                $status_code = HttpResponse::HTTP_BAD_REQUEST;
                $response['message'] = trans('message.msg_dmt_exist');
            } else {
                StudentDisciplineMeasureType::where('pkSmt', $request->pkSmt)->update($request->validated());
                $response['message'] = trans('message.msg_dmt_update_success');
            }
        } else {
            $checkPrev = StudentDisciplineMeasureType::where($column,$value)
                    ->count();
            if ($checkPrev) {
                $status_code = HttpResponse::HTTP_BAD_REQUEST;
                $response['message'] = trans('message.msg_dmt_exist');
            } else {
                $id = StudentDisciplineMeasureType::insertGetId($request->validated());
                if (!empty($id)) {
                    StudentDisciplineMeasureType::where('pkSmt', $id)->update(['smt_Uid' => "DMT" . $id]);
                    $response['message'] = trans('message.msg_dmt_add_success');

                } else {
                    $status_code = HttpResponse::HTTP_BAD_REQUEST;
                    $response['message'] = trans('message.msg_something_wrong');
                }
            }
        }

        return response()->json($response,$status_code ?? HttpResponse::HTTP_OK);
    }

    public function edit($cid)
    {
        $response = [];
        $cdata = StudentDisciplineMeasureType::where('pkSmt', '=', $cid)->first();

        if (empty($cid) || empty($cdata)) {
            $status_code = HttpResponse::HTTP_BAD_REQUEST;
        } else {
            $response['data'] = $cdata;
        }

        return response()->json($response,$status_code ?? HttpResponse::HTTP_OK);

    }

    public function destroy($cid)
    {
        $response = [];

        if (empty($cid)) {
            $status_code = HttpResponse::HTTP_BAD_REQUEST;
        } else {
            StudentDisciplineMeasureType::where('pkSmt', $cid)->delete();

            $response['message'] = trans('message.msg_dmt_delete_success');
        }

        return response()->json($response,$status_code ?? HttpResponse::HTTP_OK);
    }

    /**
     * Fetch Discipline Measure Type List
     *
     * @param \Illuminate\Http\Request $request
     * @return void
     */
    public function disciplinemeasuretype_list()
    {
        $discipline_type = StudentDisciplineMeasureType::select('*', 'smt_DisciplineMeasureName_' . $this->current_language . ' as smt_DisciplineMeasureName');
        $discipline_type->when(request('search'), function ($q){
            return $q->where('smt_Uid', 'LIKE', '%' . request('search') . '%')
            ->orWhere('smt_DisciplineMeasureName_' . $this->current_language, 'LIKE', '%' . request('search') . '%')
            ->orWhere('smt_Notes', 'LIKE', '%' . request('search') . '%');
        });

        return DataTables::of($discipline_type)
            ->editColumn('smt_Notes', function($discipline_type){
                return substr($discipline_type->smt_Notes,0,45);
            })
            ->addColumn('action', function ($discipline_type) {
                $str = '<a cid="'.$discipline_type->pkSmt.'" onclick="triggerEdit('.$discipline_type->pkSmt.')" href="javascript:void(0)"><i class="fa fa-pen"></i></a>&nbsp;&nbsp';
                $str .= '<a onclick="triggerDelete('.$discipline_type->pkSmt.')" href="javascript:void(0)"><i class="fa fa-trash"></i></a>';
                return $str;
            })
            ->addIndexColumn()
            ->escapeColumns()
            ->toJSON();

    }

}
