<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Course;
use App\Models\EducationPlansForeignLanguage;
use App\Models\EducationPlansMandatoryCourse;
use App\Models\EducationPlansOptionalCourse;
use App\Http\Requests\CourseRequest;
use DataTableService;
use Datatables;
use HtmlBuilder;
use HttpResponse;

class CourseController extends Controller
{
    public function index(HtmlBuilder $builder)
    {
        $builder_data['columns'] = [
            ['data' => 'DT_RowIndex', 'name' => 'DT_RowIndex', 'title' => trans('general.sr_no'), 'render' => null, 'orderable' => false, 'searchable' => false],
            ['data' => 'crs_Uid', 'name' => 'crs_Uid', 'title' => trans('general.gn_uid')],
            ['data' => 'crs_CourseName', 'name' => 'crs_CourseName', 'title' => trans('general.gn_name')],
            ['data' => 'crs_CourseAlternativeName', 'name' => 'crs_CourseAlternativeName', 'title' => trans('general.gn_alternative_name')],
            ['data' => 'crs_Notes', 'name' => 'crs_Notes', 'title' => trans('general.gn_notes')],
            ['data' => 'action', 'name' => 'action', 'title' => trans('general.gn_actions'), 'orderable' => false, 'searchable' => false, 'width' => '10%'],
        ];

        $builder_data['ajax'] = [
            'url'=> route('courses.list'),
            'data' => 'function(d) {
                d.search =  $("#search_course").val();
            }'
        ];

        $dt_html = new DataTableService($builder_data, $builder);
        $dt_html = $dt_html->builder;

        return view('admin.courses.courses',compact('dt_html'));
    }

    public function store(CourseRequest $request)
    {
        $column = 'crs_CourseName_'.$this->current_language;
        $value = $request[$column];

        $response = [];
        if (!empty($request->pkCrs)) {
            $checkPrev = Course::where($column,$value)->where('pkCrs', '!=', $request->pkCrs)->count();
            $checkAlternative = Course::where('crs_CourseAlternativeName', $request->crs_CourseAlternativeName)
                ->where('pkCrs', '!=', $request->pkCrs)
                ->count();
            $checkPrevUid = Course::where('crs_Uid', $request->crs_Uid)
                ->where('pkCrs', '!=', $request->pkCrs)
                ->count();
            if ($checkPrev) {
                $status_code = HttpResponse::HTTP_BAD_REQUEST;
                $response['message'] = trans('message.msg_course_exist');
            } elseif ($checkAlternative) {
                $status_code = HttpResponse::HTTP_BAD_REQUEST;
                $response['message'] = trans('message.msg_course_alternative_exist');
            } elseif ($checkPrevUid) {
                $status_code = HttpResponse::HTTP_BAD_REQUEST;
                $response['message'] = trans('message.msg_course_uid_exist');
            } else {
                Course::where('pkCrs', $request->pkCrs)->update($request->validated());
                $response['message'] = trans('message.msg_course_update_success');
            }
        } else {
            $checkPrev = Course::where($column,$value)->count();
            $checkPrevUid = Course::where('crs_Uid', $request->crs_Uid)->count();
            if ($checkPrev) {
                $status_code = HttpResponse::HTTP_BAD_REQUEST;
                $response['message'] = trans('message.msg_course_exist');
            } elseif ($checkPrevUid) {
                $status_code = HttpResponse::HTTP_BAD_REQUEST;
                $response['message'] = trans('message.msg_course_uid_exist');
            } else {
                $id = Course::insertGetId($request->validated());
                if (!empty($id)) {
                    $response['message'] = trans('message.msg_course_add_success');
                } else {
                    $status_code = HttpResponse::HTTP_BAD_REQUEST;
                    $response['message'] = trans('message.msg_something_wrong');
                }
            }
        }

        return response()->json($response,$status_code ?? HttpResponse::HTTP_OK);
    }

    public function edit($cid)
    {
        $response = [];
        $cdata = Course::where('pkCrs', '=', $cid)->first();

        if (empty($cid) || empty($cdata)) {
            $status_code = HttpResponse::HTTP_BAD_REQUEST;
        } else {
            $response['data'] = $cdata;
        }

        return response()->json($response,$status_code ?? HttpResponse::HTTP_OK);

    }

    public function destroy($cid)
    {
        $response = [];
        if (empty($cid)) {
            $status_code = HttpResponse::HTTP_BAD_REQUEST;
        } else {

            $epmc = EducationPlansMandatoryCourse::where('fkEplCrs', $cid)
                ->count();
            $epfc = EducationPlansForeignLanguage::where('fkEflCrs', $cid)
                ->count();
            $epoc = EducationPlansOptionalCourse::where('fkEocCrs', $cid)
                ->count();

            if ($epmc != 0 || $epfc != 0 || $epoc != 0) {
                $status_code = HttpResponse::HTTP_BAD_REQUEST;
                $response['message'] = trans('message.msg_course_delete_prompt');
            } else {
                Course::where('pkCrs', $cid)->delete();
                $response['message'] = trans('message.msg_course_delete_success');
            }
        }

        return response()->json($response,$status_code ?? HttpResponse::HTTP_OK);
    }

    /**
     * Courses Listing
     * @return JSON
     */
    public function courses_list()
    {
        $course = Course::select('*', 'crs_CourseName_' . $this->current_language . ' as crs_CourseName');

        $course->when(request('search'),function($q){
            return $q->where('crs_Uid', 'LIKE', '%' . request('search') . '%')
            ->orWhere('crs_CourseName_' . $this->current_language, 'LIKE', '%' . request('search') . '%')
            ->orWhere('crs_CourseAlternativeName', 'LIKE', '%' . request('search') . '%');
        });

        return DataTables::of($course)
            ->addColumn('action', function ($course) {
                $str = '<a cid="'.$course->pkCrs.'" onclick="triggerEdit('.$course->pkCrs.')" href="javascript:void(0)"><i class="fa fa-pen"></i></a>&nbsp;&nbsp';
                $str .= '<a onclick="triggerDelete('.$course->pkCrs.')" href="javascript:void(0)"><i class="fa fa-trash"></i></a>';
                return $str;
            })
            ->addIndexColumn()
            ->escapeColumns()
            ->toJSON();
    }
}
