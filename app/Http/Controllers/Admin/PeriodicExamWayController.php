<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\PeriodicExamWay;
use App\Http\Requests\PeriodicExamWayRequest;
use DataTableService;
use Datatables;
use HtmlBuilder;
use HttpResponse;

class PeriodicExamWayController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(HtmlBuilder $builder)
    {
        $builder_data['columns'] = [
            ['data' => 'DT_RowIndex', 'name' => 'DT_RowIndex', 'title' => trans('general.sr_no'), 'render' => null, 'orderable' => false, 'searchable' => false],
            ['data' => 'pew_Uid', 'name' => 'pew_Uid', 'title' => trans('general.gn_uid')],
            ['data' => 'pew_Name', 'name' => 'pew_Name', 'title' => trans('general.gn_name')],
            ['data' => 'pew_Note', 'name' => 'pew_Note', 'title' => trans('general.gn_notes')],
            ['data' => 'pew_Default', 'name' => 'pew_Default', 'title' => trans('general.gn_default')],
            ['data' => 'action', 'name' => 'action', 'title' => trans('general.gn_actions'), 'orderable' => false, 'searchable' => false, 'width' => '10%'],
        ];

        $builder_data['ajax'] = [
            'url'=> route('periodicexamway.list'),
            'data' => 'function(d) {
                d.search =  $("#search_periodic_exam_way").val();
            }'
        ];

        $dt_html = new DataTableService($builder_data, $builder);
        $dt_html = $dt_html->builder;

        return view('admin.periodicExamWay.periodicExamWay',compact('dt_html'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PeriodicExamWayRequest $request)
    {
        $column = 'pew_Name_'.$this->current_language;
        $value = $request[$column];

        $response = [];
        if (!empty($request->pkPew)) {
            $checkPrev = PeriodicExamWay::where($column,$value)
                    ->where('pkPew', '!=', $request->pkPew)->count();
            if ($checkPrev) {
                $status_code = HttpResponse::HTTP_BAD_REQUEST;
                $response['message'] = trans('message.msg_pew_exists');
            } else {
                PeriodicExamWay::where('pkPew', $request->pkPew)->update($request->validated());

                $response['message'] = trans('message.msg_pew_update_success');
            }
        } else {
            $checkPrev = PeriodicExamWay::where($column,$value)
                    ->count();
            if ($checkPrev) {
                $status_code = HttpResponse::HTTP_BAD_REQUEST;
                $response['message'] = trans('message.msg_pew_exists');
            } else {
                $id = PeriodicExamWay::insertGetId($request->validated());
                if (!empty($id)) {
                    PeriodicExamWay::where('pkPew', $id)->update(['pew_Uid' => "PEW" . $id]);

                    $response['message'] = trans('message.msg_pew_add_success');

                } else {
                    $status_code = HttpResponse::HTTP_BAD_REQUEST;
                    $response['message'] = trans('message.msg_something_wrong');
                }
            }
        }

        return response()->json($response,$status_code ?? HttpResponse::HTTP_OK);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($cid)
    {
        $response = [];
        $cdata = PeriodicExamWay::where('pkPew', '=', $cid)->first();

        if (empty($cid) || empty($cdata)) {
            $status_code = HttpResponse::HTTP_BAD_REQUEST;
        } else {

            $response['data'] = $cdata;
        }

        return response()->json($response,$status_code ?? HttpResponse::HTTP_OK);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($cid)
    {
        $response = [];
        if (empty($cid)) {
            $status_code = HttpResponse::HTTP_BAD_REQUEST;
        } else {
            PeriodicExamWay::where('pkPew', $cid)->delete();

            $response['message'] = trans('message.msg_periodicexamway_delete_success');
        }

        return response()->json($response,$status_code ?? HttpResponse::HTTP_OK);
    }

    /**
     * Get Periodic Exam ways list
     *
     * @param \Illuminate\Http\Request $request
     * @return JSON
     */
    public function periodicexamway_list()
    {
        $periodic_way = PeriodicExamWay::select('*', 'pew_Name_' . $this->current_language . ' as pew_Name');
        $periodic_way->when(request('search'),function($q){
            return $q->where('pew_Uid', 'LIKE', '%' . request('search') . '%')
            ->orWhere('pew_Note', 'LIKE', '%' . request('search') . '%')
            ->orWhere('pew_Name_' . $this->current_language, 'LIKE', '%' . request('search') . '%');
         });

        return DataTables::of($periodic_way)
        ->editColumn('pew_Default',function($periodic_way){
            if($periodic_way->pew_Default == 1){
                return '<a class="btn btn-success btn-sm" href="javascript:void(0)">'.trans('general.gn_default').'</a>';
            } else {
                return '<a class="btn btn-danger btn-sm" href="javascript:void(0)" onclick="changeDefaultModal('.$periodic_way->pkPew.')">'.trans('general.gn_not_default').'</a>';
            }
        })
        ->addColumn('action', function ($periodic_way) {
            $str = '<a onclick="triggerEdit(' . $periodic_way->pkPew . ')" href="javascript:void(0)"><i class="fa fa-pen"></i></a>&nbsp;&nbsp';
            $str .= '<a onclick="triggerDelete('.$periodic_way->pkPew.')" href="javascript:void(0)"><i class="fa fa-trash"></i></a>';
            return $str;
        })
        ->rawColumns(['pew_Default','action'])
        ->addIndexColumn()
        ->escapeColumns()
        ->toJSON();
    }

    /**
     * Change default periodic way
     *
     * @param \Illuminate\Http\Request $request
     * @return JSON
     */
    public function change_periodicexamway()
    {
        $id = request('id');
        $response = [];

        if (empty($id)) {
            $status_code = HttpResponse::HTTP_BAD_REQUEST;
            $response['message'] = trans('message.msg_something_wrong');
        } else {
            $existActive = PeriodicExamWay::where('pew_Default', 1)->first();
            if (!empty($existActive)) {
                $existActive->pew_Default = 0;
                $existActive->save();
            }

            PeriodicExamWay::where('pkPew', $id)->update(['pew_Default' => 1]);

            $response['message'] = trans('message.msg_periodic_way_active_success');
        }

        return response()->json($response,$status_code ?? HttpResponse::HTTP_OK);
    }
}
