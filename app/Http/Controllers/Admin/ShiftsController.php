<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Shifts;
use App\Http\Requests\ShiftsRequest;
use DataTableService;
use Datatables;
use HtmlBuilder;
use HttpResponse;

class ShiftsController extends Controller
{
    /**
     * Shifts View
     *
     * @param \Illuminate\Http\Request $request
     * @return view - view('admin.shifts.shifts')
     */
    public function index(HtmlBuilder $builder)
    {
        $builder_data['columns'] = [
            ['data' => 'DT_RowIndex', 'name' => 'DT_RowIndex', 'title' => trans('general.sr_no'), 'render' => null, 'orderable' => false, 'searchable' => false],
            ['data' => 'shi_ShiftName', 'name' => 'shi_ShiftName', 'title' => trans('general.gn_name')],
            ['data' => 'shi_ShiftNote', 'name' => 'shi_ShiftNote', 'title' => trans('general.gn_notes')],
            ['data' => 'action', 'name' => 'action', 'title' => trans('general.gn_actions'), 'orderable' => false, 'searchable' => false, 'width' => '10%'],
        ];

        $builder_data['ajax'] = [
            'url'=> route('shifts.list'),
            'data' => 'function(d) {
                d.search =  $("#search_et").val();
            }'
        ];

        $dt_html = new DataTableService($builder_data, $builder);
        $dt_html = $dt_html->builder;

        return view('admin.shifts.shifts',compact('dt_html'));
    }

    public function store(ShiftsRequest $request)
    {
        $column = 'shi_ShiftName_'.$this->current_language;
        $value = $request[$column];

        $response = [];
        if (!empty($request->pkShi)) {
            $checkPrev = Shifts::where($column,$value)
                    ->where('pkShi', '!=', $request->pkShi)
                    ->count();
            if ($checkPrev) {
                $status_code = HttpResponse::HTTP_BAD_REQUEST;
                $response['message'] = trans('message.msg_shift_exist');
            } else {
                Shifts::where('pkShi', $request->pkShi)->update($request->validated());

                $response['message'] = trans('message.msg_shift_update_success');
            }
        } else {
            $checkPrev = Shifts::where($column,$value)->count();
            if ($checkPrev) {
                $status_code = HttpResponse::HTTP_BAD_REQUEST;
                $response['message'] = trans('message.msg_shift_exist');
            } else {
                $id = Shifts::insertGetId($request->validated());
                if (!empty($id)) {
                    $response['message'] = trans('message.msg_shift_add_success');
                } else {
                    $status_code = HttpResponse::HTTP_BAD_REQUEST;
                    $response['message'] = trans('message.msg_something_wrong');
                }
            }
        }

        return response()->json($response,$status_code ?? HttpResponse::HTTP_OK);
    }

    public function edit($cid)
    {
        $response = [];
        $cdata = Shifts::where('pkShi', '=', $cid)->first();

        if (empty($cid) || empty($cdata)) {
            $status_code = HttpResponse::HTTP_BAD_REQUEST;
        } else {
            $response['data'] = $cdata;
        }

        return response()->json($response,$status_code ?? HttpResponse::HTTP_OK);
    }

    public function destroy($cid)
    {
        $response = [];

        if (empty($cid)) {
            $status_code = HttpResponse::HTTP_BAD_REQUEST;
            $response['message'] = trans('message.msg_something_wrong');
        } else {
            $delCount = 0;
            if ($delCount > 0) {
                $status_code = HttpResponse::HTTP_BAD_REQUEST;
                $response['message'] = trans('message.msg_shift_delete_prompt');
            } else {
                Shifts::where('pkShi', $cid)->delete();

                $response['message'] = trans('message.msg_shift_delete_success');
            }
        }

        return response()->json($response,$status_code ?? HttpResponse::HTTP_OK);
    }

    /**
     * Get List of Shifts
     *
     * @param \Illuminate\Http\Request $request
     * @return JSON
     */
    public function shifts_list()
    {
        $shift = Shifts::select('*', 'shi_ShiftName_' . $this->current_language . ' as shi_ShiftName');

        $shift->when(request('search'),function ($q){
            return $q->where('shi_ShiftName_' . $this->current_language, 'LIKE', '%' . request('search') . '%')
            ->orWhere('shi_ShiftNote','LIKE','%' . request('search') . '%');
        });

        return DataTables::of($shift)
            ->editColumn('shi_ShiftNote',function($shift){
                return substr($shift->shi_ShiftNote,0,45);
            })
            ->addColumn('action', function ($shift) {
                $str = '<a cid="'.$shift->pkShi.'" onclick="triggerEdit('.$shift->pkShi.')" href="javascript:void(0)"><i class="fa fa-pen"></i></a>&nbsp;&nbsp';
                $str .= '<a onclick="triggerDelete('.$shift->pkShi.')" href="javascript:void(0)"><i class="fa fa-trash"></i></a>';
                return $str;
            })
            ->addIndexColumn()
            ->escapeColumns()
            ->toJSON();
    }
}
