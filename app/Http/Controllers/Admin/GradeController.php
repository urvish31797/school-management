<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\ClassCreationGrades;
use App\Models\CourseOrderAllocation;
use App\Models\EducationPlansForeignLanguage;
use App\Models\EducationPlansMandatoryCourse;
use App\Models\EducationPlansOptionalCourse;
use App\Models\EnrollStudent;
use App\Models\Grade;
use App\Http\Requests\GradeRequest;
use DataTableService;
use Datatables;
use HtmlBuilder;
use HttpResponse;

class GradeController extends Controller
{
    public function index(HtmlBuilder $builder)
    {
        $builder_data['columns'] = [
            ['data' => 'DT_RowIndex', 'name' => 'DT_RowIndex', 'title' => trans('general.sr_no'), 'render' => null, 'orderable' => false, 'searchable' => false],
            ['data' => 'gra_Uid', 'name' => 'gra_Uid', 'title' => trans('general.gn_uid')],
            ['data' => 'gra_GradeName', 'name' => 'gra_GradeName', 'title' => trans('general.gn_name')],
            ['data' => 'gra_GradeNumeric', 'name' => 'gra_GradeNumeric', 'title' => trans('general.gn_numeric_grade')],
            ['data' => 'gra_GradeNameRoman', 'name' => 'gra_GradeNameRoman', 'title' => trans('general.gn_name_roman')],
            ['data' => 'gra_Notes', 'name' => 'gra_Notes', 'title' => trans('general.gn_notes')],
            ['data' => 'action', 'name' => 'action', 'title' => trans('general.gn_actions'), 'orderable' => false, 'searchable' => false, 'width' => '10%'],
        ];

        $builder_data['ajax'] = [
            'url' => route('grades.list'),
            'data' => 'function(d) {
                d.search = $("#search_grade").val();
            }',
        ];

        $dt_html = new DataTableService($builder_data, $builder);
        $dt_html = $dt_html->builder;

        return view('admin.grades.grades', compact('dt_html'));
    }

    public function store(GradeRequest $request)
    {
        $response = [];
        $column = 'gra_GradeName_'.$this->current_language;
        $value = $request[$column];

        if (!empty($request->pkGra)) {
            $checkPrev = Grade::where($column,$value)
                    ->where('pkGra', '!=', $request->pkGra)->count();
            if ($checkPrev) {
                $status_code = HttpResponse::HTTP_BAD_REQUEST;
                $response['message'] = trans('message.msg_grade_exist');
            } else {
                Grade::where('pkGra', $request->pkGra)->update($request->validated());
                $response['message'] = trans('message.msg_grade_update_success');
            }
        } else {
            $checkPrev = Grade::where($column,$value)->count();
            if ($checkPrev) {
                $status_code = HttpResponse::HTTP_BAD_REQUEST;
                $response['message'] = trans('message.msg_grade_exist');
            } else {
                $id = Grade::insertGetId($request->validated());
                if (!empty($id)) {
                    Grade::where('pkGra', $id)->update(['gra_Uid' => "GRD" . $id]);
                    $response['message'] = trans('message.msg_grade_add_success');
                } else {
                    $status_code = HttpResponse::HTTP_BAD_REQUEST;
                    $response['message'] = trans('message.msg_something_wrong');
                }
            }
        }

        return response()->json($response,$status_code ?? HttpResponse::HTTP_OK);
    }

    public function edit($cid)
    {
        $response = [];
        $cdata = Grade::where('pkGra', '=', $cid)->first();

        if (empty($cid) || empty($cdata)) {
            $status_code = HttpResponse::HTTP_BAD_REQUEST;
        } else {
            $response['data'] = $cdata;
        }

        return response()->json($response,$status_code ?? HttpResponse::HTTP_OK);
    }

    public function destroy($cid)
    {
        $response = [];

        if (empty($cid)) {
            $status_code = HttpResponse::HTTP_BAD_REQUEST;
        } else {
            $EnrollStudent = EnrollStudent::where('fkSteGra', $cid)->count();
            $CourseOrderAllocation = CourseOrderAllocation::where('fkCoaGra', $cid)->count();
            $ClassCreationGrades = ClassCreationGrades::where('fkCcgGra', $cid)->count();
            $EducationPlansMandatoryCourse = EducationPlansMandatoryCourse::where('fkEmcGra', $cid)->count();
            $EducationPlansForeignLanguage = EducationPlansForeignLanguage::where('fkEflGra', $cid)->count();
            $EducationPlansOptionalCourse = EducationPlansOptionalCourse::where('fkEocGra', $cid)->count();

            if ($EnrollStudent != 0 || $CourseOrderAllocation != 0 || $ClassCreationGrades != 0 || $EducationPlansMandatoryCourse != 0 || $EducationPlansForeignLanguage != 0 || $EducationPlansOptionalCourse != 0) {
                $status_code = HttpResponse::HTTP_BAD_REQUEST;
                $response['message'] = trans('message.msg_grade_delete_prompt');
            } else {
                Grade::where('pkGra', $cid)->delete();
                $response['message'] = trans('message.msg_grade_delete_success');
            }
        }

        return response()->json($response,$status_code ?? HttpResponse::HTTP_OK);
    }

    public function grades_list()
    {
        $grade = Grade::select('*', 'gra_GradeName_' . $this->current_language . ' as gra_GradeName');
        $grade->when(request('search'), function ($q) {
            return $q->where('gra_Uid', 'LIKE', '%' . request('search') . '%')
                ->orWhere('gra_Notes', 'LIKE', '%' . request('search') . '%')
                ->orWhere('gra_GradeName_' . $this->current_language, 'LIKE', '%' . request('search') . '%');
        });

        return DataTables::of($grade)
            ->editColumn('gra_Notes', function ($grade) {
                return substr($grade->gra_Notes, 0, 45);
            })
            ->addColumn('action', function ($grade) {
                $str = '<a cid="' . $grade->pkGra . '" onclick="triggerEdit(' . $grade->pkGra . ')" href="javascript:void(0)"><i class="fa fa-pen"></i></a>&nbsp;&nbsp';
                $str .= '<a onclick="triggerDelete(' . $grade->pkGra . ')" href="javascript:void(0)"><i class="fa fa-trash"></i></a>';
                return $str;
            })
            ->addIndexColumn()
            ->escapeColumns()
            ->toJSON();
    }
}
