<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\EducationWay;
use App\Http\Requests\EducationWayRequest;
use DataTableService;
use Datatables;
use HtmlBuilder;
use HttpResponse;

class EducationwayController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(HtmlBuilder $builder)
    {
        $builder_data['columns'] = [
            ['data' => 'DT_RowIndex', 'name' => 'DT_RowIndex', 'title' => trans('general.sr_no'), 'render' => null, 'orderable' => false, 'searchable' => false],
            ['data' => 'ew_Uid', 'name' => 'ew_Uid', 'title' => trans('general.gn_uid')],
            ['data' => 'ew_Name', 'name' => 'ew_Name', 'title' => trans('general.gn_name')],
            ['data' => 'ew_Note', 'name' => 'ew_Note', 'title' => trans('general.gn_notes')],
            ['data' => 'ew_Default', 'name' => 'ew_Default', 'title' => trans('general.gn_default')],
            ['data' => 'action', 'name' => 'action', 'title' => trans('general.gn_actions'), 'orderable' => false, 'searchable' => false, 'width' => '10%'],
        ];

        $builder_data['ajax'] = [
            'url'=> route('educationway.list'),
            'data' => 'function(d) {
                d.search =  $("#search_education_way").val();
            }'
        ];

        $dt_html = new DataTableService($builder_data, $builder);
        $dt_html = $dt_html->builder;

        return view('admin.educationWay.educationWay',compact('dt_html'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function store(EducationWayRequest $request)
    {
        $column = 'ew_Name_'.$this->current_language;
        $value = $request[$column];

        $response = [];
        if (!empty($request->pkEw)) {
            $checkPrev = EducationWay::where($column,$value)
                    ->where('pkEw', '!=', $request->pkEw)->count();
            if ($checkPrev) {
                $status_code = HttpResponse::HTTP_BAD_REQUEST;
                $response['message'] = trans('message.msg_ew_exists');
            } else {
                EducationWay::where('pkEw', $request->pkEw)->update($request->validated());
                $response['message'] = trans('message.msg_ew_update_success');
            }
        } else {
            $checkPrev = EducationWay::where($column,$value)
                    ->count();
            if ($checkPrev) {
                $status_code = HttpResponse::HTTP_BAD_REQUEST;
                $response['message'] = trans('message.msg_ew_exists');
            } else {
                $id = EducationWay::insertGetId($request->validated());
                if (!empty($id)) {
                    EducationWay::where('pkEw', $id)->update(['ew_Uid' => "EW" . $id]);
                    $response['message'] = trans('message.msg_ew_add_success');

                } else {
                    $status_code = HttpResponse::HTTP_BAD_REQUEST;
                    $response['message'] = trans('message.msg_something_wrong');
                }
            }
        }

        return response()->json($response,$status_code ?? HttpResponse::HTTP_OK);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($cid)
    {
        $response = [];
        $cdata = EducationWay::where('pkEw', '=', $cid)->first();

        if (empty($cid) || empty($cdata)) {
            $status_code = HttpResponse::HTTP_BAD_REQUEST;
        } else {
            $response['data'] = $cdata;
        }

        return response()->json($response,$status_code ?? HttpResponse::HTTP_OK);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($cid)
    {
        $response = [];
        if (empty($cid)) {
            $status_code = HttpResponse::HTTP_BAD_REQUEST;
        } else {
            EducationWay::where('pkEw', $cid)->delete();
            $response['message'] = trans('message.msg_educationway_delete_success');
        }

        return response()->json($response,$status_code ?? HttpResponse::HTTP_OK);
    }

    /**
     * Get Education ways list
     *
     * @param \Illuminate\Http\Request $request
     * @return JSON
     */
    public function educationway_list()
    {
        $education_ways = EducationWay::select('*', 'ew_Name_' . $this->current_language . ' as ew_Name');

        $education_ways->when(request('search'),function($q){
            return $q->where('ew_Uid', 'LIKE', '%' . request('search') . '%')
            ->orWhere('ew_Note', 'LIKE', '%' . request('search') . '%')
            ->orWhere('ew_Name_' . $this->current_language, 'LIKE', '%' . request('search') . '%');
         });

        return DataTables::of($education_ways)
        ->editColumn('ew_Default',function($education_ways){
            if($education_ways->ew_Default == 1){
                return '<a class="btn btn-success btn-sm" href="javascript:void(0)">'.trans('general.gn_default').'</a>';
            } else {
                return '<a class="btn btn-danger btn-sm" href="javascript:void(0)" onclick="changeDefaultModal('.$education_ways->pkEw.')">'.trans('general.gn_not_default').'</a>';
            }
        })
        ->addColumn('action', function ($education_ways) {
            $str = '<a onclick="triggerEdit(' . $education_ways->pkEw . ')" href="javascript:void(0)"><i class="fa fa-pen"></i></a>&nbsp;&nbsp';
            $str .= '<a onclick="triggerDelete('.$education_ways->pkEw.')" href="javascript:void(0)"><i class="fa fa-trash"></i></a>';
            return $str;
        })
        ->rawColumns(['ew_Default','action'])
        ->addIndexColumn()
        ->escapeColumns()
        ->toJSON();
    }

    /**
     * Change Default Education Way
     *
     * @return JSON
     */
    public function change_educationway()
    {
        $id = request('id');
        $response = [];

        if (empty($id)) {
            $status_code = HttpResponse::HTTP_BAD_REQUEST;
            $response['message'] = trans('message.msg_something_wrong');
        } else {
            $existActive = EducationWay::where('ew_Default', 1)->first();
            if (!empty($existActive)) {
                $existActive->ew_Default = 0;
                $existActive->save();
            }

            EducationWay::where('pkEw', $id)->update(['ew_Default' => 1]);
            $response['message'] = trans('message.msg_education_way_active_success');
        }

        return response()->json($response,$status_code ?? HttpResponse::HTTP_OK);
    }
}
