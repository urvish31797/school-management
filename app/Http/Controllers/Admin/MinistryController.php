<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Traits\CommonTrait;
use App\Http\Traits\UserTrait;
use App\Http\Traits\FileTrait;
use App\Models\Admin;
use App\Helpers\FrontHelper;
use App\Helpers\MailHelper;
use App\Helpers\UtilHelper;
use App\Http\Requests\MinistryRequest;
use Illuminate\Http\Request;
use DataTableService;
use Datatables;
use HtmlBuilder;
use HttpResponse;

class MinistryController extends Controller
{
    use CommonTrait,UserTrait,FileTrait;

    public function index(HtmlBuilder $builder)
    {
        $builder_data['columns'] = [
            ['data' => 'DT_RowIndex', 'name' => 'DT_RowIndex', 'title' => trans('general.sr_no'), 'render' => null, 'orderable' => false, 'searchable' => false],
            ['data' => 'adm_Uid', 'name' => 'adm_Uid', 'title' => trans('general.gn_uid')],
            ['data' => 'adm_Name', 'name' => 'adm_Name', 'title' => trans('general.gn_name')],
            ['data' => 'can_CantonName', 'name' => 'can_CantonName', 'title' => trans('general.gn_canton'), 'orderable' => false],
            ['data' => 'adm_GovId', 'name' => 'adm_GovId', 'title' => trans('general.gn_gov_id')],
            ['data' => 'email', 'name' => 'email', 'title' => trans('login.ln_email')],
            ['data' => 'adm_Status', 'name' => 'adm_Status', 'title' => trans('general.gn_status')],
            ['data' => 'action', 'name' => 'action', 'title' => trans('general.gn_actions'), 'orderable' => false, 'searchable' => false, 'width' => '10%'],
        ];

        $builder_data['ajax'] = [
            'url' => route('ministries.list'),
            'data' => 'function(d) {
                d.search =  $("#search_ministry").val();
            }',
        ];

        $dt_html = new DataTableService($builder_data, $builder);
        $dt_html = $dt_html->builder;

        return view('admin.ministries.ministries', compact('dt_html'));
    }

    public function create()
    {
        return view('admin.ministries.addMinistry');
    }

    public function store(Request $request)
    {
        $response = [];
        $input = $request->all();

        if (isset($input['id']) && !empty($input['id'])) {
            $id = $input['id'];
            $check_user_params = ['type'=>'Admin',
                                 'id'=>$id,
                                 'email'=>$input['email'],
                                 'gov_id'=>$input['adm_GovId'],
                                 'temp_id'=>$input['adm_TempGovId']];
            $check_user_status = $this->checkUserEmailorUidExist($check_user_params);

            if($check_user_status['email_exist']){
                $response['message'] = trans('message.msg_email_exist');
                return response()->json($response,HttpResponse::HTTP_BAD_REQUEST);
            }

            if($check_user_status['gov_id_exist']){
                $response['message'] = trans('message.msg_govt_id_exist');
                return response()->json($response,HttpResponse::HTTP_BAD_REQUEST);
            }

            if($check_user_status['temp_id_exist']){
                $response['message'] = trans('message.msg_govt_id_exist');
                return response()->json($response,HttpResponse::HTTP_BAD_REQUEST);
            }

            $user = Admin::findorFail($id);
            $user->adm_Name = $input['adm_Name'];
            $user->adm_Phone = $input['adm_Phone'];
            $user->adm_Title = $input['adm_Title'];
            $user->adm_GovId = $input['adm_GovId'];
            $user->adm_TempGovId = $input['adm_TempGovId'];
            $user->adm_Gender = $input['adm_Gender'];
            $user->fkAdmCan = $input['fkAdmCan'];
            $user->adm_Status = $input['adm_Status'];
            $user->adm_Address = $input['adm_Address'];
            $user->adm_DOB = UtilHelper::sqlDate($input['adm_DOB'],2);

            if($request->file('upload_profile')){
                if (!empty($user->adm_Photo)) {
                    $unlink_file = $user->adm_Photo;
                }

                $upload_params = [
                    'folder_path'=>config('assetpath.user_images'),
                    'unlink_file'=>$unlink_file ?? ''
                ];
                $image = $request->file('upload_profile');
                $filename = $this->uploadFiletoStorage($image,$upload_params);

                if(!empty($filename)){
                    $user->adm_Photo = $filename;
                }
            }

            if ($user->email != $input['email']) {
                $current_time = UtilHelper::currentDateTime();
                $verification_key = md5(FrontHelper::generatePassword(20));
                $reset_pass_token = base64_encode($input['email'] . '&&MinistryAdmin&&' . $current_time);

                $data = ['email' => $input['email'], 'name' => $input['adm_Name'], 'verify_key' => $verification_key, 'reset_pass_link' => $reset_pass_token, 'subject' => 'New Ministry Credentials'];

                MailHelper::sendNewCredentials($data);
                $user->email = $input['email'];
                $user->email_verified_at = null;
                $user->email_verification_key = $verification_key;
            }

            if ($user->save()) {
                $response['message'] = trans('message.msg_ministry_update_success');
            } else {
                $status_code = HttpResponse::HTTP_BAD_REQUEST;
                $response['message'] = trans('message.msg_something_wrong');
            }
        } else {
            $check_user_params = ['type'=>'Admin',
                                 'email'=>$input['email'],
                                 'gov_id'=>$input['adm_GovId'],
                                 'temp_id'=>$input['adm_TempGovId']];
            $check_user_status = $this->checkUserEmailorUidExist($check_user_params);

            if($check_user_status['email_exist']){
                $response['message'] = trans('message.msg_email_exist');
                return response()->json($response,HttpResponse::HTTP_BAD_REQUEST);
            }

            if($check_user_status['gov_id_exist']){
                $response['message'] = trans('message.msg_govt_id_exist');
                return response()->json($response,HttpResponse::HTTP_BAD_REQUEST);
            }

            if($check_user_status['temp_id_exist']){
                $response['message'] = trans('message.msg_govt_id_exist');
                return response()->json($response,HttpResponse::HTTP_BAD_REQUEST);
            }

            $temp_pass = FrontHelper::generatePassword(10);
            $verification_key = md5(FrontHelper::generatePassword(20));

            $user = new Admin;
            $user->email = $input['email'];
            $user->adm_Name = $input['adm_Name'];
            $user->adm_Phone = $input['adm_Phone'];
            $user->email_verification_key = $verification_key;
            $user->adm_Title = $input['adm_Title'];
            $user->adm_GovId = $input['adm_GovId'];
            $user->adm_TempGovId = $input['adm_TempGovId'];
            $user->adm_Gender = $input['adm_Gender'];
            $user->fkAdmCan = $input['fkAdmCan'];
            $user->adm_Status = $input['adm_Status'];
            $user->adm_Address = $input['adm_Address'];
            $user->type = 'MinistryAdmin';
            $user->adm_DOB = UtilHelper::sqlDate($input['adm_DOB'],2);

            // $user = $request->validated();
            // $user['type'] = 'MinistryAdmin';
            // $user['adm_GovId'] = $input['adm_GovId'];
            // $user['adm_TempGovId'] = $input['adm_TempGovId'];
            // $user['email_verification_key'] = $verification_key;

            if($request->file('upload_profile')){

                $upload_params = [
                    'folder_path'=>config('assetpath.user_images'),
                ];
                $image = $request->file('upload_profile');
                $filename = $this->uploadFiletoStorage($image,$upload_params);

                if(!empty($filename)){
                    $user->adm_Photo = $filename;
                }
            }

            // $user = Admin::create($user);

            if ($user->save()) {
                Admin::where('id', $user->id)->update(['adm_Uid' => "MSA" . $user->id]);
                $current_time = UtilHelper::currentDateTime();
                $reset_pass_token = base64_encode($input['email'] . '&&MinistryAdmin&&' . $current_time);
                $data = ['email' => $input['email'], 'name' => $input['adm_Name'], 'verify_key' => $verification_key, 'pass' => $temp_pass, 'reset_pass_link' => $reset_pass_token, 'subject' => 'New Ministry Credentials'];

                MailHelper::sendNewCredentials($data);

                $response['message'] = trans('message.msg_ministry_add_success');
            } else {
                $status_code = HttpResponse::HTTP_BAD_REQUEST;
                $response['message'] = trans('message.msg_something_wrong');
            }
        }

        return response()->json($response,$status_code ?? HttpResponse::HTTP_OK);
    }

    public function edit($id)
    {
        $data = Admin::findorFail($id);
        return view('admin.ministries.editMinistry', compact('data'));
    }

    public function show($id)
    {
        $mdata = [];
        if (!empty($id)) {
            $mdata = Admin::with([
                'getCanton' => function ($q) {
                    $q->select('*', 'can_CantonName_' . $this->current_language . ' as can_CantonName');
                },
                'getCanton.state' => function ($q) {
                    $q->select('*', 'sta_StateName_' . $this->current_language . ' AS sta_StateName');
                },
                'getCanton.state.country' => function ($q) {
                    $q->select('*', 'cny_CountryName_' . $this->current_language . ' as cny_CountryName');
                },
            ])->findorFail($id);

            $mdata['canton'] = $mdata->getCanton->can_CantonName ?? '';
            $mdata['state'] = $mdata->getCanton->state->sta_StateName ?? '';
            $mdata['country'] = $mdata->getCanton->state->country->cny_CountryName ?? '';
        }

        return view('admin.ministries.viewMinistry', ['mdata' => $mdata]);

    }

    public function destroy($cid)
    {
        $response = [];

        if (empty($cid)) {
            $status_code = HttpResponse::HTTP_BAD_REQUEST;
        } else {
            Admin::where('id', $cid)->where('type', 'MinistryAdmin')->delete();
            $response['message'] = trans('message.msg_ministry_delete_success');
        }

        return response()->json($response,$status_code ?? HttpResponse::HTTP_OK);
    }

    public function ministries_list()
    {
        $admin = Admin::with(
            [
                'getCanton' => function ($q) {
                    $q->select('pkCan', 'can_CantonName_' . $this->current_language . ' AS can_CantonName');
                },
            ]
        )->where('type', '=', 'MinistryAdmin');

        $admin->when(request('search'), function ($q) {
            return $q->where('adm_Uid', 'LIKE', '%' . request('search') . '%')
                ->orWhere('adm_Name', 'LIKE', '%' . request('search') . '%')
                ->orWhere('email', 'LIKE', '%' . request('search') . '%')
                ->orWhere('adm_GovId', 'LIKE', '%' . request('search') . '%')
                ->orWhere('adm_TempGovId', 'LIKE', '%' . request('search') . '%');
        });

        $admin->when(empty(array_first(request('order'))['column']), function($q){
            return $q->orderBy('id','ASC');
        });

        return DataTables::of($admin)
            ->editColumn('can_CantonName', function ($admin) {
                return $admin->getCanton->can_CantonName ?? '';
            })
            ->editColumn('adm_GovId', function ($admin) {
                if(!empty($admin->adm_GovId)){
                    return $admin->adm_GovId;
                } else {
                    return $admin->adm_TempGovId;
                }
            })
            ->addColumn('action', function ($admin) {
                $str = '<a href="ministries/' . $admin->id . '"><i class="fa fa-eye"></i></a>&nbsp;&nbsp';
                $str .= '<a href="ministries/' . $admin->id . '/edit"><i class="fa fa-pen"></i></a>&nbsp;&nbsp';
                $str .= '<a onclick="triggerDelete(' . $admin->id . ')" href="javascript:void(0)"><i class="fa fa-trash"></i></a>';
                return $str;
            })
            ->addIndexColumn()
            ->escapeColumns()
            ->toJSON();
    }

}
