<?php
/**
 * MarkExplanationController
 *
 * @package    Laravel
 * @subpackage Controller
 * @since      1.0
 */

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\MarksExplanation;
use App\Http\Requests\MarkExplanationRequest;
use DataTableService;
use Datatables;
use HtmlBuilder;
use HttpResponse;

class MarkExplanationController extends Controller
{
    public function index(HtmlBuilder $builder)
    {
        $builder_data['columns'] = [
            ['data' => 'DT_RowIndex', 'name' => 'DT_RowIndex', 'title' => trans('general.sr_no'), 'render' => null, 'orderable' => false, 'searchable' => false],
            ['data' => 'me_NumericExplanationMark', 'name' => 'me_NumericExplanationMark', 'title' => trans('general.gn_numeric_mark')],
            ['data' => 'me_Explanation', 'name' => 'me_Explanation', 'title' => trans('sidebar.sidebar_mark_explanation')],
            ['data' => 'action', 'name' => 'action', 'title' => trans('general.gn_actions'), 'orderable' => false, 'searchable' => false, 'width' => '10%'],
        ];

        $builder_data['ajax'] = [
            'url'=> route('markexplanation.list'),
            'data' => 'function(d) {
                d.search =  $("#search_mark_explanation").val();
            }'
        ];

        $dt_html = new DataTableService($builder_data, $builder);
        $dt_html = $dt_html->builder;

        return view('admin.markExplanation.markExplanation',compact('dt_html'));
    }

    public function store(MarkExplanationRequest $request)
    {
        $column = 'me_NumericExplanationMark';
        $value = $request[$column];

        $response = [];
        if (!empty($request->pkMe)) {
            $checkPrev = MarksExplanation::where($column,$value)
            ->where('pkMe', '!=', $request->pkMe)
            ->count();
            if ($checkPrev) {
                $status_code = HttpResponse::HTTP_BAD_REQUEST;
                $response['message'] = trans('message.msg_me_exists');
            } else {
                MarksExplanation::where('pkMe', $request->pkMe)->update($request->validated());

                $response['message'] = trans('message.msg_me_update_success');
            }
        } else {
            $checkPrev = MarksExplanation::where($column,$value)
                ->count();
            if ($checkPrev) {
                $status_code = HttpResponse::HTTP_BAD_REQUEST;
                $response['message'] = trans('message.msg_me_exists');
            } else {
                $id = MarksExplanation::insertGetId($request->validated());
                if (!empty($id)) {
                    $response['message'] = trans('message.msg_me_add_success');
                } else {
                    $status_code = HttpResponse::HTTP_BAD_REQUEST;
                    $response['message'] = trans('message.msg_something_wrong');
                }
            }
        }

        return response()->json($response,$status_code ?? HttpResponse::HTTP_OK);
    }

    public function edit($cid)
    {
        $response = [];
        $cdata = MarksExplanation::where('pkMe', '=', $cid)->first();

        if (empty($cid) || empty($cdata)) {
            $status_code = HttpResponse::HTTP_BAD_REQUEST;
        } else {
            $response['data'] = $cdata;
        }

        return response()->json($response,$status_code ?? HttpResponse::HTTP_OK);
    }

    public function destroy($cid)
    {
        $response = [];
        if (empty($cid)) {
            $status_code = HttpResponse::HTTP_BAD_REQUEST;
        } else {
            MarksExplanation::where('pkMe', $cid)->delete();
            $response['message'] = trans('message.msg_me_delete_success');
        }

        return response()->json($response,$status_code ?? HttpResponse::HTTP_OK);
    }

    /**
     * Get Mark Explanations List
     *
     * @return JSON
     */
    public function markexplanation_list()
    {
        $mark_explanation = MarksExplanation::select('*', 'me_Explanation_' . $this->current_language . ' as me_Explanation');

        $mark_explanation->when(request('search'),function($q){
            return $q->where('me_NumericExplanationMark', 'LIKE', '%' . request('search') . '%')
            ->orWhere('me_Explanation_' . $this->current_language, 'LIKE', '%' . request('search') . '%');
        });

        return DataTables::of($mark_explanation)
            ->addColumn('action', function ($mark_explanation) {
                $str = '<a cid="'.$mark_explanation->pkMe.'" onclick="triggerEdit('.$mark_explanation->pkMe.')" href="javascript:void(0)"><i class="fa fa-pen"></i></a>&nbsp;&nbsp';
                $str .= '<a onclick="triggerDelete('.$mark_explanation->pkMe.')" href="javascript:void(0)"><i class="fa fa-trash"></i></a>';
                return $str;
            })
            ->addIndexColumn()
            ->escapeColumns()
            ->toJSON();

    }
}
