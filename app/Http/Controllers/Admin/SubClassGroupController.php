<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\SubClassGroup;
use App\Http\Requests\SubClassGroupRequest;
use DataTableService;
use Datatables;
use HtmlBuilder;
use HttpResponse;

class SubClassGroupController extends Controller
{
    public function index(HtmlBuilder $builder)
    {
        $builder_data['columns'] = [
            ['data' => 'DT_RowIndex', 'name' => 'DT_RowIndex', 'title' => trans('general.sr_no'), 'render' => null, 'orderable' => false, 'searchable' => false],
            ['data' => 'scg_Uid', 'name' => 'scg_Uid', 'title' => trans('general.gn_uid')],
            ['data' => 'scg_Name', 'name' => 'scg_Name', 'title' => trans('general.gn_name')],
            ['data' => 'scg_Notes', 'name' => 'scg_Notes', 'title' => trans('general.gn_notes')],
            ['data' => 'action', 'name' => 'action', 'title' => trans('general.gn_actions'), 'orderable' => false, 'searchable' => false, 'width' => '10%'],
        ];

        $builder_data['ajax'] = [
            'url'=> route('subclassgroup.list'),
            'data' => 'function(d) {
                d.search =  $("#search_subClassGroup").val();
            }'
        ];

        $dt_html = new DataTableService($builder_data, $builder);
        $dt_html = $dt_html->builder;

        return view('admin.subClassGroup.subClassGroup',compact('dt_html'));
    }

    public function store(SubClassGroupRequest $request)
    {
        $column = 'scg_Name_'.$this->current_language;
        $value = $request[$column];

        $response = [];
        if (!empty($request->pkScg)) {
            $checkPrev = SubClassGroup::where($column,$value)
                    ->where('pkScg', '!=', $request->pkScg)->count();
            if ($checkPrev) {
                $status_code = HttpResponse::HTTP_BAD_REQUEST;
                $response['message'] = trans('message.msg_scg_exist');
            } else {
                SubClassGroup::where('pkScg', $request->pkScg)->update($request->validated());
                
                $response['message'] = trans('message.msg_scg_update_success');
            }
        } else {
            $checkPrev = SubClassGroup::where($column,$value)
                    ->count();
            if ($checkPrev) {
                $status_code = HttpResponse::HTTP_BAD_REQUEST;
                $response['message'] = trans('message.msg_scg_exist');
            } else {
                $id = SubClassGroup::insertGetId($request->validated());
                if (!empty($id)) {
                    SubClassGroup::where('pkScg', $id)->update(['scg_Uid' => "SCG" . $id]);
                    
                    $response['message'] = trans('message.msg_scg_add_success');

                } else {
                    $status_code = HttpResponse::HTTP_BAD_REQUEST;
                    $response['message'] = trans('message.msg_something_wrong');
                }
            }
        }

        return response()->json($response,$status_code ?? HttpResponse::HTTP_OK);
    }

    public function edit($cid)
    {
        $response = [];
        $cdata = SubClassGroup::where('pkScg', '=', $cid)->first();

        if (empty($cid) || empty($cdata)) {
            $status_code = HttpResponse::HTTP_BAD_REQUEST;
        } else {
            
            $response['data'] = $cdata;
        }

        return response()->json($response,$status_code ?? HttpResponse::HTTP_OK);

    }

    public function destroy($cid)
    {
        $response = [];

        if (empty($cid)) {
            $status_code = HttpResponse::HTTP_BAD_REQUEST;
        } else {
            SubClassGroup::where('pkScg', $cid)->delete();
            
            $response['message'] = trans('message.msg_scg_delete_success');
        }

        return response()->json($response,$status_code ?? HttpResponse::HTTP_OK);
    }

    /**
     * Get Subclass Group List
     *
     * @param \Illuminate\Http\Request $request
     * @return JSON
     */
    public function subclassgroup_list()
    {
        $subclass_groups = SubClassGroup::select('*', 'scg_Name_' . $this->current_language . ' as scg_Name');
        $subclass_groups->when(request('search'), function ($q){
            return $q->where('scg_Uid', 'LIKE', '%' . request('search') . '%')
            ->orWhere('scg_Name_' . $this->current_language, 'LIKE', '%' . request('search') . '%')
            ->orWhere('scg_Notes', 'LIKE', '%' . request('search') . '%');
        });

        return DataTables::of($subclass_groups)
            ->editColumn('scg_Notes', function($subclass_groups){
                return substr($subclass_groups->scg_Notes,0,45);
            })
            ->addColumn('action', function ($subclass_groups) {
                $str = '<a cid="'.$subclass_groups->pkScg.'" onclick="triggerEdit('.$subclass_groups->pkScg.')" href="javascript:void(0)"><i class="fa fa-pen"></i></a>&nbsp;&nbsp';
                $str .= '<a onclick="triggerDelete('.$subclass_groups->pkScg.')" href="javascript:void(0)"><i class="fa fa-trash"></i></a>';
                return $str;
            })
            ->addIndexColumn()
            ->escapeColumns()
            ->toJSON();
    }

}
