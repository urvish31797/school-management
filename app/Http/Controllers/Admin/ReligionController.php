<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Employee;
use App\Models\Religion;
use App\Http\Requests\ReligionRequest;
use DataTableService;
use Datatables;
use HtmlBuilder;
use HttpResponse;

class ReligionController extends Controller
{
    public function index(HtmlBuilder $builder)
    {
        $builder_data['columns'] = [
            ['data' => 'DT_RowIndex', 'name' => 'DT_RowIndex', 'title' => trans('general.sr_no'), 'render' => null, 'orderable' => false, 'searchable' => false],
            ['data' => 'rel_Uid', 'name' => 'rel_Uid', 'title' => trans('general.gn_uid')],
            ['data' => 'rel_ReligionName', 'name' => 'rel_ReligionName', 'title' => trans('general.gn_name')],
            ['data' => 'rel_ReligionNameAdjective', 'name' => 'rel_ReligionNameAdjective', 'title' => trans('general.gn_adjective_name')],
            ['data' => 'rel_Notes', 'name' => 'rel_Notes', 'title' => trans('general.gn_notes')],
            ['data' => 'action', 'name' => 'action', 'title' => trans('general.gn_actions'), 'orderable' => false, 'searchable' => false, 'width' => '10%'],
        ];

        $builder_data['ajax'] = [
            'url'=> route('religions.list'),
            'data' => 'function(d) {
                d.search =  $("#search_religion").val();
            }'
        ];

        $dt_html = new DataTableService($builder_data, $builder);
        $dt_html = $dt_html->builder;

        return view('admin.religions.religions', compact('dt_html'));
    }

    public function store(ReligionRequest $request)
    {
        $column = 'rel_ReligionName_'.$this->current_language;
        $value = $request[$column];

        $response = [];
        if (!empty($request->pkRel)) {
            $checkPrev = Religion::where($column,$value)
                    ->where('pkRel', '!=', $request->pkRel)->count();
            if ($checkPrev) {
                $status_code = HttpResponse::HTTP_BAD_REQUEST;
                $response['message'] = trans('message.msg_religion_exists');
            } else {
                Religion::where('pkRel', $request->pkRel)->update($request->validated());

                $response['message'] = trans('message.msg_religion_update_success');
            }
        } else {
            $checkPrev = Religion::where($column,$value)
                    ->count();
            if ($checkPrev) {
                $status_code = HttpResponse::HTTP_BAD_REQUEST;
                $response['message'] = trans('message.msg_religion_exists');
            } else {
                $id = Religion::insertGetId($request->validated());
                if (!empty($id)) {
                    Religion::where('pkRel', $id)->update(['rel_Uid' => "REL" . $id]);

                    $response['message'] = trans('message.msg_religion_add_success');

                } else {
                    $status_code = HttpResponse::HTTP_BAD_REQUEST;
                    $response['message'] = trans('message.msg_something_wrong');
                }
            }
        }

        return response()->json($response,$status_code ?? HttpResponse::HTTP_OK);
    }

    public function edit($id)
    {
        $response = [];
        $cdata = Religion::where('pkRel', '=', $id)->first();

        if (empty($id) || empty($cdata)) {
            $status_code = HttpResponse::HTTP_BAD_REQUEST;
        } else {

            $response['data'] = $cdata;
        }

        return response()->json($response,$status_code ?? HttpResponse::HTTP_OK);
    }

    public function destroy($id)
    {
        $response = [];

        if (empty($id)) {
            $status_code = HttpResponse::HTTP_BAD_REQUEST;
        } else {
            $Employee = Employee::where('fkEmpRel', $id)
                ->count();
            $Student = 0;

            if ($Employee != 0 || $Student != 0) {
                $status_code = HttpResponse::HTTP_BAD_REQUEST;
                $response['message'] = trans('message.msg_religion_delete_prompt');
            } else {
                Religion::where('pkRel', $id)->delete();
                $response['message'] = trans('message.msg_religion_delete_success');
            }
        }

        return response()->json($response,$status_code ?? HttpResponse::HTTP_OK);
    }

    /**
     * Religions list
     *
     * @return JSON
     */
    public function religions_list()
    {
        $religions = Religion::select('*', 'rel_ReligionName_' . $this->current_language . ' as rel_ReligionName');
        $religions->when(request('search'), function ($q){
            return $q->where('rel_Uid', 'LIKE', '%' . request('search') . '%')
            ->orWhere('rel_Notes', 'LIKE', '%' . request('search') . '%')
            ->orWhere('rel_ReligionName_' . $this->current_language, 'LIKE', '%' . request('search') . '%');
        });

        return DataTables::of($religions)
            ->editColumn('rel_Notes', function($religions){
                return substr($religions->rel_Notes,0,45);
            })
            ->addColumn('action', function ($religions) {
                $str = '<a cid="'.$religions->pkRel.'" onclick="triggerEdit('.$religions->pkRel.')" href="javascript:void(0)"><i class="fa fa-pen"></i></a>&nbsp;&nbsp';
                $str .= '<a onclick="triggerDelete('.$religions->pkRel.')" href="javascript:void(0)"><i class="fa fa-trash"></i></a>';
                return $str;
            })
            ->addIndexColumn()
            ->escapeColumns()
            ->toJSON();

    }

}
