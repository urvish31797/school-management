<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\CertificateType;
use App\Http\Requests\CertificateTypeRequest;
use DataTableService;
use Datatables;
use HtmlBuilder;
use HttpResponse;

class CertificateTypeController extends Controller
{
    public function index(HtmlBuilder $builder)
    {
        $builder_data['columns'] = [
            ['data' => 'DT_RowIndex', 'name' => 'DT_RowIndex', 'title' => trans('general.sr_no'), 'render' => null, 'orderable' => false, 'searchable' => false],
            ['data' => 'cty_Uid', 'name' => 'cty_Uid', 'title' => trans('general.gn_uid')],
            ['data' => 'cty_Name', 'name' => 'cty_Name', 'title' => trans('general.gn_name')],
            ['data' => 'action', 'name' => 'action', 'title' => trans('general.gn_actions'), 'orderable' => false, 'searchable' => false, 'width' => '10%'],
        ];

        $builder_data['ajax'] = [
            'url'=> route('certificatetype.list'),
            'data' => 'function(d) {
                d.search =  $("#search_certificate_type").val();
            }'
        ];

        $dt_html = new DataTableService($builder_data, $builder);
        $dt_html = $dt_html->builder;

        return view('admin.certificateType.certificateType',compact('dt_html'));
    }

    public function store(CertificateTypeRequest $request)
    {
        $column = 'cty_Name_'.$this->current_language;
        $value = $request[$column];

        $response = [];
        if (!empty($request->pkCty)) {
            $checkPrev = CertificateType::where($column,$value)
                    ->where('pkCty', '!=', $request->pkCty)->count();
            if ($checkPrev) {
                $status_code = HttpResponse::HTTP_BAD_REQUEST;
                $response['message'] = trans('message.msg_ct_exists');
            } else {
                CertificateType::where('pkCty', $request->pkCty)->update($request->validated());
                $response['message'] = trans('message.msg_ct_update_success');
            }
        } else {
            $checkPrev = CertificateType::where($column,$value)
                    ->count();
            if ($checkPrev) {
                $status_code = HttpResponse::HTTP_BAD_REQUEST;
                $response['message'] = trans('message.msg_ct_exists');
            } else {
                $id = CertificateType::insertGetId($request->validated());
                if (!empty($id)) {
                    CertificateType::where('pkCty', $id)->update(['cty_Uid' => "CT" . $id]);

                    $response['message'] = trans('message.msg_ct_add_success');

                } else {
                    $status_code = HttpResponse::HTTP_BAD_REQUEST;
                    $response['message'] = trans('message.msg_something_wrong');
                }
            }
        }

        return response()->json($response,$status_code ?? HttpResponse::HTTP_OK);
    }

    public function edit($cid)
    {
        $response = [];
        $cdata = CertificateType::where('pkCty', '=', $cid)->first();

        if (empty($cid) || empty($cdata)) {
            $status_code = HttpResponse::HTTP_BAD_REQUEST;
        } else {
            $response['data'] = $cdata;
        }

        return response()->json($response,$status_code ?? HttpResponse::HTTP_OK);
    }

    public function destroy($cid)
    {
        $response = [];

        if (empty($cid)) {
            $status_code = HttpResponse::HTTP_BAD_REQUEST;
        } else {
            CertificateType::where('pkCty', $cid)->delete();
            $response['message'] = trans('message.msg_certificatetype_delete_success');
        }

        return response()->json($response,$status_code ?? HttpResponse::HTTP_OK);
    }

    /**
     * Get Certificates Type List
     *
     * @param \Illuminate\Http\Request $request
     * @return JSON
     */
    public function certificatetype_list()
    {
        $certificate_type = CertificateType::select('pkCty', 'cty_Name_' . $this->current_language . ' as cty_Name', 'cty_Uid');

        $certificate_type->when(request('search'),function($q){
            return $q->where('cty_Uid', 'LIKE', '%' . request('search') . '%')
            ->orWhere('cty_Name_' . $this->current_language, 'LIKE', '%' . request('search') . '%');
        });

        return DataTables::of($certificate_type)
            ->addColumn('action', function ($certificate_type) {
                $str = '<a cid="'.$certificate_type->pkCty.'" onclick="triggerEdit('.$certificate_type->pkCty.')" href="javascript:void(0)"><i class="fa fa-pen"></i></a>&nbsp;&nbsp';
                $str .= '<a onclick="triggerDelete('.$certificate_type->pkCty.')" href="javascript:void(0)"><i class="fa fa-trash"></i></a>';
                return $str;
            })
            ->addIndexColumn()
            ->escapeColumns()
            ->toJSON();
    }
}
