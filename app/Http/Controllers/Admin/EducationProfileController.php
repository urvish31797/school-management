<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\EducationPlan;
use App\Models\EducationProfile;
use App\Http\Requests\EducationProfileRequest;
use DataTableService;
use Datatables;
use HtmlBuilder;
use HttpResponse;

class EducationProfileController extends Controller
{
    public function index(HtmlBuilder $builder)
    {
        $builder_data['columns'] = [
            ['data' => 'DT_RowIndex', 'name' => 'DT_RowIndex', 'title' => trans('general.sr_no'), 'render' => null, 'orderable' => false, 'searchable' => false],
            ['data' => 'epr_Uid', 'name' => 'epr_Uid', 'title' => trans('general.gn_uid')],
            ['data' => 'epr_EducationProfileName', 'name' => 'epr_EducationProfileName', 'title' => trans('general.gn_name')],
            ['data' => 'epr_Notes', 'name' => 'epr_Notes', 'title' => trans('general.gn_notes')],
            ['data' => 'epr_Status', 'name' => 'epr_Status', 'title' => trans('general.gn_status')],
            ['data' => 'action', 'name' => 'action', 'title' => trans('general.gn_actions'), 'orderable' => false, 'searchable' => false, 'width' => '10%'],
        ];

        $builder_data['ajax'] = [
            'url'=> route('educationprofiles.list'),
            'data' => 'function(d) {
                d.search =  $("#search_education_profile").val();
            }'
        ];

        $dt_html = new DataTableService($builder_data, $builder);
        $dt_html = $dt_html->builder;

        return view('admin.educationProfiles.educationProfiles', compact('dt_html'));
    }

    public function store(EducationProfileRequest $request)
    {
        $column = 'epr_EducationProfileName_'.$this->current_language;
        $value = $request[$column];

        $response = [];
        if (!empty($request->pkEpr)) {
            $checkPrev = EducationProfile::where($column,$value)
                    ->where('pkEpr', '!=', $request->pkEpr)->count();
            if ($checkPrev) {
                $status_code = HttpResponse::HTTP_BAD_REQUEST;
                $response['message'] = trans('message.msg_education_profile_exist');
            } else {
                EducationProfile::where('pkEpr', $request->pkEpr)->update($request->validated());

                $response['message'] = trans('message.msg_education_profile_update_success');
            }
        } else {
            $checkPrev = EducationProfile::where($column,$value)->count();
            if ($checkPrev) {
                $status_code = HttpResponse::HTTP_BAD_REQUEST;
                $response['message'] = trans('message.msg_education_profile_exist');
            } else {
                $id = EducationProfile::insertGetId($request->validated());
                if (!empty($id)) {
                    EducationProfile::where('pkEpr', $id)->update(['epr_Uid' => "EDP" . $id]);

                    $response['message'] = trans('message.msg_education_profile_add_success');

                } else {
                    $status_code = HttpResponse::HTTP_BAD_REQUEST;
                    $response['message'] = trans('message.msg_something_wrong');
                }
            }
        }

        return response()->json($response,$status_code ?? HttpResponse::HTTP_OK);
    }

    public function edit($cid)
    {
        $response = [];
        $cdata = EducationProfile::where('pkEpr', '=', $cid)->first();

        if (empty($cid) || empty($cdata)) {
            $status_code = HttpResponse::HTTP_BAD_REQUEST;
        } else {
            $response['data'] = $cdata;
        }

        return response()->json($response,$status_code ?? HttpResponse::HTTP_OK);
    }

    public function destroy($cid)
    {
        $response = [];

        if (empty($cid)) {
            $status_code = HttpResponse::HTTP_BAD_REQUEST;
        } else {
            $EducationPlan = EducationPlan::where('fkEplEpr', $cid)
                ->count();

            if ($EducationPlan != 0) {
                $status_code = HttpResponse::HTTP_BAD_REQUEST;
                $response['message'] = trans('message.msg_education_profile_delete_prompt');
            } else {
                EducationProfile::where('pkEpr', $cid)->delete();
                $response['message'] = trans('message.msg_education_profile_delete_success');
            }
        }

        return response()->json($response,$status_code ?? HttpResponse::HTTP_OK);
    }

    /**
     * Get Education Profiles List
     *
     * @param \Illuminate\Http\Request $request
     * @return JSON
     */
    public function educationprofiles_list()
    {
        $education_profile = EducationProfile::select('*', 'epr_EducationProfileName_' . $this->current_language . ' as epr_EducationProfileName');

        $education_profile->when(request('search'),function($q){
            return $q->where('epr_Uid', 'LIKE', '%' . request('search') . '%')
            ->orWhere('epr_Notes', 'LIKE', '%' . request('search') . '%')
            ->orWhere('epr_EducationProfileName_' . $this->current_language, 'LIKE', '%' . request('search') . '%');
        });

        return DataTables::of($education_profile)
            ->editColumn('epr_Notes', function ($education_profile) {
                return substr($education_profile->epr_Notes,0,45);
            })
            ->editColumn('epr_Status', function($education_profile){
                if($education_profile->epr_Status == "Active"){
                    return trans('general.gn_active');
                } else {
                    return trans('general.gn_inactive');
                }
            })
            ->addColumn('action', function ($education_profile) {
                $str = '<a cid="'.$education_profile->pkEpr.'" onclick="triggerEdit('.$education_profile->pkEpr.')" href="javascript:void(0)"><i class="fa fa-pen"></i></a>&nbsp;&nbsp';
                $str .= '<a onclick="triggerDelete('.$education_profile->pkEpr.')" href="javascript:void(0)"><i class="fa fa-trash"></i></a>';
                return $str;
            })
            ->addIndexColumn()
            ->escapeColumns()
            ->toJSON();

    }

}
