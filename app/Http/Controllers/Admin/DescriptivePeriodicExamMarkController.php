<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\DescriptivePeriodicExamMark;
use App\Http\Requests\DescriptivePeriodicExamMarkRequest;
use DataTableService;
use Datatables;
use HtmlBuilder;
use HttpResponse;

class DescriptivePeriodicExamMarkController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(HtmlBuilder $builder)
    {
        $builder_data['columns'] = [
            ['data' => 'DT_RowIndex', 'name' => 'DT_RowIndex', 'title' => trans('general.sr_no'), 'render' => null, 'orderable' => false, 'searchable' => false],
            ['data' => 'dm_Uid', 'name' => 'dm_Uid', 'title' => trans('general.gn_uid')],
            ['data' => 'dm_Name', 'name' => 'dm_Name', 'title' => trans('general.gn_name')],
            ['data' => 'dm_Note', 'name' => 'dm_Note', 'title' => trans('general.gn_notes')],
            ['data' => 'action', 'name' => 'action', 'title' => trans('general.gn_actions'), 'orderable' => false, 'searchable' => false, 'width' => '10%'],
        ];

        $builder_data['ajax'] = [
            'url'=> route('descriptiveperiodicexammark.list'),
            'data' => 'function(d) {
                d.search =  $("#search_periodic_exam_mark").val();
            }'
        ];

        $dt_html = new DataTableService($builder_data, $builder);
        $dt_html = $dt_html->builder;

        return view('admin.descPeriodicExamMark.descPeriodicExamMark',compact('dt_html'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(DescriptivePeriodicExamMarkRequest $request)
    {
        $response = [];
        $column = 'dm_Name_'.$this->current_language;
        $value = $request[$column];

        if (!empty($request->pkDpem)) {
            $checkPrev = DescriptivePeriodicExamMark::where($column,$value)
                    ->where('pkDpem', '!=', $request->pkDpem)->count();
            if ($checkPrev) {
                $status_code = HttpResponse::HTTP_BAD_REQUEST;
                $response['message'] = trans('message.msg_dpem_exists');
            } else {
                DescriptivePeriodicExamMark::where('pkDpem', $request->pkDpem)->update($request->validated());

                $response['message'] = trans('message.msg_dpem_update_success');
            }
        } else {
            $checkPrev = DescriptivePeriodicExamMark::where($column,$value)
                    ->count();
            if ($checkPrev) {
                $status_code = HttpResponse::HTTP_BAD_REQUEST;
                $response['message'] = trans('message.msg_dpem_exists');
            } else {
                $id = DescriptivePeriodicExamMark::insertGetId($request->validated());
                if (!empty($id)) {
                    DescriptivePeriodicExamMark::where('pkDpem', $id)->update(['dm_Uid' => "DPEM" . $id]);

                    $response['message'] = trans('message.msg_dpem_add_success');

                } else {
                    $status_code = HttpResponse::HTTP_BAD_REQUEST;
                    $response['message'] = trans('message.msg_something_wrong');
                }
            }
        }

        return response()->json($response,$status_code ?? HttpResponse::HTTP_OK);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($cid)
    {
        $response = [];
        $cdata = DescriptivePeriodicExamMark::where('pkDpem', '=', $cid)->first();

        if (empty($cid) || empty($cdata)) {
            $status_code = HttpResponse::HTTP_BAD_REQUEST;
        } else {

            $response['data'] = $cdata;
        }

        return response()->json($response,$status_code ?? HttpResponse::HTTP_OK);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($cid)
    {
        $response = [];
        if (empty($cid)) {
            $status_code = HttpResponse::HTTP_BAD_REQUEST;
        } else {
            DescriptivePeriodicExamMark::where('pkDpem', $cid)->delete();

            $response['message'] = trans('message.msg_periodicexammark_delete_success');
        }

        return response()->json($response,$status_code ?? HttpResponse::HTTP_OK);
    }

    /**
     * Get Periodic Exam Descriptive Mark list
     *
     * @param \Illuminate\Http\Request $request
     * @return JSON
     */
    public function descriptiveperiodicexammark_list()
    {
        $desc_mark = DescriptivePeriodicExamMark::select('*', 'dm_Name_' . $this->current_language . ' as dm_Name');
        $desc_mark->when(request('search'),function($q){
            return $q->where('dm_Uid', 'LIKE', '%' . request('search') . '%')
            ->orWhere('dm_Note', 'LIKE', '%' . request('search') . '%')
            ->orWhere('dm_Name_' . $this->current_language, 'LIKE', '%' . request('search') . '%');
         });

        return DataTables::of($desc_mark)
        ->addColumn('action', function ($desc_mark) {
            $str = '<a onclick="triggerEdit(' . $desc_mark->pkDpem . ')" href="javascript:void(0)"><i class="fa fa-pen"></i></a>&nbsp;&nbsp';
            $str .= '<a onclick="triggerDelete('.$desc_mark->pkDpem.')" href="javascript:void(0)"><i class="fa fa-trash"></i></a>';
            return $str;
        })
        ->rawColumns(['pew_Default','action'])
        ->addIndexColumn()
        ->escapeColumns()
        ->toJSON();
    }
}
