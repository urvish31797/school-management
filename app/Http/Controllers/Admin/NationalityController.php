<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Admin;
use App\Models\Employee;
use App\Models\Nationality;
use App\Http\Requests\NationalityRequest;
use DataTableService;
use Datatables;
use HtmlBuilder;
use HttpResponse;

class NationalityController extends Controller
{
    public function index(HtmlBuilder $builder)
    {
        $builder_data['columns'] = [
            ['data' => 'DT_RowIndex', 'name' => 'DT_RowIndex', 'title' => trans('general.sr_no'), 'render' => null, 'orderable' => false, 'searchable' => false],
            ['data' => 'nat_Uid', 'name' => 'nat_Uid', 'title' => trans('general.gn_uid')],
            ['data' => 'nat_NationalityName', 'name' => 'nat_NationalityName', 'title' => trans('general.gn_name')],
            ['data' => 'nat_NationalityNameMale', 'name' => 'nat_NationalityNameMale', 'title' => trans('general.gn_name_of_males')],
            ['data' => 'nat_NationalityNameFemale', 'name' => 'nat_NationalityNameFemale', 'title' => trans('general.gn_name_of_females')],
            ['data' => 'nat_Notes', 'name' => 'nat_Notes', 'title' => trans('general.gn_notes')],
            ['data' => 'action', 'name' => 'action', 'title' => trans('general.gn_actions'), 'orderable' => false, 'searchable' => false, 'width' => '10%'],
        ];

        $builder_data['ajax'] = [
            'url' => route('nationalities.list'),
            'data' => 'function(d) {
                d.search = $("#search_nationality").val();
            }'
        ];

        $dt_html = new DataTableService($builder_data, $builder);
        $dt_html = $dt_html->builder;

        return view('admin.nationalities.nationalities', compact('dt_html'));
    }

    public function store(NationalityRequest $request)
    {
        $column = 'nat_NationalityName_'.$this->current_language;
        $value = $request[$column];

        $response = [];
        if (!empty($request->pkNat)) {
            $checkPrev = Nationality::where($column,$value)
                    ->where('pkNat', '!=', $request->pkNat)->count();
            if (!empty($checkPrev)) {
                $status_code = HttpResponse::HTTP_BAD_REQUEST;
                $response['message'] = trans('message.msg_nationality_exists');
            } else {
                Nationality::where('pkNat', $request->pkNat)->update($request->validated());
                $response['message'] = trans('message.msg_nationality_update_success');
            }
        } else {
            $checkPrev = Nationality::where($column,$value)
                    ->first();
            if (!empty($checkPrev)) {
                $status_code = HttpResponse::HTTP_BAD_REQUEST;
                $response['message'] = trans('message.msg_nationality_exists');
            } else {
                $id = Nationality::insertGetId($request->validated());
                if (!empty($id)) {
                    Nationality::where('pkNat', $id)->update(['nat_Uid' => "NTL" . $id]);
                    $response['message'] = trans('message.msg_nationality_add_success');
                } else {
                    $status_code = HttpResponse::HTTP_BAD_REQUEST;
                    $response['message'] = trans('message.msg_something_wrong');
                }
            }
        }

        return response()->json($response,$status_code ?? HttpResponse::HTTP_OK);
    }

    public function edit($id)
    {
        $response = [];
        $cdata = Nationality::where('pkNat', '=', $id)->first();

        if (empty($id) || empty($cdata)) {
            $status_code = HttpResponse::HTTP_BAD_REQUEST;
        } else {
            $response['data'] = $cdata;
        }

        return response()->json($response,$status_code ?? HttpResponse::HTTP_OK);
    }

    public function destroy($id)
    {
        $response = [];

        if (empty($id)) {
            $status_code = HttpResponse::HTTP_BAD_REQUEST;
        } else {
            $Employee = Employee::where('fkEmpNat', $id)
                ->count();
            $Student = 0;
            if ($Employee != 0 || $Student != 0) {
                $status_code = HttpResponse::HTTP_BAD_REQUEST;
                $response['message'] = trans('message.msg_nationality_delete_prompt');
            } else {
                Nationality::where('pkNat', $id)->delete();
                $response['message'] = trans('message.msg_nationality_delete_success');
            }
        }

        return response()->json($response,$status_code ?? HttpResponse::HTTP_OK);
    }

    /**
     * Nationalities List
     *
     * @param \Illuminate\Http\Request $request
     * @return JSON
     */
    public function nationalities_list()
    {
        $nationality = Nationality::select('*', 'nat_NationalityName_' . $this->current_language . ' as nat_NationalityName');
        $nationality->when(request('search'), function ($q){
            return $q->where('nat_Uid', 'LIKE', '%' . request('search') . '%')
            ->orWhere('nat_Notes', 'LIKE', '%' . request('search') . '%')
            ->orWhere('nat_NationalityName_' . $this->current_language, 'LIKE', '%' . request('search') . '%');
        });

        return DataTables::of($nationality)
            ->editColumn('cla_Notes', function($nationality){
                return substr($nationality->cla_Notes,0,45);
            })
            ->addColumn('action', function ($nationality) {
                $str = '<a cid="'.$nationality->pkNat.'" onclick="triggerEdit('.$nationality->pkNat.')" href="javascript:void(0)"><i class="fa fa-pen"></i></a>&nbsp;&nbsp';
                $str .= '<a onclick="triggerDelete('.$nationality->pkNat.')" href="javascript:void(0)"><i class="fa fa-trash"></i></a>';
                return $str;
            })
            ->addIndexColumn()
            ->escapeColumns()
            ->toJSON();
    }

}
