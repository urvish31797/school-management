<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\ExtracurricuralActivityType;
use App\Http\Requests\ExtracurricuralActivityTypeRequest;
use DataTableService;
use Datatables;
use HtmlBuilder;
use HttpResponse;

class ExtracurricuralActivityTypeController extends Controller
{
    public function index(HtmlBuilder $builder)
    {
        $builder_data['columns'] = [
            ['data' => 'DT_RowIndex', 'name' => 'DT_RowIndex', 'title' => trans('general.sr_no'), 'render' => null, 'orderable' => false, 'searchable' => false],
            ['data' => 'sat_Uid', 'name' => 'sat_Uid', 'title' => trans('general.gn_uid')],
            ['data' => 'sat_StudentExtracurricuralActivityName', 'name' => 'sat_StudentExtracurricuralActivityName', 'title' => trans('general.gn_name')],
            ['data' => 'sat_Notes', 'name' => 'sat_Notes', 'title' => trans('general.gn_notes')],
            ['data' => 'action', 'name' => 'action', 'title' => trans('general.gn_actions'), 'orderable' => false, 'searchable' => false, 'width' => '10%'],
        ];

        $builder_data['ajax'] = [
            'url'=> route('extracurricuralactivitytype.list'),
            'data' => 'function(d) {
                d.search =  $("#search_extracurricuralActivityType").val();
            }'
        ];

        $dt_html = new DataTableService($builder_data, $builder);
        $dt_html = $dt_html->builder;

        return view('admin.extracurricuralActivityType.extracurricuralActivityType',compact('dt_html'));
    }

    public function store(ExtracurricuralActivityTypeRequest $request)
    {
        $column = 'sat_StudentExtracurricuralActivityName_'.$this->current_language;
        $value = $request[$column];

        $response = [];
        if (!empty($request->pkSat)) {
            $checkPrev = ExtracurricuralActivityType::where($column,$value)
                    ->where('pkSat', '!=', $request->pkSat)->count();
            if ($checkPrev) {
                $status_code = HttpResponse::HTTP_BAD_REQUEST;
                $response['message'] = trans('message.msg_extracurricular_exist');
            } else {
                ExtracurricuralActivityType::where('pkSat', $request->pkSat)->update($request->validated());

                $response['message'] = trans('message.msg_extracurricular_update_success');
            }
        } else {
            $checkPrev = ExtracurricuralActivityType::where($column,$value)
                    ->count();
            if ($checkPrev) {
                $status_code = HttpResponse::HTTP_BAD_REQUEST;
                $response['message'] = trans('message.msg_extracurricular_exist');
            } else {
                $id = ExtracurricuralActivityType::insertGetId($request->validated());
                if (!empty($id)) {
                    ExtracurricuralActivityType::where('pkSat', $id)->update(['sat_Uid' => "ECA" . $id]);
                    $response['message'] = trans('message.msg_extracurricular_add_success');

                } else {
                    $status_code = HttpResponse::HTTP_BAD_REQUEST;
                    $response['message'] = trans('message.msg_something_wrong');
                }
            }
        }

        return response()->json($response,$status_code ?? HttpResponse::HTTP_OK);
    }

    public function edit($cid)
    {
        $response = [];
        $cdata = ExtracurricuralActivityType::where('pkSat', '=', $cid)->first();

        if (empty($cid) || empty($cdata)) {
            $status_code = HttpResponse::HTTP_BAD_REQUEST;
        } else {
            $response['data'] = $cdata;
        }

        return response()->json($response,$status_code ?? HttpResponse::HTTP_OK);

    }

    public function destroy($cid)
    {
        $response = [];

        if (empty($cid)) {
            $status_code = HttpResponse::HTTP_BAD_REQUEST;
        } else {
            ExtracurricuralActivityType::where('pkSat', $cid)->delete();
            $response['message'] = trans('message.msg_extracurricular_delete_success');
        }

        return response()->json($response,$status_code ?? HttpResponse::HTTP_OK);
    }

    /**
     * Fetch Extra Curricular Activity Type List
     *
     * @param \Illuminate\Http\Request $request
     * @return JSON
     */
    public function extracurricuralactivitytype_list()
    {
        $extra_types = ExtracurricuralActivityType::select('*', 'sat_StudentExtracurricuralActivityName_' . $this->current_language . ' as sat_StudentExtracurricuralActivityName');
        $extra_types->when(request('search'),function($q){
            return $q->where('sat_Uid', 'LIKE', '%' . request('search') . '%')
            ->orWhere('sat_Notes', 'LIKE', '%' . request('search') . '%')
            ->orWhere('sat_StudentExtracurricuralActivityName_' . $this->current_language, 'LIKE', '%' . request('search') . '%');
         });

         return DataTables::of($extra_types)
            ->addColumn('action', function ($extra_types) {
                $str = '<a onclick="triggerEdit('.$extra_types->pkSat.')" href="javascript:void(0)"><i class="fa fa-pen"></i></a>&nbsp;&nbsp';
                $str .= '<a onclick="triggerDelete('.$extra_types->pkSat.')" href="javascript:void(0)"><i class="fa fa-trash"></i></a>';
                return $str;
            })
            ->addIndexColumn()
            ->escapeColumns()
            ->toJSON();

    }
}
