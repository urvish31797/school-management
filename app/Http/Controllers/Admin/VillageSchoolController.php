<?php
/**
 * VillageSchoolController
 *
 * @package    Laravel
 * @subpackage Controller
 * @since      1.0
 */

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Admin;
use App\Models\PostalCode;
use App\Models\VillageSchool;
use App\Http\Requests\VillageSchoolRequest;
use DataTableService;
use Datatables;
use HtmlBuilder;
use HttpResponse;
use Illuminate\Http\Request;

class VillageSchoolController extends Controller
{
    public function index(HtmlBuilder $builder)
    {
        $PostalCodes = PostalCode::select('pkPof', 'pof_PostOfficeNumber', 'pof_PostOfficeName_' . $this->current_language . ' as pof_PostOfficeName')
            ->get();

        $builder_data['columns'] = [
            ['data' => 'DT_RowIndex', 'name' => 'DT_RowIndex', 'title' => trans('general.sr_no'), 'render' => null, 'orderable' => false, 'searchable' => false],
            ['data' => 'vsc_Uid', 'name' => 'vsc_Uid', 'title' => trans('general.gn_uid')],
            ['data' => 'vsc_VillageSchoolName', 'name' => 'vsc_VillageSchoolName', 'title' => trans('general.gn_name')],
            ['data' => 'sch_SchoolName', 'name' => 'fkVscSch', 'title' => trans('general.gn_main_school'), 'orderable' => false, 'searchable' => false],
            ['data' => 'vsc_Residence', 'name' => 'vsc_Residence', 'title' => trans('general.gn_residence')],
            ['data' => 'vsc_Notes', 'name' => 'vsc_Notes', 'title' => trans('general.gn_note')],
            ['data' => 'action', 'name' => 'action', 'title' => trans('general.gn_actions'), 'orderable' => false, 'searchable' => false, 'width' => '10%'],
        ];

        $builder_data['ajax'] = [
            'url'=> route('villageschools.list'),
            'data' => 'function(d) {
                d.search =  $("#search_village_school").val();
            }'
        ];

        $dt_html = new DataTableService($builder_data, $builder);
        $dt_html = $dt_html->builder;

        return view('admin.villageSchools.villageSchools')->with(compact('PostalCodes','dt_html'));
    }

    public function store(VillageSchoolRequest $request)
    {
        $response = [];
        $column = 'vsc_VillageSchoolName_'.$this->current_language;
        $value = $request[$column];

        if (!empty($request->pkVsc)) {
            $checkPrev = VillageSchool::where($column,$value)
                    ->where('pkVsc', '!=', $request->pkVsc)->first();
            if (!empty($checkPrev)) {
                $status_code = HttpResponse::HTTP_BAD_REQUEST;
                $response['message'] = trans('message.msg_village_school_exist');
            } else {
                $id = VillageSchool::where('pkVsc', $request->pkVsc)->update($request->validated());
                
                $response['message'] = trans('message.msg_village_school_update_success');
            }
        }

        return response()->json($response,$status_code ?? HttpResponse::HTTP_OK);
    }

    public function edit($cid)
    {
        $response = [];
        $cdata = VillageSchool::where('pkVsc', '=', $cid)->first();

        if (empty($cid) || empty($cdata)) {
            $status_code = HttpResponse::HTTP_BAD_REQUEST;
        } else {
            
            $response['data'] = $cdata;
        }

        return response()->json($response,$status_code ?? HttpResponse::HTTP_OK);
    }

    public function destroy($cid)
    {
        $response = [];

        if (empty($cid)) {
            $status_code = HttpResponse::HTTP_BAD_REQUEST;
        } else {
            VillageSchool::where('pkVsc', $cid)->delete();
            
            $response['message'] = trans('message.msg_village_school_delete_success');
        }

        return response()->json($response,$status_code ?? HttpResponse::HTTP_OK);
    }

    public function villageschools_list(Request $request)
    {        
        $VillageSchool = VillageSchool::with(['school'=>function($q){
            $q->select('pkSch','sch_SchoolName_' . $this->current_language . ' as sch_SchoolName');
        }])
        ->select('*', 'vsc_VillageSchoolName_' . $this->current_language . ' as vsc_VillageSchoolName');

        $VillageSchool->when(request('search'), function ($q){
            return $q->where('vsc_Uid', 'LIKE', '%' . request('search') . '%')
            ->orWhere('vsc_VillageSchoolName_' . $this->current_language, 'LIKE', '%' . request('search') . '%');
        });

        $VillageSchool->when(empty(array_first(request('order'))['column']), function($q){
            return $q->orderBy('pkVsc','ASC');
        });

        return DataTables::of($VillageSchool)
            ->editColumn('vsc_Address', function($VillageSchool){
                return substr($VillageSchool->vsc_Address,0,45);
            })
            ->editColumn('sch_SchoolName', function($VillageSchool){
                return $VillageSchool->school->sch_SchoolName;
            })
            ->addColumn('action', function ($VillageSchool) {
                $str = '<a cid="'.$VillageSchool->pkVsc.'" onclick="triggerEdit('.$VillageSchool->pkVsc.')" href="javascript:void(0)"><i class="fa fa-pen"></i></a>&nbsp;&nbsp';
                $str .= '<a onclick="triggerDelete('.$VillageSchool->pkVsc.')" href="javascript:void(0)"><i class="fa fa-trash"></i></a>';
                return $str;
            })
            ->addIndexColumn()
            ->escapeColumns()
            ->toJSON();
    }

}
