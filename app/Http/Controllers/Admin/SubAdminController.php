<?php
namespace App\Http\Controllers\Admin;

use App\Helpers\FrontHelper;
use App\Helpers\MailHelper;
use App\Helpers\UtilHelper;
use App\Http\Controllers\Controller;
use App\Models\Admin;
use App\Models\Canton;
use App\Models\Country;
use App\Models\Employee;
use App\Models\State;
use App\Models\Student;
use App\Http\Traits\UserTrait;
use App\Http\Traits\FileTrait;
use Illuminate\Http\Request;

class SubAdminController extends Controller
{
    use UserTrait,FileTrait;

    public function index(Request $request)
    {
        return view('admin.subAdmins.subAdmins');
    }

    public function create(Request $request)
    {
        $data = Canton::get();

        $datas = ['data' => $data];

        return view('admin.subAdmins.addSubAdmin', $datas);
    }

    public function store(Request $request)
    {
        $response = [];
        $input = $request->all();
        if (isset($input['id']) && !empty($input['id'])) {
            $id = $input['id'];
            $check_user_params = ['type'=>'Admin',
                                 'id'=>$id,
                                 'email'=>$input['email'],
                                 'gov_id'=>$input['adm_GovId'],
                                 'temp_id'=>$input['adm_TempGovId']];
            $check_user_status = $this->checkUserEmailorUidExist($check_user_params);

            if($check_user_status['email_exist']){
                $response['status'] = false;
                $response['message'] = trans('message.msg_email_exist');
                return response()->json($response);
            }

            if($check_user_status['gov_id_exist']){
                $response['status'] = false;
                $response['message'] = trans('message.msg_govt_id_exist');
                return response()->json($response);
            }

            if($check_user_status['temp_id_exist']){
                $response['status'] = false;
                $response['message'] = trans('message.msg_govt_id_exist');
                return response()->json($response);
            }

            $user = Admin::find($id);
            $user->adm_Name = $input['adm_Name'];
            $user->adm_Phone = $input['adm_Phone'];
            $user->adm_Title = $input['adm_Title'];
            $user->adm_GovId = $input['adm_GovId'];
            $user->adm_TempGovId = $input['adm_TempGovId'];
            $user->adm_Gender = $input['adm_Gender'];
            $user->fkAdmCan = $input['fkAdmCan'];
            $user->adm_Status = $input['adm_Status'];
            $user->adm_Address = $input['adm_Address'];
            $user->adm_DOB = UtilHelper::sqlDate($input['adm_DOB'],2);

            if($request->file('upload_profile')){
                if (!empty($user->adm_Photo)) {
                    $unlink_file = $user->adm_Photo;
                }

                $upload_params = [
                    'folder_path'=>config('assetpath.user_images'),
                    'unlink_file'=>$unlink_file ?? ''
                ];
                $image = $request->file('upload_profile');
                $filename = $this->uploadFiletoStorage($image,$upload_params);

                if(!empty($filename)){
                    $user->adm_Photo = $filename;
                }
            }

            if ($user->email != $input['email']) {
                $current_time = date("Y-m-d H:i:s");
                $verification_key = md5(FrontHelper::generatePassword(20));

                $reset_pass_token = base64_encode($input['email'] . '&&MinistrySubAdmin&&' . $current_time);
                $data = ['email' => $input['email'], 'name' => $input['adm_Name'], 'verify_key' => $verification_key, 'reset_pass_link' => $reset_pass_token, 'subject' => 'New Ministry Sub Admin Credentials'];

                MailHelper::sendNewCredentials($data);
                $user->email = $input['email'];
                $user->email_verified_at = null;
                $user->email_verification_key = $verification_key;
            }

            if ($user->save()) {
                $response['status'] = true;
                $response['message'] = trans('message.msg_sub_admin_update_success');
            } else {
                $response['status'] = false;
                $response['message'] = trans('message.msg_something_wrong');
            }
        } else {
            $image = $request->file('upload_profile');

            $temp_pass = FrontHelper::generatePassword(10);
            $verification_key = md5(FrontHelper::generatePassword(20));

            $check_user_params = ['type'=>'Admin',
                                 'email'=>$input['email'],
                                 'gov_id'=>$input['adm_GovId'],
                                 'temp_id'=>$input['adm_TempGovId']];
            $check_user_status = $this->checkUserEmailorUidExist($check_user_params);

            if($check_user_status['email_exist']){
                $response['status'] = false;
                $response['message'] = trans('message.msg_email_exist');
                return response()->json($response);
            }

            if($check_user_status['gov_id_exist']){
                $response['status'] = false;
                $response['message'] = trans('message.msg_govt_id_exist');
                return response()->json($response);
            }

            if($check_user_status['temp_id_exist']){
                $response['status'] = false;
                $response['message'] = trans('message.msg_govt_id_exist');
                return response()->json($response);
            }

            $user = new Admin;
            $user->email = $input['email'];
            $user->adm_Name = $input['adm_Name'];
            $user->adm_Phone = $input['adm_Phone'];
            $user->email_verification_key = $verification_key;
            $user->adm_Title = $input['adm_Title'];
            $user->adm_GovId = $input['adm_GovId'];
            $user->adm_TempGovId = $input['adm_TempGovId'];
            $user->adm_Gender = $input['adm_Gender'];
            $user->fkAdmCan = $input['fkAdmCan'];
            $user->adm_Status = $input['adm_Status'];
            $user->adm_Address = $input['adm_Address'];
            $user->type = 'MinistrySubAdmin';
            $user->adm_DOB = UtilHelper::sqlDate($input['adm_DOB'],2);

            if($request->file('upload_profile')){
                if (!empty($user->adm_Photo)) {
                    $unlink_file = $user->adm_Photo;
                }

                $upload_params = [
                    'folder_path'=>config('assetpath.user_images')
                ];
                $image = $request->file('upload_profile');
                $filename = $this->uploadFiletoStorage($image,$upload_params);

                if(!empty($filename)){
                    $user->adm_Photo = $filename;
                }
            }

            if ($user->save()) {
                Admin::where('id', $user->id)->update(['adm_Uid' => "SBA" . $user->id]);
                $current_time = date("Y-m-d H:i:s");
                $reset_pass_token = base64_encode($input['email'] . '&&MinistrySubAdmin&&' . $current_time);
                $data = ['email' => $input['email'], 'name' => $input['adm_Name'], 'verify_key' => $verification_key, 'pass' => $temp_pass, 'reset_pass_link' => $reset_pass_token, 'subject' => 'New Ministry Sub Admin Credentials'];

                MailHelper::sendNewCredentials($data);

                $response['status'] = true;
                $response['message'] = trans('message.msg_sub_admin_add_success');
            } else {
                $response['status'] = false;
                $response['message'] = trans('message.msg_something_wrong');
            }
        }

        return response()->json($response);
    }

    public function edit($id)
    {
        $response = [];
        $cantons = Canton::get();
        $mdata = Admin::where('id', $id)->first();
        $mdata['cantons'] = $cantons;

        return view('admin.subAdmins.editSubAdmin', ['data' => $mdata]);
    }

    public function show($id)
    {
        $mdata = '';
        if (!empty($id)) {
            $mdata = Admin::where('id', '=', $id)->where('deleted_at', '=', null)
                ->first();
            $canton = Canton::select('can_CantonName_' . $this->current_language . ' as can_CantonName', 'fkCanSta')
                ->where('pkCan', $mdata->fkAdmCan)
                ->first();
            $state = State::select('sta_StateName_' . $this->current_language . ' as sta_StateName', 'fkStaCny')
                ->where('pkSta', $canton->fkCanSta)
                ->first();
            $country = Country::select('cny_CountryName_' . $this->current_language . ' as cny_CountryName')
                ->where('pkCny', $state->fkStaCny)
                ->first();
            $mdata['canton'] = $canton->can_CantonName;
            $mdata['state'] = $state->sta_StateName;
            $mdata['country'] = $country->cny_CountryName;
        }
        if (request()
            ->ajax()) {
            return \View::make('admin.subAdmins.viewSubAdmin')
                ->with(['mdata' => $mdata])->renderSections();
        }
        return view('admin.subAdmins.viewSubAdmin', ['mdata' => $mdata]);

    }

    public function destroy(Request $request, $cid)
    {
        $response = [];

        if (empty($cid)) {
            $response['status'] = false;
        } else {
            Admin::where('id', $cid)->where('type', 'MinistrySubAdmin')->delete();
            $response['status'] = true;
        }

        return response()->json($response);
    }

    /**
     * Get Subadmin List
     *
     * @param \Illuminate\Http\Request $request
     * @return JSON
     */
    public function subadmin_list(Request $request)
    {
        $data = $request->all();

        $perpage = !empty($data['length']) ? (int) $data['length'] : 10;

        $filter = isset($data['search']) && is_string($data['search']) ? $data['search'] : '';

        $sort_type = isset($data['order'][0]['dir']) && is_string($data['order'][0]['dir']) ? $data['order'][0]['dir'] : '';
        $sort_col = $data['order'][0]['column'];

        $sort_field = $data['columns'][$sort_col]['data'];

        $admin = new Admin;

        if ($filter) {
            $admin = $admin->where('adm_Uid', 'LIKE', '%' . $filter . '%')->orWhere('adm_Name', 'LIKE', '%' . $filter . '%')->orWhere('email', 'LIKE', '%' . $filter . '%')->orWhere('adm_GovId', 'LIKE', '%' . $filter . '%');
        }
        $adminQuery = $admin->where('type', '=', 'MinistrySubAdmin');

        if ($sort_col != 0) {
            $adminQuery = $adminQuery->orderBy($sort_field, $sort_type);
        }

        $total_admins = $adminQuery->count();

        $offset = $data['start'];

        $counter = $offset;
        $admindata = [];
        $admins = $adminQuery->offset($offset)->limit($perpage)->get()
            ->toArray();

        foreach ($admins as $key => $value) {
            $value['index'] = $counter + 1;
            $value['adm_Statu'] = $value['adm_Status'];
            if ($value['adm_Status'] == 'Active') {
                $value['adm_Status'] = trans('general.gn_active');
            } else {
                $value['adm_Status'] = trans('general.gn_inactive');
            }
            $admindata[$counter] = $value;
            $counter++;
        }

        $price = array_column($admindata, 'index');

        if ($sort_col == 0) {
            if ($sort_type == 'desc') {
                array_multisort($price, SORT_DESC, $admindata);
            } else {
                array_multisort($price, SORT_ASC, $admindata);
            }
        }

        $admindata = array_values($admindata);

        $result = array(
            "draw" => $data['draw'],
            "recordsTotal" => $total_admins,
            "recordsFiltered" => $total_admins,
            "data" => $admindata,
        );

        return response()->json($result);
    }

}
