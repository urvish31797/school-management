<?php
/**
 * TeacherController
 *
 * @package    Laravel
 * @subpackage Controller
 * @since      1.0
 */

namespace App\Http\Controllers\Admin;

use App\Helpers\FrontHelper;
use App\Helpers\MailHelper;
use App\Helpers\UtilHelper;
use App\Http\Controllers\Controller;
use App\Http\Traits\CommonTrait;
use App\Models\AcademicDegree;
use App\Models\Admin;
use App\Models\Citizenship;
use App\Models\College;
use App\Models\Country;
use App\Models\Employee;
use App\Models\EmployeeDesignation;
use App\Models\EmployeesEducationDetail;
use App\Models\EmployeesEngagement;
use App\Models\EmployeesWeekHourRates;
use App\Models\EmployeeType;
use App\Models\EngagementType;
use App\Models\Nationality;
use App\Models\QualificationDegree;
use App\Models\Religion;
use App\Models\School;
use App\Models\University;
use Illuminate\Http\Request;

class TeacherController extends Controller
{
    use CommonTrait;

    public function index(Request $request)
    {
        $employeeType = EmployeeType::where('epty_Name', '!=', 'SchoolSubAdmin')->where('epty_Name', '!=', 'HomeroomTeacher')
            ->get();
        if (request()
            ->ajax()) {
            return \View::make('employee.employees.employees')
                ->with('employeeType', $employeeType)->renderSections();
        }
        return view('employee.employees.employees', ['employeeType' => $employeeType]);
    }

    /**
     * Edit employee page and fetch employee data
     *
     * @param [int] $id
     * @return view - employee.employees.editEmployee
     */
    public function edit($id)
    {
        $response = [];
        $AcademicDegrees = AcademicDegree::select('pkAcd', 'acd_AcademicDegreeName_' . $this->current_language . ' as acd_AcademicDegreeName')
            ->get();
        $Colleges = College::select('pkCol', 'col_CollegeName_' . $this->current_language . ' as col_CollegeName')
            ->get();
        $Countries = Country::select('pkCny', 'cny_CountryName_' . $this->current_language . ' as cny_CountryName')
            ->get();
        $QualificationDegrees = QualificationDegree::select('pkQde', 'qde_QualificationDegreeName_' . $this->current_language . ' as qde_QualificationDegreeName')
            ->get();

        $Municipalities = $this->getMuncipalityDropdownData($this->current_language);
        $PostalCodes = $this->getPostalCodeDropdownData($this->current_language);

        $Citizenships = Citizenship::select('pkCtz', 'ctz_CitizenshipName_' . $this->current_language . ' as ctz_CitizenshipName')
            ->get();
        $Nationalities = Nationality::select('pkNat', 'nat_NationalityName_' . $this->current_language . ' as nat_NationalityName')
            ->get();
        $Religions = Religion::select('pkRel', 'rel_ReligionName_' . $this->current_language . ' as rel_ReligionName')
            ->get();
        $Universities = University::select('pkUni', 'uni_UniversityName_' . $this->current_language . ' as uni_UniversityName')
            ->get();
        $EmployeeDesignations = EmployeeDesignation::select('pkEde', 'ede_EmployeeDesignationName_' . $this->current_language . ' as ede_EmployeeDesignationName')
            ->get();
        $Schools = School::select('pkSch', 'sch_SchoolName_' . $this->current_language . ' as sch_SchoolName')
            ->get();
        $EngagementTypes = EngagementType::select('pkEty', 'ety_EngagementTypeName_' . $this->current_language . ' as ety_EngagementTypeName')
            ->get();

        $mainSchool = EmployeesEngagement::select('fkEenSch')->where('fkEenEmp', $this
                ->logged_user
                ->id)
                ->first();
        if ($mainSchool) {
            $mainSchool = $mainSchool->fkEenSch;
        }

        $employeeType = EmployeeType::select('pkEpty', 'epty_Name')->where('epty_Name', '!=', 'SchoolSubAdmin')
            ->get();

        $EmployeesDetail = Employee::with(['employeeEducation', 'employeeEducation.academicDegree' => function ($query) {
            $query->select('pkAcd', 'acd_AcademicDegreeName_' . $this->current_language . ' as acd_AcademicDegreeName');
        }
            , 'employeeEducation.college' => function ($query) {
                $query->select('pkCol', 'col_CollegeName_' . $this->current_language . ' as col_CollegeName');
            }
            , 'employeeEducation.university' => function ($query) {
                $query->select('pkUni', 'uni_UniversityName_' . $this->current_language . ' as uni_UniversityName');
            }
            , 'employeeEducation.university.college', 'employeeEducation.qualificationDegree' => function ($query) {
                $query->select('pkQde', 'qde_QualificationDegreeName_' . $this->current_language . ' as qde_QualificationDegreeName');
            }
            , 'employeeEducation.employeeDesignation' => function ($query) {
                $query->select('pkEde', 'ede_EmployeeDesignationName_' . $this->current_language . ' as ede_EmployeeDesignationName');
            }
            , 'municipality' => function ($query) {
                $query->select('pkMun', 'mun_MunicipalityName_' . $this->current_language . ' as mun_MunicipalityName');
            }
            , 'postalCode' => function ($query) {
                $query->select('pkPof', 'pof_PostOfficeNumber');
            }
            , 'country' => function ($query) {
                $query->select('pkCny', 'cny_CountryName_' . $this->current_language . ' as cny_CountryName');
            }
            , 'nationality' => function ($query) {
                $query->select('pkNat', 'nat_NationalityName_' . $this->current_language . ' as nat_NationalityName');
            }
            , 'religion' => function ($query) {
                $query->select('pkRel', 'rel_ReligionName_' . $this->current_language . ' as rel_ReligionName');
            }
            , 'citizenship' => function ($query) {
                $query->select('pkCtz', 'ctz_CitizenshipName_' . $this->current_language . ' as ctz_CitizenshipName');
            }
            , 'EmployeesEngagement.engagementType' => function ($query) {
                $query->select('pkEty', 'ety_EngagementTypeName_' . $this->current_language . ' as ety_EngagementTypeName');
            }
            , 'EmployeesEngagement.school' => function ($query) {
                $query->select('pkSch', 'sch_SchoolName_' . $this->current_language . ' as sch_SchoolName');
            }
            , 'EmployeesEngagement.employeeType', 'EmployeesEngagement' => function ($query) {
                $query->whereNull('een_DateOfFinishEngagement');
            }
            ,

        ]);
        $EmployeesDetail = $EmployeesDetail->where('id', '=', $id)->first();

        $datas = ['employeeType' => $employeeType, 'Countries' => $Countries, 'Municipalities' => $Municipalities, 'PostalCodes' => $PostalCodes, 'Citizenships' => $Citizenships, 'Nationalities' => $Nationalities, 'Religions' => $Religions, 'Universities' => $Universities, 'Colleges' => $Colleges, 'EmployeeDesignations' => $EmployeeDesignations, 'QualificationDegrees' => $QualificationDegrees, 'AcademicDegrees' => $AcademicDegrees, 'EmployeesDetail' => $EmployeesDetail, 'Schools' => $Schools, 'EngagementTypes' => $EngagementTypes, 'mainSchool' => $mainSchool];
        if (request()
            ->ajax()) {
            return \View::make('employee.employees.editEmployee')
                ->with($datas)->renderSections();
        }
        return view('employee.employees.editEmployee')
            ->with($datas);
    }

    /**
     * View Employee Page and view employee data
     *
     * @param [int] $id
     * @return view - employee.employees.viewEmployee
     */
    public function show($id)
    {
        $mdata = '';
        if (!empty($id)) {

            $mdata = Employee::with(['country' => function ($query) {
                $query->select('pkCny', 'cny_CountryName_' . $this->current_language . ' as cny_CountryName');
            }
                , "employeesEngagement" => function ($q) use ($id) {
                    $q->whereNull('een_DateOfFinishEngagement')
                        ->where('fkEenEmp', $id);
                }
                , "employeesEngagement.getLatestHourRates"
                , "employeesEngagement.getAllHourRates" => function ($q) {
                    $q->orderBy('pkEwh', 'DESC');
                }
                , "employeesEngagement.school" => function ($q) {
                    $q->select('pkSch', 'sch_SchoolName_' . $this->current_language . ' as sch_SchoolName');
                }
                , 'employeesEngagement.employeeType', 'employeesEngagement.engagementType' => function ($query) {
                    $query->select('pkEty', 'ety_EngagementTypeName_' . $this->current_language . ' as ety_EngagementTypeName');
                }
            ])
                ->where('id', $id)->first();
        }
        if (request()
            ->ajax()) {
            return \View::make('employee.employees.viewEmployee')
                ->with('mdata', $mdata)->renderSections();
        }
        return view('employee.employees.viewEmployee', ['mdata' => $mdata]);

    }

    public function store(Request $request)
    {
        try {
            $response = [];
            $input = $request->all();
            //Edit Only Engagement
            if (isset($input['emp_engagment_id']) && !empty($input['emp_engagment_id'])) {

                $inData = [];
                foreach ($input as $key => $value) {
                    foreach ($value as $k => $v) {
                        $inData[$k][$key] = $v;
                    }
                }
                $engData = [];

                foreach ($inData as $key => $inDatav) {
                    if (isset($inDatav['een_ActingAsPrincipal']) && $inDatav['fkEenEpty'] == 1) {
                        $acting = "Yes";
                    } else {
                        $acting = "No";
                    }

                    $engData['een_DateOfEngagement'] = UtilHelper::sqlDate($inDatav['start_date']);
                    $engData['fkEenEty'] = $inDatav['fkEenEty'];
                    $engData['fkEenEpty'] = $inDatav['fkEenEpty'];
                    $engData['een_Notes'] = $inDatav['een_Notes'];
                    $engData['een_ActingAsPrincipal'] = $acting;
                    if (isset($inDatav['end_date']) && !empty($inDatav['end_date'])) {
                        $engData['een_DateOfFinishEngagement'] = UtilHelper::sqlDate($inDatav['end_date']);
                    } else {
                        $engData['een_DateOfFinishEngagement'] = null;
                    }

                    EmployeesEngagement::where('pkEen', $inDatav['emp_engagment_id'])->update($engData);
                    EmployeesWeekHourRates::addHourlyRate($inDatav['emp_engagment_id'], $inDatav['ewh_WeeklyHoursRate'], $inDatav['ewh_Notes'], UtilHelper::sqlDate($inDatav['start_date']));
                }

                if ($inDatav['emp_engagment_id']) {
                    $response['status'] = true;
                    $response['message'] = trans('message.msg_Teacher_update_success');
                } else {
                    $response['status'] = false;
                    $response['message'] = trans('message.msg_something_wrong');
                }
                return response()
                    ->json($response);

            }

            $image = $request->file('upload_profile');
            $id = $input['id'];
            $pkSch = $input['sid'];

            $emailExistCount = Employee::where('email', '=', $input['email'])->where('id', '!=', $id)->count();
            $emailAdmExistCount = Admin::where('email', '=', $input['email'])->count();

            $govIdExistCount = 0;
            if (!empty($input['emp_EmployeeID'])) {
                $govIdExistCount = Employee::where('emp_EmployeeID', '=', $input['emp_EmployeeID'])
                    ->where('id', '!=', $id)
                    ->count();
            }

            $tempIdExistCount = 0;
            if (!empty($input['emp_TempCitizenId'])) {
                $tempIdExistCount = Employee::where('emp_TempCitizenId', '=', $input['emp_TempCitizenId'])
                    ->where('emp_TempCitizenId', '!=', null)
                    ->where('id', '!=', $id)
                    ->count();
            }

            if ($emailExistCount != 0 || $emailAdmExistCount != 0) {
                $response['status'] = false;
                $response['message'] = trans('message.msg_email_exist');
                return response()
                    ->json($response);
            }

            if ($govIdExistCount != 0) {
                $response['status'] = false;
                $response['message'] = trans('message.msg_employee_id_exist');
                return response()
                    ->json($response);
            }

            if ($tempIdExistCount != 0) {
                $response['status'] = false;
                $response['message'] = trans('message.msg_temp_citizen_id_exist');
                return response()
                    ->json($response);
            }

            $user = Employee::find($id);
            $user->emp_EmployeeName = $input['emp_EmployeeName'];
            $user->emp_EmployeeSurname = $input['emp_EmployeeSurname'];
            $user->emp_EmployeeID = $input['emp_EmployeeID'];
            $user->emp_TempCitizenId = $input['emp_TempCitizenId'];
            $user->emp_EmployeeGender = $input['emp_EmployeeGender'];
            $user->emp_DateOfBirth = UtilHelper::sqlDate($input['emp_DateOfBirth']);
            $user->emp_PlaceOfBirth = $input['emp_PlaceOfBirth'];
            $user->emp_Address = $input['emp_Address'];
            $user->emp_PhoneNumber = $input['emp_PhoneNumber'];
            $user->emp_Notes = $input['emp_Notes'];
            $user->fkEmpMun = $input['fkEmpMun'];
            $user->fkEmpPof = $input['fkEmpPof'];
            $user->fkEmpCny = $input['fkEmpCny'];
            $user->fkEmpNat = $input['fkEmpNat'];
            $user->fkEmpRel = $input['fkEmpRel'];
            $user->fkEmpCtz = $input['fkEmpCtz'];
            $user->emp_Status = $input['emp_Status'];

            $empType = EmployeeType::select('pkEpty')->where('epty_Name', '=', 'Teacher')
                ->where('epty_ParentId', '=', null)
                ->first();
            $SchoolData = School::select('sch_SchoolName_en')->where('pkSch', $pkSch)->first();

            if (!empty($image)) {
                $input['emp_PicturePath'] = time() . '.' . $image->getClientOriginalExtension();
                $destinationPath = storage_path('app/public/images/users');
                $image->move($destinationPath, $input['emp_PicturePath']);
                if (!empty($user->emp_PicturePath)) {
                    $filepath = storage_path('app/public/images/users') . "/" . $user->emp_PicturePath;
                    if (file_exists($filepath)) {
                        unlink($filepath);
                    }
                }
                $user->emp_PicturePath = $input['emp_PicturePath'];
            }

            $tdata = $input['total_details'];
            $details = [];
            for ($i = 1; $i <= $tdata; $i++) {
                $filename = '';
                $image = '';
                if ($request->hasFile('eed_DiplomaPicturePath_' . $i)) {
                    $image = $request->file('eed_DiplomaPicturePath_' . $i);
                    $filename = time() . $i . '.' . $image->getClientOriginalExtension();
                    $destinationPath = storage_path('app/public/files/users');
                    $image->move($destinationPath, $filename);
                } else {
                    $filename = $input['file_name_' . $i];
                }
                $details[] = ['fkEedEmp' => $user->id, 'fkEedCol' => $input['fkEedCol_' . $i], 'fkEedUni' => $input['fkEedUni_' . $i], 'fkEedAcd' => $input['fkEedAcd_' . $i], 'fkEedQde' => $input['fkEedQde_' . $i], 'fkEedEde' => $input['fkEedEde_' . $i], 'eed_ShortTitle' => $input['eed_ShortTitle_' . $i], 'eed_SemesterNumbers' => $input['eed_SemesterNumbers_' . $i], 'eed_EctsPoints' => $input['eed_EctsPoints_' . $i], 'eed_YearsOfPassing' => $input['eed_YearsOfPassing_' . $i], 'eed_Notes' => $input['eed_Notes_' . $i], 'eed_PicturePath' => $filename];
            }
            EmployeesEducationDetail::where('fkEedEmp', $id)->delete();

            EmployeesEducationDetail::insert($details);

            if ($user->email != $input['email']) {
                $current_time = date("Y-m-d H:i:s");
                $verification_key = md5(FrontHelper::generatePassword(20));

                $reset_pass_token = base64_encode($input['email'] . '&&Teacher&&' . $current_time);
                $data = ['email' => $input['email'], 'name' => $input['emp_EmployeeName'] . " " . $input['emp_EmployeeSurname'], 'school' => $SchoolData->sch_SchoolName_en, 'verify_key' => $verification_key, 'reset_pass_link' => $reset_pass_token, 'subject' => 'New School Teacher Credentials'];

                MailHelper::sendNewTeacherCredentials($data);

                $user->email = $input['email'];
                $user->email_verified_at = null;
                $user->email_verification_key = $verification_key;
            }

            if ($user->save()) {
                $response['status'] = true;
                $response['message'] = trans('message.msg_Teacher_update_success');
                $response['redirect'] = url('/admin/employees');
            } else {
                $response['status'] = false;
                $response['message'] = trans('message.sg_something_wrong');
            }

            return response()
                ->json($response);
        } catch (\Exception $e) {

            $response['status'] = false;
            $response['message'] = trans('message.sg_something_wrong');

            return response()
                ->json($response);
        }
    }

    /**
     * Employee List
     *
     * @param \Illuminate\Http\Request $request
     * @return void
     */
    public function employees_list(Request $request)
    {
        $data = $request->all();

        $perpage = !empty($data['length']) ? (int) $data['length'] : 10;

        $filter = isset($data['search']) && is_string($data['search']) ? $data['search'] : '';

        $sort_type = isset($data['order'][0]['dir']) && is_string($data['order'][0]['dir']) ? $data['order'][0]['dir'] : '';
        $sort_col = isset($data['order'][0]['column']) ? $data['order'][0]['column'] : '';

        $sort_field = isset($data['columns'][$sort_col]['data']) ? $data['columns'][$sort_col]['data'] : '';

        $empPostoffices = '';
        if ($this->logged_user->type == "MinistryAdmin") {
            $cantonId = $this->logged_user->fkAdmCan;
            $empPostoffices = Employee::whereHas('postalCode.municipality.canton', function ($q) use ($cantonId) {
                $q->where('pkCan', $cantonId);
            })->get()->pluck('fkEmpPof')->all();
        }

        if (isset($data['emp_type']) && !empty($data['emp_type'])) {
            $emp_type[] = $data['emp_type'];
        }
        $allowedIn = isset($data['emp_type']) ? $emp_type : [$this->employees[0], $this->employees[1], $this->employees[2]];
        $allowedTypes = $this->getSelectedRoles($allowedIn);

        $Teacher = Employee::whereHas("employeesEngagement", function ($q) use ($filter, $allowedTypes) {
            $q->where('een_DateOfFinishEngagement', '=', null)
                ->whereIn('fkEenEpty', $allowedTypes);
        })->with(["employeesEngagement" => function ($q) use ($filter, $allowedTypes) {
            $q->where('een_DateOfFinishEngagement', '=', null)
                ->whereIn('fkEenEpty', $allowedTypes);
        }
            , 'employeesEngagement.employeeType', 'employeesEngagement.engagementType' => function ($query) {
                $query->select('pkEty', 'ety_EngagementTypeName_' . $this->current_language . ' as ety_EngagementTypeName');
            }
        ]);

        if ($filter) {
            $Teacher = $Teacher->where(function ($q) use ($filter) {
                $q->orWhere(function ($query) use ($filter) {
                    $explodestring = explode(" ", $filter);
                    $firstname = $explodestring[0] ?? $filter;
                    $lastname = $explodestring[1] ?? $filter;

                    $query->where('emp_EmployeeID', 'LIKE', '%' . $filter . '%')
                        ->orWhere('emp_EmployeeName', 'LIKE', '%' . $firstname . '%')
                        ->orWhere('emp_EmployeeSurname', 'LIKE', $lastname . '%')
                        ->orWhere('email', 'LIKE', '%' . $filter . '%');
                });
            });
        }

        if ($this->logged_user->type == "MinistryAdmin") {
            $Teacher = $Teacher->where(function ($q) use ($empPostoffices) {
                $q->whereIn('fkEmpPof', $empPostoffices);
            });
        }

        if ($sort_col != 0) {
            $Teacher = $Teacher->orderBy($sort_field, $sort_type);
        }
        $total_admins = $Teacher->count();

        $offset = $data['start'];
        $counter = $offset;

        $Teacher = $Teacher->offset($offset)->limit($perpage)->get()
            ->toArray();

        $teacherdata = [];

        foreach ($Teacher as $key => $value) {
            if (!empty($value['employees_engagement'])) {

                $type = [];
                foreach ($value['employees_engagement'] as $k => $v) {

                    if ($v['employee_type']['epty_subCatName'] != '') {
                        $type[] = $v['employee_type']['epty_subCatName'];
                    } else {
                        $type[] = $v['employee_type']['epty_Name'];
                    }
                }

                $etype = implode(", ", $type);

                if ($value['emp_Status'] == 'Active') {
                    $emp_status = trans('general.gn_active');
                } else {
                    $emp_status = trans('general.gn_inactive');
                }

                $found = array_column($teacherdata, 'id');
                if (!in_array($value['id'], $found)) {
                    $teacherdata[] = ['index' => $counter + 1, 'id' => $value['id'], 'emp_TempCitizenId' => $value['emp_TempCitizenId'], 'emp_EmployeeID' => $value['emp_EmployeeID'], 'type' => $etype, 'email' => $value['email'], 'emp_EmployeeName' => $value['emp_EmployeeName'], 'emp_EmployeeSurname' => $value['emp_EmployeeSurname'], 'emp_EmployeeSurname' => $value['emp_EmployeeSurname'], 'emp_Status' => $emp_status];
                    $counter++;
                }
            }
        }

        $price = array_column($teacherdata, 'index');

        if ($sort_col == 0) {
            if ($sort_type == 'desc') {
                array_multisort($price, SORT_DESC, $teacherdata);
            } else {
                array_multisort($price, SORT_ASC, $teacherdata);
            }
        }

        $teacherdata = array_values($teacherdata);
        $result = array(
            "draw" => $data['draw'],
            "recordsTotal" => $total_admins,
            "recordsFiltered" => $total_admins,
            "data" => $teacherdata,
        );

        return response()->json($result);
    }
}
