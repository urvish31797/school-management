<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\StudentBehaviour;
use App\Http\Requests\StudentBehaviourRequest;
use DataTableService;
use Datatables;
use HtmlBuilder;
use HttpResponse;

class StudentBehaviourController extends Controller
{
    public function index(HtmlBuilder $builder)
    {
        $builder_data['columns'] = [
            ['data' => 'DT_RowIndex', 'name' => 'DT_RowIndex', 'title' => trans('general.sr_no'), 'render' => null, 'orderable' => false, 'searchable' => false],
            ['data' => 'sbe_Uid', 'name' => 'sbe_Uid', 'title' => trans('general.gn_uid')],
            ['data' => 'sbe_BehaviourName', 'name' => 'sbe_BehaviourName', 'title' => trans('general.gn_name')],
            ['data' => 'sbe_Abbriviation', 'name' => 'sbe_Abbriviation', 'title' => trans('general.gn_behaviour_abbreviation')],
            ['data' => 'sbe_Notes', 'name' => 'sbe_Notes', 'title' => trans('general.gn_notes')],
            ['data' => 'action', 'name' => 'action', 'title' => trans('general.gn_actions'), 'orderable' => false, 'searchable' => false, 'width' => '10%'],
        ];

        $builder_data['ajax'] = [
            'url'=> route('studentbehaviour.list'),
            'data' => 'function(d) {
                d.search =  $("#search_studentBehaviour").val();
            }'
        ];

        $dt_html = new DataTableService($builder_data, $builder);
        $dt_html = $dt_html->builder;

        return view('admin.studentBehaviour.studentBehaviour',compact('dt_html'));
    }

    /**
     * Add & Update Student Behaviour
     *
     * @return JSON - $response
     */
    public function store(StudentBehaviourRequest $request)
    {
        $column = 'sbe_BehaviourName_'.$this->current_language;
        $value = $request[$column];

        $response = [];
        if (!empty($request->pkSbe)) {
            $checkPrev = StudentBehaviour::where($column,$value)
                    ->where('pkSbe', '!=', $request->pkSbe)->count();
            if ($checkPrev) {
                $status_code = HttpResponse::HTTP_BAD_REQUEST;
                $response['message'] = trans('message.msg_behaviour_exist');
            } else {
                StudentBehaviour::where('pkSbe', $request->pkSbe)->update($request->validated());
                $response['message'] = trans('message.msg_behaviour_update_success');
            }
        } else {
            $checkPrev = StudentBehaviour::where($column,$value)
                    ->count();
            if ($checkPrev) {
                $status_code = HttpResponse::HTTP_BAD_REQUEST;
                $response['message'] = trans('message.msg_behaviour_exist');
            } else {
                $id = StudentBehaviour::insertGetId($request->validated());
                if (!empty($id)) {
                    StudentBehaviour::where('pkSbe', $id)->update(['sbe_Uid' => "SBE" . $id]);
                    $response['message'] = trans('message.msg_behaviour_add_success');

                } else {
                    $status_code = HttpResponse::HTTP_BAD_REQUEST;
                    $response['message'] = trans('message.msg_something_wrong');
                }
            }
        }

        return response()->json($response,$status_code ?? HttpResponse::HTTP_OK);
    }

    /**
     * Get single student behaviour
     *
     * @param \Illuminate\Http\Request $request
     * @return JSON - $response
     */
    public function edit($cid)
    {
        $response = [];
        $cdata = StudentBehaviour::where('pkSbe', '=', $cid)->first();

        if (empty($cid) || empty($cdata)) {
            $status_code = HttpResponse::HTTP_BAD_REQUEST;
        } else {
            $response['data'] = $cdata;
        }

        return response()->json($response,$status_code ?? HttpResponse::HTTP_OK);

    }

    /**
     * Delete Student Behaviour
     *
     * @param \Illuminate\Http\Request $request
     * @return JSON - $response
     */
    public function destroy($cid)
    {
        $response = [];

        if (empty($cid)) {
            $status_code = HttpResponse::HTTP_BAD_REQUEST;
        } else {
            StudentBehaviour::where('pkSbe', $cid)->delete();
            $response['message'] = trans('message.msg_behaviour_delete_success');
        }

        return response()->json($response,$status_code ?? HttpResponse::HTTP_OK);
    }

    /**
     * List of Student Behaviours
     *
     * @param \Illuminate\Http\Request $request
     * @return JSON - result
     */
    public function studentbehaviour_list()
    {
        $student_behaviour = StudentBehaviour::select('*', 'sbe_BehaviourName_' . $this->current_language . ' as sbe_BehaviourName');
        $student_behaviour->when(request('search'),function($q){
            return $q->where('sbe_Uid', 'LIKE', '%' . request('search') . '%')
            ->orWhere('sbe_Notes', 'LIKE', '%' . request('search') . '%')
            ->orWhere('sbe_Abbriviation', 'LIKE', '%' . request('search') . '%')
            ->orWhere('sbe_BehaviourName_' . $this->current_language, 'LIKE', '%' . request('search') . '%');
         });

         return DataTables::of($student_behaviour)
            ->addColumn('action', function ($student_behaviour) {
                $str = '<a onclick="triggerEdit('.$student_behaviour->pkSbe.')" href="javascript:void(0)"><i class="fa fa-pen"></i></a>&nbsp;&nbsp';
                $str .= '<a onclick="triggerDelete('.$student_behaviour->pkSbe.')" href="javascript:void(0)"><i class="fa fa-trash"></i></a>';
                return $str;
            })
            ->addIndexColumn()
            ->escapeColumns()
            ->toJSON();
    }

}
