<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\EmployeesEngagement;
use App\Models\EngagementType;
use App\Http\Requests\EngagementTypeRequest;
use DataTableService;
use Datatables;
use HtmlBuilder;
use HttpResponse;

class EngagementTypeController extends Controller
{
    public function index(HtmlBuilder $builder)
    {
        $builder_data['columns'] = [
            ['data' => 'DT_RowIndex', 'name' => 'DT_RowIndex', 'title' => trans('general.sr_no'), 'render' => null, 'orderable' => false, 'searchable' => false],
            ['data' => 'ety_EngagementTypeName', 'name' => 'ety_EngagementTypeName', 'title' => trans('general.gn_name')],
            ['data' => 'ety_Notes', 'name' => 'ety_Notes', 'title' => trans('general.gn_notes')],
            ['data' => 'action', 'name' => 'action', 'title' => trans('general.gn_actions'), 'orderable' => false, 'searchable' => false, 'width' => '10%'],
        ];

        $builder_data['ajax'] = [
            'url'=> route('engagementtypes.list'),
            'data' => 'function(d) {
                d.search =  $("#search_et").val();
            }'
        ];

        $dt_html = new DataTableService($builder_data, $builder);
        $dt_html = $dt_html->builder;

        return view('admin.engagementTypes.engagementTypes', compact('dt_html'));
    }

    public function store(EngagementTypeRequest $request)
    {
        $response = [];
        $column = 'ety_EngagementTypeName_'.$this->current_language;
        $value = $request[$column];
        if (!empty($request->pkEty)) {
            $checkPrev = EngagementType::where($column,$value)
                    ->where('pkEty', '!=', $request->pkEty)->count();
            if ($checkPrev) {
                $status_code = HttpResponse::HTTP_BAD_REQUEST;
                $response['message'] = trans('message.msg_et_exist');
            } else {
                EngagementType::where('pkEty', $request->pkEty)->update($request->validated());
                $response['message'] = trans('message.msg_et_update_success');
            }
        } else {
            $checkPrev = EngagementType::where($column,$value)
                    ->count();
            if ($checkPrev) {
                $status_code = HttpResponse::HTTP_BAD_REQUEST;
                $response['message'] = trans('message.msg_et_exist');
            } else {
                $id = EngagementType::insertGetId($request->validated());
                if (!empty($id)) {

                    $response['message'] = trans('message.msg_et_add_success');

                } else {
                    $status_code = HttpResponse::HTTP_BAD_REQUEST;
                    $response['message'] = trans('message.msg_something_wrong');
                }
            }
        }

        return response()->json($response,$status_code ?? HttpResponse::HTTP_OK);
    }

    public function edit($cid)
    {
        $response = [];
        $cdata = EngagementType::where('pkEty', '=', $cid)->first();

        if (empty($cid) || empty($cdata)) {
            $status_code = HttpResponse::HTTP_BAD_REQUEST;
        } else {

            $response['data'] = $cdata;
        }

        return response()->json($response,$status_code ?? HttpResponse::HTTP_OK);
    }

    public function destroy($cid)
    {
        $response = [];

        if (empty($cid)) {
            $status_code = HttpResponse::HTTP_BAD_REQUEST;
            $response['message'] = trans('message.msg_something_wrong');
        } else {
            $empCount = EmployeesEngagement::where('fkEenEty', $cid)->whereNull('een_DateOfFinishEngagement')->count();

            if ($empCount > 0) {
                $status_code = HttpResponse::HTTP_BAD_REQUEST;
                $response['message'] = trans('message.msg_et_delete_prompt');
            } else {
                EngagementType::where('pkEty', $cid)->delete();

                $response['message'] = trans('message.msg_et_delete_success');
            }
        }

        return response()->json($response,$status_code ?? HttpResponse::HTTP_OK);
    }

    /**
     * Get Engagement Type Listing
     *
     * @param \Illuminate\Http\Request $request
     * @return JSON
     */
    public function engagementtypes_list()
    {
        $engagement = EngagementType::select('*', 'ety_EngagementTypeName_' . $this->current_language . ' as ety_EngagementTypeName');

        $engagement->when(request('search'),function($q){
            return $q->where('ety_EngagementTypeName_' . $this->current_language, 'LIKE', '%' . request('search') . '%');
        });

        return DataTables::of($engagement)
            ->addColumn('action', function ($engagement) {
                $str = '<a cid="'.$engagement->pkEty.'" onclick="triggerEdit('.$engagement->pkEty.')" href="javascript:void(0)"><i class="fa fa-pen"></i></a>&nbsp;&nbsp';
                $str .= '<a onclick="triggerDelete('.$engagement->pkEty.')" href="javascript:void(0)"><i class="fa fa-trash"></i></a>';
                return $str;
            })
            ->addIndexColumn()
            ->escapeColumns()
            ->toJSON();
    }

}
