<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Admin;
use App\Models\Canton;
use App\Models\State;
use App\Http\Requests\StateRequest;
use DataTableService;
use Datatables;
use HtmlBuilder;
use HttpResponse;

class StateController extends Controller
{
    public function index(HtmlBuilder $builder)
    {
        $builder_data['columns'] = [
            ['data' => 'DT_RowIndex', 'name' => 'DT_RowIndex', 'title' => trans('general.sr_no'), 'render' => null, 'orderable' => false, 'searchable' => false],
            ['data' => 'sta_Uid', 'name' => 'sta_Uid', 'title' => trans('general.gn_uid')],
            ['data' => 'sta_StateName', 'name' => 'sta_StateName', 'title' => trans('general.gn_name')],
            ['data' => 'cny_CountryName', 'name' => 'cny_CountryName', 'title' => trans('general.gn_country'), 'orderable' => false],
            ['data' => 'sta_Note', 'name' => 'sta_Note', 'title' => trans('general.gn_notes')],
            ['data' => 'sta_Status', 'name' => 'sta_Status', 'title' => trans('general.gn_status')],
            ['data' => 'action', 'name' => 'action', 'title' => trans('general.gn_actions'), 'orderable' => false, 'searchable' => false, 'width' => '10%'],
        ];

        $builder_data['ajax'] = [
            'url'=> route('state.list'),
            'data' => 'function(d) {
                d.search =  $("#search_state").val();
                d.country_filter = $("#country_filter").val();
            }'
        ];

        $dt_html = new DataTableService($builder_data, $builder);
        $dt_html = $dt_html->builder;

        return view('admin.states.states', compact('dt_html'));
    }

    public function store(StateRequest $request)
    {
        $response = [];
        $column = 'sta_StateName_'.$this->current_language;
        $value = $request[$column];
        if (!empty($request->pkSta)) {
            $checkPrev = State::where($column, $value)
            ->where('pkSta', '!=', $request->pkSta)
            ->count();
            if ($checkPrev) {
                $status_code = HttpResponse::HTTP_BAD_REQUEST;
                $response['message'] = trans('message.msg_state_exist');
            } else {
                State::where('pkSta', $request->pkSta)->update($request->validated());
                $response['message'] = trans('message.msg_state_update_success');
            }
        } else {
            $checkPrev = State::where($column, $value)->count();
            if ($checkPrev) {
                $status_code = HttpResponse::HTTP_BAD_REQUEST;
                $response['message'] = trans('message.msg_state_exist');
            } else {
                $id = State::insertGetId($request->validated());
                if (!empty($id)) {
                    State::where('pkSta', $id)->update(['sta_Uid' => "STA" . $id]);
                    $response['message'] = trans('message.msg_state_add_success');
                } else {
                    $status_code = HttpResponse::HTTP_BAD_REQUEST;
                    $response['message'] = trans('message.msg_something_wrong');
                }
            }
        }

        return response()->json($response,$status_code ?? HttpResponse::HTTP_OK);
    }

    public function edit($id)
    {
        $response = [];
        $cdata = State::where('pkSta', '=', $id)->first();

        if (empty($id) || empty($cdata)) {
            $status_code = HttpResponse::HTTP_BAD_REQUEST;
        } else {
            $response['data'] = $cdata;
        }

        return response()->json($response,$status_code ?? HttpResponse::HTTP_OK);

    }

    public function destroy($cid)
    {
        $response = [];
        if (empty($cid)) {
            $status_code = HttpResponse::HTTP_BAD_REQUEST;
        } else {
            $Canton = Canton::where('fkCanSta', $cid)->count();
            if ($Canton != 0) {
                $status_code = HttpResponse::HTTP_BAD_REQUEST;
                $response['message'] = trans('message.msg_state_delete_prompt');
            } else {
                State::where('pkSta', $cid)->delete();
                $response['message'] = trans('message.msg_state_delete_success');
            }
        }

        return response()->json($response,$status_code ?? HttpResponse::HTTP_OK);
    }

    /**
     * Fetch States List
     *
     * @return JSON
     */
    public function state_list()
    {
        $states = State::select('pkSta', 'sta_Uid', 'sta_StateName_' . $this->current_language . ' as sta_StateName', 'sta_Note', 'sta_Status', 'fkStaCny')
        ->whereHas('country',function($q){
            $q->when(request('search'),function($q){
                $q->where('cny_CountryName_' . $this->current_language , 'LIKE', '%' . request('search') . '%');
            });
        })
        ->with(array(
            'country' => function ($query){
                $query->select('pkCny', 'cny_CountryName_' . $this->current_language . ' as cny_CountryName');
            },
        ));

        $states->when(empty(array_first(request('order'))['column']), function($q){
            return $q->orderBy('pkSta','ASC');
        });

        $states->when(request('search'), function ($q){
            return $q->orWhere('sta_Uid', 'LIKE', '%' . request('search') . '%')
            ->orWhere('sta_Note','LIKE', '%'. request('search') .'%')
            ->orWhere('sta_StateName_' . $this->current_language, 'LIKE', '%' . request('search') . '%');
        });

        $states->when(request('country_filter'), function ($q){
            return $q->where('fkStaCny', '=', request('country_filter'));
        });

        return DataTables::of($states)
            ->editColumn('cny_CountryName', function($states){
                return $states->country->cny_CountryName;
            })
            ->editColumn('sta_Note', function($states){
                return substr($states->sta_Note,0,45);
            })
            ->editColumn('sta_Status', function($states){
                if ($states->sta_Status == 'Active') {
                    return trans('general.gn_active');
                } else {
                    return trans('general.gn_inactive');
                }
            })
            ->addColumn('action', function ($states) {
                $str = '<a cid="'.$states->pkSta.'" onclick="triggerEdit('.$states->pkSta.')" href="javascript:void(0)"><i class="fa fa-pen"></i></a>&nbsp;&nbsp';
                $str .= '<a onclick="triggerDelete('.$states->pkSta.')" href="javascript:void(0)"><i class="fa fa-trash"></i></a>';
                return $str;
            })
            ->addIndexColumn()
            ->escapeColumns()
            ->toJSON();
    }

}
