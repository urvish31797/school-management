<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Language;
use App\Models\Translation;
use App\Http\Requests\TranslationRequest;
use DataTableService;
use Datatables;
use HtmlBuilder;
use HttpResponse;
use Cache;

class TranslationController extends Controller
{
    public function index(HtmlBuilder $builder)
    {
        $builder_data['columns'] = [
            ['data' => 'DT_RowIndex', 'name' => 'DT_RowIndex', 'title' => trans('general.sr_no'), 'render' => null, 'orderable' => false, 'searchable' => false],
            ['data' => 'group', 'name' => 'group', 'title' => trans('general.gn_section')],
            ['data' => 'key', 'name' => 'key', 'title' => trans('general.gn_translation_key')],
            ['data' => 'text', 'name' => 'text', 'title' => trans('general.gn_name')],
            ['data' => 'action', 'name' => 'action', 'title' => trans('general.gn_actions'), 'orderable' => false, 'searchable' => false, 'width' => '10%'],
        ];

        $builder_data['ajax'] = [
            'url'=> route('translations.list'),
            'data' => 'function(d) {
                d.search =  $("#search_translation").val();
            }'
        ];

        $dt_html = new DataTableService($builder_data, $builder);
        $dt_html = $dt_html->builder;

        return view('admin.translations.translations',compact('dt_html'));
    }

    public function store(TranslationRequest $request)
    {
        $response = [];
        if (!empty($request->id)) {
            $checkPrevKey = Translation::where('key', $request->key)
            ->where('id', '!=', $request->id)->count();

            if ($checkPrevKey) {
                $status_code = HttpResponse::HTTP_BAD_REQUEST;
                $response['message'] = trans('message.msg_translation_exists');
            } else {
                foreach ($this->languages as $k => $v) {
                    $text[$v->language_key] = $request[$v->language_key];
                }

                Translation::where('id', $request->id)->update([
                    'group' => $request->group,
                    'key' => $request->key,
                    'text' => json_encode($text, JSON_UNESCAPED_UNICODE),
                ]);

                self::deleteLangGroupCache($request->group);
                $response['message'] = trans('message.msg_translation_update_success');            
            }
        } else {

            $checkPrevKey = Translation::where('key', $request->key)->count();

            if ($checkPrevKey) {
                $status_code = HttpResponse::HTTP_BAD_REQUEST;
                $response['message'] = trans('message.msg_translation_exists');
            } else {
                foreach ($this->languages as $k => $v) {
                    $text[$v->language_key] = $request[$v->language_key];
                }

                $id = Translation::create([
                    'group' => $request->group,
                    'key' => $request->key,
                    'text' => $text,
                ]);

                if (!empty($id)) {
                    $response['message'] = trans('message.msg_translation_add_success');
                } else {
                    $status_code = HttpResponse::HTTP_BAD_REQUEST;
                    $response['message'] = trans('message.msg_something_wrong');
                }
            }
        }

        return response()->json($response,$status_code ?? HttpResponse::HTTP_OK);
    }

    public function edit($id)
    {
        $response = [];
        $cdata = Translation::where('id', '=', $id)->first();

        if (empty($id) || empty($cdata)) {
            $status_code = HttpResponse::HTTP_BAD_REQUEST;
        } else {
            $data = ['id' => $id, 'group' => $cdata->group, 'key' => $cdata->key];
            foreach ($this->languages as $k => $v) {
                $data[$v->language_key] = $cdata->text[$v->language_key];
            }

            $response['data'] = $data;        
        }

        return response()->json($response,$status_code ?? HttpResponse::HTTP_OK);
    }

    public function destroy($id)
    {
        $response = [];
        if (empty($id)) {
            $status_code = HttpResponse::HTTP_BAD_REQUEST;
        } else {
            $response['message'] = trans('message.msg_translation_delete_success');
            $line = Translation::where('id', $id)->first();
            $group = $line ? $line->group : '';

            if (!empty($group) && !empty($line)) {
                self::deleteLangGroupCache($group);
                $line->delete();
            }

        }
        return response()->json($response,$status_code ?? HttpResponse::HTTP_OK);
    }

    public static function deleteLangGroupCache($config_group)
    {
        $languages = Language::get();
        foreach ($languages as $language) {
            if ($config_group) {
                Cache::forget(Translation::getCacheKey($config_group, $language->language_key));
            }
        }
    }

    /**
     * Get Translation List
     *
     * @param \Illuminate\Http\Request $request
     * @return JSON
     */
    public function translations_list()
    {
        $translation = Translation::select('*');

        $translation->when(request('search'), function ($q){
            return $q->where('key', 'LIKE', '%' . request('search') . '%')
            ->orWhere('group', 'LIKE', '%' . request('search') . '%')
            ->orWhere('text', 'LIKE', '%' . request('search') . '%');
        });

        $translation->when(empty(array_first(request('order'))['column']), function($q){
            return $q->orderBy('id','ASC');
        });

        return DataTables::of($translation)
            ->editColumn('text', function($translation){
                return $translation->text[$this->current_language];
            })
            ->addColumn('action', function ($translation) {
                $str = '<a cid="'.$translation->id.'" onclick="triggerEdit('.$translation->id.')" href="javascript:void(0)"><i class="fa fa-pen"></i></a>&nbsp;&nbsp';
                $str .= '<a onclick="triggerDelete('.$translation->id.')" href="javascript:void(0)"><i class="fa fa-trash"></i></a>';
                return $str;
            })
            ->addIndexColumn()
            ->escapeColumns()
            ->toJSON();
    }

}
