<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\College;
use App\Models\EmployeesEducationDetail;
use App\Models\University;
use App\Http\Traits\FileTrait;
use App\Http\Requests\UniversityRequest;
use DataTableService;
use Datatables;
use HtmlBuilder;
use HttpResponse;

class UniversityController extends Controller
{
    use FileTrait;
    public function index(HtmlBuilder $builder)
    {
        $builder_data['columns'] = [
            ['data' => 'DT_RowIndex', 'name' => 'DT_RowIndex', 'title' => trans('general.sr_no'), 'render' => null, 'orderable' => false, 'searchable' => false],
            ['data' => 'uni_Uid', 'name' => 'uni_Uid', 'title' => trans('general.gn_uid')],
            ['data' => 'uni_UniversityName', 'name' => 'uni_UniversityName', 'title' => trans('general.gn_name')],
            ['data' => 'cny_CountryName', 'name' => 'cny_CountryName', 'title' => trans('general.gn_country'), 'orderable' => false],
            ['data' => 'uni_YearStartedFounded', 'name' => 'uni_YearStartedFounded', 'title' => trans('general.gn_started_year')],
            ['data' => 'oty_OwnershipTypeName', 'name' => 'oty_OwnershipTypeName', 'title' => trans('general.gn_ownership_type'), 'orderable' => false],
            ['data' => 'uni_Notes', 'name' => 'uni_Notes', 'title' => trans('general.gn_notes')],
            ['data' => 'action', 'name' => 'action', 'title' => trans('general.gn_actions'), 'orderable' => false, 'searchable' => false, 'width' => '10%'],
        ];

        $builder_data['ajax'] = [
            'url'=> route('universities.list'),
            'data' => 'function(d) {
                d.search = $("#search_university").val();
                d.year = $("#year_filter").val();
                d.country = $("#country_filter").val();
                d.ownership = $("#ownership_filter").val();
            }'
        ];

        $dt_html = new DataTableService($builder_data, $builder);
        $dt_html = $dt_html->builder;

        return view('admin.universities.universities', compact('dt_html'));
    }

    public function store(UniversityRequest $request)
    {
        $response = [];
        $column = 'uni_UniversityName_'.$this->current_language;
        $value = $request[$column];
        if (!empty($request->pkUni)) {
            $university_id = $request->pkUni;
            $checkPrev = University::where($column,$value)
                    ->where('pkUni', '!=', $request->pkUni)
                    ->count();
            if ($checkPrev) {
                $status_code = HttpResponse::HTTP_BAD_REQUEST;
                $response['message'] = trans('message.msg_university_exist');
            } else {
                University::where('pkUni', $request->pkUni)->update($request->validated());
                $response['message'] = trans('message.msg_university_update_success');
            }
        } else {
            $checkPrev = University::where($column,$value)->count();
            if ($checkPrev) {
                $status_code = HttpResponse::HTTP_BAD_REQUEST;
                $response['message'] = trans('message.msg_university_exist');
            } else {
                $id = University::insertGetId($request->validated());
                if (!empty($id)) {
                    University::where('pkUni', $id)->update(['uni_Uid' => "UNI" . $id]);
                    $university_id = $id;
                    $response['message'] = trans('message.msg_university_add_success');
                } else {
                    $status_code = HttpResponse::HTTP_BAD_REQUEST;
                    $response['message'] = trans('message.msg_something_wrong');
                }
            }
        }

        if($request->file('upload_profile') && !empty($university_id)){

            $university = University::where('pkUni',$university_id)->first();
            if (!empty($university->uni_PicturePath)) {
                $unlink_file = $university->uni_PicturePath;
            }

            $upload_params = [
                'folder_path' => config('assetpath.universities_images'),
                'unlink_file' => $unlink_file ?? ''
            ];

            $image = $request->file('upload_profile');
            $filename = $this->uploadFiletoStorage($image,$upload_params);

            if(!empty($filename)){
                University::where('pkUni',$university_id)->update(['uni_PicturePath'=>$filename]);
            }
        }

        return response()->json($response,$status_code ?? HttpResponse::HTTP_OK);
    }

    public function edit($cid)
    {
        $response = [];
        $cdata = University::where('pkUni', '=', $cid)->first();
        if (empty($cid) || empty($cdata)) {
            $status_code = HttpResponse::HTTP_BAD_REQUEST;
        } else {

            if (!empty($cdata->uni_PicturePath)) {
                $cdata->uni_PicturePath = asset(config('assetpath.universities_images')) . '/' . $cdata->uni_PicturePath;
            }
            $response['data'] = $cdata;
        }

        return response()->json($response,$status_code ?? HttpResponse::HTTP_OK);
    }

    public function destroy($cid)
    {
        $response = [];

        if (empty($cid)) {
            $status_code = HttpResponse::HTTP_BAD_REQUEST;
        } else {
            $College = College::where('fkColUni', $cid)
                ->count();
            $eed = EmployeesEducationDetail::where('fkEedUni', $cid)
                ->count();

            if ($eed != 0 || $College != 0) {
                $status_code = HttpResponse::HTTP_BAD_REQUEST;
                $response['message'] = trans('message.msg_university_delete_prompt');
            } else {
                University::where('pkUni', $cid)->delete();

                $response['message'] = trans('message.msg_university_delete_success');
            }
        }

        return response()->json($response,$status_code ?? HttpResponse::HTTP_OK);
    }

    /**
     * Fetch Universities List
     *
     * @param \Illuminate\Http\Request $request
     * @return JSON
     */
    public function universities_list()
    {
        $university = University::select('*','uni_UniversityName_'.$this->current_language.' AS uni_UniversityName')
        ->with(['ownershipType' => function ($query) {
                $query->select('pkOty', 'oty_OwnershipTypeName_' . $this->current_language . ' as oty_OwnershipTypeName');
            }
            , 'country' => function ($query) {
                $query->select('pkCny', 'cny_CountryName_' . $this->current_language . ' as cny_CountryName');
            },
        ]);

        $university->when(request('search'), function ($q){
            return $q->where('uni_Uid', 'LIKE', '%' . request('search') . '%')
            ->orWhere('uni_Notes', 'LIKE', '%' . request('search') . '%')
            ->orWhere('uni_UniversityName_' . $this->current_language, 'LIKE', '%' . request('search') . '%');
        });

        $university->when(request('country'), function ($q){
            return $q->where('fkUniCny', '=', request('country'));
        });

        $university->when(request('ownership'), function ($q){
            return $q->where('fkUniOty', '=', request('ownership'));
        });

        $university->when(request('year'), function ($q){
            return $q->where('uni_YearStartedFounded', '=', request('year'));
        });

        return DataTables::of($university)
            ->editColumn('oty_OwnershipTypeName', function($university){
                return $university->ownershipType->oty_OwnershipTypeName;
            })
            ->editColumn('cny_CountryName', function($university){
                return $university->country->cny_CountryName;
            })
            ->editColumn('uni_Notes', function($university){
                return substr($university->uni_Notes,0,45);
            })
            ->addColumn('action', function ($university) {
                $str = '<a cid="'.$university->pkUni.'" onclick="triggerEdit('.$university->pkUni.')" href="javascript:void(0)"><i class="fa fa-pen"></i></a>&nbsp;&nbsp';
                $str .= '<a onclick="triggerDelete('.$university->pkUni.')" href="javascript:void(0)"><i class="fa fa-trash"></i></a>';
                return $str;
            })
            ->addIndexColumn()
            ->escapeColumns()
            ->toJSON();
    }
}
