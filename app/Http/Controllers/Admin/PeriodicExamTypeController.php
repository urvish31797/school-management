<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use App\Models\PeriodicExamType;
use App\Http\Requests\PeriodicExamTypeRequest;
use DataTableService;
use Datatables;
use HtmlBuilder;
use HttpResponse;

class PeriodicExamTypeController extends Controller
{
    public function index(HtmlBuilder $builder)
    {
        $builder_data['columns'] = [
            ['data' => 'DT_RowIndex', 'name' => 'DT_RowIndex', 'title' => trans('general.sr_no'), 'render' => null, 'orderable' => false, 'searchable' => false],
            ['data' => 'pet_Name', 'name' => 'pet_Name', 'title' => trans('general.gn_name')],
            ['data' => 'pet_Note', 'name' => 'pet_Note', 'title' => trans('general.gn_notes')],
            ['data' => 'action', 'name' => 'action', 'title' => trans('general.gn_actions'), 'orderable' => false, 'searchable' => false, 'width' => '10%'],
        ];

        $builder_data['ajax'] = [
            'url'=> route('periodic-exam-type.list'),
            'data' => 'function(d) {
                d.search =  $("#search_periodic_exam_type").val();
            }'
        ];

        $dt_html = new DataTableService($builder_data, $builder);
        $dt_html = $dt_html->builder;

        return view('admin.periodicExamType.periodicExamType',compact('dt_html'));
    }

    public function store(PeriodicExamTypeRequest $request)
    {
        $response = [];

        $column = 'pet_Name_'.$this->current_language;
        $value = $request[$column];

        if (!empty($request->pkPet)) {
            $checkPrev = PeriodicExamType::where($column,$value)
                    ->where('pkPet', '!=', $request->pkPet)
                    ->count();
            if ($checkPrev) {
                $status_code = HttpResponse::HTTP_BAD_REQUEST;
                $response['message'] = trans('message.msg_periodic_exam_type_exist');
            } else {
                PeriodicExamType::where('pkPet', $request->pkPet)->update($request->validated());
                $response['message'] = trans('message.msg_periodic_exam_type_update_success');
            }
        } else {
            $checkPrev = PeriodicExamType::where($column,$value)
                    ->count();
            if ($checkPrev) {
                $status_code = HttpResponse::HTTP_BAD_REQUEST;
                $response['message'] = trans('message.msg_periodic_exam_type_exist');
            } else {
                $id = PeriodicExamType::insert($request->validated());
                if (!empty($id)) {

                    $response['message'] = trans('message.msg_periodic_exam_type_add_success');
                } else {
                    $status_code = HttpResponse::HTTP_BAD_REQUEST;
                    $response['message'] = trans('message.msg_something_wrong');
                }
            }

        }

        return response()->json($response,$status_code ?? HttpResponse::HTTP_OK);
    }

    public function edit($cid)
    {
        $response = [];
        $cdata = PeriodicExamType::where('pkPet', '=', $cid)->first();

        if (empty($cid) || empty($cdata)) {
            $status_code = HttpResponse::HTTP_BAD_REQUEST;
        } else {

            $response['data'] = $cdata;
        }

        return response()->json($response,$status_code ?? HttpResponse::HTTP_OK);
    }

    public function destroy($cid)
    {
        $response = [];
        if (empty($cid)) {
            $status_code = HttpResponse::HTTP_BAD_REQUEST;
        } else {
            PeriodicExamType::where('pkPet', $cid)->delete();
            $response['message'] = trans('message.msg_periodic_exam_type_delete_success');
        }

        return response()->json($response,$status_code ?? HttpResponse::HTTP_OK);
    }

    public function periodic_exam_type_list()
    {
        $periodic_exam_type = PeriodicExamType::select('*', 'pet_Name_' . $this->current_language . ' as pet_Name');
        $periodic_exam_type->when(request('search'),function($q){
            return $q->where('pet_Note', 'LIKE', '%' . request('search') . '%')
            ->orWhere('pet_Name_' . $this->current_language, 'LIKE', '%' . request('search') . '%');
         });

        return DataTables::of($periodic_exam_type)
        ->addColumn('action', function ($periodic_exam_type) {
            $str = '<a onclick="triggerEdit(' . $periodic_exam_type->pkPet . ')" href="javascript:void(0)"><i class="fa fa-pen"></i></a>&nbsp;&nbsp';
            $str .= '<a onclick="triggerDelete('.$periodic_exam_type->pkPet.')" href="javascript:void(0)"><i class="fa fa-trash"></i></a>';
            return $str;
        })
        ->rawColumns(['action'])
        ->addIndexColumn()
        ->escapeColumns()
        ->toJSON();
    }
}
