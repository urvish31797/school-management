<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\GradeAttemptsNumber;
use App\Http\Requests\GradeAttemptsNumberRequest;
use DataTableService;
use Datatables;
use HtmlBuilder;
use HttpResponse;

class GradeAttemptsNumberController extends Controller
{
    public function index(HtmlBuilder $builder)
    {
        $builder_data['columns'] = [
            ['data' => 'DT_RowIndex', 'name' => 'DT_RowIndex', 'title' => trans('general.sr_no'), 'render' => null, 'orderable' => false, 'searchable' => false],
            ['data' => 'gan_Uid', 'name' => 'gan_Uid', 'title' => trans('general.gn_uid')],
            ['data' => 'gan_Name', 'name' => 'gan_Name', 'title' => trans('general.gn_name')],
            ['data' => 'action', 'name' => 'action', 'title' => trans('general.gn_actions'), 'orderable' => false, 'searchable' => false, 'width' => '10%'],
        ];

        $builder_data['ajax'] = [
            'url'=> route('gradeattemptsnumber.list'),
            'data' => 'function(d) {
                d.search =  $("#search_grade_attempts_number").val();
            }'
        ];

        $dt_html = new DataTableService($builder_data, $builder);
        $dt_html = $dt_html->builder;

        return view('admin.gradeAttemptsNumber.gradeAttemptsNumber', compact('dt_html'));
    }

    public function store(GradeAttemptsNumberRequest $request)
    {
        $column = 'gan_Name_'.$this->current_language;
        $value = $request[$column];

        $response = [];
        if (!empty($request->pkGan)) {
            $checkPrev = GradeAttemptsNumber::where($column,$value)
                    ->where('pkGan', '!=', $request->pkGan)->count();
            if ($checkPrev) {
                $status_code = HttpResponse::HTTP_BAD_REQUEST;
                $response['message'] = trans('message.msg_gan_exists');
            } else {
                GradeAttemptsNumber::where('pkGan', $request->pkGan)->update($request->validated());
                $response['message'] = trans('message.msg_gan_update_success');
            }
        } else {
            $checkPrev = GradeAttemptsNumber::where($column,$value)
                    ->count();
            if ($checkPrev) {
                $status_code = HttpResponse::HTTP_BAD_REQUEST;
                $response['message'] = trans('message.msg_gan_exists');
            } else {
                $id = GradeAttemptsNumber::insertGetId($request->validated());
                if (!empty($id)) {
                    GradeAttemptsNumber::where('pkGan', $id)->update(['gan_Uid' => "GAN" . $id]);
                    $response['message'] = trans('message.msg_gan_add_success');
                } else {
                    $status_code = HttpResponse::HTTP_BAD_REQUEST;
                    $response['message'] = trans('message.msg_something_wrong');
                }
            }
        }

        return response()->json($response,$status_code ?? HttpResponse::HTTP_OK);
    }

    public function edit($cid)
    {
        $response = [];
        $cdata = GradeAttemptsNumber::where('pkGan', '=', $cid)->first();

        if (empty($cid) || empty($cdata)) {
            $status_code = HttpResponse::HTTP_BAD_REQUEST;
        } else {

            $response['data'] = $cdata;
        }

        return response()->json($response,$status_code ?? HttpResponse::HTTP_OK);
    }

    public function destroy($cid)
    {
        $response = [];
        if (empty($cid)) {
            $status_code = HttpResponse::HTTP_BAD_REQUEST;
        } else {
            GradeAttemptsNumber::where('pkGan', $cid)->delete();

            $response['message'] = trans('message.msg_gradeattemptnumber_delete_success');
        }

        return response()->json($response,$status_code ?? HttpResponse::HTTP_OK);
    }

    /**
     * Get Grade Attempt Numbers List
     *
     * @param \Illuminate\Http\Request $request
     * @return JSON
     */
    public function gradeattemptsnumber_list()
    {
        $grade_attempts_number = GradeAttemptsNumber::select('*', 'gan_Name_' . $this->current_language . ' as gan_Name');

        $grade_attempts_number->when(request('search'),function($q){
            return $q->where('gan_Uid', 'LIKE', '%' . request('search') . '%')
            ->orWhere('gan_Name_' . $this->current_language, 'LIKE', '%' . request('search') . '%');
        });

        return DataTables::of($grade_attempts_number)
            ->addColumn('action', function ($grade_attempts_number) {
                $str = '<a cid="'.$grade_attempts_number->pkGan.'" onclick="triggerEdit('.$grade_attempts_number->pkGan.')" href="javascript:void(0)"><i class="fa fa-pen"></i></a>&nbsp;&nbsp';
                $str .= '<a onclick="triggerDelete('.$grade_attempts_number->pkGan.')" href="javascript:void(0)"><i class="fa fa-trash"></i></a>';
                return $str;
            })
            ->addIndexColumn()
            ->escapeColumns()
            ->toJSON();
    }

}
