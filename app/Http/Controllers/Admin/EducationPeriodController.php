<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\EducationPeriod;
use App\Http\Requests\EducationPeriodRequest;
use DataTableService;
use Datatables;
use HtmlBuilder;
use HttpResponse;

class EducationPeriodController extends Controller
{
    public function index(HtmlBuilder $builder)
    {
        $builder_data['columns'] = [
            ['data' => 'DT_RowIndex', 'name' => 'DT_RowIndex', 'title' => trans('general.sr_no'), 'render' => null, 'orderable' => false, 'searchable' => false],
            ['data' => 'edp_Uid', 'name' => 'edp_Uid', 'title' => trans('general.gn_uid')],
            ['data' => 'edp_EducationPeriodName', 'name' => 'edp_EducationPeriodName', 'title' => trans('general.gn_name')],
            ['data' => 'edp_EducationPeriodNameAdjective', 'name' => 'edp_EducationPeriodNameAdjective', 'title' => trans('general.gn_adjective_name')],
            ['data' => 'action', 'name' => 'action', 'title' => trans('general.gn_actions'), 'orderable' => false, 'searchable' => false, 'width' => '10%'],
        ];

        $builder_data['ajax'] = [
            'url'=> route('educationperiod.list'),
            'data' => 'function(d) {
                d.search =  $("#search_educationPeriod").val();
            }'
        ];

        $dt_html = new DataTableService($builder_data, $builder);
        $dt_html = $dt_html->builder;

        return view('admin.educationPeriod.educationPeriod',compact('dt_html'));
    }

    /**
     * save education period
     *
     * @param \Illuminate\Http\Request $request
     * @return JSON - $response
     */
    public function store(EducationPeriodRequest $request)
    {
        $column = 'edp_EducationPeriodName_'.$this->current_language;
        $value = $request[$column];

        $response = [];
        if (!empty($request->pkEdp)) {
            $checkPrev = EducationPeriod::where($column,$value)
                    ->where('pkEdp', '!=', $request->pkEdp)->count();
            if ($checkPrev) {
                $status_code = HttpResponse::HTTP_BAD_REQUEST;
                $response['message'] = trans('message.msg_education_period_exist');
            } else {
                EducationPeriod::where('pkEdp', $request->pkEdp)->update($request->validated());
                $response['message'] = trans('message.msg_education_period_update_success');
            }
        } else {
            $checkPrev = EducationPeriod::where($column,$value)
                    ->count();
            if ($checkPrev) {
                $status_code = HttpResponse::HTTP_BAD_REQUEST;
                $response['message'] = trans('message.msg_education_period_exist');
            } else {
                $id = EducationPeriod::insertGetId($request->validated());
                if (!empty($id)) {
                    EducationPeriod::where('pkEdp', $id)->update(['edp_Uid' => "EDP" . $id]);
                    $response['message'] = trans('message.msg_education_period_add_success');
                } else {
                    $status_code = HttpResponse::HTTP_BAD_REQUEST;
                    $response['message'] = trans('message.msg_something_wrong');
                }
            }
        }

        return response()->json($response,$status_code ?? HttpResponse::HTTP_OK);
    }

    /**
     * Fetch Education period
     *
     * @param \Illuminate\Http\Request $request
     * @return JSON - $response
     */
    public function edit($cid)
    {
        $response = [];
        $cdata = EducationPeriod::where('pkEdp', '=', $cid)->first();

        if (empty($cid) || empty($cdata)) {
            $status_code = HttpResponse::HTTP_BAD_REQUEST;
        } else {

            $response['data'] = $cdata;
        }

        return response()->json($response,$status_code ?? HttpResponse::HTTP_OK);
    }

    /**
     * Delete education period
     *
     * @param \Illuminate\Http\Request $request
     * @return JSON - $response
     */
    public function destroy($cid)
    {
        $response = [];
        if (empty($cid)) {
            $status_code = HttpResponse::HTTP_BAD_REQUEST;
        } else {
            $ClassCreation = 0;
            if ($ClassCreation != 0) {
                $status_code = HttpResponse::HTTP_BAD_REQUEST;
                $response['message'] = trans('message.msg_school_year_delete_prompt');
            } else {
                EducationPeriod::where('pkEdp', $cid)->delete();

                $response['message'] = trans('message.msg_school_year_delete_success');
            }
        }

        return response()->json($response,$status_code ?? HttpResponse::HTTP_OK);
    }

    /**
     * Fetch Educationperiod data for table view
     *
     * @return JSON
     */
    public function educationperiod_list()
    {
        $education_period = EducationPeriod::select('*', 'edp_EducationPeriodName_' . $this->current_language . ' as edp_EducationPeriodName');

        $education_period->when(request('search'),function($q){
            return $q->where('edp_Uid', 'LIKE', '%' . request('search') . '%')
            ->orWhere('edp_EducationPeriodName_' . $this->current_language, 'LIKE', '%' . request('search') . '%')
            ->orWhere('edp_EducationPeriodNameAdjective', 'LIKE', '%' . request('search') . '%');
        });

        return DataTables::of($education_period)
            ->addColumn('action', function ($education_period) {
                $str = '<a cid="'.$education_period->pkEdp.'" onclick="triggerEdit('.$education_period->pkEdp.')" href="javascript:void(0)"><i class="fa fa-pen"></i></a>&nbsp;&nbsp';
                $str .= '<a onclick="triggerDelete('.$education_period->pkEdp.')" href="javascript:void(0)"><i class="fa fa-trash"></i></a>';
                return $str;
            })
            ->addIndexColumn()
            ->escapeColumns()
            ->toJSON();

    }
}
