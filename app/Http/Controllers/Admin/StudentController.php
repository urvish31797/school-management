<?php
namespace App\Http\Controllers\Admin;

use App\Helpers\FrontHelper;
use App\Helpers\MailHelper;
use App\Helpers\UtilHelper;
use App\Http\Controllers\Controller;
use App\Models\Admin;
use App\Models\ClassStudentsAllocation;
use App\Models\Country;
use App\Models\EducationPlan;
use App\Models\EducationProgram;
use App\Models\Employee;
use App\Models\EmployeesEngagement;
use App\Models\EnrollStudent;
use App\Models\Grade;
use App\Models\JobAndWork;
use App\Models\MainBook;
use App\Models\Municipality;
use App\Models\Nationality;
use App\Models\PostalCode;
use App\Models\Religion;
use App\Models\School;
use App\Models\SchoolEducationPlanAssignment;
use App\Models\SchoolYear;
use App\Models\Student;
use App\Models\VillageSchool;
use Illuminate\Http\Request;

class StudentController extends Controller
{
    /**
     * Students Listing Page
     * @param  Request $request
     * @return view - employee.student.student
     */
    public function index(Request $request)
    {
        if (request()
            ->ajax()) {
            return \View::make('employee.student.student')->renderSections();
        }
        return view('employee.student.student');
    }

    /**
     * Edit Student Page
     * @param  Request $request
     * @param  Int  $id
     * @return view - employee.student.editStudent
     */
    public function edit(Request $request, $id)
    {
        $input = $request->all();
        $mainSchool = $this->logged_user->sid;
        $municipality = Municipality::with([
            'canton.state.country' => function ($q) {
                $q->select('pkCny', 'cny_CountryName_' . $this->current_language . ' as cny_CountryName');
            },
        ])->select('pkMun', 'fkMunCan', 'mun_MunicipalityName_' . $this->current_language . ' as mun_MunicipalityName')->get();

        $params = ['language'=>$this->current_language,'fkVscSch'=>$mainSchool];
        $village_schools = VillageSchool::GetVillageSchoolList($params)
        ->pluck('vsc_VillageSchoolName','pkVsc')
        ->prepend(trans('general.gn_select'), '');

        $nationality = Nationality::select('pkNat', 'nat_NationalityName_' . $this->current_language . ' as nat_NationalityName')
            ->get();
        $country = Country::select('pkCny', 'cny_CountryName_' . $this->current_language . ' as cny_CountryName')
            ->get();
        $religion = Religion::select('pkRel', 'rel_ReligionName_' . $this->current_language . ' as rel_ReligionName')
            ->get();

        $postalCode = PostalCode::with([
            'municipality' => function ($q) {
                $q->select('pkMun', 'fkMunCan', 'mun_MunicipalityName_' . $this->current_language . ' as mun_MunicipalityName');
            },
            'municipality.canton.state.country' => function ($q) {
                $q->select('pkCny', 'cny_CountryName_' . $this->current_language . ' as cny_CountryName');
            },
        ])->select('pkPof', 'fkPofMun', 'pof_PostOfficeNumber', 'pof_PostOfficeName_' . $this->current_language . ' as pof_PostOfficeName')
            ->get();

        $jawWork = JobAndWork::select('pkJaw', 'jaw_Name_' . $this->current_language . ' as jaw_Name')
            ->get();

        $mainBooks = MainBook::select('pkMbo', 'mbo_Uid', 'mbo_MainBookNameRoman')->get();
        $schoolYear = SchoolYear::select('pkSye', 'sye_NameNumeric', 'sye_NameCharacter_' . $this->current_language . ' AS sye_NameCharacter')->get();

        $enrollStudent = EnrollStudent::where('fkSteStu', $id)->orderBy('pkSte', 'DESC')
            ->whereNull('ste_EnrollmentFinishDate')
            ->first();
        $mainSchool = $enrollStudent->fkSteSch;

        $sclEduPlnAssinment = SchoolEducationPlanAssignment::where('fkSepSch', $mainSchool)->where('sep_Status', 'Active')
            ->get()
            ->toArray();
        $eduProg = array();
        $eduPlan = array();
        foreach ($sclEduPlnAssinment as $key => $value) {
            $eduProg[] = $value['fkSepEdp'];
            $eduPlan[] = $value['fkSepEpl'];
        }
        $educationProg = EducationProgram::select('pkEdp', 'edp_Name_' . $this->current_language . ' as edp_Name')
            ->whereIn('pkEdp', $eduProg)->get();

        //Get Education Education Program from  SchoolEducationPlanAssignment
        if (isset($input['fkSteEdp']) && $input['fkSteEdp']) {
            $sclEduPlnAssinmen = SchoolEducationPlanAssignment::where('fkSepSch', $mainSchool)->where('fkSepEdp', $input['fkSteEdp'])->where('sep_Status', 'Active')
                ->get()
                ->toArray();
            foreach ($sclEduPlnAssinmen as $key => $value) {
                $eduPlan1[] = $value['fkSepEpl'];
            }

            $educationPlans = EducationPlan::select('pkEpl', 'epl_EducationPlanName_' . $this->current_language . ' as epl_EducationPlanName')
                ->whereIn('pkEpl', $eduPlan1)->get()
                ->toArray();

            return response()
                ->json(['status' => true, 'educationPlans' => $educationPlans]);
        }

        \DB::statement("SET SQL_MODE=''");
        $aStudentData = Student::with(['enrollStudent' => function ($q) use ($mainSchool) {
            $q->where('fkSteSch', '!=', null)
                ->where('fkSteSch', '!=', 0);
            }
            , 'enrollStudent.school' => function ($q) use ($mainSchool) {
                $q->select('*', 'sch_SchoolName_' . $this->current_language . ' as sch_SchoolName');
            }
            , 'enrollStudent.villageschool' => function ($q){
                $q->select('pkVsc','vsc_VillageSchoolName_'.$this->current_language.' AS vsc_VillageSchoolName');
            }
            , 'enrollStudent.grade' => function ($q) {
                $q->select('pkGra', 'gra_GradeNameRoman', 'gra_GradeNumeric');
            }
            , 'enrollStudent.schoolYear' => function ($q) {
                $q->select('pkSye', 'sye_NameNumeric', 'sye_NameCharacter_' . $this->current_language . ' AS sye_NameCharacter');
            }
            , 'enrollStudent.educationProgram' => function ($q) {
                $q->select('pkEdp', 'edp_Name_' . $this->current_language . ' as edp_Name');
            }
            , 'enrollStudent.educationProgram.schoolEducationPlanAssignment' => function ($q) use ($mainSchool) {
                $q->where('fkSepSch', $mainSchool);
            }
            , 'enrollStudent.educationProgram.schoolEducationPlanAssignment.educationPlan' => function ($q) {
                $q->select('*', 'pkEpl', 'epl_EducationPlanName_' . $this->current_language . ' as epl_EducationPlanName');
            }
            , 'enrollStudent.educationProgram.schoolEducationPlanAssignment.educationPlan.groupedByGrades.grade' => function ($q) {
                $q->select('*', 'pkGra', 'gra_GradeNumeric');
            }
            , 'enrollStudent.educationPlan' => function ($q) {
                $q->select('pkEpl', 'epl_EducationPlanName_' . $this->current_language . ' as epl_EducationPlanName');
            }
        ])->where('id', $id)->first();

        $allGrades = Grade::select('*', 'pkGra', 'gra_GradeNumeric')
            ->get();

        if (request()
            ->ajax()) {
            return \View::make('employee.student.editStudent')
                ->with(['aStudentData' => $aStudentData, 'village_schools' => $village_schools, 'jawWork' => $jawWork, 'postalCode' => $postalCode, 'municipality' => $municipality, 'nationality' => $nationality, 'country' => $country, 'religion' => $religion, 'mainBooks' => $mainBooks, 'schoolYear' => $schoolYear, 'educationProg' => $educationProg, 'allGrades' => $allGrades])->renderSections();
        }
        return view('employee.student.editStudent', ['aStudentData' => $aStudentData, 'village_schools' => $village_schools, 'jawWork' => $jawWork, 'postalCode' => $postalCode, 'municipality' => $municipality, 'nationality' => $nationality, 'country' => $country, 'religion' => $religion, 'mainBooks' => $mainBooks, 'schoolYear' => $schoolYear, 'educationProg' => $educationProg, 'allGrades' => $allGrades]);

    }

    /**
     * Update Student Data
     * @param  Request $request
     * @return JSON $response
     */
    public function store(Request $request)
    {
        $input = $request->all();
        $image = $request->file('stu_PicturePath');
        $response = [];

        if (!empty($image)) {
            $input['stu_PicturePath'] = time() . '.' . $image->getClientOriginalExtension();
            $destinationPath = storage_path('app/public/images/students');
            $image->move($destinationPath, $input['stu_PicturePath']);
            $imgData = Student::select('stu_PicturePath')->where('id', $input['id'])->first();
            if (!empty($imgData->stu_PicturePath)) {
                $filepath = storage_path('app/public/images/students') . "/" . $imgData->stu_PicturePath;
                if (file_exists($filepath)) {
                    unlink($filepath);
                }
            }
        }

        $govIdExistCount = 0;
        if (isset($input['stu_StudentID']) && !empty($input['stu_StudentID'])) {
            $govIdExistCount = Student::where('stu_StudentID', $input['stu_StudentID'])
                ->where('id', '!=', $input['id'])
                ->count();
        }

        $tempIdExistCount = 0;
        if (isset($input['stu_TempCitizenId']) && !empty($input['stu_TempCitizenId'])) {
            $tempIdExistCount = Student::where('stu_TempCitizenId', '=', $input['stu_TempCitizenId'])
                ->where('stu_TempCitizenId', '!=', null)
                ->where('id', '!=', $input['id'])
                ->count();
        }

        $emailExistCount = 0;
        $emailAdmExistCount = 0;
        $emailEmpExistCount = 0;

        if (!empty($input['email'])) {
            $emailExistCount = Student::where('email', '=', $input['email'])->where('id', '!=', $input['id'])->count();
            $emailAdmExistCount = Admin::where('email', '=', $input['email'])->count();
            $emailEmpExistCount = Employee::where('email', '=', $input['email'])->count();
        }

        if ($emailExistCount != 0 || $emailAdmExistCount != 0 || $emailEmpExistCount != 0) {
            $response['status'] = false;
            $response['message'] = trans('message.msg_email_exist');
            return response()
                ->json($response);
        }

        if ($govIdExistCount != 0) {
            $response['status'] = false;
            $response['message'] = trans('message.msg_student_id_exist');
            return response()
                ->json($response);
        }

        if ($tempIdExistCount != 0) {
            $response['status'] = false;
            $response['message'] = trans('message.msg_temp_citizen_id_exist');
            return response()
                ->json($response);
        }

        if (!empty($input['id'])) {
            $input['stu_DateOfBirth'] = UtilHelper::sqlDate($input['stu_DateOfBirth']);

            $sdata = Student::where('id', $input['id'])->first();

            if ($sdata->email != $input['email']) {
                $current_time = date("Y-m-d H:i:s");
                $verification_key = md5(FrontHelper::generatePassword(20));
                $reset_pass_token = base64_encode($input['email'] . '&&Student&&' . $current_time);

                $data = ['email' => $input['email'], 'name' => $input['stu_StudentName'] . " " . $input['stu_StudentSurname'], 'verify_key' => $verification_key, 'reset_pass_link' => $reset_pass_token, 'subject' => 'New Student Credentials'];

                MailHelper::sendNewCredentials($data);
                $input['email_verified_at'] = null;
                $input['email_verification_key'] = $verification_key;
            } else {
                $verification_key = md5(FrontHelper::generatePassword(20));
                $input['email_verification_key'] = $verification_key;
                $input['email_verified_at'] = null;
            }

            if (!empty($input['stu_TempCitizenId'])) {
                $input['stu_StudentID'] = '';
            }

            if (!empty($input['stu_StudentID'])) {
                $input['stu_TempCitizenId'] = '';
            }
            $id = Student::where('id', $input['id'])->update($input);

        }

        if ($id) {
            $response['status'] = true;
            $response['message'] = trans('message.msg_student_update_success');
            $response['redirect'] = url('admin/students');
        } else {
            $response['status'] = false;
            $response['message'] = trans('message.msg_something_wrong');
        }
        return response()
            ->json($response);
    }

    /**
     * Delete Student
     * @param  Request $request
     * @return JSON $response
     */
    public function destroy(Request $request, $cid)
    {
        $response = [];

        if (empty($cid)) {
            $response['status'] = false;
        } else {

            $eed = Student::where('id', $cid)->delete();

            if ($eed) {
                $response['status'] = true;
                $response['message'] = trans('message.msg_student_delete_success');
            } else {
                $response['status'] = false;
                $response['message'] = trans('message.msg_student_delete_prompt');
            }
        }

        return response()
            ->json($response);
    }

    /**
     * View Student Page
     * @param  Request $request
     * @param  int  $id
     * @return view - employee.student.viewStudent
     */
    public function show(Request $request, $id)
    {

        $enrollStudent = EnrollStudent::where('fkSteStu', $id)->orderBy('pkSte', 'DESC')
            ->whereNull('ste_EnrollmentFinishDate')
            ->first();
        $mainSchool = $enrollStudent->fkSteSch;

        $aStudentData = Student::with(['country' => function ($q) {
                $q->select('pkCny', 'cny_CountryName_' . $this->current_language . ' as cny_CountryName');
            }, 'postalCode' => function ($q) {
                $q->select('pkPof', 'fkPofMun', 'pof_PostOfficeNumber', 'pof_PostOfficeName_' . $this->current_language . ' as pof_PostOfficeName');
            }
            , 'municipality' => function ($q) {
                $q->select('pkMun', 'mun_MunicipalityName_' . $this->current_language . ' as mun_MunicipalityName');
            }
            , 'nationality' => function ($q) {
                $q->select('pkNat', 'nat_NationalityName_' . $this->current_language . ' as nat_NationalityName');
            }
            , 'riligeion' => function ($q) {
                $q->select('pkRel', 'rel_ReligionName_' . $this->current_language . ' as rel_ReligionName');
            }
            , 'enrollStudent' => function ($q) use ($mainSchool) {
                $q->where('fkSteSch', '!=', null)
                    ->where('fkSteSch', '!=', 0);
            }
            , 'enrollStudent.grade' => function ($q) {
                $q->select('pkGra', 'gra_GradeNameRoman', 'gra_GradeNumeric');
            }
            , 'enrollStudent.schoolYear' => function ($q) {
                $q->select('pkSye', 'sye_NameNumeric', 'sye_NameCharacter_' . $this->current_language . ' AS sye_NameCharacter');
            }
            , 'enrollStudent.educationProgram' => function ($q) {
                $q->select('pkEdp', 'edp_Name_' . $this->current_language . ' as edp_Name');
            }
            , 'enrollStudent.educationPlan' => function ($q) {
                $q->select('pkEpl', 'epl_EducationPlanName_' . $this->current_language . ' as epl_EducationPlanName');
            }
            , 'enrollStudent.school' => function ($q){
                $q->select('pkSch','sch_SchoolName_'.$this->current_language.' AS sch_SchoolName');
            }
            , 'enrollStudent.villageschool' => function ($q){
                $q->select('pkVsc','vsc_VillageSchoolName_'.$this->current_language.' AS vsc_VillageSchoolName');
            }
        ])->where('id', $id)->first();

        return view('employee.student.viewStudent', ['mdata' => $aStudentData]);
    }

    /**
     * Edit Student
     * @param  Request $request
     * @return JSON $response
     */
    public function editStudentEnroll(Request $request)
    {
        $input = $request->all();
        $response = [];

        $tdata = $input['total_details'];
        $pkSte = explode(',', $input['pkSte']);
        $oldEng = explode(',', $input['oldEng']);
        $mainSchool = EmployeesEngagement::select('fkEenSch')->where('fkEenEmp', $this
                ->logged_user
                ->id)
                ->first();
        $mainSchool = $mainSchool->fkEenSch;

        $classStudents = ClassStudentsAllocation::whereIn('fkCsaSen', $oldEng)->get()
            ->count();

        if ($classStudents != 0) {
            $response['status'] = false;
            $response['message'] = trans('message.msg_enroll_delete_fail');
            return response()
                ->json($response);
        } else {
            EnrollStudent::whereIn('pkSte', $oldEng)->delete();
        }

        for ($i = 1; $i <= $tdata; $i++) {
            $details = [];
            $finishDate = null;
            $expellDate = null;
            $breakDate = null;
            if (!empty($input['ste_FinishingDate_' . $i])) {
                $finishDate = UtilHelper::sqlDate($input['ste_FinishingDate_' . $i]);
            }
            if (!empty($input['ste_ExpellingDate' . $i])) {
                $expellDate = UtilHelper::sqlDate($input['ste_ExpellingDate' . $i]);
            }
            if (!empty($input['ste_BreakingDate' . $i])) {
                $breakDate = UtilHelper::sqlDate($input['ste_BreakingDate' . $i]);
            }
            $details = ['fkSteStu' => $input['fkSteStu'],
                        'fkSteSch' => $mainSchool,
                        'ste_EnrollmentDate' => UtilHelper::sqlDate($input['start_date_' . $i]),
                        'fkSteMbo' => $input['fkSteMbo_' . $i],
                        'fkSteGra' => $input['fkSteGra_' . $i],
                        'fkSteEdp' => $input['fkSteEdp_' . $i],
                        'fkSteEpl' => $input['fkSteEpl_' . $i],
                        'fkSteSye' => $input['fkSteSye_' . $i],
                        'fkSteSye' => $input['fkSteSye_' . $i],
                        'ste_EnrollBasedOn' => $input['ste_EnrollBasedOn_' . $i],
                        'ste_MainBookOrderNumber' => $input['ste_MainBook_' . $i],
                        'ste_Reason' => $input['ste_Reason_' . $i],
                        'ste_FinishingDate' => $finishDate,
                        'ste_BreakingDate' => $breakDate,
                        'ste_ExpellingDate' => $expellDate];

            if ($pkSte[$i - 1] == 'new') {
                EnrollStudent::insert($details);
            } else {
                EnrollStudent::where('pkSte', $pkSte[$i - 1])->update($details);
            }
        }

        $response['status'] = true;
        $response['message'] = trans('message.msg_enroll_update_success');

        return response()
            ->json($response);
    }

    /**
     * Fetch Students Listing data
     * @param  Request $request
     * @return JSON $result
     */
    public function students_list(Request $request)
    {
        $data = $request->all();

        $perpage = !empty($data['length']) ? (int) $data['length'] : 10;

        $filter = isset($data['search']) && is_string($data['search']) ? $data['search'] : '';

        $sort_type = isset($data['order'][0]['dir']) && is_string($data['order'][0]['dir']) ? $data['order'][0]['dir'] : '';
        $sort_col = isset($data['order'][0]['column']) ? $data['order'][0]['column'] : '';

        $sort_field = isset($data['columns'][$sort_col]['data']) ? $data['columns'][$sort_col]['data'] : '';

        $schools = '';
        if ($this->logged_user->type == "MinistryAdmin") {
            $cantonId = $this->logged_user->fkAdmCan;
            $schools = School::whereHas('postalCode.municipality.canton', function ($q) use ($cantonId) {
                $q->where('pkCan', $cantonId);
            })->get()->pluck('pkSch')->all();
        }

        $student = Student::whereHas('enrollStudent', function ($q) use ($schools) {
            $q->whereNull('ste_EnrollmentFinishDate')
                ->where(function ($q) use ($schools) {
                    if ($this->logged_user->type == "MinistryAdmin") {
                        $q->whereIn('fkSteSch', $schools);
                    }
                });
        });

        if ($filter) {
            $student = $student->where(function ($q) use ($filter) {

                $explodestring = explode(" ", $filter);
                $firstname = $explodestring[0] ?? $filter;
                $lastname = $explodestring[1] ?? $filter;

                $q->where('stu_StudentID', 'LIKE', '%' . $filter . '%')
                    ->orWhere('stu_StudentName', 'LIKE', '%' . $firstname . '%')
                    ->orWhere('stu_StudentSurname', 'LIKE', $lastname . '%')
                    ->orWhere('stu_StudentGender', 'LIKE', '%' . $filter . '%');
            });
        }

        $studentQuery = $student;

        if ($sort_col != 0) {
            $studentQuery = $studentQuery->orderBy($sort_field, $sort_type);
        }

        $total_students = $studentQuery->count();
        $offset = $data['start'];
        $counter = $offset;
        $studentdata = [];
        $students = $studentQuery->offset($offset)->limit($perpage)->get()
            ->toArray();

        foreach ($students as $key => $value) {
            $value['index'] = $counter + 1;
            if ($value['stu_StudentGender'] == 'Male') {
                $value['stu_StudentGender'] = trans('general.gn_male');
            } else {
                $value['stu_StudentGender'] = trans('general.gn_female');
            }
            $studentdata[$counter] = $value;
            $counter++;
        }

        $price = array_column($studentdata, 'index');
        if ($sort_col == 0) {
            if ($sort_type == 'desc') {
                array_multisort($price, SORT_DESC, $studentdata);
            } else {
                array_multisort($price, SORT_ASC, $studentdata);
            }
        }
        $studentdata = array_values($studentdata);
        $result = array(
            "draw" => $data['draw'],
            "recordsTotal" => $total_students,
            "recordsFiltered" => $total_students,
            "data" => $studentdata,
        );

        return response()->json($result);
    }

    /**
     * Used for checking student Govt or temp Id
     * @param  Request $request
     * @return JSON $response
     */
    public function checkStuId(Request $request)
    {
        $response = [];
        $input = $request->all();

        if (!empty($input['id'])) {
            $student = Student::where(function ($query) use ($input) {
                $query->where('stu_StudentID', $input['stuId'])->orWhere('stu_TempCitizenId', $input['stuId']);
            })->where('id', '!=', $input['id'])->count();
        } else {
            $student = Student::where(function ($query) use ($input) {
                $query->where('stu_StudentID', $input['stuId'])->orWhere('stu_TempCitizenId', $input['stuId']);
            })->count();
        }

        if ($student != 0) {
            $response['status'] = false;
            $response['message'] = trans('message.msg_student_id_exist');
        } else {
            $response['status'] = true;
        }

        return response()->json($response);
    }
}
