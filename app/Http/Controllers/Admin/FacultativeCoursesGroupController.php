<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\FacultativeCoursesGroup;
use App\Http\Requests\FacultativeCoursesGroupRequest;
use HttpResponse;
use DataTableService;
use Datatables;
use HtmlBuilder;

class FacultativeCoursesGroupController extends Controller
{
    public function index(HtmlBuilder $builder)
    {
        $builder_data['columns'] = [
            ['data' => 'DT_RowIndex', 'name' => 'DT_RowIndex', 'title' => trans('general.sr_no'), 'render' => null, 'orderable' => false, 'searchable' => false],
            ['data' => 'fcg_Uid', 'name' => 'fcg_Uid', 'title' => trans('general.gn_uid')],
            ['data' => 'fcg_Name', 'name' => 'fcg_Name', 'title' => trans('general.gn_name')],
            ['data' => 'fcg_Notes', 'name' => 'fcg_Notes', 'title' => trans('general.gn_notes')],
            ['data' => 'action', 'name' => 'action', 'title' => trans('general.gn_actions'), 'orderable' => false, 'searchable' => false, 'width' => '10%'],
        ];

        $builder_data['ajax'] = [
            'url'=> route('facultativecoursesgroup.list'),
            'data' => 'function(d) {
                d.search =  $("#search_facultativeCoursesGroup").val();
            }'
        ];

        $dt_html = new DataTableService($builder_data, $builder);
        $dt_html = $dt_html->builder;

    return view('admin.facultativeCoursesGroup.facultativeCoursesGroup',compact('dt_html'));
    }

    public function store(FacultativeCoursesGroupRequest $request)
    {
        $column = 'fcg_Name_'.$this->current_language;
        $value = $request[$column];

        $response = [];
        if (!empty($request->pkFcg)) {
            $checkPrev = FacultativeCoursesGroup::where($column,$value)
                    ->where('pkFcg', '!=', $request->pkFcg)->count();
            if ($checkPrev) {
                $status_code = HttpResponse::HTTP_BAD_REQUEST;
                $response['message'] = trans('message.msg_fcg_exist');
            } else {
                FacultativeCoursesGroup::where('pkFcg', $request->pkFcg)->update($request->validated());
                
                $response['message'] = trans('message.msg_fcg_update_success');
            }
        } else {
            $checkPrev = FacultativeCoursesGroup::where($column,$value)
                    ->count();
            if ($checkPrev) {
                $status_code = HttpResponse::HTTP_BAD_REQUEST;
                $response['message'] = trans('message.msg_fcg_exist');
            } else {
                $id = FacultativeCoursesGroup::insertGetId($request->validated());
                if (!empty($id)) {
                    FacultativeCoursesGroup::where('pkFcg', $id)->update(['fcg_Uid' => "FCG" . $id]);
                    
                    $response['message'] = trans('message.msg_fcg_add_success');

                } else {
                    $status_code = HttpResponse::HTTP_BAD_REQUEST;
                    $response['message'] = trans('message.msg_something_wrong');
                }
            }
        }

        return response()->json($response,$status_code ?? HttpResponse::HTTP_OK);
    }

    public function edit($cid)
    {
        $response = [];
        $cdata = FacultativeCoursesGroup::where('pkFcg', '=', $cid)->first();

        if (empty($cid) || empty($cdata)) {
            $status_code = HttpResponse::HTTP_BAD_REQUEST;
        } else {
            
            $response['data'] = $cdata;
        }

        return response()->json($response,$status_code ?? HttpResponse::HTTP_OK);
    }

    public function destroy($cid)
    {
        $response = [];

        if (empty($cid)) {
            $status_code = HttpResponse::HTTP_BAD_REQUEST;
        } else {
            FacultativeCoursesGroup::where('pkFcg', $cid)->delete();
            
            $response['message'] = trans('message.msg_fcg_delete_success');
        }

        return response()->json($response,$status_code ?? HttpResponse::HTTP_OK);
    }

    /**
     * Fetch Facultative Course List
     *
     * @param \Illuminate\Http\Request $request
     * @return JSON
     */
    public function facultativecoursesgroup_list()
    {
        $facultative_groups = FacultativeCoursesGroup::select('*', 'fcg_Name_' . $this->current_language . ' as fcg_Name');
        $facultative_groups->when(request('search'), function ($q){
            return $q->where('fcg_Uid', 'LIKE', '%' . request('search') . '%')
            ->orWhere('fcg_Name_' . $this->current_language, 'LIKE', '%' . request('search') . '%')
            ->orWhere('fcg_Notes', 'LIKE', '%' . request('search') . '%');
        });

        return DataTables::of($facultative_groups)
            ->editColumn('fcg_Notes', function($facultative_groups){
                return substr($facultative_groups->fcg_Notes,0,45);
            })
            ->addColumn('action', function ($facultative_groups) {
                $str = '<a cid="'.$facultative_groups->pkFcg.'" onclick="triggerEdit('.$facultative_groups->pkFcg.')" href="javascript:void(0)"><i class="fa fa-pen"></i></a>&nbsp;&nbsp';
                $str .= '<a onclick="triggerDelete('.$facultative_groups->pkFcg.')" href="javascript:void(0)"><i class="fa fa-trash"></i></a>';
                return $str;
            })
            ->addIndexColumn()
            ->escapeColumns()
            ->toJSON();
    }

}
