<?php
namespace App\Http\Controllers\Admin;

use App\Helpers\FrontHelper;
use App\Helpers\MailHelper;
use App\Helpers\UtilHelper;
use App\Http\Controllers\Controller;
use App\Http\Traits\CommonTrait;
use App\Models\Admin;
use App\Models\Citizenship;
use App\Models\Country;
use App\Models\EducationPlan;
use App\Models\EducationProgram;
use App\Models\Employee;
use App\Models\EmployeesEngagement;
use App\Models\EmployeesWeekHourRates;
use App\Models\EmployeeType;
use App\Models\EngagementType;
use App\Models\EnrollStudent;
use App\Models\MainBook;
use App\Models\Nationality;
use App\Models\PostalCode;
use App\Models\Religion;
use App\Models\School;
use App\Models\SchoolEducationPlanAssignment;
use App\Models\VillageSchool;
use Illuminate\Http\Request;

class SchoolController extends Controller
{
    use CommonTrait;

    public function index(Request $request)
    {
        return view('admin.schools.schools');
    }

    public function create(Request $request)
    {
        $cantonId = $this->logged_user->fkAdmCan;
        $postOffice = PostalCode::whereHas('municipality.canton', function ($q) use ($cantonId) {
            $q->where('pkCan', $cantonId);
        })->get()->toArray();

        $Countries = Country::select('pkCny', 'cny_CountryName_' . $this->current_language . ' as cny_CountryName')
            ->get();
        $Municipalities = $this->getMuncipalityDropdownData($this->current_language);
        $PostalCodes = $this->getPostalCodeDropdownData($this->current_language);
        $Citizenships = Citizenship::select('pkCtz', 'ctz_CitizenshipName_' . $this->current_language . ' as ctz_CitizenshipName')
            ->get();
        $Nationalities = Nationality::select('pkNat', 'nat_NationalityName_' . $this->current_language . ' as nat_NationalityName')
            ->get();
        $Religions = Religion::select('pkRel', 'rel_ReligionName_' . $this->current_language . ' as rel_ReligionName')
            ->get();
        $streams = EducationProgram::select('pkEdp', 'edp_ParentId', 'edp_Name_' . $this->current_language . ' as edp_Name')
            ->get()
            ->toArray();
        $educationProgram = FrontHelper::buildtree($streams);

        if (request()
            ->ajax()) {
            return \View::make('admin.schools.addSchool')
                ->with([
                    'educationProgram' => $educationProgram,
                    'cantonId' => $this->logged_user->fkAdmCan,
                    'postOffice' => $postOffice,
                    'Countries' => $Countries,
                    'Municipalities' => $Municipalities,
                    'PostalCodes' => $PostalCodes,
                    'Citizenships' => $Citizenships,
                    'Nationalities' => $Nationalities,
                    'Religions' => $Religions
                    ])
                    ->renderSections();
        }

        return view('admin.schools.addSchool', [
        'cantonId' => $this->logged_user->fkAdmCan,
        'educationProgram' => $educationProgram,
        'postOffice' => $postOffice,
        'Countries' => $Countries,
        'Municipalities' => $Municipalities,
        'PostalCodes' => $PostalCodes,
        'Citizenships' => $Citizenships,
        'Nationalities' => $Nationalities,
        'Religions' => $Religions]);
    }

    public function store(Request $request)
    {
        $input = $request->all();
        $response = [];
        $PSdata = [];

        if (isset($input['pkSch']) && !empty($input['pkSch'])) {
            $SP = explode(',', $input['SP']);
            $sc_id = $input['sc_id'];
            $start_date = $input['start_date'];
            $end_date = $input['end_date'];
            if (isset($input['sep_Status'])) {
                $SepStatus = $input['sep_Status'];
            } else {
                $SepStatus = [];
            }

            unset($input['SP']);
            unset($input['sep_Status']);
            unset($input['fkEplEdp']);
            unset($input['sc_id']);
            unset($input['start_date']);
            unset($input['end_date']);
            unset($input['eplan']);

            $input['sch_FoundingDate'] = UtilHelper::sqlDate($input['sch_FoundingDate']);

            $prevData = EmployeesEngagement::select('fkEenEmp')->where('fkEenSch', $input['pkSch'])->first();
            $prevEmpData = Employee::where('id', $prevData->fkEenEmp)
                ->first();
            $checkPrev = School::where('sch_SchoolName_' . $this->current_language, $input['sch_SchoolName_' . $this
                    ->current_language])
                    ->where('pkSch', '!=', $input['pkSch'])->first();

            School::where('pkSch', $input['pkSch'])->update($input);

            SchoolEducationPlanAssignment::where('fkSepSch', $input['pkSch'])->forceDelete();

            foreach ($SP as $k => $v) {
                $EProgram = EducationPlan::select('fkEplEdp')->where("pkEpl", '=', $v)->first();
                $sp_status = 'Inactive';
                if (in_array($v, $SepStatus)) {
                    $sp_status = 'Active';
                }
                $PSdata[] = ['fkSepSch' => $input['pkSch'], 'fkSepEpl' => $v, 'fkSepEdp' => $EProgram->fkEplEdp, 'sep_Status' => $sp_status];
            }

            SchoolEducationPlanAssignment::insert(array_reverse($PSdata));
            if (!empty($sc_id)) {

                $existSC = EmployeesEngagement::where('fkEenSch',$input['pkSch'])
                    ->where('fkEenEpty',2)
                    ->whereNull('een_DateOfFinishEngagement')
                    ->first();

                if (empty($end_date)) {
                    if($existSC->fkEenEmp != $sc_id)
                    {
                        $ee = Employee::find($sc_id);
                        $CoordinatorEmail = $ee->email;
                        $CoordinatorName = $ee->emp_EmployeeName . " " . $ee->emp_EmployeeSurname;
                        $verification_key = md5(FrontHelper::generatePassword(20));
                        $current_time = date("Y-m-d H:i:s");
                        $existEmployees = "Yes";

                        $school = School::where('pkSch', $input['pkSch'])->first();
                        $schoolName = $school->sch_SchoolName_en;

                        //create new enagement
                        $empEngagement = new EmployeesEngagement;
                        $empEngagement->fkEenSch = $input['pkSch'];
                        $empEngagement->fkEenEmp = $sc_id;
                        $empEngagement->fkEenEty = 1;
                        $empEngagement->fkEenEpty = 2;
                        $empEngagement->een_DateOfEngagement = UtilHelper::sqlDate($start_date);
                        $empEngagement->save();

                        $reset_pass_token = base64_encode($CoordinatorEmail . '&&SchoolCoordinator&&' . $current_time);
                        $data = ['email' => $CoordinatorEmail, 'name' => $CoordinatorName, 'verify_key' => $verification_key, 'reset_pass_link' => $reset_pass_token, 'subject' => 'New School Coordinator Credentials', 'school' => $schoolName, 'existEmployee' => $existEmployees];

                        MailHelper::schoolCoordinatorCredentials($data);

                        //old sc will be inactive from existing school
                        $existSC->een_DateOfFinishEngagement = UtilHelper::sqlDate($start_date);
                        $existSC->save();

                        //check if no role is active then we need to inactive that employee.
                        $active_roles = Employee::whereHas('EmployeesEngagement',function($q) use($existSC){
                            $q->CheckExistActiveRole($existSC->fkEenEmp);
                        })
                        ->count();

                        if(empty($active_roles)){
                            Employee::where('id',$existSC->fkEenEmp)->update(['emp_Status'=>'Inactive']);
                        }
                    }
                }
                else
                {
                    if(!empty($existSC))
                    {
                        $existSC->een_DateOfFinishEngagement = UtilHelper::sqlDate($end_date);
                        $existSC->save();
                    }
                }
            }

            $response['status'] = true;
            $response['message'] = trans('message.msg_school_update_success');

        } else {
            $SP = explode(',', $input['SP']);
            $emp_EmployeeName = $input['emp_EmployeeName'];
            $emp_EmployeeSurname = $input['emp_EmployeeSurname'];
            $existEmployee = $input['sel_exists_employee'];
            $start_date = $input['start_date'];
            $emp_PhoneNumber = $input['emp_PhoneNumber'];
            $emp_EmployeeID = $input['emp_EmployeeID'];
            $emp_TempCitizenId = $input['emp_TempCitizenId'];
            $emp_DateOfBirth = $input['emp_DateOfBirth'];
            $emp_EmployeeGender = $input['emp_EmployeeGender'];
            $fkEmpCny = $input['fkEmpCny'];
            $fkEmpMun = $input['fkEmpMun'];
            $fkEmpNat = $input['fkEmpNat'];
            $fkEmpRel = $input['fkEmpRel'];
            $fkEmpCtz = $input['fkEmpCtz'];
            $fkEmpPof = $input['fkEmpPof'];
            $een_notes = $input['een_notes'];
            $ewh_WeeklyHoursRate = $input['ewh_WeeklyHoursRate'];
            $ewh_Notes = $input['ewh_Notes'];
            $scId = $input['sc_id'];

            $input['sch_FoundingDate'] = UtilHelper::sqlDate($input['sch_FoundingDate']);

            $CoordinatorEmail = $input['sch_CoordEmail'];
            if (isset($input['sep_Status'])) {
                $SepStatus = $input['sep_Status'];
            } else {
                $SepStatus = [];
            }

            unset($input['SP']);
            unset($input['pkSch']);
            unset($input['emp_EmployeeName']);
            unset($input['emp_EmployeeSurname']);
            unset($input['sch_CoordEmail']);
            unset($input['sep_Status']);
            unset($input['fkEplEdp']);
            unset($input['eplan']);
            unset($input['sel_exists_employee']);
            unset($input['start_date']);
            unset($input['emp_PhoneNumber']);
            unset($input['emp_EmployeeID']);
            unset($input['emp_DateOfBirth']);
            unset($input['emp_TempCitizenId']);
            unset($input['emp_EmployeeGender']);
            unset($input['fkEmpCny']);
            unset($input['fkEmpMun']);
            unset($input['fkEmpNat']);
            unset($input['fkEmpRel']);
            unset($input['fkEmpCtz']);
            unset($input['fkEmpPof']);
            unset($input['een_notes']);
            unset($input['ewh_WeeklyHoursRate']);
            unset($input['ewh_Notes']);
            unset($input['sc_id']);

            $checkPrev = School::where('sch_SchoolName_' . $this->current_language, $input['sch_SchoolName_' . $this
                    ->current_language])
                    ->first();

            if ($existEmployee == "No") {
                $govIdExistCount = 0;
                if (!empty($emp_EmployeeID)) {
                    $govIdExistCount = Employee::where('emp_EmployeeID', '=', $emp_EmployeeID)->count();
                }

                $tempIdExistCount = 0;
                if (!empty($emp_TempCitizenId)) {
                    $tempIdExistCount = Employee::where('emp_TempCitizenId', '=', $emp_TempCitizenId)->where('emp_TempCitizenId', '!=', null)
                        ->count();
                }

                if ($govIdExistCount != 0) {
                    $response['status'] = false;
                    $response['message'] = trans('message.msg_employee_id_exist');
                    return response()
                        ->json($response);
                }

                if ($tempIdExistCount != 0) {
                    $response['status'] = false;
                    $response['message'] = trans('message.msg_temp_citizen_id_exist');
                    return response()
                        ->json($response);
                }
            }

            $checkPrevEmpEmail = Employee::where('email', $CoordinatorEmail)->first();
            $checkPrevEmpEmailAdmin = Admin::where('email', $CoordinatorEmail)->first();
            if (!empty($checkPrev)) {
                $response['status'] = false;
                $response['message'] = trans("message.msg_school_exist");
            } elseif (!empty($checkPrevEmpEmail) || !empty($checkPrevEmpEmailAdmin)) {
                $response['status'] = false;
                $response['message'] = trans("message.msg_email_already_registered");
            } else {
                $id = School::insertGetId($input);
                if (!empty($id)) {
                    School::where('pkSch', $id)->update(['sch_Uid' => "SCH" . $id]);

                    foreach ($SP as $k => $v) {
                        $EProgram = EducationPlan::select('fkEplEdp')->where("pkEpl", '=', $v)->first();
                        $sp_status = 'Inactive';
                        if (in_array($v, $SepStatus)) {
                            $sp_status = 'Active';
                        }
                        $PSdata[] = ['fkSepSch' => $id, 'fkSepEpl' => $v, 'fkSepEdp' => $EProgram->fkEplEdp, 'sep_Status' => $sp_status];
                    }

                    SchoolEducationPlanAssignment::insert($PSdata);
                    $verification_key = md5(FrontHelper::generatePassword(20));
                    if ($existEmployee == 'No') {
                        $user = new Employee;
                        $user->email = $CoordinatorEmail;
                        $user->emp_EmployeeName = $emp_EmployeeName;
                        $user->emp_EmployeeSurname = $emp_EmployeeSurname;
                        $user->emp_PhoneNumber = $emp_PhoneNumber;
                        $user->emp_EmployeeID = $emp_EmployeeID;
                        $user->emp_TempCitizenId = $emp_TempCitizenId;
                        $user->emp_DateOfBirth = UtilHelper::sqlDate($emp_DateOfBirth);
                        $user->emp_EmployeeGender = $emp_EmployeeGender;
                        $user->fkEmpCny = $fkEmpCny;
                        $user->fkEmpMun = $fkEmpMun;
                        $user->fkEmpNat = $fkEmpNat;
                        $user->fkEmpRel = $fkEmpRel;
                        $user->fkEmpCtz = $fkEmpCtz;
                        $user->fkEmpPof = $fkEmpPof;
                        $user->email_verification_key = $verification_key;
                        $user->save();

                        $CoordinatorName = $emp_EmployeeName . " " . $emp_EmployeeSurname;
                    } else {
                        $user = Employee::find($scId);

                        $CoordinatorName = $user->emp_EmployeeName . " " . $user->emp_EmployeeSurname;
                        $CoordinatorEmail = $user->email;
                    }

                    $empType = EmployeeType::select('pkEpty')->where('epty_Name', '=', 'SchoolCoordinator')
                        ->where('epty_ParentId', '=', null)
                        ->first();
                    $engType = EngagementType::select('pkEty')->where('ety_EngagementTypeName_en', '=', 'Full Time')
                        ->first(); //Full time
                    $current_time = date("Y-m-d H:i:s");

                    if ($existEmployee == 'No') {
                        $ee = new EmployeesEngagement;
                        $ee->fkEenSch = $id;
                        $ee->fkEenEmp = $user->id;
                        $ee->fkEenEty = $engType->pkEty; //Full time
                        $ee->fkEenEpty = $empType->pkEpty;
                        $ee->een_notes = $een_notes;
                        $ee->een_DateOfEngagement = UtilHelper::sqlDate($start_date);
                        $ee->save();
                    } else {
                        $ee = new EmployeesEngagement;
                        $user = Employee::find($scId);

                        $ee->fkEenSch = $id;
                        $ee->fkEenEmp = $scId;
                        $ee->fkEenEty = $engType->pkEty; //Full time
                        $ee->fkEenEpty = $empType->pkEpty;
                        $ee->een_DateOfEngagement = UtilHelper::sqlDate($start_date);
                        $ee->save();
                    }

                    if ($ee->save()) {
                        EmployeesWeekHourRates::addHourlyRate($ee->pkEen, $ewh_WeeklyHoursRate, $ewh_Notes, UtilHelper::sqlDate($start_date));
                    }

                    $reset_pass_token = base64_encode($CoordinatorEmail . '&&SchoolCoordinator&&' . $current_time);
                    $data = ['email' => $CoordinatorEmail, 'name' => $CoordinatorName, 'verify_key' => $verification_key, 'reset_pass_link' => $reset_pass_token, 'subject' => 'New School Coordinator Credentials', 'school' => $input['sch_SchoolName_en'], 'existEmployee' => $existEmployee];

                    MailHelper::schoolCoordinatorCredentials($data);

                    $response['status'] = true;
                    $response['message'] = trans('message.msg_school_add_success');

                } else {
                    $response['status'] = false;
                    $response['message'] = trans('message.msg_something_wrong');
                }
            }
        }

        return response()
            ->json($response);
    }

    public function fetchEducationPlan(Request $request)
    {
        $cantonId = $this->logged_user->fkAdmCan;
        $input = $request->all();
        $response = [];
        $EPparent = '-';
        $mdata = EducationPlan::with(['nationalEducationPlan' => function ($query) {
            $query->select('pkNep', 'nep_NationalEducationPlanName_' . $this->current_language . ' as nep_NationalEducationPlanName');
        }
            , 'QualificationDegree' => function ($query) {
                $query->select('pkQde', 'qde_QualificationDegreeName_' . $this->current_language . ' as qde_QualificationDegreeName');
            }
            , 'educationProfile' => function ($query) {
                $query->select('pkEpr', 'epr_EducationProfileName_' . $this->current_language . ' as epr_EducationProfileName');
            }
        ])
            ->addSelect('*', 'epl_EducationPlanName_' . $this->current_language . ' as epl_EducationPlanName');
        $mdata = $mdata->where('fkEplEdp', '=', $input['pid'])
            ->where(function ($query) use ($cantonId) {
                if ($this->logged_user->type == "MinistryAdmin") {
                    $query->where('fkEplCan', $cantonId);
                }
            })->get();

        $EPmain = EducationProgram::select('edp_ParentId', 'pkEdp', 'edp_Name_' . $this->current_language . ' as edp_Name')
            ->where('pkEdp', '=', $input['pid'])->first();

        if (!empty($EPmain->edp_ParentId)) {
            $EPparent = EducationProgram::select('edp_ParentId', 'pkEdp', 'edp_Name_' . $this->current_language . ' as edp_Name')
                ->where('pkEdp', '=', $EPmain->edp_ParentId)
                ->first();
            $EPparent = $EPparent->edp_Name;
        }

        if (!empty($mdata)) {
            $response['status'] = true;
            $response['data'] = $mdata;
            $response['EPmain'] = $EPmain->edp_Name;
            $response['EPparent'] = $EPparent;
        } else {
            $response['status'] = false;
        }

        return response()->json($response);
    }

    public function educationPlanDeleteCheck(Request $request)
    {
        /*
         * Used for Checking student enrollment before removing education plan
         */
        $input = $request->all();
        $response = [];

        $mdata = EnrollStudent::where('fkSteSch', $input['eid'])->where('fkSteEpl', $input['cid'])->get()
            ->count();

        if ($mdata != 0) {
            $response['status'] = false;
            $response['message'] = trans('message.msg_school_plan_delete_prompt');
        } else {
            $response['status'] = true;
        }
        return response()->json($response);

    }

    public function edit($id)
    {
        if ($this->logged_user->type == "MinistryAdmin") {
            $cantonId = $this->logged_user->fkAdmCan;
        } else {
            $cantonId = School::GetSchoolCanton($id);
        }

        $postOffice = PostalCode::with(['municipality', 'municipality.canton'])
            ->whereHas('municipality.canton', function ($q) use ($cantonId) {
                if ($this->logged_user->type == "MinistryAdmin") {
                    $q->where('pkCan', $cantonId);
                }
            })->get()
            ->toArray();

        $streams = EducationProgram::select('pkEdp', 'edp_ParentId', 'edp_Name_' . $this->current_language . ' as edp_Name')
            ->get()
            ->toArray();
        $educationProgram = FrontHelper::buildtree($streams);

        if (!empty($id)) {
            $mdata = School::whereHas('employeesEngagement',function($q) use($id){
                    $q->whereNull('een_DateOfFinishEngagement')
                    ->where('fkEenEpty',2)
                    ->where('fkEenSch',$id)
                    ->orderBy('pkEen','DESC');
                })
                ->with([
                'employeesEngagement'=>function($q) use($id){
                    $q->whereNull('een_DateOfFinishEngagement')
                    ->where('fkEenEpty',2)
                    ->where('fkEenSch',$id)
                    ->orderBy('pkEen','DESC');
                },
                'employeesEngagement.employee',
                'schoolEducationPlanAssignment.educationPlan.educationProfile',
                'schoolEducationPlanAssignment.educationPlan.QualificationDegree',
                'schoolEducationPlanAssignment.educationPlan.nationalEducationPlan',
                'schoolEducationPlanAssignment.educationProgram.parent',
                'schoolEducationPlanAssignment.educationPlan' => function ($q) use ($id) {
                    $q->select('*', 'epl_EducationPlanName_' . $this->current_language . ' as epl_EducationPlanName');
                },
                'schoolEducationPlanAssignment.educationProgram' => function ($q) use ($id) {
                    $q->select('*', 'edp_Name_' . $this->current_language . ' as edp_Name');
                }]);

            $mdata = $mdata->where('pkSch', '=', $id)->first();
        }

        if (request()
            ->ajax()) {
            return \View::make('admin.schools.editSchool')
                ->with(['cantonId' => $cantonId, 'educationProgram' => $educationProgram, 'mdata' => $mdata, 'school_id' => $id, 'postOffice' => $postOffice])->renderSections();
        }
        return view('admin.schools.editSchool', ['cantonId' => $cantonId, 'educationProgram' => $educationProgram, 'mdata' => $mdata, 'school_id' => $id, 'postOffice' => $postOffice]);

    }

    /**
     * Get School List
     *
     * @param \Illuminate\Http\Request $request
     * @return JSON
     */
    public function schools_list(Request $request)
    {
        $data = $request->all();

        $perpage = !empty($data['length']) ? (int) $data['length'] : 10;

        $filter = isset($data['search']) && is_string($data['search']) ? $data['search'] : '';

        $sort_type = isset($data['order'][0]['dir']) && is_string($data['order'][0]['dir']) ? $data['order'][0]['dir'] : '';

        $sort_col = $data['order'][0]['column'];

        $sort_field = $data['columns'][$sort_col]['data'];

        $postOffice = '';
        if ($this->logged_user->type == "MinistryAdmin") {
            $cantonId = $this->logged_user->fkAdmCan;
            $postOffice = PostalCode::whereHas('municipality.canton', function ($q) use ($cantonId) {
                $q->where('pkCan', $cantonId);
            })->get()->pluck('pkPof')->all();
        }

        $empType = EmployeeType::select('pkEpty')->where('epty_Name', 'SchoolCoordinator')
            ->first();
        $pkEpty = $empType->pkEpty;

        $School = School::with(['employeesEngagement' => function ($q) use ($pkEpty) {
            $q->where('een_DateOfFinishEngagement', null)
                ->where('fkEenEpty', $pkEpty);
        }
            , 'employeesEngagement.employee'])
            ->whereHas('employeesEngagement', function ($q) use ($pkEpty, $filter) {
                $q->whereNull('een_DateOfFinishEngagement')
                    ->where('fkEenEpty', $pkEpty);
                // ->whereHas('employee', function ($q1) use ($filter)
                // {
                //     if ($filter)
                //     {
                //         $q1->where(function($q) use($filter){
                //             $q->where('emp_EmployeeName', 'LIKE', $filter . '%')
                //             ->orWhere('email', 'LIKE', $filter . '%');
                //         });
                //     }
                // });
            });

        if ($filter) {
            $School = $School->where(function ($q) use ($filter) {
                $q->orWhere('sch_Uid', 'LIKE', '%' . $filter . '%')
                    ->orWhere('sch_SchoolName_' . $this->current_language, 'LIKE', '%' . $filter . '%')
                    ->orWhere('sch_MinistryApprovalCertificate', 'LIKE', '%' . $filter . '%');
            });
        }

        if ($this->logged_user->type == "MinistryAdmin") {
            $School = $School->where(function ($query) use ($postOffice) {
                $query->whereIn('fkSchPof', $postOffice);
            });
        }

        $SchoolQuery = $School;

        if ($sort_col != 0) {
            $SchoolQuery = $SchoolQuery->orderBy($sort_field, $sort_type);
        }

        $total_School = $SchoolQuery->count();

        $offset = $data['start'];

        $counter = $offset;
        $SchoolData = [];

        $Schools = $SchoolQuery->select('*', 'sch_SchoolName_' . $this->current_language . ' as sch_SchoolName')
            ->offset($offset)->limit($perpage)->get()
            ->toArray();

        foreach ($Schools as $key => $value) {
            $value['index'] = $counter + 1;
            $SchoolData[$counter] = $value;
            $counter++;
        }

        $price = array_column($SchoolData, 'index');

        if ($sort_col == 0) {
            if ($sort_type == 'desc') {
                array_multisort($price, SORT_DESC, $SchoolData);
            } else {
                array_multisort($price, SORT_ASC, $SchoolData);
            }
        }
        $SchoolData = array_values($SchoolData);
        $result = array(
            "draw" => $data['draw'],
            "recordsTotal" => $total_School,
            "recordsFiltered" => $total_School,
            "data" => $SchoolData,
        );

        return response()->json($result);
    }

    public function show($id)
    {
        $cantonId = $this->logged_user->fkAdmCan;
        $mdata = '';
        if (!empty($id)) {
            $mdata = School::whereHas('employeesEngagement',function($q) use($id){
                    $q->whereNull('een_DateOfFinishEngagement')
                    ->where('fkEenEpty',2)
                    ->where('fkEenSch',$id)
                    ->orderBy('pkEen','DESC');
                })
                ->with([
                'employeesEngagement'=>function($q) use($id){
                    $q->whereNull('een_DateOfFinishEngagement')
                    ->where('fkEenEpty',2)
                    ->where('fkEenSch',$id)
                    ->orderBy('pkEen','DESC');
                },
                'employeesEngagement.employee',
                'schoolEducationPlanAssignment.educationPlan',
                'schoolEducationPlanAssignment.educationProgram.parent',
                'schoolEducationPlanAssignment.educationPlan' => function ($q) use ($id) {
                    $q->select('*', 'epl_EducationPlanName_' . $this->current_language . ' as epl_EducationPlanName');
                },
                'schoolEducationPlanAssignment.educationProgram' => function ($q) use ($id) {
                    $q->select('*', 'edp_Name_' . $this->current_language . ' as edp_Name');
                },
                'educationperiod' => function ($q) {
                    $q->select('pkEdp', 'edp_EducationPeriodName_' . $this->current_language . ' AS edp_EducationPeriodName');
                }
                , 'postalCode' => function ($q) {
                    $q->select('pkPof', 'pof_PostOfficeName_' . $this->current_language . ' as pof_PostOfficeName', 'pof_PostOfficeNumber');
                }
                , 'postalCode.municipality', 'postalCode.municipality.canton' => function ($q) use ($cantonId) {
                    $q->where('pkCan', $cantonId);
                },
                ]);

            $mdata = $mdata->where('pkSch', '=', $id)->first();

        }
        if (request()
            ->ajax()) {
            return \View::make('admin.schools.viewSchool')
                ->with('mdata', $mdata)->renderSections();
        }
        return view('admin.schools.viewSchool', ['mdata' => $mdata]);

    }

    public function destroy(Request $request, $cid)
    {
        $response = [];

        if (empty($cid)) {
            $response['status'] = false;
        } else {
            $Employee = Employee::where('fkEmpRel', $cid)
                ->count();
            $EmployeesEngagement = EmployeesEngagement::where('fkEenSch', $cid)
                ->count();
            $VillageSchool = VillageSchool::where('fkVscSch', $cid)
                ->count();
            $MainBook = MainBook::where('fkMboSch', $cid)
                ->count();

            if ($Employee != 0 || $MainBook != 0 || $EmployeesEngagement != 0 || $VillageSchool != 0) {
                $response['status'] = false;
                $response['message'] = trans('message.msg_school_delete_prompt');
            } else {
                School::where('pkSch', $cid)->delete();
                $response['status'] = true;
                $response['message'] = trans('message.msg_school_delete_success');
            }
        }

        return response()
            ->json($response);
    }

}
