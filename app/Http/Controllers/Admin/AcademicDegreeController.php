<?php
namespace App\Http\Controllers\Admin;

use App\Http\Requests\AcademicDegreeRequest;
use App\Http\Controllers\Controller;
use App\Models\AcademicDegree;
use App\Models\EmployeesEducationDetail;
use DataTableService;
use Datatables;
use HtmlBuilder;
use HttpResponse;

class AcademicDegreeController extends Controller
{
    public function index(HtmlBuilder $builder)
    {
        $builder_data['columns'] = [
            ['data' => 'DT_RowIndex', 'name' => 'DT_RowIndex', 'title' => trans('general.sr_no'), 'render' => null, 'orderable' => false, 'searchable' => false],
            ['data' => 'acd_Uid', 'name' => 'acd_Uid', 'title' => trans('general.gn_uid')],
            ['data' => 'acd_AcademicDegreeName', 'name' => 'acd_AcademicDegreeName', 'title' => trans('general.gn_name')],
            ['data' => 'acd_Notes', 'name' => 'acd_Notes', 'title' => trans('general.gn_notes')],
            ['data' => 'action', 'name' => 'action', 'title' => trans('general.gn_actions'), 'orderable' => false, 'searchable' => false, 'width' => '10%'],
        ];

        $builder_data['ajax'] = [
            'url'=> route('academicdegree.list'),
            'data' => 'function(d) {
                d.search =  $("#search_academic_degree").val();
            }'
        ];

        $dt_html = new DataTableService($builder_data, $builder);
        $dt_html = $dt_html->builder;

        return view('admin.academicDegrees.academicDegrees',compact('dt_html'));
    }

    public function store(AcademicDegreeRequest $request)
    {
        $response = [];
        $column = 'acd_AcademicDegreeName_'.$this->current_language;
        $value = $request[$column];

        if (!empty($request->pkAcd)) {
            $checkPrev = AcademicDegree::where($column, $value)
                    ->where('pkAcd', '!=', $request->pkAcd)
                    ->count();
            if ($checkPrev) {
                $status_code = HttpResponse::HTTP_BAD_REQUEST;
                $response['message'] = trans('message.msg_academic_degree_exist');
            } else {
                AcademicDegree::where('pkAcd', $request->pkAcd)->update($request->validated());
                $response['message'] = trans('message.msg_academic_degree_update_success');
            }
        } else {
            $checkPrev = AcademicDegree::where($column, $value)->count();
            if ($checkPrev) {
                $status_code = HttpResponse::HTTP_BAD_REQUEST;
                $response['message'] = trans('message.msg_academic_degree_exist');
            } else {
                $id = AcademicDegree::insertGetId($request->validated());
                if (!empty($id)) {
                    AcademicDegree::where('pkAcd', $id)->update(['acd_Uid' => "EDU" . $id]);
                    $response['message'] = trans('message.msg_academic_degree_add_success');
                } else {
                    $status_code = HttpResponse::HTTP_BAD_REQUEST;
                    $response['message'] = trans('message.msg_something_wrong');
                }
            }
        }

        return response()->json($response,$status_code ?? HttpResponse::HTTP_OK);
    }

    public function edit($id)
    {
        $response = [];
        $cdata = AcademicDegree::where('pkAcd', '=', $id)->first();

        if (empty($id) || empty($cdata)) {
            $status_code = HttpResponse::HTTP_BAD_REQUEST;
        } else {
            $response['data'] = $cdata;
        }

        return response()->json($response,$status_code ?? HttpResponse::HTTP_OK);

    }

    public function destroy($id)
    {
        $response = [];
        if (empty($id)) {
            $status_code = HttpResponse::HTTP_BAD_REQUEST;
        } else {
            $eed = EmployeesEducationDetail::where('fkEedAcd', $id)
                ->count();
            if ($eed != 0) {
                $status_code = HttpResponse::HTTP_BAD_REQUEST;
                $response['message'] = trans('message.msg_academic_degree_delete_prompt');
            } else {
                AcademicDegree::where('pkAcd', $id)->delete();
                $response['message'] = trans('message.msg_academic_degree_delete_success');
            }
        }

        return response()->json($response,$status_code ?? HttpResponse::HTTP_OK);
    }

    public function academicdegree_list()
    {
        $academic = AcademicDegree::select('pkAcd', 'acd_Uid', 'acd_Notes','acd_AcademicDegreeName_' . $this->current_language. ' AS acd_AcademicDegreeName');

        $academic->when(request('search'), function ($q){
            return $q->where('acd_Uid', 'LIKE', '%' . request('search') . '%')
            ->orWhere('acd_AcademicDegreeName_' . $this->current_language, 'LIKE', '%' . request('search') . '%')
            ->orWhere('acd_Notes', 'LIKE', '%' . request('search') . '%');
        });

        $academic->when(empty(array_first(request('order'))['column']), function($q){
            return $q->orderBy('pkAcd','ASC');
        });

        return DataTables::of($academic)
            ->editColumn('acd_Notes', function($academic){
                return substr($academic->acd_Notes,0,45);
            })
            ->addColumn('action', function ($academic) {
                $str = '<a cid="'.$academic->pkAcd.'" onclick="triggerEdit('.$academic->pkAcd.')" href="javascript:void(0)"><i class="fa fa-pen"></i></a>&nbsp;&nbsp';
                $str .= '<a onclick="triggerDelete('.$academic->pkAcd.')" href="javascript:void(0)"><i class="fa fa-trash"></i></a>';
                return $str;
            })
            ->addIndexColumn()
            ->escapeColumns()
            ->toJSON();
    }
}
