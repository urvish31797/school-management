<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Employee;
use App\Models\Municipality;
use App\Models\PostalCode;
use App\Http\Requests\MunicipalityRequest;
use DataTableService;
use Datatables;
use HtmlBuilder;
use HttpResponse;

class MunicipalityController extends Controller
{
    public function index(HtmlBuilder $builder)
    {
        $builder_data['columns'] = [
            ['data' => 'DT_RowIndex', 'name' => 'DT_RowIndex', 'title' => trans('general.sr_no'), 'render' => null, 'orderable' => false, 'searchable' => false],
            ['data' => 'mun_Uid', 'name' => 'mun_Uid', 'title' => trans('general.gn_uid')],
            ['data' => 'mun_MunicipalityName', 'name' => 'mun_MunicipalityName', 'title' => trans('general.gn_name')],
            ['data' => 'can_CantonName', 'name' => 'can_CantonName', 'title' => trans('general.gn_canton'), 'orderable' => false],
            ['data' => 'mun_Notes', 'name' => 'mun_Notes', 'title' => trans('general.gn_notes')],
            ['data' => 'action', 'name' => 'action', 'title' => trans('general.gn_actions'), 'orderable' => false, 'searchable' => false, 'width' => '10%'],
        ];

        $builder_data['ajax'] = [
            'url'=> route('municipalities.list'),
            'data' => 'function(d) {
                d.search = $("#search_municipality").val();
                d.canton = $("#canton_filter").val();
            }'
        ];

        $dt_html = new DataTableService($builder_data, $builder);
        $dt_html = $dt_html->builder;

        return view('admin.municipalities.municipalities', compact('dt_html'));
    }

    public function store(MunicipalityRequest $request)
    {
        $response = [];
        $column = 'mun_MunicipalityName_'.$this->current_language;
        $value = $request[$column];
        if (!empty($request->pkMun)) {
            $checkPrev = Municipality::where($column,$value)
            ->where('pkMun', '!=', $request->pkMun)
            ->count();
            if ($checkPrev) {
                $status_code = HttpResponse::HTTP_BAD_REQUEST;
                $response['message'] = trans('message.msg_municipality_exist');
            } else {
                Municipality::where('pkMun', $request->pkMun)->update($request->validated());
                $response['message'] = trans('message.msg_municipality_update_success');
            }
        } else {
            $checkPrev = Municipality::where($column,$value)->count();
            if ($checkPrev) {
                $status_code = HttpResponse::HTTP_BAD_REQUEST;
                $response['message'] = trans('message.msg_municipality_exist');
            } else {
                $id = Municipality::insertGetId($request->validated());
                if (!empty($id)) {
                    Municipality::where('pkMun', $id)->update(['mun_Uid' => "MUN" . $id]);
                    $response['message'] = trans('message.msg_municipality_add_success');

                } else {
                    $status_code = HttpResponse::HTTP_BAD_REQUEST;
                    $response['message'] = trans('message.msg_something_wrong');
                }
            }
        }

        return response()->json($response,$status_code ?? HttpResponse::HTTP_OK);
    }

    public function edit($id)
    {
        $response = [];
        $cdata = Municipality::where('pkMun', '=', $id)->first();

        if (empty($id) || empty($cdata)) {
            $status_code = HttpResponse::HTTP_BAD_REQUEST;
        } else {
            $response['data'] = $cdata;
        }

        return response()->json($response,$status_code ?? HttpResponse::HTTP_OK);

    }

    public function destroy($id)
    {
        $response = [];

        if (empty($id)) {
            $status_code = HttpResponse::HTTP_BAD_REQUEST;
        } else {
            $Employee = Employee::where('fkEmpMun', $id)->count();
            $PostalCode = PostalCode::where('fkPofMun', $id)->count();
            $Student = Student::where('fkStuMun', $id)->count();

            if ($Employee != 0 || $PostalCode != 0 || $Student != 0) {
                $status_code = HttpResponse::HTTP_BAD_REQUEST;
                $response['message'] = trans('message.msg_municipality_delete_prompt');
            } else {
                Municipality::where('pkMun', $id)->delete();
                $response['message'] = trans('message.msg_municipality_delete_success');
            }
        }

        return response()->json($response,$status_code ?? HttpResponse::HTTP_OK);
    }

    /**
     * Fetch Municipalities List
     *
     * @param \Illuminate\Http\Request $request
     * @return JSON
     */
    public function municipalities_list()
    {
        $municipality = Municipality::select('pkMun', 'mun_Uid', 'mun_MunicipalityName_' . $this->current_language . ' as mun_MunicipalityName', 'fkMunCan', 'mun_Notes')
        ->with([
            'canton' => function ($query) {
                $query->select('pkCan', 'can_CantonName_' . $this->current_language . ' as can_CantonName');
            },
        ]);

        $municipality->when(empty(array_first(request('order'))['column']), function($q){
            return $q->orderBy('pkMun','ASC');
        });

        $municipality->when(request('search'), function ($q){
            return $q->where('mun_Uid', 'LIKE', '%' . request('search') . '%')
            ->orWhere('mun_Notes', 'LIKE', '%' . request('search') . '%')
            ->orWhere('mun_MunicipalityName_' . $this->current_language, 'LIKE', '%' . request('search') . '%');
        });


        $municipality->when(request('canton'), function ($q){
            return $q->where('fkMunCan', '=', request('canton'));
        });

        return DataTables::of($municipality)
            ->editColumn('can_CantonName', function($municipality){
                return $municipality->canton->can_CantonName;
            })
            ->editColumn('mun_Notes', function($municipality){
                return substr($municipality->mun_Notes,0,45);
            })
            ->addColumn('action', function ($municipality) {
                $str = '<a cid="'.$municipality->pkMun.'" onclick="triggerEdit('.$municipality->pkMun.')" href="javascript:void(0)"><i class="fa fa-pen"></i></a>&nbsp;&nbsp';
                $str .= '<a onclick="triggerDelete('.$municipality->pkMun.')" href="javascript:void(0)"><i class="fa fa-trash"></i></a>';
                return $str;
            })
            ->addIndexColumn()
            ->escapeColumns()
            ->toJSON();
    }

}
