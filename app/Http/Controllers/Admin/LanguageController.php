<?php
namespace App\Http\Controllers\Admin;

use App\Helpers\FrontHelper;
use App\Http\Controllers\Controller;
use App\Models\Language;
use App\Http\Requests\LanguageRequest;
use App\Http\Traits\FileTrait;
use DataTableService;
use Datatables;
use HtmlBuilder;
use Cache;
use HttpResponse;

class LanguageController extends Controller
{
    use FileTrait;
    public function index(HtmlBuilder $builder)
    {
        $builder_data['columns'] = [
            ['data' => 'DT_RowIndex', 'name' => 'DT_RowIndex', 'title' => trans('general.sr_no'), 'render' => null, 'orderable' => false, 'searchable' => false],
            ['data' => 'language_key', 'name' => 'language_key', 'title' => trans('general.gn_language_key')],
            ['data' => 'language_name', 'name' => 'language_name', 'title' => trans('general.gn_language_name')],
            ['data' => 'action', 'name' => 'action', 'title' => trans('general.gn_actions'), 'orderable' => false, 'searchable' => false, 'width' => '10%'],
        ];

        $builder_data['ajax'] = [
            'url'=> route('languages.list'),
            'data' => 'function(d) {
                d.search =  $("#search_language").val();
            }'
        ];

        $dt_html = new DataTableService($builder_data, $builder);
        $dt_html = $dt_html->builder;

        return view('admin.languages.languages',compact('dt_html'));
    }

    public function store(LanguageRequest $request)
    {
        $response = [];
        $prev_id = Language::orderBy('id', 'desc')->first()->language_key;
        
        if (!empty($request->id)) {
            $checkPrevKey = Language::where('language_key', $request->language_key)->where('id', '!=', $request->id)->first();
            $checkPrev = Language::where('language_name', $request->language_name)->where('id', '!=', $request->id)->first();
            if (!empty($checkPrev)) {
                $status_code = HttpResponse::HTTP_BAD_REQUEST;
                $response['message'] = trans('message.msg_language_exists');
            } elseif (!empty($checkPrevKey)) {
                $status_code = HttpResponse::HTTP_BAD_REQUEST;
                $response['message'] = trans('message.msg_language_key_exists');
            } else {

                $old_key = Language::where('id', $request->id)->first()->language_key;
                $inserted_id = $request->id;

                FrontHelper::updateLanguageColumn('AcademicDegrees', 'acd_AcademicDegreeName_' . $old_key, 'acd_AcademicDegreeName_' . $request->language_key, 'varchar(255)');
                FrontHelper::updateLanguageColumn('Citizenships', 'ctz_CitizenshipName_' . $old_key, 'ctz_CitizenshipName_' . $request->language_key, 'varchar(50)');
                FrontHelper::updateLanguageColumn('Cantons', 'can_CantonName_' . $old_key, 'can_CantonName_' . $request->language_key, 'varchar(255)');
                FrontHelper::updateLanguageColumn('Classes', 'cla_ClassName_' . $old_key, 'cla_ClassName_' . $request->language_key, 'varchar(255)');
                FrontHelper::updateLanguageColumn('Colleges', 'col_CollegeName_' . $old_key, 'col_CollegeName_' . $request->language_key, 'varchar(255)');
                FrontHelper::updateLanguageColumn('Countries', 'cny_CountryName_' . $old_key, 'cny_CountryName_' . $request->language_key, 'varchar(255)');
                FrontHelper::updateLanguageColumn('Courses', 'crs_CourseName_' . $old_key, 'crs_CourseName_' . $request->language_key, 'varchar(255)');
                FrontHelper::updateLanguageColumn('DescriptiveFinalMarks', 'dfm_Text_' . $old_key, 'dfm_Text_' . $request->language_key, 'varchar(255)');
                FrontHelper::updateLanguageColumn('EducationPeriods', 'edp_EducationPeriodName_' . $old_key, 'edp_EducationPeriodName_' . $request->language_key, 'varchar(50)');
                FrontHelper::updateLanguageColumn('EducationPlans', 'epl_EducationPlanName_' . $old_key, 'epl_EducationPlanName_' . $request->language_key, 'varchar(200)');
                FrontHelper::updateLanguageColumn('EducationProfiles', 'epr_EducationProfileName_' . $old_key, 'epr_EducationProfileName_' . $request->language_key, 'varchar(255)');
                FrontHelper::updateLanguageColumn('EducationPrograms', 'edp_Name_' . $old_key, 'edp_Name_' . $request->language_key, 'varchar(100)');
                FrontHelper::updateLanguageColumn('EngagementTypes', 'ety_EngagementTypeName_' . $old_key, 'ety_EngagementTypeName_' . $request->language_key, 'varchar(255)');
                FrontHelper::updateLanguageColumn('ExtracurricuralActivityTypes', 'sat_StudentExtracurricuralActivityName_' . $old_key, 'sat_StudentExtracurricuralActivityName_' . $request->language_key, 'varchar(100)');
                FrontHelper::updateLanguageColumn('FacultativeCoursesGroups', 'fcg_Name_' . $old_key, 'fcg_Name_' . $request->language_key, 'varchar(100)');
                FrontHelper::updateLanguageColumn('ForeignLanguageGroups', 'fon_Name_' . $old_key, 'fon_Name_' . $request->language_key, 'varchar(100)');
                FrontHelper::updateLanguageColumn('GeneralPurposeGroups', 'gpg_Name_' . $old_key, 'gpg_Name_' . $request->language_key, 'varchar(100)');
                FrontHelper::updateLanguageColumn('Grades', 'gra_GradeName_' . $old_key, 'gra_GradeName_' . $request->language_key, 'varchar(100)');
                FrontHelper::updateLanguageColumn('JobAndWork', 'jaw_Name_' . $old_key, 'jaw_Name_' . $request->language_key, 'varchar(255)');
                FrontHelper::updateLanguageColumn('Municipalities', 'mun_MunicipalityName_' . $old_key, 'mun_MunicipalityName_' . $request->language_key, 'varchar(100)');
                FrontHelper::updateLanguageColumn('NationalEducationPlans', 'nep_NationalEducationPlanName_' . $old_key, 'nep_NationalEducationPlanName_' . $request->language_key, 'varchar(255)');
                FrontHelper::updateLanguageColumn('Nationalities', 'nat_NationalityName_' . $old_key, 'nat_NationalityName_' . $request->language_key, 'varchar(50)');
                FrontHelper::updateLanguageColumn('OptionalCoursesGroups', 'ocg_Name_' . $old_key, 'ocg_Name_' . $request->language_key, 'varchar(100)');
                FrontHelper::updateLanguageColumn('OwnershipTypes', 'oty_OwnershipTypeName_' . $old_key, 'oty_OwnershipTypeName_' . $request->language_key, 'varchar(255)');
                FrontHelper::updateLanguageColumn('PostOffices', 'pof_PostOfficeName_' . $old_key, 'pof_PostOfficeName_' . $request->language_key, 'varchar(50)');
                FrontHelper::updateLanguageColumn('QualificationsDegrees', 'qde_QualificationDegreeName_' . $old_key, 'qde_QualificationDegreeName_' . $request->language_key, 'varchar(100)');
                FrontHelper::updateLanguageColumn('Religions', 'rel_ReligionName_' . $old_key, 'rel_ReligionName_' . $request->language_key, 'varchar(50)');
                FrontHelper::updateLanguageColumn('Schools', 'sch_SchoolName_' . $old_key, 'sch_SchoolName_' . $request->language_key, 'varchar(200)');
                FrontHelper::updateLanguageColumn('SchoolYears', 'sye_NameCharacter_' . $old_key, 'sye_NameCharacter_' . $request->language_key, 'varchar(11)');
                FrontHelper::updateLanguageColumn('States', 'sta_StateName_' . $old_key, 'sta_StateName_' . $request->language_key, 'varchar(100)');
                FrontHelper::updateLanguageColumn('StudentBehaviours', 'sbe_BehaviourName_' . $old_key, 'sbe_BehaviourName_' . $request->language_key, 'varchar(100)');
                FrontHelper::updateLanguageColumn('StudentDisciplineMeasureTypes', 'smt_DisciplineMeasureName_' . $old_key, 'smt_DisciplineMeasureName_' . $request->language_key, 'varchar(100)');
                FrontHelper::updateLanguageColumn('Universities', 'uni_UniversityName_' . $old_key, 'uni_UniversityName_' . $request->language_key, 'varchar(255)');
                FrontHelper::updateLanguageColumn('Vocations', 'vct_VocationName_' . $old_key, 'vct_VocationName_' . $request->language_key, 'varchar(100)');
                FrontHelper::updateLanguageColumn('VillageSchools', 'vsc_VillageSchoolName_' . $old_key, 'vsc_VillageSchoolName_' . $request->language_key, 'varchar(255)');
                FrontHelper::updateLanguageColumn('GradeAttemptsNumber', 'gan_Name_' . $old_key, 'gan_Name_' . $request->language_key, 'varchar(200)');
                FrontHelper::updateLanguageColumn('CertificateType', 'cty_Name_' . $old_key, 'cty_Name_' . $request->language_key, 'varchar(200)');
                FrontHelper::updateLanguageColumn('MarksExplanation', 'me_Explanation_' . $old_key, 'me_Explanation_' . $request->language_key, 'varchar(30)');
                FrontHelper::updateLanguageColumn('CertificateBluePrints', 'cbp_BluePrintName_' . $old_key, 'cbp_BluePrintName_' . $request->language_key, 'varchar(60)');
                FrontHelper::updateLanguageColumn('CertificateBluePrints', 'cbp_htmlContent_' . $old_key, 'cbp_htmlContent_' . $request->language_key, 'LONGTEXT');
                FrontHelper::updateLanguageColumn('Shifts', 'shi_ShiftName_' . $old_key, 'shi_ShiftName_' . $request->language_key, 'varchar(20)');

                Language::where('id', $request->id)->update($request->validated());
                $response['message'] = trans('message.msg_language_update_success');
            }
        } else {
            $checkPrev = Language::where('language_name', $request->language_name)->first();
            $checkPrevKey = Language::where('language_key', $request->language_key)->first();
            if (!empty($checkPrev)) {
                
                $status_code = HttpResponse::HTTP_BAD_REQUEST;
                $response['message'] = trans('message.msg_language_exists');

            } elseif (!empty($checkPrevKey)) {

                $status_code = HttpResponse::HTTP_BAD_REQUEST;
                $response['message'] = trans('message.msg_language_key_exists');

            } else {
                $id = Language::insertGetId($request->validated());
                if (!empty($id)) {
                    $inserted_id = $id;
                    FrontHelper::addLanguageColumn('AcademicDegrees', 'acd_AcademicDegreeName_' . $request->language_key, 'acd_AcademicDegreeName_' . $prev_id, 'varchar(255)');
                    FrontHelper::addLanguageColumn('Cantons', 'can_CantonName_' . $request->language_key, 'can_CantonName_' . $prev_id, 'varchar(255)');
                    FrontHelper::addLanguageColumn('Citizenships', 'ctz_CitizenshipName_' . $request->language_key, 'ctz_CitizenshipName_' . $prev_id, 'varchar(50)');
                    FrontHelper::addLanguageColumn('Classes', 'cla_ClassName_' . $request->language_key, 'cla_ClassName_' . $prev_id, 'varchar(255)');
                    FrontHelper::addLanguageColumn('Colleges', 'col_CollegeName_' . $request->language_key, 'col_CollegeName_' . $prev_id, 'varchar(255)');
                    FrontHelper::addLanguageColumn('Countries', 'cny_CountryName_' . $request->language_key, 'cny_CountryName_' . $prev_id, 'varchar(255)');
                    FrontHelper::addLanguageColumn('Courses', 'crs_CourseName_' . $request->language_key, 'crs_CourseName_' . $prev_id, 'varchar(255)');
                    FrontHelper::addLanguageColumn('DescriptiveFinalMarks', 'dfm_Text_' . $request->language_key, 'dfm_Text_' . $prev_id, 'varchar(255)');
                    FrontHelper::addLanguageColumn('EducationPeriods', 'edp_EducationPeriodName_' . $request->language_key, 'edp_EducationPeriodName_' . $prev_id, 'varchar(50)');
                    FrontHelper::addLanguageColumn('EducationPlans', 'epl_EducationPlanName_' . $request->language_key, 'epl_EducationPlanName_' . $prev_id, 'varchar(200)');
                    FrontHelper::addLanguageColumn('EducationProfiles', 'epr_EducationProfileName_' . $request->language_key, 'epr_EducationProfileName_' . $prev_id, 'varchar(255)');
                    FrontHelper::addLanguageColumn('EducationPrograms', 'edp_Name_' . $request->language_key, 'edp_Name_' . $prev_id, 'varchar(100)');
                    FrontHelper::addLanguageColumn('EngagementTypes', 'ety_EngagementTypeName_' . $request->language_key, 'ety_EngagementTypeName_' . $prev_id, 'varchar(255)');
                    FrontHelper::addLanguageColumn('ExtracurricuralActivityTypes', 'sat_StudentExtracurricuralActivityName_' . $request->language_key, 'sat_StudentExtracurricuralActivityName_' . $prev_id, 'varchar(100)');
                    FrontHelper::addLanguageColumn('FacultativeCoursesGroups', 'fcg_Name_' . $request->language_key, 'fcg_Name_' . $prev_id, 'varchar(100)');
                    FrontHelper::addLanguageColumn('ForeignLanguageGroups', 'fon_Name_' . $request->language_key, 'fon_Name_' . $prev_id, 'varchar(100)');
                    FrontHelper::addLanguageColumn('GeneralPurposeGroups', 'gpg_Name_' . $request->language_key, 'gpg_Name_' . $prev_id, 'varchar(100)');
                    FrontHelper::addLanguageColumn('Grades', 'gra_GradeName_' . $request->language_key, 'gra_GradeName_' . $prev_id, 'varchar(100)');
                    FrontHelper::addLanguageColumn('JobAndWork', 'jaw_Name_' . $request->language_key, 'jaw_Name_' . $prev_id, 'varchar(255)');
                    FrontHelper::addLanguageColumn('Municipalities', 'mun_MunicipalityName_' . $request->language_key, 'mun_MunicipalityName_' . $prev_id, 'varchar(100)');
                    FrontHelper::addLanguageColumn('NationalEducationPlans', 'nep_NationalEducationPlanName_' . $request->language_key, 'nep_NationalEducationPlanName_' . $prev_id, 'varchar(255)');
                    FrontHelper::addLanguageColumn('Nationalities', 'nat_NationalityName_' . $request->language_key, 'nat_NationalityName_' . $prev_id, 'varchar(50)');
                    FrontHelper::addLanguageColumn('OptionalCoursesGroups', 'ocg_Name_' . $request->language_key, 'ocg_Name_' . $prev_id, 'varchar(100)');
                    FrontHelper::addLanguageColumn('OwnershipTypes', 'oty_OwnershipTypeName_' . $request->language_key, 'oty_OwnershipTypeName_' . $prev_id, 'varchar(255)');
                    FrontHelper::addLanguageColumn('PostOffices', 'pof_PostOfficeName_' . $request->language_key, 'pof_PostOfficeName_' . $prev_id, 'varchar(50)');
                    FrontHelper::addLanguageColumn('QualificationsDegrees', 'qde_QualificationDegreeName_' . $request->language_key, 'qde_QualificationDegreeName_' . $prev_id, 'varchar(100)');
                    FrontHelper::addLanguageColumn('Religions', 'rel_ReligionName_' . $request->language_key, 'rel_ReligionName_' . $prev_id, 'varchar(50)');
                    FrontHelper::addLanguageColumn('Schools', 'sch_SchoolName_' . $request->language_key, 'sch_SchoolName_' . $prev_id, 'varchar(200)');
                    FrontHelper::addLanguageColumn('SchoolYears', 'sye_NameCharacter_' . $request->language_key, 'sye_NameCharacter_' . $prev_id, 'varchar(11)');
                    FrontHelper::addLanguageColumn('States', 'sta_StateName_' . $request->language_key, 'sta_StateName_' . $prev_id, 'varchar(100)');
                    FrontHelper::addLanguageColumn('StudentBehaviours', 'sbe_BehaviourName_' . $request->language_key, 'sbe_BehaviourName_' . $prev_id, 'varchar(100)');
                    FrontHelper::addLanguageColumn('StudentDisciplineMeasureTypes', 'smt_DisciplineMeasureName_' . $request->language_key, 'smt_DisciplineMeasureName_' . $prev_id, 'varchar(100)');
                    FrontHelper::addLanguageColumn('Universities', 'uni_UniversityName_' . $request->language_key, 'uni_UniversityName_' . $prev_id, 'varchar(255)');
                    FrontHelper::addLanguageColumn('Vocations', 'vct_VocationName_' . $request->language_key, 'vct_VocationName_' . $prev_id, 'varchar(100)');
                    FrontHelper::addLanguageColumn('VillageSchools', 'vsc_VillageSchoolName_' . $request->language_key, 'vsc_VillageSchoolName_' . $prev_id, 'varchar(255)');
                    FrontHelper::addLanguageColumn('GradeAttemptsNumber', 'gan_Name_' . $request->language_key, 'gan_Name_' . $prev_id, 'varchar(200)');
                    FrontHelper::addLanguageColumn('CertificateType', 'cty_Name_' . $request->language_key, 'cty_Name_' . $prev_id, 'varchar(200)');
                    FrontHelper::addLanguageColumn('MarksExplanation', 'me_Explanation_' . $request->language_key, 'me_Explanation_' . $prev_id, 'varchar(30)');
                    FrontHelper::addLanguageColumn('CertificateBluePrints', 'cbp_BluePrintName_' . $request->language_key, 'cbp_BluePrintName_' . $prev_id, 'varchar(60)');
                    FrontHelper::addLanguageColumn('CertificateBluePrints', 'cbp_htmlContent_' . $request->language_key, 'cbp_htmlContent_' . $prev_id, 'LONGTEXT');
                    FrontHelper::addLanguageColumn('Shifts', 'shi_ShiftName_' . $request->language_key, 'shi_ShiftName_' . $prev_id, 'varchar(20)');

                    $response['message'] = trans('message.msg_language_add_success');
                } else {
                    $status_code = HttpResponse::HTTP_BAD_REQUEST;
                    $response['message'] = trans('message.msg_something_wrong');
                }
            }
        }

        if($request->file('upload_flag') && !empty($inserted_id)){

            $lang = Language::where('id',$inserted_id)->first();
            if (!empty($lang->flag)) {
                $unlink_file = $lang->flag;
            }

            $upload_params = [
                'folder_path' => config('assetpath.lang_images'),
                'unlink_file' => $unlink_file ?? ''
            ];

            $image = $request->file('upload_flag');
            $filename = $this->uploadFiletoStorage($image,$upload_params);

            if(!empty($filename)){
                Language::where('id',$inserted_id)->update(['flag'=>$filename]);
            }
        }

        Cache::forget('languages');
        return response()->json($response,$status_code ?? HttpResponse::HTTP_OK);
    }

    public function edit($id)
    {
        $response = [];
        $cdata = Language::where('id', '=', $id)->first();

        if (empty($id) || empty($cdata)) {
                $status_code = HttpResponse::HTTP_BAD_REQUEST;
        } else {
            if (!empty($cdata->flag)) {
                $cdata->flag = asset(config('assetpath.lang_images')) . '/' . $cdata->flag;
            }
            $response['data'] = $cdata;
        }

        return response()->json($response,$status_code ?? HttpResponse::HTTP_OK);

    }

    public function destroy($id)
    {
        $response = [];
        if (empty($id)) {
            $status_code = HttpResponse::HTTP_BAD_REQUEST;
        } else {
            $lang = Language::select('language_key')->where('id', $id)->first();
            $langkey = $lang->language_key;

            FrontHelper::dropColumn('AcademicDegrees', 'acd_AcademicDegreeName_' . $langkey);
            FrontHelper::dropColumn('Cantons', 'can_CantonName_' . $langkey);
            FrontHelper::dropColumn('Citizenships', 'ctz_CitizenshipName_' . $langkey);
            FrontHelper::dropColumn('Classes', 'cla_ClassName_' . $langkey);
            FrontHelper::dropColumn('Colleges', 'col_CollegeName_' . $langkey);
            FrontHelper::dropColumn('Countries', 'cny_CountryName_' . $langkey);
            FrontHelper::dropColumn('Courses', 'crs_CourseName_' . $langkey);
            FrontHelper::dropColumn('DescriptiveFinalMarks', 'dfm_Text_' . $langkey);
            FrontHelper::dropColumn('EducationPeriods', 'edp_EducationPeriodName_' . $langkey);
            FrontHelper::dropColumn('EducationPlans', 'epl_EducationPlanName_' . $langkey);
            FrontHelper::dropColumn('EducationProfiles', 'epr_EducationProfileName_' . $langkey);
            FrontHelper::dropColumn('EducationPrograms', 'edp_Name_' . $langkey);
            FrontHelper::dropColumn('EngagementTypes', 'ety_EngagementTypeName_' . $langkey);
            FrontHelper::dropColumn('ExtracurricuralActivityTypes', 'sat_StudentExtracurricuralActivityName_' . $langkey);
            FrontHelper::dropColumn('FacultativeCoursesGroups', 'fcg_Name_' . $langkey);
            FrontHelper::dropColumn('ForeignLanguageGroups', 'fon_Name_' . $langkey);
            FrontHelper::dropColumn('GeneralPurposeGroups', 'gpg_Name_' . $langkey);
            FrontHelper::dropColumn('Grades', 'gra_GradeName_' . $langkey);
            FrontHelper::dropColumn('JobAndWork', 'jaw_Name_' . $langkey);
            FrontHelper::dropColumn('Municipalities', 'mun_MunicipalityName_' . $langkey);
            FrontHelper::dropColumn('NationalEducationPlans', 'nep_NationalEducationPlanName_' . $langkey);
            FrontHelper::dropColumn('Nationalities', 'nat_NationalityName_' . $langkey);
            FrontHelper::dropColumn('OptionalCoursesGroups', 'ocg_Name_' . $langkey);
            FrontHelper::dropColumn('OwnershipTypes', 'oty_OwnershipTypeName_' . $langkey);
            FrontHelper::dropColumn('PostOffices', 'pof_PostOfficeName_' . $langkey);
            FrontHelper::dropColumn('QualificationsDegrees', 'qde_QualificationDegreeName_' . $langkey);
            FrontHelper::dropColumn('Religions', 'rel_ReligionName_' . $langkey);
            FrontHelper::dropColumn('Schools', 'sch_SchoolName_' . $langkey);
            FrontHelper::dropColumn('SchoolYears', 'sye_NameCharacter_' . $langkey);
            FrontHelper::dropColumn('States', 'sta_StateName_' . $langkey);
            FrontHelper::dropColumn('StudentBehaviours', 'sbe_BehaviourName_' . $langkey);
            FrontHelper::dropColumn('StudentDisciplineMeasureTypes', 'smt_DisciplineMeasureName_' . $langkey);
            FrontHelper::dropColumn('Universities', 'uni_UniversityName_' . $langkey);
            FrontHelper::dropColumn('Vocations', 'vct_VocationName_' . $langkey);
            FrontHelper::dropColumn('VillageSchools', 'vsc_VillageSchoolName_' . $langkey);
            FrontHelper::dropColumn('GradeAttemptsNumber', 'gan_Name_' . $langkey);
            FrontHelper::dropColumn('CertificateType', 'cty_Name_' . $langkey);
            FrontHelper::dropColumn('MarksExplanation', 'me_Explanation_' . $langkey);
            FrontHelper::dropColumn('CertificateBluePrints', 'cbp_BluePrintName_' . $langkey);
            FrontHelper::dropColumn('CertificateBluePrints', 'cbp_htmlContent_' . $langkey);
            FrontHelper::dropColumn('Shifts', 'shi_ShiftName_' . $langkey);

            Language::where('id', $id)->delete();
            $response['message'] = trans('message.msg_language_deleted');
        }

        Cache::forget('languages');
        return response()->json($response,$status_code ?? HttpResponse::HTTP_OK);
    }

    /**
     * Get Languages List
     *
     * @param \Illuminate\Http\Request $request
     * @return JSON
     */
    public function languages_list()
    {
        $language = Language::select('*');

        $language->when(request('search'), function ($q){
            return $q->where('language_key', 'LIKE', '%' . request('search') . '%')
            ->orWhere('language_name', 'LIKE', '%' . request('search') . '%');
        });

        $language->when(empty(array_first(request('order'))['column']), function($q){
            return $q->orderBy('id','ASC');
        });

        return DataTables::of($language)
            ->addColumn('action', function ($language) {
                $str = '<a cid="'.$language->id.'" onclick="triggerEdit('.$language->id.')" href="javascript:void(0)"><i class="fa fa-pen"></i></a>&nbsp;&nbsp';
                $str .= '<a onclick="triggerDelete('.$language->id.')" href="javascript:void(0)"><i class="fa fa-trash"></i></a>';
                return $str;
            })
            ->addIndexColumn()
            ->escapeColumns()
            ->toJSON();
    }

}
