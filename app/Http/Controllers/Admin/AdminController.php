<?php
/**
 * AdminController
 *
 * @package    Laravel
 * @subpackage Controller
 * @since      1.0
 */

namespace App\Http\Controllers\Admin;

use App\Helpers\UtilHelper;
use App\Http\Controllers\Controller;
use App\Models\Admin;
use App\Http\Traits\UserTrait;
use App\Http\Traits\FileTrait;
use Auth;
use Illuminate\Http\Request;

class AdminController extends Controller
{
    use UserTrait,FileTrait;
    public function index(Request $request)
    {
        $user = Auth::guard('admin')->user();
        $SuperAdminCount = 0;
        if ($user->type == 'HertronicAdmin' || $user->type == 'MinistryAdmin' || $user->type == 'MinistrySubAdmin') {
            $SuperAdminCount = Admin::where('type', '=', 'HertronicAdmin')->count();
        }
        if (request()
            ->ajax()) {
            return \View::make('admin.dashboard.dashboard')
                ->with(compact('SuperAdminCount'))
                ->renderSections();
        }
        return view('admin.dashboard.dashboard', compact('SuperAdminCount'));
    }

    public function profile(Request $request)
    {
        /**
         * Used for Admin Profile
         * @return redirect to Admin->Profile
         */
        $admin_data = Admin::findorFail($this->logged_user->id);
        return view('admin.dashboard.profile',compact('admin_data'));
    }

    public function editProfile(Request $request)
    {
        $response = [];
        $input = $request->all();

        $check_user_params = [
                             'type'=>'Admin',
                             'id'=>$this->logged_user->id,
                             'email'=>$input['email'],
                             'gov_id'=>$input['govt_id'],
                             'temp_id'=>$input['adm_TempGovId']
                            ];
        $check_user_status = $this->checkUserEmailorUidExist($check_user_params);

        if($check_user_status['email_exist']){
            $response['status'] = false;
            $response['message'] = trans('message.msg_email_exist');
            return response()
                ->json($response);
        }

        if($check_user_status['gov_id_exist']){
            $response['status'] = false;
            $response['message'] = trans('message.msg_govt_id_exist');
            return response()
                ->json($response);
        }

        if($check_user_status['temp_id_exist']){
            $response['status'] = false;
            $response['message'] = trans('message.msg_govt_id_exist');
            return response()
                ->json($response);
        }

        $user = Admin::findorfail($this->logged_user->id);

        $user->email = $input['email'];
        $user->adm_Name = $input['name'];
        $user->adm_Title = $input['title'];
        $user->adm_Phone = $input['phone'];
        $user->adm_GovId = $input['govt_id'];
        $user->adm_TempGovId = $input['adm_TempGovId'];
        $user->adm_Gender = $input['gender'];
        $user->adm_DOB = UtilHelper::sqlDate($input['dob']);

        if($request->file('upload_profile')){
            if (!empty($user->adm_Photo)) {
                $unlink_file = $user->adm_Photo;
            }

            $upload_params = [
                'folder_path'=>config('assetpath.user_images'),
                'unlink_file'=>$unlink_file ?? ''
            ];
            $image = $request->file('upload_profile');
            $filename = $this->uploadFiletoStorage($image,$upload_params);

            if(!empty($filename)){
                $user->adm_Photo = $filename;
            }
        }

        if ($user->save()) {
            $this->logged_user = $user;
            $this->logged_user->type = 'admin';
            $response['message'] = trans('message.msg_profile_update_success');

        } else {
            $status_code = 400;
            $response['message'] = trans('message.msg_something_wrong');
        }

        return response()
            ->json($response,$status_code ?? 200);

    }

}
