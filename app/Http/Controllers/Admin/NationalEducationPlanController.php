<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\EducationPlan;
use App\Models\NationalEducationPlan;
use App\Http\Requests\NationalEducationPlanRequest;
use DataTableService;
use Datatables;
use HtmlBuilder;
use HttpResponse;

class NationalEducationPlanController extends Controller
{
    public function index(HtmlBuilder $builder)
    {
        $builder_data['columns'] = [
            ['data' => 'DT_RowIndex', 'name' => 'DT_RowIndex', 'title' => trans('general.sr_no'), 'render' => null, 'orderable' => false, 'searchable' => false],
            ['data' => 'nep_Uid', 'name' => 'nep_Uid', 'title' => trans('general.gn_uid')],
            ['data' => 'nep_NationalEducationPlanName', 'name' => 'nep_NationalEducationPlanName', 'title' => trans('general.gn_name')],
            ['data' => 'nep_Notes', 'name' => 'nep_Notes', 'title' => trans('general.gn_notes')],
            ['data' => 'nep_Status', 'name' => 'nep_Status', 'title' => trans('general.gn_status')],
            ['data' => 'action', 'name' => 'action', 'title' => trans('general.gn_actions'), 'orderable' => false, 'searchable' => false, 'width' => '10%'],
        ];

        $builder_data['ajax'] = [
            'url'=> route('nationaleducationplans.list'),
            'data' => 'function(d) {
                d.search =  $("#search_education_plan").val();
            }'
        ];

        $dt_html = new DataTableService($builder_data, $builder);
        $dt_html = $dt_html->builder;

        return view('admin.nationalEducationPlans.nationalEducationPlans',compact('dt_html'));
    }

    public function store(NationalEducationPlanRequest $request)
    {
        $column = 'nep_NationalEducationPlanName_'.$this->current_language;
        $value = $request[$column];

        $response = [];
        if (!empty($request->pkNep)) {
            $checkPrev = NationalEducationPlan::where($column,$value)
                    ->where('pkNep', '!=', $request->pkNep)->count();
            if ($checkPrev) {
                $status_code = HttpResponse::HTTP_BAD_REQUEST;
                $response['message'] = trans('message.msg_nep_exist');
            } else {
                NationalEducationPlan::where('pkNep', $request->pkNep)->update($request->validated());
                $response['message'] = trans('message.msg_nep_update_success');
            }
        } else {
            $checkPrev = NationalEducationPlan::where($column,$value)
                    ->count();
            if ($checkPrev) {
                $status_code = HttpResponse::HTTP_BAD_REQUEST;
                $response['message'] = trans('message.msg_nep_exist');
            } else {
                $id = NationalEducationPlan::insertGetId($request->validated());
                if (!empty($id)) {
                    NationalEducationPlan::where('pkNep', $id)->update(['nep_Uid' => "NEP" . $id]);

                    $response['message'] = trans('message.msg_nep_add_success');

                } else {
                    $status_code = HttpResponse::HTTP_BAD_REQUEST;
                    $response['message'] = trans('message.msg_something_wrong');
                }
            }
        }

        return response()->json($response,$status_code ?? HttpResponse::HTTP_OK);
    }

    public function edit($cid)
    {
        $response = [];
        $cdata = NationalEducationPlan::where('pkNep', '=', $cid)->first();

        if (empty($cid) || empty($cdata)) {
            $status_code = HttpResponse::HTTP_BAD_REQUEST;
        } else {

            $response['data'] = $cdata;
        }

        return response()->json($response,$status_code ?? HttpResponse::HTTP_OK);

    }

    public function destroy($cid)
    {
        $response = [];

        if (empty($cid)) {
            $status_code = HttpResponse::HTTP_BAD_REQUEST;
        } else {

            $EducationPlan = EducationPlan::where('fkEplNep', $cid)
                ->count();

            if ($EducationPlan != 0) {
                $status_code = HttpResponse::HTTP_BAD_REQUEST;
                $response['message'] = trans('message.msg_nep_delete_prompt');
            } else {
                NationalEducationPlan::where('pkNep', $cid)->delete();

                $response['message'] = trans('message.msg_nep_delete_success');
            }
        }

        return response()->json($response,$status_code ?? HttpResponse::HTTP_OK);
    }

    /**
     * Get Education Plan List
     *
     * @param \Illuminate\Http\Request $request
     * @return JSON
     */
    public function nationaleducationplans_list()
    {
        $education_plan = NationalEducationPlan::select('*', 'nep_NationalEducationPlanName_' . $this->current_language . ' as nep_NationalEducationPlanName');

        $education_plan->when(request('search'),function($q){
            return $q->where('nep_Uid', 'LIKE', '%' . request('search') . '%')
            ->orWhere('nep_Notes', 'LIKE', '%' . request('search') . '%')
            ->orWhere('nep_NationalEducationPlanName_' . $this->current_language, 'LIKE', '%' . request('search') . '%');
        });

        return DataTables::of($education_plan)
            ->editColumn('nep_Notes',function($education_plan){
                return substr($education_plan->nep_Notes,0,45);
            })
            ->editColumn('nep_Status',function($education_plan){
                if($education_plan->nep_Status == "Active"){
                    return trans('general.gn_active');
                }else{
                    return trans('general.gn_inactive');
                }
            })
            ->addColumn('action', function ($education_plan) {
                $str = '<a cid="'.$education_plan->pkNep.'" onclick="triggerEdit('.$education_plan->pkNep.')" href="javascript:void(0)"><i class="fa fa-pen"></i></a>&nbsp;&nbsp';
                $str .= '<a onclick="triggerDelete('.$education_plan->pkNep.')" href="javascript:void(0)"><i class="fa fa-trash"></i></a>';
                return $str;
            })
            ->addIndexColumn()
            ->escapeColumns()
            ->toJSON();


    }

}
