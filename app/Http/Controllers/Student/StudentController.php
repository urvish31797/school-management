<?php
/**
 * StudentController
 *
 * @package    Laravel
 * @subpackage Controller
 * @since      1.0
 */

namespace App\Http\Controllers\Student;

use App\Helpers\UtilHelper;
use App\Http\Controllers\Controller;
use App\Http\Traits\CommonTrait;
use App\Models\AcademicDegree;
use App\Models\Admin;
use App\Models\Citizenship;
use App\Models\College;
use App\Models\Country;
use App\Models\Employee;
use App\Models\EmployeeDesignation;
use App\Models\EmployeesEngagement;
use App\Models\EmployeeType;
use App\Models\EngagementType;
use App\Models\Municipality;
use App\Models\Nationality;
use App\Models\PostalCode;
use App\Models\QualificationDegree;
use App\Models\Religion;
use App\Models\University;
use Illuminate\Http\Request;
use Session;

class StudentController extends Controller
{
    use CommonTrait;

    /**
     * Dashboard
     *
     * @param \Illuminate\Http\Request $request
     * @return view - student.dashboard.dashboard
     */
    public function dashboard(Request $request)
    {
        $SubAdminCount = 0;
        $view = '';

        if ($this->logged_user->type == 'Student') {
            $data['SubAdminCount'] = $SubAdminCount;
            $view = 'student.dashboard.dashboard';
        }

        return view($view, $data);
    }

    /**
     * Profile Page
     *
     * @param \Illuminate\Http\Request $request
     * @return view - employee.dashboard.profile
     */
    public function profile(Request $request)
    {
        $AcademicDegrees = AcademicDegree::select('pkAcd', 'acd_AcademicDegreeName_' . $this->current_language . ' as acd_AcademicDegreeName')->get();
        $Colleges = College::select('pkCol', 'col_CollegeName_' . $this->current_language . ' as col_CollegeName')->get();
        $Countries = Country::select('pkCny', 'cny_CountryName_' . $this->current_language . ' as cny_CountryName')->get();
        $QualificationDegrees = QualificationDegree::select('pkQde', 'qde_QualificationDegreeName_' . $this->current_language . ' as qde_QualificationDegreeName')->get();
        $Municipalities = Municipality::select('pkMun', 'mun_MunicipalityName_' . $this->current_language . ' as mun_MunicipalityName')->get();
        $PostalCodes = PostalCode::select('pkPof', 'pof_PostOfficeNumber', 'pof_PostOfficeName_' . $this->current_language . ' as pof_PostOfficeName')->get();
        $Citizenships = Citizenship::select('pkCtz', 'ctz_CitizenshipName_' . $this->current_language . ' as ctz_CitizenshipName')->get();
        $Nationalities = Nationality::select('pkNat', 'nat_NationalityName_' . $this->current_language . ' as nat_NationalityName')->get();
        $Religions = Religion::select('pkRel', 'rel_ReligionName_' . $this->current_language . ' as rel_ReligionName')->get();
        $Universities = University::select('pkUni', 'uni_UniversityName_' . $this->current_language . ' as uni_UniversityName')->get();
        $EmployeeDesignations = EmployeeDesignation::select('pkEde', 'ede_EmployeeDesignationName_' . $this->current_language . ' as ede_EmployeeDesignationName')->get();

        $schoolDetail = Employee::with('EmployeesEngagement', 'EmployeesEngagement.employeeType')->whereHas('EmployeesEngagement', function ($query) {
            $query->whereHas('employeeType', function ($query) {
                $query->where('epty_Name', 'SchoolCoordinator')->orWhere('epty_Name', 'SchoolSubAdmin')->orWhere('epty_Name', 'Teacher');
            })->where('fkEenEmp', $this->logged_user->id)->where(function ($query) {
                $query->where('een_DateOfFinishEngagement', '=', null)
                    ->orWhere('een_DateOfFinishEngagement', '>=', now());
            });
        })->first();

        $mainSchool = $schoolDetail->EmployeesEngagement[0]->fkEenSch;
        $EngagementTypes = EngagementType::select('pkEty', 'ety_EngagementTypeName_' . $this->current_language . ' as ety_EngagementTypeName')->get();

        $EmployeeTypes = $this->getSelectedRoles([$this->employees[0], $this->employees[1], $this->employees[2]], false);

        $currentEmpType = EmployeeType::where('epty_Name', $this->logged_user->type)->first();

        $EmployeesEngagements = EmployeesEngagement::where('fkEenEmp', $this->logged_user->id)->where('fkEenSch', $mainSchool)->where('een_DateOfFinishEngagement', null)->where('fkEenEpty', '!=', $currentEmpType->pkEpty)->get();

        $EmployeesDetail = Employee::with(['employeeEducation',
            'employeeEducation.academicDegree' => function ($query) {
                $query->select('pkAcd', 'acd_AcademicDegreeName_' . $this->current_language . ' as acd_AcademicDegreeName');
            },
            'employeeEducation.college' => function ($query) {
                $query->select('pkCol', 'col_CollegeName_' . $this->current_language . ' as col_CollegeName');
            },
            'employeeEducation.university' => function ($query) {
                $query->select('pkUni', 'uni_UniversityName_' . $this->current_language . ' as uni_UniversityName');
            },
            'employeeEducation.university.college',
            'employeeEducation.qualificationDegree' => function ($query) {
                $query->select('pkQde', 'qde_QualificationDegreeName_' . $this->current_language . ' as qde_QualificationDegreeName');
            },
            'employeeEducation.employeeDesignation' => function ($query) {
                $query->select('pkEde', 'ede_EmployeeDesignationName_' . $this->current_language . ' as ede_EmployeeDesignationName');
            },
            'municipality' => function ($query) {
                $query->select('pkMun', 'mun_MunicipalityName_' . $this->current_language . ' as mun_MunicipalityName');
            },
            'postalCode' => function ($query) {
                $query->select('pkPof', 'pof_PostOfficeNumber');
            },
            'country' => function ($query) {
                $query->select('pkCny', 'cny_CountryName_' . $this->current_language . ' as cny_CountryName');
            },
            'nationality' => function ($query) {
                $query->select('pkNat', 'nat_NationalityName_' . $this->current_language . ' as nat_NationalityName');
            },
            'religion' => function ($query) {
                $query->select('pkRel', 'rel_ReligionName_' . $this->current_language . ' as rel_ReligionName');
            },
            'citizenship' => function ($query) {
                $query->select('pkCtz', 'ctz_CitizenshipName_' . $this->current_language . ' as ctz_CitizenshipName');
            },
            'EmployeesEngagement.engagementType' => function ($query) {
                $query->select('pkEty', 'ety_EngagementTypeName_' . $this->current_language . ' as ety_EngagementTypeName');
            },
            'EmployeesEngagement.school' => function ($query) {
                $query->select('pkSch', 'sch_SchoolName_' . $this->current_language . ' as sch_SchoolName');
            },
            'EmployeesEngagement.employeeType',

        ]);
        $EmployeesDetail = $EmployeesDetail->where('id', '=', $this->logged_user->id)->first();

        return view('employee.dashboard.profile')->with(['Countries' => $Countries, 'Municipalities' => $Municipalities, 'PostalCodes' => $PostalCodes, 'Citizenships' => $Citizenships, 'Nationalities' => $Nationalities, 'Religions' => $Religions, 'Universities' => $Universities, 'Colleges' => $Colleges, 'EmployeeDesignations' => $EmployeeDesignations, 'QualificationDegrees' => $QualificationDegrees, 'AcademicDegrees' => $AcademicDegrees, 'EmployeesDetail' => $EmployeesDetail, 'EngagementTypes' => $EngagementTypes, 'EmployeesEngagements' => $EmployeesEngagements, 'EmployeeTypes' => $EmployeeTypes, 'currentEmpType' => $currentEmpType->pkEpty, 'MainSchool' => $mainSchool]);
    }

    /**
     * Update Profile
     *
     * @param \Illuminate\Http\Request $request
     * @return JSON - $response
     */
    public function editProfile(Request $request)
    {

        $response = [];
        $input = $request->all();
        $image = $request->file('upload_profile');

        $emailExist = Employee::where('email', '=', $input['email'])->where('id', '!=', $this->logged_user->id)->get();
        $govIdExist = Employee::where('emp_EmployeeID', '=', $input['emp_EmployeeID'])->where('id', '!=', $this->logged_user->id)->get();
        $tempIdExist = Employee::where('emp_TempCitizenId', '=', $input['emp_TempCitizenId'])->where('emp_TempCitizenId', '!=', '')->where('id', '!=', $this->logged_user->id)->get();
        $emailAdmExist = Admin::where('email', '=', $input['email'])->get();
        $checkPrev = Employee::where('emp_EmployeeName', $input['emp_EmployeeName'])->where('id', '!=', $this->logged_user->id)->first();

        $emailExistCount = $emailExist->count();
        $emailAdmExistCount = $emailAdmExist->count();
        $tempIdExistCount = $tempIdExist->count();
        $govIdExistCount = $govIdExist->count();

        if ($emailExistCount != 0 || $emailAdmExistCount != 0) {
            $response['status'] = false;
            $response['message'] = trans('message.msg_email_exist');
            return response()->json($response);
        }

        if ($govIdExistCount != 0) {
            $response['status'] = false;
            $response['message'] = trans('message.msg_employee_id_exist');
            return response()->json($response);
        }

        if ($tempIdExistCount != 0) {
            $response['status'] = false;
            $response['message'] = trans('message.msg_temp_citizen_id_exist');
            return response()->json($response);
        }

        if (!empty($checkPrev)) {
            $response['status'] = false;
            $response['message'] = trans('message.msg_user_exist');
            return response()->json($response);
        }

        $user = Employee::findorfail($this->logged_user->id);

        $user->email = $input['email'];
        $user->emp_EmployeeName = $input['emp_EmployeeName'];
        $user->emp_EmployeeSurname = $input['emp_EmployeeSurname'];
        $user->emp_EmployeeID = $input['emp_EmployeeID'];
        $user->emp_TempCitizenId = $input['emp_TempCitizenId'];
        $user->emp_EmployeeGender = $input['emp_EmployeeGender'];
        $user->emp_DateOfBirth = UtilHelper::sqlDate($input['emp_DateOfBirth']);
        $user->emp_PlaceOfBirth = $input['emp_PlaceOfBirth'];
        $user->emp_Address = $input['emp_Address'];
        $user->emp_PhoneNumber = $input['emp_PhoneNumber'];
        $user->emp_Notes = $input['emp_Notes'];
        $user->fkEmpMun = $input['fkEmpMun'];
        $user->fkEmpPof = $input['fkEmpPof'];
        $user->fkEmpCny = $input['fkEmpCny'];
        $user->fkEmpNat = $input['fkEmpNat'];
        $user->fkEmpRel = $input['fkEmpRel'];
        $user->fkEmpCtz = $input['fkEmpCtz'];

        if (!empty($image)) {
            $input['emp_PicturePath'] = time() . '.' . $image->getClientOriginalExtension();
            $destinationPath = storage_path('app/public/images/users');
            $image->move($destinationPath, $input['emp_PicturePath']);
            if (!empty($user->emp_PicturePath)) {
                $filepath = storage_path('app/public/images/users/') . $user->emp_PicturePath;
                if (file_exists($filepath)) {
                    unlink($filepath);
                }
            }
            $user->emp_PicturePath = $input['emp_PicturePath'];
        }

        if ($user->save()) {
            $this->logged_user = $user;
            $this->logged_user->utype = 'employee';
            $mdata = Employee::with('employeesEngagement.employeeType')->where('id', '=', $this->logged_user->id)->first();
            $this->logged_user->type = $mdata->employeesEngagement[0]->employeeType->epty_Name;
            $response['status'] = true;
            $response['message'] = trans('message.msg_profile_update_success');

        } else {
            $response['status'] = false;
            $response['message'] = trans('message.msg_something_wrong');
        }

        return response()->json($response);

    }

    /**
     * Switch Role
     *
     * @param \Illuminate\Http\Request $request
     * @return JSON $response
     */
    public function switchRole(Request $request)
    {
        $response = [];
        $data = $request->all();

        $this->logged_user->type = $data['role'];
        Session::put('curr_emp_type', $data['role']);
        $response['status'] = true;
        $response['redirect'] = url($this->logged_user->utype . '/dashboard');
        return response()->json($response);

    }

}
