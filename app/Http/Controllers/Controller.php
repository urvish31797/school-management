<?php

namespace App\Http\Controllers;

use App\Models\Employee;
use App\Models\Language;
use App\Models\SchoolYear;
use Auth;
use Cache;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\App;
use Session;
use View;

class Controller extends BaseController
{
    public $logged_user;

    public $admins = ['HertronicAdmin', 'MinistryAdmin', 'MinistrySubAdmin'];

    //Do not change this order this order used in many places so be careful.
    public $employees = ['Teacher', 'Principal', 'SchoolCoordinator', 'SchoolSubAdmin', 'HomeroomTeacher'];
    public $employeesRoleId = [4, 1, 2, 3, 6];
    public $languages;

    public function __construct()
    {
        $this->middleware(function ($request, $next) {

            if (Session::has('current_language')) {
                $this->current_language = Session::get('current_language');
                App::setLocale(Session::get('current_language'));
            } else {
                $this->current_language = 'en';
                App::setLocale('en');
            }

            if (!empty(Auth::guard('admin')->check()) && empty(Auth::guard('employee')->check())) {

                $this->logged_user = Auth::guard('admin')->user();
                $this->logged_user->utype = 'admin';

            } elseif (!empty(Auth::guard('employee')->check())) {

                $this->logged_user = Auth::guard('employee')->user();
                $this->logged_user->utype = 'employee';

                if (Session::has('curr_emp_type')) {
                    $this->logged_user->type = Session::get('curr_emp_type');
                }
                if (Session::has('curr_emp_eid')) {
                    $this->logged_user->eid = Session::get('curr_emp_eid');
                }
                if (Session::has('curr_emp_sid')) {
                    $this->logged_user->sid = Session::get('curr_emp_sid');
                }
                if (Session::has('curr_school_sem')) {
                    $this->logged_user->semid = Session::get('curr_school_sem');
                }
                if (Session::has('curr_school_year')) {
                    $this->logged_user->syid = Session::get('curr_school_year');
                }
                if (Session::has('curr_course_class')) {
                    $this->logged_user->cosid = Session::get('curr_course_class');
                }
                if (Session::has('curr_shift')) {
                    $this->logged_user->shiftid = Session::get('curr_shift');
                }
                if (Session::has('employeeRoles')) {
                    $employeeRoles = Session::get('employeeRoles');
                } else {
                    $employeeRoles = Employee::select('id', 'emp_EmployeeName', 'emp_EmployeeSurname')->with([
                        'EmployeesEngagement' => function ($query) {
                            $query->whereNull('een_DateOfFinishEngagement')->leftJoin('HomeRoomTeacher', 'HomeRoomTeacher.fkHrtEen', '=', 'EmployeesEngagement.pkEen');
                        },
                        'EmployeesEngagement.school' => function ($query) {
                            $query->select('pkSch', 'sch_SchoolName_' . $this->current_language . ' as sch_SchoolName');
                        },
                        'EmployeesEngagement.employeeType' => function ($q) {
                            $q->select('pkEpty', 'epty_Name');
                        },
                    ]);
                    $employeeRoles = $employeeRoles->where('id', '=', $this->logged_user->id)->first();
                    Session::put('employeeRoles', $employeeRoles);
                }
                View::share('employeeRoles', $employeeRoles);

                if ($this->logged_user->type != 'SchoolCoordinator') {
                    if (Session::has('schoolYear')) {
                        $schoolYear = Session::get('schoolYear');
                    } else {
                        $schoolYear = SchoolYear::select('pkSye', 'sye_NameCharacter_' . $this->current_language . ' AS sye_NameCharacter')->get();
                        Session::put('schoolYear', $schoolYear);
                    }
                    View::share('schoolYear', $schoolYear);
                }

            } elseif (!empty(Auth::guard('student')->check())) {

                $this->logged_user = Auth::guard('student')->user();
                $this->logged_user->utype = 'student';
                $this->logged_user->type = 'Student';
            }

            if (Cache::has('languages')) {
                $this->languages = Cache::get('languages');
            } else {
                $this->languages = Cache::rememberForever('languages', function () {
                    return Language::get();
                });
            }

            View::share('languages', $this->languages);
            View::share('logged_user', $this->logged_user);
            View::share('current_language', $this->current_language);

            return $next($request);
        });
    }

}
