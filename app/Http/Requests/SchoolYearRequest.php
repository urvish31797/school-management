<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use LanguageRequestService;

class SchoolYearRequest extends FormRequest
{
    public $rules = [];
    public $messages = [];
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function __construct()
    {
        $multi_lang_request_params = ['sye_NameCharacter'=>'required|max:10|min:5'];
        $normal_request_params = [
            'sye_NameNumeric'=>'required',
            'sye_ShortName'=>'required',
            'fkSywCan' => 'required|array|min:1',
            'fkSywCan.*'=>'required|distinct|min:1',
            'sye_NumberofWeek' => 'required|min:1',
            'sye_NumberofWeek.*' => 'required|min:1',
            'pkSye' => 'nullable'
        ];
        $lang_data = new LanguageRequestService($multi_lang_request_params,$normal_request_params);
        $this->rules = $lang_data->fields;
        $this->messages = $lang_data->fields_messages;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return $this->rules;
    }

    public function messages()
    {
        return $this->messages;
    }
}
