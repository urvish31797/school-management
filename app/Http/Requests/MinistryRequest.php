<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use LanguageRequestService;
use Carbon\Carbon;

class MinistryRequest extends FormRequest
{
    public $rules = [];
    public $messages = [];
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function __construct()
    {
        $multi_lang_request_params = [];
        $normal_request_params = [
                                'email'=>'required|email',
                                'adm_Name'=>'required',
                                'adm_Phone'=>'required|max:10|min:10',
                                'adm_Title'=>'required',
                                'adm_Gender'=>'required|in:Male,Female',
                                'adm_DOB'=>'required',
                                'adm_GovId' => 'required',
                                'adm_TempGovId' => 'required',
                                // 'adm_GovId' => 'required_if:havent_identification_number,off',
                                // 'adm_TempGovId' => 'required_if:havent_identification_number,on',
                                'adm_Address'=>'nullable',
                                'fkAdmCan'=>'required',
                                'adm_Status'=>'required|in:Active,Inactive',
                                'adm_Photo'=>'nullable|size:2000|mimes:jpeg,png,jpg',
                                ];
        $lang_data = new LanguageRequestService($multi_lang_request_params,$normal_request_params);
        $this->rules = $lang_data->fields;
        $this->messages = $lang_data->fields_messages;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return $this->rules;
    }

    public function messages()
    {
        return $this->messages;
    }
}
