<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use LanguageRequestService;

class CollegeRequest extends FormRequest
{
    public $rules = [];
    public $messages = [];
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function __construct()
    {
        $multi_lang_request_params = ['col_CollegeName'=>'required|max:10|min:2'];
        $normal_request_params = [
                                    'fkColUni' => 'required_if:col_BelongsToUniversity,Yes',
                                    'fkColCny'=>'required',
                                    'fkColOty'=>'required',
                                    'col_BelongsToUniversity'=>'required|in:Yes,No',
                                    'col_YearStartedFounded'=>'required',
                                    'col_Notes'=>'nullable',
                                    'col_PicturePath'=>'nullable|size:100|mimes:jpeg,png,jpg',
                                ];
        $lang_data = new LanguageRequestService($multi_lang_request_params,$normal_request_params);
        $this->rules = $lang_data->fields;
        $this->messages = $lang_data->fields_messages;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return $this->rules;
    }

    public function messages()
    {
        return $this->messages;
    }
}
