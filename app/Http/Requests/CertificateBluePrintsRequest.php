<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use LanguageRequestService;

class CertificateBluePrintsRequest extends FormRequest
{
    public $rules = [];
    public $messages = [];
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function __construct()
    {
        $multi_lang_request_params = ['cbp_BluePrintName'=>'required','cbp_htmlContent'=>'required'];
        $normal_request_params = [
            'cbp_keywords'=>'required',
            'cbp_startdate'=>'required',
            'cbp_enddate'=>'required',
            'cbp_Cbpstatus'=>'in:Active,Inactive'
        ];
        $lang_data = new LanguageRequestService($multi_lang_request_params,$normal_request_params);
        $this->rules = $lang_data->fields;
        $this->messages = $lang_data->fields_messages;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return $this->rules;
    }

    public function messages()
    {
        return $this->messages;
    }
}
