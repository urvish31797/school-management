<?php

namespace App\Http\Requests;
use Cache;
use Illuminate\Foundation\Http\FormRequest;
use LanguageRequestService;

class TranslationRequest extends FormRequest
{
    public $rules = [];
    public $messages = [];
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function __construct()
    {
        $multi_request_params = [];
        $languages = Cache::get('languages');
        foreach ($languages as $lang_value) {
            $normal_request_params[$lang_value->language_key] = 'required';   
        }
        $normal_request_params['group'] = 'required';
        $normal_request_params['key'] = 'required';
        $lang_data = new LanguageRequestService($multi_request_params,$normal_request_params);
        $this->rules = $lang_data->fields;
        $this->messages = $lang_data->fields_messages;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return $this->rules;
    }

    public function messages()
    {
        return $this->messages;
    }
}
