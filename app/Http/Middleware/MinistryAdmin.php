<?php

namespace App\Http\Middleware;

use Auth;
use Closure;
use Session;

class MinistryAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $error = "Access Prohibited";
        $sessionerror = "Session Timeout";

        $data = $request->session()->all();
        $check_emp_type = isset($data['curr_emp_type']) ? true : false;

        if (Auth::guard('admin')->user()) {

            if (!empty(Auth::guard('admin')->user()->type) && Auth::guard('admin')->user()->type == 'MinistryAdmin' && $check_emp_type == false) {
                return $next($request);
            } else {
                if (request()->ajax()) {
                    return response()->json(['error' => $sessionerror], 401);
                } else {
                    if (Auth::guard('employee')->check()) {
                        return redirect('employee/dashboard');
                    } else if (Auth::guard('admin')->check()) {
                        return redirect('admin/dashboard');
                    }
                }
            }

        } else {
            if (request()->ajax()) {
                return response()->json(['error' => $sessionerror], 401);
            } else {
                return redirect()->back()->with('middleware_error', $error);
            }
        }

    }
}
