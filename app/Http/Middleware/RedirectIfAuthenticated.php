<?php

namespace App\Http\Middleware;

use Closure;
use Redirect;
use Illuminate\Support\Facades\Auth;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = [])
    {
        if (Auth::guard($guard)->check()) {
            return redirect($guard."/dashboard");
        }
        else
        {
            Redirect::to('logout')->send();
        }
        
        return $next($request);
    }
}
