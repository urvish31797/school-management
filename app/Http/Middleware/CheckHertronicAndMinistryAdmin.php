<?php

namespace App\Http\Middleware;

use Auth;
use Closure;
use Session;

class CheckHertronicAndMinistryAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $error = "Access Prohibited";
        $sessionerror = "Session Timeout";

        $data = $request->session()->all();
        $check_emp_type = isset($data['curr_emp_type']) ? true : false;
        if (Auth::guard('admin')->check()) {

            if (!$check_emp_type && !empty(Auth::guard('admin')->user()->type) && Auth::guard('admin')->user()->type == 'HertronicAdmin' || Auth::guard('admin')->user()->type == 'MinistryAdmin') {
                return $next($request);
            } else {
                if (request()->ajax()) {
                    return response()->json(['error' => $sessionerror], 401);
                } else {
                    return redirect()->back()->with('middleware_error', $error);
                }
            }
        } else {
            if (request()->ajax()) {
                return response()->json(['error' => $sessionerror], 401);
            } else {
                return redirect()->back()->with('middleware_error', $error);
            }
        }
    }
}
