<?php

namespace App\Http\Middleware;

use Auth;
use Closure;

class CheckStudent
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $error = "Access Prohibited";
        $sessionerror = "Session Timeout";

        if (Auth::guard('student')->user()) {
            return $next($request);
        } else {
            if (request()->ajax()) {
                return response()->json(['error' => $sessionerror], 401);
            } else {
                return redirect()->back()->with('middleware_error', $error);
            }
        }
    }
}
