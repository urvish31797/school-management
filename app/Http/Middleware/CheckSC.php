<?php

namespace App\Http\Middleware;

use App\Models\Employee;
use App\Models\EmployeesEngagement;
use App\Models\EmployeeType;
use Auth;
use Closure;
use Session;

class CheckSC
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $error = "Access Prohibited";
        $sessionerror = "Session Timeout";

        if (Auth::guard('employee')->user()) {

            if (Session::has('curr_emp_type')) {
                $logged_user_type = Session::get('curr_emp_type');
            } else {
                $logged_user = Auth::guard('employee')->user();
                $mdata = Employee::with('employeesEngagement.employeeType')->where('id', '=', $logged_user->id)->first();
                $logged_user_type = $mdata->employeesEngagement[0]->employeeType->epty_Name;
                Session::put('curr_emp_type', $logged_user_type);
            }

            if (!empty($logged_user_type) && $logged_user_type == "SchoolCoordinator" || $logged_user_type == "SchoolSubAdmin") {
                return $next($request);
            } else {
                if (request()->ajax()) {
                    return response()->json(['error' => $sessionerror], 401);
                } else {
                    return redirect('employee/dashboard')->with('middleware_error', $error);
                }
            }
        } else {
            if (request()->ajax()) {
                return response()->json(['error' => $sessionerror], 401);
            } else {
                return redirect()->back()->with('middleware_error', $error);
            }
        }
    }
}
