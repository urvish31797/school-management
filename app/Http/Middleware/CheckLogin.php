<?php

namespace App\Http\Middleware;

use Auth;
use Closure;
use Redirect;

class CheckLogin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::guard('employee')->check()) {
            return redirect('employee/dashboard');
        } else if (Auth::guard('admin')->check()) {
            return redirect('admin/dashboard');
        } else {
            // Redirect::to('/')->send();
            return $next($request);
        }
    }
}
