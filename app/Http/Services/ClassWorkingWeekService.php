<?php

namespace App\Http\Services;
use App\Models\Student;
use App\Models\ClassCalendarWorkingWeeks;

class ClassWorkingWeekService
{
    /**
     * Class Student List
     *
     * @param [int] $class_sem_id
     * @return object
     */
    public function getClassStudentList($class_sem_id)
    {
        return Student::GetStudentwithId()
        ->whereHas('enrollStudent.classStudents',function($q) use($class_sem_id){
            $q->where('fkSemCcs',$class_sem_id);
        })
        ->with(['enrollStudent'=>function($q) use($class_sem_id){
            $q->select('pkSte','fkSteStu');
        }])
        ->get()
        ->map(function($row){
            return ['id'=>$row->enrollStudent[0]->pkSte,'name'=>$row->studentname];
        })
        ->pluck('name','id')
        ->all();
    }

    public function getOnDutyStudents($data)
    {
        extract($data);
        $details = ['first_student'=>'-',
                    'second_student'=>'-',
                    'working_week'=>'-'];

        $ccw_row = ClassCalendarWorkingWeeks::with(['working_week'=>function($q) use($language){
            $q->select('pkWek','wek_weekName_' . $language . ' AS wek_weekName');
        }])
        ->where('fkCcwShi',$shift_id)
        ->where('fkCcwCcs',$class_sem_id)
        ->whereDate('ccw_startDate','<=',$date)
        ->whereDate('ccw_endDate','>=',$date)
        ->orderBy('pkCcw','ASC')
        ->first();

        if(!empty($ccw_row))
        {
            $enrollIds = [$ccw_row->fkCcwSteOne,$ccw_row->fkCcwSteTwo];
            $working_week = $ccw_row->working_week->wek_weekName;
            $students = Student::with(['enrollStudent'])->whereHas('enrollStudent',function($q) use($enrollIds){
                $q->whereIn('pkSte',$enrollIds);
            })
            ->GetStudentName()
            ->get();

            foreach ($students as $val) {

                if($enrollIds[0] == array_first($val->enrollStudent)->pkSte){
                    $first_student = $val->studentname;
                }
                else
                {
                    $second_student = $val->studentname;
                }

                $details = ['first_student'=>$first_student ?? '','second_student'=>$second_student ?? ''];
            }

            $details['working_week'] = $working_week;
        }

        return $details;
    }

}