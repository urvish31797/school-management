<?php

namespace App\Http\Services;

class ClassCreationSemesterService
{
    /**
     * Create grade with class name
     *
     * @param [object] $class_detail
     * @return return string example((1a, 2a, 3a, 4a, 5a))
     */
    public function getGradewithClass($class_detail)
    {
        $classname = $class_detail->map(function($q){
            return $q->classCreationClasses->cla_ClassName;
        });

        $classGrades = $class_detail->map(function($q){
            return $q->classCreationGrades->pluck('grade')->all();
        });

        foreach (array_first($classGrades) as $key => $value) {
            $classes[] = $value->gra_GradeNumeric.array_first($classname);
        }
        return implode(", ",$classes);
    }
}