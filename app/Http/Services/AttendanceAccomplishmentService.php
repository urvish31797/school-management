<?php

namespace App\Http\Services;

use App\Models\ClassCreationSemester;
use App\Models\ClassStudentsAllocation;
use App\Models\ClassStudentsSemester;
use App\Models\Course;
use App\Models\CourseGroupAllocation;
use App\Models\DailyBookLecture;
use App\Models\DailySyllabusAccomplishment;
use App\Models\EducationPlansMandatoryCourse;
use App\Models\School;
use App\Models\SchoolYearWeekNumber;
use App\Models\SubClassGroup;
use App\Models\VillageSchool;
use UtilHelper;

class AttendanceAccomplishmentService
{
    /**
     * Return Class Type
     *
     * @param [array] $classdata
     * @return array
     */
    public function getClassType($classdata)
    {
        extract($classdata);

        if (empty($coursegroupId) && empty($subclassId)) {
            $classType = 1;
        } elseif (!empty($coursegroupId)) {
            $classType = 2;
        } elseif (!empty($subclassId)) {
            $classType = 3;
        }

        return $classType;
    }

    /**
     * Get Class Information
     *
     * @param [type] $classdata
     * * @param [type] $allGrades
     * * @param [type] $language
     * @return array
     */
    public function getClassInformation($classdata, $allGrades, $language)
    {
        extract($classdata);
        $classInfo = "-";
        $villageschoolName = "-";
        $subclassName = "";
        $grp_name = "";

        $courseName = Course::GetCourseName(compact('language', 'courseId'));
        if (!empty($villageschoolId)) {
            $villageschoolName = VillageSchool::GetVillageSchoolName(compact('language', 'villageschoolId'));
        }

        if (!empty($subclassId)) {
            $subclassName = SubClassGroup::GetSubClassName(compact('language', 'subclassId'));
        }

        if (!empty($coursegroupId)) {
            $grp_name = CourseGroupAllocation::GetCourseGroupName(compact('coursegroupId', 'language'));
        }

        if (!empty($allGrades)) {
            if (!empty($subclassName)) {
                $classInfo = $allGrades . " - " . $subclassName;
            } elseif (!empty($grp_name)) {
                $classInfo = $allGrades . " - " . $grp_name;
            } else {
                $classInfo = $allGrades;
            }
        }

        return compact('courseName', 'villageschoolName', 'classInfo', 'courseId');
    }

    /**
     * Create Dynamic Class Components
     *
     * @param [array] $classdata
     * @param [string] $language
     * @return array
     */
    public function createClassComponents($classdata, $language)
    {
        $classRows = array();
        $classNames = [];
        $classIds = [];
        $gradess = [];
        $gradessId = [];
        $classTeacherOtherCourses = [];
        $classInformation = '';
        if (!empty($classdata)) {
            $allGrades = '';
            extract($classdata);

            $classType = self::getClassType($current_lecture_data);
            $teacherId = $current_employee;
            $classId = $current_lecture_data['classId'];
            $courseId = $current_lecture_data['courseId'];
            $villageschoolId = $current_lecture_data['villageschoolId'];
            $classSemesterData = ClassCreationSemester::GetGradewithClass(compact('language', 'classId'))->toArray();

            foreach ($classSemesterData as $key => $value) {
                $classLabelName = $value['class_creation']['class_creation_classes']['cla_ClassName'];
                $classLabelId = $value['class_creation']['class_creation_classes']['pkCla'];
                $Grades = $value['class_creation']['class_creation_grades'];
                foreach ($Grades as $k => $v) {
                    $classNames[] = $classLabelName;
                    $classIds[] = $classLabelId;
                    $gradess[] = $v['grade']['gra_GradeNumeric'];
                    $gradessId[] = $v['grade']['pkGra'];
                }
            }

            if (!empty($gradess) && !empty($classNames) && !empty($gradessId)) {
                asort($gradess);
                $allGrades = UtilHelper::createClassSyntaxLine($gradess, $classNames);
                $classInformation = self::getClassInformation($current_lecture_data, $allGrades, $language);

                if (!empty($villageschoolId)) {

                    \DB::statement("SET SQL_MODE=''");
                    $classTeacherOtherCourses = ClassStudentsAllocation::select('pkCsa', 'fkCsaEmc', 'fkCsaScg', 'fkCsaCga')
                        ->whereHas('classCreationTeachers', function ($q) use ($classId, $teacherId) {
                            $q->where('fkCtcCcs', $classId)
                                ->where('fkCtcEeg', $teacherId);
                        })
                        ->with([
                            'classCreationCourses' => function ($q) use ($language) {
                                $q->select('pkCrs', 'crs_CourseName_' . $language . ' AS crs_CourseName');
                            },
                        ]);

                    if ($classType == 1) {
                        $classTeacherOtherCourses = $classTeacherOtherCourses
                            ->whereNull('fkCsaScg')
                            ->whereNull('fkCsaCga')
                            ->groupBy('fkCsaEmc')
                            ->get()
                            ->pluck('classCreationCourses');
                    } elseif ($classType == 2) {
                        $classTeacherOtherCourses = $classTeacherOtherCourses
                            ->whereNull('fkCsaScg')
                            ->groupBy('fkCsaEmc')
                            ->get()
                            ->pluck('classCreationCourses');
                    } elseif ($classType == 3) {
                        $classTeacherOtherCourses = $classTeacherOtherCourses
                            ->whereNull('fkCsaCga')
                            ->groupBy('fkCsaEmc')
                            ->get()
                            ->pluck('classCreationCourses');
                    }
                }

                $schoolyearweeknumber = 0;
                $schoolcanton = School::GetSchoolCanton($current_school);
                if (isset($schoolcanton) && !empty($schoolcanton)) {
                    $schoolyearweeknumber = $this->getSchoolYearWeekNumber($current_school_year, $schoolcanton);
                }

                for ($i = 0; $i < count($gradessId); $i++) {
                    // Logic for prescribed total hours start here
                    $courseweekhrs = 0;
                    $prescribed_total_hrs = 0;

                    $graId = $gradessId[$i];
                    $studentDetail = $this->getEducationPlanFromClassStudent($classId, $graId);

                    if (isset($studentDetail) && !empty($studentDetail)) {
                        $educationplanId = $studentDetail['epl'];
                        $courseweekhrs = $this->getCourseWeekHrsFromEducationPlan(compact('educationplanId', 'graId', 'courseId'));
                    }

                    $prescribed_total_hrs = $courseweekhrs * $schoolyearweeknumber;
                    // Logic for prescribed total hours end here

                    // Logic for running lecture through school year start here
                    $dataHourOrder = ['fkDbkCcs' => $classId, 'fkSyaCrs' => $courseId, 'fkSyaEen' => $teacherId, 'fkSyaGra' => $graId];
                    $running_class_hour = $this->getTeacherLectureNumber($dataHourOrder);
                    // Logic for running lecture through school year end here

                    $row['graId'] = $graId;
                    $row['gradeName'] = $gradess[$i];
                    $row['classId'] = $classIds[$i];
                    $row['className'] = $classNames[$i];
                    $row['courses'] = $classTeacherOtherCourses;
                    $row['prescribed'] = $prescribed_total_hrs;
                    $row['hournumber'] = $running_class_hour;
                    $classRows[] = $row;
                }
            }
        }
        return compact('classRows', 'classInformation', 'classType');
    }

    /**
     * Fetch Schoolyear week numbers
     *
     * @param [int] $schoolyearId
     * @param [int] $cantonId
     * @return int|null
     */
    public function getSchoolYearWeekNumber($schoolyearId, $cantonId)
    {
        $schoolyearweekRow = SchoolYearWeekNumber::select('sye_NumberofWeek')->where('fkSywSye', $schoolyearId)->where('fkSywCan', $cantonId)->first();
        if (!empty($schoolyearweekRow)) {
            return $schoolyearweekRow->sye_NumberofWeek;
        } else {
            return null;
        }
    }

    /**
     * This method is use for get education plan from student. because we need course id & week hours from education plan
     *
     * @param [int] $classSemId
     * @param [int] $gradeId
     * @return array|null
     */
    public function getEducationPlanFromClassStudent($classSemId, $gradeId)
    {
        if (!empty($classSemId)) {
            $student = ClassStudentsSemester::select('fkSemSen')
                ->whereHas('classCreationSemester', function ($q) use ($classSemId) {
                    $q->where('pkCcs', $classSemId);
                })
                ->whereHas('studentEnroll', function ($q) use ($gradeId) {
                    $q->where('fkSteGra', $gradeId);
                })
                ->with(['studentEnroll' => function ($q) {
                    $q->select('pkSte', 'fkSteEpl', 'fkSteGra');
                }])
                ->limit(1)
                ->orderBy('pkSem', 'DESC')
                ->first();

            if (!empty($student)) {
                return ['epl' => $student->studentEnroll->fkSteEpl];
            } else {
                return null;
            }

        } else {
            return null;
        }
    }

    /**
     * This method is use for get course week hours from education plan
     *
     * @param [array] $data
     * @return void
     */
    public function getCourseWeekHrsFromEducationPlan($data)
    {
        extract($data);
        $edpDetail = EducationPlansMandatoryCourse::select('emc_hours')
            ->where('fkEmcEpl', $educationplanId)
            ->where('fkEmcGra', $graId)
            ->where('fkEplCrs', $courseId)
            ->first();

        if (!empty($edpDetail)) {
            return $edpDetail->emc_hours;
        } else {
            return null;
        }
    }

    /**
     * Get Teacher Lecture Number
     *
     * @param [array] $data
     * @return void
     */
    public function getTeacherLectureNumber($data)
    {
        extract($data);
        $count = DailySyllabusAccomplishment::whereHas('dailyBookLecture.dailyBook', function ($q) use ($fkDbkCcs) {
            $q->where('fkDbkCcs', $fkDbkCcs);
        })
            ->where('fkSyaCrs', $fkSyaCrs)
            ->where('fkSyaEen', $fkSyaEen)
            ->where('fkSyaGra', $fkSyaGra)
            ->count();

        return $count + 1;
    }

    public function fetchPreviousSyllabusData($courseData, $course, $grade, $classLabelId, $filter)
    {
        $lectures = [];
        $row = '';
        extract($courseData);

        $subclassId = $current_lecture_data['subclassId'];
        $coursegroupId = $current_lecture_data['coursegroupId'];
        $classCcsId = $current_lecture_data['classId'];

        $subclassId = ($subclassId > 0) ? $subclassId : null;
        $coursegroupId = ($coursegroupId > 0) ? $coursegroupId : null;

        $syllabusHistory = DailySyllabusAccomplishment::select('pkSya', 'fkSyaDbl', 'sya_CourseUnitNumber', 'sya_CourseContent')
            ->where('fkSyaCrs', $course)
            ->where('fkSyaScg', $subclassId)
            ->where('fkSyaCga', $coursegroupId)
            ->where('fkSyaEen', $current_employee)
            ->where('fkSyaGra', $grade)
            ->where('fkSyaCla', $classLabelId)
            ->where(function ($q) use ($filter) {
                if ($filter) {
                    $q->where('sya_CourseUnitNumber', 'LIKE', '%' . $filter . '%')
                        ->orWhere('sya_CourseContent', 'LIKE', '%' . $filter . '%');
                }
            })
            ->whereHas('dailyBookLecture.dailyBook', function ($q) use ($classCcsId) {
                $q->where('fkDbkCcs', $classCcsId);
            })
            ->with(['dailyBookLecture.dailyBook' => function ($q) {
                $q->select('pkDbk', 'dbk_Date')
                    ->orderBy('dbk_Date', 'DESC');
            }])
            ->orderBy('pkSya', 'DESC')
            ->get();

        foreach ($syllabusHistory as $k => $v) {
            $id = $v->pkSya;
            $unit = $v->sya_CourseUnitNumber;
            $content = $v->sya_CourseContent;
            $date = $v->dailyBookLecture->dailyBook->dbk_Date;

            $row .= '<div class="card w-100 unit-row" id="' . $id . '">' .
                '<div class="card-body">' .
                '<h6 class="card-title">' . $unit . '</h6>' .
                '<p class="card-text">' . $content . '</p>' .
                '<p class="label">' . $date . '</p>' .
                '</div>' .
                '</div>';
        }

        return ['row' => $row, 'count' => count($syllabusHistory)];
    }

    public function getDailyBookLectureDetails($id,$language)
    {
        return DailyBookLecture::with([
            'educationWay'=>function($query)use($language) {
                $query->select('pkEw','ew_Name_'.$language.' AS ew_Name');
            },
            'attendanceWay'=>function($query)use($language) {
                $query->select('pkAw','aw_Name_'.$language.' AS aw_Name');
            },
            'dailyBook' => function($query) {
                $query->select('pkDbk','fkDbkVsc','fkDbkShi','dbk_Verified','fkDbkCcs', 'dbk_Date');
            },
            'dailyBook.shift'=>function($query) use($language){
                $query->select('pkShi', 'shi_ShiftName_'.$language.' as shi_ShiftName');
            },
            'dailyBook.villageSchool' => function($query) use($language) {
                $query->select('pkVsc', 'vsc_VillageSchoolName_' . $language . ' as vsc_VillageSchoolName');
            },
            'syllabusAccomplishment.course'=>function($query) use($language){
                $query->select('pkCrs','crs_CourseName_'.$language.' AS crs_CourseName');
            },
            'syllabusAccomplishment.subClassgroup' => function($query) use($language) {
                $query->select('pkScg', 'scg_Name_' . $language . ' as scg_Name');
            },
            'syllabusAccomplishment.otherCourseGroup' => function($query) use($language) {
                $query->select('pkCga');
            },
            'syllabusAccomplishment.grade' => function($query){
                $query->select('pkGra','gra_GradeNumeric');
            },
            'syllabusAccomplishment.class' => function($query) use($language){
                $query->select('pkCla', 'cla_ClassName_' . $language . ' as cla_ClassName');
            },
            'syllabusAccomplishment.studentAttendance',
            'syllabusAccomplishment.studentAttendance.classStudentsSemester.studentEnroll.student'=>function($query){
                $query->GetStudentName();
            }
        ])->findorFail($id);
    }

    public function getTeacherClassDetails($id,$language)
    {
        $dailyBookLectureData = $this->getDailyBookLectureDetails($id,$language);
        
        $basic_details = [
            'lecturenumber'=>$dailyBookLectureData->dbl_LectureNumber ?? '',
            'educationway'=>$dailyBookLectureData->educationWay->ew_Name ?? '',
            'educationwayid'=>$dailyBookLectureData->fkDblEw ?? '',
            'attendanceway'=>$dailyBookLectureData->attendanceWay->aw_Name ?? '',
            'attendancewayid'=>$dailyBookLectureData->fkDblAw ?? '',
            'date'=>$dailyBookLectureData->dailyBook->dbk_Date ?? '',
            'class_sem_id'=>$dailyBookLectureData->dailyBook->fkDbkCcs ?? '',
            'verified'=>$dailyBookLectureData->dailyBook->dbk_Verified ?? '',
            'shift'=>$dailyBookLectureData->dailyBook->shift->shi_ShiftName ?? '',
            'shift_id'=>$dailyBookLectureData->dailyBook->shift->pkShi ?? '',
            'note'=>$dailyBookLectureData->dbl_Notes ?? '',
            'villageSchool'=>$dailyBookLectureData->dailyBook->villageSchool->vsc_VillageSchoolName ?? '-'
        ];

        $class_details = $dailyBookLectureData->syllabusAccomplishment->map(function ($q) use($language){
                $coursegroupId = isset($q->otherCourseGroup) ? $q->otherCourseGroup->pkCga : null;
                $course_group = isset($coursegroupId) ? CourseGroupAllocation::GetCourseGroupName(compact('coursegroupId', 'language')) : '';
                $subclass = isset($q->subClassgroup) ? $q->subClassgroup->scg_Name : '';

                $attendance_details = $q->studentAttendance->map(function($qs) use($q){
                    return [
                        'studentname'=>$qs->classStudentsSemester->studentEnroll->student->studentname ?? '',
                        'attendancestatus'=>$qs->full_attendance_status ?? '',
                        'original_status'=>$qs->sae_LectureAttendance ?? '',
                        'attendanceLecturestatus'=>$qs->sae_LecutreStudentStatus ?? '',
                        'grade'=>$q->grade->gra_GradeNumeric ?? '',
                        'class'=>$q->class->cla_ClassName ?? '',
                        'tdesc'=>$qs->sae_StudentTDesc ?? '',
                        'hdesc'=>$qs->sae_StudentHTDesc ?? '',
                        'pkSae'=>$qs->pkSae ?? ''
                    ];
                });

                $class_grade_details = [
                    'grade_class' => $q->grade->gra_GradeNumeric."".$q->class->cla_ClassName
                ];

                $lecture_details = [
                    'pkSya'=>$q->pkSya ?? '',
                    'sya_CourseUnitNumber'=>$q->sya_CourseUnitNumber ?? '',
                    'sya_CourseContent'=>$q->sya_CourseContent ?? '',
                    'sya_LectureOrderNumber'=>$q->sya_LectureOrderNumber ?? '',
                    'grade'=>$q->grade->gra_GradeNumeric ?? '',
                    'class'=>$q->class->cla_ClassName ?? '',
                    'course'=>$q->course->crs_CourseName ?? '',
                    'subclass'=>$subclass ?? '',
                    'othergroup'=>$course_group ?? '',
                ];

                return [
                        'attendance_details'=>$attendance_details,
                        'class_grade_details'=>$class_grade_details,
                        'lecture_details'=>$lecture_details
                        ];
        });

        $classes = [];
        $classes = $class_details->map(function($q){
            return $q['class_grade_details']['grade_class'];
        })->toArray();

        $basic_details['classgradename'] = implode(", ",$classes) ?? '';
        return compact('basic_details','class_details');
    }

    /**
     * Fetch dynamically lecture number, prescribed hours for particular class and particular grade
     *
     * @param [int] $course_id
     * @param [int] $grade_id
     * @param [JSON] $current_class_detail
     * @return JSON
     */
    public function getCourseHoursforVillageSchoolClass($course_id,$grade_id,$current_class_detail)
    {
        $prescribed_total_hrs = 0;
        $lecture_data = json_decode($current_class_detail,true);
        extract($lecture_data);

        // Logic for prescribed total hours start here
        $classId = $current_lecture_data['classId'];
        $courseId = $course_id;
        $graId = $grade_id;
        $teacherId = $current_employee;

        $studentDetail = $this->getEducationPlanFromClassStudent($classId, $graId);

        $schoolyearweeknumber = 0;
        $schoolcanton = School::GetSchoolCanton($current_school);
        if (isset($schoolcanton) && !empty($schoolcanton)) {
            $schoolyearweeknumber = $this->getSchoolYearWeekNumber($current_school_year, $schoolcanton);
        }

        if (isset($studentDetail) && !empty($studentDetail)) {
            $educationplanId = $studentDetail['epl'];
            $courseweekhrs = $this->getCourseWeekHrsFromEducationPlan(compact('educationplanId', 'graId', 'courseId'));

            $prescribed_total_hrs = $courseweekhrs * $schoolyearweeknumber;
        }
        // Logic for prescribed total hours end here

        // Logic for running lecture through school year start here
        $dataHourOrder = [
            'fkDbkCcs' => $classId,
            'fkSyaCrs' => $courseId,
            'fkSyaEen' => $teacherId,
            'fkSyaGra' => $graId
        ];
        $running_class_hour = $this->getTeacherLectureNumber($dataHourOrder);
        // Logic for running lecture through school year end here

        return compact('prescribed_total_hrs','running_class_hour');
    }
}
