<?php

namespace App\Http\Services;
use App\Models\ClassCreationSemester;
use App\Models\HomeRoomTeacher;
use App\Models\EmployeesEngagement;
use App\Models\ClassStudentsSemester;
use App\Models\ClassStudentsAllocation;
use App\Models\CourseGroupAllocation;
use UtilHelper;
use Session;

class EmployeeService
{
    /**
     * Get Assigned Homeroomteacher class Name
     *
     * @param [array] $data
     * @return string
     */
    public function getHomerroomTeacherClass($data)
    {
        extract($data);
        $params = [
            'fkClrSch' => $schoolId,
            'fkClrSye' => $schoolYear
        ];

        $semester_ids = ClassCreationSemester::whereHas('classCreation',function($q) use($params){
            $q->GetSchoolClasses($params);
        })
        ->pluck('pkCcs')
        ->all();

        $homeroom_teacher = HomeRoomTeacher::whereIn('fkHrtCcs',$semester_ids)
        ->where('fkHrtEen',$engagementId)
        ->first();

        $class = "";
        if(!empty($homeroom_teacher))
        {
            $classId = $homeroom_teacher->fkHrtCcs;
            $param_class = compact('language','classId');
            $class_detail = ClassCreationSemester::GetGradewithClass($param_class)->pluck('classCreation');
            $villageSchool = array_first($class_detail)->villageSchool->vsc_VillageSchoolName ?? '';
            $class = (new ClassCreationSemesterService())->getGradewithClass($class_detail);

            $class = empty($villageSchool) ? $class : $class." - ".$villageSchool;

            Session::put('curr_course_class',$classId);
            Session::put('classname',$class);
        }

        return $class;
    }

    // Fetch Teacher Classes Functions Start from Here
    /**
     * Get Teachers Classes List
     * @param [array] $data
     * @return array
     */
    public function getTeachersClasses($data)
    {
        extract($data);
        $classes = [];
        $normalClasses = [];
        $subClasses = [];
        $otherClasses = [];

        $otherTeachingGroups = [];
        $subClassGroups = [];

        if (!empty($schoolYear) && !empty($schoolId) && !empty($semesterId) && !empty($engagementId) && !empty($language)) {
            $empEngagement = EmployeesEngagement::find($engagementId);
            $semesterIds = ClassCreationSemester::with(['classCreation'])
                ->whereHas('classCreation', function ($q) use ($schoolId, $schoolYear) {
                    $q->where('fkClrSye', $schoolYear)
                        ->where('fkClrSch', $schoolId)
                        ->where('clr_Status', 'Publish');
                })
                ->where('fkCcsEdp', $semesterId)
                ->get()
                ->pluck('pkCcs')
                ->all();

            if (!empty($semesterIds)) {
                \DB::statement("SET SQL_MODE=''");
                $TeacherCourses = array();
                foreach ($semesterIds as $id) {
                    $courses = [];
                    $students = ClassStudentsSemester::whereHas('classCreationSemester.classTeachers', function ($q) use ($id, $engagementId) {
                        $q->where('fkCtcEeg', $engagementId)->where('fkCtcCcs', $id);
                    })
                        ->where('fkSemCcs', $id)
                        ->pluck('pkSem')
                        ->all();

                    if (!empty($students)) {
                        $courses = ClassStudentsAllocation::select('fkCsaEmc')
                            ->whereIn('fkCsaSem', $students)
                            ->distinct()
                            ->pluck('fkCsaEmc');
                    }

                    if (count($courses)) {
                        for ($i = 1; $i <= 3; $i++) {
                            $where = '';
                            switch ($i) {
                                case 1:
                                    //this is for normal class
                                    $where = "fkCsaScg IS NULL AND fkCsaCga IS NULL";
                                    break;

                                case 2:
                                    //this is for subclass
                                    $where = "fkCsaScg IS NOT NULL";
                                    break;

                                case 3:
                                    //this is for other course group
                                    $where = "fkCsaCga IS NOT NULL";
                                    break;

                                default:
                                    $where = '';
                                    break;
                            }

                            $TeacherCourse = ClassStudentsAllocation::GetTeacherCourseClass(compact('language', 'id', 'engagementId', 'courses', 'where', 'i'))->toArray();
                            if (count($TeacherCourse)) {
                                $TeacherCourses[] = $TeacherCourse;
                            }
                        }
                    }
                }

                //this is for simple class
                if (count($TeacherCourses)) {
                    $classesData = $this->createNormalClassArray($TeacherCourses);
                    $normalClasses = $classesData['classes'];
                    $subClassGroups = $classesData['subclasses'];
                    $otherTeachingGroups = $classesData['otherclasses'];
                }

                // this is for subclass
                if (count($subClassGroups)) {
                    $subClasses = $this->createSubClassArray($subClassGroups);
                }

                // this is for other teaching groups need to create seperate logic beacuse we need to merge same teaching groups id and combine class
                if (count($otherTeachingGroups)) {
                    $otherClasses = $this->createOtherClassArray($otherTeachingGroups, $language);
                }
            }
        }

        $classes = array_merge($normalClasses, $subClasses, $otherClasses);

        if (!empty($classes)) {

            //this logic for do not consider isbreak = true row from array for default selection
            $index = ($classes[0]['isBreak']) ? 1 : 0;

            if(!empty($classes[$index]['classId']) && !Session::has('curr_course_class')) {
                Session::put('curr_course_class', $classes[$index]);
            }
        } else {
            if (Session::has('curr_course_class')) {
                Session::forget('curr_course_class');
            }
        }
        return $classes;
    }

    /**
     * Create Normal/Simple Class Array
     *
     * @param [array] $TeacherCourses
     * @return void
     */
    public function createNormalClassArray($TeacherCourses)
    {
        $subClassGroups = [];
        $otherTeachingGroups = [];
        $normalClasses = [];

        foreach ($TeacherCourses as $k => $v) {
            foreach ($v as $key => $value) {
                if (isset($value['class_creation_teachers'][0]) && isset($value['class_creation_courses']) && !empty($value['class_creation_courses'])) {
                    $courseName = $value['class_creation_courses']['crs_CourseName'];
                    $className = $value['class_creation_teachers'][0]['class_creation_semester']['class_creation']['class_creation_classes']['cla_ClassName'];
                    $gradesData = $value['class_creation_teachers'][0]['class_creation_semester']['class_creation']['class_creation_grades'];
                    $villageSchool = $value['class_creation_teachers'][0]['class_creation_semester']['class_creation']['village_school'];
                    $grades = [];
                    if (isset($gradesData)) {
                        foreach ($gradesData as $gk => $vk) {
                            $grades[] = $vk['grade']['gra_GradeNumeric'];
                        }
                    }
                    asort($grades);
                    $gradeClassName = UtilHelper::createClassSyntaxLine($grades, $className);

                    if (!empty($value['fkCsaScg'])) {
                        $subClassGroups[$value['fkCsaScg']][$value['fkCsaEmc']][] = $value;
                    }

                    if (!empty($value['fkCsaCga'])) {
                        $otherTeachingGroups[$value['fkCsaCga']][] = $value;
                    }

                    if (empty($value['fkCsaCga']) && empty($value['fkCsaScg'])) {
                        if (!empty($villageSchool)) {
                            $pkVscId = $villageSchool['pkVsc'];
                            $courseLabel = $courseName . " - " . $gradeClassName . " - " . $villageSchool['vsc_VillageSchoolName'];
                        } else {
                            $pkVscId = 0;
                            $courseLabel = $courseName . " - " . $gradeClassName;
                        }

                        $coursegroupId = 0;
                        $subclassId = 0;
                        $normalClasses[] = ['isBreak' => false,
                            'classId' => $value['class_creation_teachers'][0]['class_creation_semester']['pkCcs'],
                            'ccsGroups' => $value['class_creation_teachers'][0]['class_creation_semester']['pkCcs'],
                            'gradeClassName' => $gradeClassName,
                            'courseId' => $value['class_creation_courses']['pkCrs'],
                            'className' => $className,
                            'villageschoolId' => $pkVscId,
                            'subclassId' => $subclassId,
                            'coursegroupId' => $coursegroupId,
                            'courseName' => $courseLabel];
                    }
                }
            }
        }

        $filterClasses = array_column($normalClasses, 'courseName');
        array_multisort($filterClasses, SORT_ASC, $normalClasses);

        return ['classes' => $normalClasses, 'subclasses' => $subClassGroups, 'otherclasses' => $otherTeachingGroups];
    }

    /**
     * Create Sub Class Array
     *
     * @param [array] $subClassGroups
     * @param [string] $language
     * @return JSON
     */
    public function createSubClassArray($subClassGroups)
    {
        $subClasses[] = ['isBreak' => true, 'courseName' => '', 'break_label' => "===============" . trans('sidebar.sidebar_nav_scg') . "==============="];
        foreach ($subClassGroups as $key => $value) {
            foreach ($value as $k => $v) {
                $courseName = $v[0]['class_creation_courses']['crs_CourseName'];
                $className = $v[0]['class_creation_teachers'][0]['class_creation_semester']['class_creation']['class_creation_classes']['cla_ClassName'];
                $gradesData = $v[0]['class_creation_teachers'][0]['class_creation_semester']['class_creation']['class_creation_grades'];
                $grades = [];
                if (isset($gradesData)) {
                    foreach ($gradesData as $gk => $vk) {
                        $grades[] = $vk['grade']['gra_GradeNumeric'];
                    }
                }
                asort($grades);

                $villageSchool = $v[0]['class_creation_teachers'][0]['class_creation_semester']['class_creation']['village_school'];
                $subclassId = $v[0]['fkCsaScg'];
                $subclassName = $v[0]['sub_class_group']['scg_Name'];

                $gradeClassName = UtilHelper::createClassSyntaxLine($grades, $className);
                if (!empty($villageSchool)) {
                    $pkVscId = $villageSchool['pkVsc'];
                    $courseLabel = $courseName . " - " . $gradeClassName . " - " . $subclassName . " - " . $villageSchool['vsc_VillageSchoolName'];
                } else {
                    $pkVscId = 0;
                    $courseLabel = $courseName . " - " . $gradeClassName . " - " . $subclassName;
                }

                $coursegroupId = 0;
                $subClasses[] = ['isBreak' => false,
                    'classId' => $v[0]['class_creation_teachers'][0]['class_creation_semester']['pkCcs'],
                    'gradeClassName' => $gradeClassName,
                    'ccsGroups' => $v[0]['class_creation_teachers'][0]['class_creation_semester']['pkCcs'],
                    'courseId' => $v[0]['class_creation_courses']['pkCrs'],
                    'className' => $className,
                    'villageschoolId' => $pkVscId,
                    'subclassId' => $subclassId,
                    'coursegroupId' => $coursegroupId,
                    'courseName' => $courseLabel];
            }
        }

        $filterClasses = array_column($subClasses, 'courseName');
        array_multisort($filterClasses, SORT_ASC, $subClasses);

        return $subClasses;
    }

    /**
     * Create Other Teaching groups Class
     *
     * @param [array] $otherTeachingGroups
     * @param [string] $language
     * @return JSON
     */
    public function createOtherClassArray($otherTeachingGroups, $language)
    {
        $otherClasses[] = ['isBreak' => true, 'courseName' => '', 'break_label' => "============" . trans('sidebar.sidebar_nav_course_groups') . "============"];

        $ccsGroups = [];
        foreach ($otherTeachingGroups as $key => $value) {
            $grades = [];
            foreach ($value as $k => $v) {
                $className = $v['class_creation_teachers'][0]['class_creation_semester']['class_creation']['class_creation_classes']['cla_ClassName'];
                $gradesData = $v['class_creation_teachers'][0]['class_creation_semester']['class_creation']['class_creation_grades'];
                $villageSchool = $v['class_creation_teachers'][0]['class_creation_semester']['class_creation']['village_school'];

                if (isset($gradesData)) {
                    foreach ($gradesData as $gk => $vk) {
                        if (!in_array($vk['grade']['gra_GradeNumeric'].$className, $grades)) {
                            $grades[] = $vk['grade']['gra_GradeNumeric'].$className;
                        }
                    }
                }

                $pkCcs = $v['class_creation_teachers'][0]['fkCtcCcs'];
                $ccsGroups[] = $v['class_creation_teachers'][0]['fkCtcCcs'];
                $pkCrs = $v['class_creation_courses']['pkCrs'];
                $courseName = $v['class_creation_courses']['crs_CourseName'];
                $coursegroupId = $v['fkCsaCga'];
                $courseGroupName = CourseGroupAllocation::GetCourseGroupName(compact('coursegroupId', 'language'));
            }

            $gradeClassName = implode(", ",$grades);
            if (!empty($villageSchool)) {
                $pkVscId = $villageSchool['pkVsc'];
                $courseLabel = $courseName . " - " . $gradeClassName . " - " . $courseGroupName . " - " . $villageSchool['vsc_VillageSchoolName'];
            } else {
                $pkVscId = 0;
                $courseLabel = $courseName . " - " . $gradeClassName . " - " . $courseGroupName;
            }

            $subclassId = 0;
            $otherClasses[] = ['isBreak' => false,
                'classId' => $pkCcs,
                'ccsGroups' => implode(",",$ccsGroups),
                'gradeClassName' => $gradeClassName,
                'courseId' => $pkCrs,
                'className' => $className,
                'villageschoolId' => $pkVscId,
                'subclassId' => $subclassId,
                'coursegroupId' => $coursegroupId,
                'courseName' => $courseLabel];
        }

        $filterClasses = array_column($otherClasses, 'courseName');
        array_multisort($filterClasses, SORT_ASC, $otherClasses);

        return $otherClasses;
    }

    // Fetch Teacher Classes Functions end here
}