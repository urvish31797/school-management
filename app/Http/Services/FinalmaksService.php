<?php

namespace App\Http\Services;

use App\Models\EducationPlansForeignLanguage;
use App\Models\EducationPlansOptionalCourse;

class FinalmarksService
{
    /**
     * Course Order logic method
     *
     * @param [object] $mdata
     * @param [object] $courseOrderData
     * @return array
     */
    public function createCourseOrderforCourse($mdata, $courseOrderData)
    {
        $courses = [];
        $OCGCrs = [];
        $FCGCrs = [];
        $FLGCrs = [];

        $educationPlan = $mdata->classCreationSemester[0]->classStudentsSemester[0]->studentEnroll->educationPlan->pkEpl;
        $grade = $mdata->classCreationSemester[0]->classStudentsSemester[0]->studentEnroll->grade->pkGra;

        foreach ($mdata->classCreationSemester[0]->classStudentsSemester[0]->classStudentsAllocation as $k => $v) {
            foreach ($courseOrderData->courseOrderAllocation as $ko => $vo) {
                if (empty($v->fkCsaCga) && $v->fkCsaEmc == $vo->fkCoaCrs) {
                    $courses[$vo->coa_OrderNo] = ['pkCsa' => $v->pkCsa,
                        'pkCrs' => $vo->fkCoaCrs,
                        'crs_CourseName' => $v->classCreationCourses->crs_CourseName,
                        'crs_CourseAlternativeName' => $v->classCreationCourses->crs_CourseAlternativeName,
                        'final_mark' => $v->csa_FinalMarks,
                        'dfm_manual' => $v->csa_ManualDescriptMarks,
                        'fkCsaDfm' => $v->fkCsaDfm];
                } else {
                    if (!empty($v->courseGroup->fkCgaFlg) && !empty($v->fkCsaCga) && $vo->course->crs_CourseType == "DummyForeignCourse") {
                        $courseName = explode("-", $vo->course->crs_CourseName_en);
                        $groupOrderNumber = $v->courseGroup->cga_OrderNumber;
                        $where = ['fkEflEpl' => $educationPlan, 'fkEflGra' => $grade, 'efc_order' => $groupOrderNumber];
                        $select = ['fkEflCrs'];
                        $ForiegnEducationPlanCourse = EducationPlansForeignLanguage::FindForiegnCourse(compact('where', 'select'));

                        if ($ForiegnEducationPlanCourse->fkEflCrs == $vo->fkCoaCrs) {
                            $pkCsaAry = array_column($FLGCrs, 'pkCsa');
                            if (!in_array($v->pkCsa, $pkCsaAry)) {
                                $FLGCrs[$vo->coa_OrderNo] = ['pkCsa' => $v->pkCsa,
                                    'pkCrs' => $vo->fkCoaCrs,
                                    'crs_CourseName' => $v->classCreationCourses->crs_CourseName,
                                    'crs_CourseAlternativeName' => $v->classCreationCourses->crs_CourseAlternativeName,
                                    'final_mark' => $v->csa_FinalMarks,
                                    'dfm_manual' => $v->csa_ManualDescriptMarks,
                                    'fkCsaDfm' => $v->fkCsaDfm];
                            }
                        }
                    }
                    if (!empty($v->courseGroup->fkCgaOcg) && !empty($v->fkCsaCga) && $vo->course->crs_CourseType == "DummyOptionalCourse") {
                        $groupOrderNumber = $v->courseGroup->cga_OrderNumber;
                        $where = ['fkEocEpl' => $educationPlan, 'fkEocGra' => $grade, 'eoc_order' => $groupOrderNumber];
                        $select = ['fkEocCrs'];
                        $OptionalEducationPlanCourse = EducationPlansOptionalCourse::FindOptionalCourse(compact('where', 'select'));

                        if ($OptionalEducationPlanCourse->fkEocCrs == $vo->fkCoaCrs) {
                            $pkCsaAry = array_column($OCGCrs, 'pkCsa');
                            if (!in_array($v->pkCsa, $pkCsaAry)) {
                                $OCGCrs[$vo->coa_OrderNo] = ['pkCsa' => $v->pkCsa,
                                    'pkCrs' => $vo->fkCoaCrs,
                                    'crs_CourseName' => $v->classCreationCourses->crs_CourseName,
                                    'crs_CourseAlternativeName' => $v->classCreationCourses->crs_CourseAlternativeName,
                                    'final_mark' => $v->csa_FinalMarks,
                                    'dfm_manual' => $v->csa_ManualDescriptMarks,
                                    'fkCsaDfm' => $v->fkCsaDfm];
                            }
                        }

                    }
                    if (!empty($v->courseGroup->fkCgaFcg) && !empty($v->fkCsaCga)) {
                        $FCGCrs[$v->courseGroup->cga_OrderNumber] = ['pkCsa' => $v->pkCsa,
                            'pkCrs' => $vo->fkCoaCrs,
                            'crs_CourseName' => $v->classCreationCourses->crs_CourseName,
                            'crs_CourseAlternativeName' => $v->classCreationCourses->crs_CourseAlternativeName,
                            'final_mark' => $v->csa_FinalMarks,
                            'dfm_manual' => $v->csa_ManualDescriptMarks,
                            'fkCsaDfm' => $v->fkCsaDfm];
                    }
                }
            }
        }

        //then combine foriegn language course with mandatory course
        if (!empty($FLGCrs)) {
            foreach ($FLGCrs as $key => $value) {
                $courses[$key] = $value;
            }
        }

        ksort($courses);
        ksort($OCGCrs);
        ksort($FCGCrs);

        return ["courses" => $courses,
            "OCGCrs" => $OCGCrs,
            "FCGCrs" => $FCGCrs];
    }

    /**
     * Show DFM Logic method
     *
     * @param [object] $mdata
     * @return boolean true|false
     */
    public function checkStatusforDescriptiveFinalMark($mdata)
    {
        $showDFM = false;
        if ($mdata->classCreationSemester[0]
            ->classStudentsSemester[0]
            ->studentEnroll
            ->educationProgram->edp_Name_en == "Elementary School") {
            if ($mdata->classCreationSemester[0]
                ->classStudentsSemester[0]
                ->studentEnroll
                ->grade->gra_GradeName == 1) {
                if ($mdata->classCreationSemester[0]
                    ->semester->edp_EducationPeriodName_en == 'First Semester' || $mdata->classCreationSemester[0]
                    ->semester->edp_EducationPeriodName_en == 'Second Semester' || $mdata->classCreationSemester[0]
                    ->semester->edp_EducationPeriodName_en == 'End Of School Year') {
                    $showDFM = true;
                }
            } elseif ($mdata->classCreationSemester[0]
                    ->classStudentsSemester[0]
                    ->studentEnroll
                    ->grade->gra_GradeName == 2) {
                    if ($mdata->classCreationSemester[0]
                    ->semester->edp_EducationPeriodName_en == 'First Semester') {
                    $showDFM = true;
                }
            }
        }

        return $showDFM;
    }

    /**
     * Generate Certificate PDF using puppetter
     *
     * @param [int] $id
     * @return array
     */
    public function generatePuppeteerPDF($id)
    {
        $res = array();
        if (!empty($id)) {
            $nodepath = config('services.NODE_PATH');
            $puppeteer_path = base_path('/puppeteer.js');

            $url = url('customapi/render-certificate')."/".$id;
            $storage_path = storage_path('app/public/').config('assetpath.certificate_files');
            if (!file_exists($storage_path)) {
                mkdir($storage_path, 0777, true);
            }
            $filename = time() . ".pdf";
            $saveurl = $storage_path . "/" . $filename;

            exec('' . $nodepath . ' ' . $puppeteer_path . ' ' . $url . ' ' . $saveurl . ' ' . $filename . ' ', $response, $exitcode);
        
            if ($exitcode == 0 && isset($response) && !empty($response) && $response[0]) {
                $file_path = $response[1];
                $pdf_name = $response[2];
                $full_pdf_path = $storage_path . "/" . $pdf_name;

                $res['status'] = true;
                $res['filepath'] = $full_pdf_path;
                $res['filename'] = $pdf_name;
            }
        }

        return $res;
    }

}
