<?php

namespace App\Http\Services;

use App\Models\ClassCreationGrades;
use App\Models\ClassStudentsSemester;

class ClassCreationService
{
    /**
     * Check validation for selected students on step-2 compare with first step selected grades
     *
     * @param [int] $pkClr
     * @param [array] $grades
     * @return boolean
     */
    public function checkSelectedStudents($pkClr, $grades)
    {
        $status = false;
        $uniqueGrades = array_unique($grades);
        $gradesData = ClassCreationGrades::where('fkCcgClr', $pkClr)->pluck('fkCcgGra')->all();
        if (count($gradesData) != count($uniqueGrades)) {
            $status = true;
        } else {
            $comparegrades = array_diff($uniqueGrades, $gradesData);
            if (count($comparegrades)) {
                $status = true;
            }
        }

        return $status;
    }

    public function getAlreadyEnrolledStudent($data)
    {
        extract($data);

        return ClassStudentsSemester::select('pkSem', 'fkSemCcs', 'fkSemSen')
            ->whereHas('classCreationSemester', function ($q) use ($fkClrEdp, $pkCcs) {
                $q->where('fkCcsEdp', $fkClrEdp)->where('pkCcs', '!=', $pkCcs);
            })
            ->whereHas('classCreationSemester.classCreation', function ($q) use ($fkClrSye, $fkClrSch, $fkClrVsc) {
                $q->where('fkClrSye', $fkClrSye)->where('fkClrSch', $fkClrSch);
                // ->where('fkClrVsc',$fkClrVsc);
            })
            ->whereHas('classCreationSemester.classCreation.classCreationGrades', function ($q) use ($classGrades) {
                $q->whereIn('fkCcgGra', $classGrades);
            })
            ->get()
            ->pluck('fkSemSen')
            ->all();
    }
}
