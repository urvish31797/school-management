<?php

namespace App\Http\Services;

use App\Models\ClassCreation;
use App\Models\ClassCreationSemester;
use App\Models\ClassStudentsAllocation;
use App\Models\CourseGroupAllocation;

class CourseGroupService
{
    public function checkCourseGroupExists($params)
    {
        extract($params);
        $exist_courseGroup = CourseGroupAllocation::where('fkCgaSch', $fkClrSch)
            ->where('fkCgaEdu', $semester_id)
            ->where('fkCgaSye', $fkClrSye);

        if (isset($pkCga) && !empty($pkCga)) {
            $exist_courseGroup = $exist_courseGroup->where('pkCga', '!=', $pkCga);
        }

        switch ($course_group_main) {
            case 'ocg':
                $exist_courseGroup = $exist_courseGroup->where('fkCgaOcg', $course_group_sub);
                break;
            case 'fcg':
                $exist_courseGroup = $exist_courseGroup->where('fkCgaFcg', $course_group_sub);
                break;
            case 'flg':
                $exist_courseGroup = $exist_courseGroup->where('fkCgaFlg', $course_group_sub);
                break;
            case 'gpg':
                $exist_courseGroup = $exist_courseGroup->where('fkCgaGpg', $course_group_sub);
                break;
            default:
                break;
        }

        return $exist_courseGroup->count();
    }

    public function checkStudentOrderExists($params)
    {
        extract($params);
        $checkStatus = [];
        $classesIds = ClassCreation::whereHas('classCreationGrades', function ($q) use ($grade_ids) {
            $q->whereIn('fkCcgGra', $grade_ids);
        })
            ->GetSchoolClasses(compact('fkClrSch', 'fkClrSye'))->pluck('pkClr')->all();
        if (!empty($classesIds)) {
            $classSemIds = ClassCreationSemester::GetSemesterClasses($semester_id, $classesIds);
            if (!empty($classSemIds)) {

                $courseGroups = $this->getSimilarCourseGroupOrder($params);

                foreach ($student_ids as $key => $value) {
                    $groups = ClassStudentsAllocation::select('fkCsaCga')
                        ->distinct()
                        ->where('fkCsaSem', $value)
                        ->whereNotNull('fkCsaCga')
                        ->pluck('fkCsaCga')->all();

                    if (!empty($groups) && self::checkMatchCourse($groups, $courseGroups)) {
                        $checkStatus[] = $value;
                    }
                }
            }
        }

        return !empty($checkStatus) ? true : false;
    }

    public function checkMatchCourse($groups, $courseGroups)
    {
        $tempcrs = [];
        foreach ($groups as $key => $value) {
            if (in_array($value, $courseGroups)) {
                $tempcrs[] = $value;
            }
        }

        return !empty($tempcrs) ? true : false;
    }

    public function getSimilarCourseGroupOrder($params)
    {
        extract($params);
        return CourseGroupAllocation::select('pkCga', 'cga_OrderNumber')
            ->where('fkCgaSch', $fkClrSch)
            ->where('fkCgaSye', $fkClrSye)
            ->where('fkCgaEdu', $semester_id)
            ->whereNotNull($column)
            ->where('cga_OrderNumber', $course_order)
            ->pluck('pkCga')
            ->all();
    }
}
