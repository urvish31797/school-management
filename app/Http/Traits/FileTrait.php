<?php
namespace App\Http\Traits;

trait FileTrait
{
    function kbtomb($bytes) {
        $filesize = round($bytes / 1024, 2);
        return $filesize." MB";
    }

    function uploadFiletoStorage($file,$data){
        try{
            extract($data);
            $filename = time() . '.' . $file->getClientOriginalExtension();
            $destinationPath = storage_path('app/public/').$folder_path;
            if (!empty($unlink_file)) {
                $filepath = $destinationPath . "/" . $unlink_file;
                if (file_exists($filepath)) {
                    unlink($filepath);
                }
            }

            $file->move($destinationPath, $filename);

            return $filename;
        }catch(\Exception $e){
            return null;
        }
    }
}