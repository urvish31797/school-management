<?php
namespace App\Http\Traits;

use App\Models\Admin;
use App\Models\Employee;
use App\Models\Student;

trait UserTrait
{
    function checkUserEmailorUidExist($params)
    {
        $temp_id_exist = false;
        $gov_id_exist = false;
        $email_exist = false;

        extract($params);
        $id = isset($id) ? $id : '';

        if(!empty($email))
        {
            $admin_email_exist = Admin::where('email', '=', $email);
            $admin_email_exist = !empty($id) && ($type == "Admin") ? $admin_email_exist->where('id', '!=', $id) : $admin_email_exist;
            $admin_email_exist = $admin_email_exist->count();

            $employee_email_exist = Employee::where('email', '=', $email);
            $employee_email_exist = !empty($id) && ($type == "Employee") ? $employee_email_exist->where('id', '!=', $id) : $employee_email_exist;
            $employee_email_exist = $employee_email_exist->count();

            $student_email_exist = Student::where('email', '=', $email)->orWhere('stu_ParentsEmail', '=', $email);
            $student_email_exist = !empty($id) && ($type == "Student") ? $student_email_exist->where('id', '!=', $id) : $student_email_exist;
            $student_email_exist = $student_email_exist->count();

            if($admin_email_exist > 0 || $employee_email_exist > 0 || $student_email_exist > 0)
            {
                $email_exist = true;
            }
        }

        if(!empty($gov_id))
        {
            $admin_gov_id_exist = Admin::where('adm_GovId', '=', $gov_id);
            $admin_gov_id_exist = !empty($id) && ($type == "Admin") ? $admin_gov_id_exist->where('id', '!=', $id) : $admin_gov_id_exist;
            $admin_gov_id_exist = $admin_gov_id_exist->count();

            $employee_gov_id_exist = Employee::where('emp_EmployeeID', '=', $gov_id);
            $employee_gov_id_exist = !empty($id) && ($type == "Employee") ? $employee_gov_id_exist->where('id', '!=', $id) : $employee_gov_id_exist;
            $employee_gov_id_exist = $employee_gov_id_exist->count();

            $student_gov_id_exist = Student::where('stu_StudentID', '=', $gov_id);
            $student_gov_id_exist = !empty($id) && ($type == "Student") ? $student_gov_id_exist->where('id', '!=', $id) : $student_gov_id_exist;
            $student_gov_id_exist = $student_gov_id_exist->count();

            if($admin_gov_id_exist > 0 || $employee_gov_id_exist > 0 || $student_gov_id_exist > 0)
            {
                $gov_id_exist = true;
            }
        }

        if(!empty($temp_id))
        {
            $admin_temp_id_exist = Admin::where('adm_TempGovId','=', $temp_id);
            $admin_temp_id_exist = !empty($id) && ($type == "Admin") ? $admin_temp_id_exist->where('id', '!=', $id) : $admin_temp_id_exist;
            $admin_temp_id_exist = $admin_temp_id_exist->count();

            $employee_temp_id_exist = Employee::where('emp_TempCitizenId', '=', $temp_id);
            $employee_temp_id_exist = !empty($id) && ($type == "Employee") ? $employee_temp_id_exist->where('id', '!=', $id) : $employee_temp_id_exist;
            $employee_temp_id_exist = $employee_temp_id_exist->count();

            $student_temp_id_exist = Student::where('stu_TempCitizenId', '=', $temp_id);
            $student_temp_id_exist = !empty($id) && ($type == "Student") ? $student_temp_id_exist->where('id', '!=', $id) : $student_temp_id_exist;
            $student_temp_id_exist = $student_temp_id_exist->count();

            if($admin_temp_id_exist > 0 || $employee_temp_id_exist > 0 || $student_temp_id_exist > 0)
            {
                $temp_id_exist = true;
            }
        }

        return ['email_exist'=>$email_exist,'gov_id_exist'=>$gov_id_exist,'temp_id_exist'=>$temp_id_exist];

    }

}