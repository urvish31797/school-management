<?php
namespace App\Http\Traits;

use App\Models\Canton;
use App\Models\ClassCreationSemester;
use App\Models\Classes;
use App\Models\EmployeeType;
use App\Models\Municipality;
use App\Models\PostalCode;
use App\Models\School;
use App\Models\SchoolYear;

trait CommonTrait
{
    /**
     * Get Current Year
     * @return SchoolYear Model Object
     */
    public function getCurrentYear()
    {
        return SchoolYear::where('sye_DefaultYear', 1)->first();
    }

    /**
     * Get CUrrent Semester
     * @param [int] $schoolId
     * @return School Model Object
     */
    public function getCurrentSemester($schoolId)
    {
        return School::where('pkSch', $schoolId)->first();
    }

    /**
     * Get Selected Roles
     * @param [int] $roles
     * @param bool $isId
     * @return array, Model Object
     */
    public function getSelectedRoles($roles, $isId = true)
    {
        $empType = EmployeeType::whereIn('epty_Name', $roles)->get();

        if ($isId) {
            foreach ($empType as $key => $value) {
                $allowedTypes[] = $value->pkEpty;
            }

            return $allowedTypes;
        } else {
            return $empType;
        }
    }

    /**
     * Get Municipality Dropdown
     *
     * @param [string] $lang
     * @return Model Object
     */
    public function getMuncipalityDropdownData($lang)
    {
        return Municipality::with([
            'canton.state.country' => function ($q) use ($lang) {
                $q->select('pkCny', 'cny_CountryName_' . $lang . ' as cny_CountryName');
            }])
            ->select('pkMun', 'fkMunCan', 'mun_MunicipalityName_' . $lang . ' as mun_MunicipalityName')
            ->get();
    }

    /**
     * Get Postal Code
     *
     * @param [string] $lang
     * @return Model Object
     */
    public function getPostalCodeDropdownData($lang)
    {
        return PostalCode::with([
            'municipality' => function ($q) use ($lang) {
                $q->select('pkMun', 'fkMunCan', 'mun_MunicipalityName_' . $lang . ' as mun_MunicipalityName');
            },
            'municipality.canton.state.country' => function ($q) use ($lang) {
                $q->select('pkCny', 'cny_CountryName_' . $lang . ' as cny_CountryName');
            }])
            ->select('pkPof', 'fkPofMun', 'pof_PostOfficeNumber', 'pof_PostOfficeName_' . $lang . ' as pof_PostOfficeName')
            ->get();
    }

    /**
     * Get Canton List for dropdown
     *
     * @param [string] $lang
     * @return Model Object
     */
    public function getCantonList($lang, $cantonId = null)
    {
        return Canton::select('pkCan', 'fkCanSta', 'can_CantonName_' . $lang . ' AS can_CantonName')
            ->with(['state' => function ($q) use ($lang) {
                $q->select('pkSta', 'sta_StateName_' . $lang . ' AS sta_StateName');
            }])
            ->where(function ($q) use ($cantonId) {
                if ($cantonId) {
                    $q->where('pkCan', $cantonId);
                }
            })
            ->orderBy('can_CantonName', 'ASC')
            ->get();
    }
}
