<?php

namespace App\Providers;

use Illuminate\Support\Facades\Route;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to your controller routes.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Http\Controllers';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @return void
     */
    public function boot()
    {
        //
        parent::boot();
    }

    /**
     * Define the routes for the application.
     *
     * @return void
     */
    public function map()
    {
        // $this->mapApiRoutes();

        $this->mapWebRoutes();

        $this->mapAdminRoutes();

        $this->mapEmployeeRoutes();

        $this->mapStudentRoutes();

    }

    /**
     * Define the "web" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @return void
     */
    protected function mapWebRoutes()
    {
        Route::middleware('web')
             ->namespace($this->namespace)
             ->group(base_path('routes/web.php'));
    }

    /**
     * Define the "api" routes for the application.
     *
     * These routes are typically stateless.
     *
     * @return void
     */
    protected function mapApiRoutes()
    {
        Route::prefix('api')
             ->middleware('api')
             ->namespace($this->namespace)
             ->group(base_path('routes/api.php'));
    }

    /**
     * Define the "admin web" routes for the application.
     *
     * These routes are typically stateless.
     *
     * @return void
     */
    protected function mapAdminRoutes()
    {
        Route::middleware('web')
            ->namespace($this->namespace)  //or change its custom_namespace to namespace if you don't need change diractory of your controllers
            ->group(base_path('routes/web/admin_routes.php'));
    }

    /**
     * Define the "employee web" routes for the application.
     *
     * These routes are typically stateless.
     *
     * @return void
     */
    protected function mapEmployeeRoutes()
    {
        Route::middleware('web')
            ->namespace($this->namespace)  //or change its custom_namespace to namespace if you don't need change diractory of your controllers
            ->group(base_path('routes/web/employee_routes.php'));
    }

    /**
     * Define the "students web" routes for the application.
     *
     * These routes are typically stateless.
     *
     * @return void
     */
    protected function mapStudentRoutes()
    {
        Route::middleware('web')
            ->namespace($this->namespace)  //or change its custom_namespace to namespace if you don't need change diractory of your controllers
            ->group(base_path('routes/web/student_routes.php'));
    }

}
