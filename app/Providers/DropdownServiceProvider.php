<?php

namespace App\Providers;

use App\ExtraServices\DropdownService;
use Collective\Html\HtmlServiceProvider;

/**
 * Class MacroServiceProvider
 * @package App\Providers
 */
class DropdownServiceProvider extends HtmlServiceProvider
{
    public function boot()
    {
        $this->publishes([
            __DIR__ . '/DropdownService.php' => base_path('app/ExtraServices/DropdownService.php'),
        ]);
    }

    public function register()
    {
        parent::register();

        $this->app->singleton('form', function ($app) {
            $forms = new DropdownService($app['html'], $app['url'], $app['view'], $app['session.store']->token());
            return $forms->setSessionStore($app['session.store']);
        });
    }

}
