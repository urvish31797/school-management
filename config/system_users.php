<?php

return [
    /**
     * Admin User
     */
    'hertronic_admin' => 'HertronicAdmin',
    'ministry_admin' => 'MinistryAdmin',
    'ministry_sub_admin' => 'MinistrySubAdmin',

    /**
     * Employee User
     */
    'school_coordinator' => 'SchoolCoordinator',
    'school_sub_admin' => 'SchoolSubAdmin',
    'principal' => 'Principal',
    'teacher' => 'Teacher',
    'homeroom_teacher' => 'HomeroomTeacher',
];