<?php

return [
    /**
     * Images Paths
     */
    'user_images' => 'images/users',
    'colleges_images' => 'images/colleges',
    'schools_images' => 'images/schools',
    'students_images' => 'images/students',
    'universities_images' => 'images/universities',

    /**
     * Files Paths
    */
    'certificate_files' => 'files/studentcertificates',
    'users_files' => 'files/users'
];
